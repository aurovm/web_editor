(function (window) {
var Auro = {};
Auro.args = typeof process == "undefined" ? [] : process.argv.slice(1);
Auro.exit = function (code) {
  if (typeof process !== "undefined") process.exit(code)
  else throw "Auro Exit with code " + code
}
Auro.string_new = function (buf) {
  if (typeof buf === 'string') return buf
  var codes = []
  for (var j = 0; j < buf.length; j++) {
    var c = buf[j]
    if (c > 0xEF) {
      c = (c & 0xF) << 0x12 | (buf[++j] & 0x3F) << 0xC | (buf[++j] & 0x3F) << 0x6 | (buf[++j] & 0x3F)
    } else if (c > 0xDF) {
      c = (c & 0xF) << 0xC | (buf[++j] & 0x3F) << 0x6 | (buf[++j] & 0x3F)
    } else if (c > 0xBF) {
      c = (c & 0x1F) << 0x6 | (buf[++j] & 0x3F)
    }
    if (c > 0xFFFF) {
      c -= 0x10000
      codes.push(c >>> 10 & 0x3FF | 0xD800)
      codes.push(0xDC00 | c & 0x3FF)
    } else {
      codes.push(c)
    }
  }
  return String.fromCharCode.apply(String, codes)
}
Auro.require = function (name) {
  if (typeof require !== 'function') return null
  try { return require(name) }
  catch (e) {
  if (e.code === 'MODULE_NOT_FOUND') return null
    else throw e
  }
};
Auro.fs = Auro.require('fs');
Auro.io_open = function (path, mode) {
  return {f: Auro.fs.openSync(path, mode), size: Auro.fs.statSync(path).size, pos: 0}
}
Auro.io_read = function (file, size) {
  var buf = new Uint8Array(size)
  var redd = Auro.fs.readSync(file.f, buf, 0, size, file.pos)
  file.pos += redd
  return buf.slice(0, redd)
}
Auro.io_write = function (file, buf) {
  var written = Auro.fs.writeSync(file.f, buf, 0, buf.length, file.pos)
  file.pos += written
}
Auro.io_close = function (file) {
  Auro.fs.closeSync(file.f)
}
Auro.io_eof = function (file) {
  return file.pos >= file.size
}
function _new (reg_0, reg_1) {
  var reg_6;
  reg_6 = {a: reg_0, b: reg_1, c: (0 - 1), d: []};
  return reg_6;
}
Auro.charat = function (str, i) {
  return [str[i], i+1]
}
function fn3 (reg_0) {
  var reg_2, reg_3, reg_4;
  reg_2 = reg_0.charCodeAt(0);
  reg_4 = (reg_2 == 9);
  if (!reg_4) {
    reg_4 = (reg_2 == 10);
  }
  reg_3 = reg_4;
  if (!reg_3) {
    reg_3 = (reg_2 == 32);
  }
  return reg_3;
}
function fn4 (reg_0) {
  var reg_2, reg_3;
  reg_2 = reg_0.charCodeAt(0);
  reg_3 = (reg_2 >= 48);
  if (reg_3) {
    reg_3 = (reg_2 <= 57);
  }
  return reg_3;
}
function fn5 (reg_0) {
  var reg_2, reg_3, reg_4, reg_7, reg_12;
  reg_2 = reg_0.charCodeAt(0);
  reg_4 = (reg_2 == 95);
  if (!reg_4) {
    reg_7 = (reg_2 >= 65);
    if (reg_7) {
      reg_7 = (reg_2 <= 90);
    }
    reg_4 = reg_7;
  }
  reg_3 = reg_4;
  if (!reg_3) {
    reg_12 = (reg_2 >= 97);
    if (reg_12) {
      reg_12 = (reg_2 <= 122);
    }
    reg_3 = reg_12;
  }
  return reg_3;
}
function fn6 (reg_0) {
  var reg_1, reg_2, reg_3, reg_4, reg_5, reg_6, reg_7, reg_8, reg_9, reg_10, reg_11, reg_12, reg_13, reg_14, reg_15, reg_16, reg_17, reg_18, reg_19, reg_20;
  reg_20 = (reg_0 == "true");
  if (!reg_20) {
    reg_20 = (reg_0 == "false");
  }
  reg_19 = reg_20;
  if (!reg_19) {
    reg_19 = (reg_0 == "null");
  }
  reg_18 = reg_19;
  if (!reg_18) {
    reg_18 = (reg_0 == "void");
  }
  reg_17 = reg_18;
  if (!reg_17) {
    reg_17 = (reg_0 == "if");
  }
  reg_16 = reg_17;
  if (!reg_16) {
    reg_16 = (reg_0 == "else");
  }
  reg_15 = reg_16;
  if (!reg_15) {
    reg_15 = (reg_0 == "while");
  }
  reg_14 = reg_15;
  if (!reg_14) {
    reg_14 = (reg_0 == "return");
  }
  reg_13 = reg_14;
  if (!reg_13) {
    reg_13 = (reg_0 == "continue");
  }
  reg_12 = reg_13;
  if (!reg_12) {
    reg_12 = (reg_0 == "break");
  }
  reg_11 = reg_12;
  if (!reg_11) {
    reg_11 = (reg_0 == "goto");
  }
  reg_10 = reg_11;
  if (!reg_10) {
    reg_10 = (reg_0 == "as");
  }
  reg_9 = reg_10;
  if (!reg_9) {
    reg_9 = (reg_0 == "is");
  }
  reg_8 = reg_9;
  if (!reg_8) {
    reg_8 = (reg_0 == "new");
  }
  reg_7 = reg_8;
  if (!reg_7) {
    reg_7 = (reg_0 == "type");
  }
  reg_6 = reg_7;
  if (!reg_6) {
    reg_6 = (reg_0 == "struct");
  }
  reg_5 = reg_6;
  if (!reg_5) {
    reg_5 = (reg_0 == "import");
  }
  reg_4 = reg_5;
  if (!reg_4) {
    reg_4 = (reg_0 == "module");
  }
  reg_3 = reg_4;
  if (!reg_3) {
    reg_3 = (reg_0 == "extern");
  }
  reg_2 = reg_3;
  if (!reg_2) {
    reg_2 = (reg_0 == "export");
  }
  reg_1 = reg_2;
  if (!reg_1) {
    reg_1 = (reg_0 == "private");
  }
  return reg_1;
}
function fn7 (reg_0) {
  var reg_2, reg_3, reg_4, reg_5;
  reg_2 = reg_0.charCodeAt(0);
  reg_5 = (reg_2 == 33);
  if (!reg_5) {
    reg_5 = (reg_2 == 60);
  }
  reg_4 = reg_5;
  if (!reg_4) {
    reg_4 = (reg_2 == 61);
  }
  reg_3 = reg_4;
  if (!reg_3) {
    reg_3 = (reg_2 == 62);
  }
  return reg_3;
}
function fn8 (reg_0) {
  var reg_2, reg_3;
  reg_2 = reg_0.charCodeAt(0);
  reg_3 = (reg_2 == 38);
  if (!reg_3) {
    reg_3 = (reg_2 == 124);
  }
  return reg_3;
}
function fn9 (reg_0) {
  var reg_2, reg_3, reg_4, reg_5, reg_6, reg_7, reg_8, reg_9, reg_10, reg_11, reg_12, reg_13, reg_14, reg_15;
  reg_2 = reg_0.charCodeAt(0);
  reg_15 = (reg_2 == 40);
  if (!reg_15) {
    reg_15 = (reg_2 == 41);
  }
  reg_14 = reg_15;
  if (!reg_14) {
    reg_14 = (reg_2 == 42);
  }
  reg_13 = reg_14;
  if (!reg_13) {
    reg_13 = (reg_2 == 43);
  }
  reg_12 = reg_13;
  if (!reg_12) {
    reg_12 = (reg_2 == 44);
  }
  reg_11 = reg_12;
  if (!reg_11) {
    reg_11 = (reg_2 == 45);
  }
  reg_10 = reg_11;
  if (!reg_10) {
    reg_10 = (reg_2 == 46);
  }
  reg_9 = reg_10;
  if (!reg_9) {
    reg_9 = (reg_2 == 58);
  }
  reg_8 = reg_9;
  if (!reg_8) {
    reg_8 = (reg_2 == 59);
  }
  reg_7 = reg_8;
  if (!reg_7) {
    reg_7 = (reg_2 == 63);
  }
  reg_6 = reg_7;
  if (!reg_6) {
    reg_6 = (reg_2 == 91);
  }
  reg_5 = reg_6;
  if (!reg_5) {
    reg_5 = (reg_2 == 93);
  }
  reg_4 = reg_5;
  if (!reg_4) {
    reg_4 = (reg_2 == 123);
  }
  reg_3 = reg_4;
  if (!reg_3) {
    reg_3 = (reg_2 == 125);
  }
  return reg_3;
}
function fn10 (reg_0) {
  var reg_2, reg_3, reg_4;
  reg_2 = reg_0.charCodeAt(0);
  reg_4 = (reg_2 == 34);
  if (!reg_4) {
    reg_4 = (reg_2 == 39);
  }
  reg_3 = reg_4;
  if (!reg_3) {
    reg_3 = (reg_2 == 96);
  }
  return reg_3;
}
function fn11 (reg_0, reg_1) {
  console.log(((("Lexer error: " + reg_0) + ", at line ") + String(reg_1)));
  Auro.exit(1);
  return;
}
function fn12 (reg_0) {
  var reg_2, reg_4, reg_5;
  if (fn4(reg_0)) {
    reg_2 = true;
    return reg_2;
  }
  reg_4 = reg_0.charCodeAt(0);
  reg_5 = (reg_4 >= 97);
  if (reg_5) {
    reg_5 = (reg_4 <= 102);
  }
  return reg_5;
}
function fn13 (reg_0) {
  var reg_2, reg_5, reg_9;
  reg_2 = reg_0.charCodeAt(0);
  if (fn4(reg_0)) {
    reg_5 = (reg_2 - 48);
    return reg_5;
  }
  reg_9 = ((10 + reg_2) - 97);
  return reg_9;
}
function fn2 (reg_0) {
  var reg_4, reg_6, reg_8, reg_9, reg_11, reg_12, reg_13, reg_22, reg_23, reg_26, reg_31, reg_32, reg_41, reg_42, reg_46, reg_47, reg_61, reg_62, reg_67, reg_68, reg_70, reg_71, reg_79, reg_81, reg_82, reg_83, reg_92, reg_93, reg_98, reg_102, reg_103, reg_109, reg_111, reg_112, reg_113, reg_118, reg_119, reg_128, reg_130, reg_131, reg_138, reg_139, reg_143, reg_144, reg_145, reg_156, reg_157, reg_168, reg_169, reg_172, reg_174, reg_177, reg_178, reg_186, reg_187, reg_188, reg_189, reg_220, reg_221, reg_226, reg_227, reg_228, reg_241, reg_242, reg_257, reg_258;
  var goto_113=false, goto_117=false, goto_118=false, goto_151=false, goto_170=false, goto_202=false, goto_206=false, goto_232=false, goto_255=false, goto_261=false, goto_274=false, goto_323=false, goto_332=false, goto_341=false, goto_375=false, goto_408=false;
  reg_0 = (reg_0 + " ");
  reg_4 = [];
  reg_6 = reg_0.length;
  reg_8 = 0;
  reg_11 = 1;
  var _r = Auro.charat(reg_0, reg_8); reg_12 = _r[0]; reg_13 = _r[1];
  reg_9 = reg_12;
  reg_8 = reg_13;
  loop_1: while ((reg_8 < reg_6)) {
    loop_5: while (true) {
      if (fn3(reg_9)) {
        if ((reg_9.charCodeAt(0) == 10)) {
          reg_11 = (reg_11 + 1);
        }
        if ((reg_8 >= reg_6)) {
          if (true) break loop_1;
        }
        var _r = Auro.charat(reg_0, reg_8); reg_22 = _r[0]; reg_23 = _r[1];
        reg_9 = reg_22;
        reg_8 = reg_23;
        if (true) continue loop_5;
      }
      reg_9.charCodeAt(0);
      goto_118 = !(reg_9.charCodeAt(0) == 47);
      if (goto_118) break loop_5;
      if ((reg_8 < reg_6)) {
        var _r = Auro.charat(reg_0, reg_8); reg_31 = _r[0]; reg_32 = _r[1];
        reg_9 = reg_31;
        reg_8 = reg_32;
      }
      if ((reg_9.charCodeAt(0) == 47)) {
        loop_7: while (!(reg_9.charCodeAt(0) == 10)) {
          if ((reg_8 >= reg_6)) {
            if (true) break loop_1;
          }
          var _r = Auro.charat(reg_0, reg_8); reg_41 = _r[0]; reg_42 = _r[1];
          reg_9 = reg_41;
          reg_8 = reg_42;
        }
        reg_11 = (reg_11 + 1);
        if ((reg_8 < reg_6)) {
          var _r = Auro.charat(reg_0, reg_8); reg_46 = _r[0]; reg_47 = _r[1];
          reg_9 = reg_46;
          reg_8 = reg_47;
        }
        if (true) continue loop_5;
        goto_117 = true;
        if (goto_117) break loop_5;
      }
      goto_113 = !(reg_9.charCodeAt(0) == 42);
      if (goto_113) break loop_5;
      loop_6: while (true) {
        if ((reg_9.charCodeAt(0) == 10)) {
          reg_11 = (reg_11 + 1);
        }
        if ((reg_9.charCodeAt(0) == 42)) {
          if ((reg_8 >= reg_6)) {
            if (true) break loop_1;
          }
          var _r = Auro.charat(reg_0, reg_8); reg_61 = _r[0]; reg_62 = _r[1];
          reg_9 = reg_61;
          reg_8 = reg_62;
          if ((reg_9.charCodeAt(0) == 47)) {
            if (true) break loop_6;
          }
        }
        if ((reg_8 >= reg_6)) {
          if (true) break loop_1;
        }
        var _r = Auro.charat(reg_0, reg_8); reg_67 = _r[0]; reg_68 = _r[1];
        reg_9 = reg_67;
        reg_8 = reg_68;
      }
      if ((reg_8 < reg_6)) {
        var _r = Auro.charat(reg_0, reg_8); reg_70 = _r[0]; reg_71 = _r[1];
        reg_9 = reg_70;
        reg_8 = reg_71;
      }
    }
    goto_118 = goto_118;
    if (!goto_118) {
      if (!goto_117) {
        goto_113 = goto_113;
        if (!goto_113) {
        }
        if ((goto_113 || false)) {
          goto_113 = false;
          reg_26 = {a: "/", b: "", c: reg_11};
        }
      }
      goto_117 = false;
    }
    if ((goto_118 || false)) {
      goto_118 = false;
      goto_151 = !(reg_9.charCodeAt(0) == 36);
      if (!goto_151) {
        reg_79 = "$";
        if ((reg_8 < reg_6)) {
          var _r = Auro.charat(reg_0, reg_8); reg_81 = _r[0]; reg_82 = _r[1];
          reg_9 = reg_81;
          reg_8 = reg_82;
          reg_83 = (reg_9.charCodeAt(0) == 60);
          if (!reg_83) {
            reg_83 = (reg_9.charCodeAt(0) == 46);
          }
          if (reg_83) {
            reg_79 = (reg_79 + reg_9);
            if ((reg_8 < reg_6)) {
              var _r = Auro.charat(reg_0, reg_8); reg_92 = _r[0]; reg_93 = _r[1];
              reg_9 = reg_92;
              reg_8 = reg_93;
            }
          }
        }
        reg_26 = {a: reg_79, b: "", c: reg_11};
      }
      if ((goto_151 || false)) {
        goto_151 = false;
        goto_170 = !fn4(reg_9);
        if (!goto_170) {
          reg_98 = "";
          loop_4: while (fn4(reg_9)) {
            reg_98 = (reg_98 + reg_9);
            if ((reg_8 >= reg_6)) {
              if (true) break loop_4;
            }
            var _r = Auro.charat(reg_0, reg_8); reg_102 = _r[0]; reg_103 = _r[1];
            reg_9 = reg_102;
            reg_8 = reg_103;
          }
          reg_26 = {a: "num", b: reg_98, c: reg_11};
        }
        if ((goto_170 || false)) {
          goto_170 = false;
          goto_206 = !fn5(reg_9);
          if (!goto_206) {
            reg_109 = ("" + reg_9);
            if ((reg_8 < reg_6)) {
              var _r = Auro.charat(reg_0, reg_8); reg_111 = _r[0]; reg_112 = _r[1];
              reg_9 = reg_111;
              reg_8 = reg_112;
              loop_3: while (true) {
                reg_113 = fn5(reg_9);
                if (!reg_113) {
                  reg_113 = fn4(reg_9);
                }
                if (!reg_113) break loop_3;
                reg_109 = (reg_109 + reg_9);
                if ((reg_8 >= reg_6)) {
                  if (true) break loop_3;
                }
                var _r = Auro.charat(reg_0, reg_8); reg_118 = _r[0]; reg_119 = _r[1];
                reg_9 = reg_118;
                reg_8 = reg_119;
              }
            }
            goto_202 = !fn6(reg_109);
            if (!goto_202) {
              reg_26 = {a: reg_109, b: "", c: reg_11};
            }
            if ((goto_202 || false)) {
              goto_202 = false;
              reg_26 = {a: "name", b: reg_109, c: reg_11};
            }
          }
          if ((goto_206 || false)) {
            goto_206 = false;
            goto_232 = !fn7(reg_9);
            if (!goto_232) {
              reg_128 = ("" + reg_9);
              if ((reg_8 < reg_6)) {
                var _r = Auro.charat(reg_0, reg_8); reg_130 = _r[0]; reg_131 = _r[1];
                reg_9 = reg_130;
                reg_8 = reg_131;
                if ((reg_9.charCodeAt(0) == 61)) {
                  reg_128 = (reg_128 + "=");
                  if ((reg_8 < reg_6)) {
                    var _r = Auro.charat(reg_0, reg_8); reg_138 = _r[0]; reg_139 = _r[1];
                    reg_9 = reg_138;
                    reg_8 = reg_139;
                  }
                }
              }
              reg_26 = {a: reg_128, b: "", c: reg_11};
            }
            if ((goto_232 || false)) {
              goto_232 = false;
              goto_261 = !fn8(reg_9);
              if (!goto_261) {
                reg_143 = reg_9;
                var _r = Auro.charat(reg_0, reg_8); reg_144 = _r[0]; reg_145 = _r[1];
                reg_9 = reg_144;
                reg_8 = reg_145;
                goto_255 = !(reg_143.charCodeAt(0) == reg_9.charCodeAt(0));
                if (!goto_255) {
                  reg_26 = {a: (("" + reg_9) + reg_9), b: "", c: reg_11};
                  if ((reg_8 < reg_6)) {
                    var _r = Auro.charat(reg_0, reg_8); reg_156 = _r[0]; reg_157 = _r[1];
                    reg_9 = reg_156;
                    reg_8 = reg_157;
                  }
                }
                if ((goto_255 || false)) {
                  goto_255 = false;
                  reg_26 = {a: ("" + reg_143), b: "", c: reg_11};
                }
              }
              if ((goto_261 || false)) {
                goto_261 = false;
                goto_274 = !fn9(reg_9);
                if (!goto_274) {
                  reg_26 = {a: ("" + reg_9), b: "", c: reg_11};
                  if ((reg_8 < reg_6)) {
                    var _r = Auro.charat(reg_0, reg_8); reg_168 = _r[0]; reg_169 = _r[1];
                    reg_9 = reg_168;
                    reg_8 = reg_169;
                  }
                }
                if ((goto_274 || false)) {
                  goto_274 = false;
                  goto_408 = !fn10(reg_9);
                  if (!goto_408) {
                    reg_172 = reg_9.charCodeAt(0);
                    reg_174 = "";
                    loop_2: while (true) {
                      if ((reg_8 >= reg_6)) {
                        fn11("Unfinished string", reg_11);
                      }
                      var _r = Auro.charat(reg_0, reg_8); reg_177 = _r[0]; reg_178 = _r[1];
                      reg_9 = reg_177;
                      reg_8 = reg_178;
                      if ((reg_9.charCodeAt(0) == reg_172)) break loop_2;
                      if ((reg_9.charCodeAt(0) == 92)) {
                        if ((reg_8 >= reg_6)) {
                          fn11("Unfinished string", reg_11);
                        }
                        var _r = Auro.charat(reg_0, reg_8); reg_186 = _r[0]; reg_187 = _r[1];
                        reg_9 = reg_186;
                        reg_8 = reg_187;
                        reg_189 = (reg_9.charCodeAt(0) == 92);
                        if (!reg_189) {
                          reg_189 = (reg_9.charCodeAt(0) == 34);
                        }
                        reg_188 = reg_189;
                        if (!reg_188) {
                          reg_188 = (reg_9.charCodeAt(0) == 39);
                        }
                        goto_323 = !reg_188;
                        if (!goto_323) {
                          reg_174 = (reg_174 + reg_9);
                        }
                        if ((goto_323 || false)) {
                          goto_323 = false;
                          goto_332 = !(reg_9.charCodeAt(0) == 110);
                          if (!goto_332) {
                            reg_174 = (reg_174 + String.fromCharCode(10));
                          }
                          if ((goto_332 || false)) {
                            goto_332 = false;
                            goto_341 = !(reg_9.charCodeAt(0) == 116);
                            if (!goto_341) {
                              reg_174 = (reg_174 + String.fromCharCode(9));
                            }
                            if ((goto_341 || false)) {
                              goto_341 = false;
                              goto_375 = !(reg_9.charCodeAt(0) == 120);
                              if (!goto_375) {
                                if (((reg_8 + 2) >= reg_6)) {
                                  fn11("Unfinished string", reg_11);
                                }
                                var _r = Auro.charat(reg_0, reg_8); reg_220 = _r[0]; reg_221 = _r[1];
                                reg_9 = reg_220;
                                reg_8 = reg_221;
                                if (fn12(reg_9)) {
                                  reg_226 = (fn13(reg_9) * 16);
                                  var _r = Auro.charat(reg_0, reg_8); reg_227 = _r[0]; reg_228 = _r[1];
                                  reg_9 = reg_227;
                                  reg_8 = reg_228;
                                  if (fn12(reg_9)) {
                                    reg_226 = (reg_226 + fn13(reg_9));
                                    reg_174 = (reg_174 + String.fromCharCode(reg_226));
                                    if (true) continue loop_2;
                                  }
                                }
                                fn11("Invalid hexadecimal code", reg_11);
                              }
                              if ((goto_375 || false)) {
                                goto_375 = false;
                                reg_174 = ((reg_174 + String.fromCharCode(92)) + reg_9);
                              }
                            }
                          }
                        }
                        if (true) continue loop_2;
                      }
                      reg_174 = (reg_174 + reg_9);
                    }
                    if ((reg_8 < reg_6)) {
                      var _r = Auro.charat(reg_0, reg_8); reg_241 = _r[0]; reg_242 = _r[1];
                      reg_9 = reg_241;
                      reg_8 = reg_242;
                    }
                    if ((reg_172 == 34)) {
                      reg_26 = {a: "str", b: reg_174, c: reg_11};
                    }
                    if ((reg_172 == 39)) {
                      reg_26 = {a: "char", b: reg_174, c: reg_11};
                    }
                    if ((reg_172 == 96)) {
                      reg_26 = {a: "name", b: reg_174, c: reg_11};
                    }
                  }
                  if ((goto_408 || false)) {
                    goto_408 = false;
                    fn11(("Unexpected character " + reg_9), reg_11);
                  }
                }
              }
            }
          }
        }
      }
    }
    reg_4.push(reg_26);
  }
  reg_257 = "eof";
  reg_258 = "";
  reg_4.push({a: reg_257, b: reg_258, c: (reg_11 + 1)});
  return reg_4;
}
function fn14 (reg_0) {
  var reg_3;
  reg_3 = reg_0.a[reg_0.b];
  return reg_3;
}
function fn17 (reg_0) {
  var reg_2;
  reg_2 = fn14(reg_0);
  reg_0.b = (reg_0.b + 1);
  return reg_2;
}
function fn16 (reg_0, reg_1) {
  var reg_6, reg_7;
  if ((fn14(reg_0).a == reg_1)) {
    reg_5 = fn17(reg_0);
    reg_6 = true;
    return reg_6;
  }
  reg_7 = false;
  return reg_7;
}
function fn19 (reg_0) {
  var reg_2;
  reg_2 = fn14(reg_0).c;
  return reg_2;
}
function fn22 (reg_0, reg_1) {
  var reg_2, reg_6;
  reg_2 = "line ";
  reg_6 = (reg_2 + String(reg_1.c));
  if ((reg_1.a == "eof")) {
    reg_6 = "end of file";
  }
  console.log(((("Parse error: " + reg_0) + ", at ") + reg_6));
  Auro.exit(1);
  return;
}
function fn21 (reg_0, reg_1) {
  if (!(reg_0.a == reg_1)) {
    fn22(((("expected " + reg_1) + " but got ") + reg_0.a), reg_0);
  }
  return;
}
function fn20 (reg_0) {
  var reg_2, reg_4;
  reg_2 = fn17(reg_0);
  fn21(reg_2, "name");
  reg_4 = reg_2.b;
  return reg_4;
}
function inline (reg_0, reg_1) {
  reg_0.c = reg_1;
  return reg_0;
}
function push (reg_0, reg_1) {
  reg_0.d.push(reg_1);
  return;
}
function fn18 (reg_0) {
  var reg_2, reg_8, reg_12, reg_16, reg_25, reg_31, reg_36, reg_45;
  reg_2 = fn19(reg_0);
  if (fn16(reg_0, "$.")) {
    reg_8 = inline(_new("generic_arg", fn20(reg_0)), reg_2);
    return reg_8;
  }
  reg_12 = _new("item", fn20(reg_0));
  loop_2: while (fn16(reg_0, ".")) {
    reg_16 = fn20(reg_0);
    loop_3: while (fn16(reg_0, "$")) {
      reg_16 = ((reg_16 + "\x1d") + fn20(reg_0));
    }
    reg_25 = _new("field", reg_16);
    push(reg_25, reg_12);
    reg_12 = reg_25;
  }
  if (fn16(reg_0, "$<")) {
    reg_31 = _new("generic", "");
    push(reg_31, inline(reg_12, reg_2));
    reg_12 = reg_31;
    loop_1: while (true) {
      reg_36 = _new("decl", fn20(reg_0));
      fn21(fn17(reg_0), "=");
      push(reg_36, fn18(reg_0));
      push(reg_12, reg_36);
      if (!fn16(reg_0, ",")) break loop_1;
    }
    fn21(fn17(reg_0), ">");
  }
  reg_45 = inline(reg_12, reg_2);
  return reg_45;
}
function fn23 (reg_0) {
  var reg_2;
  reg_2 = "";
  loop_1: while (true) {
    reg_2 = (reg_2 + fn20(reg_0));
    if (fn16(reg_0, ".")) {
      reg_2 = (reg_2 + "\x1f");
      if (true) continue loop_1;
    }
    if (!fn16(reg_0, "$")) break loop_1;
    reg_2 = (reg_2 + "\x1d");
  }
  return reg_2;
}
function fn25 (reg_0) {
  var reg_2;
  reg_2 = fn14(reg_0).a;
  return reg_2;
}
function len (reg_0) {
  var reg_2;
  reg_2 = reg_0.d.length;
  return reg_2;
}
function child (reg_0, reg_1) {
  var reg_4;
  reg_4 = reg_0.d[reg_1];
  return reg_4;
}
function fn24 (reg_0) {
  var reg_4, reg_8, reg_16, reg_22, reg_29, reg_43, reg_51, reg_54, reg_59, reg_62;
  var goto_60=false;
  reg_4 = fn14(reg_0).c;
  reg_8 = _new("outs", "");
  if (!fn16(reg_0, "void")) {
    loop_2: while (true) {
      push(reg_8, fn18(reg_0));
      if (!fn16(reg_0, ",")) break loop_2;
    }
  }
  reg_16 = fn23(reg_0);
  goto_60 = !fn16(reg_0, "(");
  if (!goto_60) {
    reg_22 = _new("ins", "");
    if (!(fn25(reg_0) == ")")) {
      loop_1: while (true) {
        reg_29 = _new("arg", "");
        push(reg_29, fn18(reg_0));
        if ((fn25(reg_0) == "name")) {
          reg_29.b = fn20(reg_0);
        }
        push(reg_22, reg_29);
        if (!(fn25(reg_0) == ",")) break loop_1;
        reg_38 = fn17(reg_0);
      }
    }
    fn21(fn17(reg_0), ")");
    reg_43 = _new("function", reg_16);
    reg_43.c = reg_4;
    push(reg_43, reg_22);
    push(reg_43, reg_8);
    return reg_43;
  }
  if ((goto_60 || false)) {
    goto_60 = false;
    if ((len(reg_8) == 1)) {
      if (fn16(reg_0, ";")) {
        reg_51 = child(reg_8, 0);
        reg_54 = _new("decl", reg_16);
        reg_54.c = reg_4;
        push(reg_54, reg_51);
        return reg_54;
      }
      if (fn16(reg_0, "=")) {
        reg_59 = child(reg_8, 0);
        reg_62 = _new("decl_assign", reg_16);
        reg_62.c = reg_4;
        push(reg_62, reg_59);
        fn21(fn17(reg_0), ";");
        return reg_62;
      }
    }
  }
  fn21(fn14(reg_0), "(");
}
function fn31 (reg_0) {
  var reg_1;
  reg_1 = (reg_0 == "-");
  if (!reg_1) {
    reg_1 = (reg_0 == "!");
  }
  return reg_1;
}
function fn33 (reg_0) {
  var reg_1;
  reg_1 = fn18(reg_0);
  return reg_1;
}
function fn32 (reg_0) {
  var reg_2, reg_3, reg_5, reg_7, reg_11, reg_50;
  var goto_17=false, goto_25=false, goto_33=false, goto_41=false, goto_49=false, goto_57=false, goto_73=false;
  reg_2 = fn19(reg_0);
  reg_5 = fn17(reg_0);
  reg_7 = reg_5.a;
  goto_17 = !(reg_7 == "(");
  if (!goto_17) {
    reg_11 = fn29(reg_0);
    fn21(fn17(reg_0), ")");
    return reg_11;
  }
  if ((goto_17 || false)) {
    goto_17 = false;
    goto_25 = !(reg_7 == "num");
    if (!goto_25) {
      reg_3 = _new("num", reg_5.b);
    }
    if ((goto_25 || false)) {
      goto_25 = false;
      goto_33 = !(reg_7 == "str");
      if (!goto_33) {
        reg_3 = _new("str", reg_5.b);
      }
      if ((goto_33 || false)) {
        goto_33 = false;
        goto_41 = !(reg_7 == "true");
        if (!goto_41) {
          reg_3 = _new("true", "");
        }
        if ((goto_41 || false)) {
          goto_41 = false;
          goto_49 = !(reg_7 == "false");
          if (!goto_49) {
            reg_3 = _new("false", "");
          }
          if ((goto_49 || false)) {
            goto_49 = false;
            goto_57 = !(reg_7 == "name");
            if (!goto_57) {
              reg_3 = _new("var", reg_5.b);
            }
            if ((goto_57 || false)) {
              goto_57 = false;
              goto_73 = !(reg_7 == "new");
              if (!goto_73) {
                reg_3 = _new("new", "");
                push(reg_3, fn33(reg_0));
                fn21(fn17(reg_0), "(");
                push(reg_3, fn28(reg_0, ")"));
              }
              if ((goto_73 || false)) {
                goto_73 = false;
                fn22("invalid expression", reg_5);
              }
            }
          }
        }
      }
    }
  }
  reg_50 = inline(reg_3, reg_2);
  return reg_50;
}
function fn30 (reg_0) {
  var reg_4, reg_7, reg_9, reg_12, reg_13, reg_15, reg_17, reg_23, reg_32, reg_39, reg_45, reg_51, reg_62;
  if (fn31(fn25(reg_0))) {
    reg_4 = fn19(reg_0);
    reg_7 = fn17(reg_0).a;
    reg_9 = fn30(reg_0);
    reg_12 = _new("unop", reg_7);
    push(reg_12, reg_9);
    reg_13 = inline(reg_12, reg_4);
    return reg_13;
  }
  reg_15 = fn32(reg_0);
  loop_2: while (true) {
    reg_17 = fn19(reg_0);
    if (fn16(reg_0, "(")) {
      reg_23 = _new("call", "");
      push(reg_23, reg_15);
      push(reg_23, fn28(reg_0, ")"));
      reg_15 = inline(reg_23, reg_17);
      if (true) continue loop_2;
      if (true) break loop_2;
    }
    if (fn16(reg_0, ".")) {
      reg_32 = _new("field", fn20(reg_0));
      push(reg_32, reg_15);
      reg_15 = inline(reg_32, reg_17);
      if (true) continue loop_2;
      if (true) break loop_2;
    }
    if (!fn16(reg_0, "[")) break loop_2;
    reg_39 = _new("index", "");
    push(reg_39, reg_15);
    push(reg_39, fn29(reg_0));
    fn21(fn17(reg_0), "]");
    reg_15 = inline(reg_39, reg_17);
  }
  reg_45 = fn19(reg_0);
  loop_1: while (fn16(reg_0, "as")) {
    reg_51 = _new("cast", "");
    if (fn16(reg_0, "?")) {
      reg_51.a = "anycast";
    }
    push(reg_51, reg_15);
    push(reg_51, fn33(reg_0));
    reg_15 = inline(reg_51, reg_45);
  }
  if (fn16(reg_0, "is")) {
    reg_62 = _new("is", "");
    push(reg_62, reg_15);
    push(reg_62, fn33(reg_0));
    reg_15 = inline(reg_62, reg_45);
  }
  return reg_15;
}
function fn34 (reg_0) {
  var reg_1, reg_2, reg_3, reg_4, reg_5, reg_6, reg_7, reg_8, reg_9, reg_10, reg_11;
  reg_11 = (reg_0 == "+");
  if (!reg_11) {
    reg_11 = (reg_0 == "-");
  }
  reg_10 = reg_11;
  if (!reg_10) {
    reg_10 = (reg_0 == "*");
  }
  reg_9 = reg_10;
  if (!reg_9) {
    reg_9 = (reg_0 == "/");
  }
  reg_8 = reg_9;
  if (!reg_8) {
    reg_8 = (reg_0 == "<");
  }
  reg_7 = reg_8;
  if (!reg_7) {
    reg_7 = (reg_0 == ">");
  }
  reg_6 = reg_7;
  if (!reg_6) {
    reg_6 = (reg_0 == "<=");
  }
  reg_5 = reg_6;
  if (!reg_5) {
    reg_5 = (reg_0 == "==");
  }
  reg_4 = reg_5;
  if (!reg_4) {
    reg_4 = (reg_0 == ">=");
  }
  reg_3 = reg_4;
  if (!reg_3) {
    reg_3 = (reg_0 == "!=");
  }
  reg_2 = reg_3;
  if (!reg_2) {
    reg_2 = (reg_0 == "&&");
  }
  reg_1 = reg_2;
  if (!reg_1) {
    reg_1 = (reg_0 == "||");
  }
  return reg_1;
}
function fn29 (reg_0) {
  var reg_2, reg_7, reg_9, reg_12;
  reg_2 = fn30(reg_0);
  loop_1: while (fn34(fn25(reg_0))) {
    reg_7 = fn17(reg_0).a;
    reg_9 = fn30(reg_0);
    reg_12 = _new("binop", reg_7);
    if ((reg_7 == "||")) {
      reg_12.a = "logic";
    }
    if ((reg_7 == "&&")) {
      reg_12.a = "logic";
    }
    push(reg_12, reg_2);
    push(reg_12, reg_9);
    reg_2 = reg_12;
  }
  return reg_2;
}
function fn28 (reg_0, reg_1) {
  var reg_5;
  reg_5 = _new("exprlist", "");
  if (!(fn25(reg_0) == reg_1)) {
    loop_1: while (true) {
      push(reg_5, fn29(reg_0));
      if (!fn16(reg_0, ",")) break loop_1;
    }
  }
  fn21(fn17(reg_0), reg_1);
  return reg_5;
}
function fn35 (reg_0, reg_1) {
  var reg_2, reg_5;
  reg_2 = reg_0.a;
  reg_5 = reg_2[(reg_0.b + reg_1)];
  return reg_5;
}
function fn36 (reg_0) {
  var reg_4, reg_9;
  reg_4 = _new("decl", "");
  push(reg_4, fn33(reg_0));
  loop_1: while (true) {
    reg_9 = _new("declpart", fn20(reg_0));
    if (fn16(reg_0, "=")) {
      push(reg_9, fn29(reg_0));
    }
    push(reg_4, reg_9);
    if (!fn16(reg_0, ",")) break loop_1;
  }
  fn21(fn17(reg_0), ";");
  return reg_4;
}
function fn37 (reg_0, reg_1) {
  var reg_5, reg_13, reg_19;
  reg_5 = _new("exprlist", "");
  push(reg_5, reg_1);
  loop_1: while (!!fn16(reg_0, ",")) {
    push(reg_5, fn29(reg_0));
  }
  fn21(fn17(reg_0), "=");
  reg_13 = fn29(reg_0);
  fn21(fn17(reg_0), ";");
  reg_19 = _new("assignment", "");
  push(reg_19, reg_5);
  push(reg_19, reg_13);
  return reg_19;
}
function fn27 (reg_0) {
  var reg_2, reg_4, reg_8, reg_14, reg_17, reg_23, reg_26, reg_32, reg_39, reg_45, reg_55, reg_61, reg_68, reg_75, reg_79, reg_85, reg_88, reg_97, reg_101, reg_105, reg_107, reg_108, reg_116;
  var goto_118=false;
  reg_2 = fn19(reg_0);
  reg_4 = fn14(reg_0);
  if ((fn25(reg_0) == "{")) {
    reg_8 = fn26(reg_0);
    return reg_8;
  }
  if (fn16(reg_0, "goto")) {
    reg_14 = _new("goto", fn20(reg_0));
    fn21(fn17(reg_0), ";");
    reg_17 = inline(reg_14, reg_2);
    return reg_17;
  }
  if (fn16(reg_0, "return")) {
    reg_23 = _new("return", "");
    push(reg_23, fn28(reg_0, ";"));
    reg_26 = inline(reg_23, reg_2);
    return reg_26;
  }
  if (fn16(reg_0, "break")) {
    reg_32 = _new("break", "");
    if ((fn25(reg_0) == "name")) {
      reg_32.b = fn20(reg_0);
    }
    fn21(fn17(reg_0), ";");
    reg_39 = inline(reg_32, reg_2);
    return reg_39;
  }
  if (fn16(reg_0, "if")) {
    reg_45 = _new("if", "");
    fn21(fn17(reg_0), "(");
    push(reg_45, fn29(reg_0));
    fn21(fn17(reg_0), ")");
    push(reg_45, fn27(reg_0));
    if (fn16(reg_0, "else")) {
      push(reg_45, fn27(reg_0));
    }
    reg_55 = inline(reg_45, reg_2);
    return reg_55;
  }
  if (fn16(reg_0, "while")) {
    reg_61 = _new("while", "");
    fn21(fn17(reg_0), "(");
    push(reg_61, fn29(reg_0));
    fn21(fn17(reg_0), ")");
    push(reg_61, fn27(reg_0));
    reg_68 = inline(reg_61, reg_2);
    return reg_68;
  }
  if ((fn25(reg_0) == "name")) {
    reg_75 = fn35(reg_0, 1).a;
    if ((reg_75 == ":")) {
      reg_79 = fn20(reg_0);
      reg_80 = fn17(reg_0);
      goto_118 = !(fn25(reg_0) == "while");
      if (!goto_118) {
        reg_85 = fn27(reg_0);
        reg_85.b = reg_79;
        return reg_85;
      }
      if ((goto_118 || false)) {
        goto_118 = false;
        reg_88 = inline(_new("label", reg_79), reg_2);
        return reg_88;
      }
    }
    if ((reg_75 == "[")) {
      if ((fn35(reg_0, 2).a == "]")) {
        reg_97 = inline(fn36(reg_0), reg_2);
        return reg_97;
      }
    }
    if ((reg_75 == "?")) {
      reg_101 = inline(fn36(reg_0), reg_2);
      return reg_101;
    }
    if ((reg_75 == "name")) {
      reg_105 = inline(fn36(reg_0), reg_2);
      return reg_105;
    }
  }
  reg_107 = fn29(reg_0);
  reg_108 = (fn25(reg_0) == "=");
  if (!reg_108) {
    reg_108 = (fn25(reg_0) == ",");
  }
  if (reg_108) {
    reg_116 = inline(fn37(reg_0, reg_107), reg_2);
    return reg_116;
  }
  if ((reg_107.a == "call")) {
    fn21(fn17(reg_0), ";");
    return reg_107;
  }
  fn22("invalid statement", reg_4);
}
function fn26 (reg_0) {
  var reg_4, reg_10, reg_12;
  reg_4 = _new("block", "");
  fn21(fn17(reg_0), "{");
  loop_1: while (true) {
    if (fn16(reg_0, "}")) {
      return reg_4;
    }
    reg_10 = fn19(reg_0);
    reg_12 = fn27(reg_0);
    reg_12.c = reg_10;
    push(reg_4, reg_12);
  }
}
function fn38 (reg_0) {
  var reg_4, reg_6, reg_8, reg_17, reg_26, reg_46;
  var goto_30=false;
  reg_4 = _new("module-def", fn20(reg_0));
  reg_6 = fn14(reg_0);
  reg_8 = false;
  if (fn16(reg_0, "$<")) {
    reg_8 = true;
    fn21(fn17(reg_0), ">");
  }
  if (fn16(reg_0, "=")) {
    reg_4.a = "module-assign";
    goto_30 = !fn16(reg_0, "import");
    if (!goto_30) {
      reg_17 = _new("import", fn23(reg_0));
    }
    if ((goto_30 || false)) {
      goto_30 = false;
      reg_17 = fn18(reg_0);
      if (fn16(reg_0, "(")) {
        reg_26 = reg_17;
        reg_17 = _new("functor", "");
        push(reg_17, reg_26);
        push(reg_17, fn18(reg_0));
        fn21(fn17(reg_0), ")");
      }
    }
    fn21(fn17(reg_0), ";");
    push(reg_4, reg_17);
    if (reg_8) {
      push(reg_4, _new("generic", ""));
    }
    return reg_4;
  }
  fn21(fn17(reg_0), "{");
  loop_1: while (!(fn25(reg_0) == "}")) {
    reg_46 = _new("item", fn23(reg_0));
    fn21(fn17(reg_0), "=");
    push(reg_46, fn18(reg_0));
    fn21(fn17(reg_0), ";");
    push(reg_4, reg_46);
  }
  fn21(fn17(reg_0), "}");
  if (reg_8) {
    fn22("definition modules can't be generic", reg_6);
  }
  return reg_4;
}
function fn15 (reg_0) {
  var reg_2, reg_8, reg_17, reg_20, reg_21, reg_27, reg_33, reg_45, reg_87;
  var goto_55=false, goto_78=false, goto_92=false, goto_100=false, goto_115=false, goto_118=false, goto_133=false;
  reg_2 = fn14(reg_0);
  if (fn16(reg_0, "export")) {
    reg_8 = _new("export", "");
    push(reg_8, fn18(reg_0));
    if (fn16(reg_0, "as")) {
      reg_8.a = "export-alias";
      reg_8.b = fn20(reg_0);
    }
    fn21(fn17(reg_0), ";");
    reg_17 = inline(reg_8, reg_2.c);
    return reg_17;
  }
  reg_20 = fn16(reg_0, "private");
  goto_55 = !fn16(reg_0, "struct");
  if (!goto_55) {
    reg_27 = _new("struct", fn23(reg_0));
    fn21(fn17(reg_0), "{");
    loop_1: while (!fn16(reg_0, "}")) {
      reg_33 = fn24(reg_0);
      if ((reg_33.a == "function")) {
        push(reg_33, fn26(reg_0));
      }
      push(reg_27, reg_33);
    }
    reg_21 = inline(reg_27, reg_2.c);
  }
  if ((goto_55 || false)) {
    goto_55 = false;
    goto_92 = !fn16(reg_0, "type");
    if (!goto_92) {
      reg_45 = _new("type", fn23(reg_0));
      goto_78 = !fn16(reg_0, "(");
      if (!goto_78) {
        fn21(fn17(reg_0), "(");
        push(reg_45, fn33(reg_0));
        fn21(fn17(reg_0), ")");
        fn21(fn17(reg_0), ";");
      }
      if ((goto_78 || false)) {
        goto_78 = false;
        if (fn16(reg_0, "=")) {
          reg_45.a = "type-assign";
          push(reg_45, fn18(reg_0));
          fn21(fn17(reg_0), ";");
        }
      }
      reg_21 = inline(reg_45, reg_2.c);
    }
    if ((goto_92 || false)) {
      goto_92 = false;
      goto_100 = !fn16(reg_0, "module");
      if (!goto_100) {
        reg_21 = inline(fn38(reg_0), reg_2.c);
      }
      if ((goto_100 || false)) {
        goto_100 = false;
        reg_21 = fn24(reg_0);
        goto_118 = !(reg_21.a == "function");
        if (!goto_118) {
          goto_115 = !fn16(reg_0, "=");
          if (!goto_115) {
            push(reg_21, fn18(reg_0));
            fn21(fn17(reg_0), ";");
          }
          if ((goto_115 || false)) {
            goto_115 = false;
            push(reg_21, fn26(reg_0));
          }
        }
        if ((goto_118 || false)) {
          goto_118 = false;
          if (!(reg_21.a == "decl_assign")) {
            fn22("invalid top level statement", reg_2);
          }
        }
      }
    }
  }
  goto_133 = !reg_20;
  if (!goto_133) {
    reg_87 = _new("private", "");
    push(reg_87, reg_21);
    return reg_87;
  }
  if ((goto_133 || false)) {
    goto_133 = false;
    return reg_21;
  }
  return reg_21;
}
function fn1 (reg_0) {
  var reg_4, reg_8;
  reg_4 = _new("program", "");
  reg_8 = {a: fn2(reg_0), b: 0};
  loop_1: while (!(fn14(reg_8).a == "eof")) {
    push(reg_4, fn15(reg_8));
  }
  return reg_4;
}
function MAX () {
  var reg_2;
  reg_2 = (64 * 8);
  return reg_2;
}
var cns1 = MAX()
function newPart () {
  var reg_4;
  reg_4 = {a: new Uint8Array(cns1), b: 0, c: null};
  return reg_4;
}
function aulang_writer$_new () {
  var reg_0, reg_2;
  reg_0 = newPart();
  reg_2 = {a: reg_0, b: reg_0, c: 0};
  return reg_2;
}
function Module (reg_0) {
  var reg_4, reg_5, reg_6, reg_8, reg_13;
  reg_4 = (reg_0.e.length + 1);
  reg_5 = "";
  reg_6 = "";
  reg_8 = false;
  reg_13 = {a: reg_4, b: reg_5, c: reg_6, d: reg_8, e: false, f: [], g: 0};
  reg_0.e.push(reg_13);
  return reg_13;
}
var record_23 = function (val) { this.val = val; }
function aulang_item$_new (reg_0, reg_1) {
  var reg_4;
  reg_4 = {a: reg_0, b: reg_1, c: false};
  return reg_4;
}
function Item (reg_0, reg_1, reg_2) {
  var reg_3;
  reg_3 = aulang_item$_new(reg_1, reg_2);
  reg_0.l.push(reg_3);
  return reg_3;
}
var record_33 = function (val) { this.val = val; }
function error (reg_0, reg_1, reg_2) {
  throw new Error(((((reg_1 + ", at ") + reg_0.a) + " at line ") + String(reg_2)));
  return;
}
function set (reg_0, reg_1, reg_2) {
  reg_0.f.push({a: reg_1, b: reg_2});
  return;
}
function to_string (reg_0) {
  var reg_4, reg_5, reg_16;
  if ((reg_0.a == "item")) {
    reg_4 = reg_0.b;
    return reg_4;
  }
  reg_5 = reg_0.a;
  if (!(reg_0.b == "")) {
    reg_5 = ((reg_5 + ":") + reg_0.b);
  }
  reg_5 = (reg_5 + "[");
  reg_16 = 0;
  loop_1: while ((reg_16 < len(reg_0))) {
    if ((reg_16 > 0)) {
      reg_5 = (reg_5 + ", ");
    }
    reg_5 = (reg_5 + to_string(child(reg_0, reg_16)));
    reg_16 = (reg_16 + 1);
  }
  reg_5 = (reg_5 + "]");
  return reg_5;
}
function Type (reg_0, reg_1, reg_2) {
  var reg_6;
  reg_6 = {a: reg_0.f.length, b: reg_1, c: reg_2, d: ""};
  reg_0.f.push(reg_6);
  return reg_6;
}
var record_25 = function (val) { this.val = val; }
function aulang_compiler$Function (reg_0) {
  var reg_2, reg_3, reg_9;
  reg_2 = reg_0.g.length;
  reg_3 = "";
  reg_9 = {a: reg_2, b: reg_3, c: false, d: [], e: [], f: null};
  reg_0.g.push(reg_9);
  return reg_9;
}
function newReg (reg_0) {
  var reg_3, reg_6;
  reg_3 = (0 - 1);
  reg_6 = {a: reg_3, b: reg_0, c: false};
  return reg_6;
}
var record_44 = function (val) { this.val = val; }
function inst (reg_0, reg_1) {
  reg_0.b.push(reg_1);
  return;
}
function get_var (reg_0, reg_1, reg_2) {
  var reg_4, reg_15, reg_16;
  reg_4 = reg_0.a[reg_1];
  if ((reg_4 == null)) {
    if ((reg_0.c == null)) {
      throw new Error((("Variable " + reg_1) + " not found"));
    }
    reg_15 = get_var(reg_0.c, reg_1, reg_2);
    return reg_15;
  }
  reg_16 = reg_4;
  return reg_16;
}
function compile_expr_list (reg_0, reg_1) {
  var reg_2, reg_3;
  reg_2 = [];
  reg_3 = 0;
  loop_1: while ((reg_3 < len(reg_1))) {
    reg_2.push(compile_expr(reg_0, child(reg_1, reg_3)));
    reg_3 = (reg_3 + 1);
  }
  return reg_2;
}
var record_47 = function (val) { this.val = val; }
function compile_call (reg_0, reg_1, reg_2) {
  var reg_3, reg_6, reg_9;
  reg_3 = "function";
  reg_6 = aulang_item$_new(reg_3, child(reg_1, 0));
  reg_9 = compile_expr_list(reg_0, child(reg_1, 1));
  inst(reg_0, new record_47({a: new record_33(reg_6), b: reg_9, c: reg_2, d: reg_1}));
  return;
}
function atoi (reg_0) {
  var reg_1, reg_2, reg_5, reg_6, reg_7, reg_8, reg_10;
  reg_1 = 0;
  reg_2 = 0;
  loop_1: while ((reg_2 < reg_0.length)) {
    var _r = Auro.charat(reg_0, reg_2); reg_6 = _r[0]; reg_7 = _r[1];
    reg_5 = reg_6;
    reg_2 = reg_7;
    reg_8 = reg_5.charCodeAt(0);
    reg_10 = (reg_1 * 10);
    reg_1 = (reg_10 + (reg_8 - 48));
  }
  return reg_1;
}
var Integer = function (val) { this.val = val; }
var record_48 = function (val) { this.val = val; }
var record_49 = function (val) { this.val = val; }
var record_50 = function (val) { this.val = val; }
function label (reg_0) {
  var reg_6, reg_9, reg_13;
  if ((reg_0.c == null)) {
    reg_0.d = (reg_0.d + 1);
    reg_6 = ".label_";
    reg_9 = (reg_6 + String(reg_0.d));
    return reg_9;
    if (true) { goto(18); };
  }
  reg_13 = label(reg_0.c);
  return reg_13;
}
var record_51 = function (val) { this.val = val; }
var record_52 = function (val) { this.val = val; }
var record_53 = function (val) { this.val = val; }
var record_54 = function (val) { this.val = val; }
function compile_expr (reg_0, reg_1) {
  var reg_6, reg_11, reg_12, reg_17, reg_27, reg_36, reg_45, reg_54, reg_55, reg_65, reg_68, reg_71, reg_78, reg_80, reg_89, reg_102;
  var goto_130=false, goto_137=false;
  if ((reg_1.a == "var")) {
    reg_6 = get_var(reg_0, reg_1.b, reg_1);
    return reg_6;
  }
  if ((reg_1.a == "call")) {
    reg_11 = newReg("");
    reg_12 = [];
    reg_12.push(reg_11);
    compile_call(reg_0, reg_1, reg_12);
    return reg_11;
  }
  if ((reg_1.a == "num")) {
    reg_17 = newReg("");
    inst(reg_0, new record_48({a: reg_17, b: new Integer(atoi(reg_1.b))}));
    return reg_17;
  }
  if ((reg_1.a == "str")) {
    reg_27 = newReg("");
    inst(reg_0, new record_48({a: reg_27, b: reg_1.b}));
    return reg_27;
  }
  if ((reg_1.a == "true")) {
    reg_36 = newReg("");
    inst(reg_0, new record_48({a: reg_36, b: true}));
    return reg_36;
  }
  if ((reg_1.a == "false")) {
    reg_45 = newReg("");
    inst(reg_0, new record_48({a: reg_45, b: false}));
    return reg_45;
  }
  if ((reg_1.a == "unop")) {
    reg_54 = newReg("");
    reg_55 = reg_1.b;
    inst(reg_0, new record_49({a: reg_55, b: compile_expr(reg_0, child(reg_1, 0)), c: reg_54, d: reg_1}));
    return reg_54;
  }
  if ((reg_1.a == "binop")) {
    reg_65 = newReg("");
    reg_68 = compile_expr(reg_0, child(reg_1, 0));
    reg_71 = compile_expr(reg_0, child(reg_1, 1));
    inst(reg_0, new record_50({a: reg_1.b, b: reg_68, c: reg_71, d: reg_65, e: reg_1}));
    return reg_65;
  }
  if ((reg_1.a == "logic")) {
    reg_78 = label(reg_0);
    reg_80 = newReg("");
    inst(reg_0, new record_51({a: reg_80}));
    inst(reg_0, new record_52({a: compile_expr(reg_0, child(reg_1, 0)), b: reg_80, c: false}));
    goto_130 = !(reg_1.b == "||");
    if (!goto_130) {
      reg_89 = false;
    }
    if ((goto_130 || false)) {
      goto_130 = false;
      goto_137 = !(reg_1.b == "&&");
      if (!goto_137) {
        reg_89 = true;
      }
      if ((goto_137 || false)) {
        goto_137 = false;
        reg_102 = (("logic operator " + reg_1.b) + " not supported, at line ");
        throw new Error((reg_102 + String(reg_1.c)));
      }
    }
    inst(reg_0, new record_53({a: reg_78, b: reg_80, c: 0, d: reg_89}));
    inst(reg_0, new record_52({a: compile_expr(reg_0, child(reg_1, 1)), b: reg_80, c: false}));
    inst(reg_0, new record_54({a: reg_78}));
    return reg_80;
  }
  throw new Error((reg_1.a + " expressions not yet supported"));
}
var record_55 = function (val) { this.val = val; }
var record_56 = function (val) { this.val = val; }
function getLoopInfo (reg_0, reg_1, reg_2) {
  var reg_7, reg_8, reg_29;
  var goto_32=false;
  if (!(reg_0.e == null)) {
    reg_7 = reg_0.e;
    reg_8 = (reg_1 == "");
    if (!reg_8) {
      reg_8 = (reg_7.a == reg_1);
    }
    if (reg_8) {
      return reg_7;
    }
  }
  if ((reg_0.c == null)) {
    goto_32 = !(reg_1 == "");
    if (!goto_32) {
      throw new Error(((("Not in loop " + reg_1) + ", at line ") + String(reg_2)));
    }
    if ((goto_32 || false)) {
      goto_32 = false;
      throw new Error(("Not in a loop, at line " + String(reg_2)));
    }
  }
  reg_29 = getLoopInfo(reg_0.c, reg_1, reg_2);
  return reg_29;
}
var record_57 = function (val) { this.val = val; }
function newLoopInfo (reg_0, reg_1) {
  var reg_4;
  reg_4 = {a: reg_1, b: label(reg_0), c: label(reg_0)};
  return reg_4;
}
var record_58 = function (val) { this.val = val; }
function compile_stmt (reg_0, reg_1) {
  var reg_5, reg_6, reg_11, reg_12, reg_21, reg_24, reg_25, reg_26, reg_46, reg_48, reg_56, reg_60, reg_61, reg_68, reg_71, reg_80, reg_103, reg_111, reg_112, reg_117, reg_123, reg_149, reg_157, reg_170;
  var goto_45=false, goto_111=false, goto_243=false;
  if ((reg_1.a == "block")) {
    reg_5 = {};
    reg_6 = reg_0.b;
    reg_11 = {a: reg_5, b: reg_6, c: reg_0, d: 0, e: null};
    reg_12 = 0;
    loop_4: while ((reg_12 < len(reg_1))) {
      compile_stmt(reg_11, child(reg_1, reg_12));
      reg_12 = (reg_12 + 1);
    }
    return;
  }
  if ((reg_1.a == "decl")) {
    reg_21 = 1;
    loop_3: while ((reg_21 < len(reg_1))) {
      reg_24 = child(reg_1, reg_21);
      reg_25 = reg_24.b;
      reg_26 = newReg(reg_25);
      goto_45 = !(len(reg_24) > 0);
      if (!goto_45) {
        inst(reg_0, new record_55({a: compile_expr(reg_0, child(reg_24, 0)), b: reg_26}));
      }
      if ((goto_45 || false)) {
        goto_45 = false;
        inst(reg_0, new record_56({a: reg_26, b: child(reg_1, 0)}));
      }
      reg_0.a[reg_25] = reg_26;
      reg_21 = (reg_21 + 1);
    }
    return;
  }
  if ((reg_1.a == "assignment")) {
    reg_46 = child(reg_1, 0);
    reg_48 = child(reg_1, 1);
    goto_111 = !(len(reg_46) > 1);
    if (!goto_111) {
      if (!(reg_48.a == "call")) {
        reg_56 = "Expression does not have multiple values, at line ";
        throw new Error((reg_56 + String(reg_1.c)));
      }
      reg_60 = [];
      reg_61 = 0;
      loop_2: while ((reg_61 < len(reg_46))) {
        reg_60.push(newReg(""));
        reg_61 = (reg_61 + 1);
      }
      compile_call(reg_0, reg_48, reg_60);
      reg_68 = 0;
      loop_1: while ((reg_68 < len(reg_46))) {
        reg_71 = reg_60[reg_68];
        inst(reg_0, new record_52({a: reg_71, b: get_var(reg_0, child(reg_46, reg_68).b, reg_46), c: false}));
        reg_68 = (reg_68 + 1);
      }
    }
    if ((goto_111 || false)) {
      goto_111 = false;
      reg_80 = compile_expr(reg_0, reg_48);
      inst(reg_0, new record_52({a: reg_80, b: get_var(reg_0, child(reg_46, 0).b, reg_46), c: false}));
    }
    return;
  }
  if ((reg_1.a == "break")) {
    inst(reg_0, new record_57({a: getLoopInfo(reg_0, reg_1.b, reg_1.c).c, b: 0, c: reg_1.c}));
    return;
  }
  if ((reg_1.a == "while")) {
    reg_103 = newLoopInfo(reg_0, reg_1.b);
    if ((reg_1.b.length > 0)) {
      inst(reg_0, new record_54({a: reg_1.b}));
    }
    reg_111 = {};
    reg_112 = reg_0.b;
    reg_117 = {a: reg_111, b: reg_112, c: reg_0, d: 0, e: reg_103};
    inst(reg_0, new record_54({a: reg_103.b}));
    reg_123 = compile_expr(reg_117, child(reg_1, 0));
    inst(reg_0, new record_53({a: reg_103.c, b: reg_123, c: 0, d: true}));
    compile_stmt(reg_117, child(reg_1, 1));
    inst(reg_0, new record_57({a: reg_103.b, b: 0, c: 0}));
    inst(reg_0, new record_54({a: reg_103.c}));
    return;
  }
  if ((reg_1.a == "if")) {
    if ((child(reg_1, 1).a == "goto")) {
      reg_149 = compile_expr(reg_0, child(reg_1, 0));
      inst(reg_0, new record_53({a: child(reg_1, 1).b, b: reg_149, c: 0, d: false}));
      return;
    }
    reg_157 = label(reg_0);
    inst(reg_0, new record_53({a: reg_157, b: compile_expr(reg_0, child(reg_1, 0)), c: 0, d: true}));
    compile_stmt(reg_0, child(reg_1, 1));
    goto_243 = !(len(reg_1) == 3);
    if (!goto_243) {
      reg_170 = label(reg_0);
      inst(reg_0, new record_57({a: reg_170, b: 0, c: 0}));
      inst(reg_0, new record_54({a: reg_157}));
      compile_stmt(reg_0, child(reg_1, 2));
      inst(reg_0, new record_54({a: reg_170}));
    }
    if ((goto_243 || false)) {
      goto_243 = false;
      inst(reg_0, new record_54({a: reg_157}));
    }
    return;
  }
  if ((reg_1.a == "call")) {
    compile_call(reg_0, reg_1, []);
    return;
  }
  if ((reg_1.a == "return")) {
    inst(reg_0, new record_58({a: compile_expr_list(reg_0, child(reg_1, 0)), b: reg_1}));
    return;
  }
  if ((reg_1.a == "goto")) {
    inst(reg_0, new record_57({a: reg_1.b, b: 0, c: reg_1.c}));
    return;
  }
  if ((reg_1.a == "label")) {
    inst(reg_0, new record_54({a: reg_1.b}));
    return;
  }
  throw new Error(("Unknown statement: " + reg_1.a));
  return;
}
function compile_function (reg_0, reg_1) {
  var reg_2, reg_7, reg_9, reg_10, reg_14, reg_17, reg_18;
  reg_2 = [];
  reg_7 = {a: {}, b: reg_2, c: null, d: 0, e: null};
  reg_9 = child(reg_1, 0);
  reg_10 = 0;
  loop_1: while ((reg_10 < len(reg_9))) {
    reg_14 = child(reg_9, reg_10).b;
    reg_17 = child(child(reg_9, reg_10), 0);
    reg_18 = newReg(reg_14);
    inst(reg_7, new record_44({a: reg_18, b: reg_17, c: false}));
    reg_7.a[reg_14] = reg_18;
    reg_10 = (reg_10 + 1);
  }
  compile_stmt(reg_7, reg_0);
  return reg_2;
}
var record_29 = function (val) { this.val = val; }
var record_59 = function (val) { this.val = val; }
function is_builtin_name (reg_0) {
  var reg_1, reg_2, reg_3, reg_4;
  reg_4 = (reg_0 == "string");
  if (!reg_4) {
    reg_4 = (reg_0 == "int");
  }
  reg_3 = reg_4;
  if (!reg_3) {
    reg_3 = (reg_0 == "bool");
  }
  reg_2 = reg_3;
  if (!reg_2) {
    reg_2 = (reg_0 == "itos");
  }
  reg_1 = reg_2;
  if (!reg_1) {
    reg_1 = (reg_0 == "println");
  }
  return reg_1;
}
function builtin_module (reg_0, reg_1, reg_2) {
  var reg_3, reg_5, reg_7;
  reg_3 = Module(reg_0);
  reg_3.b = "import";
  reg_3.c = reg_2;
  reg_5 = new record_23(reg_3);
  reg_0.i[reg_1] = reg_5;
  reg_7 = new record_23(reg_3);
  return reg_7;
}
function builtin_type (reg_0, reg_1, reg_2, reg_3) {
  var reg_7, reg_8, reg_10;
  reg_7 = Type(reg_0, reg_3, new record_23(get_builtin(reg_0, reg_2).val));
  reg_7.d = reg_1;
  reg_8 = new record_25(reg_7);
  reg_0.i[reg_1] = reg_8;
  reg_10 = new record_25(reg_7);
  return reg_10;
}
function builtin_function (reg_0, reg_1, reg_2, reg_3) {
  var reg_5, reg_6, reg_8;
  reg_5 = get_builtin(reg_0, reg_2).val;
  reg_6 = aulang_compiler$Function(reg_0);
  reg_6.c = new record_23(reg_5);
  reg_6.b = reg_3;
  reg_8 = new record_29(reg_6);
  reg_0.i[reg_1] = reg_8;
  return reg_6;
}
function get_builtin (reg_0, reg_1) {
  var reg_3, reg_6, reg_10, reg_14, reg_18, reg_22, reg_26, reg_31, reg_36, reg_41, reg_46, reg_51, reg_52, reg_55, reg_58, reg_63, reg_64, reg_67, reg_72, reg_73, reg_76, reg_81, reg_83, reg_86, reg_91, reg_93, reg_96, reg_97, reg_98, reg_99, reg_108, reg_122, reg_124, reg_128, reg_129, reg_130, reg_131, reg_132, reg_133, reg_146, reg_166, reg_168, reg_171, reg_174, reg_179, reg_180, reg_183, reg_188, reg_189, reg_192, reg_195, reg_200, reg_202, reg_206, reg_211, reg_213, reg_216, reg_219;
  reg_3 = reg_0.i[reg_1];
  if (!(reg_3 == null)) {
    reg_6 = reg_3;
    return reg_6;
  }
  if ((reg_1 == "system_module")) {
    reg_10 = builtin_module(reg_0, reg_1, "auro\x1fsystem");
    return reg_10;
  }
  if ((reg_1 == "string_module")) {
    reg_14 = builtin_module(reg_0, reg_1, "auro\x1fstring");
    return reg_14;
  }
  if ((reg_1 == "int_module")) {
    reg_18 = builtin_module(reg_0, reg_1, "auro\x1fint");
    return reg_18;
  }
  if ((reg_1 == "bool_module")) {
    reg_22 = builtin_module(reg_0, reg_1, "auro\x1fbool");
    return reg_22;
  }
  if ((reg_1 == "buffer_module")) {
    reg_26 = builtin_module(reg_0, reg_1, "auro\x1fbuffer");
    return reg_26;
  }
  if ((reg_1 == "string")) {
    reg_31 = builtin_type(reg_0, reg_1, "string_module", "string");
    return reg_31;
  }
  if ((reg_1 == "int")) {
    reg_36 = builtin_type(reg_0, reg_1, "int_module", "int");
    return reg_36;
  }
  if ((reg_1 == "bool")) {
    reg_41 = builtin_type(reg_0, reg_1, "bool_module", "bool");
    return reg_41;
  }
  if ((reg_1 == "buffer")) {
    reg_46 = builtin_type(reg_0, reg_1, "buffer_module", "buffer");
    return reg_46;
  }
  if ((reg_1 == "new_string")) {
    reg_51 = builtin_function(reg_0, reg_1, "string_module", "new");
    reg_52 = reg_51.d;
    reg_52.push(get_builtin(reg_0, "buffer"));
    reg_55 = reg_51.e;
    reg_55.push(get_builtin(reg_0, "string"));
    reg_58 = new record_29(reg_51);
    return reg_58;
  }
  if ((reg_1 == "true")) {
    reg_63 = builtin_function(reg_0, reg_1, "bool_module", "true");
    reg_64 = reg_63.e;
    reg_64.push(get_builtin(reg_0, "bool"));
    reg_67 = new record_29(reg_63);
    return reg_67;
  }
  if ((reg_1 == "false")) {
    reg_72 = builtin_function(reg_0, reg_1, "bool_module", "false");
    reg_73 = reg_72.e;
    reg_73.push(get_builtin(reg_0, "bool"));
    reg_76 = new record_29(reg_72);
    return reg_76;
  }
  if ((reg_1 == "not")) {
    reg_81 = builtin_function(reg_0, reg_1, "bool_module", "not");
    reg_83 = get_builtin(reg_0, "bool");
    reg_81.d.push(reg_83);
    reg_81.e.push(reg_83);
    reg_86 = new record_29(reg_81);
    return reg_86;
  }
  if ((reg_1 == "ineg")) {
    reg_91 = builtin_function(reg_0, reg_1, "int_module", "neg");
    reg_93 = get_builtin(reg_0, "int");
    reg_91.d.push(reg_93);
    reg_91.e.push(reg_93);
    reg_96 = new record_29(reg_91);
    return reg_96;
  }
  reg_99 = (reg_1 == "iadd");
  if (!reg_99) {
    reg_99 = (reg_1 == "isub");
  }
  reg_98 = reg_99;
  if (!reg_98) {
    reg_98 = (reg_1 == "imul");
  }
  reg_97 = reg_98;
  if (!reg_97) {
    reg_97 = (reg_1 == "idiv");
  }
  if (reg_97) {
    if ((reg_1 == "iadd")) {
      reg_108 = "add";
    }
    if ((reg_1 == "isub")) {
      reg_108 = "sub";
    }
    if ((reg_1 == "imul")) {
      reg_108 = "mul";
    }
    if ((reg_1 == "idiv")) {
      reg_108 = "div";
    }
    reg_122 = builtin_function(reg_0, reg_1, "int_module", reg_108);
    reg_124 = get_builtin(reg_0, "int");
    reg_122.d.push(reg_124);
    reg_122.d.push(reg_124);
    reg_122.e.push(reg_124);
    reg_128 = new record_29(reg_122);
    return reg_128;
  }
  reg_133 = (reg_1 == "ieq");
  if (!reg_133) {
    reg_133 = (reg_1 == "ine");
  }
  reg_132 = reg_133;
  if (!reg_132) {
    reg_132 = (reg_1 == "ilt");
  }
  reg_131 = reg_132;
  if (!reg_131) {
    reg_131 = (reg_1 == "igt");
  }
  reg_130 = reg_131;
  if (!reg_130) {
    reg_130 = (reg_1 == "ile");
  }
  reg_129 = reg_130;
  if (!reg_129) {
    reg_129 = (reg_1 == "ige");
  }
  if (reg_129) {
    if ((reg_1 == "ieq")) {
      reg_146 = "eq";
    }
    if ((reg_1 == "ine")) {
      reg_146 = "ne";
    }
    if ((reg_1 == "ilt")) {
      reg_146 = "lt";
    }
    if ((reg_1 == "igt")) {
      reg_146 = "gt";
    }
    if ((reg_1 == "ile")) {
      reg_146 = "le";
    }
    if ((reg_1 == "ige")) {
      reg_146 = "ge";
    }
    reg_166 = builtin_function(reg_0, reg_1, "int_module", reg_146);
    reg_168 = get_builtin(reg_0, "int");
    reg_166.d.push(reg_168);
    reg_166.d.push(reg_168);
    reg_171 = reg_166.e;
    reg_171.push(get_builtin(reg_0, "bool"));
    reg_174 = new record_29(reg_166);
    return reg_174;
  }
  if ((reg_1 == "println")) {
    reg_179 = builtin_function(reg_0, reg_1, "system_module", "println");
    reg_180 = reg_179.d;
    reg_180.push(get_builtin(reg_0, "string"));
    reg_183 = new record_29(reg_179);
    return reg_183;
  }
  if ((reg_1 == "itos")) {
    reg_188 = builtin_function(reg_0, reg_1, "string_module", "itos");
    reg_189 = reg_188.d;
    reg_189.push(get_builtin(reg_0, "int"));
    reg_192 = reg_188.e;
    reg_192.push(get_builtin(reg_0, "string"));
    reg_195 = new record_29(reg_188);
    return reg_195;
  }
  if ((reg_1 == "concat")) {
    reg_200 = builtin_function(reg_0, reg_1, "string_module", "concat");
    reg_202 = get_builtin(reg_0, "string");
    reg_200.d.push(reg_202);
    reg_200.d.push(reg_202);
    reg_200.e.push(reg_202);
    reg_206 = new record_29(reg_200);
    return reg_206;
  }
  if ((reg_1 == "streq")) {
    reg_211 = builtin_function(reg_0, reg_1, "string_module", "eq");
    reg_213 = get_builtin(reg_0, "string");
    reg_211.d.push(reg_213);
    reg_211.d.push(reg_213);
    reg_216 = reg_211.e;
    reg_216.push(get_builtin(reg_0, "bool"));
    reg_219 = new record_29(reg_211);
    return reg_219;
  }
  throw new Error((reg_1 + " is not a builtin item"));
}
function getFromNode (reg_0, reg_1) {
  var reg_5, reg_15, reg_16, reg_23, reg_25, reg_32, reg_33;
  if ((reg_1.a == "field")) {
    reg_5 = "module";
    reg_15 = new record_59({a: getItem(reg_0, new record_33(aulang_item$_new(reg_5, child(reg_1, 0)))).val, b: reg_1.b});
    return reg_15;
    if (true) { goto(52); };
  }
  reg_16 = (reg_1.a == "item");
  if (!reg_16) {
    reg_16 = (reg_1.a == "var");
  }
  if (reg_16) {
    reg_23 = reg_1.b;
    reg_25 = reg_0.c[reg_23];
    if (!(reg_25 == null)) {
      return reg_25;
    }
    if (is_builtin_name(reg_1.b)) {
      reg_32 = get_builtin(reg_0, reg_1.b);
      return reg_32;
    }
    reg_33 = null;
    return reg_33;
    if (true) { goto(52); };
  }
  error(reg_0, ("Unsupported item " + reg_1.a), reg_1.c);
}
function to_str (reg_0) {
  var reg_3, reg_6;
  reg_3 = (reg_0.a + " ");
  reg_6 = (reg_3 + to_string(reg_0.b));
  return reg_6;
}
function getItem (reg_0, reg_1) {
  var reg_3, reg_7, reg_9, reg_13, reg_17, reg_28, reg_32, reg_39, reg_42, reg_49, reg_52, reg_63;
  var goto_52=false;
  if ((reg_1 instanceof record_33)) {
    reg_3 = reg_1.val;
    if (!(typeof reg_3.c === 'boolean')) {
      reg_7 = reg_3.c;
      return reg_7;
    }
    reg_9 = getFromNode(reg_0, reg_3.b);
    if ((reg_9 == null)) {
      reg_13 = (to_str(reg_3) + " not found");
      error(reg_0, reg_13, reg_3.b.c);
    }
    reg_1 = reg_9;
    reg_17 = (reg_3.b.a == "item");
    if (reg_17) {
      reg_17 = (reg_3.b.b == "string");
    }
    if (reg_17) {
      reg_26 = reg_1.val;
    }
    if ((reg_1 instanceof record_59)) {
      reg_28 = reg_1.val;
      goto_52 = !(reg_3.a == "type");
      if (!goto_52) {
        reg_32 = reg_28.b;
        reg_1 = new record_25(Type(reg_0, reg_32, new record_23(reg_28.a)));
      }
      if ((goto_52 || false)) {
        goto_52 = false;
        reg_39 = ("Cannot import " + to_str(reg_3));
        error(reg_0, reg_39, reg_3.b.c);
      }
    }
    reg_3.c = reg_1;
    reg_42 = "unknown";
    if ((reg_1 instanceof record_23)) {
      reg_42 = "module";
    }
    if ((reg_1 instanceof record_25)) {
      reg_42 = "type";
    }
    if ((reg_1 instanceof record_29)) {
      reg_42 = "function";
    }
    reg_49 = (reg_3.a == reg_42);
    if (!reg_49) {
      reg_52 = (reg_3.a == "item");
      if (reg_52) {
        reg_52 = !(reg_42 == "unknown");
      }
      reg_49 = reg_52;
    }
    if (reg_49) {
      return reg_1;
    }
    reg_63 = ((to_string(reg_3.b) + " is a ") + reg_42);
    error(reg_0, reg_63, reg_3.b.c);
  }
  return reg_1;
}
function voidRetInst (reg_0) {
  var reg_2;
  reg_2 = {a: [], b: reg_0};
  return reg_2;
}
function ins_len (reg_0) {
  var reg_2;
  reg_2 = reg_0.b.length;
  return reg_2;
}
function outs_len (reg_0) {
  var reg_2;
  reg_2 = reg_0.c.length;
  return reg_2;
}
function out (reg_0, reg_1) {
  var reg_3;
  reg_3 = reg_0.c[reg_1];
  return reg_3;
}
function vals_len (reg_0) {
  var reg_2;
  reg_2 = reg_0.a.length;
  return reg_2;
}
function Const (reg_0, reg_1, reg_2) {
  var reg_5;
  reg_5 = {a: reg_0.h.length, b: reg_1, c: reg_2};
  reg_0.h.push(reg_5);
  return reg_5;
}
var record_31 = function (val) { this.val = val; }
var ArrayList_any = function (val) { this.val = val; }
function _new_call (reg_0, reg_1) {
  var reg_4;
  reg_4 = {a: reg_0, b: [], c: [], d: reg_1};
  return reg_4;
}
function outs_push (reg_0, reg_1) {
  reg_0.c.push(reg_1);
  return;
}
function ins_push (reg_0, reg_1) {
  reg_0.b.push(reg_1);
  return;
}
function transform_instructions (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_6, reg_11, reg_24, reg_27, reg_29, reg_31, reg_34, reg_36, reg_43, reg_49, reg_54, reg_57, reg_63, reg_69, reg_74, reg_77, reg_80, reg_84, reg_92, reg_98, reg_104, reg_108, reg_114, reg_119, reg_125, reg_137, reg_140, reg_145, reg_150, reg_153, reg_154, reg_167, reg_168, reg_180, reg_191, reg_196, reg_205, reg_206, reg_217, reg_222, reg_225, reg_229, reg_231, reg_237, reg_241, reg_245, reg_246, reg_247, reg_264, reg_268, reg_317, reg_321, reg_326, reg_329, reg_334, reg_336, reg_342, reg_349, reg_352, reg_354, reg_355, reg_356, reg_364;
  var goto_222=false, goto_243=false, goto_255=false, goto_270=false, goto_294=false, goto_301=false, goto_360=false, goto_367=false, goto_375=false, goto_393=false, goto_400=false, goto_407=false, goto_414=false, goto_421=false, goto_428=false, goto_435=false, goto_442=false, goto_449=false, goto_456=false, goto_464=false;
  reg_3 = reg_1.f;
  reg_4 = 0;
  reg_5 = {};
  reg_6 = (reg_1.e.length == 0);
  if (reg_6) {
    reg_11 = (reg_3.length == 0);
    if (!reg_11) {
      reg_11 = !(reg_3[(reg_3.length - 1)] instanceof record_58);
    }
    reg_6 = reg_11;
  }
  if (reg_6) {
    reg_3.push(new record_58(voidRetInst(reg_0.b)));
  }
  reg_24 = 0;
  loop_2: while ((reg_24 < reg_3.length)) {
    reg_27 = reg_3[reg_24];
    if ((reg_27 instanceof record_47)) {
      reg_29 = reg_27.val;
      reg_31 = reg_29.a.val;
      reg_34 = getItem(reg_0, new record_33(reg_31)).val;
      reg_29.a = new record_29(reg_34);
      reg_36 = ins_len(reg_29);
      if (!(reg_36 == reg_34.d.length)) {
        reg_43 = (to_str(reg_31) + " expects ");
        reg_49 = ((reg_43 + String(reg_34.d.length)) + " arguments, but ");
        reg_54 = ((reg_49 + String(ins_len(reg_29))) + " were given");
        error(reg_0, reg_54, reg_29.d.c);
      }
      reg_57 = outs_len(reg_29);
      if ((reg_57 > reg_34.e.length)) {
        reg_63 = (to_str(reg_31) + " returns ");
        reg_69 = ((reg_63 + String(reg_34.e.length)) + " arguments, but ");
        reg_74 = ((reg_69 + String(outs_len(reg_29))) + " were expected");
        error(reg_0, reg_74, reg_29.d.c);
      }
      reg_77 = 0;
      loop_3: while ((reg_77 < outs_len(reg_29))) {
        reg_80 = (reg_4 + reg_77);
        out(reg_29, reg_77).a = reg_80;
        reg_84 = getItem(reg_0, reg_34.e[reg_77]);
        out(reg_29, reg_77).c = reg_84;
        reg_77 = (reg_77 + 1);
      }
      reg_4 = (reg_4 + reg_34.e.length);
    }
    if ((reg_27 instanceof record_58)) {
      reg_92 = reg_27.val;
      if (!(reg_1.e.length == vals_len(reg_92))) {
        reg_98 = "Function returns ";
        reg_104 = ((reg_98 + String(reg_1.e.length)) + " values");
        error(reg_0, reg_104, reg_92.b.c);
      }
    }
    if ((reg_27 instanceof record_56)) {
      reg_108 = reg_27.val;
      reg_108.a.a = reg_4;
      reg_114 = getItem(reg_0, new record_33(aulang_item$_new("type", reg_108.b)));
      reg_108.a.c = reg_114;
      reg_4 = (reg_4 + 1);
    }
    if ((reg_27 instanceof record_44)) {
      reg_119 = reg_27.val;
      reg_119.a.a = reg_4;
      reg_125 = getItem(reg_0, new record_33(aulang_item$_new("type", reg_119.b)));
      reg_119.a.c = reg_125;
      reg_4 = (reg_4 + 1);
      reg_3.splice(reg_24, 1);
      reg_24 = (reg_24 - 1);
    }
    if ((reg_27 instanceof record_51)) {
      reg_27.val.a.a = reg_4;
      reg_4 = (reg_4 + 1);
    }
    if ((reg_27 instanceof record_55)) {
      reg_137 = reg_27.val;
      reg_137.b.a = reg_4;
      reg_140 = reg_137.a.c;
      reg_137.b.c = reg_140;
      reg_4 = (reg_4 + 1);
    }
    if ((reg_27 instanceof record_52)) {
      reg_145 = reg_27.val;
      if ((typeof reg_145.b.c === 'boolean')) {
        reg_150 = reg_145.a.c;
        reg_145.b.c = reg_150;
      }
    }
    if ((reg_27 instanceof record_48)) {
      reg_153 = reg_27.val;
      goto_222 = !(reg_153.b instanceof Integer);
      if (!goto_222) {
        reg_153.b = new record_31(Const(reg_0, "int", reg_153.b));
        reg_154 = get_builtin(reg_0, "int");
      }
      if ((goto_222 || false)) {
        goto_222 = false;
        goto_243 = !(typeof reg_153.b === 'string');
        if (!goto_243) {
          reg_167 = Const(reg_0, "bin", reg_153.b);
          reg_168 = [];
          reg_168.push(get_builtin(reg_0, "new_string"));
          reg_168.push(new record_31(reg_167));
          reg_153.b = new record_31(Const(reg_0, "call", new ArrayList_any(reg_168)));
          reg_154 = get_builtin(reg_0, "string");
        }
        if ((goto_243 || false)) {
          goto_243 = false;
          goto_270 = !(typeof reg_153.b === 'boolean');
          if (!goto_270) {
            goto_255 = !reg_153.b;
            if (!goto_255) {
              reg_180 = get_builtin(reg_0, "true").val;
            }
            if ((goto_255 || false)) {
              goto_255 = false;
              reg_180 = get_builtin(reg_0, "false").val;
            }
            reg_191 = _new_call(new record_29(reg_180), reg_0.b);
            outs_push(reg_191, reg_153.a);
            reg_3[reg_24]=new record_47(reg_191);
            reg_154 = get_builtin(reg_0, "bool");
          }
          if ((goto_270 || false)) {
            goto_270 = false;
            reg_196 = "Unknown constant value type";
            error(reg_0, reg_196, (0 - 1));
          }
        }
      }
      reg_153.a.a = reg_4;
      reg_153.a.c = reg_154;
      reg_4 = (reg_4 + 1);
    }
    if ((reg_27 instanceof record_49)) {
      reg_205 = reg_27.val;
      goto_294 = !(reg_205.a == "-");
      if (!goto_294) {
        reg_206 = "ineg";
      }
      if ((goto_294 || false)) {
        goto_294 = false;
        goto_301 = !(reg_205.a == "!");
        if (!goto_301) {
          reg_206 = "not";
        }
        if ((goto_301 || false)) {
          goto_301 = false;
          reg_217 = ("Unsupported unary operator " + reg_205.a);
          error(reg_0, reg_217, (0 - 1));
        }
      }
      reg_222 = get_builtin(reg_0, reg_206).val;
      reg_225 = _new_call(new record_29(reg_222), reg_0.b);
      ins_push(reg_225, reg_205.b);
      outs_push(reg_225, reg_205.c);
      reg_205.c.a = reg_4;
      reg_229 = 0;
      reg_231 = reg_222.e[reg_229];
      reg_205.c.c = reg_231;
      reg_3[reg_24]=new record_47(reg_225);
      reg_4 = (reg_4 + 1);
    }
    if ((reg_27 instanceof record_50)) {
      reg_237 = reg_27.val;
      reg_241 = getItem(reg_0, reg_237.b.c).val;
      reg_245 = getItem(reg_0, reg_237.c.c).val;
      reg_247 = (reg_241.d == "string");
      if (reg_247) {
        reg_247 = (reg_245.d == "string");
      }
      goto_375 = !reg_247;
      if (!goto_375) {
        goto_360 = !(reg_237.a == "+");
        if (!goto_360) {
          reg_246 = "concat";
        }
        if ((goto_360 || false)) {
          goto_360 = false;
          goto_367 = !(reg_237.a == "==");
          if (!goto_367) {
            reg_246 = "streq";
          }
          if ((goto_367 || false)) {
            goto_367 = false;
            reg_264 = ("Unsupported string operator " + reg_237.a);
            error(reg_0, reg_264, (0 - 1));
          }
        }
      }
      if ((goto_375 || false)) {
        goto_375 = false;
        reg_268 = (reg_241.d == "int");
        if (reg_268) {
          reg_268 = (reg_245.d == "int");
        }
        goto_464 = !reg_268;
        if (!goto_464) {
          goto_393 = !(reg_237.a == "+");
          if (!goto_393) {
            reg_246 = "iadd";
          }
          if ((goto_393 || false)) {
            goto_393 = false;
            goto_400 = !(reg_237.a == "-");
            if (!goto_400) {
              reg_246 = "isub";
            }
            if ((goto_400 || false)) {
              goto_400 = false;
              goto_407 = !(reg_237.a == "*");
              if (!goto_407) {
                reg_246 = "imul";
              }
              if ((goto_407 || false)) {
                goto_407 = false;
                goto_414 = !(reg_237.a == "/");
                if (!goto_414) {
                  reg_246 = "idiv";
                }
                if ((goto_414 || false)) {
                  goto_414 = false;
                  goto_421 = !(reg_237.a == "<");
                  if (!goto_421) {
                    reg_246 = "ilt";
                  }
                  if ((goto_421 || false)) {
                    goto_421 = false;
                    goto_428 = !(reg_237.a == ">");
                    if (!goto_428) {
                      reg_246 = "igt";
                    }
                    if ((goto_428 || false)) {
                      goto_428 = false;
                      goto_435 = !(reg_237.a == "<=");
                      if (!goto_435) {
                        reg_246 = "ile";
                      }
                      if ((goto_435 || false)) {
                        goto_435 = false;
                        goto_442 = !(reg_237.a == ">=");
                        if (!goto_442) {
                          reg_246 = "ige";
                        }
                        if ((goto_442 || false)) {
                          goto_442 = false;
                          goto_449 = !(reg_237.a == "==");
                          if (!goto_449) {
                            reg_246 = "ieq";
                          }
                          if ((goto_449 || false)) {
                            goto_449 = false;
                            goto_456 = !(reg_237.a == "!=");
                            if (!goto_456) {
                              reg_246 = "ine";
                            }
                            if ((goto_456 || false)) {
                              goto_456 = false;
                              reg_317 = ("Unsupported int operator " + reg_237.a);
                              error(reg_0, reg_317, (0 - 1));
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
        if ((goto_464 || false)) {
          goto_464 = false;
          reg_321 = "Cannot operate these types";
          error(reg_0, reg_321, (0 - 1));
        }
      }
      reg_326 = get_builtin(reg_0, reg_246).val;
      reg_329 = _new_call(new record_29(reg_326), reg_0.b);
      ins_push(reg_329, reg_237.b);
      ins_push(reg_329, reg_237.c);
      outs_push(reg_329, reg_237.d);
      reg_237.d.a = reg_4;
      reg_334 = 0;
      reg_336 = reg_326.e[reg_334];
      reg_237.d.c = reg_336;
      reg_3[reg_24]=new record_47(reg_329);
      reg_4 = (reg_4 + 1);
    }
    if ((reg_27 instanceof record_54)) {
      reg_342 = new Integer(reg_24);
      reg_5[reg_27.val.a] = reg_342;
      reg_3.splice(reg_24, 1);
      reg_24 = (reg_24 - 1);
    }
    reg_24 = (reg_24 + 1);
  }
  reg_349 = 0;
  loop_1: while ((reg_349 < reg_3.length)) {
    reg_352 = reg_3[reg_349];
    if ((reg_352 instanceof record_57)) {
      reg_354 = reg_352.val;
      reg_355 = reg_354.a;
      reg_356 = reg_5[reg_355];
      if ((reg_356 == null)) {
        error(reg_0, ("Unknown label " + reg_355), reg_354.c);
      }
      reg_354.b = reg_356.val;
    }
    if ((reg_352 instanceof record_53)) {
      reg_364 = reg_352.val;
      reg_364.c = reg_5[reg_364.a].val;
    }
    reg_349 = (reg_349 + 1);
  }
  return;
}
Auro.str_tobuf = function (str) {
  bytes = []
  for (var i = 0; i < str.length; i++) {
    var c = str.charCodeAt(i)
    if (c >= 0xD800 && c <= 0xDFFF) {
      c = (c - 0xD800 << 10 | str.charCodeAt(++i) - 0xDC00) + 0x10000
    }
    if (c < 0x80) {
      bytes.push(c)
    } else if (c < 0x800) {
      bytes.push(c >> 0x6 | 0xC0, c & 0x3F | 0x80)
    } else if (c < 0x10000) {
      bytes.push(c >> 0xC | 0xE0, c >> 0x6 & 0x3F | 0x80, c & 0x3F | 0x80)
    } else {
      bytes.push(c >> 0x12 | 0xF0, c >> 0xC & 0x3F | 0x80, c >> 0x6 & 0x3F | 0x80, c & 0x3F | 0x80)
    }
  }
  return Uint8Array.from(bytes)
}
function aulang_writer$byte (reg_0, reg_1) {
  var reg_2, reg_6;
  reg_2 = reg_0.b;
  if ((reg_2.b >= cns1)) {
    reg_6 = newPart();
    reg_2.c = reg_6;
    reg_0.b = reg_6;
    reg_0.c = (reg_0.c + 1);
    reg_2 = reg_6;
  }
  reg_2.a[reg_2.b]=reg_1;
  reg_2.b = (reg_2.b + 1);
  return;
}
function rawstr (reg_0, reg_1) {
  var reg_2, reg_3;
  reg_2 = Auro.str_tobuf(reg_1);
  reg_3 = 0;
  loop_1: while ((reg_3 < reg_2.length)) {
    aulang_writer$byte(reg_0, reg_2[reg_3]);
    reg_3 = (reg_3 + 1);
  }
  return;
}
function _num (reg_0, reg_1) {
  if ((reg_1 > 127)) {
    _num(reg_0, (reg_1 >> 7));
  }
  aulang_writer$byte(reg_0, ((reg_1 & 127) | 128));
  return;
}
function num (reg_0, reg_1) {
  if ((reg_1 > 127)) {
    _num(reg_0, (reg_1 >> 7));
  }
  aulang_writer$byte(reg_0, (reg_1 & 127));
  return;
}
function str (reg_0, reg_1) {
  num(reg_0, reg_1.length);
  rawstr(reg_0, reg_1);
  return;
}
function writeModules (reg_0) {
  var reg_1, reg_2, reg_4, reg_7, reg_21, reg_26, reg_28, reg_29, reg_30, reg_62, reg_65;
  var goto_21=false, goto_49=false, goto_57=false, goto_65=false, goto_76=false, goto_90=false, goto_107=false;
  reg_1 = reg_0.d;
  reg_2 = reg_0.e;
  num(reg_1, reg_2.length);
  reg_4 = 0;
  loop_1: while ((reg_4 < reg_2.length)) {
    reg_7 = reg_2[reg_4];
    reg_4 = (reg_4 + 1);
    goto_21 = !(reg_7.b == "import");
    if (!goto_21) {
      aulang_writer$byte(reg_1, 1);
      str(reg_1, reg_7.c);
    }
    if ((goto_21 || false)) {
      goto_21 = false;
      goto_76 = !(reg_7.b == "define");
      if (!goto_76) {
        aulang_writer$byte(reg_1, 2);
        num(reg_1, reg_7.f.length);
        reg_21 = 0;
        loop_2: while ((reg_21 < reg_7.f.length)) {
          reg_26 = reg_7.f[reg_21];
          reg_28 = getItem(reg_0, reg_26.b);
          goto_49 = !(reg_28 instanceof record_23);
          if (!goto_49) {
            reg_29 = 0;
            reg_30 = reg_28.val.a;
          }
          if ((goto_49 || false)) {
            goto_49 = false;
            goto_57 = !(reg_28 instanceof record_25);
            if (!goto_57) {
              reg_29 = 1;
              reg_30 = reg_28.val.a;
            }
            if ((goto_57 || false)) {
              goto_57 = false;
              goto_65 = !(reg_28 instanceof record_29);
              if (!goto_65) {
                reg_29 = 2;
                reg_30 = reg_28.val.a;
              }
              if ((goto_65 || false)) {
                goto_65 = false;
                throw new Error("???");
              }
            }
          }
          aulang_writer$byte(reg_1, reg_29);
          num(reg_1, reg_30);
          str(reg_1, reg_26.a);
          reg_21 = (reg_21 + 1);
        }
      }
      if ((goto_76 || false)) {
        goto_76 = false;
        goto_90 = !(reg_7.b == "use");
        if (!goto_90) {
          aulang_writer$byte(reg_1, 3);
          num(reg_1, getItem(reg_0, reg_7.d).val.a);
          str(reg_1, reg_7.c);
        }
        if ((goto_90 || false)) {
          goto_90 = false;
          goto_107 = !(reg_7.b == "build");
          if (!goto_107) {
            aulang_writer$byte(reg_1, 4);
            reg_62 = getItem(reg_0, reg_7.d).val;
            reg_65 = getItem(reg_0, reg_7.e).val;
            num(reg_1, reg_62.a);
            num(reg_1, reg_65.a);
          }
          if ((goto_107 || false)) {
            goto_107 = false;
            throw new Error("What am i doing?");
          }
        }
      }
    }
  }
  return;
}
function writeTypes (reg_0) {
  var reg_1, reg_4, reg_9;
  reg_1 = reg_0.d;
  num(reg_1, reg_0.f.length);
  reg_4 = 0;
  loop_1: while ((reg_4 < reg_0.f.length)) {
    reg_9 = reg_0.f[reg_4];
    reg_4 = (reg_4 + 1);
    num(reg_1, (getItem(reg_0, reg_9.c).val.a + 1));
    str(reg_1, reg_9.b);
  }
  return;
}
function writeFunctions (reg_0) {
  var reg_1, reg_5, reg_6, reg_11, reg_23, reg_25, reg_34, reg_36;
  var goto_27=false, goto_67=false;
  reg_1 = reg_0.d;
  num(reg_1, reg_0.g.length);
  reg_4 = 0;
  reg_5 = [];
  reg_6 = 0;
  loop_1: while ((reg_6 < reg_0.g.length)) {
    reg_11 = reg_0.g[reg_6];
    reg_6 = (reg_6 + 1);
    goto_27 = !(reg_11.f == null);
    if (!goto_27) {
      num(reg_1, (getItem(reg_0, reg_11.c).val.a + 2));
    }
    if ((goto_27 || false)) {
      goto_27 = false;
      aulang_writer$byte(reg_1, 1);
    }
    reg_23 = 0;
    reg_25 = reg_11.d.length;
    num(reg_1, reg_25);
    loop_3: while ((reg_23 < reg_25)) {
      num(reg_1, getItem(reg_0, reg_11.d[reg_23]).val.a);
      reg_23 = (reg_23 + 1);
    }
    reg_34 = 0;
    reg_36 = reg_11.e.length;
    num(reg_1, reg_36);
    loop_2: while ((reg_34 < reg_36)) {
      num(reg_1, getItem(reg_0, reg_11.e[reg_34]).val.a);
      reg_34 = (reg_34 + 1);
    }
    goto_67 = !(reg_11.f == null);
    if (!goto_67) {
      str(reg_1, reg_11.b);
    }
    if ((goto_67 || false)) {
      goto_67 = false;
      reg_5.push(reg_11);
    }
  }
  return;
}
function writeConstants (reg_0) {
  var reg_1, reg_4, reg_9, reg_22, reg_29, reg_33, reg_39, reg_45, reg_50, reg_58;
  var goto_24=false, goto_36=false, goto_77=false;
  reg_1 = reg_0.d;
  num(reg_1, reg_0.h.length);
  reg_4 = 0;
  loop_1: while ((reg_4 < reg_0.h.length)) {
    reg_9 = reg_0.h[reg_4];
    reg_4 = (reg_4 + 1);
    goto_24 = !(reg_9.b == "int");
    if (!goto_24) {
      aulang_writer$byte(reg_1, 1);
      num(reg_1, reg_9.c.val);
    }
    if ((goto_24 || false)) {
      goto_24 = false;
      goto_36 = !(reg_9.b == "bin");
      if (!goto_36) {
        reg_22 = reg_9.c;
        aulang_writer$byte(reg_1, 2);
        num(reg_1, reg_22.length);
        rawstr(reg_1, reg_22);
      }
      if ((goto_36 || false)) {
        goto_36 = false;
        goto_77 = !(reg_9.b == "call");
        if (!goto_77) {
          reg_29 = reg_9.c.val;
          reg_33 = getItem(reg_0, reg_29[0]).val;
          aulang_writer$byte(reg_1, (reg_33.a + 16));
          reg_39 = (reg_29.length - 1);
          if (!(reg_39 == reg_33.d.length)) {
            throw new Error("Argument mismatch");
          }
          reg_45 = 1;
          loop_2: while ((reg_45 < reg_29.length)) {
            reg_50 = reg_29[reg_45].val.a;
            num(reg_1, (reg_50 + reg_0.g.length));
            reg_45 = (reg_45 + 1);
          }
        }
        if ((goto_77 || false)) {
          goto_77 = false;
          reg_58 = ("Unknown Constant kind " + reg_9.b);
          error(reg_0, reg_58, (0 - 1));
        }
      }
    }
  }
  return;
}
function aulang_codegen$in (reg_0, reg_1) {
  var reg_3;
  reg_3 = reg_0.b[reg_1];
  return reg_3;
}
function val (reg_0, reg_1) {
  var reg_3;
  reg_3 = reg_0.a[reg_1];
  return reg_3;
}
function inst_name (reg_0) {
  var reg_2, reg_4, reg_6, reg_8, reg_10, reg_12, reg_14, reg_16, reg_18, reg_20, reg_22, reg_24, reg_26;
  if ((reg_0 instanceof record_47)) {
    reg_2 = "CallInst";
    return reg_2;
  }
  if ((reg_0 instanceof record_58)) {
    reg_4 = "RetInst";
    return reg_4;
  }
  if ((reg_0 instanceof record_55)) {
    reg_6 = "DupInst";
    return reg_6;
  }
  if ((reg_0 instanceof record_52)) {
    reg_8 = "SetInst";
    return reg_8;
  }
  if ((reg_0 instanceof record_51)) {
    reg_10 = "VarInst";
    return reg_10;
  }
  if ((reg_0 instanceof record_56)) {
    reg_12 = "DeclInst";
    return reg_12;
  }
  if ((reg_0 instanceof record_44)) {
    reg_14 = "ArgInst";
    return reg_14;
  }
  if ((reg_0 instanceof record_57)) {
    reg_16 = "JmpInst";
    return reg_16;
  }
  if ((reg_0 instanceof record_53)) {
    reg_18 = "JifInst";
    return reg_18;
  }
  if ((reg_0 instanceof record_54)) {
    reg_20 = "LblInst";
    return reg_20;
  }
  if ((reg_0 instanceof record_48)) {
    reg_22 = "ConstInst";
    return reg_22;
  }
  if ((reg_0 instanceof record_49)) {
    reg_24 = "UnopInst";
    return reg_24;
  }
  if ((reg_0 instanceof record_50)) {
    reg_26 = "BinopInst";
    return reg_26;
  }
  reg_27 = reg_0;
  throw new Error("Unknown instruction type");
}
function writeCode (reg_0) {
  var reg_1, reg_2, reg_7, reg_11, reg_13, reg_16, reg_18, reg_24, reg_35, reg_42, reg_44, reg_51, reg_61, reg_72;
  var goto_13=false, goto_43=false, goto_56=false, goto_73=false, goto_83=false, goto_92=false, goto_104=false, goto_112=false, goto_120=false, goto_128=false;
  reg_1 = reg_0.d;
  reg_2 = 0;
  loop_1: while ((reg_2 < reg_0.g.length)) {
    reg_7 = reg_0.g[reg_2];
    goto_13 = !(reg_7.f == null);
    if (!goto_13) {
    }
    if ((goto_13 || false)) {
      if (!goto_13) {
      }
      goto_13 = false;
      reg_11 = reg_7.f;
      num(reg_1, reg_11.length);
      reg_13 = 0;
      loop_2: while ((reg_13 < reg_11.length)) {
        reg_16 = reg_11[reg_13];
        goto_43 = !(reg_16 instanceof record_47);
        if (!goto_43) {
          reg_18 = reg_16.val;
          num(reg_1, (reg_18.a.val.a + 16));
          reg_24 = 0;
          loop_4: while ((reg_24 < ins_len(reg_18))) {
            num(reg_1, aulang_codegen$in(reg_18, reg_24).a);
            reg_24 = (reg_24 + 1);
          }
        }
        if ((goto_43 || false)) {
          goto_43 = false;
          goto_56 = !(reg_16 instanceof record_48);
          if (!goto_56) {
            reg_35 = reg_16.val.b.val.a;
            num(reg_1, ((reg_35 + reg_0.g.length) + 16));
          }
          if ((goto_56 || false)) {
            goto_56 = false;
            goto_73 = !(reg_16 instanceof record_58);
            if (!goto_73) {
              reg_42 = reg_16.val;
              aulang_writer$byte(reg_1, 0);
              reg_44 = 0;
              loop_3: while ((reg_44 < vals_len(reg_42))) {
                num(reg_1, val(reg_42, reg_44).a);
                reg_44 = (reg_44 + 1);
              }
            }
            if ((goto_73 || false)) {
              goto_73 = false;
              reg_51 = (reg_16 instanceof record_51);
              if (!reg_51) {
                reg_51 = (reg_16 instanceof record_56);
              }
              goto_83 = !reg_51;
              if (!goto_83) {
                aulang_writer$byte(reg_1, 2);
              }
              if ((goto_83 || false)) {
                goto_83 = false;
                goto_92 = !(reg_16 instanceof record_55);
                if (!goto_92) {
                  aulang_writer$byte(reg_1, 3);
                  num(reg_1, reg_16.val.a.a);
                }
                if ((goto_92 || false)) {
                  goto_92 = false;
                  goto_104 = !(reg_16 instanceof record_52);
                  if (!goto_104) {
                    reg_61 = reg_16.val;
                    aulang_writer$byte(reg_1, 4);
                    num(reg_1, reg_61.b.a);
                    num(reg_1, reg_61.a.a);
                  }
                  if ((goto_104 || false)) {
                    goto_104 = false;
                    goto_112 = !(reg_16 instanceof record_57);
                    if (!goto_112) {
                      aulang_writer$byte(reg_1, 5);
                      num(reg_1, reg_16.val.b);
                    }
                    if ((goto_112 || false)) {
                      goto_112 = false;
                      goto_128 = !(reg_16 instanceof record_53);
                      if (!goto_128) {
                        reg_72 = reg_16.val;
                        goto_120 = !reg_72.d;
                        if (!goto_120) {
                          aulang_writer$byte(reg_1, 7);
                        }
                        if ((goto_120 || false)) {
                          goto_120 = false;
                          aulang_writer$byte(reg_1, 6);
                        }
                        num(reg_1, reg_72.c);
                        num(reg_1, reg_72.b.a);
                      }
                      if ((goto_128 || false)) {
                        goto_128 = false;
                        throw new Error(("Unsupported instruction: " + inst_name(reg_16)));
                      }
                    }
                  }
                }
              }
            }
          }
        }
        reg_13 = (reg_13 + 1);
      }
    }
    reg_2 = (reg_2 + 1);
  }
  return;
}
function next (reg_0) {
  var reg_3, reg_7;
  if ((reg_0.c == null)) {
    reg_3 = null;
    return reg_3;
  }
  reg_7 = reg_0.c;
  return reg_7;
}
function tobuffer (reg_0) {
  var reg_3, reg_7, reg_8, reg_9, reg_10, reg_19;
  var goto_31=false;
  reg_3 = (reg_0.c * cns1);
  reg_7 = new Uint8Array((reg_3 + reg_0.b.b));
  reg_8 = reg_0.a;
  reg_9 = 0;
  loop_1: while (true) {
    reg_10 = 0;
    loop_2: while ((reg_10 < reg_8.b)) {
      reg_7[reg_9]=reg_8.a[reg_10];
      reg_10 = (reg_10 + 1);
      reg_9 = (reg_9 + 1);
    }
    reg_19 = next(reg_8);
    goto_31 = !!(reg_19 == null);
    if (goto_31) break loop_1;
    reg_8 = reg_19;
  }
  if (!goto_31) {
  }
  goto_31 = false;
  return reg_7;
}
function getBuffer (reg_0) {
  var reg_1, reg_5;
  reg_1 = reg_0.d;
  rawstr(reg_1, "Auro 0.6");
  aulang_writer$byte(reg_1, 0);
  writeModules(reg_0);
  writeTypes(reg_0);
  writeFunctions(reg_0);
  writeConstants(reg_0);
  writeCode(reg_0);
  num(reg_1, 0);
  reg_5 = tobuffer(reg_1);
  return reg_5;
}
function compile (reg_0, reg_1) {
  var reg_2, reg_3, reg_4, reg_5, reg_6, reg_7, reg_8, reg_9, reg_13, reg_14, reg_16, reg_21, reg_24, reg_34, reg_36, reg_37, reg_40, reg_50, reg_55, reg_64, reg_77, reg_80, reg_81, reg_83, reg_86, reg_87, reg_88, reg_99, reg_108, reg_109, reg_115, reg_116, reg_121, reg_123, reg_124, reg_129, reg_132, reg_137, reg_138, reg_141, reg_144, reg_149, reg_154, reg_166, reg_167, reg_169, reg_175, reg_189, reg_190, reg_194, reg_201, reg_206, reg_211, reg_217, reg_227;
  var goto_58=false, goto_77=false, goto_92=false, goto_98=false, goto_128=false, goto_157=false, goto_211=false, goto_219=false, goto_232=false;
  reg_2 = {};
  reg_3 = aulang_writer$_new();
  reg_4 = [];
  reg_5 = [];
  reg_6 = [];
  reg_7 = [];
  reg_8 = {};
  reg_9 = {};
  reg_13 = {a: reg_1, b: reg_0, c: reg_2, d: reg_3, e: reg_4, f: reg_5, g: reg_6, h: reg_7, i: reg_8, j: reg_9, k: false, l: []};
  reg_14 = Module(reg_13);
  reg_14.b = "define";
  reg_16 = 0;
  loop_4: while ((reg_16 < len(reg_13.b))) {
    reg_21 = child(reg_13.b, reg_16);
    reg_16 = (reg_16 + 1);
    reg_24 = true;
    if ((reg_21.a == "private")) {
      reg_24 = false;
      reg_21 = child(reg_21, 0);
    }
    goto_98 = !(reg_21.a == "module-assign");
    if (!goto_98) {
      reg_34 = Module(reg_13);
      reg_34.g = reg_21.c;
      reg_36 = new record_23(reg_34);
      reg_37 = reg_21.b;
      reg_13.c[reg_37] = reg_36;
      reg_40 = child(reg_21, 0);
      goto_58 = !(reg_40.a == "import");
      if (!goto_58) {
        reg_34.b = "import";
        reg_34.c = reg_40.b;
      }
      if ((goto_58 || false)) {
        goto_58 = false;
        goto_77 = !(reg_40.a == "functor");
        if (!goto_77) {
          reg_34.b = "build";
          reg_50 = "module";
          reg_34.d = new record_33(Item(reg_13, reg_50, child(reg_40, 0)));
          reg_55 = "module";
          reg_34.e = new record_33(Item(reg_13, reg_55, child(reg_40, 1)));
        }
        if ((goto_77 || false)) {
          goto_77 = false;
          goto_92 = !(reg_40.a == "field");
          if (!goto_92) {
            reg_34.b = "use";
            reg_64 = "module";
            reg_34.d = new record_33(Item(reg_13, reg_64, child(reg_40, 0)));
            reg_34.c = reg_40.b;
          }
          if ((goto_92 || false)) {
            goto_92 = false;
            error(reg_13, (reg_40.a + " module expression"), reg_40.c);
          }
        }
      }
    }
    if ((goto_98 || false)) {
      goto_98 = false;
      goto_128 = !(reg_21.a == "module-def");
      if (!goto_128) {
        reg_77 = Module(reg_13);
        reg_77.b = "define";
        reg_77.g = reg_21.c;
        reg_80 = new record_23(reg_77);
        reg_81 = reg_21.b;
        reg_13.c[reg_81] = reg_80;
        reg_83 = 0;
        loop_7: while ((reg_83 < len(reg_21))) {
          reg_86 = child(reg_21, reg_83);
          reg_87 = reg_86.b;
          reg_88 = "item";
          set(reg_77, reg_87, new record_33(Item(reg_13, reg_88, child(reg_86, 0))));
          reg_83 = (reg_83 + 1);
        }
      }
      if ((goto_128 || false)) {
        goto_128 = false;
        goto_157 = !(reg_21.a == "type-assign");
        if (!goto_157) {
          reg_99 = child(reg_21, 0);
          if (!(reg_99.a == "field")) {
            error(reg_13, ("Invalid type definition: " + to_string(reg_99)), reg_21.c);
          }
          reg_108 = reg_99.b;
          reg_109 = "module";
          reg_115 = new record_25(Type(reg_13, reg_108, new record_33(Item(reg_13, reg_109, child(reg_99, 0)))));
          reg_116 = reg_21.b;
          reg_13.c[reg_116] = reg_115;
        }
        if ((goto_157 || false)) {
          goto_157 = false;
          goto_232 = !(reg_21.a == "function");
          if (!goto_232) {
            reg_121 = aulang_compiler$Function(reg_13);
            reg_123 = child(reg_21, 0);
            reg_124 = 0;
            loop_6: while ((reg_124 < len(reg_123))) {
              reg_129 = child(child(reg_123, reg_124), 0);
              reg_132 = new record_33(Item(reg_13, "type", reg_129));
              reg_121.d.push(reg_132);
              reg_124 = (reg_124 + 1);
            }
            reg_137 = child(reg_21, 1);
            reg_138 = 0;
            loop_5: while ((reg_138 < len(reg_137))) {
              reg_141 = child(reg_137, reg_138);
              reg_144 = new record_33(Item(reg_13, "type", reg_141));
              reg_121.e.push(reg_144);
              reg_138 = (reg_138 + 1);
            }
            reg_149 = child(reg_21, 2);
            goto_211 = !(reg_149.a == "field");
            if (!goto_211) {
              reg_121.b = reg_149.b;
              reg_154 = "module";
              reg_121.c = new record_33(Item(reg_13, reg_154, child(reg_149, 0)));
            }
            if ((goto_211 || false)) {
              goto_211 = false;
              goto_219 = !(reg_149.a == "block");
              if (!goto_219) {
                reg_121.f = compile_function(reg_149, reg_21);
              }
              if ((goto_219 || false)) {
                goto_219 = false;
                error(reg_13, "Currently only imported functions are supported", reg_21.c);
              }
            }
            reg_166 = new record_29(reg_121);
            reg_167 = reg_21.b;
            reg_13.c[reg_167] = reg_166;
            if (reg_24) {
              reg_169 = new record_29(reg_121);
              set(reg_14, reg_21.b, reg_169);
            }
          }
          if ((goto_232 || false)) {
            goto_232 = false;
            if ((reg_21.a == "export")) {
              reg_175 = child(reg_21, 0);
              if (!(typeof reg_13.k === 'boolean')) {
                error(reg_13, "Export overrides previous export", reg_21.c);
              }
              reg_13.k = new record_33(Item(reg_13, "module", reg_175));
            }
          }
        }
      }
    }
  }
  if (!(typeof reg_13.k === 'boolean')) {
    reg_189 = getItem(reg_13, reg_13.k).val;
    reg_190 = 0;
    reg_13.e[reg_190]=reg_189;
    reg_194 = (reg_189.a - 1);
    reg_13.e.splice(reg_194, 1);
    reg_189.a = 0;
    loop_3: while ((reg_194 < reg_13.e.length)) {
      reg_201 = (reg_194 + 1);
      reg_13.e[reg_194].a = reg_201;
      reg_194 = (reg_194 + 1);
    }
  }
  reg_206 = 0;
  loop_2: while ((reg_206 < reg_13.g.length)) {
    reg_211 = reg_13.g[reg_206];
    if (!(reg_211.f == null)) {
      transform_instructions(reg_13, reg_211);
    }
    reg_206 = (reg_206 + 1);
  }
  reg_217 = 0;
  loop_1: while ((reg_217 < reg_13.l.length)) {
    reg_224 = getItem(reg_13, new record_33(reg_13.l[reg_217]));
    reg_217 = (reg_217 + 1);
  }
  reg_227 = getBuffer(reg_13);
  return reg_227;
}
function compile_src (reg_0, reg_1) {
  var reg_3;
  reg_3 = compile(fn1(reg_0), reg_1);
  return reg_3;
}
function fn39 (reg_0) {
  var reg_2;
  reg_2 = "";
  loop_1: while (true) {
    reg_2 = (reg_2 + Auro.string_new(Auro.io_read(reg_0, 512)));
    if (Auro.io_eof(reg_0)) {
      if (true) break loop_1;
    }
  }
  return reg_2;
}
function fn40 () {
  var reg_3, reg_14, reg_16, reg_20, reg_25;
  var goto_5=false;
  goto_5 = !(Auro.args.length == 3);
  if (!goto_5) {
  }
  if ((goto_5 || false)) {
    goto_5 = false;
    reg_3 = "Usage: ";
    console.log(((reg_3 + Auro.args[0]) + " <input> <output>"));
    Auro.exit(1);
  }
  reg_14 = Auro.io_open(Auro.args[1], 'r');
  reg_16 = fn39(reg_14);
  Auro.io_close(reg_14);
  reg_20 = compile_src(reg_16, Auro.args[1]);
  reg_25 = Auro.io_open(Auro.args[2], 'w');
  Auro.io_write(reg_25, reg_20);
  Auro.io_close(reg_25);
  return;
}
window["Aulang"] = {
  "argc": (function () {return Auro.args.length}),
  "argv": (function (a) {return Auro.args[a]}),
  "exit": Auro.exit,
  "newstr": Auro.string_new,
  "r": (function () {return 'r'}),
  "w": (function () {return 'w'}),
  "open": Auro.io_open,
  "read": Auro.io_read,
  "write": Auro.io_write,
  "close": Auro.io_close,
  "eof": Auro.io_eof,
  "compile": compile_src,
  "readall": fn39,
  "main": fn40,
};
})(window)
