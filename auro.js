(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){

var Compiler = require("./compiler.js")

var Auro = {
  modules: {},
  state: require("./state.js"),
  macros: require("./macros"),
  Compiler: Compiler,
  compile: Compiler.compile_to_string,
}

Compiler.setModuleLoader(function load_module (name) {
  var mod = Auro.modules[name]
  if (mod) return Compiler.getModule(mod, name)
})

if (typeof window === "undefined")
  throw new Error("Not running in a browser")

window.Auro = Auro

window.addEventListener("load", function () {
  var scripts = document.getElementsByTagName("script")

  var toload = 0
  var main_mod

  function loadEnd () {
    if (main_mod) {
      jscode = Auro.compile(main_mod, 'browser')
      var fn = new Function(jscode)
      fn()
    }
  }

  for (var i = 0; i < scripts.length; i++) {
    var script = scripts[i]
    if (script.type != "application/auro") continue

    var mod_name = script.getAttribute("auro-name")
    
    if (!mod_name && script.src) {
      mod_name = script.src.match(/^(?:.*\/)?([^?]+)/)[1]
      if (mod_name) {
        mod_name = mod_name.replace(/\./g, "\x1f")
      }
    }

    if (!mod_name) {
      console.warn("auro script does not have attribute auro-name")
      return
    }

    if (script.getAttribute("auro-main") == "auro-main") {
      main_mod = mod_name
    }

    if (script.src) {
      var xhr = new XMLHttpRequest();
      window.myxhr = xhr

      // Close over the loop variables (Fuck Javascript)
      var callback = (function (xhr, mod_name) {
        return function (e) {
          Auro.modules[mod_name] = new Uint8Array(xhr.response)
          if (--toload == 0) loadEnd()
        }
      })(xhr, mod_name)

      xhr.addEventListener("load", callback)

      xhr.responseType = "arraybuffer"
      xhr.open("GET", script.src)
      xhr.send()
      toload++
    } else if (script.textContent) {
      console.warn("TODO: Decode auro scripts as base64")
    }
  }
})

},{"./compiler.js":3,"./macros":4,"./state.js":6}],2:[function(require,module,exports){

function writeNodes (writer, nodes) {
  for (var i = 0; i < nodes.length; i++) {
    var stmt = nodes[i];
    if (stmt.write) stmt.write(writer);
    else if (stmt.use) writer.write(nodes[i].use() + ";");
    else writer.write("// ", JSON.stringify(nodes[i]));
  }
}

//=== Nodes ===//

function Assign (reg, expr) {
  this.reg = reg;
  this.expr = expr;

  this.use = function () { return this.reg.use() + " = " + this.expr.use() }
}

function Call (fn, args, outs) {
  this.fn = fn;
  this.args = args;
  this.outs = outs;

  this.use = function () {
    var args = this.args.map(function (arg) {return arg.use()});
    var left = "", right = "";
    if (this.outs != "inline") {
      if (this.outs.length > 1) {
        left = "var _r = ";
        for (var i = 0; i < this.outs.length; i++) {
          right += "; " + this.outs[i].use() + " = _r[" + i + "]";
        }
      } else if (this.outs.length == 1) {
        left = this.outs[0].use() + " = ";
      }
    }
    return left + this.fn.use(args) + right;
  }
}

function Return (args) {
  this.args = args;

  this.use = function () {
    if (this.args.length == 0) return "return";
    if (this.args.length == 1) return "return " + this.args[0].use();
    return "return [" + this.args.map(function (arg) {return arg.use()}).join(", ") + "]";
  }
}

function Label (id) {
  this.label = id;
  this.use = function () { return "// label " + this.label }
}

function Not (expr) {
  if (expr.isNot) return expr.expr
  if (expr == True) return False
  if (expr == False) return True
  return {
    expr: expr,
    isNot: true,
    use: function () { return "!" + this.expr.use() }
  }
}

function Or (a, b) {
  this.use = function () { return "(" + a.use() + " || " + b.use() + ")" }
}

var True  = {use: function () { return "true" }}
var False = {use: function () { return "false" }}



//=== Transformations ===//

function insertLabels (old, lbls) {
  var out = [];

  var insert = true;
  for (var i = 0; i < old.length; i++) {
    var stmt = old[i];
    if (insert || lbls.indexOf(i) >= 0) {
      out.push(new Label(i));
      insert = false;
    }
    if (!stmt.nop) out.push(stmt);
    if (stmt.isBranch) {
      insert = true;
      stmt.index = i;
    }
  }

  return out;
}

function expand (stmts) {
  // A register is propagated when the assignment that sets its value can be
  //   removed and the expression is moved directly where it is used.
  // An expression is said to be expanded when any of the values it uses
  //   is a register and it has been propagated.
  // Registers can only be propagated when doing so does not modify the order
  //   of execution of the expressions in the code, that is, when nothing is
  //   done between where they are evaluated and where they are used. In C-like
  //   languages, function arguments are evaluated in positional order and then
  //   the function is called with those values.
  // This is done before identifying control flow structures, so the statements
  //   with expressions are assignemnts, function calls and branches.
  // Removed statements are also processed because they contain previously
  //   propagated expressions and can be expanded further.
  // While processing statements, no expressions have been propagated, so all
  //   expressions are registers.
  // If a register is only used once and its assignment is known to be right
  //   before the statement that uses it, previous assignments are irrelevant
  //   and it can be expanded.

  function tryPropagate (stmt, reg) {
    if (reg.uses > 1) return false;
    if (stmt instanceof Assign && stmt.reg == reg) {
      stmt.remove = true;
      reg.prev = stmt.expr;
      reg.sets--;
      reg.uses--;
      return true;
    } else if (stmt instanceof Call && stmt.outs.length == 1 && stmt.outs[0] == reg) {
      stmt.outs = "inline";
      reg.prev = stmt;
      reg.sets--;
      reg.uses--;
      return true;
    }
    return false;
  }

  function getExpanded (reg) {
    while (reg.prev) {
      var prev = reg.prev;
      reg.prev = undefined;
      reg = prev;
    }
    return reg;
  }

  for (var i = stmts.length-1; i >= 0; i--) {
    var stmt = stmts[i];
    if (stmt instanceof Assign) {
      var prev = stmts[i-1];
      var reg = stmt.expr;
      tryPropagate(prev, reg);
    } else if (stmt instanceof Call) {
      var k = 1;
      for (var j = stmt.args.length - 1; j >= 0; j--) {
        var arg = stmt.args[j];
        var prev = stmts[i-k];
        if (tryPropagate(prev, arg)) k++
      }
    } else if (stmt.isBranch && stmt.cond) {
      var prev = stmts[i-1];
      var reg = stmt.cond;
      tryPropagate(prev, reg);
    }
  }

  var old = stmts;
  stmts = [];
  for (var i = 0; i < old.length; i++) {
    var stmt = old[i];
    if (stmt instanceof Assign) {
      if (stmt.reg.prev) continue;
      stmt.expr = getExpanded(stmt.expr);
      if (stmt.reg.uses == 0) stmt = stmt.expr;
    } else if (stmt instanceof Call) {
      for (var j = 0; j < stmt.args.length; j++) {
        stmt.args[j] = getExpanded(stmt.args[j]);
      }
      if (stmt.outs === "inline") continue;
    } else if (stmt.isBranch && stmt.cond) {
      stmt.cond = getExpanded(stmt.cond);
    }
    stmts.push(stmt);
  }
  return stmts;
}

function regularizeBranches (stmts) {
  for (var i = 0; i < stmts.length; i++) {
    var stmt = stmts[i]
    if (stmt.isBranch) {
      if (!stmt.cond) {
        stmt.cond = True
      } else if (stmt.neg) {
        stmt.cond = Not(stmt.cond)
        stmt.neg = false
      }
    }
  }
}

function removeGotos (stmts, fnName) {
  // Structured code generation:
  // Stupid papers do not include dates god dammit
  // "Taming Control Flow: A Structured Approach to Eliminating Goto Statements"
  //    Ana M. Erosa and Laurie J. Hendren

  var gotos = []
  var labels = {}
  var usedLabels = {}

  function getLabelReg (lbl) {
    var r = usedLabels[lbl]
    if (!r) {
      r = {use: function () {return "goto_" + lbl}}
      usedLabels[lbl] = r
    }
    return  r
  }

  function Block (stmts, parent) {
    this.stmts = stmts

    this.lineage = [this]
    this.level = 1
    this.setParent = function (parent) {
      this.parent = parent
      this.lineage = parent.lineage.slice(0)
      this.lineage.push(this)
      this.level = this.lineage.length
      for (var i = 0; i < this.stmts.length; i++) {
        var stmt = this.stmts[i]
        if (stmt.body !== undefined) stmt.body.setParent(this)
      }
    }
    if (parent) this.setParent(parent)

    this.calculateOffsets = function () {
      for (var i = 0; i < this.stmts.length; i++) {
        var stmt = this.stmts[i]
        if (stmt.isBranch || stmt.label !== undefined) {
          stmt.offset = i
          stmt.block = this
        }
        if (stmt.body !== undefined) {
          stmt.body.setParent(this)
          stmt.body.offset = i
        }
      }
    }

    this.slice = function (start, end) { return this.stmts.slice(start, end) }

    this.replace = function (start, end, item) {
      this.stmts.splice(start, end-start, item)
      this.calculateOffsets()
    }

    this.insert = function (pos, item) {
      this.stmts.splice(pos, 0, item)
      this.calculateOffsets()
    }

    this.direct = function (other) {
      var min = Math.min(this.level, other.level)
      for (var i = 0; i < min; i++) {
        if (this.lineage[i] != other.lineage[i]) return false
      }
      return true
    }

    this.calculateOffsets()
  }

  function Break (cond, target) {
    this.cond = cond
    this.target = target
    this.use = function () { return "if (" + this.cond.use() + ") break " + this.target.name }
  }

  function Continue (cond, target) {
    this.cond = cond
    this.target = target
    this.use = function () { return "if (" + this.cond.use() + ") continue " + this.target.name }
  }

  function If (body, cond) {
    this.isIf = true
    this.body = body
    this.cond = Not(cond)
    body.container = this
    this.write = function (writer) {
      var cond = this.cond ? this.cond.use() : "true"
      writer.write("if (", cond, ") {")
      writer.indent();
      writeNodes(writer, this.body.stmts);
      writer.dedent();
      writer.write("}");
    }
  }

  var loopcount = 1
  function Loop (body, cond, startlbl, breaklbl) {
    this.isLoop = true
    this.body = body
    body.container = this
    this.name = "loop_" + (loopcount++)

    var stmts = body.stmts

    for (var i = 0; i < stmts.length; i++) {
      var stmt = stmts[i]
      if (stmt.isBranch && stmt.lbl === breaklbl) {
        stmts[i] = new Break(stmt.cond, this)
        gotos.splice(gotos.indexOf(stmt), 1)
      }
      if (stmt.isBranch && stmt.lbl === startlbl) {
        stmts[i] = new Continue(stmt.cond, this)
        gotos.splice(gotos.indexOf(stmt), 1)
      }
    }

    if (cond != True) stmts.push(new Break(Not(cond), this))

    this.cond = True
    if (stmts[0] instanceof Break && stmts[0].target == this) {
      this.cond = Not(stmts.shift().cond)
    }

    body.calculateOffsets()

    this.write = function (writer) {
      writer.write(this.name + ": while (", this.cond.use(), ") {");
      writer.indent();
      writeNodes(writer, this.body.stmts);
      writer.dedent();
      writer.write("}")
    }
  }

  for (var i = 0; i < stmts.length; i++) {
    var stmt = stmts[i]
    if (stmt.isBranch) {
      gotos.push(stmt)
    } else if (stmt.label) {
      labels[stmt.label] = stmt
    }
  }

  var topBlock = new Block(stmts)

  mainloop:
  while (gotos.length > 0) {
    var stmt = gotos.pop()
    var label = labels[stmt.lbl]

    if (!label) {
      console.warn("WARNING: A goto in " + fnName + " goes to non-existing label " + stmt.lbl)
      continue mainloop
    }

    var block = stmt.block

    // Move outwards until direct and the goto not inmost
    while (!block.direct(label.block) || block.level > label.block.level) {
      var reg = getLabelReg(label.label)
      if (block.container instanceof Loop) {
        block.replace(stmt.offset, stmt.offset+1, new Break(reg, block.container))
      } else {
        var inner = block.slice(stmt.offset+1, block.stmts.length)
        var body = new Block(inner, block)
        var ifstmt = new If(body, reg)
        block.replace(stmt.offset, block.stmts.length, ifstmt)
      }
      block.insert(stmt.offset, new Assign(reg, stmt.cond))
      stmt.cond = reg
      block.parent.insert(block.offset+1, stmt)
      block = stmt.block
    }

    // Already direct, move inwards until siblings
    while (block.level < label.block.level) {
      var nextblock = label.block.lineage[block.level]
      if (nextblock.offset < stmt.offset) {
        throw new Exception("Goto Lifting not implemented")
        break mainloop
      } else {
        var reg = getLabelReg(label.label)
        var inner = block.slice(stmt.offset+1, nextblock.offset)
        var body = new Block(inner, block)
        var ifstmt = new If(body, reg)
        block.replace(stmt.offset, nextblock.offset, ifstmt)
        block.insert(stmt.offset, new Assign(reg, stmt.cond))
        nextblock.container.cond = new Or(reg, nextblock.container.cond)
        nextblock.insert(0, stmt)
        stmt.cond = reg
        block = stmt.block
      }
    }
    
    // Guaranteed to be siblings... I think
    if (stmt.block.level == label.block.level) {
      if (stmt.offset < label.offset) {
        // The label must end up outside the if, because the gotos are
        // processed bottom up and no inner structure will use the labels
        // again
        if (stmt.offset+1 == label.offset) {
          block.replace(stmt.offset, label.offset+1, label)
        } else {
          var inner = block.slice(stmt.offset+1, label.offset)
          var body = new Block(inner, block)
          var ifstmt = new If(body, stmt.cond)
          block.replace(stmt.offset, label.offset, ifstmt)
        }
      } else {
        // The label must end up outside the loop, because any inner goto
        // using it must necessarily be a continue statement
        var inner = block.slice(label.offset+1, stmt.offset)
        var body = new Block(inner, block)
        var breakstmt = block.stmts[stmt.offset+1]
        var breaklbl = breakstmt ? block.stmts[stmt.offset+1].label : null
        var ifstmt = new Loop(body, stmt.cond, label.label, breaklbl)
        block.replace(label.offset+1, stmt.offset+1, ifstmt)
      }
    } else throw new Error("goto " + stmt.lbl + " not sibling of target label")
    
    var reg = usedLabels[label.label]
    if (reg) {
      label.block.insert(label.offset, new Assign(reg, False))
    }
  }

  if (Object.keys(usedLabels).length) {
    topBlock.stmts.unshift({
      use: function () {
        var names = []
        for (var key in usedLabels) {
          names.push(usedLabels[key].use() + "=false")
        }
        return "var " + names.join(", ")
      }
    })
  }

  return topBlock.stmts
}

function cleanUp (old) {
  var stmts = []
  for (var i = 0; i < old.length; i++) {
    var stmt = old[i]
    if (stmt instanceof Label) continue
    if (stmt.isIf) {
      if (stmt.cond == False) continue
      var body = cleanUp(stmt.body.stmts)
      if (stmt.cond == True) {
        stmts = stmts.join(body)
        continue
      } else {
        stmt.body.stmts = body
      }
    } else if (stmt.isLoop) {
      var body = cleanUp(stmt.body.stmts)
      stmt.body.stmts = body
    }
    stmts.push(stmt)
  }
  return stmts
}


//=== Main Interface ===//

function Code (fn, getfn) {
  this._fn = fn;
  this._getfn = getfn;

  this.ins = fn.ins;
  this.outs = fn.outs;
  this.dependencies = [];
}

Code.prototype.build = function () {
  var fn = this._fn;

  var lbls = [];
  var regs = {};
  var regc = 0;

  function Reg (id, set) {
    if (this instanceof Reg) {
      this.id = id;
      this.uses = 0;
      this.sets = 0;
    } else {
      var reg;
      if (regs[id]) {
        reg = regs[id];
      } else {
        var reg = new Reg(id);
        regs[id] = reg;
      }
      if (set) reg.sets++;
      else reg.uses++;
      return reg;
    }
  }
  Reg.prototype.use = function () { return "reg_" + this.id; }

  function Branch (lbl, cond, neg) {
    this.isBranch = true;
    this.lbl = lbl;
    this.cond = cond;
    this.neg = Boolean(neg);
    lbls.push(lbl);
  }
  Branch.prototype.use = function () {
    if (this.cond) {
      var cond = this.cond.use();
      if (this.neg) cond = "!" + cond;
      return "if (" + cond + ") { goto(" + this.lbl + "); }";
    }
    return "goto(" + this.lbl + ")";
  }

  for (var i = 0; i < fn.ins.length; i++) { Reg(regc++, true) }

  var stmts = [];

  for (var i = 0; i < fn.code.length; i++) {
    var stmt = undefined;
    var inst = fn.code[i];
    var k = inst.type;
    if (k=="hlt") stmt = new Halt();
    if (k=="var") {Reg(regc++, true); stmt = {nop: true};}
    if (k=="dup") stmt = new Assign(Reg(regc++, true), Reg(inst.a));
    if (k=="set") stmt = new Assign(Reg(inst.a, true), Reg(inst.b));
    if (k=="jmp") stmt = new Branch(inst.a);
    if (k=="jif") stmt = new Branch(inst.a, Reg(inst.b));
    if (k=="nif") stmt = new Branch(inst.a, Reg(inst.b), true);
    if (k=="call") {
      var ff = this._getfn(inst.index);
      
      this.dependencies.push(ff);

      var args = inst.args.map(function (x) {return Reg(x);});
      var outs = [];
      for (var j = 0; j < ff.outs.length; j++) {
        outs.push(Reg(regc++, true));
      }
      stmt = new Call(ff, args, outs);
    }
    if (k=="end") {
      var args = inst.args.map(function (x) {return Reg(x);});
      stmt = new Return(args);
    }
    if (stmt === undefined) throw new Error("Unknown instruction " + k);
    else if (stmt) stmts.push(stmt);
  }

  regs.length = regc;

  stmts = insertLabels(stmts, lbls)
  stmts = expand(stmts)
  regularizeBranches(stmts)

  stmts = removeGotos(stmts, this.fnName)
  stmts = cleanUp(stmts)

  this.ast = stmts;
  this.regs = regs;
  
  var args = [], regs = [];
  for (var i = 0; i < this.ins.length; i++)
    args.push(this.regs[i].use());
  for (var i = this.ins.length; i < this.regs.length; i++) {
    var reg = this.regs[i];
    if (reg && reg.uses > 0 && reg.sets > 0) {
      regs.push(reg.use());
    }
  }

  this.regs = regs
  this.args = args
}

Code.prototype.compile = function (writer) {
  writer.write("function ", this.name, " (" + this.args.join(", ") + ") {");
  writer.indent();

  if (this.regs.length > 0) {
    writer.write("var " + this.regs.join(", ") + ";");
  }
  writeNodes(writer, this.ast);
  writer.dedent();
  writer.write("}");
}

Code.prototype.use = function (args) { return this.name + "(" + args.join(", ") + ")"; }

module.exports = Code;
},{}],3:[function(require,module,exports){

const parse = require("./parse.js")
const Writer = require("./writer.js")
const Code = require("./code.js")
const macros = require("./macros.js")
const state = require("./state.js")

const macro = macros.macro

var modLoader = function () { return null }

for (var name in macros.modules) {
  state.modules[name] = macros.modules[name]
}

function load_module (name) {
  var mod = state.modules[name]
  if (!mod) {
    mod = modLoader(name)
    state.modules[name] = mod
  }
  if (!mod) throw new Error("Module " + name + " not found")
  return mod
}

function decode_utf8 (bytes) {
  var codes = []
  for (var i = 0; i < bytes.length; i++) {
    var c = bytes[i]
    if (c > 0xEF) {
      c = (c & 0xF) << 0x12 |
          (bytes[++i] & 0x3F) << 0xC |
          (bytes[++i] & 0x3F) << 0x6 |
          (bytes[++i] & 0x3F)
    } else if (c > 0xDF) {
      c = (c & 0xF) << 0xC |
          (bytes[++i] & 0x3F) << 0x6 |
          (bytes[++i] & 0x3F)
    } else if (c > 0xBF) {
      c = (c & 0x1F) << 0x6 | (bytes[++i] & 0x3F)
    }

    if (c > 0xFFFF) {
      c -= 0x10000
      codes.push(c >>> 10 & 0x3FF | 0xD800)
      codes.push(0xDC00 | c & 0x3FF)
    } else {
      codes.push(c)
    }
  }

  return String.fromCharCode.apply(String, codes)
}

function escape (_str) {
  var str = ""
  for (var j = 0; j < _str.length; j++) {
    var code = _str.charCodeAt(j)
    var char = _str[j]
    if (char == '"') char = "\\\""
    else if (char == '\\') char = "\\\\"
    else if (char == "\n") char = "\\n"
    else if (char == "\t") char = "\\t"
    else if (code < 32 || code == 127) {
      var s = code.toString(16)
      while (s.length < 2) s = "0" + s
      char = "\\x" + s
    }
    str += char
  }
  return '"' + str + '"'
}

var findName = state.findName

var fnCount = 0, tpCount = 0, modCount = 0, cnsCount = 0

function getModule (data, moduleName) {
  var parsed = parse(data)
  var sourcemap = {};

  var modcache = {};
  function get_module (n) {
    if (modcache[n]) {
      var m = modcache[n]
      return m
    }
    function save (m) { modcache[n] = m; return m }
    var mdata = parsed.modules[n-1]
    if (mdata.type == "build") {
      var base = get_module(mdata.base)
      var arg = get_module(mdata.argument)
      if (!base.build) console.log(parsed.modules[mdata.base-1])
      var mod = base.build(arg)
      return save(mod)
    }
    if (mdata.type == "import") {
      return save(load_module(mdata.name))
    }
    if (mdata.type == "define") {
      return save({
        name: "mod" + ++modCount,
        get: function (iname) {
          var iparts = iname.split("\x1d")
          var exact = false
          var matches = []
          var item

          // Match item name with auro name matching rules (Complicated rules)
          mdata.items.forEach(function (it) {
            // Exact names have the biggest preference
            if (exact) return

            var parts = it.name.split("\x1d")

            // Main parts must match
            if (iparts[0] == parts[0]) {
              parts.splice(0, 1)

              // All parts in the given name must exist at least once
              // in this item's name
              for (var i = 1; i < iparts.length; i++) {
                var ix = parts.indexOf(iparts[i])

                // Not in item's name, fail
                if (ix < 0) return

                parts.splice(ix, 1)
              }

              if (parts.length == 0) exact = true
              matches.push(it.name)
              item = it
            }
          })

          if (!exact && matches.length > 1) {
            throw new Error("Name not specific enough: " +
              escape(iname) + " matches " +
              matches.map(escape).join(", ") +
              " in module " + moduleName)
          }

          if (!item) return null
          if (!item.value) {
            if (item.type == "function")
              item.value = get_function(item.index)
            else if (item.type == "type")
              item.value = get_type(item.index)
            else if (item.type == "module")
              item.value = get_module(item.index)
          }
          return item.value
        },
        get_items: function () {
          var items = {}

          mdata.items.forEach(function (item) {
            if (!item.value) {
              if (item.type == "function")
                item.value = get_function(item.index)
              else if (item.type == "type")
                item.value = get_type(item.index)
              else if (item.type == "module")
                item.value = get_module(item.index)
            }
            items[item.name] = item.value
          })

          return items
        }
      })
    }
    if (mdata.type == "use") {
      var mod = get_module(mdata.module)
      var item = mod.get(mdata.item)
      if (!item) throw new Error("Module", mdata.item, "not found in", mod)
      return save(item)
    }
    throw new Error(mdata.type + " modules not yet supported");
  }

  var funcache = {};
  function get_function (n) {
    if (funcache[n]) return funcache[n];
    var fn = parsed.functions[n];
    var f;
    if (fn.type == "import") {
      var mod = get_module(fn.module)
      f = mod.get(fn.name)
      if (!f) console.log(mod)
    } else if (fn.type == "code") {
      f = new Code(fn, get_function);
      if (sourcemap[n] && sourcemap[n].name) {
        f.name = findName(sourcemap[n].name, moduleName)
      } else {
        f.name = findName("fn" + ++fnCount)
      }
      f.fnName = f.name
    } else if (fn.type == "int") {
      f = macro(String(fn.value), 0, 1);
    } else if (fn.type == "bin") {
      var str = "\"";
      for (var j = 0; j < fn.data.length; j++) {
        var code = fn.data[j]
        var char = String.fromCharCode(code)
        if (char == '"') {char = "\\\""}
        else if (char == "\n") {char = "\\n"}
        else if (char == "\t") {char = "\\t"}
        else if (code < 32) {
          var s = code.toString(16)
          while (s.length < 2) s = "0" + s
          char = "\\x" + s
        }
        str += char;
      }
      str += "\"";
      f = macro(str, 0, 1);
      f.bytes = fn.data
    } else if (fn.type == "call") {

      var fndef = parsed.functions[fn.index]
      var is_new_str = fndef.type == "import" && fndef.name == "new" &&
        get_module(fndef.module) == macros.modules["auro\x1fstring"];

      if (is_new_str) {
        var bytes = get_function(fn.args[0]).bytes
        if (bytes instanceof Array) {
          // This is necessary to correctly read multi-byte characters
          var _str = decode_utf8(bytes)

          var str = ""
          for (var j = 0; j < _str.length; j++) {
            var code = _str.charCodeAt(j)
            var char = _str[j]
            if (char == '"') char = "\\\""
            else if (char == '\\') char = "\\\\"
            else if (char == "\n") char = "\\n"
            else if (char == "\t") char = "\\t"
            else if (code < 32 || code == 127) {
              var s = code.toString(16)
              while (s.length < 2) s = "0" + s
              char = "\\x" + s
            }
            str += char;
          }

          f = macro('"'+str+'"', 0, 1);
        }
      } else {
        var cfn = get_function(fn.index);
        var args = fn.args.map(function (ix) {
          return get_function(ix).use([]);
        });
        var expr = cfn.use(args);
        if (cfn.pure) {
          f = macro(expr, 0, 1);
        } else {
          var name = "cns" + ++cnsCount;

          f = {
            ins: [], outs: [-1],
            use: function () {return name},
            compile: function (writer) {
              writer.write("var " + name + " = " + expr)
            },
            dependencies: [cfn]
          };
        }
      }
    } else {
      throw new Error("Unsupported function kind " + fn.type);
    }
    funcache[n] = f;
    if (f instanceof Code) f.build()
    return f;
  }

  var tpcache = {};
  function get_type (n) {
    if (tpcache[n]) return tpcache[n]
    var tp = parsed.types[n]
    var mod = get_module(tp.module)
    var t = mod.get(tp.name)
    tpcache[n] = t
    if (!t.name) t.name = "tp" + ++tpCount
    return t
  }

  function getNode (node, name) {
    if (node instanceof Object) {
      for (k in node) {
        var x = node[k];
        if (x instanceof Object && x[0] === name) {
          return x;
        }
      }
    }
  }

  var srcnode = getNode(parsed.metadata, "source map");
  if (srcnode) {
    var file_node = getNode(srcnode, "file")
    var file = file_node ? file_node[1] : "file"
    for (var i = 1; i < srcnode.length; i++) {
      var item = srcnode[i]
      if (item[0] == "function") {
        var index = item[1];
        var name_node = getNode(item, "name")
        var line_node = getNode(item, "line")
        if (getNode(item, "name")) name = 
        sourcemap[index] = {
          file: file,
          name: name_node ? name_node[1] : "",
          line: line_node ? line_node[1] : "",
        }
      }
    }
  }

  return get_module(1)
}

function compile_to_string (modname, format, libname) {
  var writer = new Writer()
 
  var mod = load_module(modname)

  function write_compiled () {
    state.toCompile.forEach(function (item) {
      if (item.compile) item.compile(writer)
    })
  }

  var items
  function get_items () {
    if (!mod.get_items)
      throw new Error("Module " + modname + " has no accessible items")
    items = mod.get_items()

    for (var k in items) {
      state.toCompile.push(items[k])
    }
  }

  function get_main () {
    var main = mod.get("main")
    state.toCompile.push(main)
    return main
  }

  function write_items (fn) {
    for (var nm in items) {
      writer.write(fn(escape(nm), items[nm].name))
    }
  }

  function write_auro () {
    writer.write("var Auro = {};")
  }

  switch (format) {
    case 'nodelib':
      get_items()
      write_auro()
      write_compiled()
      write_items(function (key, val) {
        return "module.exports[" + key + "] = " + val
      })
      break
    case 'browserlib':
      get_items()

      writer.write("(function (window) {")
      write_auro()
      write_compiled()

      writer.write("window[" + escape(libname || modname) + "] = {")
      writer.indent()
      write_items(function (key, val) {
        return key + ": " + val + ","
      })
      writer.dedent()
      writer.write("};")
      writer.write("})(window)")
      break
    case 'browser':
    case 'node':
      var main = get_main()
      write_auro()
      write_compiled()
      writer.write(main.use([]))
      break
  }

  return writer.text
}

exports.setModuleLoader = function (fn) { modLoader = fn }
exports.escape = escape
exports.getModule = getModule
exports.compile_to_string = compile_to_string
},{"./code.js":2,"./macros.js":4,"./parse.js":5,"./state.js":6,"./writer.js":7}],4:[function(require,module,exports){

var state = require('./state.js')

var type_id = 0

var alphabet = "abcdefghijklmnopqrstuvwxyz"

function alphanum (n) {
  var a = alphabet[n%26]
  if (n >= 26) {
    a = alphanum(Math.floor(n/26)-1) + a
  }
  return a
}

function alphaslice (n) {
  var arr = []
  for (var i = 0; i < n; i++) {
    arr.push(alphanum(i))
  }
  return arr
}

function nativeType (name, is_class) {
  var tp = {
    name: name,
    id: type_id++,
    test: is_class ? null :
      macro("(typeof #1 === '" + name + "')", 1, 1)
  }
  return tp
}

function wrapperType (name) {
  name = name

  var tp = {
    name: name,
    id: type_id++,
    wrap: macro("new " + name + "(#1)", 1, 1),
    unwrap: macro("#1.val", 1, 1),
    test: macro("(#1 instanceof " + name + ")"),
    compile: function (w) {
      w.write("var " + this.name + " = function (val) { this.val = val; }")
    }
  }

  tp.wrap.dependencies = [tp]
  tp.unwrap.dependencies = [tp]
  tp.test.dependencies = [tp]

  return tp
}

function BaseModule (modname, data) {
  this.data = data
  this.get = function (name) {
    var val = data[name]
    if (!val) throw new Error(name + " not found in " + modname)
    if (typeof val == "function") {
      val = val()
      data[name] = val
    }
    return val
  }
}

function paramModule (mod) {
  mod._build = mod.build
  mod.cache = {}

  if (!mod.get_id) mod.get_id = function (arg) { return arg.get("0").id }

  mod.build = function (arg) {
    var id = this.get_id(arg)
    var mod = this.cache[id]
    if (!mod) {
      mod = this._build(arg, id)
      this.cache[id] = mod
    }
    return mod
  }
  return mod
}



var auroConsts = {
  args: "typeof process == \"undefined\" ? [] : process.argv.slice(1)",
  require: "function (name) {" +
    "\n  if (typeof require !== 'function') return null" +
    "\n  try { return require(name) }" +
    "\n  catch (e) {" +
    "\n  if (e.code === 'MODULE_NOT_FOUND') return null" +
    "\n    else throw e" +
    "\n  }" +
    "\n}",
  fs: "Auro.require('fs')",
}

for (var name in auroConsts) {
  auroConsts[name] = {
    name: name,
    code: auroConsts[name],
    compile: function (w) {
      w.write("Auro." + this.name + " = " + this.code + ";")
    }
  }
}

function getConsts (consts) {
  if (consts) return consts.map(function (it) {
    return (typeof it == 'string') ? auroConsts[it] : it
  })
}

function auroFn (name, ins, outc, code, deps) {
  var fn = {
    type: "function",
    code: code,
    name: "Auro." + name,
    ins: new Array(ins.length),
    outs: new Array(outc),
    use: function (args) {
      return this.name + "(" + args.join(", ") + ")"
    },
    compile: function (writer) {
      writer.write("Auro." + name + " = function (" + ins.join(", ") + ") {")
      writer.indent()
      writer.append(code)
      writer.dedent()
      writer.write("}")
    },
    dependencies: getConsts(deps),
  }
  return fn
}

function macro (str, inc, outc, deps) {
  var m = {
    type: "macro", macro: str,
    ins: new Array(inc), outs: new Array(outc),
    use: function (args) {
      var expr = this.macro;
      for (var i = 0; i < this.ins.length; i++) {
        var patt = new RegExp("#" + (i+1) + "(?!\\d)", "g")
        expr = expr.replace(patt, args[i]);
      }
      return expr;
    },
    dependencies: getConsts(deps),
  }
  var args = alphaslice(inc)
  m.name = "(function (" + args.join(",") + ") {return " + m.use(args) + "})"
  return m
}

macro.id = macro("#1", 1, 1)

var macro_modules = {
  "auro\x1fbool": new BaseModule("auro.bool", {
    "bool": nativeType("boolean"),
    "true": macro("true", 0, 1),
    "false": macro("false", 0, 1),
    "not": macro("!#1", 1, 1),
  }),
  "auro\x1fsystem": new BaseModule("auro.system", {
    "println": macro("console.log(#1)", 1, 0),
    "error": macro("throw new Error(#1)", 1, 0),
    "exit": auroFn("exit", ["code"], 0, "if (typeof process !== \"undefined\") process.exit(code)\nelse throw \"Auro Exit with code \" + code"),
    argc: macro("Auro.args.length", 0, 1, ["args"]),
    argv: macro("Auro.args[#1]", 1, 1, ["args"]),
  }),
  "auro\x1fint": new BaseModule("auro.int", {
    "int": wrapperType("Integer"),
    "neg": macro("-(#1)", 1, 1),
    "add": macro("(#1 + #2)", 2, 1),
    "sub": macro("(#1 - #2)", 2, 1),
    "mul": macro("(#1 * #2)", 2, 1),
    "div": macro("((#1 / #2) | 0)", 2, 1),
    "mod": macro("(#1 % #2)", 2, 1),
    "eq": macro("(#1 == #2)", 2, 1),
    "ne": macro("(#1 != #2)", 2, 1),
    "gt": macro("(#1 > #2)", 2, 1),
    "lt": macro("(#1 < #2)", 2, 1),
    "ge": macro("(#1 >= #2)", 2, 1),
    "le": macro("(#1 <= #2)", 2, 1),
    "gz": macro("(#1 > 0)", 1, 1),
    "nz": macro("(#1 != 0)", 1, 1),
  }),
  "auro\x1fint\x1fbit": new BaseModule("auro.int.bit", {
    "not": macro("~#1", 1, 1),
    "and": macro("(#1 & #2)", 2, 1),
    "or": macro("(#1 | #2)", 2, 1),
    "xor": macro("(#1 ^ #2)", 2, 1),
    "eq": macro("~(#1 ^ #2)", 2, 1),
    "shl": macro("(#1 << #2)", 2, 1),
    "shr": macro("(#1 >> #2)", 2, 1),
  }),
  "auro\x1ffloat": new BaseModule("auro.float", {
    "float": nativeType("number"),
    "neg": macro("-(#1)", 1, 1),
    "add": macro("(#1 + #2)", 2, 1),
    "sub": macro("(#1 - #2)", 2, 1),
    "mul": macro("(#1 * #2)", 2, 1),
    "div": macro("(#1 / #2)", 2, 1),
    "eq": macro("(#1 == #2)", 2, 1),
    "ne": macro("(#1 != #2)", 2, 1),
    "gt": macro("(#1 > #2)", 2, 1),
    "lt": macro("(#1 < #2)", 2, 1),
    "ge": macro("(#1 >= #2)", 2, 1),
    "le": macro("(#1 <= #2)", 2, 1),
    "gz": macro("(#1 > 0)", 1, 1),
    "nz": macro("(#1 != 0)", 1, 1),
    "itof": macro("#1", 1, 1),
    "ftoi": macro("#1", 1, 1),
    "decimal": macro("Auro.Float.decimal(#1, #2)", 2, 1),
    "nan": macro("NaN", 0, 1),
    "infinity": macro("Infinity", 0, 1),
    "isnan": macro("isNaN(#1)", 0, 1),
    "isinfinity": macro("Auro.Float.isInfinite(#1)", 1, 1),
  }),
  "auro\x1fstring": new BaseModule("auro.string", {
    "string": nativeType("string"),
    // TODO: This is fragile with wrong utf8 strings
    "new": auroFn("string_new", ["buf"], 1,
      "if (typeof buf === 'string') return buf" +
      "\nvar codes = []" +
      "\nfor (var j = 0; j < buf.length; j++) {" +
      "\n  var c = buf[j]" +
      "\n  if (c > 0xEF) {" +
      "\n    c = (c & 0xF) << 0x12 | (buf[++j] & 0x3F) << 0xC | (buf[++j] & 0x3F) << 0x6 | (buf[++j] & 0x3F)" +
      "\n  } else if (c > 0xDF) {" +
      "\n    c = (c & 0xF) << 0xC | (buf[++j] & 0x3F) << 0x6 | (buf[++j] & 0x3F)" +
      "\n  } else if (c > 0xBF) {" +
      "\n    c = (c & 0x1F) << 0x6 | (buf[++j] & 0x3F)" +
      "\n  }" +
      "\n  if (c > 0xFFFF) {" +
      "\n    c -= 0x10000" +
      "\n    codes.push(c >>> 10 & 0x3FF | 0xD800)" +
      "\n    codes.push(0xDC00 | c & 0x3FF)" +
      "\n  } else {" +
      "\n    codes.push(c)" +
      "\n  }" +
      "\n}" +
      "\nreturn String.fromCharCode.apply(String, codes)"),
    "itos": macro("String(#1)", 1, 1),
    "ftos": macro("String(#1)", 1, 1),
    "concat": macro("(#1 + #2)", 2, 1),
    "slice": macro("#1.slice(#2, #3)", 3, 1),
    "add": macro("(#1 + #2)", 2, 1),
    "eq": macro("(#1 == #2)", 2, 1),
    "length": macro("#1.length", 1, 1),
    "charat": auroFn("charat", ["str", "i"], 2, "return [str[i], i+1]"),
    "newchar": macro("String.fromCharCode(#1)", 1, 1),
    "codeof": macro("#1.charCodeAt(0)", 1, 1),
    "tobuffer": auroFn("str_tobuf", ["str"], 1,
      "bytes = []" +
      "\nfor (var i = 0; i < str.length; i++) {" +
      "\n  var c = str.charCodeAt(i)" +
      "\n  if (c >= 0xD800 && c <= 0xDFFF) {" +
      "\n    c = (c - 0xD800 << 10 | str.charCodeAt(++i) - 0xDC00) + 0x10000" +
      "\n  }" +
      "\n  if (c < 0x80) {" +
      "\n    bytes.push(c)" +
      "\n  } else if (c < 0x800) {" +
      "\n    bytes.push(c >> 0x6 | 0xC0, c & 0x3F | 0x80)" +
      "\n  } else if (c < 0x10000) {" +
      "\n    bytes.push(c >> 0xC | 0xE0, c >> 0x6 & 0x3F | 0x80, c & 0x3F | 0x80)" +
      "\n  } else {" +
      "\n    bytes.push(c >> 0x12 | 0xF0, c >> 0xC & 0x3F | 0x80, c >> 0x6 & 0x3F | 0x80, c & 0x3F | 0x80)" +
      "\n  }" +
      "\n}" +
      "\nreturn Uint8Array.from(bytes)"),
  }),
  "auro\x1fmath": new BaseModule("auro.math", {
    "pi": macro("Math.PI", 0, 1),
    "e": macro("Math.E", 0, 1),
    "sqrt2": macro("Math.SQRT2", 0, 1),
    "abs": macro("Math.abs(#1)", 1, 1),
    "ceil": macro("Math.ceil(#1)", 1, 1),
    "floor": macro("Math.floor(#1)", 1, 1),
    "round": macro("Math.round(#1)", 1, 1),
    "trunc": macro("Math.trunc(#1)", 1, 1),
    "ln": macro("Math.log(#1)", 1, 1),
    "exp": macro("Math.exp(#1)", 1, 1),
    "sqrt": macro("Math.sqrt(#1)", 1, 1),
    "cbrt": macro("Math.cbrt(#1)", 1, 1),
    "pow": macro("Math.pow(#1, #2)", 2, 1),
    "log": macro("(Math.log(#1) / Math.log(#2))", 2, 1),
    "mod": macro("(#1 % #2)", 2, 1),
    "sin": macro("Math.sin(#1)", 1, 1),
    "cos": macro("Math.cos(#1)", 1, 1),
    "tan": macro("Math.tan(#1)", 1, 1),
    "asin": macro("Math.asin(#1)", 1, 1),
    "acos": macro("Math.acos(#1)", 1, 1),
    "atan": macro("Math.atan(#1)", 1, 1),
    "sinh": macro("Math.sinh(#1)", 1, 1),
    "cosh": macro("Math.cosh(#1)", 1, 1),
    "tanh": macro("Math.tanh(#1)", 1, 1),
    "atan2": macro("Math.atan2(#1, #2)", 2, 1),
  }),
  "auro\x1fbuffer": new BaseModule("auro.buffer", {
    buffer: nativeType("Uint8Array", true),
    "new": macro("new Uint8Array(#1)", 1, 1),
    get: macro("#1[#2]", 2, 1),
    set: macro("#1[#2]=#3", 3, 0),
    size: macro("#1.length", 1, 1),
    readonly: macro("false", 1, 1),
  }),
  "auro\x1fio": new BaseModule("auro.system", {
    file: wrapperType("File"),
    r: macro("'r'", 0, 1),
    w: macro("'w'", 0, 1),
    a: macro("'a'", 0, 1),
    open: auroFn("io_open", ["path", "mode"], 1, "return {f: Auro.fs.openSync(path, mode), size: Auro.fs.statSync(path).size, pos: 0}", ["require", "fs"]),
    close: auroFn("io_close", ["file"], 0, "Auro.fs.closeSync(file.f)", ["require", "fs"]),
    read: auroFn("io_read", ["file", "size"], 1,
      "var buf = new Uint8Array(size)" +
      "\nvar redd = Auro.fs.readSync(file.f, buf, 0, size, file.pos)" +
      "\nfile.pos += redd" +
      "\nreturn buf.slice(0, redd)", ["require", "fs"]),
    write: auroFn("io_write", ["file", "buf"], 0,
      "var written = Auro.fs.writeSync(file.f, buf, 0, buf.length, file.pos)" +
      "\nfile.pos += written", ["require", "fs"]),
    eof: auroFn("io_eof", ["file"], 1, "return file.pos >= file.size"),
  }),
  "auro\x1farray": paramModule({
    build: function (arg) {
      var base = arg.get("0")
      var tp = wrapperType("Array_" + base.name);
      return  new BaseModule("auro.array", {
        "": tp,
        "new": macro("new Array(#2).fill(#1)", 2, 1),
        "empty": macro("[]", 0, 1),
        "get": macro("#1[#2]", 2, 1),
        "set": macro("#1[#2] = #3", 3, 0),
        "len": macro("#1.length", 1, 1),
        "push": macro("#1.push(#2)", 2, 0),
      });
    }
  }),
  "auro\x1fany": paramModule({
    base_mod: new BaseModule("auro.any", {
      "any": {
        name: "any",
        id: type_id++,
        test: macro("true")
      }
    }),
    build: function (arg) {
      var base = arg.get("0");
      if (!base) return this.base_mod;
      var id = base.id;
      return { "get": function (name) {
        if (name == "new") return base.wrap || macro.id
        if (name == "get") return base.unwrap || macro.id
        if (name == "test") return base.test || macro("(#1 instanceof " + base.name + ")", 1, 1, [base])
      } };
    },
    get: function (name) {
      if (name == "any") return this.base_mod.data.any;
    }
  }),
  "auro\x1fnull": paramModule({ build: function (arg) {
    var base = arg.get("0");
    var tp = wrapperType(state.findName("Null_" + base.name));
    return new BaseModule("auro.null", {
      "": tp,
      "null": macro("null", 0, 1),
      "new": macro.id,
      "get": macro.id,
      // null and undefined are loosely equals, so this tests both
      "isnull": macro("(#1 == null)", 1, 1),
    });
  } }),
  "auro\x1frecord": paramModule({
    get_id: function (arg) {
      var arr = [];
      var count = 0;
      while (true) {
        var a = arg.get(String(count));
        if (!a) break;
        arr.push(a.id);
        count++;
      }
      return arr.join(",");
    },
    build: function (arg, id) {
      var count = id.split(",").length
      var tname = state.findName("record_" + type_id)
      var tp = wrapperType(tname)

      var fields = []
      for (var i = 0; i < count; i++) {
        fields.push(alphanum(i) + ": #" + (i+1))
      }
      var new_macro = macro("{" + fields.join(", ") + "}", count, 1)

      return { get: function (name) {
        if (name == "new") return new_macro
        if (name == "") return tp
        
        var a = name.slice(0, 3);
        var n = name.slice(3);
        var l = alphanum(n)
        if (a == "get") return macro("#1." + l, 1, 1);
        if (a == "set") return macro("#1." + l + " = #2", 2, 0);
      } };
    }
  }),
  "auro\x1ftypeshell": {build: function (arg) {
    // Each time it's called, a new type is created
    var tname = state.findName("type_" + type_id++)
    var tp = wrapperType(tname)
    return new BaseModule("auro.typeshell", {
      "": tp,
      "new": macro.id,
      "get": macro.id,
    });
  } },
  "auro\x1ffunction": paramModule({
    get_id: function (arg) {
      var inlist = [];
      var innames = [];
      var outlist = [];
      var outnames = [];

      var i = 0;
      while (true) {
        var a = arg.get("in" + String(i));
        if (!a) break;
        inlist.push(a.id);
        i++;
      }

      var i = 0;
      while (true) {
        var a = arg.get("out" + String(i));
        if (!a) break;
        outlist.push(a.id);
        i++;
      }

      return inlist.join(",") + ":" + outlist.join(",");
    },
    build: function (arg, id) {

      var sig = id.split(":").map(function (l) {
        if (l == "") return []
        return l.split(",").map(parseInt)
      })
      var inlist = sig[0]
      var outlist = sig[1]

      var tp = wrapperType(state.findName("Function$" + type_id))

      mod = new BaseModule("auro.function", {
        "": tp,
        "apply": {
          ins: inlist,
          outs: outlist,
          use: function (fargs) {
            return fargs[0] + "(" + fargs.slice(1).join(", ") + ")"
          }
        },
        "new": { build: function (args) {
          var fn = args.get("0")

          return new BaseModule("function", {"": {
            ins: [],
            outs: [0],
            use: function (fargs) { return fn.name },
            dependencies: [fn],
          }})
        } },
        closure: {
          name: "Auro.Closure",
          build: function (args) {
            var fn = args.get("0")

            return new BaseModule("closure", {"new": {
              ins: inlist.slice(0, -1),
              outs: [0],
              use: function (fargs) {
                var args = alphaslice(inlist.length)
                var inargs = args.join(",")
                args.push("this")
                var fnargs = args.join(",")

                var def = "(function (" + inargs + ") { return " + fn.name + "(" + fnargs + ") })"
                return def + ".bind(" + fargs[0] + ")"
              },
              dependencies: [fn]
            }});
          }
        }
      });
      mod.name = "function" + tp.name
      return mod;
    }
  }),
  "auro\x1futils\x1fstringmap": paramModule({
    build: function (arg) {
      var base = arg.get("0")
      var tp = wrapperType(state.findName("StringMap_" + base.name));

      var itertp = {
        name: state.findName("StringMapIter_" + base.name),
        id: type_id++,
        compile: function (w) {
          w.write("function " + this.name + " (map) {")
          w.indent()
          w.write("this.map = map")
          w.write("this.i = 0")
          w.write("this.keys = Object.keys(map)")
          w.dedent()
          w.write("}")

          w.write(this.name + ".prototype.next = function () {")
          w.indent()
          w.write("if (this.i >= this.keys.length) return null")
          w.write("var k = this.keys[this.i++]")
          w.write("return {a: k, b: this.map[k]}")
          w.dedent()
          w.write("}")
        }
      }

      return new BaseModule("auro.utils.stringmap", {
        "": tp,
        "iterator": itertp,
        "new": macro("{}", 0, 1),
        "get": macro("#1[#2]", 2, 1),
        "set": macro("#1[#2] = #3", 3, 0),
        "remove": macro("delete #1[#2]", 2, 0),
        "new\x1diterator": macro("new " + itertp.name + "(#1)", 1, 1, [itertp]),
        "next\x1diterator": macro("#1.next()", 1, 1),
      })
    }
  }),
  "auro\x1futils\x1farraylist": paramModule({
    build: function (arg) {
      var base = arg.get("0");

      var name = state.findName("ArrayList_" + base.name)
      var tp = wrapperType(name)
      return new BaseModule("auro.utils.arraylist", {
        "": tp,
        "new": macro("[]", 0, 1),
        "get": macro("#1[#2]", 2, 1),
        "set": macro("#1[#2]=#3", 3, 0),
        "len": macro("#1.length", 1, 1),
        "push": macro("#1.push(#2)", 2, 0),
        "remove": macro("#1.splice(#2, 1)", 2, 0),
      });
    }
  }),
}

exports.macro = macro
exports.modules = macro_modules
},{"./state.js":6}],5:[function(require,module,exports){
module.exports = function parse (buffer) {

buffer = new Uint8Array(buffer);
var pos = 0;

function fail (msg) { throw new Error(msg + ". at byte " + pos.toString(16)); }
function unsupported (msg) { fail("Unsupported " + msg); }

function readByte () {
  if (pos >= buffer.length)
    fail("Unexpected end of file");
  return buffer[pos++];
}

function readInt () {
  var n = 0;
  var b = readByte();
  while ((b & 0x80) > 0) {
    n = (n << 7) | (b & 0x7f);
    b = readByte();
  }
  return (n << 7) | (b & 0x7f);
}

function readStr () {
  var len = readInt();
  var str = "";
  while (len > 0) {
    byte = readByte();
    str += String.fromCharCode(byte);
    len--;
  }
  return str;
}

function parseN (n, f) {
  var arr = [];
  for (var i = 0; i < n; i++)
    arr.push(f());
  return arr;
}

function readInts (n) { return parseN(n, readInt); }

var magic = "";
while (true) {
  var byte = readByte();
  if (byte == 0) break;
  magic += String.fromCharCode(byte);
}
if (magic !== "Auro 0.6") fail("Not an Auro 0.6 module");

var modules = parseN(readInt(), function () {
  var k = readInt();
  switch (k) {
    case 0: fail("Unknown import");
    case 1: return {
      type: "import",
      name: readStr(),
    };
    case 2: return {
      type: "define",
      items: parseN(readInt(), function () {
        var types = ["module", "type", "function", "const"];
        var k = readInt();
        if (k > 3) fail("Unknown item kind " + k);
        return {
          type: types[k],
          index: readInt(),
          name: readStr(),
        }
      })
    };
    case 3: return {
      type: "use",
      module: readInt(),
      item: readStr(),
    };
    case 4: return {
      type: "build",
      base: readInt(),
      argument: readInt(),
    };
    default: fail("Unknown import kind " + k);
  }
});

var types = parseN(readInt(), function () {
  var k = readInt();
  if (k == 0) fail("Null type");
  return {
    type: "import",
    module: k-1,
    name: readStr(),
  };
});

var functions = parseN(readInt(), function () {
  var k = readInt();
  var f;
  switch (k) {
    case 0: fail("Null function");
    case 1: f = {
      type: "code"
    }; break;
    default: f = {
      type: "import",
      module: k-2,
    };
  }
  f.ins = parseN(readInt(), readInt);
  f.outs = parseN(readInt(), readInt);
  if (k>1) f.name = readStr();
  return f;
});

var constCount = readInt();
for (var i = 0; i < constCount; i++) {
  var f;
  var k = readInt();
  if (k == 1) {
    f = {type: "int", value: readInt()};
  } else if (k == 2) {
    var len = readInt();
    var arr = [];
    for (var j = 0; j < len; j++)
      arr.push(readByte());
    f = {
      type: "bin",
      data: arr,
    };
  } else if (k < 16) {
    fail("Unknown constant kind " + k);
  } else {
    var ix = k-16;
    // Functions not yet in the function list are constants
    var argcount = (ix >= functions.length)? 0 : functions[ix].ins.length;
    f = {
      type: "call",
      index: ix,
      args: readInts(argcount),
    };
  }
  f.ins = [];
  f.outs = [-1];
  functions.push(f);
}

function parseCode (fn) {
  function one (tp) { return {type: tp, a: readInt()}; }
  function two (tp) { return {type: tp, a: readInt(), b: readInt()}; }
  var count = readInt();
  return parseN(count, function () {
    var k = readInt();
    switch (k) {
      case 0: return {
        type: "end",
        args: readInts(fn.outs.length)
      };
      case 1: return {type: "hlt"};
      case 2: return {type: "var"};
      case 3: return one("dup");
      case 4: return two("set");
      case 5: return one("jmp");
      case 6: return two("jif");
      case 7: return two("nif");
    }
    if (k < 16) fail("Unknown instruction " + k);
    var ix = k-16;
    if (ix >= functions.length)
      fail("Function index out of bounds");
    var ff = functions[ix];
    return {
      type: "call",
      index: ix,
      args: readInts(ff.ins.length),
    }
  });
}

for (var i = 0; i < functions.length; i++) {
  var fn = functions[i];
  if (fn.type == "code")
    fn.code = parseCode(fn);
}

function parseNode () {
  var n = readInt();
  if (n & 1) return n>>1;
  if (n & 2) {
    n = n >> 2;
    var str = "";
    while (n > 0) {
      byte = readByte();
      str += String.fromCharCode(byte);
      n--;
    }
    return str;
  }
  return parseN(n>>2, parseNode);
}

var metadata = parseNode();

return {
  modules: modules,
  types: types,
  functions: functions,
  metadata: metadata,
};

}
},{}],6:[function(require,module,exports){

var reservedNames =  [
  // https://www.w3schools.com/js/js_reserved.asp
  "abstract", "await", "arguments", "boolean",
  "break", "byte", "case", "catch",
  "char", "class", "const", "continue",
  "debugger", "default", "delete", "do",
  "double", "else", "enum", "eval",
  "export", "extends", "false", "final",
  "finally", "float", "for", "function",
  "goto", "if", "implements", "import",
  "in", "instanceof", "int", "interface",
  "let", "long", "native", "new",
  "null", "package", "private", "protected",
  "public", "return", "short", "static",
  "super", "switch", "synchronized", "this",
  "throw", "throws", "transient", "true",
  "try", "typeof", "var", "void",
  "volatile", "while", "with", "yield",
  // Other keywords
  "undefined", "NaN", "Infinity",
  // Global Objects
  "Object", "Function", "Boolean", "Error", "Number", "Math", "String", "Array",
  // Browser specific
  "document", "window", "console",
  // NodeJS specific
  "global", "require", "module", "process", "Buffer",
  // Auro
  "Auro"
]


var nameSet = {}
reservedNames.forEach(function (name) { nameSet[name] = true })

var toCompile = []
var push = toCompile.push.bind(toCompile)
toCompile.push = function (val) {
  if (val.compiled) return
  val.compiled = true

  var deps = val.dependencies
  if (deps) {
    deps.forEach(function (dep) {
      toCompile.push(dep)
    })
  }
  if (this.indexOf(val) < 0) push(val)
}

exports.modules = {}
exports.nameSet = nameSet
exports.toCompile = toCompile

exports.findName = function (orig, modname) {
  function normalize (name) {
    name = name.replace(/[^$\w]+/g, "_")
    if (name.match(/^\d/)) name = "_"+name
    return name
  }
  var name = normalize(orig)
  if (exports.nameSet[name] && modname)
    name = normalize(modname + "$" + orig)
  var i = 1
  while (exports.nameSet[name]) {
    name = normalize(orig + "$" + i++)
  }
  exports.nameSet[name] = true
  return name
}

exports.reset = function () {
  toCompile.forEach(function (it) {it.compiled = false})
  toCompile.length = 0
}

},{}],7:[function(require,module,exports){

function Writer (text) {
  if (!(this instanceof Writer)) return new Writer()
  this.text = text ? text + "\n" : ""
  this._pre = ""
}

Writer.prototype.indent = function () { this._pre += "  "; }
Writer.prototype.dedent = function () { this._pre = this._pre.slice(2); }

Writer.prototype.write = function () {
  var line = this._pre;
  for (var i = 0; i < arguments.length; i++) {
    line += arguments[i];
  }
  this.text += line + "\n";
}

Writer.prototype.append = function (string) {
  var lines = string.replace(/\n$/, "").split("\n")

  for (var i = 0; i < lines.length; i++) {
    lines[i] = this._pre + lines[i]
  }
  this.text += lines.join("\n") + "\n"
}

module.exports = Writer;

},{}]},{},[1]);
