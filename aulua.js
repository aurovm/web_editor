(function (window) {
var Auro = {};
Auro.args = typeof process == "undefined" ? [] : process.argv.slice(1);
var Integer = function (val) { this.val = val; }
var File = function (val) { this.val = val; }
Auro.require = function (name) {
  if (typeof require !== 'function') return null
  try { return require(name) }
  catch (e) {
  if (e.code === 'MODULE_NOT_FOUND') return null
    else throw e
  }
};
Auro.fs = Auro.require('fs');
var record_7 = function (val) { this.val = val; }
var type_8 = function (val) { this.val = val; }
function nil () {
  var reg_3;
  reg_3 = new type_8({a: true});
  return reg_3;
}
var record_10 = function (val) { this.val = val; }
var record_11 = function (val) { this.val = val; }
function anyStr (reg_0) {
  var reg_1;
  reg_1 = reg_0;
  return reg_1;
}
var cns1 = anyStr("setmetatable")
var StringMap_any = function (val) { this.val = val; }
var ArrayList_any = function (val) { this.val = val; }
var record_15 = function (val) { this.val = val; }
var ArrayList_record_15 = function (val) { this.val = val; }
var type_17 = function (val) { this.val = val; }
var Null_type_17 = function (val) { this.val = val; }
function StringMapIter_any (map) {
  this.map = map
  this.i = 0
  this.keys = Object.keys(map)
}
StringMapIter_any.prototype.next = function () {
  if (this.i >= this.keys.length) return null
  var k = this.keys[this.i++]
  return {a: k, b: this.map[k]}
}
var Null_StringMapIter_any = function (val) { this.val = val; }
var Null_string = function (val) { this.val = val; }
var record_22 = function (val) { this.val = val; }
function testTable (reg_0) {
  var reg_1;
  reg_1 = (reg_0 instanceof record_22);
  return reg_1;
}
function getTable (reg_0) {
  var reg_1;
  reg_1 = reg_0.val;
  return reg_1;
}
var Null_any = function (val) { this.val = val; }
function testNil (reg_0) {
  var reg_1;
  reg_1 = (reg_0 instanceof type_8);
  return reg_1;
}
function testInt (reg_0) {
  var reg_1;
  reg_1 = (reg_0 instanceof Integer);
  return reg_1;
}
function getInt (reg_0) {
  var reg_1;
  reg_1 = reg_0.val;
  return reg_1;
}
function testStr (reg_0) {
  var reg_1;
  reg_1 = (typeof reg_0 === 'string');
  return reg_1;
}
function getStr (reg_0) {
  var reg_1;
  reg_1 = reg_0;
  return reg_1;
}
function getFloats (reg_0, reg_1) {
  var reg_2, reg_3, reg_10, reg_12, reg_13, reg_20, reg_22, reg_23, reg_24;
  var goto_7=false, goto_13=false, goto_24=false, goto_30=false;
  goto_7 = !(typeof reg_0 === 'number');
  if (!goto_7) {
    reg_2 = reg_0;
  }
  if ((goto_7 || false)) {
    goto_7 = false;
    goto_13 = !(reg_0 instanceof Integer);
    if (!goto_13) {
      reg_2 = reg_0.val;
    }
    if ((goto_13 || false)) {
      goto_13 = false;
      reg_10 = 0;
      reg_12 = 0;
      reg_13 = false;
      return [reg_10, reg_12, reg_13];
    }
  }
  goto_24 = !(typeof reg_1 === 'number');
  if (!goto_24) {
    reg_3 = reg_1;
  }
  if ((goto_24 || false)) {
    goto_24 = false;
    goto_30 = !(reg_1 instanceof Integer);
    if (!goto_30) {
      reg_3 = reg_1.val;
    }
    if ((goto_30 || false)) {
      goto_30 = false;
      reg_20 = 0;
      reg_22 = 0;
      reg_23 = false;
      return [reg_20, reg_22, reg_23];
    }
  }
  reg_24 = true;
  return [reg_2, reg_3, reg_24];
}
function testBool (reg_0) {
  var reg_1;
  reg_1 = (typeof reg_0 === 'boolean');
  return reg_1;
}
function getBool (reg_0) {
  var reg_1;
  reg_1 = reg_0;
  return reg_1;
}
var Null_record_22 = function (val) { this.val = val; }
var type_25 = function (val) { this.val = val; }
var record_27 = function (val) { this.val = val; }
var record_28 = function (val) { this.val = val; }
function IdState () {
  var reg_1;
  reg_1 = {a: 0};
  return reg_1;
}
var cns2 = IdState()
function emptyTable () {
  var reg_1, reg_3, reg_11;
  reg_1 = cns2.a;
  reg_3 = (reg_1 + 1);
  cns2.a = reg_3;
  reg_11 = {a: reg_1, b: {}, c: [], d: [], e: null, f: null, g: null};
  return reg_11;
}
var record_29 = function (val) { this.val = val; }
function State () {
  var reg_6;
  reg_6 = {a: false, b: emptyTable(), c: emptyTable(), d: emptyTable(), e: emptyTable(), f: emptyTable()};
  return reg_6;
}
var cns3 = State()
function get_metatable (reg_0) {
  var reg_3, reg_5, reg_8, reg_12, reg_16, reg_17;
  if (testTable(reg_0)) {
    reg_3 = getTable(reg_0).e;
    if ((reg_3 == null)) {
      reg_5 = null;
      return reg_5;
    }
    reg_8 = reg_3;
    return reg_8;
  }
  if ((reg_0 instanceof type_25)) {
    reg_12 = reg_0.val.c;
    return reg_12;
  }
  if (testStr(reg_0)) {
    reg_16 = cns3.d;
    return reg_16;
  }
  reg_17 = null;
  return reg_17;
}
var record_30 = function (val) { this.val = val; }
var Function$31 = function (val) { this.val = val; }
function testFn (reg_0) {
  var reg_1;
  reg_1 = (reg_0 instanceof Function$31);
  return reg_1;
}
function getFn (reg_0) {
  var reg_1;
  reg_1 = reg_0.val;
  return reg_1;
}
function newStack () {
  var reg_2;
  reg_2 = {a: 0, b: []};
  return reg_2;
}
function push (reg_0, reg_1) {
  reg_0.b.push(reg_1);
  return;
}
function more (reg_0) {
  var reg_1, reg_4;
  reg_1 = reg_0.a;
  reg_4 = (reg_1 < reg_0.b.length);
  return reg_4;
}
function first (reg_0) {
  var reg_2, reg_4, reg_5;
  if (more(reg_0)) {
    reg_2 = reg_0.a;
    reg_4 = reg_0.b[reg_2];
    return reg_4;
    if (true) { goto(9); };
  }
  reg_5 = nil();
  return reg_5;
}
function meta_binop (reg_0, reg_1, reg_2) {
  var reg_3, reg_9, reg_11, reg_12, reg_15, reg_16;
  var goto_9=false, goto_10=false;
  reg_3 = get_metatable(reg_0);
  goto_10 = !(reg_3 == null);
  if (!goto_10) {
    reg_3 = get_metatable(reg_1);
    goto_9 = !(reg_3 == null);
    if (!goto_9) {
    }
  }
  if ((goto_10 || (goto_9 || false))) {
    if (!goto_10) {
      if (!goto_9) {
      }
      goto_9 = false;
    }
    goto_10 = false;
    reg_9 = lua$get(reg_3, reg_2);
    if (testFn(reg_9)) {
      reg_11 = getFn(reg_9);
      reg_12 = newStack();
      push(reg_12, reg_0);
      push(reg_12, reg_1);
      reg_15 = first(reg_11(reg_12));
      return reg_15;
    }
  }
  reg_16 = null;
  return reg_16;
}
function tobool (reg_0) {
  var reg_2, reg_4, reg_5;
  if (testBool(reg_0)) {
    reg_2 = getBool(reg_0);
    return reg_2;
    if (true) { goto(12); };
  }
  if (testNil(reg_0)) {
    reg_4 = false;
    return reg_4;
    if (true) { goto(12); };
  }
  reg_5 = true;
  return reg_5;
}
function equals (reg_0, reg_1) {
  var reg_2, reg_5, reg_6, reg_11, reg_12, reg_17, reg_18, reg_19, reg_21, reg_22, reg_23, reg_24, reg_25, reg_28, reg_29, reg_30, reg_31, reg_32, reg_35, reg_38, reg_39, reg_43, reg_45, reg_47, reg_49, reg_50, reg_54, reg_56, reg_60, reg_62, reg_64, reg_66, reg_67;
  reg_2 = testNil(reg_0);
  if (reg_2) {
    reg_2 = testNil(reg_1);
  }
  if (reg_2) {
    reg_5 = true;
    return reg_5;
  }
  reg_6 = testInt(reg_0);
  if (reg_6) {
    reg_6 = testInt(reg_1);
  }
  if (reg_6) {
    reg_11 = (getInt(reg_0) == getInt(reg_1));
    return reg_11;
  }
  reg_12 = testStr(reg_0);
  if (reg_12) {
    reg_12 = testStr(reg_1);
  }
  if (reg_12) {
    reg_17 = (getStr(reg_0) == getStr(reg_1));
    return reg_17;
  }
  var _r = getFloats(reg_0, reg_1); reg_21 = _r[0]; reg_22 = _r[1]; reg_23 = _r[2];
  reg_18 = reg_21;
  reg_19 = reg_22;
  if (reg_23) {
    reg_24 = (reg_18 == reg_19);
    return reg_24;
  }
  reg_25 = testBool(reg_0);
  if (reg_25) {
    reg_25 = testBool(reg_1);
  }
  if (reg_25) {
    reg_28 = getBool(reg_0);
    reg_29 = getBool(reg_1);
    reg_31 = reg_28;
    if (reg_31) {
      reg_31 = reg_29;
    }
    reg_30 = reg_31;
    if (!reg_30) {
      reg_32 = !reg_28;
      if (reg_32) {
        reg_32 = !reg_29;
      }
      reg_30 = reg_32;
    }
    return reg_30;
  }
  reg_35 = testTable(reg_0);
  if (reg_35) {
    reg_35 = testTable(reg_1);
  }
  if (reg_35) {
    reg_38 = getTable(reg_0);
    reg_39 = getTable(reg_1);
    if ((reg_38.a == reg_39.a)) {
      reg_43 = true;
      return reg_43;
    }
    reg_45 = meta_binop(reg_0, reg_1, "__eq");
    if ((reg_45 == null)) {
      reg_47 = false;
      return reg_47;
    }
    reg_49 = tobool(reg_45);
    return reg_49;
  }
  reg_50 = (reg_0 instanceof type_25);
  if (reg_50) {
    reg_50 = (reg_1 instanceof type_25);
  }
  if (reg_50) {
    reg_54 = reg_0.val;
    reg_56 = reg_1.val;
    if ((reg_54.a == reg_56.a)) {
      reg_60 = true;
      return reg_60;
    }
    reg_62 = meta_binop(reg_0, reg_1, "__eq");
    if ((reg_62 == null)) {
      reg_64 = false;
      return reg_64;
    }
    reg_66 = tobool(reg_62);
    return reg_66;
  }
  reg_67 = false;
  return reg_67;
}
function lua$get (reg_0, reg_1) {
  var reg_3, reg_4, reg_11, reg_13, reg_15, reg_17, reg_19, reg_20, reg_21, reg_26, reg_29, reg_32;
  var goto_20=false, goto_30=false;
  goto_20 = !(reg_1 instanceof Integer);
  if (!goto_20) {
    reg_3 = reg_1.val;
    reg_4 = (reg_3 > 0);
    if (reg_4) {
      reg_4 = (reg_3 <= reg_0.c.length);
    }
    if (reg_4) {
      reg_11 = (reg_3 - 1);
      reg_13 = reg_0.c[reg_11];
      return reg_13;
    }
  }
  if ((goto_20 || false)) {
    goto_20 = false;
    if ((typeof reg_1 === 'string')) {
      reg_15 = reg_1;
      reg_17 = reg_0.b[reg_15];
      goto_30 = !(reg_17 == null);
      if (!goto_30) {
        reg_19 = nil();
        return reg_19;
      }
      if ((goto_30 || false)) {
        goto_30 = false;
        reg_20 = reg_17;
        return reg_20;
      }
    }
  }
  reg_21 = 0;
  loop_1: while ((reg_21 < reg_0.d.length)) {
    reg_26 = reg_0.d[reg_21];
    if (equals(reg_1, reg_26.a)) {
      reg_29 = reg_26.b;
      return reg_29;
    }
    reg_21 = (reg_21 + 1);
  }
  reg_32 = nil();
  return reg_32;
}
function typestr (reg_0) {
  var reg_2, reg_4, reg_5, reg_8, reg_10, reg_12, reg_14, reg_15;
  if (testTable(reg_0)) {
    reg_2 = "table";
    return reg_2;
    if (true) { goto(37); };
  }
  if (testStr(reg_0)) {
    reg_4 = "string";
    return reg_4;
    if (true) { goto(37); };
  }
  reg_5 = (typeof reg_0 === 'number');
  if (!reg_5) {
    reg_5 = testInt(reg_0);
  }
  if (reg_5) {
    reg_8 = "number";
    return reg_8;
    if (true) { goto(37); };
  }
  if (testNil(reg_0)) {
    reg_10 = "nil";
    return reg_10;
    if (true) { goto(37); };
  }
  if (testBool(reg_0)) {
    reg_12 = "bool";
    return reg_12;
    if (true) { goto(37); };
  }
  if (testFn(reg_0)) {
    reg_14 = "function";
    return reg_14;
    if (true) { goto(37); };
  }
  reg_15 = "userdata";
  return reg_15;
}
function tostr (reg_0) {
  var reg_2, reg_5, reg_8, reg_11, reg_12, reg_13, reg_16, reg_19, reg_21, reg_22, reg_24, reg_26, reg_29, reg_33, reg_35, reg_40, reg_41;
  var goto_5=false, goto_11=false, goto_17=false, goto_24=false;
  goto_5 = !testStr(reg_0);
  if (!goto_5) {
    reg_2 = getStr(reg_0);
    return reg_2;
  }
  if ((goto_5 || false)) {
    goto_5 = false;
    goto_11 = !(typeof reg_0 === 'number');
    if (!goto_11) {
      reg_5 = String(reg_0);
      return reg_5;
    }
    if ((goto_11 || false)) {
      goto_11 = false;
      goto_17 = !testInt(reg_0);
      if (!goto_17) {
        reg_8 = String(getInt(reg_0));
        return reg_8;
      }
      if ((goto_17 || false)) {
        goto_17 = false;
        if (testBool(reg_0)) {
          goto_24 = !getBool(reg_0);
          if (!goto_24) {
            reg_11 = "true";
            return reg_11;
          }
          if ((goto_24 || false)) {
            goto_24 = false;
            reg_12 = "false";
            return reg_12;
          }
        }
      }
    }
  }
  reg_13 = get_metatable(reg_0);
  if (!(reg_13 == null)) {
    reg_16 = reg_13;
    reg_19 = lua$get(reg_16, "__tostring");
    if (testFn(reg_19)) {
      reg_21 = getFn(reg_19);
      reg_22 = newStack();
      push(reg_22, reg_0);
      reg_24 = first(reg_21(reg_22));
      if ((typeof reg_24 === 'string')) {
        reg_26 = reg_24;
        return reg_26;
      }
      throw new Error("'__tostring' must return a string");
    }
  }
  if ((reg_0 instanceof record_22)) {
    reg_29 = "table:";
    reg_33 = (reg_29 + String(reg_0.val.a));
    return reg_33;
  }
  if ((reg_0 instanceof type_25)) {
    reg_35 = "userdata:";
    reg_40 = (reg_35 + String(reg_0.val.a));
    return reg_40;
  }
  reg_41 = typestr(reg_0);
  return reg_41;
}
function get (reg_0, reg_1) {
  var reg_4, reg_7, reg_10, reg_13, reg_15, reg_17, reg_18, reg_20, reg_22;
  if (testTable(reg_0)) {
    reg_4 = lua$get(getTable(reg_0), reg_1);
    if (!testNil(reg_4)) {
      return reg_4;
    }
  }
  reg_7 = get_metatable(reg_0);
  if (!(reg_7 == null)) {
    reg_10 = reg_7;
    reg_13 = lua$get(reg_10, anyStr("__index"));
    if (testTable(reg_13)) {
      reg_15 = get(reg_13, reg_1);
      return reg_15;
    }
    if (testFn(reg_13)) {
      reg_17 = getFn(reg_13);
      reg_18 = newStack();
      push(reg_18, reg_0);
      push(reg_18, reg_1);
      reg_20 = first(reg_17(reg_18));
      return reg_20;
    }
  }
  if (testTable(reg_0)) {
    reg_22 = nil();
    return reg_22;
  }
  throw new Error((("Lua: tried to index a non-table value (" + tostr(reg_0)) + ")"));
}
function newTable () {
  var reg_1;
  reg_1 = new record_22(emptyTable());
  return reg_1;
}
var cns4 = anyStr("__index")
function lua$set (reg_0, reg_1, reg_2) {
  var reg_4, reg_11, reg_18, reg_21, reg_23, reg_28, reg_33;
  var goto_12=false, goto_27=false, goto_28=false, goto_29=false, goto_35=false;
  goto_29 = !(reg_1 instanceof Integer);
  if (!goto_29) {
    reg_4 = reg_1.val;
    goto_12 = !(reg_4 == (reg_0.c.length + 1));
    if (!goto_12) {
      reg_0.c.push(reg_2);
      goto_28 = true;
    }
    if ((goto_12 || !goto_28)) {
      goto_12 = false;
      reg_11 = (reg_4 > 0);
      if (reg_11) {
        reg_11 = (reg_4 <= reg_0.c.length);
      }
      goto_27 = !reg_11;
      if (!goto_27) {
        reg_18 = (reg_4 - 1);
        reg_0.c[reg_18]=reg_2;
        goto_28 = true;
      }
      if ((goto_27 || !goto_28)) {
        goto_27 = false;
        goto_35 = true;
      }
    }
    if ((goto_28 || (goto_28 || !goto_35))) {
      if (!goto_28) {
        goto_28 = false;
      }
      goto_28 = false;
    }
  }
  if ((goto_29 || (goto_35 || false))) {
    goto_29 = goto_29;
    if (!goto_29) {
      goto_35 = goto_35;
    }
    if ((goto_29 || !goto_35)) {
      goto_29 = false;
      goto_35 = !(typeof reg_1 === 'string');
      if (!goto_35) {
        reg_21 = reg_1;
        reg_0.b[reg_21] = reg_2;
      }
    }
    if ((goto_35 || (goto_35 || false))) {
      if (!goto_35) {
        goto_35 = false;
      }
      goto_35 = false;
      reg_23 = 0;
      loop_1: while ((reg_23 < reg_0.d.length)) {
        reg_28 = reg_0.d[reg_23];
        if (equals(reg_1, reg_28.a)) {
          reg_28.b = reg_2;
          return;
        }
        reg_23 = (reg_23 + 1);
      }
      reg_33 = {a: reg_1, b: reg_2};
      reg_0.d.push(reg_33);
    }
  }
  return;
}
function set (reg_0, reg_1, reg_2) {
  var goto_5=false;
  goto_5 = !(reg_0 instanceof record_22);
  if (!goto_5) {
    lua$set(reg_0.val, reg_1, reg_2);
  }
  if ((goto_5 || false)) {
    goto_5 = false;
    throw new Error((("Lua: tried to index a non-table value (" + tostr(reg_0)) + ")"));
  }
  return;
}
function call (reg_0, reg_1) {
  var reg_4;
  if (testFn(reg_0)) {
    reg_4 = getFn(reg_0)(reg_1);
    return reg_4;
    if (true) { goto(8); };
  }
  throw new Error("Lua: attempt to call a non-function value");
}
var cns5 = anyStr("setmetatable")
var cns6 = anyStr("__index")
var cns7 = anyStr("Lexer")
var cns8 = anyStr("Lexer")
var cns9 = anyStr("open")
var record_32 = function (val) { this.val = val; }
function next (reg_0) {
  var reg_2, reg_4, reg_8;
  if (more(reg_0)) {
    reg_2 = reg_0.a;
    reg_4 = reg_0.b[reg_2];
    reg_0.a = (reg_0.a + 1);
    return reg_4;
    if (true) { goto(13); };
  }
  reg_8 = nil();
  return reg_8;
}
var cns10 = anyStr("Lexer")
var cns11 = anyStr("error")
var cns12 = anyStr("source")
var cns13 = anyStr("sub")
Auro.charat = function (str, i) {
  return [str[i], i+1]
}
function isSpace (reg_0) {
  var reg_1, reg_2, reg_3;
  reg_1 = reg_0.charCodeAt(0);
  reg_3 = (reg_1 == 9);
  if (!reg_3) {
    reg_3 = (reg_1 == 10);
  }
  reg_2 = reg_3;
  if (!reg_2) {
    reg_2 = (reg_1 == 32);
  }
  return reg_2;
}
function parseNum (reg_0) {
  var reg_1, reg_2, reg_3, reg_4, reg_5, reg_7, reg_8, reg_9, reg_14, reg_15, reg_16, reg_26, reg_27, reg_28, reg_29, reg_35, reg_39, reg_45, reg_51, reg_57, reg_64, reg_65, reg_66, reg_67, reg_68, reg_75, reg_81, reg_87, reg_88, reg_91, reg_92, reg_93, reg_95, reg_99, reg_101, reg_102, reg_103, reg_111, reg_126, reg_133, reg_134, reg_135, reg_136, reg_140, reg_141, reg_146, reg_147, reg_148, reg_155, reg_157, reg_158, reg_159, reg_167, reg_183, reg_185, reg_186, reg_189, reg_190;
  var goto_64=false, goto_82=false, goto_100=false, goto_107=false, goto_145=false, goto_159=false, goto_210=false, goto_238=false, goto_266=false, goto_298=false;
  reg_1 = reg_0.length;
  reg_2 = 0;
  reg_4 = 0;
  reg_5 = false;
  loop_10: while ((reg_2 < reg_1)) {
    var _r = Auro.charat(reg_0, reg_2); reg_7 = _r[0]; reg_8 = _r[1];
    reg_3 = reg_7;
    reg_2 = reg_8;
    reg_9 = (reg_3.charCodeAt(0) == 48);
    if (reg_9) {
      reg_9 = (reg_2 < reg_1);
    }
    if (reg_9) {
      var _r = Auro.charat(reg_0, reg_2); reg_14 = _r[0]; reg_15 = _r[1];
      reg_3 = reg_14;
      reg_2 = reg_15;
      reg_16 = (reg_3.charCodeAt(0) == 88);
      if (!reg_16) {
        reg_16 = (reg_3.charCodeAt(0) == 120);
      }
      if (reg_16) {
        if (true) break loop_10;
      }
    }
    if (!isSpace(reg_3)) {
      goto_107 = true;
      if (goto_107) break loop_10;
    }
  }
  goto_107 = goto_107;
  if (!goto_107) {
    loop_9: while ((reg_2 < reg_1)) {
      var _r = Auro.charat(reg_0, reg_2); reg_26 = _r[0]; reg_27 = _r[1];
      reg_3 = reg_26;
      reg_2 = reg_27;
      reg_28 = reg_3.charCodeAt(0);
      reg_29 = (reg_28 <= 57);
      if (reg_29) {
        reg_29 = (reg_28 >= 48);
      }
      goto_64 = !reg_29;
      if (!goto_64) {
        reg_35 = (reg_4 * 16);
        reg_4 = (reg_35 + (reg_28 - 48));
      }
      if ((goto_64 || false)) {
        goto_64 = false;
        reg_39 = (reg_28 <= 70);
        if (reg_39) {
          reg_39 = (reg_28 >= 65);
        }
        goto_82 = !reg_39;
        if (!goto_82) {
          reg_45 = (reg_4 * 16);
          reg_4 = ((reg_45 + (reg_28 - 65)) + 10);
        }
        if ((goto_82 || false)) {
          goto_82 = false;
          reg_51 = (reg_28 >= 97);
          if (reg_51) {
            reg_51 = (reg_28 <= 102);
          }
          goto_100 = !reg_51;
          if (!goto_100) {
            reg_57 = (reg_4 * 16);
            reg_4 = ((reg_57 + (reg_28 - 97)) + 10);
          }
          if ((goto_100 || false)) {
            goto_100 = false;
            goto_145 = true;
            if (goto_145) break loop_9;
          }
        }
      }
    }
    goto_145 = goto_145;
  }
  if ((goto_107 || !goto_145)) {
    goto_107 = goto_107;
    if (!goto_107) {
    }
    loop_8: while ((goto_107 || (reg_2 < reg_1))) {
      if (!goto_107) {
        var _r = Auro.charat(reg_0, reg_2); reg_64 = _r[0]; reg_65 = _r[1];
        reg_3 = reg_64;
        reg_2 = reg_65;
      }
      goto_107 = false;
      reg_66 = reg_3.charCodeAt(0);
      reg_68 = (reg_66 == 46);
      if (!reg_68) {
        reg_68 = (reg_66 == 69);
      }
      reg_67 = reg_68;
      if (!reg_67) {
        reg_67 = (reg_66 == 101);
      }
      if (reg_67) {
        goto_159 = true;
        if (goto_159) break loop_8;
      }
      reg_75 = (reg_66 > 57);
      if (!reg_75) {
        reg_75 = (reg_66 < 48);
      }
      if (reg_75) {
        if (true) break loop_8;
      }
      reg_81 = (reg_4 * 10);
      reg_4 = (reg_81 + (reg_66 - 48));
      reg_5 = true;
    }
  }
  if ((goto_145 || !goto_159)) {
    goto_145 = false;
    loop_7: while ((reg_2 < reg_1)) {
      var _r = Auro.charat(reg_0, reg_2); reg_87 = _r[0]; reg_88 = _r[1];
      reg_3 = reg_87;
      reg_2 = reg_88;
      if (!isSpace(reg_3)) {
        reg_91 = nil();
        return reg_91;
      }
    }
    reg_92 = new Integer(reg_4);
    return reg_92;
  }
  goto_159 = false;
  reg_95 = 10;
  goto_210 = !(reg_3.charCodeAt(0) == 46);
  if (!goto_210) {
    reg_99 = 0;
    loop_6: while ((reg_2 < reg_1)) {
      var _r = Auro.charat(reg_0, reg_2); reg_101 = _r[0]; reg_102 = _r[1];
      reg_3 = reg_101;
      reg_2 = reg_102;
      reg_103 = (reg_3.charCodeAt(0) > 57);
      if (!reg_103) {
        reg_103 = (reg_3.charCodeAt(0) < 48);
      }
      if (reg_103) {
        if (true) break loop_6;
      }
      reg_111 = (reg_4 * 10);
      reg_4 = (reg_111 + (reg_3.charCodeAt(0) - 48));
      reg_99 = (reg_99 + 1);
      reg_5 = true;
    }
    reg_93 = reg_4;
    loop_5: while ((reg_99 > 0)) {
      reg_93 = (reg_93 / reg_95);
      reg_99 = (reg_99 - 1);
    }
  }
  if ((goto_210 || false)) {
    goto_210 = false;
    reg_93 = reg_4;
  }
  reg_126 = (reg_3.charCodeAt(0) == 69);
  if (!reg_126) {
    reg_126 = (reg_3.charCodeAt(0) == 101);
  }
  if (reg_126) {
    reg_133 = true;
    reg_134 = 0;
    var _r = Auro.charat(reg_0, reg_2); reg_135 = _r[0]; reg_136 = _r[1];
    reg_3 = reg_135;
    reg_2 = reg_136;
    goto_238 = !(reg_3.charCodeAt(0) == 45);
    if (!goto_238) {
      var _r = Auro.charat(reg_0, reg_2); reg_140 = _r[0]; reg_141 = _r[1];
      reg_3 = reg_140;
      reg_2 = reg_141;
      reg_133 = false;
    }
    if ((goto_238 || false)) {
      goto_238 = false;
      if ((reg_3.charCodeAt(0) == 43)) {
        var _r = Auro.charat(reg_0, reg_2); reg_146 = _r[0]; reg_147 = _r[1];
        reg_3 = reg_146;
        reg_2 = reg_147;
      }
    }
    reg_148 = (reg_3.charCodeAt(0) > 57);
    if (!reg_148) {
      reg_148 = (reg_3.charCodeAt(0) < 48);
    }
    if (reg_148) {
      reg_155 = nil();
      return reg_155;
    }
    goto_266 = true;
    if (!goto_266) {
    }
    loop_4: while ((goto_266 || (reg_2 < reg_1))) {
      if (!goto_266) {
        var _r = Auro.charat(reg_0, reg_2); reg_157 = _r[0]; reg_158 = _r[1];
        reg_3 = reg_157;
        reg_2 = reg_158;
      }
      goto_266 = false;
      reg_159 = (reg_3.charCodeAt(0) > 57);
      if (!reg_159) {
        reg_159 = (reg_3.charCodeAt(0) < 48);
      }
      if (reg_159) {
        if (true) break loop_4;
      }
      reg_167 = (reg_134 * 10);
      reg_134 = (reg_167 + (reg_3.charCodeAt(0) - 48));
    }
    goto_298 = !reg_133;
    if (!goto_298) {
      loop_3: while ((reg_134 > 0)) {
        reg_93 = (reg_93 * reg_95);
        reg_134 = (reg_134 - 1);
      }
    }
    if ((goto_298 || false)) {
      goto_298 = false;
      loop_2: while ((reg_134 > 0)) {
        reg_93 = (reg_93 / reg_95);
        reg_134 = (reg_134 - 1);
      }
    }
  }
  if (!reg_5) {
    reg_183 = nil();
    return reg_183;
  }
  loop_1: while ((reg_2 < reg_1)) {
    var _r = Auro.charat(reg_0, reg_2); reg_185 = _r[0]; reg_186 = _r[1];
    reg_3 = reg_185;
    reg_2 = reg_186;
    if (!isSpace(reg_3)) {
      reg_189 = nil();
      return reg_189;
    }
  }
  reg_190 = reg_93;
  return reg_190;
}
var cns14 = parseNum("1")
var cns15 = parseNum("1")
var cns16 = anyStr("char")
var cns17 = anyStr("char")
var cns18 = anyStr("")
function eq (reg_0, reg_1) {
  var reg_3;
  reg_3 = equals(reg_0, reg_1);
  return reg_3;
}
var cns19 = anyStr("eof")
var cns20 = anyStr("line")
var cns21 = parseNum("1")
var cns22 = anyStr("col")
var cns23 = parseNum("1")
var cns24 = anyStr("pos")
var cns25 = anyStr("saved_pos")
function Lexer_open (reg_0, reg_1) {
  var reg_4, reg_13, reg_14, reg_18, reg_25, reg_28, reg_35, reg_38;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  set(get(reg_1.a, cns10), cns11, nil());
  set(reg_1.a, cns12, reg_4);
  reg_13 = get(reg_4, cns13);
  reg_14 = newStack();
  push(reg_14, reg_4);
  push(reg_14, cns14);
  push(reg_14, cns15);
  reg_18 = first(call(reg_13, reg_14));
  set(reg_1.a, cns16, reg_18);
  reg_25 = eq(get(reg_1.a, cns17), cns18);
  set(reg_1.a, cns19, reg_25);
  reg_28 = newTable();
  set(reg_28, cns20, cns21);
  set(reg_28, cns22, cns23);
  set(reg_1.a, cns24, reg_28);
  reg_35 = nil();
  set(reg_1.a, cns25, reg_35);
  reg_38 = newStack();
  return reg_38;
}
function anyFn (reg_0) {
  var reg_1;
  reg_1 = new Function$31(reg_0);
  return reg_1;
}
var cns26 = anyStr("Lexer")
var cns27 = anyStr("error")
var cns28 = anyStr("pos")
var cns29 = anyStr("line")
var cns30 = anyStr(":")
var cns31 = anyStr("pos")
var cns32 = anyStr("col")
var cns33 = anyStr(":")
function concat (reg_0, reg_1) {
  var reg_5;
  reg_5 = anyStr((tostr(reg_0) + tostr(reg_1)));
  return reg_5;
}
var cns34 = anyStr("error")
var cns35 = anyStr("Lexer")
var cns36 = anyStr("error")
function lua_parser_lexer$function (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_13, reg_14, reg_19, reg_27, reg_28, reg_35;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns26);
  reg_8 = cns27;
  reg_13 = get(get(reg_1.a, cns28), cns29);
  reg_14 = cns30;
  reg_19 = get(get(reg_1.a, cns31), cns32);
  set(reg_7, reg_8, concat(reg_13, concat(reg_14, concat(reg_19, concat(cns33, reg_4)))));
  reg_27 = get(reg_1.a, cns34);
  reg_28 = newStack();
  push(reg_28, get(get(reg_1.a, cns35), cns36));
  reg_34 = call(reg_27, reg_28);
  reg_35 = newStack();
  return reg_35;
}
var cns37 = anyStr("err")
var cns38 = anyStr("source")
var cns39 = anyStr("sub")
var cns40 = parseNum("1")
function anyInt (reg_0) {
  var reg_1;
  reg_1 = new Integer(reg_0);
  return reg_1;
}
function stackof (reg_0) {
  var reg_1;
  reg_1 = newStack();
  push(reg_1, reg_0);
  return reg_1;
}
function length (reg_0) {
  var reg_4, reg_5, reg_8, reg_11, reg_16, reg_18, reg_22, reg_23, reg_38, reg_40, reg_48, reg_57;
  var goto_52=false, goto_61=false, goto_72=false;
  if (testStr(reg_0)) {
    reg_4 = anyInt(getStr(reg_0).length);
    return reg_4;
  }
  reg_5 = get_metatable(reg_0);
  if (!(reg_5 == null)) {
    reg_8 = reg_5;
    reg_11 = lua$get(reg_8, anyStr("__len"));
    if (!testNil(reg_11)) {
      reg_16 = first(call(reg_11, stackof(reg_0)));
      return reg_16;
    }
  }
  if ((reg_0 instanceof record_22)) {
    reg_18 = reg_0.val;
    reg_22 = (reg_18.c.length - 1);
    reg_23 = (reg_22 >= 0);
    if (reg_23) {
      reg_23 = (reg_18.c[reg_22] instanceof type_8);
    }
    goto_61 = !reg_23;
    if (!goto_61) {
      loop_2: while ((reg_22 >= 0)) {
        goto_52 = !testNil(reg_18.c[reg_22]);
        if (!goto_52) {
          reg_22 = (reg_22 - 1);
        }
        if ((goto_52 || false)) {
          goto_52 = false;
          reg_38 = new Integer((reg_22 + 1));
          return reg_38;
        }
      }
      reg_40 = new Integer(0);
      return reg_40;
    }
    if ((goto_61 || false)) {
      goto_61 = false;
      goto_72 = !(lua$get(reg_18, new Integer((reg_22 + 2))) instanceof type_8);
      if (!goto_72) {
        reg_48 = new Integer((reg_22 + 1));
        return reg_48;
      }
      if ((goto_72 || false)) {
        goto_72 = false;
        reg_22 = (reg_22 + 3);
        loop_1: while (true) {
          if ((lua$get(reg_18, anyInt(reg_22)) instanceof type_8)) {
            reg_57 = anyInt((reg_22 - 1));
            return reg_57;
          }
          reg_22 = (reg_22 + 1);
        }
      }
    }
  }
  throw new Error((("Lua: attempt to get length of a " + typestr(reg_0)) + " value"));
}
var cns41 = anyStr("source")
var cns42 = anyStr("match")
var cns43 = anyStr("^")
function append (reg_0, reg_1) {
  var reg_2;
  reg_2 = reg_1.a;
  loop_1: while ((reg_2 < reg_1.b.length)) {
    push(reg_0, reg_1.b[reg_2]);
    reg_2 = (reg_2 + 1);
  }
  return;
}
function function$1 (reg_0, reg_1) {
  var reg_4, reg_9, reg_11, reg_12, reg_19, reg_20, reg_23, reg_25, reg_26, reg_30;
  var goto_27=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  goto_27 = !tobool(next(reg_0));
  if (!goto_27) {
    reg_9 = get(reg_1.a, cns38);
    reg_11 = get(reg_9, cns39);
    reg_12 = newStack();
    push(reg_12, reg_9);
    push(reg_12, cns40);
    push(reg_12, length(reg_4));
    if (tobool(eq(first(call(reg_11, reg_12)), reg_4))) {
      reg_19 = newStack();
      push(reg_19, reg_4);
      return reg_19;
    }
  }
  if ((goto_27 || false)) {
    goto_27 = false;
    reg_20 = newStack();
    reg_23 = get(reg_1.a, cns41);
    reg_25 = get(reg_23, cns42);
    reg_26 = newStack();
    push(reg_26, reg_23);
    push(reg_26, concat(cns43, reg_4));
    append(reg_20, call(reg_25, reg_26));
    return reg_20;
  }
  reg_30 = newStack();
  return reg_30;
}
var cns44 = anyStr("check")
var cns45 = anyStr("check")
var cns46 = parseNum("0")
function str_cmp (reg_0, reg_1) {
  var reg_2, reg_3, reg_4, reg_6, reg_8, reg_10, reg_11, reg_13, reg_17, reg_19, reg_25, reg_27, reg_28;
  reg_2 = reg_0.length;
  reg_3 = reg_1.length;
  reg_4 = reg_2;
  if ((reg_3 < reg_2)) {
    reg_4 = reg_3;
  }
  reg_6 = 0;
  loop_1: while ((reg_6 < reg_4)) {
    var _r = Auro.charat(reg_0, reg_6); reg_8 = _r[0]; reg_9 = _r[1];
    reg_10 = reg_8.charCodeAt(0);
    var _r = Auro.charat(reg_1, reg_6); reg_11 = _r[0]; reg_12 = _r[1];
    reg_13 = reg_11.charCodeAt(0);
    if ((reg_10 < reg_13)) {
      reg_17 = (0 - 1);
      return reg_17;
    }
    if ((reg_10 > reg_13)) {
      reg_19 = 1;
      return reg_19;
    }
    reg_6 = (reg_6 + 1);
  }
  if ((reg_2 < reg_3)) {
    reg_25 = (0 - 1);
    return reg_25;
  }
  if ((reg_2 > reg_3)) {
    reg_27 = 1;
    return reg_27;
  }
  reg_28 = 0;
  return reg_28;
}
function meta_cmp (reg_0, reg_1, reg_2) {
  var reg_3, reg_12;
  reg_3 = meta_binop(reg_0, reg_1, reg_2);
  if ((reg_3 == null)) {
    throw new Error(((("Lua: attempt to compare " + typestr(reg_0)) + " with a ") + typestr(reg_1)));
  }
  reg_12 = reg_3;
  return reg_12;
}
function _le (reg_0, reg_1) {
  var reg_2, reg_7, reg_8, reg_9, reg_11, reg_12, reg_13, reg_14, reg_15, reg_22, reg_25;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_7 = (reg_0.val <= reg_1.val);
    return reg_7;
  }
  var _r = getFloats(reg_0, reg_1); reg_11 = _r[0]; reg_12 = _r[1]; reg_13 = _r[2];
  reg_8 = reg_11;
  reg_9 = reg_12;
  if (reg_13) {
    reg_14 = (reg_8 <= reg_9);
    return reg_14;
  }
  reg_15 = (typeof reg_0 === 'string');
  if (reg_15) {
    reg_15 = (typeof reg_1 === 'string');
  }
  if (reg_15) {
    reg_22 = (str_cmp(reg_0, reg_1) <= 0);
    return reg_22;
  }
  reg_25 = tobool(meta_cmp(reg_0, reg_1, "__le"));
  return reg_25;
}
function gt (reg_0, reg_1) {
  var reg_4;
  reg_4 = !_le(reg_0, reg_1);
  return reg_4;
}
var cns47 = anyStr("find")
var cns48 = anyStr("\n")
var cns49 = anyStr("pos")
var cns50 = anyStr("line")
var cns51 = anyStr("pos")
var cns52 = anyStr("line")
var cns53 = parseNum("1")
function meta_arith (reg_0, reg_1, reg_2) {
  var reg_3, reg_10;
  reg_3 = meta_binop(reg_0, reg_1, reg_2);
  if ((reg_3 == null)) {
    throw new Error((("Lua: attempt to perform arithmetic on a " + typestr(reg_0)) + " value"));
  }
  reg_10 = reg_3;
  return reg_10;
}
function add (reg_0, reg_1) {
  var reg_2, reg_8, reg_9, reg_10, reg_12, reg_13, reg_14, reg_16, reg_18;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_8 = new Integer((reg_0.val + reg_1.val));
    return reg_8;
  }
  var _r = getFloats(reg_0, reg_1); reg_12 = _r[0]; reg_13 = _r[1]; reg_14 = _r[2];
  reg_9 = reg_12;
  reg_10 = reg_13;
  if (reg_14) {
    reg_16 = (reg_9 + reg_10);
    return reg_16;
  }
  reg_18 = meta_arith(reg_0, reg_1, "__add");
  return reg_18;
}
var cns54 = parseNum("1")
var cns55 = anyStr("find")
var cns56 = anyStr("\n")
var cns57 = anyStr("sub")
var cns58 = anyStr("pos")
var cns59 = anyStr("col")
var cns60 = parseNum("1")
var cns61 = anyStr("pos")
var cns62 = anyStr("col")
var cns63 = anyStr("pos")
var cns64 = anyStr("col")
var cns65 = anyStr("source")
var cns66 = anyStr("sub")
var cns67 = parseNum("1")
var cns68 = parseNum("1")
function unm (reg_0) {
  var reg_5, reg_11, reg_12, reg_15, reg_18, reg_20, reg_21, reg_23;
  if ((reg_0 instanceof Integer)) {
    reg_5 = new Integer((0 - reg_0.val));
    return reg_5;
  }
  if ((typeof reg_0 === 'number')) {
    reg_11 = (0 - reg_0);
    return reg_11;
  }
  reg_12 = get_metatable(reg_0);
  if (!(reg_12 == null)) {
    reg_15 = reg_12;
    reg_18 = lua$get(reg_15, "__unm");
    if (testFn(reg_18)) {
      reg_20 = getFn(reg_18);
      reg_21 = newStack();
      push(reg_21, reg_0);
      reg_23 = first(reg_20(reg_21));
      return reg_23;
    }
  }
  throw new Error((("Lua: attempt to perform arithmetic on a " + typestr(reg_0)) + " value"));
}
var cns69 = anyStr("source")
var cns70 = anyStr("source")
var cns71 = anyStr("")
function lua$true () {
  var reg_1;
  reg_1 = true;
  return reg_1;
}
var cns72 = anyStr("eof")
function function$2 (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_11, reg_12, reg_18, reg_20, reg_21, reg_24, reg_28, reg_29, reg_40, reg_41, reg_45, reg_48, reg_49, reg_59, reg_60, reg_70, reg_72, reg_73, reg_80, reg_89, reg_92;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = get(reg_1.a, cns45);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, reg_5);
  reg_11 = first(call(reg_8, reg_9));
  reg_12 = reg_11;
  if (tobool(reg_11)) {
    reg_12 = gt(length(reg_11), cns46);
  }
  if (tobool(reg_12)) {
    reg_18 = nil();
    reg_20 = get(reg_11, cns47);
    reg_21 = newStack();
    push(reg_21, reg_11);
    push(reg_21, cns48);
    reg_24 = first(call(reg_20, reg_21));
    loop_1: while (tobool(reg_24)) {
      reg_28 = get(reg_1.a, cns49);
      reg_29 = cns50;
      set(reg_28, reg_29, add(get(get(reg_1.a, cns51), cns52), cns53));
      reg_18 = add(reg_24, cns54);
      reg_40 = get(reg_11, cns55);
      reg_41 = newStack();
      push(reg_41, reg_11);
      push(reg_41, cns56);
      push(reg_41, reg_18);
      reg_24 = first(call(reg_40, reg_41));
    }
    reg_45 = reg_11;
    if (tobool(reg_18)) {
      reg_48 = get(reg_11, cns57);
      reg_49 = newStack();
      push(reg_49, reg_11);
      push(reg_49, reg_18);
      reg_45 = first(call(reg_48, reg_49));
      set(get(reg_1.a, cns58), cns59, cns60);
    }
    reg_59 = get(reg_1.a, cns61);
    reg_60 = cns62;
    set(reg_59, reg_60, add(get(get(reg_1.a, cns63), cns64), length(reg_45)));
    reg_70 = get(reg_1.a, cns65);
    reg_72 = get(reg_70, cns66);
    reg_73 = newStack();
    push(reg_73, reg_70);
    push(reg_73, add(length(reg_11), cns67));
    push(reg_73, unm(cns68));
    reg_80 = first(call(reg_72, reg_73));
    set(reg_1.a, cns69, reg_80);
    if (tobool(eq(get(reg_1.a, cns70), cns71))) {
      reg_89 = lua$true();
      set(reg_1.a, cns72, reg_89);
    }
  }
  reg_92 = newStack();
  push(reg_92, reg_11);
  return reg_92;
}
var cns73 = anyStr("match")
var cns74 = anyStr("match")
var cns75 = anyStr("\n\x0d")
var cns76 = anyStr("match")
var cns77 = anyStr("\x0d\n")
var cns78 = anyStr("match")
var cns79 = anyStr("[\n\x0d]")
function function$3 (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_11, reg_15, reg_16, reg_23, reg_24;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newStack();
  reg_7 = get(reg_1.a, cns74);
  reg_8 = newStack();
  push(reg_8, cns75);
  reg_11 = first(call(reg_7, reg_8));
  if (!tobool(reg_11)) {
    reg_15 = get(reg_1.a, cns76);
    reg_16 = newStack();
    push(reg_16, cns77);
    reg_11 = first(call(reg_15, reg_16));
  }
  if (!tobool(reg_11)) {
    reg_23 = get(reg_1.a, cns78);
    reg_24 = newStack();
    push(reg_24, cns79);
    reg_11 = first(call(reg_23, reg_24));
  }
  push(reg_4, reg_11);
  return reg_4;
}
var cns80 = anyStr("match_nl")
var cns81 = anyStr("type")
var cns82 = anyStr("value")
var cns83 = anyStr("line")
var cns84 = anyStr("saved_pos")
var cns85 = anyStr("line")
var cns86 = anyStr("column")
var cns87 = anyStr("saved_pos")
var cns88 = anyStr("col")
function function$4 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = newTable();
  set(reg_7, cns81, reg_4);
  set(reg_7, cns82, reg_5);
  reg_10 = cns83;
  set(reg_7, reg_10, get(get(reg_1.a, cns84), cns85));
  reg_16 = cns86;
  set(reg_7, reg_16, get(get(reg_1.a, cns87), cns88));
  push(reg_6, reg_7);
  return reg_6;
}
var cns89 = anyStr("TK")
var cns90 = anyStr("  | and break do else elseif end false for function goto if in |\n  | local nil not or repeat return then true until while |\n")
var cns91 = parseNum("1")
var cns92 = anyStr("==")
var cns93 = parseNum("2")
var cns94 = anyStr("~=")
var cns95 = parseNum("3")
var cns96 = anyStr("<=")
var cns97 = parseNum("4")
var cns98 = anyStr(">=")
var cns99 = parseNum("5")
var cns100 = anyStr("<<")
var cns101 = parseNum("6")
var cns102 = anyStr(">>")
var cns103 = parseNum("7")
var cns104 = anyStr("=")
var cns105 = parseNum("8")
var cns106 = anyStr("&")
var cns107 = parseNum("9")
var cns108 = anyStr("~")
var cns109 = parseNum("10")
var cns110 = anyStr("|")
var cns111 = parseNum("11")
var cns112 = anyStr("<")
var cns113 = parseNum("12")
var cns114 = anyStr(">")
var cns115 = parseNum("13")
var cns116 = anyStr("//")
var cns117 = parseNum("14")
var cns118 = anyStr("+")
var cns119 = parseNum("15")
var cns120 = anyStr("-")
var cns121 = parseNum("16")
var cns122 = anyStr("*")
var cns123 = parseNum("17")
var cns124 = anyStr("/")
var cns125 = parseNum("18")
var cns126 = anyStr("%")
var cns127 = parseNum("19")
var cns128 = anyStr("^")
var cns129 = parseNum("20")
var cns130 = anyStr("#")
var cns131 = parseNum("21")
var cns132 = anyStr("(")
var cns133 = parseNum("22")
var cns134 = anyStr(")")
var cns135 = parseNum("23")
var cns136 = anyStr("{")
var cns137 = parseNum("24")
var cns138 = anyStr("}")
var cns139 = parseNum("25")
var cns140 = anyStr("[")
var cns141 = parseNum("26")
var cns142 = anyStr("]")
var cns143 = parseNum("27")
var cns144 = anyStr("::")
var cns145 = parseNum("28")
var cns146 = anyStr(";")
var cns147 = parseNum("29")
var cns148 = anyStr(":")
var cns149 = parseNum("30")
var cns150 = anyStr(",")
var cns151 = parseNum("31")
var cns152 = anyStr("...")
var cns153 = parseNum("32")
var cns154 = anyStr("..")
var cns155 = parseNum("33")
var cns156 = anyStr(".")
var cns157 = anyStr("match")
var cns158 = anyStr("%[%[")
var cns159 = anyStr("")
var cns160 = anyStr("check")
var cns161 = anyStr("%[=+")
var cns162 = anyStr("match")
var cns163 = anyStr("%[")
var cns164 = anyStr("match")
var cns165 = anyStr("=+")
var cns166 = anyStr("match")
var cns167 = anyStr("%[")
function anyBool (reg_0) {
  var reg_1;
  reg_1 = reg_0;
  return reg_1;
}
function not (reg_0) {
  var reg_3;
  reg_3 = anyBool(!tobool(reg_0));
  return reg_3;
}
var cns168 = anyStr("err")
var cns169 = anyStr("invalid long string delimiter")
var cns170 = anyStr("]")
var cns171 = anyStr("]")
var cns172 = anyStr("match_nl")
var cns173 = anyStr("")
var cns174 = anyStr("str")
var cns175 = anyStr("eof")
var cns176 = anyStr("match")
var cns177 = anyStr("%]=*%]?")
var cns178 = anyStr("str")
var cns179 = anyStr("str")
var cns180 = anyStr("str")
var cns181 = anyStr("str")
var cns182 = anyStr("match")
var cns183 = anyStr("[^%]]*")
var cns184 = anyStr("str")
var cns185 = anyStr("err")
var cns186 = anyStr("unfinished long string")
function function$5 (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_16, reg_17, reg_24, reg_25, reg_30, reg_31, reg_37, reg_38, reg_46, reg_47, reg_50, reg_52, reg_55, reg_61, reg_71, reg_72, reg_75, reg_79, reg_86, reg_91, reg_94, reg_95, reg_99, reg_104, reg_105, reg_108;
  var goto_16=false, goto_62=false, goto_105=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = nil();
  reg_7 = get(reg_1.a, cns157);
  reg_8 = newStack();
  push(reg_8, cns158);
  goto_16 = !tobool(first(call(reg_7, reg_8)));
  if (!goto_16) {
    reg_4 = cns159;
  }
  if ((goto_16 || false)) {
    goto_16 = false;
    reg_16 = get(reg_1.a, cns160);
    reg_17 = newStack();
    push(reg_17, cns161);
    goto_62 = !tobool(first(call(reg_16, reg_17)));
    if (!goto_62) {
      reg_24 = get(reg_1.a, cns162);
      reg_25 = newStack();
      push(reg_25, cns163);
      reg_27 = call(reg_24, reg_25);
      reg_30 = get(reg_1.a, cns164);
      reg_31 = newStack();
      push(reg_31, cns165);
      reg_4 = first(call(reg_30, reg_31));
      reg_37 = get(reg_1.a, cns166);
      reg_38 = newStack();
      push(reg_38, cns167);
      if (tobool(not(first(call(reg_37, reg_38))))) {
        reg_46 = get(reg_1.a, cns168);
        reg_47 = newStack();
        push(reg_47, cns169);
        reg_49 = call(reg_46, reg_47);
      }
    }
    if ((goto_62 || false)) {
      goto_62 = false;
      reg_50 = newStack();
      push(reg_50, nil());
      return reg_50;
    }
  }
  reg_52 = cns170;
  reg_55 = concat(reg_52, concat(reg_4, cns171));
  reg_60 = call(get(reg_1.a, cns172), newStack());
  reg_61 = cns173;
  set(reg_1.a, cns174, reg_61);
  loop_1: while (tobool(not(get(reg_1.a, cns175)))) {
    reg_71 = get(reg_1.a, cns176);
    reg_72 = newStack();
    push(reg_72, cns177);
    reg_75 = first(call(reg_71, reg_72));
    if (tobool(reg_75)) {
      goto_105 = !tobool(eq(reg_75, reg_55));
      if (!goto_105) {
        reg_79 = newStack();
        push(reg_79, get(reg_1.a, cns178));
        return reg_79;
      }
      if ((goto_105 || false)) {
        goto_105 = false;
        reg_86 = concat(get(reg_1.a, cns179), reg_75);
        set(reg_1.a, cns180, reg_86);
      }
    }
    reg_91 = get(reg_1.a, cns181);
    reg_94 = get(reg_1.a, cns182);
    reg_95 = newStack();
    push(reg_95, cns183);
    reg_99 = concat(reg_91, first(call(reg_94, reg_95)));
    set(reg_1.a, cns184, reg_99);
  }
  reg_104 = get(reg_1.a, cns185);
  reg_105 = newStack();
  push(reg_105, cns186);
  reg_107 = call(reg_104, reg_105);
  reg_108 = newStack();
  return reg_108;
}
var cns187 = anyStr("long_string")
var cns188 = anyStr("check")
var cns189 = anyStr("[a-fA-F%d]")
var cns190 = anyStr("match")
var cns191 = anyStr(".")
var cns192 = anyStr("lower")
var cns193 = anyStr("a")
function _lt (reg_0, reg_1) {
  var reg_2, reg_7, reg_8, reg_9, reg_11, reg_12, reg_13, reg_14, reg_15, reg_22, reg_25;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_7 = (reg_0.val < reg_1.val);
    return reg_7;
  }
  var _r = getFloats(reg_0, reg_1); reg_11 = _r[0]; reg_12 = _r[1]; reg_13 = _r[2];
  reg_8 = reg_11;
  reg_9 = reg_12;
  if (reg_13) {
    reg_14 = (reg_8 < reg_9);
    return reg_14;
  }
  reg_15 = (typeof reg_0 === 'string');
  if (reg_15) {
    reg_15 = (typeof reg_1 === 'string');
  }
  if (reg_15) {
    reg_22 = (str_cmp(reg_0, reg_1) < 0);
    return reg_22;
  }
  reg_25 = tobool(meta_cmp(reg_0, reg_1, "__lt"));
  return reg_25;
}
function ge (reg_0, reg_1) {
  var reg_4;
  reg_4 = !_lt(reg_0, reg_1);
  return reg_4;
}
var cns194 = anyStr("byte")
var cns195 = parseNum("97")
function sub (reg_0, reg_1) {
  var reg_2, reg_8, reg_9, reg_10, reg_12, reg_13, reg_14, reg_16, reg_18;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_8 = new Integer((reg_0.val - reg_1.val));
    return reg_8;
  }
  var _r = getFloats(reg_0, reg_1); reg_12 = _r[0]; reg_13 = _r[1]; reg_14 = _r[2];
  reg_9 = reg_12;
  reg_10 = reg_13;
  if (reg_14) {
    reg_16 = (reg_9 - reg_10);
    return reg_16;
  }
  reg_18 = meta_arith(reg_0, reg_1, "__sub");
  return reg_18;
}
var cns196 = parseNum("10")
var cns197 = anyStr("byte")
var cns198 = parseNum("48")
var cns199 = anyStr("err")
var cns200 = anyStr("hexadecimal digit expected")
function function$6 (reg_0, reg_1) {
  var reg_6, reg_7, reg_14, reg_15, reg_18, reg_20, reg_21, reg_23, reg_27, reg_29, reg_30, reg_37, reg_39, reg_40, reg_47, reg_48, reg_51;
  var goto_44=false, goto_56=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = get(reg_1.a, cns188);
  reg_7 = newStack();
  push(reg_7, cns189);
  goto_56 = !tobool(first(call(reg_6, reg_7)));
  if (!goto_56) {
    reg_14 = get(reg_1.a, cns190);
    reg_15 = newStack();
    push(reg_15, cns191);
    reg_18 = first(call(reg_14, reg_15));
    reg_20 = get(reg_18, cns192);
    reg_21 = newStack();
    push(reg_21, reg_18);
    reg_23 = first(call(reg_20, reg_21));
    goto_44 = !tobool(ge(reg_23, cns193));
    if (!goto_44) {
      reg_27 = newStack();
      reg_29 = get(reg_23, cns194);
      reg_30 = newStack();
      push(reg_30, reg_23);
      push(reg_27, add(sub(first(call(reg_29, reg_30)), cns195), cns196));
      return reg_27;
    }
    if ((goto_44 || false)) {
      goto_44 = false;
      reg_37 = newStack();
      reg_39 = get(reg_23, cns197);
      reg_40 = newStack();
      push(reg_40, reg_23);
      push(reg_37, sub(first(call(reg_39, reg_40)), cns198));
      return reg_37;
    }
  }
  if ((goto_56 || false)) {
    goto_56 = false;
    reg_47 = get(reg_1.a, cns199);
    reg_48 = newStack();
    push(reg_48, cns200);
    reg_50 = call(reg_47, reg_48);
  }
  reg_51 = newStack();
  return reg_51;
}
var cns201 = anyStr("get_hex")
var cns202 = anyStr("char")
var cns203 = anyStr("")
function ne (reg_0, reg_1) {
  var reg_4;
  reg_4 = anyBool(!equals(reg_0, reg_1));
  return reg_4;
}
var cns204 = anyStr("match")
var cns205 = anyStr("[ \x08\n\x0d\t\x0b]*")
var cns206 = anyStr("match")
var cns207 = anyStr("--")
var cns208 = anyStr("long_string")
var cns209 = anyStr("str")
var cns210 = anyStr("str")
var cns211 = anyStr("match")
var cns212 = anyStr("[^\n\x0d]*")
var cns213 = anyStr("line")
var cns214 = anyStr("pos")
var cns215 = anyStr("line")
var cns216 = anyStr("col")
var cns217 = anyStr("pos")
var cns218 = anyStr("col")
var cns219 = anyStr("saved_pos")
var cns220 = anyStr("eof")
var cns221 = anyStr("long_string")
var cns222 = anyStr("check")
var cns223 = anyStr("['\"]")
var cns224 = anyStr("match")
var cns225 = anyStr(".")
var cns226 = anyStr("")
var cns227 = anyStr("match")
var cns228 = anyStr("eof")
var cns229 = anyStr("check")
var cns230 = anyStr("[\n\x0d]")
var cns231 = anyStr("err")
var cns232 = anyStr("unfinished string")
var cns233 = anyStr("match")
var cns234 = anyStr("\\")
var cns235 = anyStr("a")
var cns236 = anyStr("\x07")
var cns237 = anyStr("b")
var cns238 = anyStr("\x08")
var cns239 = anyStr("f")
var cns240 = anyStr("\x0c")
var cns241 = anyStr("n")
var cns242 = anyStr("\n")
var cns243 = anyStr("r")
var cns244 = anyStr("\x0d")
var cns245 = anyStr("t")
var cns246 = anyStr("\t")
var cns247 = anyStr("v")
var cns248 = anyStr("\x0b")
var cns249 = anyStr("check")
var cns250 = anyStr("[abfnrtv]")
var cns251 = anyStr("match")
var cns252 = anyStr(".")
var cns253 = anyStr("check")
var cns254 = anyStr("['\"\\]")
var cns255 = anyStr("match")
var cns256 = anyStr(".")
var cns257 = anyStr("match_nl")
var cns258 = anyStr("\n")
var cns259 = anyStr("check")
var cns260 = anyStr("%d")
var cns261 = anyStr("tonumber")
var cns262 = anyStr("match")
var cns263 = anyStr("%d%d?%d?")
var cns264 = parseNum("265")
var cns265 = anyStr("err")
var cns266 = anyStr("decimal escape too large")
var cns267 = anyStr("string")
var cns268 = anyStr("char")
var cns269 = anyStr("match")
var cns270 = anyStr("x")
var cns271 = anyStr("get_hex")
var cns272 = parseNum("16")
function mul (reg_0, reg_1) {
  var reg_2, reg_8, reg_9, reg_10, reg_12, reg_13, reg_14, reg_16, reg_18;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_8 = new Integer((reg_0.val * reg_1.val));
    return reg_8;
  }
  var _r = getFloats(reg_0, reg_1); reg_12 = _r[0]; reg_13 = _r[1]; reg_14 = _r[2];
  reg_9 = reg_12;
  reg_10 = reg_13;
  if (reg_14) {
    reg_16 = (reg_9 * reg_10);
    return reg_16;
  }
  reg_18 = meta_arith(reg_0, reg_1, "__mul");
  return reg_18;
}
var cns273 = anyStr("get_hex")
var cns274 = anyStr("string")
var cns275 = anyStr("char")
var cns276 = anyStr("match")
var cns277 = anyStr("u")
var cns278 = anyStr("match")
var cns279 = anyStr("{")
var cns280 = anyStr("err")
var cns281 = anyStr("missing {")
var cns282 = parseNum("0")
var cns283 = anyStr("eof")
var cns284 = anyStr("err")
var cns285 = anyStr("unfinished string")
var cns286 = parseNum("16")
var cns287 = anyStr("get_hex")
var cns288 = parseNum("0x10FFFF")
var cns289 = anyStr("err")
var cns290 = anyStr("UTF-8 value too large")
var cns291 = anyStr("match")
var cns292 = anyStr("}")
var cns293 = anyStr("utf8")
var cns294 = anyStr("char")
var cns295 = anyStr("match")
var cns296 = anyStr("z")
var cns297 = anyStr("match")
var cns298 = anyStr("[ \x08\n\x0d\t\x0b]*")
var cns299 = anyStr("")
var cns300 = anyStr("err")
var cns301 = anyStr("invalid escape sequence")
var cns302 = anyStr("match")
var cns303 = anyStr(".")
var cns304 = anyStr("TK")
var cns305 = anyStr("STR")
var cns306 = anyStr("match")
var cns307 = anyStr("0[xX]")
var cns308 = anyStr("match")
var cns309 = anyStr("[a-fA-F%d]*%.?[a-fA-F%d]*")
var cns310 = anyStr("0x")
var cns311 = anyStr("0x.")
var cns312 = anyStr("err")
var cns313 = anyStr("malformed number")
var cns314 = anyStr("check")
var cns315 = anyStr("[pP]")
var cns316 = anyStr("match")
var cns317 = anyStr("[pP][%+%-]?")
var cns318 = anyStr("check")
var cns319 = anyStr("%d")
var cns320 = anyStr("match")
var cns321 = anyStr("%d+")
var cns322 = anyStr("err")
var cns323 = anyStr("malformed number")
var cns324 = anyStr("TK")
var cns325 = anyStr("NUM")
var cns326 = anyStr("check")
var cns327 = anyStr("%.?%d")
var cns328 = anyStr("match")
var cns329 = anyStr("%d*%.?%d*")
var cns330 = anyStr("check")
var cns331 = anyStr("[eE]")
var cns332 = anyStr("match")
var cns333 = anyStr("[eE][%+%-]?")
var cns334 = anyStr("check")
var cns335 = anyStr("%d")
var cns336 = anyStr("match")
var cns337 = anyStr("%d+")
var cns338 = anyStr("err")
var cns339 = anyStr("malformed number")
var cns340 = anyStr("TK")
var cns341 = anyStr("NUM")
var cns342 = anyStr("match")
var cns343 = anyStr("[_%a][_%w]*")
var cns344 = anyStr("find")
var cns345 = anyStr(" ")
var cns346 = anyStr(" ")
var cns347 = anyStr("TK")
var cns348 = anyStr("TK")
var cns349 = anyStr("NAME")
var cns350 = anyStr("pairs")
var cns351 = anyStr("match")
var cns352 = anyStr("TK")
var cns353 = anyStr("err")
var cns354 = anyStr("Unrecognized token ")
var cns355 = anyStr("check")
var cns356 = anyStr(".")
function function$7 (reg_0, reg_1) {
  var reg_12, reg_13, reg_18, reg_19, reg_30, reg_41, reg_42, reg_45, reg_46, reg_52, reg_64, reg_72, reg_73, reg_77, reg_78, reg_85, reg_86, reg_89, reg_95, reg_96, reg_103, reg_107, reg_108, reg_115, reg_116, reg_121, reg_122, reg_127, reg_142, reg_145, reg_146, reg_153, reg_154, reg_161, reg_162, reg_169, reg_170, reg_184, reg_185, reg_192, reg_193, reg_196, reg_197, reg_201, reg_207, reg_208, reg_215, reg_216, reg_221, reg_222, reg_234, reg_241, reg_246, reg_247, reg_252, reg_253, reg_260, reg_261, reg_269, reg_270, reg_273, reg_280, reg_281, reg_285, reg_298, reg_299, reg_304, reg_305, reg_314, reg_315, reg_320, reg_321, reg_328, reg_329, reg_335, reg_336, reg_342, reg_343, reg_349, reg_352, reg_353, reg_358, reg_359, reg_362, reg_366, reg_367, reg_373, reg_380, reg_381, reg_386, reg_387, reg_394, reg_395, reg_402, reg_403, reg_410, reg_411, reg_418, reg_419, reg_422, reg_425, reg_426, reg_431, reg_432, reg_439, reg_440, reg_446, reg_447, reg_454, reg_455, reg_462, reg_463, reg_470, reg_471, reg_478, reg_479, reg_482, reg_485, reg_486, reg_491, reg_492, reg_495, reg_497, reg_499, reg_500, reg_501, reg_510, reg_513, reg_514, reg_516, reg_519, reg_520, reg_525, reg_526, reg_528, reg_529, reg_530, reg_531, reg_532, reg_533, reg_535, reg_541, reg_542, reg_549, reg_550, reg_552, reg_553, reg_556, reg_557, reg_558, reg_561, reg_562, reg_568;
  var goto_53=false, goto_130=false, goto_154=false, goto_208=false, goto_228=false, goto_239=false, goto_286=false, goto_322=false, goto_408=false, goto_428=false, goto_438=false, goto_501=false, goto_542=false, goto_622=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  loop_4: while (tobool(ne(get(reg_1.a, cns202), cns203))) {
    reg_12 = get(reg_1.a, cns204);
    reg_13 = newStack();
    push(reg_13, cns205);
    reg_15 = call(reg_12, reg_13);
    reg_18 = get(reg_1.a, cns206);
    reg_19 = newStack();
    push(reg_19, cns207);
    push(reg_19, lua$true());
    goto_53 = !tobool(first(call(reg_18, reg_19)));
    if (!goto_53) {
      reg_30 = first(call(get(reg_1.a, cns208), newStack()));
      set(reg_1.a, cns209, reg_30);
      if (tobool(eq(get(reg_1.a, cns210), nil()))) {
        reg_41 = get(reg_1.a, cns211);
        reg_42 = newStack();
        push(reg_42, cns212);
        reg_44 = call(reg_41, reg_42);
      }
    }
    if ((goto_53 || false)) {
      goto_53 = false;
      if (true) break loop_4;
    }
  }
  reg_45 = newTable();
  reg_46 = cns213;
  set(reg_45, reg_46, get(get(reg_1.a, cns214), cns215));
  reg_52 = cns216;
  set(reg_45, reg_52, get(get(reg_1.a, cns217), cns218));
  set(reg_1.a, cns219, reg_45);
  if (tobool(get(reg_1.a, cns220))) {
    reg_64 = newStack();
    push(reg_64, nil());
    return reg_64;
  }
  reg_66 = nil();
  reg_72 = first(call(get(reg_1.a, cns221), newStack()));
  reg_73 = not(reg_72);
  if (tobool(reg_73)) {
    reg_77 = get(reg_1.a, cns222);
    reg_78 = newStack();
    push(reg_78, cns223);
    reg_73 = first(call(reg_77, reg_78));
  }
  if (tobool(reg_73)) {
    reg_85 = get(reg_1.a, cns224);
    reg_86 = newStack();
    push(reg_86, cns225);
    reg_89 = first(call(reg_85, reg_86));
    reg_72 = cns226;
    loop_2: while (tobool(lua$true())) {
      reg_95 = get(reg_1.a, cns227);
      reg_96 = newStack();
      push(reg_96, reg_89);
      push(reg_96, lua$true());
      goto_130 = !tobool(first(call(reg_95, reg_96)));
      if (!goto_130) {
        if (true) break loop_2;
      }
      if ((goto_130 || false)) {
        goto_130 = false;
        reg_103 = get(reg_1.a, cns228);
        if (!tobool(reg_103)) {
          reg_107 = get(reg_1.a, cns229);
          reg_108 = newStack();
          push(reg_108, cns230);
          reg_103 = first(call(reg_107, reg_108));
        }
        goto_154 = !tobool(reg_103);
        if (!goto_154) {
          reg_115 = get(reg_1.a, cns231);
          reg_116 = newStack();
          push(reg_116, cns232);
          reg_118 = call(reg_115, reg_116);
        }
        if ((goto_154 || false)) {
          goto_154 = false;
          reg_121 = get(reg_1.a, cns233);
          reg_122 = newStack();
          push(reg_122, cns234);
          goto_438 = !tobool(first(call(reg_121, reg_122)));
          if (!goto_438) {
            reg_127 = newTable();
            set(reg_127, cns235, cns236);
            set(reg_127, cns237, cns238);
            set(reg_127, cns239, cns240);
            set(reg_127, cns241, cns242);
            set(reg_127, cns243, cns244);
            set(reg_127, cns245, cns246);
            set(reg_127, cns247, cns248);
            reg_142 = nil();
            reg_145 = get(reg_1.a, cns249);
            reg_146 = newStack();
            push(reg_146, cns250);
            goto_208 = !tobool(first(call(reg_145, reg_146)));
            if (!goto_208) {
              reg_153 = get(reg_1.a, cns251);
              reg_154 = newStack();
              push(reg_154, cns252);
              reg_142 = get(reg_127, first(call(reg_153, reg_154)));
            }
            if ((goto_208 || false)) {
              goto_208 = false;
              reg_161 = get(reg_1.a, cns253);
              reg_162 = newStack();
              push(reg_162, cns254);
              goto_228 = !tobool(first(call(reg_161, reg_162)));
              if (!goto_228) {
                reg_169 = get(reg_1.a, cns255);
                reg_170 = newStack();
                push(reg_170, cns256);
                reg_142 = first(call(reg_169, reg_170));
              }
              if ((goto_228 || false)) {
                goto_228 = false;
                goto_239 = !tobool(first(call(get(reg_1.a, cns257), newStack())));
                if (!goto_239) {
                  reg_142 = cns258;
                }
                if ((goto_239 || false)) {
                  goto_239 = false;
                  reg_184 = get(reg_1.a, cns259);
                  reg_185 = newStack();
                  push(reg_185, cns260);
                  goto_286 = !tobool(first(call(reg_184, reg_185)));
                  if (!goto_286) {
                    reg_192 = get(reg_1.a, cns261);
                    reg_193 = newStack();
                    reg_196 = get(reg_1.a, cns262);
                    reg_197 = newStack();
                    push(reg_197, cns263);
                    append(reg_193, call(reg_196, reg_197));
                    reg_201 = first(call(reg_192, reg_193));
                    if (tobool(gt(reg_201, cns264))) {
                      reg_207 = get(reg_1.a, cns265);
                      reg_208 = newStack();
                      push(reg_208, cns266);
                      reg_210 = call(reg_207, reg_208);
                    }
                    reg_215 = get(get(reg_1.a, cns267), cns268);
                    reg_216 = newStack();
                    push(reg_216, reg_201);
                    reg_142 = first(call(reg_215, reg_216));
                  }
                  if ((goto_286 || false)) {
                    goto_286 = false;
                    reg_221 = get(reg_1.a, cns269);
                    reg_222 = newStack();
                    push(reg_222, cns270);
                    goto_322 = !tobool(first(call(reg_221, reg_222)));
                    if (!goto_322) {
                      reg_234 = mul(first(call(get(reg_1.a, cns271), newStack())), cns272);
                      reg_241 = add(reg_234, first(call(get(reg_1.a, cns273), newStack())));
                      reg_246 = get(get(reg_1.a, cns274), cns275);
                      reg_247 = newStack();
                      push(reg_247, reg_241);
                      reg_142 = first(call(reg_246, reg_247));
                    }
                    if ((goto_322 || false)) {
                      goto_322 = false;
                      reg_252 = get(reg_1.a, cns276);
                      reg_253 = newStack();
                      push(reg_253, cns277);
                      goto_408 = !tobool(first(call(reg_252, reg_253)));
                      if (!goto_408) {
                        reg_260 = get(reg_1.a, cns278);
                        reg_261 = newStack();
                        push(reg_261, cns279);
                        if (tobool(not(first(call(reg_260, reg_261))))) {
                          reg_269 = get(reg_1.a, cns280);
                          reg_270 = newStack();
                          push(reg_270, cns281);
                          reg_272 = call(reg_269, reg_270);
                        }
                        reg_273 = cns282;
                        loop_3: while (true) {
                          if (tobool(get(reg_1.a, cns283))) {
                            reg_280 = get(reg_1.a, cns284);
                            reg_281 = newStack();
                            push(reg_281, cns285);
                            reg_283 = call(reg_280, reg_281);
                          }
                          reg_285 = mul(reg_273, cns286);
                          reg_273 = add(reg_285, first(call(get(reg_1.a, cns287), newStack())));
                          if (tobool(gt(reg_273, cns288))) {
                            reg_298 = get(reg_1.a, cns289);
                            reg_299 = newStack();
                            push(reg_299, cns290);
                            reg_301 = call(reg_298, reg_299);
                          }
                          reg_304 = get(reg_1.a, cns291);
                          reg_305 = newStack();
                          push(reg_305, cns292);
                          if (tobool(first(call(reg_304, reg_305)))) break loop_3;
                        }
                        reg_314 = get(get(reg_1.a, cns293), cns294);
                        reg_315 = newStack();
                        push(reg_315, reg_273);
                        reg_142 = first(call(reg_314, reg_315));
                      }
                      if ((goto_408 || false)) {
                        goto_408 = false;
                        reg_320 = get(reg_1.a, cns295);
                        reg_321 = newStack();
                        push(reg_321, cns296);
                        goto_428 = !tobool(first(call(reg_320, reg_321)));
                        if (!goto_428) {
                          reg_328 = get(reg_1.a, cns297);
                          reg_329 = newStack();
                          push(reg_329, cns298);
                          reg_331 = call(reg_328, reg_329);
                          reg_142 = cns299;
                        }
                        if ((goto_428 || false)) {
                          goto_428 = false;
                          reg_335 = get(reg_1.a, cns300);
                          reg_336 = newStack();
                          push(reg_336, cns301);
                          reg_338 = call(reg_335, reg_336);
                        }
                      }
                    }
                  }
                }
              }
            }
            reg_72 = concat(reg_72, reg_142);
          }
          if ((goto_438 || false)) {
            goto_438 = false;
            reg_342 = get(reg_1.a, cns302);
            reg_343 = newStack();
            push(reg_343, cns303);
            reg_72 = concat(reg_72, first(call(reg_342, reg_343)));
          }
        }
      }
    }
  }
  if (tobool(reg_72)) {
    reg_349 = newStack();
    reg_352 = get(reg_1.a, cns304);
    reg_353 = newStack();
    push(reg_353, cns305);
    push(reg_353, reg_72);
    append(reg_349, call(reg_352, reg_353));
    return reg_349;
  }
  reg_358 = get(reg_1.a, cns306);
  reg_359 = newStack();
  push(reg_359, cns307);
  reg_362 = first(call(reg_358, reg_359));
  if (tobool(reg_362)) {
    reg_366 = get(reg_1.a, cns308);
    reg_367 = newStack();
    push(reg_367, cns309);
    reg_362 = concat(reg_362, first(call(reg_366, reg_367)));
    reg_373 = eq(reg_362, cns310);
    if (!tobool(reg_373)) {
      reg_373 = eq(reg_362, cns311);
    }
    goto_501 = !tobool(reg_373);
    if (!goto_501) {
      reg_380 = get(reg_1.a, cns312);
      reg_381 = newStack();
      push(reg_381, cns313);
      reg_383 = call(reg_380, reg_381);
    }
    if ((goto_501 || false)) {
      goto_501 = false;
      reg_386 = get(reg_1.a, cns314);
      reg_387 = newStack();
      push(reg_387, cns315);
      if (tobool(first(call(reg_386, reg_387)))) {
        reg_394 = get(reg_1.a, cns316);
        reg_395 = newStack();
        push(reg_395, cns317);
        reg_362 = concat(reg_362, first(call(reg_394, reg_395)));
        reg_402 = get(reg_1.a, cns318);
        reg_403 = newStack();
        push(reg_403, cns319);
        goto_542 = !tobool(first(call(reg_402, reg_403)));
        if (!goto_542) {
          reg_410 = get(reg_1.a, cns320);
          reg_411 = newStack();
          push(reg_411, cns321);
          reg_362 = concat(reg_362, first(call(reg_410, reg_411)));
        }
        if ((goto_542 || false)) {
          goto_542 = false;
          reg_418 = get(reg_1.a, cns322);
          reg_419 = newStack();
          push(reg_419, cns323);
          reg_421 = call(reg_418, reg_419);
        }
      }
    }
    reg_422 = newStack();
    reg_425 = get(reg_1.a, cns324);
    reg_426 = newStack();
    push(reg_426, cns325);
    push(reg_426, reg_362);
    append(reg_422, call(reg_425, reg_426));
    return reg_422;
  }
  reg_431 = get(reg_1.a, cns326);
  reg_432 = newStack();
  push(reg_432, cns327);
  if (tobool(first(call(reg_431, reg_432)))) {
    reg_439 = get(reg_1.a, cns328);
    reg_440 = newStack();
    push(reg_440, cns329);
    reg_362 = first(call(reg_439, reg_440));
    reg_446 = get(reg_1.a, cns330);
    reg_447 = newStack();
    push(reg_447, cns331);
    if (tobool(first(call(reg_446, reg_447)))) {
      reg_454 = get(reg_1.a, cns332);
      reg_455 = newStack();
      push(reg_455, cns333);
      reg_362 = concat(reg_362, first(call(reg_454, reg_455)));
      reg_462 = get(reg_1.a, cns334);
      reg_463 = newStack();
      push(reg_463, cns335);
      goto_622 = !tobool(first(call(reg_462, reg_463)));
      if (!goto_622) {
        reg_470 = get(reg_1.a, cns336);
        reg_471 = newStack();
        push(reg_471, cns337);
        reg_362 = concat(reg_362, first(call(reg_470, reg_471)));
      }
      if ((goto_622 || false)) {
        goto_622 = false;
        reg_478 = get(reg_1.a, cns338);
        reg_479 = newStack();
        push(reg_479, cns339);
        reg_481 = call(reg_478, reg_479);
      }
    }
    reg_482 = newStack();
    reg_485 = get(reg_1.a, cns340);
    reg_486 = newStack();
    push(reg_486, cns341);
    push(reg_486, reg_362);
    append(reg_482, call(reg_485, reg_486));
    return reg_482;
  }
  reg_491 = get(reg_1.a, cns342);
  reg_492 = newStack();
  push(reg_492, cns343);
  reg_495 = first(call(reg_491, reg_492));
  if (tobool(reg_495)) {
    reg_497 = reg_1.b;
    reg_499 = get(reg_497, cns344);
    reg_500 = newStack();
    push(reg_500, reg_497);
    reg_501 = cns345;
    push(reg_500, concat(reg_501, concat(reg_495, cns346)));
    if (tobool(ne(first(call(reg_499, reg_500)), nil()))) {
      reg_510 = newStack();
      reg_513 = get(reg_1.a, cns347);
      reg_514 = newStack();
      push(reg_514, reg_495);
      append(reg_510, call(reg_513, reg_514));
      return reg_510;
    }
    reg_516 = newStack();
    reg_519 = get(reg_1.a, cns348);
    reg_520 = newStack();
    push(reg_520, cns349);
    push(reg_520, reg_495);
    append(reg_516, call(reg_519, reg_520));
    return reg_516;
  }
  reg_525 = get(reg_1.a, cns350);
  reg_526 = newStack();
  push(reg_526, reg_1.c);
  reg_528 = call(reg_525, reg_526);
  reg_529 = next(reg_528);
  reg_530 = next(reg_528);
  reg_531 = next(reg_528);
  loop_1: while (true) {
    reg_532 = newStack();
    push(reg_532, reg_530);
    push(reg_532, reg_531);
    reg_533 = call(reg_529, reg_532);
    reg_531 = next(reg_533);
    reg_535 = next(reg_533);
    if (tobool(eq(reg_531, nil()))) break loop_1;
    reg_541 = get(reg_1.a, cns351);
    reg_542 = newStack();
    push(reg_542, reg_535);
    push(reg_542, lua$true());
    if (tobool(first(call(reg_541, reg_542)))) {
      reg_549 = get(reg_1.a, cns352);
      reg_550 = newStack();
      push(reg_550, reg_535);
      reg_552 = first(call(reg_549, reg_550));
      reg_553 = newStack();
      push(reg_553, reg_552);
      return reg_553;
    }
  }
  reg_556 = get(reg_1.a, cns353);
  reg_557 = newStack();
  reg_558 = cns354;
  reg_561 = get(reg_1.a, cns355);
  reg_562 = newStack();
  push(reg_562, cns356);
  push(reg_557, concat(reg_558, first(call(reg_561, reg_562))));
  reg_567 = call(reg_556, reg_557);
  reg_568 = newStack();
  return reg_568;
}
var cns357 = anyStr("lex")
var cns358 = anyStr("Lexer")
var cns359 = anyStr("next")
var record_33 = function (val) { this.val = val; }
var cns360 = anyStr("xpcall")
var record_34 = function (val) { this.val = val; }
function function$8 (reg_0, reg_1) {
  var reg_5, reg_6;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  push(reg_6, lua$true());
  append(reg_6, call(reg_5, newStack()));
  return reg_6;
}
var cns361 = anyStr("xpcall")
var cns362 = anyStr("xpcall")
var cns363 = anyStr("lex")
var cns364 = anyStr("Lexer")
var cns365 = anyStr("error")
var cns366 = anyStr("debug")
var cns367 = anyStr("traceback")
var cns368 = anyStr("\x1b[31mFATAL\x1b[39m ")
function function$9 (reg_0, reg_1) {
  var reg_2, reg_5, reg_18, reg_19, reg_24;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  if (tobool(eq(get(get(reg_2.a, cns364), cns365), nil()))) {
    reg_18 = get(get(reg_2.a, cns366), cns367);
    reg_19 = newStack();
    push(reg_19, concat(cns368, reg_5));
    reg_1.b = first(call(reg_18, reg_19));
  }
  reg_24 = newStack();
  return reg_24;
}
var cns369 = anyStr("status")
var cns370 = anyStr("tk")
var cns371 = anyStr("error")
var cns372 = anyStr("status")
var cns373 = anyStr("tk")
var cns374 = anyStr("Lexer")
var cns375 = anyStr("error")
var cns376 = anyStr("tk")
var cns377 = anyStr("error")
var cns378 = anyStr("tk")
function Lexer_next (reg_0, reg_1) {
  var reg_3, reg_11, reg_16, reg_17, reg_23, reg_24, reg_27, reg_34, reg_35, reg_42, reg_52, reg_59, reg_60, reg_65;
  var goto_59=false, goto_75=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_3.b = nil();
  if (tobool(not(get(reg_1.a, cns360)))) {
    reg_11 = anyFn((function (a) { return function$8(a,this) }).bind(reg_3));
    set(reg_1.a, cns361, reg_11);
  }
  reg_16 = get(reg_1.a, cns362);
  reg_17 = newStack();
  push(reg_17, get(reg_1.a, cns363));
  push(reg_17, anyFn((function (a) { return function$9(a,this) }).bind(reg_3)));
  reg_23 = call(reg_16, reg_17);
  reg_24 = next(reg_23);
  set(reg_1.a, cns369, reg_24);
  reg_27 = next(reg_23);
  set(reg_1.a, cns370, reg_27);
  if (tobool(reg_3.b)) {
    reg_34 = get(reg_1.a, cns371);
    reg_35 = newStack();
    push(reg_35, reg_3.b);
    reg_37 = call(reg_34, reg_35);
  }
  goto_59 = !tobool(get(reg_1.a, cns372));
  if (!goto_59) {
    reg_42 = newStack();
    push(reg_42, get(reg_1.a, cns373));
    return reg_42;
  }
  if ((goto_59 || false)) {
    goto_59 = false;
    goto_75 = !tobool(get(get(reg_1.a, cns374), cns375));
    if (!goto_75) {
      reg_52 = newStack();
      push(reg_52, nil());
      push(reg_52, get(reg_1.a, cns376));
      return reg_52;
    }
    if ((goto_75 || false)) {
      goto_75 = false;
      reg_59 = get(reg_1.a, cns377);
      reg_60 = newStack();
      push(reg_60, get(reg_1.a, cns378));
      reg_64 = call(reg_59, reg_60);
    }
  }
  reg_65 = newStack();
  return reg_65;
}
var cns379 = anyStr("Lexer")
var cns380 = anyStr("tokens")
var cns381 = anyStr("Lexer")
var cns382 = anyStr("next")
var cns383 = anyStr("table")
var cns384 = anyStr("insert")
var cns385 = anyStr("Lexer")
var cns386 = anyStr("next")
var cns387 = anyStr("Lexer")
var cns388 = anyStr("error")
function Lexer_tokens (reg_0, reg_1) {
  var reg_4, reg_12, reg_20, reg_21, reg_38, reg_39;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newTable();
  reg_12 = first(call(get(get(reg_1.a, cns381), cns382), newStack()));
  loop_1: while (tobool(ne(reg_12, nil()))) {
    reg_20 = get(get(reg_1.a, cns383), cns384);
    reg_21 = newStack();
    push(reg_21, reg_4);
    push(reg_21, reg_12);
    reg_22 = call(reg_20, reg_21);
    reg_12 = first(call(get(get(reg_1.a, cns385), cns386), newStack()));
  }
  if (tobool(not(get(get(reg_1.a, cns387), cns388)))) {
    reg_38 = newStack();
    push(reg_38, reg_4);
    return reg_38;
  }
  reg_39 = newStack();
  return reg_39;
}
var cns389 = anyStr("Lexer")
function lua_parser_lexer$lua_main (reg_0) {
  var reg_1, reg_2, reg_5, reg_6, reg_8, reg_13, reg_18, reg_19, reg_23, reg_27, reg_31, reg_35, reg_39, reg_43, reg_111, reg_115, reg_119, reg_124, reg_125, reg_130, reg_131, reg_134;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1, c: reg_1};
  reg_2.a = reg_0;
  reg_5 = get(reg_2.a, cns5);
  reg_6 = newStack();
  push(reg_6, newTable());
  reg_8 = newTable();
  set(reg_8, cns6, reg_2.a);
  push(reg_6, reg_8);
  reg_2.a = first(call(reg_5, reg_6));
  reg_13 = newTable();
  set(reg_2.a, cns7, reg_13);
  reg_18 = get(reg_2.a, cns8);
  reg_19 = cns9;
  set(reg_18, reg_19, anyFn((function (a) { return Lexer_open(a,this) }).bind(reg_2)));
  reg_23 = anyFn((function (a) { return lua_parser_lexer$function(a,this) }).bind(reg_2));
  set(reg_2.a, cns37, reg_23);
  reg_27 = anyFn((function (a) { return function$1(a,this) }).bind(reg_2));
  set(reg_2.a, cns44, reg_27);
  reg_31 = anyFn((function (a) { return function$2(a,this) }).bind(reg_2));
  set(reg_2.a, cns73, reg_31);
  reg_35 = anyFn((function (a) { return function$3(a,this) }).bind(reg_2));
  set(reg_2.a, cns80, reg_35);
  reg_39 = anyFn((function (a) { return function$4(a,this) }).bind(reg_2));
  set(reg_2.a, cns89, reg_39);
  reg_2.b = cns90;
  reg_43 = newTable();
  set(reg_43, cns91, cns92);
  set(reg_43, cns93, cns94);
  set(reg_43, cns95, cns96);
  set(reg_43, cns97, cns98);
  set(reg_43, cns99, cns100);
  set(reg_43, cns101, cns102);
  set(reg_43, cns103, cns104);
  set(reg_43, cns105, cns106);
  set(reg_43, cns107, cns108);
  set(reg_43, cns109, cns110);
  set(reg_43, cns111, cns112);
  set(reg_43, cns113, cns114);
  set(reg_43, cns115, cns116);
  set(reg_43, cns117, cns118);
  set(reg_43, cns119, cns120);
  set(reg_43, cns121, cns122);
  set(reg_43, cns123, cns124);
  set(reg_43, cns125, cns126);
  set(reg_43, cns127, cns128);
  set(reg_43, cns129, cns130);
  set(reg_43, cns131, cns132);
  set(reg_43, cns133, cns134);
  set(reg_43, cns135, cns136);
  set(reg_43, cns137, cns138);
  set(reg_43, cns139, cns140);
  set(reg_43, cns141, cns142);
  set(reg_43, cns143, cns144);
  set(reg_43, cns145, cns146);
  set(reg_43, cns147, cns148);
  set(reg_43, cns149, cns150);
  set(reg_43, cns151, cns152);
  set(reg_43, cns153, cns154);
  set(reg_43, cns155, cns156);
  reg_2.c = reg_43;
  reg_111 = anyFn((function (a) { return function$5(a,this) }).bind(reg_2));
  set(reg_2.a, cns187, reg_111);
  reg_115 = anyFn((function (a) { return function$6(a,this) }).bind(reg_2));
  set(reg_2.a, cns201, reg_115);
  reg_119 = anyFn((function (a) { return function$7(a,this) }).bind(reg_2));
  set(reg_2.a, cns357, reg_119);
  reg_124 = get(reg_2.a, cns358);
  reg_125 = cns359;
  set(reg_124, reg_125, anyFn((function (a) { return Lexer_next(a,this) }).bind(reg_2)));
  reg_130 = get(reg_2.a, cns379);
  reg_131 = cns380;
  set(reg_130, reg_131, anyFn((function (a) { return Lexer_tokens(a,this) }).bind(reg_2)));
  reg_134 = newStack();
  push(reg_134, get(reg_2.a, cns389));
  return reg_134;
}
var cns390 = anyStr("Lexer")
var cns391 = anyStr("Parser")
var cns392 = anyStr("Parser")
var cns393 = anyStr("open")
var record_35 = function (val) { this.val = val; }
var cns394 = anyStr("Lexer")
var cns395 = anyStr("open")
var cns396 = anyStr("Lexer")
var cns397 = anyStr("next")
var cns398 = anyStr("token")
var cns399 = anyStr("Parser")
var cns400 = anyStr("error")
function Parser_open (reg_0, reg_1) {
  var reg_4, reg_9, reg_10, reg_14, reg_16, reg_17, reg_19, reg_27;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_9 = get(get(reg_1.a, cns394), cns395);
  reg_10 = newStack();
  push(reg_10, reg_4);
  reg_11 = call(reg_9, reg_10);
  reg_14 = get(reg_1.a, cns396);
  reg_16 = get(reg_14, cns397);
  reg_17 = newStack();
  push(reg_17, reg_14);
  reg_19 = first(call(reg_16, reg_17));
  set(reg_1.a, cns398, reg_19);
  set(get(reg_1.a, cns399), cns400, nil());
  reg_27 = newStack();
  return reg_27;
}
var cns401 = anyStr("<eof>")
var cns402 = anyStr("token")
var cns403 = anyStr("token")
var cns404 = anyStr("line")
var cns405 = anyStr(":")
var cns406 = anyStr("token")
var cns407 = anyStr("column")
var cns408 = anyStr("Parser")
var cns409 = anyStr("error")
var cns410 = anyStr(": ")
var cns411 = anyStr("error")
var cns412 = anyStr("Parser")
var cns413 = anyStr("error")
function lua_parser_parser$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_14, reg_15, reg_25, reg_26, reg_32, reg_33, reg_40;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = cns401;
  if (tobool(get(reg_1.a, cns402))) {
    reg_14 = get(get(reg_1.a, cns403), cns404);
    reg_15 = cns405;
    reg_5 = concat(reg_14, concat(reg_15, get(get(reg_1.a, cns406), cns407)));
  }
  reg_25 = get(reg_1.a, cns408);
  reg_26 = cns409;
  set(reg_25, reg_26, concat(reg_5, concat(cns410, reg_4)));
  reg_32 = get(reg_1.a, cns411);
  reg_33 = newStack();
  push(reg_33, get(get(reg_1.a, cns412), cns413));
  reg_39 = call(reg_32, reg_33);
  reg_40 = newStack();
  return reg_40;
}
var cns414 = anyStr("err")
var cns415 = anyStr("token")
var cns416 = anyStr("_lookahead")
var cns417 = anyStr("Lexer")
var cns418 = anyStr("next")
var cns419 = anyStr("token")
var cns420 = anyStr("_lookahead")
var cns421 = anyStr("Lexer")
var cns422 = anyStr("error")
var cns423 = anyStr("Parser")
var cns424 = anyStr("error")
var cns425 = anyStr("Lexer")
var cns426 = anyStr("error")
var cns427 = anyStr("error")
var cns428 = anyStr("Parser")
var cns429 = anyStr("error")
function function$10 (reg_0, reg_1) {
  var reg_6, reg_9, reg_21, reg_32, reg_33, reg_41, reg_42, reg_49;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = get(reg_1.a, cns415);
  reg_9 = get(reg_1.a, cns416);
  if (!tobool(reg_9)) {
    reg_9 = first(call(get(get(reg_1.a, cns417), cns418), newStack()));
  }
  set(reg_1.a, cns419, reg_9);
  reg_21 = nil();
  set(reg_1.a, cns420, reg_21);
  if (tobool(get(get(reg_1.a, cns421), cns422))) {
    reg_32 = get(reg_1.a, cns423);
    reg_33 = cns424;
    set(reg_32, reg_33, get(get(reg_1.a, cns425), cns426));
    reg_41 = get(reg_1.a, cns427);
    reg_42 = newStack();
    push(reg_42, get(get(reg_1.a, cns428), cns429));
    reg_48 = call(reg_41, reg_42);
  }
  reg_49 = newStack();
  push(reg_49, reg_6);
  return reg_49;
}
var cns430 = anyStr("next")
var cns431 = anyStr("token")
function lua$false () {
  var reg_1;
  reg_1 = false;
  return reg_1;
}
var cns432 = parseNum("1")
function copy (reg_0) {
  var reg_3;
  reg_3 = {a: reg_0.a, b: reg_0.b};
  return reg_3;
}
function table_append (reg_0, reg_1, reg_2) {
  var reg_3, reg_4;
  reg_3 = getTable(reg_0);
  reg_4 = getInt(reg_1);
  loop_1: while (more(reg_2)) {
    lua$set(reg_3, anyInt(reg_4), next(reg_2));
    reg_4 = (reg_4 + 1);
  }
  return;
}
var cns433 = anyStr("ipairs")
var cns434 = anyStr("token")
var cns435 = anyStr("type")
function function$11 (reg_0, reg_1) {
  var reg_9, reg_11, reg_16, reg_17, reg_18, reg_19, reg_20, reg_21, reg_22, reg_23, reg_25, reg_36, reg_38;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  if (tobool(not(get(reg_1.a, cns431)))) {
    reg_9 = newStack();
    push(reg_9, lua$false());
    return reg_9;
  }
  reg_11 = newTable();
  table_append(reg_11, cns432, copy(reg_0));
  reg_16 = get(reg_1.a, cns433);
  reg_17 = newStack();
  push(reg_17, reg_11);
  reg_18 = call(reg_16, reg_17);
  reg_19 = next(reg_18);
  reg_20 = next(reg_18);
  reg_21 = next(reg_18);
  loop_1: while (true) {
    reg_22 = newStack();
    push(reg_22, reg_20);
    push(reg_22, reg_21);
    reg_23 = call(reg_19, reg_22);
    reg_21 = next(reg_23);
    reg_25 = next(reg_23);
    if (tobool(eq(reg_21, nil()))) break loop_1;
    if (tobool(eq(get(get(reg_1.a, cns434), cns435), reg_25))) {
      reg_36 = newStack();
      push(reg_36, lua$true());
      return reg_36;
    }
  }
  reg_38 = newStack();
  push(reg_38, lua$false());
  return reg_38;
}
var cns436 = anyStr("check")
var cns437 = anyStr("token")
var cns438 = anyStr("check")
function function$12 (reg_0, reg_1) {
  var reg_4, reg_7, reg_11, reg_12;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newStack();
  reg_7 = get(reg_1.a, cns437);
  if (tobool(reg_7)) {
    reg_11 = get(reg_1.a, cns438);
    reg_12 = newStack();
    append(reg_12, reg_0);
    reg_7 = not(first(call(reg_11, reg_12)));
  }
  push(reg_4, reg_7);
  return reg_4;
}
var cns439 = anyStr("check_not")
var cns440 = anyStr("check")
var cns441 = anyStr("next")
function function$13 (reg_0, reg_1) {
  var reg_6, reg_7, reg_11, reg_17, reg_19;
  var goto_20=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = get(reg_1.a, cns440);
  reg_7 = newStack();
  append(reg_7, reg_0);
  goto_20 = !tobool(first(call(reg_6, reg_7)));
  if (!goto_20) {
    reg_11 = newStack();
    append(reg_11, call(get(reg_1.a, cns441), newStack()));
    return reg_11;
  }
  if ((goto_20 || false)) {
    goto_20 = false;
    reg_17 = newStack();
    push(reg_17, nil());
    return reg_17;
  }
  reg_19 = newStack();
  return reg_19;
}
var cns442 = anyStr("try")
var cns443 = anyStr("try")
var cns444 = anyStr("err")
var cns445 = anyStr("table")
var cns446 = anyStr("concat")
var cns447 = parseNum("1")
var cns448 = anyStr(" or ")
var cns449 = anyStr("lower")
var cns450 = anyStr(" expected")
function function$14 (reg_0, reg_1) {
  var reg_6, reg_7, reg_9, reg_11, reg_14, reg_15, reg_20, reg_21, reg_22, reg_27, reg_29, reg_30, reg_36;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = get(reg_1.a, cns443);
  reg_7 = newStack();
  append(reg_7, reg_0);
  reg_9 = first(call(reg_6, reg_7));
  if (tobool(reg_9)) {
    reg_11 = newStack();
    push(reg_11, reg_9);
    return reg_11;
  }
  reg_14 = get(reg_1.a, cns444);
  reg_15 = newStack();
  reg_20 = get(get(reg_1.a, cns445), cns446);
  reg_21 = newStack();
  reg_22 = newTable();
  table_append(reg_22, cns447, copy(reg_0));
  push(reg_21, reg_22);
  push(reg_21, cns448);
  reg_27 = first(call(reg_20, reg_21));
  reg_29 = get(reg_27, cns449);
  reg_30 = newStack();
  push(reg_30, reg_27);
  push(reg_15, concat(first(call(reg_29, reg_30)), cns450));
  reg_35 = call(reg_14, reg_15);
  reg_36 = newStack();
  return reg_36;
}
var cns451 = anyStr("expect")
var cns452 = anyStr("try")
var cns453 = anyStr("err")
var cns454 = anyStr(" expected (to close ")
var cns455 = anyStr("type")
var cns456 = anyStr(" at line ")
var cns457 = anyStr("line")
var cns458 = anyStr(")")
function function$15 (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_11, reg_13, reg_16, reg_17, reg_18, reg_20, reg_21, reg_31;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = get(reg_1.a, cns452);
  reg_9 = newStack();
  push(reg_9, reg_4);
  reg_11 = first(call(reg_8, reg_9));
  if (tobool(reg_11)) {
    reg_13 = newStack();
    push(reg_13, reg_11);
    return reg_13;
  }
  reg_16 = get(reg_1.a, cns453);
  reg_17 = newStack();
  reg_18 = cns454;
  reg_20 = get(reg_5, cns455);
  reg_21 = cns456;
  push(reg_17, concat(reg_4, concat(reg_18, concat(reg_20, concat(reg_21, concat(get(reg_5, cns457), cns458))))));
  reg_30 = call(reg_16, reg_17);
  reg_31 = newStack();
  return reg_31;
}
var cns459 = anyStr("match")
var cns460 = anyStr("expect")
var cns461 = anyStr("NAME")
var cns462 = anyStr("value")
function function$16 (reg_0, reg_1) {
  var reg_6, reg_7, reg_10, reg_12, reg_15, reg_17;
  var goto_18=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = get(reg_1.a, cns460);
  reg_7 = newStack();
  push(reg_7, cns461);
  reg_10 = first(call(reg_6, reg_7));
  goto_18 = !tobool(reg_10);
  if (!goto_18) {
    reg_12 = newStack();
    push(reg_12, get(reg_10, cns462));
    return reg_12;
  }
  if ((goto_18 || false)) {
    goto_18 = false;
    reg_15 = newStack();
    push(reg_15, nil());
    return reg_15;
  }
  reg_17 = newStack();
  return reg_17;
}
var cns463 = anyStr("get_name")
var cns464 = anyStr("_lookahead")
var cns465 = anyStr("Lexer")
var cns466 = anyStr("next")
var cns467 = anyStr("_lookahead")
var cns468 = anyStr("_lookahead")
function function$17 (reg_0, reg_1) {
  var reg_6, reg_18;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = get(reg_1.a, cns464);
  if (!tobool(reg_6)) {
    reg_6 = first(call(get(get(reg_1.a, cns465), cns466), newStack()));
  }
  set(reg_1.a, cns467, reg_6);
  reg_18 = newStack();
  push(reg_18, get(reg_1.a, cns468));
  return reg_18;
}
var cns469 = anyStr("lookahead")
var record_36 = function (val) { this.val = val; }
var cns470 = parseNum("0")
var cns471 = parseNum("0")
var cns472 = anyStr("token")
var cns473 = anyStr("token")
var cns474 = anyStr("line")
var cns475 = anyStr("token")
var cns476 = anyStr("column")
var record_37 = function (val) { this.val = val; }
var cns477 = anyStr("line")
var cns478 = anyStr("column")
function function$19 (reg_0, reg_1) {
  var reg_5, reg_10;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  set(reg_5, cns477, reg_1.b);
  set(reg_5, cns478, reg_1.c);
  reg_10 = newStack();
  push(reg_10, reg_5);
  return reg_10;
}
function function$18 (reg_0, reg_1) {
  var reg_2, reg_3, reg_20;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = cns470;
  reg_3.c = cns471;
  if (tobool(get(reg_1.a, cns472))) {
    reg_3.b = get(get(reg_1.a, cns473), cns474);
    reg_3.c = get(get(reg_1.a, cns475), cns476);
  }
  reg_20 = newStack();
  push(reg_20, anyFn((function (a) { return function$19(a,this) }).bind(reg_3)));
  return reg_20;
}
var cns479 = anyStr("get_pos")
var cns480 = anyStr("Parser")
var cns481 = anyStr("singlevar")
var cns482 = anyStr("get_pos")
var cns483 = anyStr("get_name")
var cns484 = anyStr("type")
var cns485 = anyStr("var")
var cns486 = anyStr("name")
function Parser_singlevar (reg_0, reg_1) {
  var reg_9, reg_15, reg_16, reg_17, reg_18;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_9 = first(call(get(reg_1.a, cns482), newStack()));
  reg_15 = first(call(get(reg_1.a, cns483), newStack()));
  reg_16 = newStack();
  reg_17 = newStack();
  reg_18 = newTable();
  set(reg_18, cns484, cns485);
  set(reg_18, cns486, reg_15);
  push(reg_17, reg_18);
  append(reg_16, call(reg_9, reg_17));
  return reg_16;
}
var cns487 = anyStr("get_pos")
var cns488 = anyStr("expect")
var cns489 = anyStr("{")
var cns490 = anyStr("check_not")
var cns491 = anyStr("}")
var cns492 = anyStr("try")
var cns493 = anyStr("[")
var cns494 = anyStr("Parser")
var cns495 = anyStr("expr")
var cns496 = anyStr("expect")
var cns497 = anyStr("]")
var cns498 = anyStr("expect")
var cns499 = anyStr("=")
var cns500 = anyStr("Parser")
var cns501 = anyStr("expr")
var cns502 = anyStr("table")
var cns503 = anyStr("insert")
var cns504 = anyStr("type")
var cns505 = anyStr("indexitem")
var cns506 = anyStr("key")
var cns507 = anyStr("value")
var cns508 = anyStr("check")
var cns509 = anyStr("NAME")
var cns510 = anyStr("lookahead")
var cns511 = anyStr("lookahead")
var cns512 = anyStr("type")
var cns513 = anyStr("=")
var cns514 = anyStr("get_name")
var cns515 = anyStr("expect")
var cns516 = anyStr("=")
var cns517 = anyStr("Parser")
var cns518 = anyStr("expr")
var cns519 = anyStr("table")
var cns520 = anyStr("insert")
var cns521 = anyStr("type")
var cns522 = anyStr("fielditem")
var cns523 = anyStr("key")
var cns524 = anyStr("value")
var cns525 = anyStr("Parser")
var cns526 = anyStr("expr")
var cns527 = anyStr("table")
var cns528 = anyStr("insert")
var cns529 = anyStr("type")
var cns530 = anyStr("item")
var cns531 = anyStr("value")
var cns532 = anyStr("try")
var cns533 = anyStr(",")
var cns534 = anyStr(";")
var cns535 = anyStr("match")
var cns536 = anyStr("}")
var cns537 = anyStr("type")
var cns538 = anyStr("constructor")
var cns539 = anyStr("items")
function function$20 (reg_0, reg_1) {
  var reg_9, reg_12, reg_13, reg_16, reg_17, reg_20, reg_21, reg_28, reg_29, reg_41, reg_44, reg_45, reg_50, reg_51, reg_61, reg_66, reg_67, reg_68, reg_76, reg_77, reg_80, reg_105, reg_108, reg_109, reg_119, reg_124, reg_125, reg_126, reg_139, reg_144, reg_145, reg_146, reg_153, reg_154, reg_160, reg_161, reg_164, reg_165, reg_166;
  var goto_85=false, goto_156=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_9 = first(call(get(reg_1.a, cns487), newStack()));
  reg_12 = get(reg_1.a, cns488);
  reg_13 = newStack();
  push(reg_13, cns489);
  reg_16 = first(call(reg_12, reg_13));
  reg_17 = newTable();
  loop_1: while (true) {
    reg_20 = get(reg_1.a, cns490);
    reg_21 = newStack();
    push(reg_21, cns491);
    if (!tobool(first(call(reg_20, reg_21)))) break loop_1;
    reg_28 = get(reg_1.a, cns492);
    reg_29 = newStack();
    push(reg_29, cns493);
    goto_85 = !tobool(first(call(reg_28, reg_29)));
    if (!goto_85) {
      reg_41 = first(call(get(get(reg_1.a, cns494), cns495), newStack()));
      reg_44 = get(reg_1.a, cns496);
      reg_45 = newStack();
      push(reg_45, cns497);
      reg_47 = call(reg_44, reg_45);
      reg_50 = get(reg_1.a, cns498);
      reg_51 = newStack();
      push(reg_51, cns499);
      reg_53 = call(reg_50, reg_51);
      reg_61 = first(call(get(get(reg_1.a, cns500), cns501), newStack()));
      reg_66 = get(get(reg_1.a, cns502), cns503);
      reg_67 = newStack();
      push(reg_67, reg_17);
      reg_68 = newTable();
      set(reg_68, cns504, cns505);
      set(reg_68, cns506, reg_41);
      set(reg_68, cns507, reg_61);
      push(reg_67, reg_68);
      reg_73 = call(reg_66, reg_67);
    }
    if ((goto_85 || false)) {
      goto_85 = false;
      reg_76 = get(reg_1.a, cns508);
      reg_77 = newStack();
      push(reg_77, cns509);
      reg_80 = first(call(reg_76, reg_77));
      if (tobool(reg_80)) {
        reg_80 = first(call(get(reg_1.a, cns510), newStack()));
      }
      if (tobool(reg_80)) {
        reg_80 = eq(get(first(call(get(reg_1.a, cns511), newStack())), cns512), cns513);
      }
      goto_156 = !tobool(reg_80);
      if (!goto_156) {
        reg_105 = first(call(get(reg_1.a, cns514), newStack()));
        reg_108 = get(reg_1.a, cns515);
        reg_109 = newStack();
        push(reg_109, cns516);
        reg_111 = call(reg_108, reg_109);
        reg_119 = first(call(get(get(reg_1.a, cns517), cns518), newStack()));
        reg_124 = get(get(reg_1.a, cns519), cns520);
        reg_125 = newStack();
        push(reg_125, reg_17);
        reg_126 = newTable();
        set(reg_126, cns521, cns522);
        set(reg_126, cns523, reg_105);
        set(reg_126, cns524, reg_119);
        push(reg_125, reg_126);
        reg_131 = call(reg_124, reg_125);
      }
      if ((goto_156 || false)) {
        goto_156 = false;
        reg_139 = first(call(get(get(reg_1.a, cns525), cns526), newStack()));
        reg_144 = get(get(reg_1.a, cns527), cns528);
        reg_145 = newStack();
        push(reg_145, reg_17);
        reg_146 = newTable();
        set(reg_146, cns529, cns530);
        set(reg_146, cns531, reg_139);
        push(reg_145, reg_146);
        reg_150 = call(reg_144, reg_145);
      }
    }
    reg_153 = get(reg_1.a, cns532);
    reg_154 = newStack();
    push(reg_154, cns533);
    push(reg_154, cns534);
    reg_157 = call(reg_153, reg_154);
  }
  reg_160 = get(reg_1.a, cns535);
  reg_161 = newStack();
  push(reg_161, cns536);
  push(reg_161, reg_16);
  reg_163 = call(reg_160, reg_161);
  reg_164 = newStack();
  reg_165 = newStack();
  reg_166 = newTable();
  set(reg_166, cns537, cns538);
  set(reg_166, cns539, reg_17);
  push(reg_165, reg_166);
  append(reg_164, call(reg_9, reg_165));
  return reg_164;
}
var cns540 = anyStr("constructor")
var cns541 = anyStr("line")
var cns542 = anyStr("column")
var cns543 = anyStr("try")
var cns544 = anyStr(":")
var cns545 = anyStr("token")
var cns546 = anyStr("token")
var cns547 = anyStr("line")
var cns548 = anyStr("token")
var cns549 = anyStr("column")
var cns550 = anyStr("get_name")
var cns551 = anyStr("check")
var cns552 = anyStr("STR")
var cns553 = parseNum("1")
var cns554 = anyStr("Parser")
var cns555 = anyStr("simpleexp")
var cns556 = anyStr("check")
var cns557 = anyStr("{")
var cns558 = parseNum("1")
var cns559 = anyStr("constructor")
var cns560 = anyStr("expect")
var cns561 = anyStr("(")
var cns562 = anyStr("check_not")
var cns563 = anyStr(")")
var cns564 = anyStr("Parser")
var cns565 = anyStr("explist")
var cns566 = anyStr("match")
var cns567 = anyStr(")")
var cns568 = anyStr("type")
var cns569 = anyStr("call")
var cns570 = anyStr("base")
var cns571 = anyStr("values")
var cns572 = anyStr("key")
var cns573 = anyStr("line")
var cns574 = anyStr("column")
function function$21 (reg_0, reg_1) {
  var reg_4, reg_6, reg_8, reg_10, reg_11, reg_14, reg_15, reg_42, reg_43, reg_48, reg_49, reg_59, reg_60, reg_65, reg_66, reg_74, reg_75, reg_78, reg_81, reg_82, reg_97, reg_98, reg_101, reg_102;
  var goto_68=false, goto_88=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = newTable();
  reg_8 = get(reg_4, cns541);
  reg_10 = get(reg_4, cns542);
  reg_11 = nil();
  reg_14 = get(reg_1.a, cns543);
  reg_15 = newStack();
  push(reg_15, cns544);
  if (tobool(first(call(reg_14, reg_15)))) {
    if (tobool(get(reg_1.a, cns545))) {
      reg_8 = get(get(reg_1.a, cns546), cns547);
      reg_10 = get(get(reg_1.a, cns548), cns549);
    }
    reg_11 = first(call(get(reg_1.a, cns550), newStack()));
  }
  reg_42 = get(reg_1.a, cns551);
  reg_43 = newStack();
  push(reg_43, cns552);
  goto_68 = !tobool(first(call(reg_42, reg_43)));
  if (!goto_68) {
    reg_48 = newTable();
    reg_49 = cns553;
    table_append(reg_48, reg_49, call(get(get(reg_1.a, cns554), cns555), newStack()));
    reg_6 = reg_48;
  }
  if ((goto_68 || false)) {
    goto_68 = false;
    reg_59 = get(reg_1.a, cns556);
    reg_60 = newStack();
    push(reg_60, cns557);
    goto_88 = !tobool(first(call(reg_59, reg_60)));
    if (!goto_88) {
      reg_65 = newTable();
      reg_66 = cns558;
      table_append(reg_65, reg_66, call(get(reg_1.a, cns559), newStack()));
      reg_6 = reg_65;
    }
    if ((goto_88 || false)) {
      goto_88 = false;
      reg_74 = get(reg_1.a, cns560);
      reg_75 = newStack();
      push(reg_75, cns561);
      reg_78 = first(call(reg_74, reg_75));
      reg_81 = get(reg_1.a, cns562);
      reg_82 = newStack();
      push(reg_82, cns563);
      if (tobool(first(call(reg_81, reg_82)))) {
        reg_6 = first(call(get(get(reg_1.a, cns564), cns565), newStack()));
      }
      reg_97 = get(reg_1.a, cns566);
      reg_98 = newStack();
      push(reg_98, cns567);
      push(reg_98, reg_78);
      reg_100 = call(reg_97, reg_98);
    }
  }
  reg_101 = newStack();
  reg_102 = newTable();
  set(reg_102, cns568, cns569);
  set(reg_102, cns570, reg_4);
  set(reg_102, cns571, reg_6);
  set(reg_102, cns572, reg_11);
  set(reg_102, cns573, reg_8);
  set(reg_102, cns574, reg_10);
  push(reg_101, reg_102);
  return reg_101;
}
var cns575 = anyStr("funcargs")
var cns576 = anyStr("Parser")
var cns577 = anyStr("suffixedexp")
var cns578 = anyStr("check")
var cns579 = anyStr("(")
var cns580 = anyStr("expect")
var cns581 = anyStr("(")
var cns582 = anyStr("Parser")
var cns583 = anyStr("expr")
var cns584 = anyStr("match")
var cns585 = anyStr(")")
var cns586 = anyStr("check")
var cns587 = anyStr("NAME")
var cns588 = anyStr("Parser")
var cns589 = anyStr("singlevar")
var cns590 = anyStr("err")
var cns591 = anyStr("syntax error")
var cns592 = anyStr("check")
var cns593 = anyStr(".")
var cns594 = anyStr("fieldsel")
var cns595 = anyStr("try")
var cns596 = anyStr("[")
var cns597 = anyStr("get_pos")
var cns598 = anyStr("Parser")
var cns599 = anyStr("expr")
var cns600 = anyStr("expect")
var cns601 = anyStr("]")
var cns602 = anyStr("type")
var cns603 = anyStr("index")
var cns604 = anyStr("base")
var cns605 = anyStr("key")
var cns606 = anyStr("check")
var cns607 = anyStr(":")
var cns608 = anyStr("(")
var cns609 = anyStr("{")
var cns610 = anyStr("STR")
var cns611 = anyStr("funcargs")
function Parser_suffixedexp (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_17, reg_18, reg_21, reg_32, reg_33, reg_39, reg_40, reg_55, reg_56, reg_57, reg_65, reg_66, reg_73, reg_74, reg_80, reg_81, reg_91, reg_99, reg_102, reg_103, reg_106, reg_107, reg_117, reg_118, reg_128, reg_129, reg_133, reg_134;
  var goto_43=false, goto_63=false, goto_98=false, goto_145=false, goto_172=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = nil();
  reg_6 = lua$false();
  reg_9 = get(reg_1.a, cns578);
  reg_10 = newStack();
  push(reg_10, cns579);
  goto_43 = !tobool(first(call(reg_9, reg_10)));
  if (!goto_43) {
    reg_17 = get(reg_1.a, cns580);
    reg_18 = newStack();
    push(reg_18, cns581);
    reg_21 = first(call(reg_17, reg_18));
    reg_5 = first(call(get(get(reg_1.a, cns582), cns583), newStack()));
    reg_32 = get(reg_1.a, cns584);
    reg_33 = newStack();
    push(reg_33, cns585);
    push(reg_33, reg_21);
    reg_35 = call(reg_32, reg_33);
    reg_6 = lua$true();
  }
  if ((goto_43 || false)) {
    goto_43 = false;
    reg_39 = get(reg_1.a, cns586);
    reg_40 = newStack();
    push(reg_40, cns587);
    goto_63 = !tobool(first(call(reg_39, reg_40)));
    if (!goto_63) {
      reg_5 = first(call(get(get(reg_1.a, cns588), cns589), newStack()));
    }
    if ((goto_63 || false)) {
      goto_63 = false;
      reg_55 = get(reg_1.a, cns590);
      reg_56 = newStack();
      reg_57 = reg_4;
      if (!tobool(reg_4)) {
        reg_57 = cns591;
      }
      push(reg_56, reg_57);
      reg_60 = call(reg_55, reg_56);
    }
  }
  loop_1: while (tobool(lua$true())) {
    reg_65 = get(reg_1.a, cns592);
    reg_66 = newStack();
    push(reg_66, cns593);
    goto_98 = !tobool(first(call(reg_65, reg_66)));
    if (!goto_98) {
      reg_73 = get(reg_1.a, cns594);
      reg_74 = newStack();
      push(reg_74, reg_5);
      reg_5 = first(call(reg_73, reg_74));
      reg_6 = lua$false();
    }
    if ((goto_98 || false)) {
      goto_98 = false;
      reg_80 = get(reg_1.a, cns595);
      reg_81 = newStack();
      push(reg_81, cns596);
      goto_145 = !tobool(first(call(reg_80, reg_81)));
      if (!goto_145) {
        reg_91 = first(call(get(reg_1.a, cns597), newStack()));
        reg_99 = first(call(get(get(reg_1.a, cns598), cns599), newStack()));
        reg_102 = get(reg_1.a, cns600);
        reg_103 = newStack();
        push(reg_103, cns601);
        reg_105 = call(reg_102, reg_103);
        reg_106 = newStack();
        reg_107 = newTable();
        set(reg_107, cns602, cns603);
        set(reg_107, cns604, reg_5);
        set(reg_107, cns605, reg_99);
        push(reg_106, reg_107);
        reg_5 = first(call(reg_91, reg_106));
        reg_6 = lua$false();
      }
      if ((goto_145 || false)) {
        goto_145 = false;
        reg_117 = get(reg_1.a, cns606);
        reg_118 = newStack();
        push(reg_118, cns607);
        push(reg_118, cns608);
        push(reg_118, cns609);
        push(reg_118, cns610);
        goto_172 = !tobool(first(call(reg_117, reg_118)));
        if (!goto_172) {
          reg_128 = get(reg_1.a, cns611);
          reg_129 = newStack();
          push(reg_129, reg_5);
          reg_5 = first(call(reg_128, reg_129));
          reg_6 = lua$false();
        }
        if ((goto_172 || false)) {
          goto_172 = false;
          reg_133 = newStack();
          push(reg_133, reg_5);
          push(reg_133, reg_6);
          return reg_133;
        }
      }
    }
  }
  reg_134 = newStack();
  return reg_134;
}
var cns612 = anyStr("Parser")
var cns613 = anyStr("simpleexp")
var cns614 = anyStr("get_pos")
var cns615 = anyStr("check")
var cns616 = anyStr("NUM")
var cns617 = anyStr("type")
var cns618 = anyStr("num")
var cns619 = anyStr("value")
var cns620 = anyStr("next")
var cns621 = anyStr("value")
var cns622 = anyStr("check")
var cns623 = anyStr("true")
var cns624 = anyStr("false")
var cns625 = anyStr("nil")
var cns626 = anyStr("type")
var cns627 = anyStr("const")
var cns628 = anyStr("value")
var cns629 = anyStr("next")
var cns630 = anyStr("type")
var cns631 = anyStr("try")
var cns632 = anyStr("...")
var cns633 = anyStr("type")
var cns634 = anyStr("vararg")
var cns635 = anyStr("check")
var cns636 = anyStr("STR")
var cns637 = anyStr("type")
var cns638 = anyStr("str")
var cns639 = anyStr("value")
var cns640 = anyStr("next")
var cns641 = anyStr("value")
var cns642 = anyStr("check")
var cns643 = anyStr("{")
var cns644 = anyStr("constructor")
var cns645 = anyStr("check")
var cns646 = anyStr("function")
var cns647 = anyStr("next")
var cns648 = anyStr("Parser")
var cns649 = anyStr("funcbody")
var cns650 = anyStr("Parser")
var cns651 = anyStr("suffixedexp")
var cns652 = anyStr("invalid expression")
function Parser_simpleexp (reg_0, reg_1) {
  var reg_9, reg_12, reg_13, reg_18, reg_19, reg_20, reg_23, reg_35, reg_36, reg_43, reg_44, reg_45, reg_48, reg_60, reg_61, reg_66, reg_67, reg_68, reg_74, reg_75, reg_80, reg_81, reg_82, reg_85, reg_97, reg_98, reg_103, reg_111, reg_112, reg_122, reg_123, reg_124, reg_129, reg_130, reg_133, reg_138, reg_139, reg_142;
  var goto_39=false, goto_74=false, goto_95=false, goto_126=false, goto_145=false, goto_176=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_9 = first(call(get(reg_1.a, cns614), newStack()));
  reg_12 = get(reg_1.a, cns615);
  reg_13 = newStack();
  push(reg_13, cns616);
  goto_39 = !tobool(first(call(reg_12, reg_13)));
  if (!goto_39) {
    reg_18 = newStack();
    reg_19 = newStack();
    reg_20 = newTable();
    set(reg_20, cns617, cns618);
    reg_23 = cns619;
    set(reg_20, reg_23, get(first(call(get(reg_1.a, cns620), newStack())), cns621));
    push(reg_19, reg_20);
    append(reg_18, call(reg_9, reg_19));
    return reg_18;
  }
  if ((goto_39 || false)) {
    goto_39 = false;
    reg_35 = get(reg_1.a, cns622);
    reg_36 = newStack();
    push(reg_36, cns623);
    push(reg_36, cns624);
    push(reg_36, cns625);
    goto_74 = !tobool(first(call(reg_35, reg_36)));
    if (!goto_74) {
      reg_43 = newStack();
      reg_44 = newStack();
      reg_45 = newTable();
      set(reg_45, cns626, cns627);
      reg_48 = cns628;
      set(reg_45, reg_48, get(first(call(get(reg_1.a, cns629), newStack())), cns630));
      push(reg_44, reg_45);
      append(reg_43, call(reg_9, reg_44));
      return reg_43;
    }
    if ((goto_74 || false)) {
      goto_74 = false;
      reg_60 = get(reg_1.a, cns631);
      reg_61 = newStack();
      push(reg_61, cns632);
      goto_95 = !tobool(first(call(reg_60, reg_61)));
      if (!goto_95) {
        reg_66 = newStack();
        reg_67 = newStack();
        reg_68 = newTable();
        set(reg_68, cns633, cns634);
        push(reg_67, reg_68);
        append(reg_66, call(reg_9, reg_67));
        return reg_66;
      }
      if ((goto_95 || false)) {
        goto_95 = false;
        reg_74 = get(reg_1.a, cns635);
        reg_75 = newStack();
        push(reg_75, cns636);
        goto_126 = !tobool(first(call(reg_74, reg_75)));
        if (!goto_126) {
          reg_80 = newStack();
          reg_81 = newStack();
          reg_82 = newTable();
          set(reg_82, cns637, cns638);
          reg_85 = cns639;
          set(reg_82, reg_85, get(first(call(get(reg_1.a, cns640), newStack())), cns641));
          push(reg_81, reg_82);
          append(reg_80, call(reg_9, reg_81));
          return reg_80;
        }
        if ((goto_126 || false)) {
          goto_126 = false;
          reg_97 = get(reg_1.a, cns642);
          reg_98 = newStack();
          push(reg_98, cns643);
          goto_145 = !tobool(first(call(reg_97, reg_98)));
          if (!goto_145) {
            reg_103 = newStack();
            append(reg_103, call(get(reg_1.a, cns644), newStack()));
            return reg_103;
          }
          if ((goto_145 || false)) {
            goto_145 = false;
            reg_111 = get(reg_1.a, cns645);
            reg_112 = newStack();
            push(reg_112, cns646);
            goto_176 = !tobool(first(call(reg_111, reg_112)));
            if (!goto_176) {
              reg_122 = first(call(get(reg_1.a, cns647), newStack()));
              reg_123 = newStack();
              reg_124 = newStack();
              reg_129 = get(get(reg_1.a, cns648), cns649);
              reg_130 = newStack();
              push(reg_130, reg_122);
              append(reg_124, call(reg_129, reg_130));
              append(reg_123, call(reg_9, reg_124));
              return reg_123;
            }
            if ((goto_176 || false)) {
              goto_176 = false;
              reg_133 = newStack();
              reg_138 = get(get(reg_1.a, cns650), cns651);
              reg_139 = newStack();
              push(reg_139, cns652);
              append(reg_133, call(reg_138, reg_139));
              return reg_133;
            }
          }
        }
      }
    }
  }
  reg_142 = newStack();
  return reg_142;
}
var cns653 = anyStr("+")
var cns654 = parseNum("1")
var cns655 = parseNum("10")
var cns656 = parseNum("2")
var cns657 = parseNum("10")
var cns658 = anyStr("-")
var cns659 = parseNum("1")
var cns660 = parseNum("10")
var cns661 = parseNum("2")
var cns662 = parseNum("10")
var cns663 = anyStr("*")
var cns664 = parseNum("1")
var cns665 = parseNum("11")
var cns666 = parseNum("2")
var cns667 = parseNum("11")
var cns668 = anyStr("%")
var cns669 = parseNum("1")
var cns670 = parseNum("11")
var cns671 = parseNum("2")
var cns672 = parseNum("11")
var cns673 = anyStr("/")
var cns674 = parseNum("1")
var cns675 = parseNum("11")
var cns676 = parseNum("2")
var cns677 = parseNum("11")
var cns678 = anyStr("//")
var cns679 = parseNum("1")
var cns680 = parseNum("11")
var cns681 = parseNum("2")
var cns682 = parseNum("11")
var cns683 = anyStr("^")
var cns684 = parseNum("1")
var cns685 = parseNum("14")
var cns686 = parseNum("2")
var cns687 = parseNum("13")
var cns688 = anyStr("&")
var cns689 = parseNum("1")
var cns690 = parseNum("6")
var cns691 = parseNum("2")
var cns692 = parseNum("6")
var cns693 = anyStr("|")
var cns694 = parseNum("1")
var cns695 = parseNum("4")
var cns696 = parseNum("2")
var cns697 = parseNum("4")
var cns698 = anyStr("~")
var cns699 = parseNum("1")
var cns700 = parseNum("5")
var cns701 = parseNum("2")
var cns702 = parseNum("5")
var cns703 = anyStr("<<")
var cns704 = parseNum("1")
var cns705 = parseNum("7")
var cns706 = parseNum("2")
var cns707 = parseNum("7")
var cns708 = anyStr(">>")
var cns709 = parseNum("1")
var cns710 = parseNum("7")
var cns711 = parseNum("2")
var cns712 = parseNum("7")
var cns713 = anyStr("..")
var cns714 = parseNum("1")
var cns715 = parseNum("9")
var cns716 = parseNum("2")
var cns717 = parseNum("8")
var cns718 = anyStr("<")
var cns719 = parseNum("1")
var cns720 = parseNum("3")
var cns721 = parseNum("2")
var cns722 = parseNum("3")
var cns723 = anyStr(">")
var cns724 = parseNum("1")
var cns725 = parseNum("3")
var cns726 = parseNum("2")
var cns727 = parseNum("3")
var cns728 = anyStr("==")
var cns729 = parseNum("1")
var cns730 = parseNum("3")
var cns731 = parseNum("2")
var cns732 = parseNum("3")
var cns733 = anyStr("~=")
var cns734 = parseNum("1")
var cns735 = parseNum("3")
var cns736 = parseNum("2")
var cns737 = parseNum("3")
var cns738 = anyStr("<=")
var cns739 = parseNum("1")
var cns740 = parseNum("3")
var cns741 = parseNum("2")
var cns742 = parseNum("3")
var cns743 = anyStr(">=")
var cns744 = parseNum("1")
var cns745 = parseNum("3")
var cns746 = parseNum("2")
var cns747 = parseNum("3")
var cns748 = anyStr("and")
var cns749 = parseNum("1")
var cns750 = parseNum("2")
var cns751 = parseNum("2")
var cns752 = parseNum("2")
var cns753 = anyStr("or")
var cns754 = parseNum("1")
var cns755 = parseNum("1")
var cns756 = parseNum("2")
var cns757 = parseNum("1")
var cns758 = anyStr("priority")
var cns759 = parseNum("12")
var cns760 = anyStr("unary_priority")
var cns761 = anyStr("Parser")
var cns762 = anyStr("expr")
var cns763 = parseNum("0")
var cns764 = anyStr("check")
var cns765 = anyStr("not")
var cns766 = anyStr("-")
var cns767 = anyStr("~")
var cns768 = anyStr("#")
var cns769 = anyStr("get_pos")
var cns770 = anyStr("type")
var cns771 = anyStr("unop")
var cns772 = anyStr("op")
var cns773 = anyStr("next")
var cns774 = anyStr("type")
var cns775 = anyStr("value")
var cns776 = anyStr("Parser")
var cns777 = anyStr("expr")
var cns778 = anyStr("unary_priority")
var cns779 = anyStr("Parser")
var cns780 = anyStr("simpleexp")
var cns781 = anyStr("token")
var cns782 = anyStr("priority")
var cns783 = anyStr("token")
var cns784 = anyStr("type")
var cns785 = parseNum("1")
var cns786 = anyStr("get_pos")
var cns787 = anyStr("next")
var cns788 = anyStr("type")
var cns789 = anyStr("Parser")
var cns790 = anyStr("expr")
var cns791 = parseNum("2")
var cns792 = anyStr("type")
var cns793 = anyStr("binop")
var cns794 = anyStr("op")
var cns795 = anyStr("left")
var cns796 = anyStr("right")
var cns797 = anyStr("token")
var cns798 = anyStr("priority")
var cns799 = anyStr("token")
var cns800 = anyStr("type")
function Parser_expr (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_11, reg_12, reg_25, reg_26, reg_27, reg_30, reg_39, reg_44, reg_45, reg_63, reg_67, reg_74, reg_85, reg_93, reg_98, reg_99, reg_103, reg_104, reg_105, reg_115, reg_119, reg_126;
  var goto_66=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = reg_4;
  if (!tobool(reg_4)) {
    reg_5 = cns763;
  }
  reg_4 = reg_5;
  reg_8 = nil();
  reg_11 = get(reg_1.a, cns764);
  reg_12 = newStack();
  push(reg_12, cns765);
  push(reg_12, cns766);
  push(reg_12, cns767);
  push(reg_12, cns768);
  goto_66 = !tobool(first(call(reg_11, reg_12)));
  if (!goto_66) {
    reg_25 = first(call(get(reg_1.a, cns769), newStack()));
    reg_26 = newStack();
    reg_27 = newTable();
    set(reg_27, cns770, cns771);
    reg_30 = cns772;
    set(reg_27, reg_30, get(first(call(get(reg_1.a, cns773), newStack())), cns774));
    reg_39 = cns775;
    reg_44 = get(get(reg_1.a, cns776), cns777);
    reg_45 = newStack();
    push(reg_45, get(reg_1.a, cns778));
    set(reg_27, reg_39, first(call(reg_44, reg_45)));
    push(reg_26, reg_27);
    reg_8 = first(call(reg_25, reg_26));
  }
  if ((goto_66 || false)) {
    goto_66 = false;
    reg_8 = first(call(get(get(reg_1.a, cns779), cns780), newStack()));
  }
  reg_63 = get(reg_1.a, cns781);
  if (tobool(reg_63)) {
    reg_67 = get(reg_1.a, cns782);
    reg_63 = get(reg_67, get(get(reg_1.a, cns783), cns784));
  }
  loop_1: while (true) {
    reg_74 = reg_63;
    if (tobool(reg_63)) {
      reg_74 = gt(get(reg_63, cns785), reg_4);
    }
    if (!tobool(reg_74)) break loop_1;
    reg_85 = first(call(get(reg_1.a, cns786), newStack()));
    reg_93 = get(first(call(get(reg_1.a, cns787), newStack())), cns788);
    reg_98 = get(get(reg_1.a, cns789), cns790);
    reg_99 = newStack();
    push(reg_99, get(reg_63, cns791));
    reg_103 = first(call(reg_98, reg_99));
    reg_104 = newStack();
    reg_105 = newTable();
    set(reg_105, cns792, cns793);
    set(reg_105, cns794, reg_93);
    set(reg_105, cns795, reg_8);
    set(reg_105, cns796, reg_103);
    push(reg_104, reg_105);
    reg_8 = first(call(reg_85, reg_104));
    reg_115 = get(reg_1.a, cns797);
    if (tobool(reg_115)) {
      reg_119 = get(reg_1.a, cns798);
      reg_115 = get(reg_119, get(get(reg_1.a, cns799), cns800));
    }
    reg_63 = reg_115;
  }
  reg_126 = newStack();
  push(reg_126, reg_8);
  return reg_126;
}
var cns801 = anyStr("Parser")
var cns802 = anyStr("explist")
var cns803 = anyStr("Parser")
var cns804 = anyStr("expr")
var cns805 = anyStr("table")
var cns806 = anyStr("insert")
var cns807 = anyStr("try")
var cns808 = anyStr(",")
function Parser_explist (reg_0, reg_1) {
  var reg_4, reg_12, reg_17, reg_18, reg_22, reg_23, reg_29;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newTable();
  loop_1: while (true) {
    reg_12 = first(call(get(get(reg_1.a, cns803), cns804), newStack()));
    reg_17 = get(get(reg_1.a, cns805), cns806);
    reg_18 = newStack();
    push(reg_18, reg_4);
    push(reg_18, reg_12);
    reg_19 = call(reg_17, reg_18);
    reg_22 = get(reg_1.a, cns807);
    reg_23 = newStack();
    push(reg_23, cns808);
    if (tobool(not(first(call(reg_22, reg_23))))) break loop_1;
  }
  reg_29 = newStack();
  push(reg_29, reg_4);
  return reg_29;
}
var cns809 = anyStr("Parser")
var cns810 = anyStr("funcbody")
var cns811 = anyStr("expect")
var cns812 = anyStr("(")
var cns813 = anyStr("check")
var cns814 = anyStr(")")
var cns815 = anyStr("check")
var cns816 = anyStr("NAME")
var cns817 = anyStr("table")
var cns818 = anyStr("insert")
var cns819 = anyStr("get_name")
var cns820 = anyStr("try")
var cns821 = anyStr("...")
var cns822 = anyStr("expect")
var cns823 = anyStr("NAME")
var cns824 = anyStr("...")
var cns825 = anyStr("try")
var cns826 = anyStr(",")
var cns827 = anyStr("expect")
var cns828 = anyStr(")")
var cns829 = anyStr("Parser")
var cns830 = anyStr("statlist")
var cns831 = anyStr("match")
var cns832 = anyStr("end")
var cns833 = anyStr("type")
var cns834 = anyStr("function")
var cns835 = anyStr("vararg")
var cns836 = anyStr("names")
var cns837 = anyStr("body")
function Parser_funcbody (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_15, reg_16, reg_24, reg_25, reg_34, reg_35, reg_44, reg_45, reg_53, reg_54, reg_60, reg_61, reg_69, reg_70, reg_80, reg_83, reg_84, reg_87, reg_88;
  var goto_48=false, goto_62=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = lua$false();
  reg_6 = newTable();
  reg_9 = get(reg_1.a, cns811);
  reg_10 = newStack();
  push(reg_10, cns812);
  reg_12 = call(reg_9, reg_10);
  reg_15 = get(reg_1.a, cns813);
  reg_16 = newStack();
  push(reg_16, cns814);
  if (tobool(not(first(call(reg_15, reg_16))))) {
    loop_1: while (true) {
      reg_24 = get(reg_1.a, cns815);
      reg_25 = newStack();
      push(reg_25, cns816);
      goto_48 = !tobool(first(call(reg_24, reg_25)));
      if (!goto_48) {
        reg_34 = get(get(reg_1.a, cns817), cns818);
        reg_35 = newStack();
        push(reg_35, reg_6);
        append(reg_35, call(get(reg_1.a, cns819), newStack()));
        reg_41 = call(reg_34, reg_35);
      }
      if ((goto_48 || false)) {
        goto_48 = false;
        reg_44 = get(reg_1.a, cns820);
        reg_45 = newStack();
        push(reg_45, cns821);
        goto_62 = !tobool(first(call(reg_44, reg_45)));
        if (!goto_62) {
          reg_5 = lua$true();
          if (true) break loop_1;
        }
        if ((goto_62 || false)) {
          goto_62 = false;
          reg_53 = get(reg_1.a, cns822);
          reg_54 = newStack();
          push(reg_54, cns823);
          push(reg_54, cns824);
          reg_57 = call(reg_53, reg_54);
        }
      }
      reg_60 = get(reg_1.a, cns825);
      reg_61 = newStack();
      push(reg_61, cns826);
      if (tobool(not(first(call(reg_60, reg_61))))) break loop_1;
    }
  }
  reg_69 = get(reg_1.a, cns827);
  reg_70 = newStack();
  push(reg_70, cns828);
  reg_72 = call(reg_69, reg_70);
  reg_80 = first(call(get(get(reg_1.a, cns829), cns830), newStack()));
  reg_83 = get(reg_1.a, cns831);
  reg_84 = newStack();
  push(reg_84, cns832);
  push(reg_84, reg_4);
  reg_86 = call(reg_83, reg_84);
  reg_87 = newStack();
  reg_88 = newTable();
  set(reg_88, cns833, cns834);
  set(reg_88, cns835, reg_5);
  set(reg_88, cns836, reg_6);
  set(reg_88, cns837, reg_80);
  push(reg_87, reg_88);
  return reg_87;
}
var cns838 = anyStr("expect")
var cns839 = anyStr(":")
var cns840 = anyStr("expect")
var cns841 = anyStr(".")
var cns842 = anyStr("get_pos")
var cns843 = anyStr("get_name")
var cns844 = anyStr("type")
var cns845 = anyStr("field")
var cns846 = anyStr("base")
var cns847 = anyStr("key")
function function$22 (reg_0, reg_1) {
  var reg_4, reg_9, reg_10, reg_15, reg_16, reg_24, reg_30, reg_31, reg_32, reg_33;
  var goto_14=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  goto_14 = !tobool(next(reg_0));
  if (!goto_14) {
    reg_9 = get(reg_1.a, cns838);
    reg_10 = newStack();
    push(reg_10, cns839);
    reg_12 = call(reg_9, reg_10);
  }
  if ((goto_14 || false)) {
    goto_14 = false;
    reg_15 = get(reg_1.a, cns840);
    reg_16 = newStack();
    push(reg_16, cns841);
    reg_18 = call(reg_15, reg_16);
  }
  reg_24 = first(call(get(reg_1.a, cns842), newStack()));
  reg_30 = first(call(get(reg_1.a, cns843), newStack()));
  reg_31 = newStack();
  reg_32 = newStack();
  reg_33 = newTable();
  set(reg_33, cns844, cns845);
  set(reg_33, cns846, reg_4);
  set(reg_33, cns847, reg_30);
  push(reg_32, reg_33);
  append(reg_31, call(reg_24, reg_32));
  return reg_31;
}
var cns848 = anyStr("fieldsel")
var cns849 = anyStr("Parser")
var cns850 = anyStr("exprstat")
var cns851 = anyStr("get_pos")
var cns852 = anyStr("Parser")
var cns853 = anyStr("suffixedexp")
var cns854 = anyStr("invalid statement")
var cns855 = anyStr("type")
var cns856 = anyStr("call")
var cns857 = parseNum("1")
var cns858 = anyStr("try")
var cns859 = anyStr(",")
var cns860 = anyStr("Parser")
var cns861 = anyStr("suffixedexp")
var cns862 = anyStr("invalid statement")
var cns863 = anyStr("table")
var cns864 = anyStr("insert")
var cns865 = anyStr("expect")
var cns866 = anyStr("=")
var cns867 = anyStr("err")
var cns868 = anyStr("assignment to a parenthesized expression")
var cns869 = anyStr("pairs")
var cns870 = anyStr("type")
var cns871 = anyStr("var")
var cns872 = anyStr("type")
var cns873 = anyStr("field")
var cns874 = anyStr("type")
var cns875 = anyStr("index")
var cns876 = anyStr("err")
var cns877 = anyStr("assignment to a non lvalue expression")
var cns878 = anyStr("Parser")
var cns879 = anyStr("explist")
var cns880 = anyStr("type")
var cns881 = anyStr("assignment")
var cns882 = anyStr("lhs")
var cns883 = anyStr("values")
function Parser_exprstat (reg_0, reg_1) {
  var reg_9, reg_14, reg_15, reg_17, reg_18, reg_19, reg_20, reg_27, reg_28, reg_32, reg_33, reg_43, reg_44, reg_46, reg_55, reg_56, reg_60, reg_61, reg_67, reg_68, reg_73, reg_74, reg_75, reg_76, reg_77, reg_78, reg_79, reg_80, reg_82, reg_89, reg_103, reg_104, reg_114, reg_115, reg_116, reg_117, reg_123;
  var goto_33=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_9 = first(call(get(reg_1.a, cns851), newStack()));
  reg_14 = get(get(reg_1.a, cns852), cns853);
  reg_15 = newStack();
  push(reg_15, cns854);
  reg_17 = call(reg_14, reg_15);
  reg_18 = next(reg_17);
  reg_19 = next(reg_17);
  reg_20 = not(reg_19);
  if (tobool(reg_20)) {
    reg_20 = eq(get(reg_18, cns855), cns856);
  }
  goto_33 = !tobool(reg_20);
  if (!goto_33) {
    reg_27 = newStack();
    push(reg_27, reg_18);
    return reg_27;
  }
  if ((goto_33 || false)) {
    goto_33 = false;
    reg_28 = newTable();
    set(reg_28, cns857, reg_18);
    loop_2: while (true) {
      reg_32 = get(reg_1.a, cns858);
      reg_33 = newStack();
      push(reg_33, cns859);
      if (!tobool(first(call(reg_32, reg_33)))) break loop_2;
      reg_38 = nil();
      reg_43 = get(get(reg_1.a, cns860), cns861);
      reg_44 = newStack();
      push(reg_44, cns862);
      reg_46 = call(reg_43, reg_44);
      reg_18 = next(reg_46);
      if (tobool(next(reg_46))) {
        reg_19 = lua$true();
      }
      reg_55 = get(get(reg_1.a, cns863), cns864);
      reg_56 = newStack();
      push(reg_56, reg_28);
      push(reg_56, reg_18);
      reg_57 = call(reg_55, reg_56);
    }
    reg_60 = get(reg_1.a, cns865);
    reg_61 = newStack();
    push(reg_61, cns866);
    reg_63 = call(reg_60, reg_61);
    if (tobool(reg_19)) {
      reg_67 = get(reg_1.a, cns867);
      reg_68 = newStack();
      push(reg_68, cns868);
      reg_70 = call(reg_67, reg_68);
    }
    reg_73 = get(reg_1.a, cns869);
    reg_74 = newStack();
    push(reg_74, reg_28);
    reg_75 = call(reg_73, reg_74);
    reg_76 = next(reg_75);
    reg_77 = next(reg_75);
    reg_78 = next(reg_75);
    loop_1: while (true) {
      reg_79 = newStack();
      push(reg_79, reg_77);
      push(reg_79, reg_78);
      reg_80 = call(reg_76, reg_79);
      reg_78 = next(reg_80);
      reg_82 = next(reg_80);
      if (tobool(eq(reg_78, nil()))) break loop_1;
      reg_89 = ne(get(reg_82, cns870), cns871);
      if (tobool(reg_89)) {
        reg_89 = ne(get(reg_82, cns872), cns873);
      }
      if (tobool(reg_89)) {
        reg_89 = ne(get(reg_82, cns874), cns875);
      }
      if (tobool(reg_89)) {
        reg_103 = get(reg_1.a, cns876);
        reg_104 = newStack();
        push(reg_104, cns877);
        reg_106 = call(reg_103, reg_104);
      }
    }
    reg_114 = first(call(get(get(reg_1.a, cns878), cns879), newStack()));
    reg_115 = newStack();
    reg_116 = newStack();
    reg_117 = newTable();
    set(reg_117, cns880, cns881);
    set(reg_117, cns882, reg_28);
    set(reg_117, cns883, reg_114);
    push(reg_116, reg_117);
    append(reg_115, call(reg_9, reg_116));
    return reg_115;
  }
  reg_123 = newStack();
  return reg_123;
}
var cns884 = anyStr("Parser")
var cns885 = anyStr("forstat")
var cns886 = anyStr("expect")
var cns887 = anyStr("for")
var cns888 = anyStr("expect")
var cns889 = anyStr("NAME")
var cns890 = anyStr("try")
var cns891 = anyStr("=")
var cns892 = anyStr("value")
var cns893 = anyStr("Parser")
var cns894 = anyStr("expr")
var cns895 = anyStr("expect")
var cns896 = anyStr(",")
var cns897 = anyStr("Parser")
var cns898 = anyStr("expr")
var cns899 = anyStr("try")
var cns900 = anyStr(",")
var cns901 = anyStr("Parser")
var cns902 = anyStr("expr")
var cns903 = anyStr("expect")
var cns904 = anyStr("do")
var cns905 = anyStr("Parser")
var cns906 = anyStr("statlist")
var cns907 = anyStr("match")
var cns908 = anyStr("end")
var cns909 = anyStr("type")
var cns910 = anyStr("numfor")
var cns911 = anyStr("name")
var cns912 = anyStr("body")
var cns913 = anyStr("init")
var cns914 = anyStr("limit")
var cns915 = anyStr("step")
var cns916 = anyStr("check")
var cns917 = anyStr(",")
var cns918 = anyStr("in")
var cns919 = parseNum("1")
var cns920 = anyStr("value")
var cns921 = anyStr("try")
var cns922 = anyStr(",")
var cns923 = anyStr("expect")
var cns924 = anyStr("NAME")
var cns925 = anyStr("table")
var cns926 = anyStr("insert")
var cns927 = anyStr("value")
var cns928 = anyStr("expect")
var cns929 = anyStr("in")
var cns930 = anyStr("Parser")
var cns931 = anyStr("explist")
var cns932 = anyStr("expect")
var cns933 = anyStr("do")
var cns934 = anyStr("Parser")
var cns935 = anyStr("statlist")
var cns936 = anyStr("match")
var cns937 = anyStr("end")
var cns938 = anyStr("type")
var cns939 = anyStr("genfor")
var cns940 = anyStr("names")
var cns941 = anyStr("values")
var cns942 = anyStr("body")
var cns943 = anyStr("expect")
var cns944 = anyStr("=")
var cns945 = anyStr(",")
var cns946 = anyStr("in")
function Parser_forstat (reg_0, reg_1) {
  var reg_6, reg_7, reg_10, reg_13, reg_14, reg_17, reg_20, reg_23, reg_24, reg_30, reg_38, reg_41, reg_42, reg_52, reg_53, reg_56, reg_57, reg_72, reg_73, reg_83, reg_86, reg_87, reg_90, reg_91, reg_101, reg_102, reg_108, reg_109, reg_114, reg_115, reg_122, reg_123, reg_129, reg_134, reg_135, reg_141, reg_142, reg_152, reg_155, reg_156, reg_166, reg_169, reg_170, reg_173, reg_174, reg_182, reg_183, reg_188;
  var goto_121=false, goto_227=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = get(reg_1.a, cns886);
  reg_7 = newStack();
  push(reg_7, cns887);
  reg_10 = first(call(reg_6, reg_7));
  reg_13 = get(reg_1.a, cns888);
  reg_14 = newStack();
  push(reg_14, cns889);
  reg_17 = first(call(reg_13, reg_14));
  if (tobool(not(reg_17))) {
    reg_20 = newStack();
    return reg_20;
  }
  reg_23 = get(reg_1.a, cns890);
  reg_24 = newStack();
  push(reg_24, cns891);
  goto_121 = !tobool(first(call(reg_23, reg_24)));
  if (!goto_121) {
    reg_30 = get(reg_17, cns892);
    reg_38 = first(call(get(get(reg_1.a, cns893), cns894), newStack()));
    reg_41 = get(reg_1.a, cns895);
    reg_42 = newStack();
    push(reg_42, cns896);
    reg_44 = call(reg_41, reg_42);
    reg_52 = first(call(get(get(reg_1.a, cns897), cns898), newStack()));
    reg_53 = nil();
    reg_56 = get(reg_1.a, cns899);
    reg_57 = newStack();
    push(reg_57, cns900);
    if (tobool(first(call(reg_56, reg_57)))) {
      reg_53 = first(call(get(get(reg_1.a, cns901), cns902), newStack()));
    }
    reg_72 = get(reg_1.a, cns903);
    reg_73 = newStack();
    push(reg_73, cns904);
    reg_75 = call(reg_72, reg_73);
    reg_83 = first(call(get(get(reg_1.a, cns905), cns906), newStack()));
    reg_86 = get(reg_1.a, cns907);
    reg_87 = newStack();
    push(reg_87, cns908);
    push(reg_87, reg_10);
    reg_89 = call(reg_86, reg_87);
    reg_90 = newStack();
    reg_91 = newTable();
    set(reg_91, cns909, cns910);
    set(reg_91, cns911, reg_30);
    set(reg_91, cns912, reg_83);
    set(reg_91, cns913, reg_38);
    set(reg_91, cns914, reg_52);
    set(reg_91, cns915, reg_53);
    push(reg_90, reg_91);
    return reg_90;
  }
  if ((goto_121 || false)) {
    goto_121 = false;
    reg_101 = get(reg_1.a, cns916);
    reg_102 = newStack();
    push(reg_102, cns917);
    push(reg_102, cns918);
    goto_227 = !tobool(first(call(reg_101, reg_102)));
    if (!goto_227) {
      reg_108 = newTable();
      reg_109 = cns919;
      set(reg_108, reg_109, get(reg_17, cns920));
      loop_1: while (true) {
        reg_114 = get(reg_1.a, cns921);
        reg_115 = newStack();
        push(reg_115, cns922);
        if (!tobool(first(call(reg_114, reg_115)))) break loop_1;
        reg_122 = get(reg_1.a, cns923);
        reg_123 = newStack();
        push(reg_123, cns924);
        reg_17 = first(call(reg_122, reg_123));
        if (tobool(not(reg_17))) {
          reg_129 = newStack();
          return reg_129;
        }
        reg_134 = get(get(reg_1.a, cns925), cns926);
        reg_135 = newStack();
        push(reg_135, reg_108);
        push(reg_135, get(reg_17, cns927));
        reg_138 = call(reg_134, reg_135);
      }
      reg_141 = get(reg_1.a, cns928);
      reg_142 = newStack();
      push(reg_142, cns929);
      reg_144 = call(reg_141, reg_142);
      reg_152 = first(call(get(get(reg_1.a, cns930), cns931), newStack()));
      reg_155 = get(reg_1.a, cns932);
      reg_156 = newStack();
      push(reg_156, cns933);
      reg_158 = call(reg_155, reg_156);
      reg_166 = first(call(get(get(reg_1.a, cns934), cns935), newStack()));
      reg_169 = get(reg_1.a, cns936);
      reg_170 = newStack();
      push(reg_170, cns937);
      push(reg_170, reg_10);
      reg_172 = call(reg_169, reg_170);
      reg_173 = newStack();
      reg_174 = newTable();
      set(reg_174, cns938, cns939);
      set(reg_174, cns940, reg_108);
      set(reg_174, cns941, reg_152);
      set(reg_174, cns942, reg_166);
      push(reg_173, reg_174);
      return reg_173;
    }
    if ((goto_227 || false)) {
      goto_227 = false;
      reg_182 = get(reg_1.a, cns943);
      reg_183 = newStack();
      push(reg_183, cns944);
      push(reg_183, cns945);
      push(reg_183, cns946);
      reg_187 = call(reg_182, reg_183);
    }
  }
  reg_188 = newStack();
  return reg_188;
}
var cns947 = anyStr("Parser")
var cns948 = anyStr("ifstat")
var cns949 = anyStr("expect")
var cns950 = anyStr("if")
var cns951 = anyStr("Parser")
var cns952 = anyStr("expr")
var cns953 = anyStr("expect")
var cns954 = anyStr("then")
var cns955 = anyStr("Parser")
var cns956 = anyStr("statlist")
var cns957 = anyStr("table")
var cns958 = anyStr("insert")
var cns959 = anyStr("type")
var cns960 = anyStr("clause")
var cns961 = anyStr("cond")
var cns962 = anyStr("body")
var cns963 = anyStr("try")
var cns964 = anyStr("elseif")
var cns965 = anyStr("Parser")
var cns966 = anyStr("expr")
var cns967 = anyStr("expect")
var cns968 = anyStr("then")
var cns969 = anyStr("Parser")
var cns970 = anyStr("statlist")
var cns971 = anyStr("table")
var cns972 = anyStr("insert")
var cns973 = anyStr("type")
var cns974 = anyStr("clause")
var cns975 = anyStr("cond")
var cns976 = anyStr("body")
var cns977 = anyStr("try")
var cns978 = anyStr("else")
var cns979 = anyStr("Parser")
var cns980 = anyStr("statlist")
var cns981 = anyStr("match")
var cns982 = anyStr("end")
var cns983 = anyStr("type")
var cns984 = anyStr("if")
var cns985 = anyStr("clauses")
var cns986 = anyStr("els")
function Parser_ifstat (reg_0, reg_1) {
  var reg_6, reg_7, reg_10, reg_11, reg_19, reg_22, reg_23, reg_33, reg_38, reg_39, reg_40, reg_48, reg_49, reg_64, reg_65, reg_80, reg_81, reg_82, reg_88, reg_91, reg_92, reg_107, reg_108, reg_111, reg_112;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = get(reg_1.a, cns949);
  reg_7 = newStack();
  push(reg_7, cns950);
  reg_10 = first(call(reg_6, reg_7));
  reg_11 = newTable();
  reg_19 = first(call(get(get(reg_1.a, cns951), cns952), newStack()));
  reg_22 = get(reg_1.a, cns953);
  reg_23 = newStack();
  push(reg_23, cns954);
  reg_25 = call(reg_22, reg_23);
  reg_33 = first(call(get(get(reg_1.a, cns955), cns956), newStack()));
  reg_38 = get(get(reg_1.a, cns957), cns958);
  reg_39 = newStack();
  push(reg_39, reg_11);
  reg_40 = newTable();
  set(reg_40, cns959, cns960);
  set(reg_40, cns961, reg_19);
  set(reg_40, cns962, reg_33);
  push(reg_39, reg_40);
  reg_45 = call(reg_38, reg_39);
  loop_1: while (true) {
    reg_48 = get(reg_1.a, cns963);
    reg_49 = newStack();
    push(reg_49, cns964);
    if (!tobool(first(call(reg_48, reg_49)))) break loop_1;
    reg_19 = first(call(get(get(reg_1.a, cns965), cns966), newStack()));
    reg_64 = get(reg_1.a, cns967);
    reg_65 = newStack();
    push(reg_65, cns968);
    reg_67 = call(reg_64, reg_65);
    reg_33 = first(call(get(get(reg_1.a, cns969), cns970), newStack()));
    reg_80 = get(get(reg_1.a, cns971), cns972);
    reg_81 = newStack();
    push(reg_81, reg_11);
    reg_82 = newTable();
    set(reg_82, cns973, cns974);
    set(reg_82, cns975, reg_19);
    set(reg_82, cns976, reg_33);
    push(reg_81, reg_82);
    reg_87 = call(reg_80, reg_81);
  }
  reg_88 = newTable();
  reg_91 = get(reg_1.a, cns977);
  reg_92 = newStack();
  push(reg_92, cns978);
  if (tobool(first(call(reg_91, reg_92)))) {
    reg_88 = first(call(get(get(reg_1.a, cns979), cns980), newStack()));
  }
  reg_107 = get(reg_1.a, cns981);
  reg_108 = newStack();
  push(reg_108, cns982);
  push(reg_108, reg_10);
  reg_110 = call(reg_107, reg_108);
  reg_111 = newStack();
  reg_112 = newTable();
  set(reg_112, cns983, cns984);
  set(reg_112, cns985, reg_11);
  set(reg_112, cns986, reg_88);
  push(reg_111, reg_112);
  return reg_111;
}
var cns987 = anyStr("Parser")
var cns988 = anyStr("statement")
var cns989 = anyStr("get_pos")
var cns990 = anyStr("try")
var cns991 = anyStr(";")
var cns992 = anyStr("check")
var cns993 = anyStr("if")
var cns994 = anyStr("Parser")
var cns995 = anyStr("ifstat")
var cns996 = anyStr("check")
var cns997 = anyStr("while")
var cns998 = anyStr("next")
var cns999 = anyStr("Parser")
var cns1000 = anyStr("expr")
var cns1001 = anyStr("expect")
var cns1002 = anyStr("do")
var cns1003 = anyStr("Parser")
var cns1004 = anyStr("statlist")
var cns1005 = anyStr("match")
var cns1006 = anyStr("end")
var cns1007 = anyStr("type")
var cns1008 = anyStr("while")
var cns1009 = anyStr("cond")
var cns1010 = anyStr("body")
var cns1011 = anyStr("check")
var cns1012 = anyStr("do")
var cns1013 = anyStr("next")
var cns1014 = anyStr("Parser")
var cns1015 = anyStr("statlist")
var cns1016 = anyStr("match")
var cns1017 = anyStr("end")
var cns1018 = anyStr("type")
var cns1019 = anyStr("do")
var cns1020 = anyStr("body")
var cns1021 = anyStr("check")
var cns1022 = anyStr("for")
var cns1023 = anyStr("Parser")
var cns1024 = anyStr("forstat")
var cns1025 = anyStr("check")
var cns1026 = anyStr("repeat")
var cns1027 = anyStr("next")
var cns1028 = anyStr("Parser")
var cns1029 = anyStr("statlist")
var cns1030 = anyStr("match")
var cns1031 = anyStr("until")
var cns1032 = anyStr("Parser")
var cns1033 = anyStr("expr")
var cns1034 = anyStr("type")
var cns1035 = anyStr("repeat")
var cns1036 = anyStr("cond")
var cns1037 = anyStr("body")
var cns1038 = anyStr("check")
var cns1039 = anyStr("function")
var cns1040 = anyStr("expect")
var cns1041 = anyStr("function")
var cns1042 = anyStr("Parser")
var cns1043 = anyStr("singlevar")
var cns1044 = anyStr("check")
var cns1045 = anyStr(".")
var cns1046 = anyStr("fieldsel")
var cns1047 = anyStr("check")
var cns1048 = anyStr(":")
var cns1049 = anyStr("fieldsel")
var cns1050 = anyStr("Parser")
var cns1051 = anyStr("funcbody")
var cns1052 = anyStr("type")
var cns1053 = anyStr("funcstat")
var cns1054 = anyStr("lhs")
var cns1055 = anyStr("body")
var cns1056 = anyStr("method")
var cns1057 = anyStr("try")
var cns1058 = anyStr("local")
var cns1059 = anyStr("check")
var cns1060 = anyStr("function")
var cns1061 = anyStr("expect")
var cns1062 = anyStr("function")
var cns1063 = anyStr("get_name")
var cns1064 = anyStr("Parser")
var cns1065 = anyStr("funcbody")
var cns1066 = anyStr("type")
var cns1067 = anyStr("localfunc")
var cns1068 = anyStr("name")
var cns1069 = anyStr("body")
var cns1070 = anyStr("table")
var cns1071 = anyStr("insert")
var cns1072 = anyStr("get_name")
var cns1073 = anyStr("try")
var cns1074 = anyStr(",")
var cns1075 = anyStr("try")
var cns1076 = anyStr("=")
var cns1077 = anyStr("Parser")
var cns1078 = anyStr("explist")
var cns1079 = anyStr("type")
var cns1080 = anyStr("local")
var cns1081 = anyStr("names")
var cns1082 = anyStr("values")
var cns1083 = anyStr("try")
var cns1084 = anyStr("::")
var cns1085 = anyStr("get_name")
var cns1086 = anyStr("expect")
var cns1087 = anyStr("::")
var cns1088 = anyStr("type")
var cns1089 = anyStr("label")
var cns1090 = anyStr("name")
var cns1091 = anyStr("try")
var cns1092 = anyStr("return")
var cns1093 = anyStr("check_not")
var cns1094 = anyStr(";")
var cns1095 = anyStr("end")
var cns1096 = anyStr("else")
var cns1097 = anyStr("elseif")
var cns1098 = anyStr("until")
var cns1099 = anyStr("Parser")
var cns1100 = anyStr("explist")
var cns1101 = anyStr("try")
var cns1102 = anyStr(";")
var cns1103 = anyStr("type")
var cns1104 = anyStr("return")
var cns1105 = anyStr("values")
var cns1106 = anyStr("try")
var cns1107 = anyStr("break")
var cns1108 = anyStr("type")
var cns1109 = anyStr("break")
var cns1110 = anyStr("try")
var cns1111 = anyStr("goto")
var cns1112 = anyStr("get_name")
var cns1113 = anyStr("type")
var cns1114 = anyStr("goto")
var cns1115 = anyStr("name")
var cns1116 = anyStr("Parser")
var cns1117 = anyStr("exprstat")
function Parser_statement (reg_0, reg_1) {
  var reg_9, reg_12, reg_13, reg_18, reg_22, reg_23, reg_28, reg_29, reg_40, reg_41, reg_51, reg_59, reg_62, reg_63, reg_73, reg_76, reg_77, reg_80, reg_81, reg_82, reg_90, reg_91, reg_101, reg_109, reg_112, reg_113, reg_116, reg_117, reg_118, reg_125, reg_126, reg_131, reg_132, reg_143, reg_144, reg_154, reg_162, reg_165, reg_166, reg_176, reg_177, reg_178, reg_179, reg_187, reg_188, reg_195, reg_196, reg_199, reg_207, reg_208, reg_211, reg_212, reg_219, reg_220, reg_225, reg_226, reg_233, reg_234, reg_243, reg_244, reg_246, reg_247, reg_248, reg_249, reg_258, reg_259, reg_266, reg_267, reg_274, reg_275, reg_278, reg_284, reg_289, reg_290, reg_292, reg_293, reg_294, reg_295, reg_301, reg_306, reg_307, reg_316, reg_317, reg_323, reg_326, reg_327, reg_340, reg_341, reg_342, reg_350, reg_351, reg_361, reg_364, reg_365, reg_368, reg_369, reg_370, reg_377, reg_378, reg_383, reg_386, reg_387, reg_406, reg_407, reg_410, reg_411, reg_412, reg_419, reg_420, reg_425, reg_426, reg_427, reg_433, reg_434, reg_444, reg_445, reg_446, reg_447, reg_452, reg_460;
  var goto_23=false, goto_47=false, goto_109=false, goto_154=false, goto_178=false, goto_233=false, goto_328=false, goto_386=false, goto_448=false, goto_484=false, goto_543=false, goto_564=false, goto_593=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_9 = first(call(get(reg_1.a, cns989), newStack()));
  reg_12 = get(reg_1.a, cns990);
  reg_13 = newStack();
  push(reg_13, cns991);
  goto_23 = !tobool(first(call(reg_12, reg_13)));
  if (!goto_23) {
    reg_18 = newStack();
    push(reg_18, nil());
    return reg_18;
  }
  if ((goto_23 || false)) {
    goto_23 = false;
    reg_22 = get(reg_1.a, cns992);
    reg_23 = newStack();
    push(reg_23, cns993);
    goto_47 = !tobool(first(call(reg_22, reg_23)));
    if (!goto_47) {
      reg_28 = newStack();
      reg_29 = newStack();
      append(reg_29, call(get(get(reg_1.a, cns994), cns995), newStack()));
      append(reg_28, call(reg_9, reg_29));
      return reg_28;
    }
    if ((goto_47 || false)) {
      goto_47 = false;
      reg_40 = get(reg_1.a, cns996);
      reg_41 = newStack();
      push(reg_41, cns997);
      goto_109 = !tobool(first(call(reg_40, reg_41)));
      if (!goto_109) {
        reg_51 = first(call(get(reg_1.a, cns998), newStack()));
        reg_59 = first(call(get(get(reg_1.a, cns999), cns1000), newStack()));
        reg_62 = get(reg_1.a, cns1001);
        reg_63 = newStack();
        push(reg_63, cns1002);
        reg_65 = call(reg_62, reg_63);
        reg_73 = first(call(get(get(reg_1.a, cns1003), cns1004), newStack()));
        reg_76 = get(reg_1.a, cns1005);
        reg_77 = newStack();
        push(reg_77, cns1006);
        push(reg_77, reg_51);
        reg_79 = call(reg_76, reg_77);
        reg_80 = newStack();
        reg_81 = newStack();
        reg_82 = newTable();
        set(reg_82, cns1007, cns1008);
        set(reg_82, cns1009, reg_59);
        set(reg_82, cns1010, reg_73);
        push(reg_81, reg_82);
        append(reg_80, call(reg_9, reg_81));
        return reg_80;
      }
      if ((goto_109 || false)) {
        goto_109 = false;
        reg_90 = get(reg_1.a, cns1011);
        reg_91 = newStack();
        push(reg_91, cns1012);
        goto_154 = !tobool(first(call(reg_90, reg_91)));
        if (!goto_154) {
          reg_101 = first(call(get(reg_1.a, cns1013), newStack()));
          reg_109 = first(call(get(get(reg_1.a, cns1014), cns1015), newStack()));
          reg_112 = get(reg_1.a, cns1016);
          reg_113 = newStack();
          push(reg_113, cns1017);
          push(reg_113, reg_101);
          reg_115 = call(reg_112, reg_113);
          reg_116 = newStack();
          reg_117 = newStack();
          reg_118 = newTable();
          set(reg_118, cns1018, cns1019);
          set(reg_118, cns1020, reg_109);
          push(reg_117, reg_118);
          append(reg_116, call(reg_9, reg_117));
          return reg_116;
        }
        if ((goto_154 || false)) {
          goto_154 = false;
          reg_125 = get(reg_1.a, cns1021);
          reg_126 = newStack();
          push(reg_126, cns1022);
          goto_178 = !tobool(first(call(reg_125, reg_126)));
          if (!goto_178) {
            reg_131 = newStack();
            reg_132 = newStack();
            append(reg_132, call(get(get(reg_1.a, cns1023), cns1024), newStack()));
            append(reg_131, call(reg_9, reg_132));
            return reg_131;
          }
          if ((goto_178 || false)) {
            goto_178 = false;
            reg_143 = get(reg_1.a, cns1025);
            reg_144 = newStack();
            push(reg_144, cns1026);
            goto_233 = !tobool(first(call(reg_143, reg_144)));
            if (!goto_233) {
              reg_154 = first(call(get(reg_1.a, cns1027), newStack()));
              reg_162 = first(call(get(get(reg_1.a, cns1028), cns1029), newStack()));
              reg_165 = get(reg_1.a, cns1030);
              reg_166 = newStack();
              push(reg_166, cns1031);
              push(reg_166, reg_154);
              reg_168 = call(reg_165, reg_166);
              reg_176 = first(call(get(get(reg_1.a, cns1032), cns1033), newStack()));
              reg_177 = newStack();
              reg_178 = newStack();
              reg_179 = newTable();
              set(reg_179, cns1034, cns1035);
              set(reg_179, cns1036, reg_176);
              set(reg_179, cns1037, reg_162);
              push(reg_178, reg_179);
              append(reg_177, call(reg_9, reg_178));
              return reg_177;
            }
            if ((goto_233 || false)) {
              goto_233 = false;
              reg_187 = get(reg_1.a, cns1038);
              reg_188 = newStack();
              push(reg_188, cns1039);
              goto_328 = !tobool(first(call(reg_187, reg_188)));
              if (!goto_328) {
                reg_195 = get(reg_1.a, cns1040);
                reg_196 = newStack();
                push(reg_196, cns1041);
                reg_199 = first(call(reg_195, reg_196));
                reg_207 = first(call(get(get(reg_1.a, cns1042), cns1043), newStack()));
                reg_208 = lua$false();
                loop_2: while (true) {
                  reg_211 = get(reg_1.a, cns1044);
                  reg_212 = newStack();
                  push(reg_212, cns1045);
                  if (!tobool(first(call(reg_211, reg_212)))) break loop_2;
                  reg_219 = get(reg_1.a, cns1046);
                  reg_220 = newStack();
                  push(reg_220, reg_207);
                  reg_207 = first(call(reg_219, reg_220));
                }
                reg_225 = get(reg_1.a, cns1047);
                reg_226 = newStack();
                push(reg_226, cns1048);
                if (tobool(first(call(reg_225, reg_226)))) {
                  reg_233 = get(reg_1.a, cns1049);
                  reg_234 = newStack();
                  push(reg_234, reg_207);
                  push(reg_234, lua$true());
                  reg_207 = first(call(reg_233, reg_234));
                  reg_208 = lua$true();
                }
                reg_243 = get(get(reg_1.a, cns1050), cns1051);
                reg_244 = newStack();
                push(reg_244, reg_199);
                reg_246 = first(call(reg_243, reg_244));
                reg_247 = newStack();
                reg_248 = newStack();
                reg_249 = newTable();
                set(reg_249, cns1052, cns1053);
                set(reg_249, cns1054, reg_207);
                set(reg_249, cns1055, reg_246);
                set(reg_249, cns1056, reg_208);
                push(reg_248, reg_249);
                append(reg_247, call(reg_9, reg_248));
                return reg_247;
              }
              if ((goto_328 || false)) {
                goto_328 = false;
                reg_258 = get(reg_1.a, cns1057);
                reg_259 = newStack();
                push(reg_259, cns1058);
                goto_448 = !tobool(first(call(reg_258, reg_259)));
                if (!goto_448) {
                  reg_266 = get(reg_1.a, cns1059);
                  reg_267 = newStack();
                  push(reg_267, cns1060);
                  goto_386 = !tobool(first(call(reg_266, reg_267)));
                  if (!goto_386) {
                    reg_274 = get(reg_1.a, cns1061);
                    reg_275 = newStack();
                    push(reg_275, cns1062);
                    reg_278 = first(call(reg_274, reg_275));
                    reg_284 = first(call(get(reg_1.a, cns1063), newStack()));
                    reg_289 = get(get(reg_1.a, cns1064), cns1065);
                    reg_290 = newStack();
                    push(reg_290, reg_278);
                    reg_292 = first(call(reg_289, reg_290));
                    reg_293 = newStack();
                    reg_294 = newStack();
                    reg_295 = newTable();
                    set(reg_295, cns1066, cns1067);
                    set(reg_295, cns1068, reg_284);
                    set(reg_295, cns1069, reg_292);
                    push(reg_294, reg_295);
                    append(reg_293, call(reg_9, reg_294));
                    return reg_293;
                  }
                  if ((goto_386 || false)) {
                    goto_386 = false;
                    reg_301 = newTable();
                    loop_1: while (true) {
                      reg_306 = get(get(reg_1.a, cns1070), cns1071);
                      reg_307 = newStack();
                      push(reg_307, reg_301);
                      append(reg_307, call(get(reg_1.a, cns1072), newStack()));
                      reg_313 = call(reg_306, reg_307);
                      reg_316 = get(reg_1.a, cns1073);
                      reg_317 = newStack();
                      push(reg_317, cns1074);
                      if (tobool(not(first(call(reg_316, reg_317))))) break loop_1;
                    }
                    reg_323 = newTable();
                    reg_326 = get(reg_1.a, cns1075);
                    reg_327 = newStack();
                    push(reg_327, cns1076);
                    if (tobool(first(call(reg_326, reg_327)))) {
                      reg_323 = first(call(get(get(reg_1.a, cns1077), cns1078), newStack()));
                    }
                    reg_340 = newStack();
                    reg_341 = newStack();
                    reg_342 = newTable();
                    set(reg_342, cns1079, cns1080);
                    set(reg_342, cns1081, reg_301);
                    set(reg_342, cns1082, reg_323);
                    push(reg_341, reg_342);
                    append(reg_340, call(reg_9, reg_341));
                    return reg_340;
                  }
                }
                if ((goto_448 || false)) {
                  goto_448 = false;
                  reg_350 = get(reg_1.a, cns1083);
                  reg_351 = newStack();
                  push(reg_351, cns1084);
                  goto_484 = !tobool(first(call(reg_350, reg_351)));
                  if (!goto_484) {
                    reg_361 = first(call(get(reg_1.a, cns1085), newStack()));
                    reg_364 = get(reg_1.a, cns1086);
                    reg_365 = newStack();
                    push(reg_365, cns1087);
                    reg_367 = call(reg_364, reg_365);
                    reg_368 = newStack();
                    reg_369 = newStack();
                    reg_370 = newTable();
                    set(reg_370, cns1088, cns1089);
                    set(reg_370, cns1090, reg_361);
                    push(reg_369, reg_370);
                    append(reg_368, call(reg_9, reg_369));
                    return reg_368;
                  }
                  if ((goto_484 || false)) {
                    goto_484 = false;
                    reg_377 = get(reg_1.a, cns1091);
                    reg_378 = newStack();
                    push(reg_378, cns1092);
                    goto_543 = !tobool(first(call(reg_377, reg_378)));
                    if (!goto_543) {
                      reg_383 = newTable();
                      reg_386 = get(reg_1.a, cns1093);
                      reg_387 = newStack();
                      push(reg_387, cns1094);
                      push(reg_387, cns1095);
                      push(reg_387, cns1096);
                      push(reg_387, cns1097);
                      push(reg_387, cns1098);
                      if (tobool(first(call(reg_386, reg_387)))) {
                        reg_383 = first(call(get(get(reg_1.a, cns1099), cns1100), newStack()));
                      }
                      reg_406 = get(reg_1.a, cns1101);
                      reg_407 = newStack();
                      push(reg_407, cns1102);
                      reg_409 = call(reg_406, reg_407);
                      reg_410 = newStack();
                      reg_411 = newStack();
                      reg_412 = newTable();
                      set(reg_412, cns1103, cns1104);
                      set(reg_412, cns1105, reg_383);
                      push(reg_411, reg_412);
                      append(reg_410, call(reg_9, reg_411));
                      return reg_410;
                    }
                    if ((goto_543 || false)) {
                      goto_543 = false;
                      reg_419 = get(reg_1.a, cns1106);
                      reg_420 = newStack();
                      push(reg_420, cns1107);
                      goto_564 = !tobool(first(call(reg_419, reg_420)));
                      if (!goto_564) {
                        reg_425 = newStack();
                        reg_426 = newStack();
                        reg_427 = newTable();
                        set(reg_427, cns1108, cns1109);
                        push(reg_426, reg_427);
                        append(reg_425, call(reg_9, reg_426));
                        return reg_425;
                      }
                      if ((goto_564 || false)) {
                        goto_564 = false;
                        reg_433 = get(reg_1.a, cns1110);
                        reg_434 = newStack();
                        push(reg_434, cns1111);
                        goto_593 = !tobool(first(call(reg_433, reg_434)));
                        if (!goto_593) {
                          reg_444 = first(call(get(reg_1.a, cns1112), newStack()));
                          reg_445 = newStack();
                          reg_446 = newStack();
                          reg_447 = newTable();
                          set(reg_447, cns1113, cns1114);
                          set(reg_447, cns1115, reg_444);
                          push(reg_446, reg_447);
                          append(reg_445, call(reg_9, reg_446));
                          return reg_445;
                        }
                        if ((goto_593 || false)) {
                          goto_593 = false;
                          reg_452 = newStack();
                          append(reg_452, call(get(get(reg_1.a, cns1116), cns1117), newStack()));
                          return reg_452;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  reg_460 = newStack();
  return reg_460;
}
var cns1118 = anyStr("Parser")
var cns1119 = anyStr("statlist")
var cns1120 = anyStr("check_not")
var cns1121 = anyStr("end")
var cns1122 = anyStr("else")
var cns1123 = anyStr("elseif")
var cns1124 = anyStr("until")
var cns1125 = anyStr("Parser")
var cns1126 = anyStr("statement")
var cns1127 = anyStr("table")
var cns1128 = anyStr("insert")
var cns1129 = anyStr("type")
var cns1130 = anyStr("return")
function Parser_statlist (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_23, reg_29, reg_30, reg_37;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newTable();
  loop_1: while (true) {
    reg_7 = get(reg_1.a, cns1120);
    reg_8 = newStack();
    push(reg_8, cns1121);
    push(reg_8, cns1122);
    push(reg_8, cns1123);
    push(reg_8, cns1124);
    if (!tobool(first(call(reg_7, reg_8)))) break loop_1;
    reg_23 = first(call(get(get(reg_1.a, cns1125), cns1126), newStack()));
    if (tobool(reg_23)) {
      reg_29 = get(get(reg_1.a, cns1127), cns1128);
      reg_30 = newStack();
      push(reg_30, reg_4);
      push(reg_30, reg_23);
      reg_31 = call(reg_29, reg_30);
      if (tobool(eq(get(reg_23, cns1129), cns1130))) {
        if (true) break loop_1;
      }
    }
  }
  reg_37 = newStack();
  push(reg_37, reg_4);
  return reg_37;
}
var cns1131 = anyStr("Parser")
var cns1132 = anyStr("statlist")
var cns1133 = anyStr("token")
var cns1134 = anyStr("err")
var cns1135 = anyStr("end of file expected")
function function$23 (reg_0, reg_1) {
  var reg_11, reg_18, reg_19, reg_22;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_11 = first(call(get(get(reg_1.a, cns1131), cns1132), newStack()));
  if (tobool(get(reg_1.a, cns1133))) {
    reg_18 = get(reg_1.a, cns1134);
    reg_19 = newStack();
    push(reg_19, cns1135);
    reg_21 = call(reg_18, reg_19);
  }
  reg_22 = newStack();
  push(reg_22, reg_11);
  return reg_22;
}
var cns1136 = anyStr("parse_program")
var cns1137 = anyStr("Parser")
var cns1138 = anyStr("parse")
var record_38 = function (val) { this.val = val; }
var cns1139 = anyStr("xpcall")
var record_39 = function (val) { this.val = val; }
function function$24 (reg_0, reg_1) {
  var reg_5, reg_6;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  push(reg_6, lua$true());
  append(reg_6, call(reg_5, newStack()));
  return reg_6;
}
var cns1140 = anyStr("xpcall")
var cns1141 = anyStr("xpcall")
var cns1142 = anyStr("parse_program")
var cns1143 = anyStr("Parser")
var cns1144 = anyStr("error")
var cns1145 = anyStr("debug")
var cns1146 = anyStr("traceback")
var cns1147 = parseNum("2")
var cns1148 = anyStr("Parser")
var cns1149 = anyStr("trace")
var cns1150 = anyStr("debug")
var cns1151 = anyStr("traceback")
var cns1152 = parseNum("4")
function function$25 (reg_0, reg_1) {
  var reg_2, reg_5, reg_18, reg_19, reg_25, reg_26, reg_31, reg_32, reg_36;
  var goto_26=false;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  goto_26 = !tobool(eq(get(get(reg_2.a, cns1143), cns1144), nil()));
  if (!goto_26) {
    reg_18 = get(get(reg_2.a, cns1145), cns1146);
    reg_19 = newStack();
    push(reg_19, reg_5);
    push(reg_19, cns1147);
    reg_1.b = first(call(reg_18, reg_19));
  }
  if ((goto_26 || false)) {
    goto_26 = false;
    reg_25 = get(reg_2.a, cns1148);
    reg_26 = cns1149;
    reg_31 = get(get(reg_2.a, cns1150), cns1151);
    reg_32 = newStack();
    push(reg_32, reg_5);
    push(reg_32, cns1152);
    set(reg_25, reg_26, first(call(reg_31, reg_32)));
  }
  reg_36 = newStack();
  return reg_36;
}
var cns1153 = anyStr("status")
var cns1154 = anyStr("prog")
var cns1155 = anyStr("print")
var cns1156 = anyStr("os")
var cns1157 = anyStr("exit")
var cns1158 = parseNum("1")
var cns1159 = anyStr("status")
var cns1160 = anyStr("prog")
var cns1161 = anyStr("Parser")
var cns1162 = anyStr("error")
var cns1163 = anyStr("tk")
var cns1164 = anyStr("error")
var cns1165 = anyStr("tk")
function Parser_parse (reg_0, reg_1) {
  var reg_3, reg_11, reg_16, reg_17, reg_23, reg_24, reg_27, reg_34, reg_35, reg_42, reg_43, reg_50, reg_60, reg_67, reg_68, reg_73;
  var goto_68=false, goto_84=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_3.b = nil();
  if (tobool(not(get(reg_1.a, cns1139)))) {
    reg_11 = anyFn((function (a) { return function$24(a,this) }).bind(reg_3));
    set(reg_1.a, cns1140, reg_11);
  }
  reg_16 = get(reg_1.a, cns1141);
  reg_17 = newStack();
  push(reg_17, get(reg_1.a, cns1142));
  push(reg_17, anyFn((function (a) { return function$25(a,this) }).bind(reg_3)));
  reg_23 = call(reg_16, reg_17);
  reg_24 = next(reg_23);
  set(reg_1.a, cns1153, reg_24);
  reg_27 = next(reg_23);
  set(reg_1.a, cns1154, reg_27);
  if (tobool(reg_3.b)) {
    reg_34 = get(reg_1.a, cns1155);
    reg_35 = newStack();
    push(reg_35, reg_3.b);
    reg_37 = call(reg_34, reg_35);
    reg_42 = get(get(reg_1.a, cns1156), cns1157);
    reg_43 = newStack();
    push(reg_43, cns1158);
    reg_45 = call(reg_42, reg_43);
  }
  goto_68 = !tobool(get(reg_1.a, cns1159));
  if (!goto_68) {
    reg_50 = newStack();
    push(reg_50, get(reg_1.a, cns1160));
    return reg_50;
  }
  if ((goto_68 || false)) {
    goto_68 = false;
    goto_84 = !tobool(get(get(reg_1.a, cns1161), cns1162));
    if (!goto_84) {
      reg_60 = newStack();
      push(reg_60, nil());
      push(reg_60, get(reg_1.a, cns1163));
      return reg_60;
    }
    if ((goto_84 || false)) {
      goto_84 = false;
      reg_67 = get(reg_1.a, cns1164);
      reg_68 = newStack();
      push(reg_68, get(reg_1.a, cns1165));
      reg_72 = call(reg_67, reg_68);
    }
  }
  reg_73 = newStack();
  return reg_73;
}
var cns1166 = anyStr("Parser")
function lua_parser_parser$lua_main (reg_0) {
  var reg_2, reg_5, reg_6, reg_8, reg_14, reg_17, reg_22, reg_23, reg_27, reg_31, reg_35, reg_39, reg_43, reg_47, reg_51, reg_55, reg_59, reg_63, reg_68, reg_69, reg_73, reg_77, reg_82, reg_83, reg_88, reg_89, reg_92, reg_93, reg_94, reg_99, reg_100, reg_105, reg_106, reg_111, reg_112, reg_117, reg_118, reg_123, reg_124, reg_129, reg_130, reg_135, reg_136, reg_141, reg_142, reg_147, reg_148, reg_153, reg_154, reg_159, reg_160, reg_165, reg_166, reg_171, reg_172, reg_177, reg_178, reg_183, reg_184, reg_189, reg_190, reg_195, reg_196, reg_201, reg_202, reg_207, reg_208, reg_213, reg_214, reg_221, reg_226, reg_227, reg_232, reg_233, reg_238, reg_239, reg_243, reg_248, reg_249, reg_254, reg_255, reg_260, reg_261, reg_266, reg_267, reg_272, reg_273, reg_277, reg_282, reg_283, reg_286;
  reg_2 = {a: nil()};
  reg_2.a = reg_0;
  reg_5 = get(reg_2.a, cns1);
  reg_6 = newStack();
  push(reg_6, newTable());
  reg_8 = newTable();
  set(reg_8, cns4, reg_2.a);
  push(reg_6, reg_8);
  reg_2.a = first(call(reg_5, reg_6));
  reg_14 = first(lua_parser_lexer$lua_main(reg_0));
  set(reg_2.a, cns390, reg_14);
  reg_17 = newTable();
  set(reg_2.a, cns391, reg_17);
  reg_22 = get(reg_2.a, cns392);
  reg_23 = cns393;
  set(reg_22, reg_23, anyFn((function (a) { return Parser_open(a,this) }).bind(reg_2)));
  reg_27 = anyFn((function (a) { return lua_parser_parser$function(a,this) }).bind(reg_2));
  set(reg_2.a, cns414, reg_27);
  reg_31 = anyFn((function (a) { return function$10(a,this) }).bind(reg_2));
  set(reg_2.a, cns430, reg_31);
  reg_35 = anyFn((function (a) { return function$11(a,this) }).bind(reg_2));
  set(reg_2.a, cns436, reg_35);
  reg_39 = anyFn((function (a) { return function$12(a,this) }).bind(reg_2));
  set(reg_2.a, cns439, reg_39);
  reg_43 = anyFn((function (a) { return function$13(a,this) }).bind(reg_2));
  set(reg_2.a, cns442, reg_43);
  reg_47 = anyFn((function (a) { return function$14(a,this) }).bind(reg_2));
  set(reg_2.a, cns451, reg_47);
  reg_51 = anyFn((function (a) { return function$15(a,this) }).bind(reg_2));
  set(reg_2.a, cns459, reg_51);
  reg_55 = anyFn((function (a) { return function$16(a,this) }).bind(reg_2));
  set(reg_2.a, cns463, reg_55);
  reg_59 = anyFn((function (a) { return function$17(a,this) }).bind(reg_2));
  set(reg_2.a, cns469, reg_59);
  reg_63 = anyFn((function (a) { return function$18(a,this) }).bind(reg_2));
  set(reg_2.a, cns479, reg_63);
  reg_68 = get(reg_2.a, cns480);
  reg_69 = cns481;
  set(reg_68, reg_69, anyFn((function (a) { return Parser_singlevar(a,this) }).bind(reg_2)));
  reg_73 = anyFn((function (a) { return function$20(a,this) }).bind(reg_2));
  set(reg_2.a, cns540, reg_73);
  reg_77 = anyFn((function (a) { return function$21(a,this) }).bind(reg_2));
  set(reg_2.a, cns575, reg_77);
  reg_82 = get(reg_2.a, cns576);
  reg_83 = cns577;
  set(reg_82, reg_83, anyFn((function (a) { return Parser_suffixedexp(a,this) }).bind(reg_2)));
  reg_88 = get(reg_2.a, cns612);
  reg_89 = cns613;
  set(reg_88, reg_89, anyFn((function (a) { return Parser_simpleexp(a,this) }).bind(reg_2)));
  reg_92 = newTable();
  reg_93 = cns653;
  reg_94 = newTable();
  set(reg_94, cns654, cns655);
  set(reg_94, cns656, cns657);
  set(reg_92, reg_93, reg_94);
  reg_99 = cns658;
  reg_100 = newTable();
  set(reg_100, cns659, cns660);
  set(reg_100, cns661, cns662);
  set(reg_92, reg_99, reg_100);
  reg_105 = cns663;
  reg_106 = newTable();
  set(reg_106, cns664, cns665);
  set(reg_106, cns666, cns667);
  set(reg_92, reg_105, reg_106);
  reg_111 = cns668;
  reg_112 = newTable();
  set(reg_112, cns669, cns670);
  set(reg_112, cns671, cns672);
  set(reg_92, reg_111, reg_112);
  reg_117 = cns673;
  reg_118 = newTable();
  set(reg_118, cns674, cns675);
  set(reg_118, cns676, cns677);
  set(reg_92, reg_117, reg_118);
  reg_123 = cns678;
  reg_124 = newTable();
  set(reg_124, cns679, cns680);
  set(reg_124, cns681, cns682);
  set(reg_92, reg_123, reg_124);
  reg_129 = cns683;
  reg_130 = newTable();
  set(reg_130, cns684, cns685);
  set(reg_130, cns686, cns687);
  set(reg_92, reg_129, reg_130);
  reg_135 = cns688;
  reg_136 = newTable();
  set(reg_136, cns689, cns690);
  set(reg_136, cns691, cns692);
  set(reg_92, reg_135, reg_136);
  reg_141 = cns693;
  reg_142 = newTable();
  set(reg_142, cns694, cns695);
  set(reg_142, cns696, cns697);
  set(reg_92, reg_141, reg_142);
  reg_147 = cns698;
  reg_148 = newTable();
  set(reg_148, cns699, cns700);
  set(reg_148, cns701, cns702);
  set(reg_92, reg_147, reg_148);
  reg_153 = cns703;
  reg_154 = newTable();
  set(reg_154, cns704, cns705);
  set(reg_154, cns706, cns707);
  set(reg_92, reg_153, reg_154);
  reg_159 = cns708;
  reg_160 = newTable();
  set(reg_160, cns709, cns710);
  set(reg_160, cns711, cns712);
  set(reg_92, reg_159, reg_160);
  reg_165 = cns713;
  reg_166 = newTable();
  set(reg_166, cns714, cns715);
  set(reg_166, cns716, cns717);
  set(reg_92, reg_165, reg_166);
  reg_171 = cns718;
  reg_172 = newTable();
  set(reg_172, cns719, cns720);
  set(reg_172, cns721, cns722);
  set(reg_92, reg_171, reg_172);
  reg_177 = cns723;
  reg_178 = newTable();
  set(reg_178, cns724, cns725);
  set(reg_178, cns726, cns727);
  set(reg_92, reg_177, reg_178);
  reg_183 = cns728;
  reg_184 = newTable();
  set(reg_184, cns729, cns730);
  set(reg_184, cns731, cns732);
  set(reg_92, reg_183, reg_184);
  reg_189 = cns733;
  reg_190 = newTable();
  set(reg_190, cns734, cns735);
  set(reg_190, cns736, cns737);
  set(reg_92, reg_189, reg_190);
  reg_195 = cns738;
  reg_196 = newTable();
  set(reg_196, cns739, cns740);
  set(reg_196, cns741, cns742);
  set(reg_92, reg_195, reg_196);
  reg_201 = cns743;
  reg_202 = newTable();
  set(reg_202, cns744, cns745);
  set(reg_202, cns746, cns747);
  set(reg_92, reg_201, reg_202);
  reg_207 = cns748;
  reg_208 = newTable();
  set(reg_208, cns749, cns750);
  set(reg_208, cns751, cns752);
  set(reg_92, reg_207, reg_208);
  reg_213 = cns753;
  reg_214 = newTable();
  set(reg_214, cns754, cns755);
  set(reg_214, cns756, cns757);
  set(reg_92, reg_213, reg_214);
  set(reg_2.a, cns758, reg_92);
  reg_221 = cns759;
  set(reg_2.a, cns760, reg_221);
  reg_226 = get(reg_2.a, cns761);
  reg_227 = cns762;
  set(reg_226, reg_227, anyFn((function (a) { return Parser_expr(a,this) }).bind(reg_2)));
  reg_232 = get(reg_2.a, cns801);
  reg_233 = cns802;
  set(reg_232, reg_233, anyFn((function (a) { return Parser_explist(a,this) }).bind(reg_2)));
  reg_238 = get(reg_2.a, cns809);
  reg_239 = cns810;
  set(reg_238, reg_239, anyFn((function (a) { return Parser_funcbody(a,this) }).bind(reg_2)));
  reg_243 = anyFn((function (a) { return function$22(a,this) }).bind(reg_2));
  set(reg_2.a, cns848, reg_243);
  reg_248 = get(reg_2.a, cns849);
  reg_249 = cns850;
  set(reg_248, reg_249, anyFn((function (a) { return Parser_exprstat(a,this) }).bind(reg_2)));
  reg_254 = get(reg_2.a, cns884);
  reg_255 = cns885;
  set(reg_254, reg_255, anyFn((function (a) { return Parser_forstat(a,this) }).bind(reg_2)));
  reg_260 = get(reg_2.a, cns947);
  reg_261 = cns948;
  set(reg_260, reg_261, anyFn((function (a) { return Parser_ifstat(a,this) }).bind(reg_2)));
  reg_266 = get(reg_2.a, cns987);
  reg_267 = cns988;
  set(reg_266, reg_267, anyFn((function (a) { return Parser_statement(a,this) }).bind(reg_2)));
  reg_272 = get(reg_2.a, cns1118);
  reg_273 = cns1119;
  set(reg_272, reg_273, anyFn((function (a) { return Parser_statlist(a,this) }).bind(reg_2)));
  reg_277 = anyFn((function (a) { return function$23(a,this) }).bind(reg_2));
  set(reg_2.a, cns1136, reg_277);
  reg_282 = get(reg_2.a, cns1137);
  reg_283 = cns1138;
  set(reg_282, reg_283, anyFn((function (a) { return Parser_parse(a,this) }).bind(reg_2)));
  reg_286 = newStack();
  push(reg_286, get(reg_2.a, cns1166));
  return reg_286;
}
var cns1167 = parseNum("1")
var cns1168 = anyStr("")
var cns1169 = anyStr("type")
var cns1170 = anyStr("table")
var cns1171 = parseNum("0")
var cns1172 = anyStr("tostring")
var cns1173 = parseNum("0")
var cns1174 = anyStr("[")
var cns1175 = parseNum("1")
var cns1176 = parseNum("1")
var cns1177 = parseNum("0")
function lt (reg_0, reg_1) {
  var reg_3;
  reg_3 = _lt(reg_0, reg_1);
  return reg_3;
}
var cns1178 = parseNum("1")
var cns1179 = anyStr(",")
var cns1180 = anyStr("\n")
var cns1181 = anyStr("  ")
var cns1182 = anyStr("tostr")
var cns1183 = parseNum("1")
var cns1184 = anyStr("  ")
var cns1185 = anyStr("pairs")
var cns1186 = anyStr("type")
var cns1187 = anyStr("number")
var cns1188 = anyStr(",\n")
var cns1189 = anyStr("  ")
var cns1190 = anyStr("tostring")
var cns1191 = anyStr(" = ")
var cns1192 = anyStr("tostr")
var cns1193 = parseNum("1")
var cns1194 = anyStr("  ")
var cns1195 = anyStr("\n")
var cns1196 = anyStr("]")
var cns1197 = anyStr("{")
var cns1198 = anyStr("pairs")
var cns1199 = anyStr(",")
var cns1200 = anyStr("\n")
var cns1201 = anyStr("  ")
var cns1202 = anyStr("tostring")
var cns1203 = anyStr(" = ")
var cns1204 = anyStr("tostr")
var cns1205 = parseNum("1")
var cns1206 = anyStr("  ")
var cns1207 = anyStr("\n")
var cns1208 = anyStr("}")
var cns1209 = anyStr("[]")
function aulua_helpers$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_15, reg_16, reg_20, reg_25, reg_28, reg_29, reg_35, reg_36, reg_37, reg_38, reg_40, reg_51, reg_52, reg_55, reg_56, reg_71, reg_72, reg_73, reg_74, reg_75, reg_76, reg_77, reg_78, reg_79, reg_80, reg_86, reg_87, reg_93, reg_94, reg_97, reg_98, reg_100, reg_101, reg_104, reg_105, reg_118, reg_119, reg_124, reg_125, reg_128, reg_129, reg_130, reg_131, reg_132, reg_133, reg_134, reg_135, reg_136, reg_137, reg_145, reg_146, reg_149, reg_150, reg_152, reg_153, reg_156, reg_157, reg_170, reg_176, reg_178;
  var goto_60=false, goto_194=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = reg_5;
  if (!tobool(reg_5)) {
    reg_7 = cns1167;
  }
  reg_5 = reg_7;
  reg_10 = reg_6;
  if (!tobool(reg_6)) {
    reg_10 = cns1168;
  }
  reg_6 = reg_10;
  reg_15 = get(reg_1.a, cns1169);
  reg_16 = newStack();
  push(reg_16, reg_4);
  reg_20 = ne(first(call(reg_15, reg_16)), cns1170);
  if (!tobool(reg_20)) {
    reg_20 = eq(reg_5, cns1171);
  }
  if (tobool(reg_20)) {
    reg_25 = newStack();
    reg_28 = get(reg_1.a, cns1172);
    reg_29 = newStack();
    push(reg_29, reg_4);
    append(reg_25, call(reg_28, reg_29));
    return reg_25;
  }
  if (tobool(gt(length(reg_4), cns1173))) {
    reg_35 = cns1174;
    reg_36 = cns1175;
    reg_37 = length(reg_4);
    reg_38 = cns1176;
    reg_40 = lt(reg_38, cns1177);
    loop_3: while (true) {
      goto_60 = tobool(reg_40);
      if (!goto_60) {
        if (tobool(gt(reg_36, reg_37))) break loop_3;
      }
      if ((goto_60 || false)) {
        goto_60 = false;
        if (tobool(lt(reg_36, reg_37))) break loop_3;
      }
      if (tobool(gt(reg_36, cns1178))) {
        reg_35 = concat(reg_35, cns1179);
      }
      reg_51 = cns1180;
      reg_52 = cns1181;
      reg_55 = get(reg_1.a, cns1182);
      reg_56 = newStack();
      push(reg_56, get(reg_4, reg_36));
      push(reg_56, sub(reg_5, cns1183));
      push(reg_56, concat(reg_6, cns1184));
      reg_35 = concat(reg_35, concat(reg_51, concat(reg_6, concat(reg_52, first(call(reg_55, reg_56))))));
      reg_36 = add(reg_36, reg_38);
    }
    reg_71 = get(reg_1.a, cns1185);
    reg_72 = newStack();
    push(reg_72, reg_4);
    reg_73 = call(reg_71, reg_72);
    reg_74 = next(reg_73);
    reg_75 = next(reg_73);
    reg_76 = next(reg_73);
    loop_2: while (true) {
      reg_77 = newStack();
      push(reg_77, reg_75);
      push(reg_77, reg_76);
      reg_78 = call(reg_74, reg_77);
      reg_79 = next(reg_78);
      reg_76 = reg_79;
      reg_80 = next(reg_78);
      if (tobool(eq(reg_76, nil()))) break loop_2;
      reg_86 = get(reg_1.a, cns1186);
      reg_87 = newStack();
      push(reg_87, reg_79);
      if (tobool(ne(first(call(reg_86, reg_87)), cns1187))) {
        reg_93 = cns1188;
        reg_94 = cns1189;
        reg_97 = get(reg_1.a, cns1190);
        reg_98 = newStack();
        push(reg_98, reg_79);
        reg_100 = first(call(reg_97, reg_98));
        reg_101 = cns1191;
        reg_104 = get(reg_1.a, cns1192);
        reg_105 = newStack();
        push(reg_105, reg_80);
        push(reg_105, sub(reg_5, cns1193));
        push(reg_105, concat(reg_6, cns1194));
        reg_35 = concat(reg_35, concat(reg_93, concat(reg_6, concat(reg_94, concat(reg_100, concat(reg_101, first(call(reg_104, reg_105))))))));
      }
    }
    reg_118 = newStack();
    reg_119 = cns1195;
    push(reg_118, concat(reg_35, concat(reg_119, concat(reg_6, cns1196))));
    return reg_118;
  }
  reg_124 = lua$true();
  reg_125 = cns1197;
  reg_128 = get(reg_1.a, cns1198);
  reg_129 = newStack();
  push(reg_129, reg_4);
  reg_130 = call(reg_128, reg_129);
  reg_131 = next(reg_130);
  reg_132 = next(reg_130);
  reg_133 = next(reg_130);
  loop_1: while (true) {
    reg_134 = newStack();
    push(reg_134, reg_132);
    push(reg_134, reg_133);
    reg_135 = call(reg_131, reg_134);
    reg_136 = next(reg_135);
    reg_133 = reg_136;
    reg_137 = next(reg_135);
    if (tobool(eq(reg_133, nil()))) break loop_1;
    goto_194 = !tobool(reg_124);
    if (!goto_194) {
      reg_124 = lua$false();
    }
    if ((goto_194 || false)) {
      goto_194 = false;
      reg_125 = concat(reg_125, cns1199);
    }
    reg_145 = cns1200;
    reg_146 = cns1201;
    reg_149 = get(reg_1.a, cns1202);
    reg_150 = newStack();
    push(reg_150, reg_136);
    reg_152 = first(call(reg_149, reg_150));
    reg_153 = cns1203;
    reg_156 = get(reg_1.a, cns1204);
    reg_157 = newStack();
    push(reg_157, reg_137);
    push(reg_157, sub(reg_5, cns1205));
    push(reg_157, concat(reg_6, cns1206));
    reg_125 = concat(reg_125, concat(reg_145, concat(reg_6, concat(reg_146, concat(reg_152, concat(reg_153, first(call(reg_156, reg_157))))))));
  }
  reg_170 = cns1207;
  reg_125 = concat(reg_125, concat(reg_170, concat(reg_6, cns1208)));
  if (tobool(reg_124)) {
    reg_176 = newStack();
    push(reg_176, cns1209);
    return reg_176;
  }
  reg_178 = newStack();
  push(reg_178, reg_125);
  return reg_178;
}
var cns1210 = anyStr("tostr")
var cns1211 = anyStr(", at ")
var cns1212 = anyStr("line")
var cns1213 = anyStr(":")
var cns1214 = anyStr("column")
var cns1215 = anyStr("error")
function function$26 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_9, reg_10, reg_19, reg_20, reg_22;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  if (tobool(reg_5)) {
    reg_7 = cns1211;
    reg_9 = get(reg_5, cns1212);
    reg_10 = cns1213;
    reg_4 = concat(reg_4, concat(reg_7, concat(reg_9, concat(reg_10, get(reg_5, cns1214)))));
  }
  reg_19 = get(reg_1.a, cns1215);
  reg_20 = newStack();
  push(reg_20, reg_4);
  reg_21 = call(reg_19, reg_20);
  reg_22 = newStack();
  return reg_22;
}
var cns1216 = anyStr("err")
function aulua_helpers$lua_main (reg_0) {
  var reg_2, reg_4, reg_8, reg_11;
  reg_2 = {a: nil()};
  reg_2.a = reg_0;
  reg_4 = anyFn((function (a) { return aulua_helpers$function(a,this) }).bind(reg_2));
  set(reg_2.a, cns1210, reg_4);
  reg_8 = anyFn((function (a) { return function$26(a,this) }).bind(reg_2));
  set(reg_2.a, cns1216, reg_8);
  reg_11 = newStack();
  return reg_11;
}
var cns1217 = anyStr("Module")
var cns1218 = anyStr("Module")
var cns1219 = anyStr("__index")
var cns1220 = anyStr("Module")
var cns1221 = anyStr("Module")
var cns1222 = anyStr("type")
var record_40 = function (val) { this.val = val; }
var cns1223 = anyStr("id")
var cns1224 = anyStr("types")
var cns1225 = anyStr("name")
var cns1226 = anyStr("module")
var cns1227 = anyStr("id")
var cns1228 = anyStr("table")
var cns1229 = anyStr("insert")
var cns1230 = anyStr("types")
var cns1231 = anyStr("setmetatable")
var cns1232 = anyStr("__tostring")
var record_41 = function (val) { this.val = val; }
var cns1233 = anyStr("fullname")
var cns1234 = anyStr(".")
function aulua_basics$function (reg_0, reg_1) {
  var reg_6, reg_7, reg_9, reg_10, reg_12;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = reg_1.b;
  reg_9 = get(reg_7, cns1233);
  reg_10 = newStack();
  push(reg_10, reg_7);
  reg_12 = first(call(reg_9, reg_10));
  push(reg_6, concat(reg_12, concat(cns1234, reg_1.c)));
  return reg_6;
}
function Module_type (reg_0, reg_1) {
  var reg_2, reg_3, reg_6, reg_7, reg_14, reg_22, reg_23, reg_30, reg_31, reg_32, reg_33, reg_37;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newTable();
  reg_7 = cns1223;
  set(reg_6, reg_7, length(get(reg_1.a, cns1224)));
  set(reg_6, cns1225, reg_3.c);
  reg_14 = cns1226;
  set(reg_6, reg_14, get(reg_3.b, cns1227));
  reg_22 = get(get(reg_1.a, cns1228), cns1229);
  reg_23 = newStack();
  push(reg_23, get(reg_1.a, cns1230));
  push(reg_23, reg_6);
  reg_27 = call(reg_22, reg_23);
  reg_30 = get(reg_1.a, cns1231);
  reg_31 = newStack();
  push(reg_31, reg_6);
  reg_32 = newTable();
  reg_33 = cns1232;
  set(reg_32, reg_33, anyFn((function (a) { return aulua_basics$function(a,this) }).bind(reg_3)));
  push(reg_31, reg_32);
  reg_36 = call(reg_30, reg_31);
  reg_37 = newStack();
  push(reg_37, reg_6);
  return reg_37;
}
var cns1235 = anyStr("Module")
var cns1236 = anyStr("func")
var cns1237 = anyStr("_id")
var cns1238 = anyStr("funcs")
var cns1239 = anyStr("name")
var cns1240 = anyStr("ins")
var cns1241 = anyStr("outs")
var cns1242 = anyStr("module")
var cns1243 = anyStr("id")
var cns1244 = anyStr("id")
var cns1245 = anyStr("_id")
function f_id (reg_0, reg_1) {
  var reg_5, reg_6;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  push(reg_6, get(reg_5, cns1245));
  return reg_6;
}
var cns1246 = anyStr("ipairs")
var cns1247 = anyStr("ins")
var cns1248 = anyStr("id")
var cns1249 = anyStr("ipairs")
var cns1250 = anyStr("outs")
var cns1251 = anyStr("id")
var cns1252 = anyStr("table")
var cns1253 = anyStr("insert")
var cns1254 = anyStr("funcs")
var cns1255 = anyStr("setmetatable")
var cns1256 = anyStr("__tostring")
var cns1257 = anyStr("fullname")
var cns1258 = anyStr(".")
function function$27 (reg_0, reg_1) {
  var reg_6, reg_7, reg_9, reg_10, reg_12;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = reg_1.b;
  reg_9 = get(reg_7, cns1257);
  reg_10 = newStack();
  push(reg_10, reg_7);
  reg_12 = first(call(reg_9, reg_10));
  push(reg_6, concat(reg_12, concat(cns1258, reg_1.c)));
  return reg_6;
}
function Module_func (reg_0, reg_1) {
  var reg_2, reg_3, reg_6, reg_7, reg_8, reg_9, reg_20, reg_24, reg_29, reg_30, reg_31, reg_32, reg_33, reg_34, reg_35, reg_36, reg_37, reg_38, reg_43, reg_48, reg_49, reg_50, reg_51, reg_52, reg_53, reg_54, reg_55, reg_56, reg_57, reg_62, reg_69, reg_70, reg_77, reg_78, reg_79, reg_80, reg_84;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = newTable();
  reg_9 = cns1237;
  set(reg_8, reg_9, length(get(reg_1.a, cns1238)));
  set(reg_8, cns1239, reg_3.c);
  set(reg_8, cns1240, newTable());
  set(reg_8, cns1241, newTable());
  reg_20 = cns1242;
  set(reg_8, reg_20, get(reg_3.b, cns1243));
  reg_24 = cns1244;
  set(reg_8, reg_24, anyFn((function (a) { return f_id(a,this) }).bind(reg_3)));
  reg_29 = get(reg_1.a, cns1246);
  reg_30 = newStack();
  push(reg_30, reg_6);
  reg_31 = call(reg_29, reg_30);
  reg_32 = next(reg_31);
  reg_33 = next(reg_31);
  reg_34 = next(reg_31);
  loop_2: while (true) {
    reg_35 = newStack();
    push(reg_35, reg_33);
    push(reg_35, reg_34);
    reg_36 = call(reg_32, reg_35);
    reg_37 = next(reg_36);
    reg_34 = reg_37;
    reg_38 = next(reg_36);
    if (tobool(eq(reg_34, nil()))) break loop_2;
    reg_43 = get(reg_8, cns1247);
    set(reg_43, reg_37, get(reg_38, cns1248));
  }
  reg_48 = get(reg_1.a, cns1249);
  reg_49 = newStack();
  push(reg_49, reg_7);
  reg_50 = call(reg_48, reg_49);
  reg_51 = next(reg_50);
  reg_52 = next(reg_50);
  reg_53 = next(reg_50);
  loop_1: while (true) {
    reg_54 = newStack();
    push(reg_54, reg_52);
    push(reg_54, reg_53);
    reg_55 = call(reg_51, reg_54);
    reg_56 = next(reg_55);
    reg_53 = reg_56;
    reg_57 = next(reg_55);
    if (tobool(eq(reg_53, nil()))) break loop_1;
    reg_62 = get(reg_8, cns1250);
    set(reg_62, reg_56, get(reg_57, cns1251));
  }
  reg_69 = get(get(reg_1.a, cns1252), cns1253);
  reg_70 = newStack();
  push(reg_70, get(reg_1.a, cns1254));
  push(reg_70, reg_8);
  reg_74 = call(reg_69, reg_70);
  reg_77 = get(reg_1.a, cns1255);
  reg_78 = newStack();
  push(reg_78, reg_8);
  reg_79 = newTable();
  reg_80 = cns1256;
  set(reg_79, reg_80, anyFn((function (a) { return function$27(a,this) }).bind(reg_3)));
  push(reg_78, reg_79);
  reg_83 = call(reg_77, reg_78);
  reg_84 = newStack();
  push(reg_84, reg_8);
  return reg_84;
}
var cns1259 = anyStr("Module")
var cns1260 = anyStr("fullname")
var record_42 = function (val) { this.val = val; }
var cns1261 = anyStr("base")
var cns1262 = anyStr("ipairs")
var cns1263 = anyStr("argument")
var cns1264 = anyStr("items")
var cns1265 = anyStr("name")
var cns1266 = anyStr("=")
var cns1267 = anyStr("tostring")
var cns1268 = anyStr("tp")
var cns1269 = anyStr("fn")
var cns1270 = anyStr("¿?")
var cns1271 = anyStr("base")
var cns1272 = anyStr("name")
var cns1273 = anyStr("(")
var cns1274 = anyStr("table")
var cns1275 = anyStr("concat")
var cns1276 = anyStr(" ")
var cns1277 = anyStr(")")
var cns1278 = anyStr("name")
var cns1279 = anyStr("<unknown>")
function Module_fullname (reg_0, reg_1) {
  var reg_4, reg_8, reg_11, reg_12, reg_17, reg_18, reg_19, reg_20, reg_21, reg_22, reg_24, reg_28, reg_30, reg_31, reg_34, reg_35, reg_37, reg_47, reg_51, reg_52, reg_57, reg_58, reg_66, reg_68, reg_71;
  var goto_82=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  goto_82 = !tobool(get(reg_4, cns1261));
  if (!goto_82) {
    reg_8 = newTable();
    reg_11 = get(reg_1.a, cns1262);
    reg_12 = newStack();
    push(reg_12, get(get(reg_4, cns1263), cns1264));
    reg_17 = call(reg_11, reg_12);
    reg_18 = next(reg_17);
    reg_19 = next(reg_17);
    reg_20 = next(reg_17);
    loop_1: while (true) {
      reg_21 = newStack();
      push(reg_21, reg_19);
      push(reg_21, reg_20);
      reg_22 = call(reg_18, reg_21);
      reg_20 = next(reg_22);
      reg_24 = next(reg_22);
      if (tobool(eq(reg_20, nil()))) break loop_1;
      reg_28 = length(reg_8);
      reg_30 = get(reg_24, cns1265);
      reg_31 = cns1266;
      reg_34 = get(reg_1.a, cns1267);
      reg_35 = newStack();
      reg_37 = get(reg_24, cns1268);
      if (!tobool(reg_37)) {
        reg_37 = get(reg_24, cns1269);
      }
      if (!tobool(reg_37)) {
        reg_37 = cns1270;
      }
      push(reg_35, reg_37);
      set(reg_8, reg_28, concat(reg_30, concat(reg_31, first(call(reg_34, reg_35)))));
    }
    reg_47 = newStack();
    reg_51 = get(get(reg_4, cns1271), cns1272);
    reg_52 = cns1273;
    reg_57 = get(get(reg_1.a, cns1274), cns1275);
    reg_58 = newStack();
    push(reg_58, reg_8);
    push(reg_58, cns1276);
    push(reg_47, concat(reg_51, concat(reg_52, concat(first(call(reg_57, reg_58)), cns1277))));
    return reg_47;
  }
  if ((goto_82 || false)) {
    goto_82 = false;
    reg_66 = newStack();
    reg_68 = get(reg_4, cns1278);
    if (!tobool(reg_68)) {
      reg_68 = cns1279;
    }
    push(reg_66, reg_68);
    return reg_66;
  }
  reg_71 = newStack();
  return reg_71;
}
var cns1280 = anyStr("id")
var cns1281 = anyStr("modules")
var cns1282 = parseNum("2")
var cns1283 = anyStr("name")
var cns1284 = anyStr("setmetatable")
var cns1285 = anyStr("Module")
var cns1286 = anyStr("table")
var cns1287 = anyStr("insert")
var cns1288 = anyStr("modules")
function function$28 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_16, reg_17, reg_26, reg_27, reg_32;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  reg_6 = cns1280;
  set(reg_5, reg_6, add(length(get(reg_1.a, cns1281)), cns1282));
  set(reg_5, cns1283, reg_4);
  reg_16 = get(reg_1.a, cns1284);
  reg_17 = newStack();
  push(reg_17, reg_5);
  push(reg_17, get(reg_1.a, cns1285));
  reg_21 = call(reg_16, reg_17);
  reg_26 = get(get(reg_1.a, cns1286), cns1287);
  reg_27 = newStack();
  push(reg_27, get(reg_1.a, cns1288));
  push(reg_27, reg_5);
  reg_31 = call(reg_26, reg_27);
  reg_32 = newStack();
  push(reg_32, reg_5);
  return reg_32;
}
var cns1289 = anyStr("module")
var cns1290 = anyStr("Function")
var cns1291 = anyStr("Function")
var cns1292 = anyStr("__index")
var cns1293 = anyStr("Function")
var cns1294 = anyStr("Function")
var cns1295 = anyStr("id")
var cns1296 = anyStr("_id")
function Function_id (reg_0, reg_1) {
  var reg_4, reg_5;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  push(reg_5, get(reg_4, cns1296));
  return reg_5;
}
var cns1297 = anyStr("Function")
var cns1298 = anyStr("reg")
var cns1299 = anyStr("id")
var cns1300 = anyStr("regs")
var cns1301 = anyStr("table")
var cns1302 = anyStr("insert")
var cns1303 = anyStr("regs")
var cns1304 = anyStr("setmetatable")
var cns1305 = anyStr("__tostring")
var record_43 = function (val) { this.val = val; }
var cns1306 = anyStr("reg_")
var cns1307 = anyStr("id")
function function$29 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = cns1306;
  push(reg_6, concat(reg_7, get(reg_5, cns1307)));
  return reg_6;
}
function Function_reg (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_6, reg_14, reg_15, reg_21, reg_22, reg_23, reg_24, reg_28;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  reg_6 = cns1299;
  set(reg_5, reg_6, length(get(reg_4, cns1300)));
  reg_14 = get(get(reg_1.a, cns1301), cns1302);
  reg_15 = newStack();
  push(reg_15, get(reg_4, cns1303));
  push(reg_15, reg_5);
  reg_18 = call(reg_14, reg_15);
  reg_21 = get(reg_1.a, cns1304);
  reg_22 = newStack();
  push(reg_22, reg_5);
  reg_23 = newTable();
  reg_24 = cns1305;
  set(reg_23, reg_24, anyFn((function (a) { return function$29(a,this) }).bind(reg_3)));
  push(reg_22, reg_23);
  reg_27 = call(reg_21, reg_22);
  reg_28 = newStack();
  push(reg_28, reg_5);
  return reg_28;
}
var cns1308 = anyStr("Function")
var cns1309 = anyStr("inst")
var cns1310 = anyStr("setmetatable")
var cns1311 = anyStr("__tostring")
var cns1312 = anyStr("reg")
var cns1313 = anyStr("reg_")
var cns1314 = anyStr("reg")
var cns1315 = anyStr("tostring")
var cns1316 = anyStr("inst")
function function$30 (reg_0, reg_1) {
  var reg_2, reg_5, reg_9, reg_10, reg_14, reg_17, reg_18, reg_22;
  var goto_16=false;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  goto_16 = !tobool(get(reg_5, cns1312));
  if (!goto_16) {
    reg_9 = newStack();
    reg_10 = cns1313;
    push(reg_9, concat(reg_10, get(reg_5, cns1314)));
    return reg_9;
  }
  if ((goto_16 || false)) {
    goto_16 = false;
    reg_14 = newStack();
    reg_17 = get(reg_2.a, cns1315);
    reg_18 = newStack();
    push(reg_18, get(reg_5, cns1316));
    append(reg_14, call(reg_17, reg_18));
    return reg_14;
  }
  reg_22 = newStack();
  return reg_22;
}
var cns1317 = anyStr("table")
var cns1318 = anyStr("insert")
var cns1319 = anyStr("code")
function Function_inst (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_8, reg_9, reg_10, reg_11, reg_19, reg_20, reg_24;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = get(reg_1.a, cns1310);
  reg_9 = newStack();
  push(reg_9, reg_5);
  reg_10 = newTable();
  reg_11 = cns1311;
  set(reg_10, reg_11, anyFn((function (a) { return function$30(a,this) }).bind(reg_3)));
  push(reg_9, reg_10);
  reg_14 = call(reg_8, reg_9);
  reg_19 = get(get(reg_1.a, cns1317), cns1318);
  reg_20 = newStack();
  push(reg_20, get(reg_4, cns1319));
  push(reg_20, reg_5);
  reg_23 = call(reg_19, reg_20);
  reg_24 = newStack();
  push(reg_24, reg_5);
  return reg_24;
}
var cns1320 = anyStr("Function")
var cns1321 = anyStr("lbl")
var cns1322 = anyStr("label_count")
var cns1323 = parseNum("1")
var cns1324 = anyStr("label_count")
function Function_lbl (reg_0, reg_1) {
  var reg_4, reg_8, reg_10;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_8 = add(get(reg_4, cns1322), cns1323);
  set(reg_4, cns1324, reg_8);
  reg_10 = newStack();
  push(reg_10, reg_8);
  return reg_10;
}
var cns1325 = anyStr("Function")
var cns1326 = anyStr("call")
var cns1327 = anyStr("inst")
var cns1328 = parseNum("1")
function Function_call (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_9;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = get(reg_4, cns1327);
  reg_8 = newStack();
  push(reg_8, reg_4);
  reg_9 = newTable();
  table_append(reg_9, cns1328, copy(reg_0));
  push(reg_8, reg_9);
  append(reg_5, call(reg_7, reg_8));
  return reg_5;
}
var cns1329 = anyStr("Function")
var cns1330 = anyStr("push_scope")
var cns1331 = anyStr("scope")
var cns1332 = anyStr("locals")
var cns1333 = anyStr("labels")
var cns1334 = anyStr("table")
var cns1335 = anyStr("insert")
var cns1336 = anyStr("scopes")
var cns1337 = anyStr("scope")
function Function_push_scope (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_15, reg_16, reg_22;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = cns1331;
  reg_6 = newTable();
  set(reg_6, cns1332, newTable());
  set(reg_6, cns1333, newTable());
  set(reg_4, reg_5, reg_6);
  reg_15 = get(get(reg_1.a, cns1334), cns1335);
  reg_16 = newStack();
  push(reg_16, get(reg_4, cns1336));
  push(reg_16, get(reg_4, cns1337));
  reg_21 = call(reg_15, reg_16);
  reg_22 = newStack();
  return reg_22;
}
var cns1338 = anyStr("Function")
var cns1339 = anyStr("pop_scope")
var cns1340 = anyStr("table")
var cns1341 = anyStr("remove")
var cns1342 = anyStr("scopes")
var cns1343 = anyStr("scope")
var cns1344 = anyStr("scopes")
var cns1345 = anyStr("scopes")
function Function_pop_scope (reg_0, reg_1) {
  var reg_4, reg_9, reg_10, reg_14, reg_16, reg_21;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_9 = get(get(reg_1.a, cns1340), cns1341);
  reg_10 = newStack();
  push(reg_10, get(reg_4, cns1342));
  reg_13 = call(reg_9, reg_10);
  reg_14 = cns1343;
  reg_16 = get(reg_4, cns1344);
  set(reg_4, reg_14, get(reg_16, length(get(reg_4, cns1345))));
  reg_21 = newStack();
  return reg_21;
}
var cns1346 = anyStr("_id")
var cns1347 = anyStr("funcs")
var cns1348 = anyStr("name")
var cns1349 = anyStr("regs")
var cns1350 = anyStr("code")
var cns1351 = anyStr("label_count")
var cns1352 = parseNum("0")
var cns1353 = anyStr("labels")
var cns1354 = anyStr("ins")
var cns1355 = anyStr("outs")
var cns1356 = anyStr("upvals")
var cns1357 = anyStr("scopes")
var cns1358 = anyStr("loops")
var cns1359 = anyStr("setmetatable")
var cns1360 = anyStr("Function")
var cns1361 = anyStr("table")
var cns1362 = anyStr("insert")
var cns1363 = anyStr("funcs")
var cns1364 = anyStr("push_scope")
function function$31 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_32, reg_33, reg_42, reg_43, reg_49, reg_50, reg_52;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  reg_6 = cns1346;
  set(reg_5, reg_6, length(get(reg_1.a, cns1347)));
  set(reg_5, cns1348, reg_4);
  set(reg_5, cns1349, newTable());
  set(reg_5, cns1350, newTable());
  set(reg_5, cns1351, cns1352);
  set(reg_5, cns1353, newTable());
  set(reg_5, cns1354, newTable());
  set(reg_5, cns1355, newTable());
  set(reg_5, cns1356, newTable());
  set(reg_5, cns1357, newTable());
  set(reg_5, cns1358, newTable());
  reg_32 = get(reg_1.a, cns1359);
  reg_33 = newStack();
  push(reg_33, reg_5);
  push(reg_33, get(reg_1.a, cns1360));
  reg_37 = call(reg_32, reg_33);
  reg_42 = get(get(reg_1.a, cns1361), cns1362);
  reg_43 = newStack();
  push(reg_43, get(reg_1.a, cns1363));
  push(reg_43, reg_5);
  reg_47 = call(reg_42, reg_43);
  reg_49 = get(reg_5, cns1364);
  reg_50 = newStack();
  push(reg_50, reg_5);
  reg_51 = call(reg_49, reg_50);
  reg_52 = newStack();
  push(reg_52, reg_5);
  return reg_52;
}
var cns1365 = anyStr("code")
var cns1366 = anyStr("Function")
var cns1367 = anyStr("__tostring")
var cns1368 = anyStr(":lua:")
var cns1369 = anyStr("name")
var cns1370 = anyStr("")
function Function___tostring (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_8;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = cns1368;
  reg_8 = get(reg_4, cns1369);
  if (!tobool(reg_8)) {
    reg_8 = cns1370;
  }
  push(reg_5, concat(reg_6, reg_8));
  return reg_5;
}
var cns1371 = anyStr("_id")
var cns1372 = anyStr("constants")
var cns1373 = anyStr("type")
var cns1374 = anyStr("value")
var cns1375 = anyStr("ins")
var cns1376 = anyStr("outs")
var cns1377 = parseNum("1")
var cns1378 = parseNum("0")
var cns1379 = anyStr("id")
var cns1380 = anyStr("_id")
var cns1381 = anyStr("funcs")
function data_id (reg_0, reg_1) {
  var reg_2, reg_5, reg_6, reg_8;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_8 = get(reg_5, cns1380);
  push(reg_6, add(reg_8, length(get(reg_2.a, cns1381))));
  return reg_6;
}
var cns1382 = anyStr("table")
var cns1383 = anyStr("insert")
var cns1384 = anyStr("constants")
var cns1385 = anyStr("setmetatable")
var cns1386 = anyStr("__tostring")
var cns1387 = anyStr("cns_")
var cns1388 = anyStr("id")
function function$33 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_9, reg_10;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = cns1387;
  reg_9 = get(reg_5, cns1388);
  reg_10 = newStack();
  push(reg_10, reg_5);
  push(reg_6, concat(reg_7, first(call(reg_9, reg_10))));
  return reg_6;
}
function function$32 (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_6, reg_7, reg_16, reg_17, reg_20, reg_27, reg_28, reg_35, reg_36, reg_37, reg_38, reg_42;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = newTable();
  reg_7 = cns1371;
  set(reg_6, reg_7, length(get(reg_1.a, cns1372)));
  set(reg_6, cns1373, reg_4);
  set(reg_6, cns1374, reg_5);
  set(reg_6, cns1375, newTable());
  reg_16 = cns1376;
  reg_17 = newTable();
  set(reg_17, cns1377, cns1378);
  set(reg_6, reg_16, reg_17);
  reg_20 = cns1379;
  set(reg_6, reg_20, anyFn((function (a) { return data_id(a,this) }).bind(reg_3)));
  reg_27 = get(get(reg_1.a, cns1382), cns1383);
  reg_28 = newStack();
  push(reg_28, get(reg_1.a, cns1384));
  push(reg_28, reg_6);
  reg_32 = call(reg_27, reg_28);
  reg_35 = get(reg_1.a, cns1385);
  reg_36 = newStack();
  push(reg_36, reg_6);
  reg_37 = newTable();
  reg_38 = cns1386;
  set(reg_37, reg_38, anyFn((function (a) { return function$33(a,this) }).bind(reg_3)));
  push(reg_36, reg_37);
  reg_41 = call(reg_35, reg_36);
  reg_42 = newStack();
  push(reg_42, reg_6);
  return reg_42;
}
var cns1389 = anyStr("raw_const")
var cns1390 = anyStr("raw_const")
var cns1391 = anyStr("call")
var cns1392 = anyStr("f")
var cns1393 = anyStr("args")
var cns1394 = anyStr("table")
var cns1395 = anyStr("pack")
function function$34 (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_11, reg_13, reg_18, reg_19, reg_22;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns1390);
  reg_8 = newStack();
  push(reg_8, cns1391);
  reg_11 = first(call(reg_7, reg_8));
  set(reg_11, cns1392, reg_4);
  reg_13 = cns1393;
  reg_18 = get(get(reg_1.a, cns1394), cns1395);
  reg_19 = newStack();
  append(reg_19, reg_0);
  set(reg_11, reg_13, first(call(reg_18, reg_19)));
  reg_22 = newStack();
  push(reg_22, reg_11);
  return reg_22;
}
var cns1396 = anyStr("const_call")
var cns1397 = anyStr("str_cache")
var cns1398 = anyStr("anystr_f")
var cns1399 = anyStr("number")
var cns1400 = anyStr("num_cache")
var cns1401 = anyStr("parsenum_f")
var cns1402 = anyStr("raw_const")
var cns1403 = anyStr("bin")
var cns1404 = anyStr("tostring")
var cns1405 = anyStr("const_call")
var cns1406 = anyStr("rawstr_f")
var cns1407 = anyStr("const_call")
function function$35 (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_11, reg_21, reg_23, reg_26, reg_27, reg_31, reg_32, reg_35, reg_38, reg_39, reg_44, reg_45, reg_48, reg_49;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = get(reg_1.a, cns1397);
  reg_11 = get(reg_1.a, cns1398);
  if (tobool(eq(reg_5, cns1399))) {
    reg_8 = get(reg_1.a, cns1400);
    reg_11 = get(reg_1.a, cns1401);
  }
  reg_21 = get(reg_8, reg_4);
  if (tobool(reg_21)) {
    reg_23 = newStack();
    push(reg_23, reg_21);
    return reg_23;
  }
  reg_26 = get(reg_1.a, cns1402);
  reg_27 = newStack();
  push(reg_27, cns1403);
  reg_31 = get(reg_1.a, cns1404);
  reg_32 = newStack();
  push(reg_32, reg_4);
  append(reg_27, call(reg_31, reg_32));
  reg_35 = first(call(reg_26, reg_27));
  reg_38 = get(reg_1.a, cns1405);
  reg_39 = newStack();
  push(reg_39, get(reg_1.a, cns1406));
  push(reg_39, reg_35);
  reg_44 = first(call(reg_38, reg_39));
  reg_45 = newStack();
  reg_48 = get(reg_1.a, cns1407);
  reg_49 = newStack();
  push(reg_49, reg_11);
  push(reg_49, reg_44);
  append(reg_45, call(reg_48, reg_49));
  return reg_45;
}
var cns1408 = anyStr("constant")
var cns1409 = anyStr("module")
var cns1410 = anyStr("auro\x1fint")
var cns1411 = anyStr("int_m")
var cns1412 = anyStr("module")
var cns1413 = anyStr("auro\x1fbool")
var cns1414 = anyStr("bool_m")
var cns1415 = anyStr("module")
var cns1416 = anyStr("auro\x1fstring")
var cns1417 = anyStr("str_m")
var cns1418 = anyStr("module")
var cns1419 = anyStr("lua")
var cns1420 = anyStr("lua_m")
var cns1421 = anyStr("module")
var cns1422 = anyStr("closure")
var cns1423 = anyStr("closure_m")
var cns1424 = anyStr("closure_m")
var cns1425 = anyStr("from")
var cns1426 = anyStr("lua_m")
var cns1427 = anyStr("module")
var cns1428 = anyStr("auro\x1frecord")
var cns1429 = anyStr("record_m")
var cns1430 = anyStr("module")
var cns1431 = anyStr("auro\x1fany")
var cns1432 = anyStr("any_m")
var cns1433 = anyStr("module")
var cns1434 = anyStr("auro\x1fbuffer")
var cns1435 = anyStr("buffer_m")
var cns1436 = anyStr("any_m")
var cns1437 = anyStr("type")
var cns1438 = anyStr("any")
var cns1439 = anyStr("any_t")
var cns1440 = anyStr("bool_m")
var cns1441 = anyStr("type")
var cns1442 = anyStr("bool")
var cns1443 = anyStr("bool_t")
var cns1444 = anyStr("buffer_m")
var cns1445 = anyStr("type")
var cns1446 = anyStr("buffer")
var cns1447 = anyStr("bin_t")
var cns1448 = anyStr("int_m")
var cns1449 = anyStr("type")
var cns1450 = anyStr("int")
var cns1451 = anyStr("int_t")
var cns1452 = anyStr("str_m")
var cns1453 = anyStr("type")
var cns1454 = anyStr("string")
var cns1455 = anyStr("string_t")
var cns1456 = anyStr("lua_m")
var cns1457 = anyStr("type")
var cns1458 = anyStr("Stack")
var cns1459 = anyStr("stack_t")
var cns1460 = anyStr("lua_m")
var cns1461 = anyStr("type")
var cns1462 = anyStr("Function")
var cns1463 = anyStr("func_t")
var cns1464 = anyStr("str_m")
var cns1465 = anyStr("func")
var cns1466 = anyStr("new")
var cns1467 = parseNum("1")
var cns1468 = anyStr("bin_t")
var cns1469 = parseNum("1")
var cns1470 = anyStr("string_t")
var cns1471 = anyStr("rawstr_f")
var cns1472 = anyStr("lua_m")
var cns1473 = anyStr("func")
var cns1474 = anyStr("int")
var cns1475 = parseNum("1")
var cns1476 = anyStr("int_t")
var cns1477 = parseNum("1")
var cns1478 = anyStr("any_t")
var cns1479 = anyStr("anyint_f")
var cns1480 = anyStr("lua_m")
var cns1481 = anyStr("func")
var cns1482 = anyStr("string")
var cns1483 = parseNum("1")
var cns1484 = anyStr("string_t")
var cns1485 = parseNum("1")
var cns1486 = anyStr("any_t")
var cns1487 = anyStr("anystr_f")
var cns1488 = anyStr("lua_m")
var cns1489 = anyStr("func")
var cns1490 = anyStr("nil")
var cns1491 = parseNum("1")
var cns1492 = anyStr("any_t")
var cns1493 = anyStr("nil_f")
var cns1494 = anyStr("lua_m")
var cns1495 = anyStr("func")
var cns1496 = anyStr("true")
var cns1497 = parseNum("1")
var cns1498 = anyStr("any_t")
var cns1499 = anyStr("true_f")
var cns1500 = anyStr("lua_m")
var cns1501 = anyStr("func")
var cns1502 = anyStr("false")
var cns1503 = parseNum("1")
var cns1504 = anyStr("any_t")
var cns1505 = anyStr("false_f")
var cns1506 = anyStr("lua_m")
var cns1507 = anyStr("func")
var cns1508 = anyStr("tobool")
var cns1509 = parseNum("1")
var cns1510 = anyStr("any_t")
var cns1511 = parseNum("1")
var cns1512 = anyStr("bool_t")
var cns1513 = anyStr("bool_f")
var cns1514 = anyStr("lua_m")
var cns1515 = anyStr("func")
var cns1516 = anyStr("function")
var cns1517 = parseNum("1")
var cns1518 = anyStr("func_t")
var cns1519 = parseNum("1")
var cns1520 = anyStr("any_t")
var cns1521 = anyStr("func_f")
var cns1522 = anyStr("lua_m")
var cns1523 = anyStr("func")
var cns1524 = anyStr("call")
var cns1525 = parseNum("1")
var cns1526 = anyStr("any_t")
var cns1527 = parseNum("2")
var cns1528 = anyStr("stack_t")
var cns1529 = parseNum("1")
var cns1530 = anyStr("stack_t")
var cns1531 = anyStr("call_f")
var cns1532 = anyStr("lua_m")
var cns1533 = anyStr("func")
var cns1534 = anyStr("get_global")
var cns1535 = parseNum("1")
var cns1536 = anyStr("any_t")
var cns1537 = anyStr("global_f")
var cns1538 = anyStr("lua_m")
var cns1539 = anyStr("func")
var cns1540 = anyStr("newStack")
var cns1541 = parseNum("1")
var cns1542 = anyStr("stack_t")
var cns1543 = anyStr("stack_f")
var cns1544 = anyStr("lua_m")
var cns1545 = anyStr("func")
var cns1546 = anyStr("push\x1dStack")
var cns1547 = parseNum("1")
var cns1548 = anyStr("stack_t")
var cns1549 = parseNum("2")
var cns1550 = anyStr("any_t")
var cns1551 = anyStr("push_f")
var cns1552 = anyStr("lua_m")
var cns1553 = anyStr("func")
var cns1554 = anyStr("next\x1dStack")
var cns1555 = parseNum("1")
var cns1556 = anyStr("stack_t")
var cns1557 = parseNum("1")
var cns1558 = anyStr("any_t")
var cns1559 = anyStr("next_f")
var cns1560 = anyStr("lua_m")
var cns1561 = anyStr("func")
var cns1562 = anyStr("first\x1dStack")
var cns1563 = parseNum("1")
var cns1564 = anyStr("stack_t")
var cns1565 = parseNum("1")
var cns1566 = anyStr("any_t")
var cns1567 = anyStr("first_f")
var cns1568 = anyStr("lua_m")
var cns1569 = anyStr("func")
var cns1570 = anyStr("append\x1dStack")
var cns1571 = parseNum("1")
var cns1572 = anyStr("stack_t")
var cns1573 = parseNum("2")
var cns1574 = anyStr("stack_t")
var cns1575 = anyStr("append_f")
var cns1576 = anyStr("lua_m")
var cns1577 = anyStr("func")
var cns1578 = anyStr("copy\x1dStack")
var cns1579 = parseNum("1")
var cns1580 = anyStr("stack_t")
var cns1581 = parseNum("1")
var cns1582 = anyStr("stack_t")
var cns1583 = anyStr("copystack_f")
var cns1584 = anyStr("lua_m")
var cns1585 = anyStr("func")
var cns1586 = anyStr("newTable")
var cns1587 = parseNum("1")
var cns1588 = anyStr("any_t")
var cns1589 = anyStr("table_f")
var cns1590 = anyStr("lua_m")
var cns1591 = anyStr("func")
var cns1592 = anyStr("table_append")
var cns1593 = parseNum("1")
var cns1594 = anyStr("any_t")
var cns1595 = parseNum("2")
var cns1596 = anyStr("any_t")
var cns1597 = parseNum("3")
var cns1598 = anyStr("stack_t")
var cns1599 = anyStr("table_append_f")
var cns1600 = anyStr("lua_m")
var cns1601 = anyStr("func")
var cns1602 = anyStr("get")
var cns1603 = parseNum("1")
var cns1604 = anyStr("any_t")
var cns1605 = parseNum("2")
var cns1606 = anyStr("any_t")
var cns1607 = parseNum("1")
var cns1608 = anyStr("any_t")
var cns1609 = anyStr("get_f")
var cns1610 = anyStr("lua_m")
var cns1611 = anyStr("func")
var cns1612 = anyStr("set")
var cns1613 = parseNum("1")
var cns1614 = anyStr("any_t")
var cns1615 = parseNum("2")
var cns1616 = anyStr("any_t")
var cns1617 = parseNum("3")
var cns1618 = anyStr("any_t")
var cns1619 = anyStr("set_f")
var cns1620 = anyStr("lua_m")
var cns1621 = anyStr("func")
var cns1622 = anyStr("parseNum")
var cns1623 = parseNum("1")
var cns1624 = anyStr("string_t")
var cns1625 = parseNum("1")
var cns1626 = anyStr("any_t")
var cns1627 = anyStr("parsenum_f")
var cns1628 = anyStr("+")
var cns1629 = anyStr("lua_m")
var cns1630 = anyStr("func")
var cns1631 = anyStr("add")
var cns1632 = parseNum("1")
var cns1633 = anyStr("any_t")
var cns1634 = parseNum("2")
var cns1635 = anyStr("any_t")
var cns1636 = parseNum("1")
var cns1637 = anyStr("any_t")
var cns1638 = anyStr("-")
var cns1639 = anyStr("lua_m")
var cns1640 = anyStr("func")
var cns1641 = anyStr("sub")
var cns1642 = parseNum("1")
var cns1643 = anyStr("any_t")
var cns1644 = parseNum("2")
var cns1645 = anyStr("any_t")
var cns1646 = parseNum("1")
var cns1647 = anyStr("any_t")
var cns1648 = anyStr("*")
var cns1649 = anyStr("lua_m")
var cns1650 = anyStr("func")
var cns1651 = anyStr("mul")
var cns1652 = parseNum("1")
var cns1653 = anyStr("any_t")
var cns1654 = parseNum("2")
var cns1655 = anyStr("any_t")
var cns1656 = parseNum("1")
var cns1657 = anyStr("any_t")
var cns1658 = anyStr("/")
var cns1659 = anyStr("lua_m")
var cns1660 = anyStr("func")
var cns1661 = anyStr("div")
var cns1662 = parseNum("1")
var cns1663 = anyStr("any_t")
var cns1664 = parseNum("2")
var cns1665 = anyStr("any_t")
var cns1666 = parseNum("1")
var cns1667 = anyStr("any_t")
var cns1668 = anyStr("//")
var cns1669 = anyStr("lua_m")
var cns1670 = anyStr("func")
var cns1671 = anyStr("idiv")
var cns1672 = parseNum("1")
var cns1673 = anyStr("any_t")
var cns1674 = parseNum("2")
var cns1675 = anyStr("any_t")
var cns1676 = parseNum("1")
var cns1677 = anyStr("any_t")
var cns1678 = anyStr("%")
var cns1679 = anyStr("lua_m")
var cns1680 = anyStr("func")
var cns1681 = anyStr("mod")
var cns1682 = parseNum("1")
var cns1683 = anyStr("any_t")
var cns1684 = parseNum("2")
var cns1685 = anyStr("any_t")
var cns1686 = parseNum("1")
var cns1687 = anyStr("any_t")
var cns1688 = anyStr("^")
var cns1689 = anyStr("lua_m")
var cns1690 = anyStr("func")
var cns1691 = anyStr("pow")
var cns1692 = parseNum("1")
var cns1693 = anyStr("any_t")
var cns1694 = parseNum("2")
var cns1695 = anyStr("any_t")
var cns1696 = parseNum("1")
var cns1697 = anyStr("any_t")
var cns1698 = anyStr("..")
var cns1699 = anyStr("lua_m")
var cns1700 = anyStr("func")
var cns1701 = anyStr("concat")
var cns1702 = parseNum("1")
var cns1703 = anyStr("any_t")
var cns1704 = parseNum("2")
var cns1705 = anyStr("any_t")
var cns1706 = parseNum("1")
var cns1707 = anyStr("any_t")
var cns1708 = anyStr("==")
var cns1709 = anyStr("lua_m")
var cns1710 = anyStr("func")
var cns1711 = anyStr("eq")
var cns1712 = parseNum("1")
var cns1713 = anyStr("any_t")
var cns1714 = parseNum("2")
var cns1715 = anyStr("any_t")
var cns1716 = parseNum("1")
var cns1717 = anyStr("any_t")
var cns1718 = anyStr("~=")
var cns1719 = anyStr("lua_m")
var cns1720 = anyStr("func")
var cns1721 = anyStr("ne")
var cns1722 = parseNum("1")
var cns1723 = anyStr("any_t")
var cns1724 = parseNum("2")
var cns1725 = anyStr("any_t")
var cns1726 = parseNum("1")
var cns1727 = anyStr("any_t")
var cns1728 = anyStr("<")
var cns1729 = anyStr("lua_m")
var cns1730 = anyStr("func")
var cns1731 = anyStr("lt")
var cns1732 = parseNum("1")
var cns1733 = anyStr("any_t")
var cns1734 = parseNum("2")
var cns1735 = anyStr("any_t")
var cns1736 = parseNum("1")
var cns1737 = anyStr("any_t")
var cns1738 = anyStr(">")
var cns1739 = anyStr("lua_m")
var cns1740 = anyStr("func")
var cns1741 = anyStr("gt")
var cns1742 = parseNum("1")
var cns1743 = anyStr("any_t")
var cns1744 = parseNum("2")
var cns1745 = anyStr("any_t")
var cns1746 = parseNum("1")
var cns1747 = anyStr("any_t")
var cns1748 = anyStr("<=")
var cns1749 = anyStr("lua_m")
var cns1750 = anyStr("func")
var cns1751 = anyStr("le")
var cns1752 = parseNum("1")
var cns1753 = anyStr("any_t")
var cns1754 = parseNum("2")
var cns1755 = anyStr("any_t")
var cns1756 = parseNum("1")
var cns1757 = anyStr("any_t")
var cns1758 = anyStr(">=")
var cns1759 = anyStr("lua_m")
var cns1760 = anyStr("func")
var cns1761 = anyStr("ge")
var cns1762 = parseNum("1")
var cns1763 = anyStr("any_t")
var cns1764 = parseNum("2")
var cns1765 = anyStr("any_t")
var cns1766 = parseNum("1")
var cns1767 = anyStr("any_t")
var cns1768 = anyStr("binops")
var cns1769 = anyStr("not")
var cns1770 = anyStr("lua_m")
var cns1771 = anyStr("func")
var cns1772 = anyStr("not")
var cns1773 = parseNum("1")
var cns1774 = anyStr("any_t")
var cns1775 = parseNum("1")
var cns1776 = anyStr("any_t")
var cns1777 = anyStr("-")
var cns1778 = anyStr("lua_m")
var cns1779 = anyStr("func")
var cns1780 = anyStr("unm")
var cns1781 = parseNum("1")
var cns1782 = anyStr("any_t")
var cns1783 = parseNum("1")
var cns1784 = anyStr("any_t")
var cns1785 = anyStr("#")
var cns1786 = anyStr("lua_m")
var cns1787 = anyStr("func")
var cns1788 = anyStr("length")
var cns1789 = parseNum("1")
var cns1790 = anyStr("any_t")
var cns1791 = parseNum("1")
var cns1792 = anyStr("any_t")
var cns1793 = anyStr("unops")
function function$36 (reg_0, reg_1) {
  var reg_6, reg_7, reg_10, reg_15, reg_16, reg_19, reg_24, reg_25, reg_28, reg_33, reg_34, reg_37, reg_42, reg_43, reg_46, reg_51, reg_52, reg_58, reg_59, reg_62, reg_67, reg_68, reg_71, reg_76, reg_77, reg_80, reg_85, reg_87, reg_88, reg_91, reg_96, reg_98, reg_99, reg_102, reg_107, reg_109, reg_110, reg_113, reg_118, reg_120, reg_121, reg_124, reg_129, reg_131, reg_132, reg_135, reg_140, reg_142, reg_143, reg_146, reg_151, reg_153, reg_154, reg_157, reg_162, reg_164, reg_165, reg_167, reg_168, reg_172, reg_173, reg_178, reg_183, reg_185, reg_186, reg_188, reg_189, reg_193, reg_194, reg_199, reg_204, reg_206, reg_207, reg_209, reg_210, reg_214, reg_215, reg_220, reg_225, reg_227, reg_228, reg_231, reg_232, reg_237, reg_242, reg_244, reg_245, reg_248, reg_249, reg_254, reg_259, reg_261, reg_262, reg_265, reg_266, reg_271, reg_276, reg_278, reg_279, reg_281, reg_282, reg_286, reg_287, reg_292, reg_297, reg_299, reg_300, reg_302, reg_303, reg_307, reg_308, reg_313, reg_318, reg_320, reg_321, reg_323, reg_324, reg_328, reg_332, reg_333, reg_338, reg_343, reg_345, reg_346, reg_349, reg_350, reg_355, reg_360, reg_362, reg_363, reg_366, reg_367, reg_372, reg_377, reg_379, reg_380, reg_382, reg_383, reg_387, reg_393, reg_398, reg_400, reg_401, reg_403, reg_404, reg_408, reg_409, reg_414, reg_419, reg_421, reg_422, reg_424, reg_425, reg_429, reg_430, reg_435, reg_440, reg_442, reg_443, reg_445, reg_446, reg_450, reg_456, reg_461, reg_463, reg_464, reg_466, reg_467, reg_471, reg_472, reg_477, reg_482, reg_484, reg_485, reg_488, reg_489, reg_494, reg_499, reg_501, reg_502, reg_504, reg_505, reg_509, reg_513, reg_519, reg_524, reg_526, reg_527, reg_529, reg_530, reg_534, reg_538, reg_539, reg_544, reg_549, reg_551, reg_552, reg_554, reg_555, reg_559, reg_563, reg_569, reg_574, reg_576, reg_577, reg_579, reg_580, reg_584, reg_585, reg_590, reg_593, reg_594, reg_597, reg_599, reg_600, reg_602, reg_603, reg_607, reg_611, reg_612, reg_618, reg_621, reg_623, reg_624, reg_626, reg_627, reg_631, reg_635, reg_636, reg_642, reg_645, reg_647, reg_648, reg_650, reg_651, reg_655, reg_659, reg_660, reg_666, reg_669, reg_671, reg_672, reg_674, reg_675, reg_679, reg_683, reg_684, reg_690, reg_693, reg_695, reg_696, reg_698, reg_699, reg_703, reg_707, reg_708, reg_714, reg_717, reg_719, reg_720, reg_722, reg_723, reg_727, reg_731, reg_732, reg_738, reg_741, reg_743, reg_744, reg_746, reg_747, reg_751, reg_755, reg_756, reg_762, reg_765, reg_767, reg_768, reg_770, reg_771, reg_775, reg_779, reg_780, reg_786, reg_789, reg_791, reg_792, reg_794, reg_795, reg_799, reg_803, reg_804, reg_810, reg_813, reg_815, reg_816, reg_818, reg_819, reg_823, reg_827, reg_828, reg_834, reg_837, reg_839, reg_840, reg_842, reg_843, reg_847, reg_851, reg_852, reg_858, reg_861, reg_863, reg_864, reg_866, reg_867, reg_871, reg_875, reg_876, reg_882, reg_885, reg_887, reg_888, reg_890, reg_891, reg_895, reg_899, reg_900, reg_906, reg_909, reg_911, reg_912, reg_914, reg_915, reg_919, reg_923, reg_924, reg_932, reg_933, reg_936, reg_938, reg_939, reg_941, reg_942, reg_946, reg_947, reg_953, reg_956, reg_958, reg_959, reg_961, reg_962, reg_966, reg_967, reg_973, reg_976, reg_978, reg_979, reg_981, reg_982, reg_986, reg_987, reg_995;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = get(reg_1.a, cns1409);
  reg_7 = newStack();
  push(reg_7, cns1410);
  reg_10 = first(call(reg_6, reg_7));
  set(reg_1.a, cns1411, reg_10);
  reg_15 = get(reg_1.a, cns1412);
  reg_16 = newStack();
  push(reg_16, cns1413);
  reg_19 = first(call(reg_15, reg_16));
  set(reg_1.a, cns1414, reg_19);
  reg_24 = get(reg_1.a, cns1415);
  reg_25 = newStack();
  push(reg_25, cns1416);
  reg_28 = first(call(reg_24, reg_25));
  set(reg_1.a, cns1417, reg_28);
  reg_33 = get(reg_1.a, cns1418);
  reg_34 = newStack();
  push(reg_34, cns1419);
  reg_37 = first(call(reg_33, reg_34));
  set(reg_1.a, cns1420, reg_37);
  reg_42 = get(reg_1.a, cns1421);
  reg_43 = newStack();
  push(reg_43, cns1422);
  reg_46 = first(call(reg_42, reg_43));
  set(reg_1.a, cns1423, reg_46);
  reg_51 = get(reg_1.a, cns1424);
  reg_52 = cns1425;
  set(reg_51, reg_52, get(reg_1.a, cns1426));
  reg_58 = get(reg_1.a, cns1427);
  reg_59 = newStack();
  push(reg_59, cns1428);
  reg_62 = first(call(reg_58, reg_59));
  set(reg_1.a, cns1429, reg_62);
  reg_67 = get(reg_1.a, cns1430);
  reg_68 = newStack();
  push(reg_68, cns1431);
  reg_71 = first(call(reg_67, reg_68));
  set(reg_1.a, cns1432, reg_71);
  reg_76 = get(reg_1.a, cns1433);
  reg_77 = newStack();
  push(reg_77, cns1434);
  reg_80 = first(call(reg_76, reg_77));
  set(reg_1.a, cns1435, reg_80);
  reg_85 = get(reg_1.a, cns1436);
  reg_87 = get(reg_85, cns1437);
  reg_88 = newStack();
  push(reg_88, reg_85);
  push(reg_88, cns1438);
  reg_91 = first(call(reg_87, reg_88));
  set(reg_1.a, cns1439, reg_91);
  reg_96 = get(reg_1.a, cns1440);
  reg_98 = get(reg_96, cns1441);
  reg_99 = newStack();
  push(reg_99, reg_96);
  push(reg_99, cns1442);
  reg_102 = first(call(reg_98, reg_99));
  set(reg_1.a, cns1443, reg_102);
  reg_107 = get(reg_1.a, cns1444);
  reg_109 = get(reg_107, cns1445);
  reg_110 = newStack();
  push(reg_110, reg_107);
  push(reg_110, cns1446);
  reg_113 = first(call(reg_109, reg_110));
  set(reg_1.a, cns1447, reg_113);
  reg_118 = get(reg_1.a, cns1448);
  reg_120 = get(reg_118, cns1449);
  reg_121 = newStack();
  push(reg_121, reg_118);
  push(reg_121, cns1450);
  reg_124 = first(call(reg_120, reg_121));
  set(reg_1.a, cns1451, reg_124);
  reg_129 = get(reg_1.a, cns1452);
  reg_131 = get(reg_129, cns1453);
  reg_132 = newStack();
  push(reg_132, reg_129);
  push(reg_132, cns1454);
  reg_135 = first(call(reg_131, reg_132));
  set(reg_1.a, cns1455, reg_135);
  reg_140 = get(reg_1.a, cns1456);
  reg_142 = get(reg_140, cns1457);
  reg_143 = newStack();
  push(reg_143, reg_140);
  push(reg_143, cns1458);
  reg_146 = first(call(reg_142, reg_143));
  set(reg_1.a, cns1459, reg_146);
  reg_151 = get(reg_1.a, cns1460);
  reg_153 = get(reg_151, cns1461);
  reg_154 = newStack();
  push(reg_154, reg_151);
  push(reg_154, cns1462);
  reg_157 = first(call(reg_153, reg_154));
  set(reg_1.a, cns1463, reg_157);
  reg_162 = get(reg_1.a, cns1464);
  reg_164 = get(reg_162, cns1465);
  reg_165 = newStack();
  push(reg_165, reg_162);
  push(reg_165, cns1466);
  reg_167 = newTable();
  reg_168 = cns1467;
  set(reg_167, reg_168, get(reg_1.a, cns1468));
  push(reg_165, reg_167);
  reg_172 = newTable();
  reg_173 = cns1469;
  set(reg_172, reg_173, get(reg_1.a, cns1470));
  push(reg_165, reg_172);
  reg_178 = first(call(reg_164, reg_165));
  set(reg_1.a, cns1471, reg_178);
  reg_183 = get(reg_1.a, cns1472);
  reg_185 = get(reg_183, cns1473);
  reg_186 = newStack();
  push(reg_186, reg_183);
  push(reg_186, cns1474);
  reg_188 = newTable();
  reg_189 = cns1475;
  set(reg_188, reg_189, get(reg_1.a, cns1476));
  push(reg_186, reg_188);
  reg_193 = newTable();
  reg_194 = cns1477;
  set(reg_193, reg_194, get(reg_1.a, cns1478));
  push(reg_186, reg_193);
  reg_199 = first(call(reg_185, reg_186));
  set(reg_1.a, cns1479, reg_199);
  reg_204 = get(reg_1.a, cns1480);
  reg_206 = get(reg_204, cns1481);
  reg_207 = newStack();
  push(reg_207, reg_204);
  push(reg_207, cns1482);
  reg_209 = newTable();
  reg_210 = cns1483;
  set(reg_209, reg_210, get(reg_1.a, cns1484));
  push(reg_207, reg_209);
  reg_214 = newTable();
  reg_215 = cns1485;
  set(reg_214, reg_215, get(reg_1.a, cns1486));
  push(reg_207, reg_214);
  reg_220 = first(call(reg_206, reg_207));
  set(reg_1.a, cns1487, reg_220);
  reg_225 = get(reg_1.a, cns1488);
  reg_227 = get(reg_225, cns1489);
  reg_228 = newStack();
  push(reg_228, reg_225);
  push(reg_228, cns1490);
  push(reg_228, newTable());
  reg_231 = newTable();
  reg_232 = cns1491;
  set(reg_231, reg_232, get(reg_1.a, cns1492));
  push(reg_228, reg_231);
  reg_237 = first(call(reg_227, reg_228));
  set(reg_1.a, cns1493, reg_237);
  reg_242 = get(reg_1.a, cns1494);
  reg_244 = get(reg_242, cns1495);
  reg_245 = newStack();
  push(reg_245, reg_242);
  push(reg_245, cns1496);
  push(reg_245, newTable());
  reg_248 = newTable();
  reg_249 = cns1497;
  set(reg_248, reg_249, get(reg_1.a, cns1498));
  push(reg_245, reg_248);
  reg_254 = first(call(reg_244, reg_245));
  set(reg_1.a, cns1499, reg_254);
  reg_259 = get(reg_1.a, cns1500);
  reg_261 = get(reg_259, cns1501);
  reg_262 = newStack();
  push(reg_262, reg_259);
  push(reg_262, cns1502);
  push(reg_262, newTable());
  reg_265 = newTable();
  reg_266 = cns1503;
  set(reg_265, reg_266, get(reg_1.a, cns1504));
  push(reg_262, reg_265);
  reg_271 = first(call(reg_261, reg_262));
  set(reg_1.a, cns1505, reg_271);
  reg_276 = get(reg_1.a, cns1506);
  reg_278 = get(reg_276, cns1507);
  reg_279 = newStack();
  push(reg_279, reg_276);
  push(reg_279, cns1508);
  reg_281 = newTable();
  reg_282 = cns1509;
  set(reg_281, reg_282, get(reg_1.a, cns1510));
  push(reg_279, reg_281);
  reg_286 = newTable();
  reg_287 = cns1511;
  set(reg_286, reg_287, get(reg_1.a, cns1512));
  push(reg_279, reg_286);
  reg_292 = first(call(reg_278, reg_279));
  set(reg_1.a, cns1513, reg_292);
  reg_297 = get(reg_1.a, cns1514);
  reg_299 = get(reg_297, cns1515);
  reg_300 = newStack();
  push(reg_300, reg_297);
  push(reg_300, cns1516);
  reg_302 = newTable();
  reg_303 = cns1517;
  set(reg_302, reg_303, get(reg_1.a, cns1518));
  push(reg_300, reg_302);
  reg_307 = newTable();
  reg_308 = cns1519;
  set(reg_307, reg_308, get(reg_1.a, cns1520));
  push(reg_300, reg_307);
  reg_313 = first(call(reg_299, reg_300));
  set(reg_1.a, cns1521, reg_313);
  reg_318 = get(reg_1.a, cns1522);
  reg_320 = get(reg_318, cns1523);
  reg_321 = newStack();
  push(reg_321, reg_318);
  push(reg_321, cns1524);
  reg_323 = newTable();
  reg_324 = cns1525;
  set(reg_323, reg_324, get(reg_1.a, cns1526));
  reg_328 = cns1527;
  set(reg_323, reg_328, get(reg_1.a, cns1528));
  push(reg_321, reg_323);
  reg_332 = newTable();
  reg_333 = cns1529;
  set(reg_332, reg_333, get(reg_1.a, cns1530));
  push(reg_321, reg_332);
  reg_338 = first(call(reg_320, reg_321));
  set(reg_1.a, cns1531, reg_338);
  reg_343 = get(reg_1.a, cns1532);
  reg_345 = get(reg_343, cns1533);
  reg_346 = newStack();
  push(reg_346, reg_343);
  push(reg_346, cns1534);
  push(reg_346, newTable());
  reg_349 = newTable();
  reg_350 = cns1535;
  set(reg_349, reg_350, get(reg_1.a, cns1536));
  push(reg_346, reg_349);
  reg_355 = first(call(reg_345, reg_346));
  set(reg_1.a, cns1537, reg_355);
  reg_360 = get(reg_1.a, cns1538);
  reg_362 = get(reg_360, cns1539);
  reg_363 = newStack();
  push(reg_363, reg_360);
  push(reg_363, cns1540);
  push(reg_363, newTable());
  reg_366 = newTable();
  reg_367 = cns1541;
  set(reg_366, reg_367, get(reg_1.a, cns1542));
  push(reg_363, reg_366);
  reg_372 = first(call(reg_362, reg_363));
  set(reg_1.a, cns1543, reg_372);
  reg_377 = get(reg_1.a, cns1544);
  reg_379 = get(reg_377, cns1545);
  reg_380 = newStack();
  push(reg_380, reg_377);
  push(reg_380, cns1546);
  reg_382 = newTable();
  reg_383 = cns1547;
  set(reg_382, reg_383, get(reg_1.a, cns1548));
  reg_387 = cns1549;
  set(reg_382, reg_387, get(reg_1.a, cns1550));
  push(reg_380, reg_382);
  push(reg_380, newTable());
  reg_393 = first(call(reg_379, reg_380));
  set(reg_1.a, cns1551, reg_393);
  reg_398 = get(reg_1.a, cns1552);
  reg_400 = get(reg_398, cns1553);
  reg_401 = newStack();
  push(reg_401, reg_398);
  push(reg_401, cns1554);
  reg_403 = newTable();
  reg_404 = cns1555;
  set(reg_403, reg_404, get(reg_1.a, cns1556));
  push(reg_401, reg_403);
  reg_408 = newTable();
  reg_409 = cns1557;
  set(reg_408, reg_409, get(reg_1.a, cns1558));
  push(reg_401, reg_408);
  reg_414 = first(call(reg_400, reg_401));
  set(reg_1.a, cns1559, reg_414);
  reg_419 = get(reg_1.a, cns1560);
  reg_421 = get(reg_419, cns1561);
  reg_422 = newStack();
  push(reg_422, reg_419);
  push(reg_422, cns1562);
  reg_424 = newTable();
  reg_425 = cns1563;
  set(reg_424, reg_425, get(reg_1.a, cns1564));
  push(reg_422, reg_424);
  reg_429 = newTable();
  reg_430 = cns1565;
  set(reg_429, reg_430, get(reg_1.a, cns1566));
  push(reg_422, reg_429);
  reg_435 = first(call(reg_421, reg_422));
  set(reg_1.a, cns1567, reg_435);
  reg_440 = get(reg_1.a, cns1568);
  reg_442 = get(reg_440, cns1569);
  reg_443 = newStack();
  push(reg_443, reg_440);
  push(reg_443, cns1570);
  reg_445 = newTable();
  reg_446 = cns1571;
  set(reg_445, reg_446, get(reg_1.a, cns1572));
  reg_450 = cns1573;
  set(reg_445, reg_450, get(reg_1.a, cns1574));
  push(reg_443, reg_445);
  push(reg_443, newTable());
  reg_456 = first(call(reg_442, reg_443));
  set(reg_1.a, cns1575, reg_456);
  reg_461 = get(reg_1.a, cns1576);
  reg_463 = get(reg_461, cns1577);
  reg_464 = newStack();
  push(reg_464, reg_461);
  push(reg_464, cns1578);
  reg_466 = newTable();
  reg_467 = cns1579;
  set(reg_466, reg_467, get(reg_1.a, cns1580));
  push(reg_464, reg_466);
  reg_471 = newTable();
  reg_472 = cns1581;
  set(reg_471, reg_472, get(reg_1.a, cns1582));
  push(reg_464, reg_471);
  reg_477 = first(call(reg_463, reg_464));
  set(reg_1.a, cns1583, reg_477);
  reg_482 = get(reg_1.a, cns1584);
  reg_484 = get(reg_482, cns1585);
  reg_485 = newStack();
  push(reg_485, reg_482);
  push(reg_485, cns1586);
  push(reg_485, newTable());
  reg_488 = newTable();
  reg_489 = cns1587;
  set(reg_488, reg_489, get(reg_1.a, cns1588));
  push(reg_485, reg_488);
  reg_494 = first(call(reg_484, reg_485));
  set(reg_1.a, cns1589, reg_494);
  reg_499 = get(reg_1.a, cns1590);
  reg_501 = get(reg_499, cns1591);
  reg_502 = newStack();
  push(reg_502, reg_499);
  push(reg_502, cns1592);
  reg_504 = newTable();
  reg_505 = cns1593;
  set(reg_504, reg_505, get(reg_1.a, cns1594));
  reg_509 = cns1595;
  set(reg_504, reg_509, get(reg_1.a, cns1596));
  reg_513 = cns1597;
  set(reg_504, reg_513, get(reg_1.a, cns1598));
  push(reg_502, reg_504);
  push(reg_502, newTable());
  reg_519 = first(call(reg_501, reg_502));
  set(reg_1.a, cns1599, reg_519);
  reg_524 = get(reg_1.a, cns1600);
  reg_526 = get(reg_524, cns1601);
  reg_527 = newStack();
  push(reg_527, reg_524);
  push(reg_527, cns1602);
  reg_529 = newTable();
  reg_530 = cns1603;
  set(reg_529, reg_530, get(reg_1.a, cns1604));
  reg_534 = cns1605;
  set(reg_529, reg_534, get(reg_1.a, cns1606));
  push(reg_527, reg_529);
  reg_538 = newTable();
  reg_539 = cns1607;
  set(reg_538, reg_539, get(reg_1.a, cns1608));
  push(reg_527, reg_538);
  reg_544 = first(call(reg_526, reg_527));
  set(reg_1.a, cns1609, reg_544);
  reg_549 = get(reg_1.a, cns1610);
  reg_551 = get(reg_549, cns1611);
  reg_552 = newStack();
  push(reg_552, reg_549);
  push(reg_552, cns1612);
  reg_554 = newTable();
  reg_555 = cns1613;
  set(reg_554, reg_555, get(reg_1.a, cns1614));
  reg_559 = cns1615;
  set(reg_554, reg_559, get(reg_1.a, cns1616));
  reg_563 = cns1617;
  set(reg_554, reg_563, get(reg_1.a, cns1618));
  push(reg_552, reg_554);
  push(reg_552, newTable());
  reg_569 = first(call(reg_551, reg_552));
  set(reg_1.a, cns1619, reg_569);
  reg_574 = get(reg_1.a, cns1620);
  reg_576 = get(reg_574, cns1621);
  reg_577 = newStack();
  push(reg_577, reg_574);
  push(reg_577, cns1622);
  reg_579 = newTable();
  reg_580 = cns1623;
  set(reg_579, reg_580, get(reg_1.a, cns1624));
  push(reg_577, reg_579);
  reg_584 = newTable();
  reg_585 = cns1625;
  set(reg_584, reg_585, get(reg_1.a, cns1626));
  push(reg_577, reg_584);
  reg_590 = first(call(reg_576, reg_577));
  set(reg_1.a, cns1627, reg_590);
  reg_593 = newTable();
  reg_594 = cns1628;
  reg_597 = get(reg_1.a, cns1629);
  reg_599 = get(reg_597, cns1630);
  reg_600 = newStack();
  push(reg_600, reg_597);
  push(reg_600, cns1631);
  reg_602 = newTable();
  reg_603 = cns1632;
  set(reg_602, reg_603, get(reg_1.a, cns1633));
  reg_607 = cns1634;
  set(reg_602, reg_607, get(reg_1.a, cns1635));
  push(reg_600, reg_602);
  reg_611 = newTable();
  reg_612 = cns1636;
  set(reg_611, reg_612, get(reg_1.a, cns1637));
  push(reg_600, reg_611);
  set(reg_593, reg_594, first(call(reg_599, reg_600)));
  reg_618 = cns1638;
  reg_621 = get(reg_1.a, cns1639);
  reg_623 = get(reg_621, cns1640);
  reg_624 = newStack();
  push(reg_624, reg_621);
  push(reg_624, cns1641);
  reg_626 = newTable();
  reg_627 = cns1642;
  set(reg_626, reg_627, get(reg_1.a, cns1643));
  reg_631 = cns1644;
  set(reg_626, reg_631, get(reg_1.a, cns1645));
  push(reg_624, reg_626);
  reg_635 = newTable();
  reg_636 = cns1646;
  set(reg_635, reg_636, get(reg_1.a, cns1647));
  push(reg_624, reg_635);
  set(reg_593, reg_618, first(call(reg_623, reg_624)));
  reg_642 = cns1648;
  reg_645 = get(reg_1.a, cns1649);
  reg_647 = get(reg_645, cns1650);
  reg_648 = newStack();
  push(reg_648, reg_645);
  push(reg_648, cns1651);
  reg_650 = newTable();
  reg_651 = cns1652;
  set(reg_650, reg_651, get(reg_1.a, cns1653));
  reg_655 = cns1654;
  set(reg_650, reg_655, get(reg_1.a, cns1655));
  push(reg_648, reg_650);
  reg_659 = newTable();
  reg_660 = cns1656;
  set(reg_659, reg_660, get(reg_1.a, cns1657));
  push(reg_648, reg_659);
  set(reg_593, reg_642, first(call(reg_647, reg_648)));
  reg_666 = cns1658;
  reg_669 = get(reg_1.a, cns1659);
  reg_671 = get(reg_669, cns1660);
  reg_672 = newStack();
  push(reg_672, reg_669);
  push(reg_672, cns1661);
  reg_674 = newTable();
  reg_675 = cns1662;
  set(reg_674, reg_675, get(reg_1.a, cns1663));
  reg_679 = cns1664;
  set(reg_674, reg_679, get(reg_1.a, cns1665));
  push(reg_672, reg_674);
  reg_683 = newTable();
  reg_684 = cns1666;
  set(reg_683, reg_684, get(reg_1.a, cns1667));
  push(reg_672, reg_683);
  set(reg_593, reg_666, first(call(reg_671, reg_672)));
  reg_690 = cns1668;
  reg_693 = get(reg_1.a, cns1669);
  reg_695 = get(reg_693, cns1670);
  reg_696 = newStack();
  push(reg_696, reg_693);
  push(reg_696, cns1671);
  reg_698 = newTable();
  reg_699 = cns1672;
  set(reg_698, reg_699, get(reg_1.a, cns1673));
  reg_703 = cns1674;
  set(reg_698, reg_703, get(reg_1.a, cns1675));
  push(reg_696, reg_698);
  reg_707 = newTable();
  reg_708 = cns1676;
  set(reg_707, reg_708, get(reg_1.a, cns1677));
  push(reg_696, reg_707);
  set(reg_593, reg_690, first(call(reg_695, reg_696)));
  reg_714 = cns1678;
  reg_717 = get(reg_1.a, cns1679);
  reg_719 = get(reg_717, cns1680);
  reg_720 = newStack();
  push(reg_720, reg_717);
  push(reg_720, cns1681);
  reg_722 = newTable();
  reg_723 = cns1682;
  set(reg_722, reg_723, get(reg_1.a, cns1683));
  reg_727 = cns1684;
  set(reg_722, reg_727, get(reg_1.a, cns1685));
  push(reg_720, reg_722);
  reg_731 = newTable();
  reg_732 = cns1686;
  set(reg_731, reg_732, get(reg_1.a, cns1687));
  push(reg_720, reg_731);
  set(reg_593, reg_714, first(call(reg_719, reg_720)));
  reg_738 = cns1688;
  reg_741 = get(reg_1.a, cns1689);
  reg_743 = get(reg_741, cns1690);
  reg_744 = newStack();
  push(reg_744, reg_741);
  push(reg_744, cns1691);
  reg_746 = newTable();
  reg_747 = cns1692;
  set(reg_746, reg_747, get(reg_1.a, cns1693));
  reg_751 = cns1694;
  set(reg_746, reg_751, get(reg_1.a, cns1695));
  push(reg_744, reg_746);
  reg_755 = newTable();
  reg_756 = cns1696;
  set(reg_755, reg_756, get(reg_1.a, cns1697));
  push(reg_744, reg_755);
  set(reg_593, reg_738, first(call(reg_743, reg_744)));
  reg_762 = cns1698;
  reg_765 = get(reg_1.a, cns1699);
  reg_767 = get(reg_765, cns1700);
  reg_768 = newStack();
  push(reg_768, reg_765);
  push(reg_768, cns1701);
  reg_770 = newTable();
  reg_771 = cns1702;
  set(reg_770, reg_771, get(reg_1.a, cns1703));
  reg_775 = cns1704;
  set(reg_770, reg_775, get(reg_1.a, cns1705));
  push(reg_768, reg_770);
  reg_779 = newTable();
  reg_780 = cns1706;
  set(reg_779, reg_780, get(reg_1.a, cns1707));
  push(reg_768, reg_779);
  set(reg_593, reg_762, first(call(reg_767, reg_768)));
  reg_786 = cns1708;
  reg_789 = get(reg_1.a, cns1709);
  reg_791 = get(reg_789, cns1710);
  reg_792 = newStack();
  push(reg_792, reg_789);
  push(reg_792, cns1711);
  reg_794 = newTable();
  reg_795 = cns1712;
  set(reg_794, reg_795, get(reg_1.a, cns1713));
  reg_799 = cns1714;
  set(reg_794, reg_799, get(reg_1.a, cns1715));
  push(reg_792, reg_794);
  reg_803 = newTable();
  reg_804 = cns1716;
  set(reg_803, reg_804, get(reg_1.a, cns1717));
  push(reg_792, reg_803);
  set(reg_593, reg_786, first(call(reg_791, reg_792)));
  reg_810 = cns1718;
  reg_813 = get(reg_1.a, cns1719);
  reg_815 = get(reg_813, cns1720);
  reg_816 = newStack();
  push(reg_816, reg_813);
  push(reg_816, cns1721);
  reg_818 = newTable();
  reg_819 = cns1722;
  set(reg_818, reg_819, get(reg_1.a, cns1723));
  reg_823 = cns1724;
  set(reg_818, reg_823, get(reg_1.a, cns1725));
  push(reg_816, reg_818);
  reg_827 = newTable();
  reg_828 = cns1726;
  set(reg_827, reg_828, get(reg_1.a, cns1727));
  push(reg_816, reg_827);
  set(reg_593, reg_810, first(call(reg_815, reg_816)));
  reg_834 = cns1728;
  reg_837 = get(reg_1.a, cns1729);
  reg_839 = get(reg_837, cns1730);
  reg_840 = newStack();
  push(reg_840, reg_837);
  push(reg_840, cns1731);
  reg_842 = newTable();
  reg_843 = cns1732;
  set(reg_842, reg_843, get(reg_1.a, cns1733));
  reg_847 = cns1734;
  set(reg_842, reg_847, get(reg_1.a, cns1735));
  push(reg_840, reg_842);
  reg_851 = newTable();
  reg_852 = cns1736;
  set(reg_851, reg_852, get(reg_1.a, cns1737));
  push(reg_840, reg_851);
  set(reg_593, reg_834, first(call(reg_839, reg_840)));
  reg_858 = cns1738;
  reg_861 = get(reg_1.a, cns1739);
  reg_863 = get(reg_861, cns1740);
  reg_864 = newStack();
  push(reg_864, reg_861);
  push(reg_864, cns1741);
  reg_866 = newTable();
  reg_867 = cns1742;
  set(reg_866, reg_867, get(reg_1.a, cns1743));
  reg_871 = cns1744;
  set(reg_866, reg_871, get(reg_1.a, cns1745));
  push(reg_864, reg_866);
  reg_875 = newTable();
  reg_876 = cns1746;
  set(reg_875, reg_876, get(reg_1.a, cns1747));
  push(reg_864, reg_875);
  set(reg_593, reg_858, first(call(reg_863, reg_864)));
  reg_882 = cns1748;
  reg_885 = get(reg_1.a, cns1749);
  reg_887 = get(reg_885, cns1750);
  reg_888 = newStack();
  push(reg_888, reg_885);
  push(reg_888, cns1751);
  reg_890 = newTable();
  reg_891 = cns1752;
  set(reg_890, reg_891, get(reg_1.a, cns1753));
  reg_895 = cns1754;
  set(reg_890, reg_895, get(reg_1.a, cns1755));
  push(reg_888, reg_890);
  reg_899 = newTable();
  reg_900 = cns1756;
  set(reg_899, reg_900, get(reg_1.a, cns1757));
  push(reg_888, reg_899);
  set(reg_593, reg_882, first(call(reg_887, reg_888)));
  reg_906 = cns1758;
  reg_909 = get(reg_1.a, cns1759);
  reg_911 = get(reg_909, cns1760);
  reg_912 = newStack();
  push(reg_912, reg_909);
  push(reg_912, cns1761);
  reg_914 = newTable();
  reg_915 = cns1762;
  set(reg_914, reg_915, get(reg_1.a, cns1763));
  reg_919 = cns1764;
  set(reg_914, reg_919, get(reg_1.a, cns1765));
  push(reg_912, reg_914);
  reg_923 = newTable();
  reg_924 = cns1766;
  set(reg_923, reg_924, get(reg_1.a, cns1767));
  push(reg_912, reg_923);
  set(reg_593, reg_906, first(call(reg_911, reg_912)));
  set(reg_1.a, cns1768, reg_593);
  reg_932 = newTable();
  reg_933 = cns1769;
  reg_936 = get(reg_1.a, cns1770);
  reg_938 = get(reg_936, cns1771);
  reg_939 = newStack();
  push(reg_939, reg_936);
  push(reg_939, cns1772);
  reg_941 = newTable();
  reg_942 = cns1773;
  set(reg_941, reg_942, get(reg_1.a, cns1774));
  push(reg_939, reg_941);
  reg_946 = newTable();
  reg_947 = cns1775;
  set(reg_946, reg_947, get(reg_1.a, cns1776));
  push(reg_939, reg_946);
  set(reg_932, reg_933, first(call(reg_938, reg_939)));
  reg_953 = cns1777;
  reg_956 = get(reg_1.a, cns1778);
  reg_958 = get(reg_956, cns1779);
  reg_959 = newStack();
  push(reg_959, reg_956);
  push(reg_959, cns1780);
  reg_961 = newTable();
  reg_962 = cns1781;
  set(reg_961, reg_962, get(reg_1.a, cns1782));
  push(reg_959, reg_961);
  reg_966 = newTable();
  reg_967 = cns1783;
  set(reg_966, reg_967, get(reg_1.a, cns1784));
  push(reg_959, reg_966);
  set(reg_932, reg_953, first(call(reg_958, reg_959)));
  reg_973 = cns1785;
  reg_976 = get(reg_1.a, cns1786);
  reg_978 = get(reg_976, cns1787);
  reg_979 = newStack();
  push(reg_979, reg_976);
  push(reg_979, cns1788);
  reg_981 = newTable();
  reg_982 = cns1789;
  set(reg_981, reg_982, get(reg_1.a, cns1790));
  push(reg_979, reg_981);
  reg_986 = newTable();
  reg_987 = cns1791;
  set(reg_986, reg_987, get(reg_1.a, cns1792));
  push(reg_979, reg_986);
  set(reg_932, reg_973, first(call(reg_978, reg_979)));
  set(reg_1.a, cns1793, reg_932);
  reg_995 = newStack();
  return reg_995;
}
var cns1794 = anyStr("modules")
var cns1795 = anyStr("types")
var cns1796 = anyStr("funcs")
var cns1797 = anyStr("metadata")
var cns1798 = parseNum("1")
var cns1799 = anyStr("source map")
var cns1800 = anyStr("sourcemap")
var cns1801 = anyStr("constants")
var cns1802 = anyStr("num_cache")
var cns1803 = anyStr("str_cache")
function function$37 (reg_0, reg_1) {
  var reg_4, reg_7, reg_10, reg_13, reg_16, reg_21, reg_24, reg_27, reg_33;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newTable();
  set(reg_1.a, cns1794, reg_4);
  reg_7 = newTable();
  set(reg_1.a, cns1795, reg_7);
  reg_10 = newTable();
  set(reg_1.a, cns1796, reg_10);
  reg_13 = newTable();
  set(reg_1.a, cns1797, reg_13);
  reg_16 = newTable();
  set(reg_16, cns1798, cns1799);
  set(reg_1.a, cns1800, reg_16);
  reg_21 = newTable();
  set(reg_1.a, cns1801, reg_21);
  reg_24 = newTable();
  set(reg_1.a, cns1802, reg_24);
  reg_27 = newTable();
  set(reg_1.a, cns1803, reg_27);
  reg_32 = call(reg_1.b, newStack());
  reg_33 = newStack();
  return reg_33;
}
var cns1804 = anyStr("create_compiler_state")
function aulua_basics$lua_main (reg_0) {
  var reg_1, reg_2, reg_3, reg_8, reg_9, reg_15, reg_16, reg_21, reg_22, reg_27, reg_28, reg_32, reg_35, reg_40, reg_41, reg_47, reg_48, reg_53, reg_54, reg_59, reg_60, reg_65, reg_66, reg_71, reg_72, reg_77, reg_78, reg_83, reg_84, reg_88, reg_93, reg_94, reg_98, reg_102, reg_106, reg_112, reg_115;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1};
  reg_2.a = reg_0;
  reg_3 = newTable();
  set(reg_2.a, cns1217, reg_3);
  reg_8 = get(reg_2.a, cns1218);
  reg_9 = cns1219;
  set(reg_8, reg_9, get(reg_2.a, cns1220));
  reg_15 = get(reg_2.a, cns1221);
  reg_16 = cns1222;
  set(reg_15, reg_16, anyFn((function (a) { return Module_type(a,this) }).bind(reg_2)));
  reg_21 = get(reg_2.a, cns1235);
  reg_22 = cns1236;
  set(reg_21, reg_22, anyFn((function (a) { return Module_func(a,this) }).bind(reg_2)));
  reg_27 = get(reg_2.a, cns1259);
  reg_28 = cns1260;
  set(reg_27, reg_28, anyFn((function (a) { return Module_fullname(a,this) }).bind(reg_2)));
  reg_32 = anyFn((function (a) { return function$28(a,this) }).bind(reg_2));
  set(reg_2.a, cns1289, reg_32);
  reg_35 = newTable();
  set(reg_2.a, cns1290, reg_35);
  reg_40 = get(reg_2.a, cns1291);
  reg_41 = cns1292;
  set(reg_40, reg_41, get(reg_2.a, cns1293));
  reg_47 = get(reg_2.a, cns1294);
  reg_48 = cns1295;
  set(reg_47, reg_48, anyFn((function (a) { return Function_id(a,this) }).bind(reg_2)));
  reg_53 = get(reg_2.a, cns1297);
  reg_54 = cns1298;
  set(reg_53, reg_54, anyFn((function (a) { return Function_reg(a,this) }).bind(reg_2)));
  reg_59 = get(reg_2.a, cns1308);
  reg_60 = cns1309;
  set(reg_59, reg_60, anyFn((function (a) { return Function_inst(a,this) }).bind(reg_2)));
  reg_65 = get(reg_2.a, cns1320);
  reg_66 = cns1321;
  set(reg_65, reg_66, anyFn((function (a) { return Function_lbl(a,this) }).bind(reg_2)));
  reg_71 = get(reg_2.a, cns1325);
  reg_72 = cns1326;
  set(reg_71, reg_72, anyFn((function (a) { return Function_call(a,this) }).bind(reg_2)));
  reg_77 = get(reg_2.a, cns1329);
  reg_78 = cns1330;
  set(reg_77, reg_78, anyFn((function (a) { return Function_push_scope(a,this) }).bind(reg_2)));
  reg_83 = get(reg_2.a, cns1338);
  reg_84 = cns1339;
  set(reg_83, reg_84, anyFn((function (a) { return Function_pop_scope(a,this) }).bind(reg_2)));
  reg_88 = anyFn((function (a) { return function$31(a,this) }).bind(reg_2));
  set(reg_2.a, cns1365, reg_88);
  reg_93 = get(reg_2.a, cns1366);
  reg_94 = cns1367;
  set(reg_93, reg_94, anyFn((function (a) { return Function___tostring(a,this) }).bind(reg_2)));
  reg_98 = anyFn((function (a) { return function$32(a,this) }).bind(reg_2));
  set(reg_2.a, cns1389, reg_98);
  reg_102 = anyFn((function (a) { return function$34(a,this) }).bind(reg_2));
  set(reg_2.a, cns1396, reg_102);
  reg_106 = anyFn((function (a) { return function$35(a,this) }).bind(reg_2));
  set(reg_2.a, cns1408, reg_106);
  reg_2.b = anyFn((function (a) { return function$36(a,this) }).bind(reg_2));
  reg_112 = anyFn((function (a) { return function$37(a,this) }).bind(reg_2));
  set(reg_2.a, cns1804, reg_112);
  reg_115 = newStack();
  return reg_115;
}
var cns1805 = anyStr("Function")
var cns1806 = anyStr("compile_au_call")
var cns1807 = anyStr("au_type")
var cns1808 = anyStr("any_module")
var cns1809 = anyStr("module")
var cns1810 = anyStr("items")
var cns1811 = parseNum("1")
var cns1812 = anyStr("name")
var cns1813 = anyStr("0")
var cns1814 = anyStr("tp")
var cns1815 = anyStr("any_module")
var cns1816 = anyStr("module")
var cns1817 = anyStr("any_module")
var cns1818 = anyStr("base")
var cns1819 = anyStr("any_m")
var cns1820 = anyStr("any_module")
var cns1821 = anyStr("argument")
var cns1822 = anyStr("any_module")
function aulua_auro_syntax$function (reg_0, reg_1) {
  var reg_2, reg_5, reg_15, reg_16, reg_17, reg_18, reg_19, reg_23, reg_31, reg_32, reg_39;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  if (tobool(not(get(reg_5, cns1808)))) {
    reg_15 = first(call(get(reg_2.a, cns1809), newStack()));
    reg_16 = cns1810;
    reg_17 = newTable();
    reg_18 = cns1811;
    reg_19 = newTable();
    set(reg_19, cns1812, cns1813);
    set(reg_19, cns1814, reg_5);
    set(reg_17, reg_18, reg_19);
    set(reg_15, reg_16, reg_17);
    reg_23 = cns1815;
    set(reg_5, reg_23, first(call(get(reg_2.a, cns1816), newStack())));
    reg_31 = get(reg_5, cns1817);
    reg_32 = cns1818;
    set(reg_31, reg_32, get(reg_2.a, cns1819));
    set(get(reg_5, cns1820), cns1821, reg_15);
  }
  reg_39 = newStack();
  push(reg_39, get(reg_5, cns1822));
  return reg_39;
}
var cns1823 = anyStr("get_any_module_of")
var cns1824 = anyStr("macro")
var cns1825 = anyStr("key")
var cns1826 = anyStr("err")
var cns1827 = anyStr("attempt to index '")
var cns1828 = anyStr("key")
var cns1829 = anyStr("' on an auro macro")
var cns1830 = anyStr("macro")
var cns1831 = anyStr("import")
var cns1832 = anyStr("values")
var cns1833 = parseNum("1")
var cns1834 = anyStr("err")
var cns1835 = anyStr("bad argument #1 for _AU_IMPORT (string literal expected)")
var cns1836 = anyStr("ipairs")
var cns1837 = anyStr("values")
var cns1838 = anyStr("type")
var cns1839 = anyStr("str")
var cns1840 = anyStr("err")
var cns1841 = anyStr("bad argument #")
var cns1842 = anyStr(" for _AU_IMPORT (string literal expected)")
var cns1843 = anyStr("table")
var cns1844 = anyStr("insert")
var cns1845 = anyStr("value")
var cns1846 = anyStr("module")
var cns1847 = anyStr("table")
var cns1848 = anyStr("concat")
var cns1849 = anyStr("\x1f")
var cns1850 = anyStr("au_type")
var cns1851 = anyStr("module")
var cns1852 = anyStr("module_id")
var cns1853 = anyStr("id")
var cns1854 = anyStr("macro")
var cns1855 = anyStr("function")
var cns1856 = anyStr("err")
var cns1857 = anyStr("auro function definitions not yet supported")
var cns1858 = anyStr("module")
var cns1859 = anyStr("modules")
var cns1860 = anyStr("module_id")
var cns1861 = parseNum("1")
var cns1862 = anyStr("key")
var cns1863 = anyStr("err")
var cns1864 = anyStr("attempt to call a auro module")
var cns1865 = anyStr("key")
var cns1866 = anyStr("get_type")
var cns1867 = anyStr("values")
var cns1868 = parseNum("1")
var cns1869 = anyStr("values")
var cns1870 = parseNum("1")
var cns1871 = anyStr("type")
var cns1872 = anyStr("str")
var cns1873 = anyStr("err")
var cns1874 = anyStr("bad arguments for get_type (string literal expected)")
var cns1875 = anyStr("type")
var cns1876 = anyStr("values")
var cns1877 = parseNum("1")
var cns1878 = anyStr("value")
var cns1879 = anyStr("au_type")
var cns1880 = anyStr("type")
var cns1881 = anyStr("type_id")
var cns1882 = anyStr("id")
var cns1883 = anyStr("key")
var cns1884 = anyStr("get_function")
var cns1885 = anyStr("values")
var cns1886 = anyStr("type")
var cns1887 = anyStr("constructor")
var cns1888 = anyStr("err")
var cns1889 = anyStr("bad argument #")
var cns1890 = anyStr(" for get_function (table constructor expected)")
var cns1891 = anyStr("ipairs")
var cns1892 = anyStr("items")
var cns1893 = anyStr("type")
var cns1894 = anyStr("item")
var cns1895 = anyStr("err")
var cns1896 = anyStr("bad argument #")
var cns1897 = anyStr(" for get_function (field keys are not allowed)")
var cns1898 = anyStr("compileExpr")
var cns1899 = anyStr("value")
var cns1900 = anyStr("au_type")
var cns1901 = anyStr("type")
var cns1902 = anyStr("err")
var cns1903 = anyStr("bad argument #")
var cns1904 = anyStr(" for get_function (auro type expected at field #")
var cns1905 = anyStr(")")
var cns1906 = anyStr("table")
var cns1907 = anyStr("insert")
var cns1908 = anyStr("types")
var cns1909 = anyStr("type_id")
var cns1910 = parseNum("1")
function function$38 (reg_0, reg_1) {
  var reg_2, reg_5, reg_9, reg_17, reg_18, reg_19, reg_25, reg_28, reg_29, reg_32, reg_33, reg_34, reg_35, reg_36, reg_37, reg_38, reg_39, reg_50, reg_51, reg_52, reg_58, reg_60, reg_61, reg_66, reg_74, reg_75, reg_76, reg_77, reg_89, reg_90, reg_93, reg_100;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_9 = get(get(reg_1.b, cns1885), reg_5);
  if (tobool(ne(get(reg_9, cns1886), cns1887))) {
    reg_17 = get(reg_2.a, cns1888);
    reg_18 = newStack();
    reg_19 = cns1889;
    push(reg_18, concat(reg_19, concat(reg_5, cns1890)));
    push(reg_18, reg_1.b);
    reg_24 = call(reg_17, reg_18);
  }
  reg_25 = newTable();
  reg_28 = get(reg_2.a, cns1891);
  reg_29 = newStack();
  push(reg_29, get(reg_9, cns1892));
  reg_32 = call(reg_28, reg_29);
  reg_33 = next(reg_32);
  reg_34 = next(reg_32);
  reg_35 = next(reg_32);
  loop_1: while (true) {
    reg_36 = newStack();
    push(reg_36, reg_34);
    push(reg_36, reg_35);
    reg_37 = call(reg_33, reg_36);
    reg_38 = next(reg_37);
    reg_35 = reg_38;
    reg_39 = next(reg_37);
    if (tobool(eq(reg_35, nil()))) break loop_1;
    if (tobool(ne(get(reg_39, cns1893), cns1894))) {
      reg_50 = get(reg_2.a, cns1895);
      reg_51 = newStack();
      reg_52 = cns1896;
      push(reg_51, concat(reg_52, concat(reg_5, cns1897)));
      push(reg_51, reg_1.b);
      reg_57 = call(reg_50, reg_51);
    }
    reg_58 = reg_1.c;
    reg_60 = get(reg_58, cns1898);
    reg_61 = newStack();
    push(reg_61, reg_58);
    push(reg_61, get(reg_39, cns1899));
    push(reg_61, lua$true());
    reg_66 = first(call(reg_60, reg_61));
    if (tobool(ne(get(reg_66, cns1900), cns1901))) {
      reg_74 = get(reg_2.a, cns1902);
      reg_75 = newStack();
      reg_76 = cns1903;
      reg_77 = cns1904;
      push(reg_75, concat(reg_76, concat(reg_5, concat(reg_77, concat(reg_38, cns1905)))));
      push(reg_75, reg_1.b);
      reg_84 = call(reg_74, reg_75);
    }
    reg_89 = get(get(reg_2.a, cns1906), cns1907);
    reg_90 = newStack();
    push(reg_90, reg_25);
    reg_93 = get(reg_2.a, cns1908);
    push(reg_90, get(reg_93, add(get(reg_66, cns1909), cns1910)));
    reg_99 = call(reg_89, reg_90);
  }
  reg_100 = newStack();
  push(reg_100, reg_25);
  return reg_100;
}
var cns1911 = anyStr("create_type_list")
var cns1912 = anyStr("values")
var cns1913 = parseNum("3")
var cns1914 = anyStr("err")
var cns1915 = anyStr("bad arguments for get_function (3 arguments expected)")
var cns1916 = anyStr("values")
var cns1917 = parseNum("1")
var cns1918 = anyStr("type")
var cns1919 = anyStr("str")
var cns1920 = anyStr("err")
var cns1921 = anyStr("bad argument #1 for get_function (string literal expected)")
var cns1922 = anyStr("values")
var cns1923 = parseNum("1")
var cns1924 = anyStr("value")
var cns1925 = anyStr("create_type_list")
var cns1926 = parseNum("2")
var cns1927 = anyStr("create_type_list")
var cns1928 = parseNum("3")
var cns1929 = anyStr("func")
var cns1930 = anyStr("au_type")
var cns1931 = anyStr("function")
var cns1932 = anyStr("function_id")
var cns1933 = anyStr("id")
var cns1934 = anyStr("err")
var cns1935 = anyStr("attempt to index '")
var cns1936 = anyStr("key")
var cns1937 = anyStr("' on a auro module")
var cns1938 = anyStr("type")
var cns1939 = anyStr("types")
var cns1940 = anyStr("type_id")
var cns1941 = parseNum("1")
var cns1942 = anyStr("key")
var cns1943 = anyStr("values")
var cns1944 = parseNum("1")
var cns1945 = anyStr("err")
var cns1946 = anyStr("bad arguments for auro type (one argument expected)")
var cns1947 = anyStr("from_any")
var cns1948 = anyStr("get_any_module_of")
var cns1949 = anyStr("from_any")
var cns1950 = anyStr("func")
var cns1951 = anyStr("get")
var cns1952 = parseNum("1")
var cns1953 = anyStr("any_t")
var cns1954 = parseNum("1")
var cns1955 = anyStr("compileExpr")
var cns1956 = anyStr("values")
var cns1957 = parseNum("1")
var cns1958 = anyStr("inst")
var cns1959 = parseNum("1")
var cns1960 = anyStr("from_any")
var cns1961 = parseNum("2")
var cns1962 = anyStr("au_type")
var cns1963 = anyStr("value")
var cns1964 = anyStr("type_id")
var cns1965 = anyStr("id")
var cns1966 = anyStr("key")
var cns1967 = anyStr("test")
var cns1968 = anyStr("test_any")
var cns1969 = anyStr("get_any_module_of")
var cns1970 = anyStr("test_any")
var cns1971 = anyStr("func")
var cns1972 = anyStr("test")
var cns1973 = parseNum("1")
var cns1974 = anyStr("any_t")
var cns1975 = parseNum("1")
var cns1976 = anyStr("bool_t")
var cns1977 = anyStr("compileExpr")
var cns1978 = anyStr("values")
var cns1979 = parseNum("1")
var cns1980 = anyStr("inst")
var cns1981 = parseNum("1")
var cns1982 = anyStr("test_any")
var cns1983 = parseNum("2")
var cns1984 = anyStr("au_type")
var cns1985 = anyStr("value")
var cns1986 = anyStr("type_id")
var cns1987 = anyStr("bool_t")
var cns1988 = anyStr("id")
var cns1989 = anyStr("err")
var cns1990 = anyStr("attempt to index '")
var cns1991 = anyStr("key")
var cns1992 = anyStr("' on a auro type")
var cns1993 = anyStr("value")
var cns1994 = anyStr("types")
var cns1995 = anyStr("type_id")
var cns1996 = parseNum("1")
var cns1997 = anyStr("key")
var cns1998 = anyStr("err")
var cns1999 = anyStr("attempt to call a auro value")
var cns2000 = anyStr("key")
var cns2001 = anyStr("to_lua_value")
var cns2002 = anyStr("values")
var cns2003 = parseNum("0")
var cns2004 = anyStr("err")
var cns2005 = anyStr("bad arguments for to_lua_value (no arguments expected)")
var cns2006 = anyStr("to_any")
var cns2007 = anyStr("get_any_module_of")
var cns2008 = anyStr("to_any")
var cns2009 = anyStr("func")
var cns2010 = anyStr("new")
var cns2011 = parseNum("1")
var cns2012 = parseNum("1")
var cns2013 = anyStr("any_t")
var cns2014 = anyStr("inst")
var cns2015 = parseNum("1")
var cns2016 = anyStr("to_any")
var cns2017 = parseNum("2")
var cns2018 = anyStr("err")
var cns2019 = anyStr("attempt to index '")
var cns2020 = anyStr("key")
var cns2021 = anyStr("' on a auro value")
var cns2022 = anyStr("function")
var cns2023 = anyStr("key")
var cns2024 = anyStr("err")
var cns2025 = anyStr("attempt to index '")
var cns2026 = anyStr("key")
var cns2027 = anyStr("' on a auro function")
var cns2028 = anyStr("funcs")
var cns2029 = anyStr("function_id")
var cns2030 = parseNum("1")
var cns2031 = anyStr("values")
var cns2032 = anyStr("ins")
var cns2033 = anyStr("err")
var cns2034 = anyStr("bad arguments for auro type (")
var cns2035 = anyStr("ins")
var cns2036 = anyStr(" arguments expected)")
var cns2037 = parseNum("1")
var cns2038 = anyStr("au_type")
var cns2039 = anyStr("result")
var cns2040 = anyStr("regs")
var cns2041 = anyStr("ipairs")
var cns2042 = anyStr("values")
var cns2043 = anyStr("compileExpr")
var cns2044 = anyStr("au_type")
var cns2045 = anyStr("lua value")
var cns2046 = anyStr("au_type")
var cns2047 = anyStr("value")
var cns2048 = anyStr("au_type")
var cns2049 = anyStr("type_id")
var cns2050 = anyStr("ins")
var cns2051 = anyStr("types")
var cns2052 = anyStr("type_id")
var cns2053 = parseNum("1")
var cns2054 = anyStr("name")
var cns2055 = anyStr("types")
var cns2056 = anyStr("ins")
var cns2057 = parseNum("1")
var cns2058 = anyStr("name")
var cns2059 = anyStr("err")
var cns2060 = anyStr("bad argument #")
var cns2061 = anyStr(" for ")
var cns2062 = anyStr("name")
var cns2063 = anyStr(" (")
var cns2064 = anyStr(" expected but got ")
var cns2065 = anyStr(")")
var cns2066 = anyStr("table")
var cns2067 = anyStr("insert")
var cns2068 = anyStr("ipairs")
var cns2069 = anyStr("outs")
var cns2070 = anyStr("regs")
var cns2071 = anyStr("au_type")
var cns2072 = anyStr("value")
var cns2073 = anyStr("type_id")
var cns2074 = anyStr("inst")
var cns2075 = anyStr("err")
var cns2076 = anyStr("unknown au_type ")
function Function_compile_au_call (reg_0, reg_1) {
  var reg_2, reg_3, reg_6, reg_8, reg_10, reg_21, reg_22, reg_23, reg_37, reg_47, reg_48, reg_54, reg_55, reg_59, reg_60, reg_61, reg_62, reg_63, reg_64, reg_65, reg_66, reg_77, reg_78, reg_79, reg_89, reg_90, reg_96, reg_97, reg_102, reg_103, reg_107, reg_108, reg_109, reg_112, reg_122, reg_123, reg_131, reg_136, reg_144, reg_145, reg_160, reg_174, reg_175, reg_180, reg_181, reg_190, reg_191, reg_192, reg_195, reg_205, reg_217, reg_218, reg_234, reg_235, reg_245, reg_248, reg_249, reg_252, reg_255, reg_256, reg_259, reg_261, reg_262, reg_264, reg_265, reg_266, reg_269, reg_271, reg_272, reg_277, reg_278, reg_279, reg_293, reg_298, reg_313, reg_314, reg_324, reg_325, reg_327, reg_328, reg_330, reg_331, reg_333, reg_334, reg_338, reg_342, reg_344, reg_345, reg_352, reg_353, reg_355, reg_356, reg_357, reg_358, reg_363, reg_366, reg_369, reg_382, reg_383, reg_385, reg_386, reg_388, reg_389, reg_391, reg_392, reg_396, reg_397, reg_403, reg_405, reg_406, reg_413, reg_414, reg_415, reg_417, reg_418, reg_419, reg_420, reg_426, reg_435, reg_436, reg_437, reg_451, reg_456, reg_464, reg_465, reg_484, reg_485, reg_495, reg_496, reg_498, reg_499, reg_501, reg_502, reg_504, reg_506, reg_507, reg_513, reg_514, reg_516, reg_517, reg_518, reg_519, reg_526, reg_527, reg_528, reg_546, reg_547, reg_548, reg_559, reg_564, reg_568, reg_576, reg_577, reg_578, reg_587, reg_595, reg_596, reg_600, reg_601, reg_602, reg_603, reg_604, reg_605, reg_606, reg_607, reg_611, reg_613, reg_614, reg_617, reg_618, reg_632, reg_640, reg_651, reg_659, reg_662, reg_663, reg_664, reg_665, reg_667, reg_668, reg_669, reg_685, reg_686, reg_690, reg_691, reg_694, reg_695, reg_696, reg_697, reg_698, reg_699, reg_700, reg_701, reg_706, reg_707, reg_711, reg_712, reg_714, reg_715, reg_719, reg_720, reg_725;
  var goto_146=false, goto_161=false, goto_189=false, goto_252=false, goto_351=false, goto_367=false, goto_476=false, goto_561=false, goto_577=false, goto_605=false, goto_682=false, goto_698=false, goto_808=false, goto_818=false;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.c = next(reg_0);
  reg_3.b = next(reg_0);
  reg_6 = next(reg_0);
  reg_8 = get(reg_6, cns1807);
  reg_10 = anyFn((function (a) { return aulua_auro_syntax$function(a,this) }).bind(reg_3));
  set(reg_1.a, cns1823, reg_10);
  goto_161 = !tobool(eq(reg_8, cns1824));
  if (!goto_161) {
    if (tobool(get(reg_6, cns1825))) {
      reg_21 = get(reg_1.a, cns1826);
      reg_22 = newStack();
      reg_23 = cns1827;
      push(reg_22, concat(reg_23, concat(get(reg_3.b, cns1828), cns1829)));
      push(reg_22, reg_3.b);
      reg_31 = call(reg_21, reg_22);
    }
    goto_146 = !tobool(eq(get(reg_6, cns1830), cns1831));
    if (!goto_146) {
      reg_37 = newTable();
      if (tobool(lt(length(get(reg_3.b, cns1832)), cns1833))) {
        reg_47 = get(reg_1.a, cns1834);
        reg_48 = newStack();
        push(reg_48, cns1835);
        push(reg_48, reg_3.b);
        reg_51 = call(reg_47, reg_48);
      }
      reg_54 = get(reg_1.a, cns1836);
      reg_55 = newStack();
      push(reg_55, get(reg_3.b, cns1837));
      reg_59 = call(reg_54, reg_55);
      reg_60 = next(reg_59);
      reg_61 = next(reg_59);
      reg_62 = next(reg_59);
      loop_3: while (true) {
        reg_63 = newStack();
        push(reg_63, reg_61);
        push(reg_63, reg_62);
        reg_64 = call(reg_60, reg_63);
        reg_65 = next(reg_64);
        reg_62 = reg_65;
        reg_66 = next(reg_64);
        if (tobool(eq(reg_62, nil()))) break loop_3;
        if (tobool(ne(get(reg_66, cns1838), cns1839))) {
          reg_77 = get(reg_1.a, cns1840);
          reg_78 = newStack();
          reg_79 = cns1841;
          push(reg_78, concat(reg_79, concat(reg_65, cns1842)));
          push(reg_78, reg_3.b);
          reg_84 = call(reg_77, reg_78);
        }
        reg_89 = get(get(reg_1.a, cns1843), cns1844);
        reg_90 = newStack();
        push(reg_90, reg_37);
        push(reg_90, get(reg_66, cns1845));
        reg_93 = call(reg_89, reg_90);
      }
      reg_96 = get(reg_1.a, cns1846);
      reg_97 = newStack();
      reg_102 = get(get(reg_1.a, cns1847), cns1848);
      reg_103 = newStack();
      push(reg_103, reg_37);
      push(reg_103, cns1849);
      append(reg_97, call(reg_102, reg_103));
      reg_107 = first(call(reg_96, reg_97));
      reg_108 = newStack();
      reg_109 = newTable();
      set(reg_109, cns1850, cns1851);
      reg_112 = cns1852;
      set(reg_109, reg_112, get(reg_107, cns1853));
      push(reg_108, reg_109);
      return reg_108;
    }
    if ((goto_146 || false)) {
      goto_146 = false;
      if (tobool(eq(get(reg_6, cns1854), cns1855))) {
        reg_122 = get(reg_1.a, cns1856);
        reg_123 = newStack();
        push(reg_123, cns1857);
        reg_125 = call(reg_122, reg_123);
      }
    }
  }
  if ((goto_161 || false)) {
    goto_161 = false;
    goto_367 = !tobool(eq(reg_8, cns1858));
    if (!goto_367) {
      reg_131 = get(reg_1.a, cns1859);
      reg_136 = get(reg_131, sub(get(reg_6, cns1860), cns1861));
      goto_189 = !tobool(not(get(reg_3.b, cns1862)));
      if (!goto_189) {
        reg_144 = get(reg_1.a, cns1863);
        reg_145 = newStack();
        push(reg_145, cns1864);
        push(reg_145, reg_3.b);
        reg_148 = call(reg_144, reg_145);
      }
      if ((goto_189 || false)) {
        goto_189 = false;
        goto_252 = !tobool(eq(get(reg_3.b, cns1865), cns1866));
        if (!goto_252) {
          reg_160 = ne(length(get(reg_3.b, cns1867)), cns1868);
          if (!tobool(reg_160)) {
            reg_160 = ne(get(get(get(reg_3.b, cns1869), cns1870), cns1871), cns1872);
          }
          if (tobool(reg_160)) {
            reg_174 = get(reg_1.a, cns1873);
            reg_175 = newStack();
            push(reg_175, cns1874);
            push(reg_175, reg_3.b);
            reg_178 = call(reg_174, reg_175);
          }
          reg_180 = get(reg_136, cns1875);
          reg_181 = newStack();
          push(reg_181, reg_136);
          push(reg_181, get(get(get(reg_3.b, cns1876), cns1877), cns1878));
          reg_190 = first(call(reg_180, reg_181));
          reg_191 = newStack();
          reg_192 = newTable();
          set(reg_192, cns1879, cns1880);
          reg_195 = cns1881;
          set(reg_192, reg_195, get(reg_190, cns1882));
          push(reg_191, reg_192);
          return reg_191;
        }
        if ((goto_252 || false)) {
          goto_252 = false;
          goto_351 = !tobool(eq(get(reg_3.b, cns1883), cns1884));
          if (!goto_351) {
            reg_205 = anyFn((function (a) { return function$38(a,this) }).bind(reg_3));
            set(reg_1.a, cns1911, reg_205);
            if (tobool(ne(length(get(reg_3.b, cns1912)), cns1913))) {
              reg_217 = get(reg_1.a, cns1914);
              reg_218 = newStack();
              push(reg_218, cns1915);
              push(reg_218, reg_3.b);
              reg_221 = call(reg_217, reg_218);
            }
            if (tobool(ne(get(get(get(reg_3.b, cns1916), cns1917), cns1918), cns1919))) {
              reg_234 = get(reg_1.a, cns1920);
              reg_235 = newStack();
              push(reg_235, cns1921);
              push(reg_235, reg_3.b);
              reg_238 = call(reg_234, reg_235);
            }
            reg_245 = get(get(get(reg_3.b, cns1922), cns1923), cns1924);
            reg_248 = get(reg_1.a, cns1925);
            reg_249 = newStack();
            push(reg_249, cns1926);
            reg_252 = first(call(reg_248, reg_249));
            reg_255 = get(reg_1.a, cns1927);
            reg_256 = newStack();
            push(reg_256, cns1928);
            reg_259 = first(call(reg_255, reg_256));
            reg_261 = get(reg_136, cns1929);
            reg_262 = newStack();
            push(reg_262, reg_136);
            push(reg_262, reg_245);
            push(reg_262, reg_252);
            push(reg_262, reg_259);
            reg_264 = first(call(reg_261, reg_262));
            reg_265 = newStack();
            reg_266 = newTable();
            set(reg_266, cns1930, cns1931);
            reg_269 = cns1932;
            reg_271 = get(reg_264, cns1933);
            reg_272 = newStack();
            push(reg_272, reg_264);
            set(reg_266, reg_269, first(call(reg_271, reg_272)));
            push(reg_265, reg_266);
            return reg_265;
          }
          if ((goto_351 || false)) {
            goto_351 = false;
            reg_277 = get(reg_1.a, cns1934);
            reg_278 = newStack();
            reg_279 = cns1935;
            push(reg_278, concat(reg_279, concat(get(reg_3.b, cns1936), cns1937)));
            push(reg_278, reg_3.b);
            reg_287 = call(reg_277, reg_278);
          }
        }
      }
    }
    if ((goto_367 || false)) {
      goto_367 = false;
      goto_577 = !tobool(eq(reg_8, cns1938));
      if (!goto_577) {
        reg_293 = get(reg_1.a, cns1939);
        reg_298 = get(reg_293, add(get(reg_6, cns1940), cns1941));
        goto_476 = !tobool(not(get(reg_3.b, cns1942)));
        if (!goto_476) {
          if (tobool(ne(length(get(reg_3.b, cns1943)), cns1944))) {
            reg_313 = get(reg_1.a, cns1945);
            reg_314 = newStack();
            push(reg_314, cns1946);
            push(reg_314, reg_3.b);
            reg_317 = call(reg_313, reg_314);
          }
          if (tobool(not(get(reg_298, cns1947)))) {
            reg_324 = get(reg_1.a, cns1948);
            reg_325 = newStack();
            push(reg_325, reg_298);
            reg_327 = first(call(reg_324, reg_325));
            reg_328 = cns1949;
            reg_330 = get(reg_327, cns1950);
            reg_331 = newStack();
            push(reg_331, reg_327);
            push(reg_331, cns1951);
            reg_333 = newTable();
            reg_334 = cns1952;
            set(reg_333, reg_334, get(reg_1.a, cns1953));
            push(reg_331, reg_333);
            reg_338 = newTable();
            set(reg_338, cns1954, reg_298);
            push(reg_331, reg_338);
            set(reg_298, reg_328, first(call(reg_330, reg_331)));
          }
          reg_342 = reg_3.c;
          reg_344 = get(reg_342, cns1955);
          reg_345 = newStack();
          push(reg_345, reg_342);
          push(reg_345, get(get(reg_3.b, cns1956), cns1957));
          reg_352 = first(call(reg_344, reg_345));
          reg_353 = reg_3.c;
          reg_355 = get(reg_353, cns1958);
          reg_356 = newStack();
          push(reg_356, reg_353);
          reg_357 = newTable();
          reg_358 = cns1959;
          set(reg_357, reg_358, get(reg_298, cns1960));
          set(reg_357, cns1961, reg_352);
          push(reg_356, reg_357);
          reg_363 = first(call(reg_355, reg_356));
          set(reg_363, cns1962, cns1963);
          reg_366 = cns1964;
          set(reg_363, reg_366, get(reg_298, cns1965));
          reg_369 = newStack();
          push(reg_369, reg_363);
          return reg_369;
        }
        if ((goto_476 || false)) {
          goto_476 = false;
          goto_561 = !tobool(eq(get(reg_3.b, cns1966), cns1967));
          if (!goto_561) {
            if (tobool(not(get(reg_298, cns1968)))) {
              reg_382 = get(reg_1.a, cns1969);
              reg_383 = newStack();
              push(reg_383, reg_298);
              reg_385 = first(call(reg_382, reg_383));
              reg_386 = cns1970;
              reg_388 = get(reg_385, cns1971);
              reg_389 = newStack();
              push(reg_389, reg_385);
              push(reg_389, cns1972);
              reg_391 = newTable();
              reg_392 = cns1973;
              set(reg_391, reg_392, get(reg_1.a, cns1974));
              push(reg_389, reg_391);
              reg_396 = newTable();
              reg_397 = cns1975;
              set(reg_396, reg_397, get(reg_1.a, cns1976));
              push(reg_389, reg_396);
              set(reg_298, reg_386, first(call(reg_388, reg_389)));
            }
            reg_403 = reg_3.c;
            reg_405 = get(reg_403, cns1977);
            reg_406 = newStack();
            push(reg_406, reg_403);
            push(reg_406, get(get(reg_3.b, cns1978), cns1979));
            reg_413 = first(call(reg_405, reg_406));
            reg_414 = newStack();
            reg_415 = reg_3.c;
            reg_417 = get(reg_415, cns1980);
            reg_418 = newStack();
            push(reg_418, reg_415);
            reg_419 = newTable();
            reg_420 = cns1981;
            set(reg_419, reg_420, get(reg_298, cns1982));
            set(reg_419, cns1983, reg_413);
            set(reg_419, cns1984, cns1985);
            reg_426 = cns1986;
            set(reg_419, reg_426, get(get(reg_1.a, cns1987), cns1988));
            push(reg_418, reg_419);
            append(reg_414, call(reg_417, reg_418));
            return reg_414;
          }
          if ((goto_561 || false)) {
            goto_561 = false;
            reg_435 = get(reg_1.a, cns1989);
            reg_436 = newStack();
            reg_437 = cns1990;
            push(reg_436, concat(reg_437, concat(get(reg_3.b, cns1991), cns1992)));
            push(reg_436, reg_3.b);
            reg_445 = call(reg_435, reg_436);
          }
        }
      }
      if ((goto_577 || false)) {
        goto_577 = false;
        goto_698 = !tobool(eq(reg_8, cns1993));
        if (!goto_698) {
          reg_451 = get(reg_1.a, cns1994);
          reg_456 = get(reg_451, add(get(reg_6, cns1995), cns1996));
          goto_605 = !tobool(not(get(reg_3.b, cns1997)));
          if (!goto_605) {
            reg_464 = get(reg_1.a, cns1998);
            reg_465 = newStack();
            push(reg_465, cns1999);
            push(reg_465, reg_3.b);
            reg_468 = call(reg_464, reg_465);
          }
          if ((goto_605 || false)) {
            goto_605 = false;
            goto_682 = !tobool(eq(get(reg_3.b, cns2000), cns2001));
            if (!goto_682) {
              if (tobool(gt(length(get(reg_3.b, cns2002)), cns2003))) {
                reg_484 = get(reg_1.a, cns2004);
                reg_485 = newStack();
                push(reg_485, cns2005);
                push(reg_485, reg_3.b);
                reg_488 = call(reg_484, reg_485);
              }
              if (tobool(not(get(reg_456, cns2006)))) {
                reg_495 = get(reg_1.a, cns2007);
                reg_496 = newStack();
                push(reg_496, reg_456);
                reg_498 = first(call(reg_495, reg_496));
                reg_499 = cns2008;
                reg_501 = get(reg_498, cns2009);
                reg_502 = newStack();
                push(reg_502, reg_498);
                push(reg_502, cns2010);
                reg_504 = newTable();
                set(reg_504, cns2011, reg_456);
                push(reg_502, reg_504);
                reg_506 = newTable();
                reg_507 = cns2012;
                set(reg_506, reg_507, get(reg_1.a, cns2013));
                push(reg_502, reg_506);
                set(reg_456, reg_499, first(call(reg_501, reg_502)));
              }
              reg_513 = newStack();
              reg_514 = reg_3.c;
              reg_516 = get(reg_514, cns2014);
              reg_517 = newStack();
              push(reg_517, reg_514);
              reg_518 = newTable();
              reg_519 = cns2015;
              set(reg_518, reg_519, get(reg_456, cns2016));
              set(reg_518, cns2017, reg_6);
              push(reg_517, reg_518);
              append(reg_513, call(reg_516, reg_517));
              return reg_513;
            }
            if ((goto_682 || false)) {
              goto_682 = false;
              reg_526 = get(reg_1.a, cns2018);
              reg_527 = newStack();
              reg_528 = cns2019;
              push(reg_527, concat(reg_528, concat(get(reg_3.b, cns2020), cns2021)));
              push(reg_527, reg_3.b);
              reg_536 = call(reg_526, reg_527);
            }
          }
        }
        if ((goto_698 || false)) {
          goto_698 = false;
          if (tobool(eq(reg_8, cns2022))) {
            if (tobool(get(reg_3.b, cns2023))) {
              reg_546 = get(reg_1.a, cns2024);
              reg_547 = newStack();
              reg_548 = cns2025;
              push(reg_547, concat(reg_548, concat(get(reg_3.b, cns2026), cns2027)));
              push(reg_547, reg_3.b);
              reg_556 = call(reg_546, reg_547);
            }
            reg_559 = get(reg_1.a, cns2028);
            reg_564 = get(reg_559, add(get(reg_6, cns2029), cns2030));
            reg_568 = length(get(reg_3.b, cns2031));
            if (tobool(ne(reg_568, length(get(reg_564, cns2032))))) {
              reg_576 = get(reg_1.a, cns2033);
              reg_577 = newStack();
              reg_578 = cns2034;
              push(reg_577, concat(reg_578, concat(length(get(reg_564, cns2035)), cns2036)));
              push(reg_577, reg_3.b);
              reg_586 = call(reg_576, reg_577);
            }
            reg_587 = newTable();
            set(reg_587, cns2037, reg_564);
            set(reg_587, cns2038, cns2039);
            set(reg_587, cns2040, newTable());
            reg_595 = get(reg_1.a, cns2041);
            reg_596 = newStack();
            push(reg_596, get(reg_3.b, cns2042));
            reg_600 = call(reg_595, reg_596);
            reg_601 = next(reg_600);
            reg_602 = next(reg_600);
            reg_603 = next(reg_600);
            loop_2: while (true) {
              reg_604 = newStack();
              push(reg_604, reg_602);
              push(reg_604, reg_603);
              reg_605 = call(reg_601, reg_604);
              reg_606 = next(reg_605);
              reg_603 = reg_606;
              reg_607 = next(reg_605);
              if (tobool(eq(reg_603, nil()))) break loop_2;
              reg_611 = reg_3.c;
              reg_613 = get(reg_611, cns2043);
              reg_614 = newStack();
              push(reg_614, reg_611);
              push(reg_614, reg_607);
              push(reg_614, lua$true());
              reg_617 = first(call(reg_613, reg_614));
              reg_618 = nil();
              goto_808 = !tobool(not(get(reg_617, cns2044)));
              if (!goto_808) {
                reg_618 = cns2045;
              }
              if ((goto_808 || false)) {
                goto_808 = false;
                goto_818 = !tobool(ne(get(reg_617, cns2046), cns2047));
                if (!goto_818) {
                  reg_618 = get(reg_617, cns2048);
                }
                if ((goto_818 || false)) {
                  goto_818 = false;
                  reg_632 = get(reg_617, cns2049);
                  if (tobool(ne(reg_632, get(get(reg_564, cns2050), reg_606)))) {
                    reg_640 = get(reg_1.a, cns2051);
                    reg_618 = get(get(reg_640, add(get(reg_617, cns2052), cns2053)), cns2054);
                  }
                }
              }
              if (tobool(reg_618)) {
                reg_651 = get(reg_1.a, cns2055);
                reg_659 = get(get(reg_651, add(get(get(reg_564, cns2056), reg_606), cns2057)), cns2058);
                reg_662 = get(reg_1.a, cns2059);
                reg_663 = newStack();
                reg_664 = cns2060;
                reg_665 = cns2061;
                reg_667 = get(reg_564, cns2062);
                reg_668 = cns2063;
                reg_669 = cns2064;
                push(reg_663, concat(reg_664, concat(reg_606, concat(reg_665, concat(reg_667, concat(reg_668, concat(reg_659, concat(reg_669, concat(reg_618, cns2065)))))))));
                push(reg_663, reg_3.b);
                reg_680 = call(reg_662, reg_663);
              }
              reg_685 = get(get(reg_1.a, cns2066), cns2067);
              reg_686 = newStack();
              push(reg_686, reg_587);
              push(reg_686, reg_617);
              reg_687 = call(reg_685, reg_686);
            }
            reg_690 = get(reg_1.a, cns2068);
            reg_691 = newStack();
            push(reg_691, get(reg_564, cns2069));
            reg_694 = call(reg_690, reg_691);
            reg_695 = next(reg_694);
            reg_696 = next(reg_694);
            reg_697 = next(reg_694);
            loop_1: while (true) {
              reg_698 = newStack();
              push(reg_698, reg_696);
              push(reg_698, reg_697);
              reg_699 = call(reg_695, reg_698);
              reg_700 = next(reg_699);
              reg_697 = reg_700;
              reg_701 = next(reg_699);
              if (tobool(eq(reg_697, nil()))) break loop_1;
              reg_706 = get(reg_587, cns2070);
              reg_707 = newTable();
              set(reg_707, cns2071, cns2072);
              set(reg_707, cns2073, reg_701);
              set(reg_706, reg_700, reg_707);
            }
            reg_711 = newStack();
            reg_712 = reg_3.c;
            reg_714 = get(reg_712, cns2074);
            reg_715 = newStack();
            push(reg_715, reg_712);
            push(reg_715, reg_587);
            append(reg_711, call(reg_714, reg_715));
            return reg_711;
          }
        }
      }
    }
  }
  reg_719 = get(reg_1.a, cns2075);
  reg_720 = newStack();
  push(reg_720, concat(cns2076, reg_8));
  push(reg_720, reg_3.b);
  reg_724 = call(reg_719, reg_720);
  reg_725 = newStack();
  return reg_725;
}
function aulua_auro_syntax$lua_main (reg_0) {
  var reg_2, reg_5, reg_6, reg_9;
  reg_2 = {a: nil()};
  reg_2.a = reg_0;
  reg_5 = get(reg_2.a, cns1805);
  reg_6 = cns1806;
  set(reg_5, reg_6, anyFn((function (a) { return Function_compile_au_call(a,this) }).bind(reg_2)));
  reg_9 = newStack();
  return reg_9;
}
var cns2077 = anyStr("Function")
var cns2078 = anyStr("create_upval_info")
var cns2079 = anyStr("upval_module")
var cns2080 = anyStr("module")
var cns2081 = anyStr("upval_module")
var cns2082 = anyStr("base")
var cns2083 = anyStr("record_m")
var cns2084 = anyStr("module")
var cns2085 = anyStr("items")
var cns2086 = anyStr("upval_module")
var cns2087 = anyStr("argument")
var cns2088 = anyStr("upval_type")
var cns2089 = anyStr("upval_module")
var cns2090 = anyStr("type")
var cns2091 = anyStr("")
var cns2092 = anyStr("upval_new_fn")
var cns2093 = anyStr("upval_module")
var cns2094 = anyStr("func")
var cns2095 = anyStr("new")
var cns2096 = parseNum("1")
var cns2097 = anyStr("upval_type")
var cns2098 = anyStr("upval_new_call")
var cns2099 = anyStr("inst")
var cns2100 = parseNum("1")
var cns2101 = anyStr("upval_new_fn")
var cns2102 = anyStr("parent")
var cns2103 = anyStr("parent")
var cns2104 = anyStr("upval_type")
var cns2105 = anyStr("table")
var cns2106 = anyStr("insert")
var cns2107 = anyStr("items")
var cns2108 = anyStr("name")
var cns2109 = anyStr("0")
var cns2110 = anyStr("tp")
var cns2111 = anyStr("table")
var cns2112 = anyStr("insert")
var cns2113 = anyStr("upval_new_fn")
var cns2114 = anyStr("ins")
var cns2115 = anyStr("id")
var cns2116 = anyStr("table")
var cns2117 = anyStr("insert")
var cns2118 = anyStr("upval_new_call")
var cns2119 = anyStr("upval_arg")
var cns2120 = anyStr("parent_upval_getter")
var cns2121 = anyStr("upval_module")
var cns2122 = anyStr("func")
var cns2123 = anyStr("get0")
var cns2124 = parseNum("1")
var cns2125 = anyStr("upval_type")
var cns2126 = parseNum("1")
var cns2127 = anyStr("upval_accessors")
function Function_create_upval_info (reg_0, reg_1) {
  var reg_4, reg_5, reg_13, reg_14, reg_23, reg_29, reg_31, reg_33, reg_34, reg_38, reg_40, reg_42, reg_43, reg_46, reg_47, reg_52, reg_54, reg_55, reg_56, reg_57, reg_68, reg_73, reg_74, reg_77, reg_86, reg_87, reg_99, reg_100, reg_106, reg_108, reg_110, reg_111, reg_113, reg_114, reg_117, reg_123;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = cns2079;
  set(reg_4, reg_5, first(call(get(reg_1.a, cns2080), newStack())));
  reg_13 = get(reg_4, cns2081);
  reg_14 = cns2082;
  set(reg_13, reg_14, get(reg_1.a, cns2083));
  reg_23 = first(call(get(reg_1.a, cns2084), newStack()));
  set(reg_23, cns2085, newTable());
  set(get(reg_4, cns2086), cns2087, reg_23);
  reg_29 = cns2088;
  reg_31 = get(reg_4, cns2089);
  reg_33 = get(reg_31, cns2090);
  reg_34 = newStack();
  push(reg_34, reg_31);
  push(reg_34, cns2091);
  set(reg_4, reg_29, first(call(reg_33, reg_34)));
  reg_38 = cns2092;
  reg_40 = get(reg_4, cns2093);
  reg_42 = get(reg_40, cns2094);
  reg_43 = newStack();
  push(reg_43, reg_40);
  push(reg_43, cns2095);
  push(reg_43, newTable());
  reg_46 = newTable();
  reg_47 = cns2096;
  set(reg_46, reg_47, get(reg_4, cns2097));
  push(reg_43, reg_46);
  set(reg_4, reg_38, first(call(reg_42, reg_43)));
  reg_52 = cns2098;
  reg_54 = get(reg_4, cns2099);
  reg_55 = newStack();
  push(reg_55, reg_4);
  reg_56 = newTable();
  reg_57 = cns2100;
  set(reg_56, reg_57, get(reg_4, cns2101));
  push(reg_55, reg_56);
  set(reg_4, reg_52, first(call(reg_54, reg_55)));
  if (tobool(get(reg_4, cns2102))) {
    reg_68 = get(get(reg_4, cns2103), cns2104);
    reg_73 = get(get(reg_1.a, cns2105), cns2106);
    reg_74 = newStack();
    push(reg_74, get(reg_23, cns2107));
    reg_77 = newTable();
    set(reg_77, cns2108, cns2109);
    set(reg_77, cns2110, reg_68);
    push(reg_74, reg_77);
    reg_81 = call(reg_73, reg_74);
    reg_86 = get(get(reg_1.a, cns2111), cns2112);
    reg_87 = newStack();
    push(reg_87, get(get(reg_4, cns2113), cns2114));
    push(reg_87, get(reg_68, cns2115));
    reg_94 = call(reg_86, reg_87);
    reg_99 = get(get(reg_1.a, cns2116), cns2117);
    reg_100 = newStack();
    push(reg_100, get(reg_4, cns2118));
    push(reg_100, get(reg_4, cns2119));
    reg_105 = call(reg_99, reg_100);
    reg_106 = cns2120;
    reg_108 = get(reg_4, cns2121);
    reg_110 = get(reg_108, cns2122);
    reg_111 = newStack();
    push(reg_111, reg_108);
    push(reg_111, cns2123);
    reg_113 = newTable();
    reg_114 = cns2124;
    set(reg_113, reg_114, get(reg_4, cns2125));
    push(reg_111, reg_113);
    reg_117 = newTable();
    set(reg_117, cns2126, reg_68);
    push(reg_111, reg_117);
    set(reg_4, reg_106, first(call(reg_110, reg_111)));
  }
  set(reg_4, cns2127, newTable());
  reg_123 = newStack();
  return reg_123;
}
var cns2128 = anyStr("Function")
var cns2129 = anyStr("build_upvals")
var cns2130 = anyStr("upval_module")
var cns2131 = anyStr("argument")
var cns2132 = anyStr("items")
var cns2133 = anyStr("code")
var cns2134 = anyStr("code")
var cns2135 = anyStr("upval_level_regs")
var cns2136 = anyStr("level")
var cns2137 = anyStr("upval_new_call")
var cns2138 = anyStr("parent")
var cns2139 = anyStr("upval_arg")
var cns2140 = anyStr("upval_level_regs")
var cns2141 = anyStr("parent")
var cns2142 = anyStr("level")
var cns2143 = anyStr("parent")
var cns2144 = anyStr("parent")
var cns2145 = anyStr("inst")
var cns2146 = parseNum("1")
var cns2147 = anyStr("parent_upval_getter")
var cns2148 = parseNum("2")
var cns2149 = anyStr("upval_level_regs")
var cns2150 = anyStr("parent")
var cns2151 = anyStr("level")
var cns2152 = anyStr("parent")
var cns2153 = anyStr("inst")
var cns2154 = parseNum("1")
var cns2155 = anyStr("nil_f")
var cns2156 = anyStr("ipairs")
var cns2157 = anyStr("upvals")
var cns2158 = anyStr("upval_id")
var cns2159 = anyStr("any_t")
var cns2160 = anyStr("au_type")
var cns2161 = anyStr("value")
var cns2162 = anyStr("types")
var cns2163 = anyStr("type_id")
var cns2164 = anyStr("table")
var cns2165 = anyStr("insert")
var cns2166 = anyStr("name")
var cns2167 = anyStr("tostring")
var cns2168 = anyStr("tp")
var cns2169 = anyStr("table")
var cns2170 = anyStr("insert")
var cns2171 = anyStr("upval_new_fn")
var cns2172 = anyStr("ins")
var cns2173 = anyStr("id")
var cns2174 = anyStr("table")
var cns2175 = anyStr("insert")
var cns2176 = anyStr("upval_new_call")
var cns2177 = anyStr("table")
var cns2178 = anyStr("move")
var cns2179 = parseNum("1")
var cns2180 = anyStr("code")
var cns2181 = parseNum("1")
var cns2182 = anyStr("code")
function Function_build_upvals (reg_0, reg_1) {
  var reg_4, reg_10, reg_12, reg_15, reg_16, reg_18, reg_25, reg_27, reg_33, reg_38, reg_39, reg_40, reg_41, reg_48, reg_56, reg_57, reg_58, reg_59, reg_64, reg_67, reg_68, reg_71, reg_72, reg_73, reg_74, reg_75, reg_76, reg_78, reg_83, reg_86, reg_94, reg_102, reg_103, reg_104, reg_105, reg_108, reg_109, reg_118, reg_119, reg_131, reg_132, reg_140, reg_141, reg_152;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_10 = get(get(get(reg_4, cns2130), cns2131), cns2132);
  reg_12 = get(reg_4, cns2133);
  set(reg_4, cns2134, newTable());
  reg_15 = cns2135;
  reg_16 = newTable();
  reg_18 = get(reg_4, cns2136);
  set(reg_16, reg_18, get(reg_4, cns2137));
  set(reg_4, reg_15, reg_16);
  if (tobool(get(reg_4, cns2138))) {
    reg_25 = get(reg_4, cns2139);
    reg_27 = get(reg_4, cns2140);
    set(reg_27, get(get(reg_4, cns2141), cns2142), reg_25);
    reg_33 = get(reg_4, cns2143);
    loop_2: while (tobool(get(reg_33, cns2144))) {
      reg_38 = get(reg_4, cns2145);
      reg_39 = newStack();
      push(reg_39, reg_4);
      reg_40 = newTable();
      reg_41 = cns2146;
      set(reg_40, reg_41, get(reg_33, cns2147));
      set(reg_40, cns2148, reg_25);
      push(reg_39, reg_40);
      reg_25 = first(call(reg_38, reg_39));
      reg_48 = get(reg_4, cns2149);
      set(reg_48, get(get(reg_33, cns2150), cns2151), reg_25);
      reg_33 = get(reg_33, cns2152);
    }
  }
  reg_56 = get(reg_4, cns2153);
  reg_57 = newStack();
  push(reg_57, reg_4);
  reg_58 = newTable();
  reg_59 = cns2154;
  set(reg_58, reg_59, get(reg_1.a, cns2155));
  push(reg_57, reg_58);
  reg_64 = first(call(reg_56, reg_57));
  reg_67 = get(reg_1.a, cns2156);
  reg_68 = newStack();
  push(reg_68, get(reg_4, cns2157));
  reg_71 = call(reg_67, reg_68);
  reg_72 = next(reg_71);
  reg_73 = next(reg_71);
  reg_74 = next(reg_71);
  loop_1: while (true) {
    reg_75 = newStack();
    push(reg_75, reg_73);
    push(reg_75, reg_74);
    reg_76 = call(reg_72, reg_75);
    reg_74 = next(reg_76);
    reg_78 = next(reg_76);
    if (tobool(eq(reg_74, nil()))) break loop_1;
    reg_83 = get(reg_78, cns2158);
    reg_86 = get(reg_1.a, cns2159);
    if (tobool(eq(get(reg_78, cns2160), cns2161))) {
      reg_94 = get(reg_1.a, cns2162);
      reg_86 = get(reg_94, get(reg_78, cns2163));
    }
    reg_102 = get(get(reg_1.a, cns2164), cns2165);
    reg_103 = newStack();
    push(reg_103, reg_10);
    reg_104 = newTable();
    reg_105 = cns2166;
    reg_108 = get(reg_1.a, cns2167);
    reg_109 = newStack();
    push(reg_109, reg_83);
    set(reg_104, reg_105, first(call(reg_108, reg_109)));
    set(reg_104, cns2168, reg_86);
    push(reg_103, reg_104);
    reg_113 = call(reg_102, reg_103);
    reg_118 = get(get(reg_1.a, cns2169), cns2170);
    reg_119 = newStack();
    push(reg_119, get(get(reg_4, cns2171), cns2172));
    push(reg_119, get(reg_86, cns2173));
    reg_126 = call(reg_118, reg_119);
    reg_131 = get(get(reg_1.a, cns2174), cns2175);
    reg_132 = newStack();
    push(reg_132, get(reg_4, cns2176));
    push(reg_132, reg_64);
    reg_135 = call(reg_131, reg_132);
  }
  reg_140 = get(get(reg_1.a, cns2177), cns2178);
  reg_141 = newStack();
  push(reg_141, reg_12);
  push(reg_141, cns2179);
  push(reg_141, length(reg_12));
  push(reg_141, add(length(get(reg_4, cns2180)), cns2181));
  push(reg_141, get(reg_4, cns2182));
  reg_151 = call(reg_140, reg_141);
  reg_152 = newStack();
  return reg_152;
}
var cns2183 = anyStr("Function")
var cns2184 = anyStr("get_vararg")
var cns2185 = anyStr("vararg")
var cns2186 = anyStr("vararg")
var cns2187 = anyStr("error")
var cns2188 = anyStr("cannot use '...' outside a vararg function")
function Function_get_vararg (reg_0, reg_1) {
  var reg_4, reg_8, reg_13, reg_14, reg_17;
  var goto_13=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  goto_13 = !tobool(get(reg_4, cns2185));
  if (!goto_13) {
    reg_8 = newStack();
    push(reg_8, get(reg_4, cns2186));
    return reg_8;
  }
  if ((goto_13 || false)) {
    goto_13 = false;
    reg_13 = get(reg_1.a, cns2187);
    reg_14 = newStack();
    push(reg_14, cns2188);
    reg_16 = call(reg_13, reg_14);
  }
  reg_17 = newStack();
  return reg_17;
}
var cns2189 = anyStr("Function")
var cns2190 = anyStr("get_local")
var cns2191 = anyStr("scopes")
var cns2192 = parseNum("1")
var cns2193 = parseNum("1")
var cns2194 = parseNum("0")
var cns2195 = anyStr("scopes")
var cns2196 = anyStr("locals")
var cns2197 = anyStr("au_type")
var cns2198 = anyStr("au_type")
var cns2199 = anyStr("value")
var cns2200 = anyStr("is_upval")
var cns2201 = anyStr("upvals")
var cns2202 = anyStr("parent")
var cns2203 = parseNum("1")
var cns2204 = anyStr("is_upval")
var cns2205 = anyStr("upval_level")
var cns2206 = anyStr("level")
var cns2207 = anyStr("upval_id")
var cns2208 = anyStr("any_t")
var cns2209 = anyStr("au_type")
var cns2210 = anyStr("value")
var cns2211 = anyStr("types")
var cns2212 = anyStr("reg")
var cns2213 = anyStr("type_id")
var cns2214 = anyStr("upval_accessors")
var cns2215 = anyStr("getter")
var cns2216 = anyStr("upval_module")
var cns2217 = anyStr("func")
var cns2218 = anyStr("get")
var cns2219 = parseNum("1")
var cns2220 = anyStr("upval_type")
var cns2221 = parseNum("1")
var cns2222 = anyStr("setter")
var cns2223 = anyStr("upval_module")
var cns2224 = anyStr("func")
var cns2225 = anyStr("set")
var cns2226 = parseNum("1")
var cns2227 = anyStr("upval_type")
var cns2228 = parseNum("2")
var cns2229 = anyStr("table")
var cns2230 = anyStr("insert")
var cns2231 = anyStr("upvals")
var cns2232 = anyStr("parent")
var cns2233 = anyStr("parent")
var cns2234 = anyStr("get_local")
function Function_get_local (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_11, reg_13, reg_15, reg_31, reg_37, reg_47, reg_55, reg_61, reg_69, reg_77, reg_78, reg_79, reg_81, reg_83, reg_84, reg_87, reg_88, reg_91, reg_95, reg_97, reg_99, reg_100, reg_103, reg_104, reg_115, reg_116, reg_120, reg_124, reg_126, reg_128, reg_129, reg_132;
  var goto_20=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = nil();
  reg_10 = length(get(reg_4, cns2191));
  reg_11 = cns2192;
  reg_13 = unm(cns2193);
  reg_15 = lt(reg_13, cns2194);
  loop_1: while (true) {
    goto_20 = tobool(reg_15);
    if (!goto_20) {
      if (tobool(gt(reg_10, reg_11))) break loop_1;
    }
    if ((goto_20 || false)) {
      goto_20 = false;
      if (tobool(lt(reg_10, reg_11))) break loop_1;
    }
    reg_7 = get(get(get(get(reg_4, cns2195), reg_10), cns2196), reg_5);
    if (tobool(reg_7)) {
      if (true) break loop_1;
    }
    reg_10 = add(reg_10, reg_13);
  }
  if (tobool(reg_7)) {
    reg_31 = get(reg_7, cns2197);
    if (tobool(reg_31)) {
      reg_31 = ne(get(reg_7, cns2198), cns2199);
    }
    reg_37 = reg_6;
    if (tobool(reg_6)) {
      reg_37 = not(get(reg_7, cns2200));
    }
    if (tobool(reg_37)) {
      reg_37 = not(reg_31);
    }
    if (tobool(reg_37)) {
      reg_47 = length(get(reg_4, cns2201));
      if (tobool(get(reg_4, cns2202))) {
        reg_47 = add(reg_47, cns2203);
      }
      set(reg_7, cns2204, lua$true());
      reg_55 = cns2205;
      set(reg_7, reg_55, get(reg_4, cns2206));
      set(reg_7, cns2207, reg_47);
      reg_61 = get(reg_1.a, cns2208);
      if (tobool(eq(get(reg_7, cns2209), cns2210))) {
        reg_69 = get(reg_1.a, cns2211);
        reg_61 = get(reg_69, get(get(reg_1.a, cns2212), cns2213));
      }
      reg_77 = get(reg_4, cns2214);
      reg_78 = newTable();
      reg_79 = cns2215;
      reg_81 = get(reg_4, cns2216);
      reg_83 = get(reg_81, cns2217);
      reg_84 = newStack();
      push(reg_84, reg_81);
      push(reg_84, concat(cns2218, reg_47));
      reg_87 = newTable();
      reg_88 = cns2219;
      set(reg_87, reg_88, get(reg_4, cns2220));
      push(reg_84, reg_87);
      reg_91 = newTable();
      set(reg_91, cns2221, reg_61);
      push(reg_84, reg_91);
      set(reg_78, reg_79, first(call(reg_83, reg_84)));
      reg_95 = cns2222;
      reg_97 = get(reg_4, cns2223);
      reg_99 = get(reg_97, cns2224);
      reg_100 = newStack();
      push(reg_100, reg_97);
      push(reg_100, concat(cns2225, reg_47));
      reg_103 = newTable();
      reg_104 = cns2226;
      set(reg_103, reg_104, get(reg_4, cns2227));
      set(reg_103, cns2228, reg_61);
      push(reg_100, reg_103);
      push(reg_100, newTable());
      set(reg_78, reg_95, first(call(reg_99, reg_100)));
      set(reg_77, reg_47, reg_78);
      reg_115 = get(get(reg_1.a, cns2229), cns2230);
      reg_116 = newStack();
      push(reg_116, get(reg_4, cns2231));
      push(reg_116, reg_7);
      reg_119 = call(reg_115, reg_116);
    }
    reg_120 = newStack();
    push(reg_120, reg_7);
    return reg_120;
  }
  if (tobool(get(reg_4, cns2232))) {
    reg_124 = newStack();
    reg_126 = get(reg_4, cns2233);
    reg_128 = get(reg_126, cns2234);
    reg_129 = newStack();
    push(reg_129, reg_126);
    push(reg_129, reg_5);
    push(reg_129, lua$true());
    append(reg_124, call(reg_128, reg_129));
    return reg_124;
  }
  reg_132 = newStack();
  return reg_132;
}
var cns2235 = anyStr("Function")
var cns2236 = anyStr("get_level_ancestor")
var cns2237 = anyStr("level")
var cns2238 = anyStr("parent")
var cns2239 = anyStr("error")
var cns2240 = anyStr("Could not find ancestor of level ")
function Function_get_level_ancestor (reg_0, reg_1) {
  var reg_4, reg_5, reg_11, reg_16, reg_17, reg_21;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  loop_1: while (tobool(reg_4)) {
    if (tobool(eq(get(reg_4, cns2237), reg_5))) {
      reg_11 = newStack();
      push(reg_11, reg_4);
      return reg_11;
    }
    reg_4 = get(reg_4, cns2238);
  }
  reg_16 = get(reg_1.a, cns2239);
  reg_17 = newStack();
  push(reg_17, concat(cns2240, reg_5));
  reg_20 = call(reg_16, reg_17);
  reg_21 = newStack();
  return reg_21;
}
var cns2241 = anyStr("Function")
var cns2242 = anyStr("createFunction")
var cns2243 = anyStr("code")
var cns2244 = anyStr("repr")
var cns2245 = anyStr("function")
var cns2246 = anyStr("node")
var cns2247 = anyStr("parent")
var cns2248 = anyStr("level")
var cns2249 = anyStr("level")
var cns2250 = parseNum("1")
var cns2251 = anyStr("ins")
var cns2252 = parseNum("1")
var cns2253 = anyStr("stack_t")
var cns2254 = anyStr("id")
var cns2255 = parseNum("2")
var cns2256 = anyStr("upval_type")
var cns2257 = anyStr("id")
var cns2258 = anyStr("outs")
var cns2259 = parseNum("1")
var cns2260 = anyStr("stack_t")
var cns2261 = anyStr("id")
var cns2262 = anyStr("line")
var cns2263 = anyStr("line")
var cns2264 = anyStr("reg")
var cns2265 = parseNum("0")
var cns2266 = anyStr("vararg")
var cns2267 = anyStr("vararg")
var cns2268 = anyStr("upval_arg")
var cns2269 = anyStr("reg")
var cns2270 = parseNum("1")
var cns2271 = anyStr("create_upval_info")
var cns2272 = anyStr("method")
var cns2273 = anyStr("table")
var cns2274 = anyStr("insert")
var cns2275 = anyStr("names")
var cns2276 = parseNum("1")
var cns2277 = anyStr("self")
var cns2278 = anyStr("ipairs")
var cns2279 = anyStr("names")
var cns2280 = anyStr("inst")
var cns2281 = parseNum("1")
var cns2282 = anyStr("next_f")
var cns2283 = parseNum("2")
var cns2284 = anyStr("scope")
var cns2285 = anyStr("locals")
var cns2286 = anyStr("inst")
var cns2287 = parseNum("1")
var cns2288 = anyStr("local")
var cns2289 = parseNum("2")
var cns2290 = anyStr("compileBlock")
var cns2291 = anyStr("body")
var cns2292 = anyStr("body")
var cns2293 = parseNum("0")
var cns2294 = anyStr("body")
var cns2295 = anyStr("body")
var cns2296 = anyStr("type")
var cns2297 = anyStr("return")
var cns2298 = anyStr("call")
var cns2299 = anyStr("stack_f")
var cns2300 = anyStr("inst")
var cns2301 = parseNum("1")
var cns2302 = anyStr("end")
var cns2303 = parseNum("2")
var cns2304 = anyStr("build_upvals")
var cns2305 = anyStr("transform")
var cns2306 = anyStr("module")
var cns2307 = anyStr("items")
var cns2308 = parseNum("1")
var cns2309 = anyStr("name")
var cns2310 = anyStr("0")
var cns2311 = anyStr("fn")
var cns2312 = anyStr("module")
var cns2313 = anyStr("base")
var cns2314 = anyStr("closure_m")
var cns2315 = anyStr("argument")
var cns2316 = anyStr("func")
var cns2317 = anyStr("new")
var cns2318 = parseNum("1")
var cns2319 = anyStr("upval_type")
var cns2320 = parseNum("1")
var cns2321 = anyStr("func_t")
var cns2322 = anyStr("call")
var cns2323 = anyStr("upval_new_call")
var cns2324 = anyStr("call")
var cns2325 = anyStr("func_f")
function Function_createFunction (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_11, reg_15, reg_18, reg_23, reg_24, reg_25, reg_31, reg_36, reg_37, reg_38, reg_44, reg_47, reg_54, reg_55, reg_59, reg_60, reg_69, reg_70, reg_78, reg_79, reg_82, reg_83, reg_84, reg_85, reg_86, reg_87, reg_89, reg_94, reg_95, reg_96, reg_97, reg_103, reg_107, reg_109, reg_110, reg_111, reg_118, reg_119, reg_127, reg_130, reg_141, reg_142, reg_147, reg_149, reg_150, reg_151, reg_157, reg_158, reg_161, reg_162, reg_169, reg_170, reg_171, reg_172, reg_173, reg_182, reg_183, reg_189, reg_190, reg_192, reg_193, reg_196, reg_197, reg_202, reg_204, reg_205, reg_209, reg_210, reg_212, reg_213;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = get(reg_1.a, cns2243);
  reg_9 = newStack();
  reg_11 = get(reg_5, cns2244);
  if (!tobool(reg_11)) {
    reg_11 = cns2245;
  }
  push(reg_9, reg_11);
  reg_15 = first(call(reg_8, reg_9));
  set(reg_15, cns2246, reg_5);
  set(reg_15, cns2247, reg_4);
  reg_18 = cns2248;
  set(reg_15, reg_18, add(get(reg_4, cns2249), cns2250));
  reg_23 = cns2251;
  reg_24 = newTable();
  reg_25 = cns2252;
  set(reg_24, reg_25, get(get(reg_1.a, cns2253), cns2254));
  reg_31 = cns2255;
  set(reg_24, reg_31, get(get(reg_4, cns2256), cns2257));
  set(reg_15, reg_23, reg_24);
  reg_36 = cns2258;
  reg_37 = newTable();
  reg_38 = cns2259;
  set(reg_37, reg_38, get(get(reg_1.a, cns2260), cns2261));
  set(reg_15, reg_36, reg_37);
  reg_44 = cns2262;
  set(reg_15, reg_44, get(reg_5, cns2263));
  reg_47 = newTable();
  set(reg_47, cns2264, cns2265);
  if (tobool(get(reg_5, cns2266))) {
    set(reg_15, cns2267, reg_47);
  }
  reg_54 = cns2268;
  reg_55 = newTable();
  set(reg_55, cns2269, cns2270);
  set(reg_15, reg_54, reg_55);
  reg_59 = get(reg_15, cns2271);
  reg_60 = newStack();
  push(reg_60, reg_15);
  reg_61 = call(reg_59, reg_60);
  if (tobool(get(reg_5, cns2272))) {
    reg_69 = get(get(reg_1.a, cns2273), cns2274);
    reg_70 = newStack();
    push(reg_70, get(reg_5, cns2275));
    push(reg_70, cns2276);
    push(reg_70, cns2277);
    reg_75 = call(reg_69, reg_70);
  }
  reg_78 = get(reg_1.a, cns2278);
  reg_79 = newStack();
  push(reg_79, get(reg_5, cns2279));
  reg_82 = call(reg_78, reg_79);
  reg_83 = next(reg_82);
  reg_84 = next(reg_82);
  reg_85 = next(reg_82);
  loop_1: while (true) {
    reg_86 = newStack();
    push(reg_86, reg_84);
    push(reg_86, reg_85);
    reg_87 = call(reg_83, reg_86);
    reg_85 = next(reg_87);
    reg_89 = next(reg_87);
    if (tobool(eq(reg_85, nil()))) break loop_1;
    reg_94 = get(reg_15, cns2280);
    reg_95 = newStack();
    push(reg_95, reg_15);
    reg_96 = newTable();
    reg_97 = cns2281;
    set(reg_96, reg_97, get(reg_1.a, cns2282));
    set(reg_96, cns2283, reg_47);
    push(reg_95, reg_96);
    reg_103 = first(call(reg_94, reg_95));
    reg_107 = get(get(reg_15, cns2284), cns2285);
    reg_109 = get(reg_15, cns2286);
    reg_110 = newStack();
    push(reg_110, reg_15);
    reg_111 = newTable();
    set(reg_111, cns2287, cns2288);
    set(reg_111, cns2289, reg_103);
    push(reg_110, reg_111);
    set(reg_107, reg_89, first(call(reg_109, reg_110)));
  }
  reg_118 = get(reg_15, cns2290);
  reg_119 = newStack();
  push(reg_119, reg_15);
  push(reg_119, get(reg_5, cns2291));
  reg_122 = call(reg_118, reg_119);
  reg_127 = eq(length(get(reg_5, cns2292)), cns2293);
  if (!tobool(reg_127)) {
    reg_130 = get(reg_5, cns2294);
    reg_127 = ne(get(get(reg_130, length(get(reg_5, cns2295))), cns2296), cns2297);
  }
  if (tobool(reg_127)) {
    reg_141 = get(reg_15, cns2298);
    reg_142 = newStack();
    push(reg_142, reg_15);
    push(reg_142, get(reg_1.a, cns2299));
    reg_147 = first(call(reg_141, reg_142));
    reg_149 = get(reg_15, cns2300);
    reg_150 = newStack();
    push(reg_150, reg_15);
    reg_151 = newTable();
    set(reg_151, cns2301, cns2302);
    set(reg_151, cns2303, reg_147);
    push(reg_150, reg_151);
    reg_155 = call(reg_149, reg_150);
  }
  reg_157 = get(reg_15, cns2304);
  reg_158 = newStack();
  push(reg_158, reg_15);
  reg_159 = call(reg_157, reg_158);
  reg_161 = get(reg_15, cns2305);
  reg_162 = newStack();
  push(reg_162, reg_15);
  reg_163 = call(reg_161, reg_162);
  reg_169 = first(call(get(reg_1.a, cns2306), newStack()));
  reg_170 = cns2307;
  reg_171 = newTable();
  reg_172 = cns2308;
  reg_173 = newTable();
  set(reg_173, cns2309, cns2310);
  set(reg_173, cns2311, reg_15);
  set(reg_171, reg_172, reg_173);
  set(reg_169, reg_170, reg_171);
  reg_182 = first(call(get(reg_1.a, cns2312), newStack()));
  reg_183 = cns2313;
  set(reg_182, reg_183, get(reg_1.a, cns2314));
  set(reg_182, cns2315, reg_169);
  reg_189 = get(reg_182, cns2316);
  reg_190 = newStack();
  push(reg_190, reg_182);
  push(reg_190, cns2317);
  reg_192 = newTable();
  reg_193 = cns2318;
  set(reg_192, reg_193, get(reg_4, cns2319));
  push(reg_190, reg_192);
  reg_196 = newTable();
  reg_197 = cns2320;
  set(reg_196, reg_197, get(reg_1.a, cns2321));
  push(reg_190, reg_196);
  reg_202 = first(call(reg_189, reg_190));
  reg_204 = get(reg_4, cns2322);
  reg_205 = newStack();
  push(reg_205, reg_4);
  push(reg_205, reg_202);
  push(reg_205, get(reg_4, cns2323));
  reg_209 = first(call(reg_204, reg_205));
  reg_210 = newStack();
  reg_212 = get(reg_4, cns2324);
  reg_213 = newStack();
  push(reg_213, reg_4);
  push(reg_213, get(reg_1.a, cns2325));
  push(reg_213, reg_209);
  append(reg_210, call(reg_212, reg_213));
  return reg_210;
}
var cns2326 = anyStr("Function")
var cns2327 = anyStr("compile_require")
var cns2328 = anyStr("key")
var cns2329 = anyStr("base")
var cns2330 = anyStr("type")
var cns2331 = anyStr("var")
var cns2332 = anyStr("base")
var cns2333 = anyStr("name")
var cns2334 = anyStr("require")
var cns2335 = anyStr("get_local")
var cns2336 = anyStr("require")
var cns2337 = anyStr("values")
var cns2338 = parseNum("1")
var cns2339 = anyStr("error")
var cns2340 = anyStr("lua:")
var cns2341 = anyStr("line")
var cns2342 = anyStr(": require expects exactly one argument")
var cns2343 = anyStr("values")
var cns2344 = parseNum("1")
var cns2345 = anyStr("type")
var cns2346 = anyStr("str")
var cns2347 = anyStr("error")
var cns2348 = anyStr("lua:")
var cns2349 = anyStr("line")
var cns2350 = anyStr(": module name can only be a string literal")
var cns2351 = anyStr("module")
var cns2352 = anyStr("values")
var cns2353 = parseNum("1")
var cns2354 = anyStr("value")
var cns2355 = anyStr("func")
var cns2356 = anyStr("lua_main")
var cns2357 = parseNum("1")
var cns2358 = anyStr("any_t")
var cns2359 = parseNum("1")
var cns2360 = anyStr("stack_t")
var cns2361 = anyStr("get_local")
var cns2362 = anyStr(".ENV")
var cns2363 = anyStr("inst")
var cns2364 = parseNum("1")
var cns2365 = parseNum("2")
function Function_compile_require (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_24, reg_25, reg_30, reg_39, reg_40, reg_41, reg_59, reg_60, reg_61, reg_70, reg_71, reg_79, reg_81, reg_82, reg_84, reg_85, reg_89, reg_90, reg_95, reg_97, reg_98, reg_101, reg_102, reg_104, reg_105, reg_106;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = get(reg_5, cns2328);
  if (!tobool(reg_7)) {
    reg_7 = ne(get(get(reg_5, cns2329), cns2330), cns2331);
  }
  if (!tobool(reg_7)) {
    reg_7 = ne(get(get(reg_5, cns2332), cns2333), cns2334);
  }
  if (!tobool(reg_7)) {
    reg_24 = get(reg_4, cns2335);
    reg_25 = newStack();
    push(reg_25, reg_4);
    push(reg_25, cns2336);
    reg_7 = first(call(reg_24, reg_25));
  }
  if (tobool(reg_7)) {
    reg_30 = newStack();
    return reg_30;
  }
  if (tobool(ne(length(get(reg_5, cns2337)), cns2338))) {
    reg_39 = get(reg_1.a, cns2339);
    reg_40 = newStack();
    reg_41 = cns2340;
    push(reg_40, concat(reg_41, concat(get(reg_5, cns2341), cns2342)));
    reg_47 = call(reg_39, reg_40);
  }
  if (tobool(ne(get(get(get(reg_5, cns2343), cns2344), cns2345), cns2346))) {
    reg_59 = get(reg_1.a, cns2347);
    reg_60 = newStack();
    reg_61 = cns2348;
    push(reg_60, concat(reg_61, concat(get(reg_5, cns2349), cns2350)));
    reg_67 = call(reg_59, reg_60);
  }
  reg_70 = get(reg_1.a, cns2351);
  reg_71 = newStack();
  push(reg_71, get(get(get(reg_5, cns2352), cns2353), cns2354));
  reg_79 = first(call(reg_70, reg_71));
  reg_81 = get(reg_79, cns2355);
  reg_82 = newStack();
  push(reg_82, reg_79);
  push(reg_82, cns2356);
  reg_84 = newTable();
  reg_85 = cns2357;
  set(reg_84, reg_85, get(reg_1.a, cns2358));
  push(reg_82, reg_84);
  reg_89 = newTable();
  reg_90 = cns2359;
  set(reg_89, reg_90, get(reg_1.a, cns2360));
  push(reg_82, reg_89);
  reg_95 = first(call(reg_81, reg_82));
  reg_97 = get(reg_4, cns2361);
  reg_98 = newStack();
  push(reg_98, reg_4);
  push(reg_98, cns2362);
  reg_101 = first(call(reg_97, reg_98));
  reg_102 = newStack();
  reg_104 = get(reg_4, cns2363);
  reg_105 = newStack();
  push(reg_105, reg_4);
  reg_106 = newTable();
  set(reg_106, cns2364, reg_95);
  set(reg_106, cns2365, reg_101);
  push(reg_105, reg_106);
  append(reg_102, call(reg_104, reg_105));
  return reg_102;
}
var cns2366 = anyStr("Function")
var cns2367 = anyStr("compile_call")
var cns2368 = anyStr("compile_require")
var cns2369 = anyStr("compileExpr")
var cns2370 = anyStr("base")
var cns2371 = anyStr("au_type")
var cns2372 = anyStr("compile_au_call")
var cns2373 = anyStr("au_type")
var cns2374 = anyStr("call")
var cns2375 = anyStr("stack_f")
var cns2376 = anyStr("inst")
var cns2377 = parseNum("1")
var cns2378 = anyStr("push_f")
var cns2379 = parseNum("2")
var cns2380 = parseNum("3")
var cns2381 = anyStr("err")
var cns2382 = anyStr("cannot use a auro expression as value")
var cns2383 = anyStr("key")
var cns2384 = anyStr("compileExpr")
var cns2385 = anyStr("type")
var cns2386 = anyStr("str")
var cns2387 = anyStr("value")
var cns2388 = anyStr("key")
var cns2389 = anyStr("inst")
var cns2390 = parseNum("1")
var cns2391 = anyStr("get_f")
var cns2392 = parseNum("2")
var cns2393 = parseNum("3")
var cns2394 = anyStr("call")
var cns2395 = anyStr("stack_f")
var cns2396 = anyStr("key")
var cns2397 = anyStr("inst")
var cns2398 = parseNum("1")
var cns2399 = anyStr("push_f")
var cns2400 = parseNum("2")
var cns2401 = parseNum("3")
var cns2402 = anyStr("ipairs")
var cns2403 = anyStr("values")
var cns2404 = anyStr("values")
var cns2405 = anyStr("type")
var cns2406 = anyStr("call")
var cns2407 = anyStr("compile_call")
var cns2408 = anyStr("inst")
var cns2409 = parseNum("1")
var cns2410 = anyStr("append_f")
var cns2411 = parseNum("2")
var cns2412 = parseNum("3")
var cns2413 = anyStr("type")
var cns2414 = anyStr("vararg")
var cns2415 = anyStr("inst")
var cns2416 = parseNum("1")
var cns2417 = anyStr("append_f")
var cns2418 = parseNum("2")
var cns2419 = parseNum("3")
var cns2420 = anyStr("get_vararg")
var cns2421 = anyStr("compileExpr")
var cns2422 = anyStr("inst")
var cns2423 = parseNum("1")
var cns2424 = anyStr("push_f")
var cns2425 = parseNum("2")
var cns2426 = parseNum("3")
var cns2427 = anyStr("inst")
var cns2428 = parseNum("1")
var cns2429 = anyStr("call_f")
var cns2430 = parseNum("2")
var cns2431 = parseNum("3")
var cns2432 = anyStr("line")
var cns2433 = anyStr("line")
function Function_compile_call (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_8, reg_9, reg_11, reg_13, reg_15, reg_16, reg_21, reg_26, reg_27, reg_29, reg_35, reg_36, reg_41, reg_43, reg_44, reg_45, reg_46, reg_53, reg_55, reg_58, reg_59, reg_62, reg_67, reg_68, reg_69, reg_72, reg_76, reg_78, reg_79, reg_80, reg_81, reg_90, reg_91, reg_96, reg_101, reg_102, reg_103, reg_104, reg_113, reg_114, reg_117, reg_118, reg_119, reg_120, reg_121, reg_122, reg_123, reg_124, reg_131, reg_132, reg_140, reg_141, reg_143, reg_145, reg_146, reg_147, reg_148, reg_155, reg_163, reg_164, reg_165, reg_166, reg_171, reg_173, reg_174, reg_178, reg_179, reg_181, reg_183, reg_184, reg_185, reg_186, reg_193, reg_195, reg_196, reg_197, reg_198, reg_204;
  var goto_76=false, goto_82=false, goto_221=false, goto_253=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_8 = get(reg_4, cns2368);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, reg_5);
  reg_11 = first(call(reg_8, reg_9));
  if (tobool(reg_11)) {
    reg_13 = newStack();
    push(reg_13, reg_11);
    return reg_13;
  }
  reg_15 = get(reg_4, cns2369);
  reg_16 = newStack();
  push(reg_16, reg_4);
  push(reg_16, get(reg_5, cns2370));
  push(reg_16, lua$true());
  reg_21 = first(call(reg_15, reg_16));
  if (tobool(get(reg_21, cns2371))) {
    reg_26 = get(reg_4, cns2372);
    reg_27 = newStack();
    push(reg_27, reg_4);
    push(reg_27, reg_5);
    push(reg_27, reg_21);
    reg_29 = first(call(reg_26, reg_27));
    goto_76 = !tobool(not(get(reg_29, cns2373)));
    if (!goto_76) {
      reg_35 = get(reg_4, cns2374);
      reg_36 = newStack();
      push(reg_36, reg_4);
      push(reg_36, get(reg_1.a, cns2375));
      reg_41 = first(call(reg_35, reg_36));
      reg_43 = get(reg_4, cns2376);
      reg_44 = newStack();
      push(reg_44, reg_4);
      reg_45 = newTable();
      reg_46 = cns2377;
      set(reg_45, reg_46, get(reg_1.a, cns2378));
      set(reg_45, cns2379, reg_41);
      set(reg_45, cns2380, reg_29);
      push(reg_44, reg_45);
      reg_52 = call(reg_43, reg_44);
      reg_53 = newStack();
      push(reg_53, reg_41);
      return reg_53;
    }
    if ((goto_76 || false)) {
      goto_76 = false;
      goto_82 = !tobool(reg_6);
      if (!goto_82) {
        reg_55 = newStack();
        push(reg_55, reg_29);
        return reg_55;
      }
      if ((goto_82 || false)) {
        goto_82 = false;
        reg_58 = get(reg_1.a, cns2381);
        reg_59 = newStack();
        push(reg_59, cns2382);
        push(reg_59, reg_5);
        reg_61 = call(reg_58, reg_59);
      }
    }
  }
  reg_62 = reg_21;
  if (tobool(get(reg_5, cns2383))) {
    reg_67 = get(reg_4, cns2384);
    reg_68 = newStack();
    push(reg_68, reg_4);
    reg_69 = newTable();
    set(reg_69, cns2385, cns2386);
    reg_72 = cns2387;
    set(reg_69, reg_72, get(reg_5, cns2388));
    push(reg_68, reg_69);
    reg_76 = first(call(reg_67, reg_68));
    reg_78 = get(reg_4, cns2389);
    reg_79 = newStack();
    push(reg_79, reg_4);
    reg_80 = newTable();
    reg_81 = cns2390;
    set(reg_80, reg_81, get(reg_1.a, cns2391));
    set(reg_80, cns2392, reg_21);
    set(reg_80, cns2393, reg_76);
    push(reg_79, reg_80);
    reg_62 = first(call(reg_78, reg_79));
  }
  reg_90 = get(reg_4, cns2394);
  reg_91 = newStack();
  push(reg_91, reg_4);
  push(reg_91, get(reg_1.a, cns2395));
  reg_96 = first(call(reg_90, reg_91));
  if (tobool(get(reg_5, cns2396))) {
    reg_101 = get(reg_4, cns2397);
    reg_102 = newStack();
    push(reg_102, reg_4);
    reg_103 = newTable();
    reg_104 = cns2398;
    set(reg_103, reg_104, get(reg_1.a, cns2399));
    set(reg_103, cns2400, reg_96);
    set(reg_103, cns2401, reg_21);
    push(reg_102, reg_103);
    reg_110 = call(reg_101, reg_102);
  }
  reg_113 = get(reg_1.a, cns2402);
  reg_114 = newStack();
  push(reg_114, get(reg_5, cns2403));
  reg_117 = call(reg_113, reg_114);
  reg_118 = next(reg_117);
  reg_119 = next(reg_117);
  reg_120 = next(reg_117);
  loop_1: while (true) {
    reg_121 = newStack();
    push(reg_121, reg_119);
    push(reg_121, reg_120);
    reg_122 = call(reg_118, reg_121);
    reg_123 = next(reg_122);
    reg_120 = reg_123;
    reg_124 = next(reg_122);
    if (tobool(eq(reg_120, nil()))) break loop_1;
    reg_131 = eq(reg_123, length(get(reg_5, cns2404)));
    reg_132 = reg_131;
    if (tobool(reg_131)) {
      reg_132 = eq(get(reg_124, cns2405), cns2406);
    }
    goto_221 = !tobool(reg_132);
    if (!goto_221) {
      reg_140 = get(reg_4, cns2407);
      reg_141 = newStack();
      push(reg_141, reg_4);
      push(reg_141, reg_124);
      reg_143 = first(call(reg_140, reg_141));
      reg_145 = get(reg_4, cns2408);
      reg_146 = newStack();
      push(reg_146, reg_4);
      reg_147 = newTable();
      reg_148 = cns2409;
      set(reg_147, reg_148, get(reg_1.a, cns2410));
      set(reg_147, cns2411, reg_96);
      set(reg_147, cns2412, reg_143);
      push(reg_146, reg_147);
      reg_154 = call(reg_145, reg_146);
    }
    if ((goto_221 || false)) {
      goto_221 = false;
      reg_155 = reg_131;
      if (tobool(reg_131)) {
        reg_155 = eq(get(reg_124, cns2413), cns2414);
      }
      goto_253 = !tobool(reg_155);
      if (!goto_253) {
        reg_163 = get(reg_4, cns2415);
        reg_164 = newStack();
        push(reg_164, reg_4);
        reg_165 = newTable();
        reg_166 = cns2416;
        set(reg_165, reg_166, get(reg_1.a, cns2417));
        set(reg_165, cns2418, reg_96);
        reg_171 = cns2419;
        reg_173 = get(reg_4, cns2420);
        reg_174 = newStack();
        push(reg_174, reg_4);
        table_append(reg_165, reg_171, call(reg_173, reg_174));
        push(reg_164, reg_165);
        reg_176 = call(reg_163, reg_164);
      }
      if ((goto_253 || false)) {
        goto_253 = false;
        reg_178 = get(reg_4, cns2421);
        reg_179 = newStack();
        push(reg_179, reg_4);
        push(reg_179, reg_124);
        reg_181 = first(call(reg_178, reg_179));
        reg_183 = get(reg_4, cns2422);
        reg_184 = newStack();
        push(reg_184, reg_4);
        reg_185 = newTable();
        reg_186 = cns2423;
        set(reg_185, reg_186, get(reg_1.a, cns2424));
        set(reg_185, cns2425, reg_96);
        set(reg_185, cns2426, reg_181);
        push(reg_184, reg_185);
        reg_192 = call(reg_183, reg_184);
      }
    }
  }
  reg_193 = newStack();
  reg_195 = get(reg_4, cns2427);
  reg_196 = newStack();
  push(reg_196, reg_4);
  reg_197 = newTable();
  reg_198 = cns2428;
  set(reg_197, reg_198, get(reg_1.a, cns2429));
  set(reg_197, cns2430, reg_62);
  set(reg_197, cns2431, reg_96);
  reg_204 = cns2432;
  set(reg_197, reg_204, get(reg_5, cns2433));
  push(reg_196, reg_197);
  append(reg_193, call(reg_195, reg_196));
  return reg_193;
}
var cns2434 = anyStr("Function")
var cns2435 = anyStr("compile_bool")
var cns2436 = anyStr("compileExpr")
var cns2437 = anyStr("au_type")
var cns2438 = anyStr("inst")
var cns2439 = parseNum("1")
var cns2440 = anyStr("bool_f")
var cns2441 = parseNum("2")
var cns2442 = anyStr("au_type")
var cns2443 = anyStr("value")
var cns2444 = anyStr("type_id")
var cns2445 = anyStr("bool_t")
var cns2446 = anyStr("id")
var cns2447 = anyStr("au_type")
var cns2448 = anyStr("value")
var cns2449 = anyStr("type_id")
var cns2450 = anyStr("bool_t")
var cns2451 = anyStr("id")
var cns2452 = anyStr("err")
var cns2453 = anyStr("cannot use a auro expression as value")
function Function_compile_bool (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_11, reg_17, reg_18, reg_19, reg_20, reg_27, reg_34, reg_35, reg_39, reg_42, reg_50, reg_53, reg_54, reg_57;
  var goto_47=false, goto_68=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = get(reg_4, cns2436);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, reg_5);
  push(reg_8, lua$true());
  reg_11 = first(call(reg_7, reg_8));
  goto_47 = !tobool(not(get(reg_11, cns2437)));
  if (!goto_47) {
    reg_17 = get(reg_4, cns2438);
    reg_18 = newStack();
    push(reg_18, reg_4);
    reg_19 = newTable();
    reg_20 = cns2439;
    set(reg_19, reg_20, get(reg_1.a, cns2440));
    set(reg_19, cns2441, reg_11);
    set(reg_19, cns2442, cns2443);
    reg_27 = cns2444;
    set(reg_19, reg_27, get(get(reg_1.a, cns2445), cns2446));
    push(reg_18, reg_19);
    reg_34 = first(call(reg_17, reg_18));
    reg_35 = newStack();
    push(reg_35, reg_34);
    return reg_35;
  }
  if ((goto_47 || false)) {
    goto_47 = false;
    reg_39 = eq(get(reg_11, cns2447), cns2448);
    if (tobool(reg_39)) {
      reg_42 = get(reg_11, cns2449);
      reg_39 = eq(reg_42, get(get(reg_1.a, cns2450), cns2451));
    }
    goto_68 = !tobool(reg_39);
    if (!goto_68) {
      reg_50 = newStack();
      push(reg_50, reg_11);
      return reg_50;
    }
    if ((goto_68 || false)) {
      goto_68 = false;
      reg_53 = get(reg_1.a, cns2452);
      reg_54 = newStack();
      push(reg_54, cns2453);
      push(reg_54, reg_5);
      reg_56 = call(reg_53, reg_54);
    }
  }
  reg_57 = newStack();
  return reg_57;
}
var cns2454 = anyStr("Function")
var cns2455 = anyStr("compileExpr")
var cns2456 = anyStr("type")
var cns2457 = anyStr("const")
var cns2458 = anyStr("value")
var cns2459 = anyStr("nil")
var cns2460 = anyStr("nil_f")
var cns2461 = anyStr("value")
var cns2462 = anyStr("true")
var cns2463 = anyStr("true_f")
var cns2464 = anyStr("value")
var cns2465 = anyStr("false")
var cns2466 = anyStr("false_f")
var cns2467 = anyStr("inst")
var cns2468 = parseNum("1")
var cns2469 = anyStr("num")
var cns2470 = anyStr("constant")
var cns2471 = anyStr("value")
var cns2472 = anyStr("number")
var cns2473 = anyStr("inst")
var cns2474 = parseNum("1")
var cns2475 = anyStr("str")
var cns2476 = anyStr("constant")
var cns2477 = anyStr("value")
var cns2478 = anyStr("inst")
var cns2479 = parseNum("1")
var cns2480 = anyStr("vararg")
var cns2481 = anyStr("inst")
var cns2482 = parseNum("1")
var cns2483 = anyStr("first_f")
var cns2484 = parseNum("2")
var cns2485 = anyStr("get_vararg")
var cns2486 = anyStr("var")
var cns2487 = anyStr("get_local")
var cns2488 = anyStr("name")
var cns2489 = anyStr("au_type")
var cns2490 = anyStr("err")
var cns2491 = anyStr("cannot use a auro expression as value")
var cns2492 = anyStr("au_type")
var cns2493 = anyStr("value")
var cns2494 = anyStr("inst")
var cns2495 = parseNum("1")
var cns2496 = anyStr("var")
var cns2497 = parseNum("2")
var cns2498 = anyStr("au_type")
var cns2499 = anyStr("au_type")
var cns2500 = anyStr("type_id")
var cns2501 = anyStr("type_id")
var cns2502 = anyStr("get_local")
var cns2503 = anyStr("_ENV")
var cns2504 = anyStr("err")
var cns2505 = anyStr("local \"_ENV\" not in sight")
var cns2506 = anyStr("inst")
var cns2507 = parseNum("1")
var cns2508 = anyStr("var")
var cns2509 = parseNum("2")
var cns2510 = anyStr("compileExpr")
var cns2511 = anyStr("type")
var cns2512 = anyStr("str")
var cns2513 = anyStr("value")
var cns2514 = anyStr("name")
var cns2515 = anyStr("call")
var cns2516 = anyStr("get_f")
var cns2517 = anyStr("unop")
var cns2518 = anyStr("unops")
var cns2519 = anyStr("op")
var cns2520 = anyStr("compileExpr")
var cns2521 = anyStr("value")
var cns2522 = anyStr("inst")
var cns2523 = parseNum("1")
var cns2524 = parseNum("2")
var cns2525 = anyStr("line")
var cns2526 = anyStr("line")
var cns2527 = anyStr("binop")
var cns2528 = anyStr("op")
var cns2529 = anyStr("and")
var cns2530 = anyStr("op")
var cns2531 = anyStr("or")
var cns2532 = anyStr("lbl")
var cns2533 = anyStr("compileExpr")
var cns2534 = anyStr("left")
var cns2535 = anyStr("inst")
var cns2536 = parseNum("1")
var cns2537 = anyStr("local")
var cns2538 = parseNum("2")
var cns2539 = anyStr("op")
var cns2540 = anyStr("or")
var cns2541 = anyStr("inst")
var cns2542 = parseNum("1")
var cns2543 = anyStr("jif")
var cns2544 = parseNum("2")
var cns2545 = parseNum("3")
var cns2546 = anyStr("inst")
var cns2547 = parseNum("1")
var cns2548 = anyStr("nif")
var cns2549 = parseNum("2")
var cns2550 = parseNum("3")
var cns2551 = anyStr("compileExpr")
var cns2552 = anyStr("right")
var cns2553 = anyStr("inst")
var cns2554 = parseNum("1")
var cns2555 = anyStr("set")
var cns2556 = parseNum("2")
var cns2557 = parseNum("3")
var cns2558 = anyStr("inst")
var cns2559 = parseNum("1")
var cns2560 = anyStr("label")
var cns2561 = parseNum("2")
var cns2562 = anyStr("binops")
var cns2563 = anyStr("op")
var cns2564 = anyStr("compileExpr")
var cns2565 = anyStr("left")
var cns2566 = anyStr("compileExpr")
var cns2567 = anyStr("right")
var cns2568 = anyStr("inst")
var cns2569 = parseNum("1")
var cns2570 = parseNum("2")
var cns2571 = parseNum("3")
var cns2572 = anyStr("line")
var cns2573 = anyStr("line")
var cns2574 = anyStr("index")
var cns2575 = anyStr("compileExpr")
var cns2576 = anyStr("base")
var cns2577 = anyStr("compileExpr")
var cns2578 = anyStr("key")
var cns2579 = anyStr("call")
var cns2580 = anyStr("get_f")
var cns2581 = anyStr("field")
var cns2582 = anyStr("type")
var cns2583 = anyStr("str")
var cns2584 = anyStr("value")
var cns2585 = anyStr("key")
var cns2586 = anyStr("compileExpr")
var cns2587 = anyStr("type")
var cns2588 = anyStr("index")
var cns2589 = anyStr("base")
var cns2590 = anyStr("base")
var cns2591 = anyStr("key")
var cns2592 = anyStr("line")
var cns2593 = anyStr("line")
var cns2594 = anyStr("function")
var cns2595 = anyStr("createFunction")
var cns2596 = anyStr("constructor")
var cns2597 = anyStr("call")
var cns2598 = anyStr("table_f")
var cns2599 = parseNum("1")
var cns2600 = anyStr("ipairs")
var cns2601 = anyStr("items")
var cns2602 = anyStr("type")
var cns2603 = anyStr("indexitem")
var cns2604 = anyStr("compileExpr")
var cns2605 = anyStr("key")
var cns2606 = anyStr("type")
var cns2607 = anyStr("fielditem")
var cns2608 = anyStr("compileExpr")
var cns2609 = anyStr("type")
var cns2610 = anyStr("str")
var cns2611 = anyStr("value")
var cns2612 = anyStr("key")
var cns2613 = anyStr("type")
var cns2614 = anyStr("item")
var cns2615 = anyStr("compileExpr")
var cns2616 = anyStr("type")
var cns2617 = anyStr("num")
var cns2618 = anyStr("value")
var cns2619 = parseNum("1")
var cns2620 = anyStr("items")
var cns2621 = anyStr("value")
var cns2622 = anyStr("type")
var cns2623 = anyStr("call")
var cns2624 = anyStr("compile_call")
var cns2625 = anyStr("value")
var cns2626 = anyStr("value")
var cns2627 = anyStr("type")
var cns2628 = anyStr("vararg")
var cns2629 = anyStr("inst")
var cns2630 = parseNum("1")
var cns2631 = anyStr("copystack_f")
var cns2632 = parseNum("2")
var cns2633 = anyStr("get_vararg")
var cns2634 = anyStr("inst")
var cns2635 = parseNum("1")
var cns2636 = anyStr("table_append_f")
var cns2637 = parseNum("2")
var cns2638 = parseNum("3")
var cns2639 = parseNum("4")
var cns2640 = anyStr("compileExpr")
var cns2641 = anyStr("value")
var cns2642 = anyStr("inst")
var cns2643 = parseNum("1")
var cns2644 = anyStr("set_f")
var cns2645 = parseNum("2")
var cns2646 = parseNum("3")
var cns2647 = parseNum("4")
var cns2648 = anyStr("call")
var cns2649 = anyStr("compile_call")
var cns2650 = anyStr("regs")
var cns2651 = anyStr("regs")
var cns2652 = parseNum("0")
var cns2653 = anyStr("regs")
var cns2654 = parseNum("1")
var cns2655 = anyStr("au_type")
var cns2656 = anyStr("inst")
var cns2657 = parseNum("1")
var cns2658 = anyStr("first_f")
var cns2659 = parseNum("2")
var cns2660 = anyStr("err")
var cns2661 = anyStr("expression ")
var cns2662 = anyStr(" not supported")
function Function_compileExpr (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_8, reg_12, reg_37, reg_39, reg_40, reg_41, reg_49, reg_50, reg_55, reg_56, reg_58, reg_59, reg_60, reg_68, reg_69, reg_73, reg_74, reg_76, reg_77, reg_78, reg_84, reg_86, reg_87, reg_88, reg_89, reg_93, reg_95, reg_96, reg_103, reg_104, reg_108, reg_117, reg_118, reg_126, reg_127, reg_129, reg_130, reg_131, reg_135, reg_138, reg_143, reg_144, reg_147, reg_152, reg_153, reg_157, reg_158, reg_159, reg_164, reg_166, reg_167, reg_168, reg_171, reg_175, reg_176, reg_178, reg_179, reg_189, reg_192, reg_194, reg_195, reg_199, reg_200, reg_202, reg_203, reg_204, reg_207, reg_217, reg_225, reg_226, reg_228, reg_230, reg_231, reg_235, reg_237, reg_238, reg_239, reg_244, reg_251, reg_252, reg_253, reg_260, reg_261, reg_262, reg_269, reg_270, reg_274, reg_276, reg_277, reg_278, reg_285, reg_286, reg_287, reg_292, reg_295, reg_298, reg_300, reg_301, reg_305, reg_307, reg_308, reg_312, reg_313, reg_315, reg_316, reg_317, reg_321, reg_329, reg_330, reg_334, reg_336, reg_337, reg_341, reg_342, reg_344, reg_345, reg_353, reg_356, reg_359, reg_361, reg_362, reg_363, reg_366, reg_370, reg_377, reg_379, reg_380, reg_386, reg_387, reg_392, reg_393, reg_396, reg_397, reg_400, reg_401, reg_402, reg_403, reg_404, reg_405, reg_406, reg_407, reg_411, reg_418, reg_419, reg_430, reg_431, reg_432, reg_435, reg_446, reg_447, reg_448, reg_461, reg_470, reg_471, reg_484, reg_485, reg_486, reg_487, reg_491, reg_493, reg_494, reg_500, reg_501, reg_502, reg_503, reg_512, reg_513, reg_517, reg_519, reg_520, reg_521, reg_522, reg_530, reg_535, reg_536, reg_538, reg_540, reg_548, reg_556, reg_557, reg_559, reg_560, reg_561, reg_562, reg_570, reg_571, reg_572, reg_577;
  var goto_23=false, goto_34=false, goto_58=false, goto_86=false, goto_112=false, goto_139=false, goto_205=false, goto_268=false, goto_306=false, goto_372=false, goto_425=false, goto_470=false, goto_507=false, goto_543=false, goto_557=false, goto_612=false, goto_635=false, goto_684=false, goto_770=false, goto_828=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_8 = get(reg_5, cns2456);
  goto_58 = !tobool(eq(reg_8, cns2457));
  if (!goto_58) {
    reg_12 = nil();
    goto_23 = !tobool(eq(get(reg_5, cns2458), cns2459));
    if (!goto_23) {
      reg_12 = get(reg_1.a, cns2460);
    }
    if ((goto_23 || false)) {
      goto_23 = false;
      goto_34 = !tobool(eq(get(reg_5, cns2461), cns2462));
      if (!goto_34) {
        reg_12 = get(reg_1.a, cns2463);
      }
      if ((goto_34 || false)) {
        goto_34 = false;
        if (tobool(eq(get(reg_5, cns2464), cns2465))) {
          reg_12 = get(reg_1.a, cns2466);
        }
      }
    }
    reg_37 = newStack();
    reg_39 = get(reg_4, cns2467);
    reg_40 = newStack();
    push(reg_40, reg_4);
    reg_41 = newTable();
    set(reg_41, cns2468, reg_12);
    push(reg_40, reg_41);
    append(reg_37, call(reg_39, reg_40));
    return reg_37;
  }
  if ((goto_58 || false)) {
    goto_58 = false;
    goto_86 = !tobool(eq(reg_8, cns2469));
    if (!goto_86) {
      reg_49 = get(reg_1.a, cns2470);
      reg_50 = newStack();
      push(reg_50, get(reg_5, cns2471));
      push(reg_50, cns2472);
      reg_55 = first(call(reg_49, reg_50));
      reg_56 = newStack();
      reg_58 = get(reg_4, cns2473);
      reg_59 = newStack();
      push(reg_59, reg_4);
      reg_60 = newTable();
      set(reg_60, cns2474, reg_55);
      push(reg_59, reg_60);
      append(reg_56, call(reg_58, reg_59));
      return reg_56;
    }
    if ((goto_86 || false)) {
      goto_86 = false;
      goto_112 = !tobool(eq(reg_8, cns2475));
      if (!goto_112) {
        reg_68 = get(reg_1.a, cns2476);
        reg_69 = newStack();
        push(reg_69, get(reg_5, cns2477));
        reg_73 = first(call(reg_68, reg_69));
        reg_74 = newStack();
        reg_76 = get(reg_4, cns2478);
        reg_77 = newStack();
        push(reg_77, reg_4);
        reg_78 = newTable();
        set(reg_78, cns2479, reg_73);
        push(reg_77, reg_78);
        append(reg_74, call(reg_76, reg_77));
        return reg_74;
      }
      if ((goto_112 || false)) {
        goto_112 = false;
        goto_139 = !tobool(eq(reg_8, cns2480));
        if (!goto_139) {
          reg_84 = newStack();
          reg_86 = get(reg_4, cns2481);
          reg_87 = newStack();
          push(reg_87, reg_4);
          reg_88 = newTable();
          reg_89 = cns2482;
          set(reg_88, reg_89, get(reg_1.a, cns2483));
          reg_93 = cns2484;
          reg_95 = get(reg_4, cns2485);
          reg_96 = newStack();
          push(reg_96, reg_4);
          table_append(reg_88, reg_93, call(reg_95, reg_96));
          push(reg_87, reg_88);
          append(reg_84, call(reg_86, reg_87));
          return reg_84;
        }
        if ((goto_139 || false)) {
          goto_139 = false;
          goto_268 = !tobool(eq(reg_8, cns2486));
          if (!goto_268) {
            reg_103 = get(reg_4, cns2487);
            reg_104 = newStack();
            push(reg_104, reg_4);
            push(reg_104, get(reg_5, cns2488));
            reg_108 = first(call(reg_103, reg_104));
            goto_205 = !tobool(reg_108);
            if (!goto_205) {
              if (tobool(get(reg_108, cns2489))) {
                if (tobool(not(reg_6))) {
                  reg_117 = get(reg_1.a, cns2490);
                  reg_118 = newStack();
                  push(reg_118, cns2491);
                  push(reg_118, reg_5);
                  reg_120 = call(reg_117, reg_118);
                }
                if (tobool(ne(get(reg_108, cns2492), cns2493))) {
                  reg_126 = newStack();
                  push(reg_126, reg_108);
                  return reg_126;
                }
              }
              reg_127 = newStack();
              reg_129 = get(reg_4, cns2494);
              reg_130 = newStack();
              push(reg_130, reg_4);
              reg_131 = newTable();
              set(reg_131, cns2495, cns2496);
              set(reg_131, cns2497, reg_108);
              reg_135 = cns2498;
              set(reg_131, reg_135, get(reg_108, cns2499));
              reg_138 = cns2500;
              set(reg_131, reg_138, get(reg_108, cns2501));
              push(reg_130, reg_131);
              append(reg_127, call(reg_129, reg_130));
              return reg_127;
            }
            if ((goto_205 || false)) {
              goto_205 = false;
              reg_143 = get(reg_4, cns2502);
              reg_144 = newStack();
              push(reg_144, reg_4);
              push(reg_144, cns2503);
              reg_147 = first(call(reg_143, reg_144));
              if (tobool(not(reg_147))) {
                reg_152 = get(reg_1.a, cns2504);
                reg_153 = newStack();
                push(reg_153, cns2505);
                push(reg_153, reg_5);
                reg_155 = call(reg_152, reg_153);
              }
              reg_157 = get(reg_4, cns2506);
              reg_158 = newStack();
              push(reg_158, reg_4);
              reg_159 = newTable();
              set(reg_159, cns2507, cns2508);
              set(reg_159, cns2509, reg_147);
              push(reg_158, reg_159);
              reg_164 = first(call(reg_157, reg_158));
              reg_166 = get(reg_4, cns2510);
              reg_167 = newStack();
              push(reg_167, reg_4);
              reg_168 = newTable();
              set(reg_168, cns2511, cns2512);
              reg_171 = cns2513;
              set(reg_168, reg_171, get(reg_5, cns2514));
              push(reg_167, reg_168);
              reg_175 = first(call(reg_166, reg_167));
              reg_176 = newStack();
              reg_178 = get(reg_4, cns2515);
              reg_179 = newStack();
              push(reg_179, reg_4);
              push(reg_179, get(reg_1.a, cns2516));
              push(reg_179, reg_164);
              push(reg_179, reg_175);
              append(reg_176, call(reg_178, reg_179));
              return reg_176;
            }
          }
          if ((goto_268 || false)) {
            goto_268 = false;
            goto_306 = !tobool(eq(reg_8, cns2517));
            if (!goto_306) {
              reg_189 = get(reg_1.a, cns2518);
              reg_192 = get(reg_189, get(reg_5, cns2519));
              reg_194 = get(reg_4, cns2520);
              reg_195 = newStack();
              push(reg_195, reg_4);
              push(reg_195, get(reg_5, cns2521));
              reg_199 = first(call(reg_194, reg_195));
              reg_200 = newStack();
              reg_202 = get(reg_4, cns2522);
              reg_203 = newStack();
              push(reg_203, reg_4);
              reg_204 = newTable();
              set(reg_204, cns2523, reg_192);
              set(reg_204, cns2524, reg_199);
              reg_207 = cns2525;
              set(reg_204, reg_207, get(reg_5, cns2526));
              push(reg_203, reg_204);
              append(reg_200, call(reg_202, reg_203));
              return reg_200;
            }
            if ((goto_306 || false)) {
              goto_306 = false;
              goto_470 = !tobool(eq(reg_8, cns2527));
              if (!goto_470) {
                reg_217 = eq(get(reg_5, cns2528), cns2529);
                if (!tobool(reg_217)) {
                  reg_217 = eq(get(reg_5, cns2530), cns2531);
                }
                goto_425 = !tobool(reg_217);
                if (!goto_425) {
                  reg_225 = get(reg_4, cns2532);
                  reg_226 = newStack();
                  push(reg_226, reg_4);
                  reg_228 = first(call(reg_225, reg_226));
                  reg_230 = get(reg_4, cns2533);
                  reg_231 = newStack();
                  push(reg_231, reg_4);
                  push(reg_231, get(reg_5, cns2534));
                  reg_235 = first(call(reg_230, reg_231));
                  reg_237 = get(reg_4, cns2535);
                  reg_238 = newStack();
                  push(reg_238, reg_4);
                  reg_239 = newTable();
                  set(reg_239, cns2536, cns2537);
                  set(reg_239, cns2538, reg_235);
                  push(reg_238, reg_239);
                  reg_244 = first(call(reg_237, reg_238));
                  goto_372 = !tobool(eq(get(reg_5, cns2539), cns2540));
                  if (!goto_372) {
                    reg_251 = get(reg_4, cns2541);
                    reg_252 = newStack();
                    push(reg_252, reg_4);
                    reg_253 = newTable();
                    set(reg_253, cns2542, cns2543);
                    set(reg_253, cns2544, reg_228);
                    set(reg_253, cns2545, reg_235);
                    push(reg_252, reg_253);
                    reg_258 = call(reg_251, reg_252);
                  }
                  if ((goto_372 || false)) {
                    goto_372 = false;
                    reg_260 = get(reg_4, cns2546);
                    reg_261 = newStack();
                    push(reg_261, reg_4);
                    reg_262 = newTable();
                    set(reg_262, cns2547, cns2548);
                    set(reg_262, cns2549, reg_228);
                    set(reg_262, cns2550, reg_235);
                    push(reg_261, reg_262);
                    reg_267 = call(reg_260, reg_261);
                  }
                  reg_269 = get(reg_4, cns2551);
                  reg_270 = newStack();
                  push(reg_270, reg_4);
                  push(reg_270, get(reg_5, cns2552));
                  reg_274 = first(call(reg_269, reg_270));
                  reg_276 = get(reg_4, cns2553);
                  reg_277 = newStack();
                  push(reg_277, reg_4);
                  reg_278 = newTable();
                  set(reg_278, cns2554, cns2555);
                  set(reg_278, cns2556, reg_244);
                  set(reg_278, cns2557, reg_274);
                  push(reg_277, reg_278);
                  reg_283 = call(reg_276, reg_277);
                  reg_285 = get(reg_4, cns2558);
                  reg_286 = newStack();
                  push(reg_286, reg_4);
                  reg_287 = newTable();
                  set(reg_287, cns2559, cns2560);
                  set(reg_287, cns2561, reg_228);
                  push(reg_286, reg_287);
                  reg_291 = call(reg_285, reg_286);
                  reg_292 = newStack();
                  push(reg_292, reg_244);
                  return reg_292;
                }
                if ((goto_425 || false)) {
                  goto_425 = false;
                  reg_295 = get(reg_1.a, cns2562);
                  reg_298 = get(reg_295, get(reg_5, cns2563));
                  reg_300 = get(reg_4, cns2564);
                  reg_301 = newStack();
                  push(reg_301, reg_4);
                  push(reg_301, get(reg_5, cns2565));
                  reg_305 = first(call(reg_300, reg_301));
                  reg_307 = get(reg_4, cns2566);
                  reg_308 = newStack();
                  push(reg_308, reg_4);
                  push(reg_308, get(reg_5, cns2567));
                  reg_312 = first(call(reg_307, reg_308));
                  reg_313 = newStack();
                  reg_315 = get(reg_4, cns2568);
                  reg_316 = newStack();
                  push(reg_316, reg_4);
                  reg_317 = newTable();
                  set(reg_317, cns2569, reg_298);
                  set(reg_317, cns2570, reg_305);
                  set(reg_317, cns2571, reg_312);
                  reg_321 = cns2572;
                  set(reg_317, reg_321, get(reg_5, cns2573));
                  push(reg_316, reg_317);
                  append(reg_313, call(reg_315, reg_316));
                  return reg_313;
                }
              }
              if ((goto_470 || false)) {
                goto_470 = false;
                goto_507 = !tobool(eq(reg_8, cns2574));
                if (!goto_507) {
                  reg_329 = get(reg_4, cns2575);
                  reg_330 = newStack();
                  push(reg_330, reg_4);
                  push(reg_330, get(reg_5, cns2576));
                  reg_334 = first(call(reg_329, reg_330));
                  reg_336 = get(reg_4, cns2577);
                  reg_337 = newStack();
                  push(reg_337, reg_4);
                  push(reg_337, get(reg_5, cns2578));
                  reg_341 = first(call(reg_336, reg_337));
                  reg_342 = newStack();
                  reg_344 = get(reg_4, cns2579);
                  reg_345 = newStack();
                  push(reg_345, reg_4);
                  push(reg_345, get(reg_1.a, cns2580));
                  push(reg_345, reg_334);
                  push(reg_345, reg_341);
                  append(reg_342, call(reg_344, reg_345));
                  return reg_342;
                }
                if ((goto_507 || false)) {
                  goto_507 = false;
                  goto_543 = !tobool(eq(reg_8, cns2581));
                  if (!goto_543) {
                    reg_353 = newTable();
                    set(reg_353, cns2582, cns2583);
                    reg_356 = cns2584;
                    set(reg_353, reg_356, get(reg_5, cns2585));
                    reg_359 = newStack();
                    reg_361 = get(reg_4, cns2586);
                    reg_362 = newStack();
                    push(reg_362, reg_4);
                    reg_363 = newTable();
                    set(reg_363, cns2587, cns2588);
                    reg_366 = cns2589;
                    set(reg_363, reg_366, get(reg_5, cns2590));
                    set(reg_363, cns2591, reg_353);
                    reg_370 = cns2592;
                    set(reg_363, reg_370, get(reg_5, cns2593));
                    push(reg_362, reg_363);
                    append(reg_359, call(reg_361, reg_362));
                    return reg_359;
                  }
                  if ((goto_543 || false)) {
                    goto_543 = false;
                    goto_557 = !tobool(eq(reg_8, cns2594));
                    if (!goto_557) {
                      reg_377 = newStack();
                      reg_379 = get(reg_4, cns2595);
                      reg_380 = newStack();
                      push(reg_380, reg_4);
                      push(reg_380, reg_5);
                      append(reg_377, call(reg_379, reg_380));
                      return reg_377;
                    }
                    if ((goto_557 || false)) {
                      goto_557 = false;
                      goto_770 = !tobool(eq(reg_8, cns2596));
                      if (!goto_770) {
                        reg_386 = get(reg_4, cns2597);
                        reg_387 = newStack();
                        push(reg_387, reg_4);
                        push(reg_387, get(reg_1.a, cns2598));
                        reg_392 = first(call(reg_386, reg_387));
                        reg_393 = cns2599;
                        reg_396 = get(reg_1.a, cns2600);
                        reg_397 = newStack();
                        push(reg_397, get(reg_5, cns2601));
                        reg_400 = call(reg_396, reg_397);
                        reg_401 = next(reg_400);
                        reg_402 = next(reg_400);
                        reg_403 = next(reg_400);
                        loop_1: while (true) {
                          reg_404 = newStack();
                          push(reg_404, reg_402);
                          push(reg_404, reg_403);
                          reg_405 = call(reg_401, reg_404);
                          reg_406 = next(reg_405);
                          reg_403 = reg_406;
                          reg_407 = next(reg_405);
                          if (tobool(eq(reg_403, nil()))) break loop_1;
                          reg_411 = nil();
                          goto_612 = !tobool(eq(get(reg_407, cns2602), cns2603));
                          if (!goto_612) {
                            reg_418 = get(reg_4, cns2604);
                            reg_419 = newStack();
                            push(reg_419, reg_4);
                            push(reg_419, get(reg_407, cns2605));
                            reg_411 = first(call(reg_418, reg_419));
                          }
                          if ((goto_612 || false)) {
                            goto_612 = false;
                            goto_635 = !tobool(eq(get(reg_407, cns2606), cns2607));
                            if (!goto_635) {
                              reg_430 = get(reg_4, cns2608);
                              reg_431 = newStack();
                              push(reg_431, reg_4);
                              reg_432 = newTable();
                              set(reg_432, cns2609, cns2610);
                              reg_435 = cns2611;
                              set(reg_432, reg_435, get(reg_407, cns2612));
                              push(reg_431, reg_432);
                              reg_411 = first(call(reg_430, reg_431));
                            }
                            if ((goto_635 || false)) {
                              goto_635 = false;
                              if (tobool(eq(get(reg_407, cns2613), cns2614))) {
                                reg_446 = get(reg_4, cns2615);
                                reg_447 = newStack();
                                push(reg_447, reg_4);
                                reg_448 = newTable();
                                set(reg_448, cns2616, cns2617);
                                set(reg_448, cns2618, reg_393);
                                push(reg_447, reg_448);
                                reg_411 = first(call(reg_446, reg_447));
                                reg_393 = add(reg_393, cns2619);
                                if (tobool(eq(reg_406, length(get(reg_5, cns2620))))) {
                                  reg_461 = nil();
                                  goto_684 = !tobool(eq(get(get(reg_407, cns2621), cns2622), cns2623));
                                  if (!goto_684) {
                                    reg_470 = get(reg_4, cns2624);
                                    reg_471 = newStack();
                                    push(reg_471, reg_4);
                                    push(reg_471, get(reg_407, cns2625));
                                    reg_461 = first(call(reg_470, reg_471));
                                  }
                                  if ((goto_684 || false)) {
                                    goto_684 = false;
                                    if (tobool(eq(get(get(reg_407, cns2626), cns2627), cns2628))) {
                                      reg_484 = get(reg_4, cns2629);
                                      reg_485 = newStack();
                                      push(reg_485, reg_4);
                                      reg_486 = newTable();
                                      reg_487 = cns2630;
                                      set(reg_486, reg_487, get(reg_1.a, cns2631));
                                      reg_491 = cns2632;
                                      reg_493 = get(reg_4, cns2633);
                                      reg_494 = newStack();
                                      push(reg_494, reg_4);
                                      table_append(reg_486, reg_491, call(reg_493, reg_494));
                                      push(reg_485, reg_486);
                                      reg_461 = first(call(reg_484, reg_485));
                                    }
                                  }
                                  if (tobool(reg_461)) {
                                    reg_500 = get(reg_4, cns2634);
                                    reg_501 = newStack();
                                    push(reg_501, reg_4);
                                    reg_502 = newTable();
                                    reg_503 = cns2635;
                                    set(reg_502, reg_503, get(reg_1.a, cns2636));
                                    set(reg_502, cns2637, reg_392);
                                    set(reg_502, cns2638, reg_411);
                                    set(reg_502, cns2639, reg_461);
                                    push(reg_501, reg_502);
                                    reg_510 = call(reg_500, reg_501);
                                    if (true) break loop_1;
                                  }
                                }
                              }
                            }
                          }
                          reg_512 = get(reg_4, cns2640);
                          reg_513 = newStack();
                          push(reg_513, reg_4);
                          push(reg_513, get(reg_407, cns2641));
                          reg_517 = first(call(reg_512, reg_513));
                          reg_519 = get(reg_4, cns2642);
                          reg_520 = newStack();
                          push(reg_520, reg_4);
                          reg_521 = newTable();
                          reg_522 = cns2643;
                          set(reg_521, reg_522, get(reg_1.a, cns2644));
                          set(reg_521, cns2645, reg_392);
                          set(reg_521, cns2646, reg_411);
                          set(reg_521, cns2647, reg_517);
                          push(reg_520, reg_521);
                          reg_529 = call(reg_519, reg_520);
                        }
                        reg_530 = newStack();
                        push(reg_530, reg_392);
                        return reg_530;
                      }
                      if ((goto_770 || false)) {
                        goto_770 = false;
                        goto_828 = !tobool(eq(reg_8, cns2648));
                        if (!goto_828) {
                          reg_535 = get(reg_4, cns2649);
                          reg_536 = newStack();
                          push(reg_536, reg_4);
                          push(reg_536, reg_5);
                          push(reg_536, reg_6);
                          reg_538 = first(call(reg_535, reg_536));
                          reg_540 = get(reg_538, cns2650);
                          if (tobool(reg_540)) {
                            reg_540 = gt(length(get(reg_538, cns2651)), cns2652);
                          }
                          if (tobool(reg_540)) {
                            reg_548 = newStack();
                            push(reg_548, get(get(reg_538, cns2653), cns2654));
                            return reg_548;
                          }
                          if (tobool(get(reg_538, cns2655))) {
                            reg_556 = newStack();
                            push(reg_556, reg_538);
                            return reg_556;
                          }
                          reg_557 = newStack();
                          reg_559 = get(reg_4, cns2656);
                          reg_560 = newStack();
                          push(reg_560, reg_4);
                          reg_561 = newTable();
                          reg_562 = cns2657;
                          set(reg_561, reg_562, get(reg_1.a, cns2658));
                          set(reg_561, cns2659, reg_538);
                          push(reg_560, reg_561);
                          append(reg_557, call(reg_559, reg_560));
                          return reg_557;
                        }
                        if ((goto_828 || false)) {
                          goto_828 = false;
                          reg_570 = get(reg_1.a, cns2660);
                          reg_571 = newStack();
                          reg_572 = cns2661;
                          push(reg_571, concat(reg_572, concat(reg_8, cns2662)));
                          push(reg_571, reg_5);
                          reg_576 = call(reg_570, reg_571);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  reg_577 = newStack();
  return reg_577;
}
var cns2663 = anyStr("Function")
var cns2664 = anyStr("assign")
var cns2665 = anyStr("math")
var cns2666 = anyStr("max")
var cns2667 = parseNum("1")
var cns2668 = parseNum("1")
var cns2669 = parseNum("0")
var cns2670 = anyStr("type")
var cns2671 = anyStr("call")
var cns2672 = anyStr("compile_call")
var cns2673 = anyStr("type")
var cns2674 = anyStr("vararg")
var cns2675 = anyStr("inst")
var cns2676 = parseNum("1")
var cns2677 = anyStr("copystack_f")
var cns2678 = parseNum("2")
var cns2679 = anyStr("get_vararg")
var cns2680 = anyStr("regs")
var cns2681 = anyStr("regs")
var cns2682 = parseNum("1")
var cns2683 = anyStr("call")
var cns2684 = anyStr("nil_f")
var cns2685 = anyStr("au_type")
var cns2686 = anyStr("call")
var cns2687 = anyStr("next_f")
var cns2688 = anyStr("repr")
var cns2689 = anyStr("repr")
var cns2690 = anyStr("repr")
var cns2691 = anyStr("lcl")
var cns2692 = anyStr("repr")
var cns2693 = anyStr("lcl")
var cns2694 = anyStr("lcl")
var cns2695 = anyStr("type")
var cns2696 = anyStr("string")
var cns2697 = anyStr("get_local")
var cns2698 = anyStr("au_type")
var cns2699 = anyStr("compileExpr")
var cns2700 = anyStr("call")
var cns2701 = anyStr("nil_f")
var cns2702 = anyStr("lcl")
var cns2703 = anyStr("au_type")
var cns2704 = anyStr("inst")
var cns2705 = parseNum("1")
var cns2706 = anyStr("local")
var cns2707 = parseNum("2")
var cns2708 = anyStr("scope")
var cns2709 = anyStr("locals")
var cns2710 = anyStr("lcl")
var cns2711 = anyStr("base")
var cns2712 = anyStr("au_type")
var cns2713 = anyStr("error")
var cns2714 = anyStr("attempt to assign a typed expression to a table field, at line ")
var cns2715 = anyStr("inst")
var cns2716 = parseNum("1")
var cns2717 = anyStr("set_f")
var cns2718 = parseNum("2")
var cns2719 = anyStr("base")
var cns2720 = parseNum("3")
var cns2721 = anyStr("key")
var cns2722 = parseNum("4")
var cns2723 = anyStr("get_local")
var cns2724 = anyStr("au_type")
var cns2725 = anyStr("au_type")
var cns2726 = anyStr("error")
var cns2727 = anyStr("attempt to assign a lua expression to a typed local, at line ")
var cns2728 = anyStr("type_id")
var cns2729 = anyStr("type_id")
var cns2730 = anyStr("types")
var cns2731 = anyStr("type_id")
var cns2732 = parseNum("1")
var cns2733 = anyStr("name")
var cns2734 = anyStr("types")
var cns2735 = anyStr("type_id")
var cns2736 = parseNum("1")
var cns2737 = anyStr("name")
var cns2738 = anyStr("error")
var cns2739 = anyStr("attempt to assign a ")
var cns2740 = anyStr(" to a ")
var cns2741 = anyStr(" local, at line ")
var cns2742 = anyStr("au_type")
var cns2743 = anyStr("error")
var cns2744 = anyStr("attempt to assign a typed expression to a lua local, at line ")
var cns2745 = anyStr("inst")
var cns2746 = parseNum("1")
var cns2747 = anyStr("set")
var cns2748 = parseNum("2")
var cns2749 = parseNum("3")
var cns2750 = anyStr("line")
var cns2751 = anyStr("au_type")
var cns2752 = anyStr("error")
var cns2753 = anyStr("attempt to assign a typed expression to a lua global, at line ")
var cns2754 = anyStr("get_local")
var cns2755 = anyStr("_ENV")
var cns2756 = anyStr("err")
var cns2757 = anyStr("local \"_ENV\" not in sight")
var cns2758 = anyStr("node")
var cns2759 = anyStr("inst")
var cns2760 = parseNum("1")
var cns2761 = anyStr("var")
var cns2762 = parseNum("2")
var cns2763 = anyStr("compileExpr")
var cns2764 = anyStr("type")
var cns2765 = anyStr("str")
var cns2766 = anyStr("value")
var cns2767 = anyStr("call")
var cns2768 = anyStr("set_f")
function Function_assign (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_12, reg_13, reg_17, reg_18, reg_19, reg_20, reg_21, reg_23, reg_29, reg_30, reg_31, reg_33, reg_44, reg_45, reg_55, reg_56, reg_57, reg_58, reg_62, reg_64, reg_65, reg_74, reg_83, reg_84, reg_95, reg_96, reg_106, reg_112, reg_115, reg_122, reg_123, reg_130, reg_131, reg_133, reg_134, reg_141, reg_142, reg_146, reg_147, reg_162, reg_163, reg_164, reg_173, reg_184, reg_185, reg_190, reg_191, reg_192, reg_193, reg_197, reg_200, reg_206, reg_207, reg_209, reg_220, reg_221, reg_226, reg_233, reg_240, reg_243, reg_250, reg_253, reg_254, reg_255, reg_256, reg_269, reg_270, reg_275, reg_276, reg_277, reg_289, reg_290, reg_295, reg_296, reg_299, reg_304, reg_305, reg_312, reg_313, reg_314, reg_319, reg_321, reg_322, reg_323, reg_328, reg_330, reg_331, reg_337;
  var goto_30=false, goto_62=false, goto_121=false, goto_129=false, goto_142=false, goto_153=false, goto_210=false, goto_255=false, goto_295=false, goto_322=false, goto_365=false, goto_395=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_12 = get(get(reg_1.a, cns2665), cns2666);
  reg_13 = newStack();
  push(reg_13, length(reg_5));
  push(reg_13, length(reg_6));
  reg_17 = first(call(reg_12, reg_13));
  reg_18 = nil();
  reg_19 = cns2667;
  reg_20 = reg_17;
  reg_21 = cns2668;
  reg_23 = lt(reg_21, cns2669);
  loop_1: while (true) {
    goto_30 = tobool(reg_23);
    if (!goto_30) {
      if (tobool(gt(reg_19, reg_20))) break loop_1;
    }
    if ((goto_30 || false)) {
      goto_30 = false;
      if (tobool(lt(reg_19, reg_20))) break loop_1;
    }
    reg_29 = get(reg_5, reg_19);
    reg_30 = get(reg_6, reg_19);
    reg_31 = nil();
    reg_33 = eq(reg_19, length(reg_6));
    if (tobool(reg_33)) {
      reg_33 = lt(reg_19, length(reg_5));
    }
    if (tobool(reg_33)) {
      goto_62 = !tobool(eq(get(reg_30, cns2670), cns2671));
      if (!goto_62) {
        reg_44 = get(reg_4, cns2672);
        reg_45 = newStack();
        push(reg_45, reg_4);
        push(reg_45, reg_30);
        push(reg_45, lua$true());
        reg_18 = first(call(reg_44, reg_45));
      }
      if ((goto_62 || false)) {
        goto_62 = false;
        if (tobool(eq(get(reg_30, cns2673), cns2674))) {
          reg_55 = get(reg_4, cns2675);
          reg_56 = newStack();
          push(reg_56, reg_4);
          reg_57 = newTable();
          reg_58 = cns2676;
          set(reg_57, reg_58, get(reg_1.a, cns2677));
          reg_62 = cns2678;
          reg_64 = get(reg_4, cns2679);
          reg_65 = newStack();
          push(reg_65, reg_4);
          table_append(reg_57, reg_62, call(reg_64, reg_65));
          push(reg_56, reg_57);
          reg_18 = first(call(reg_55, reg_56));
        }
      }
    }
    goto_142 = !tobool(reg_18);
    if (!goto_142) {
      goto_121 = !tobool(get(reg_18, cns2680));
      if (!goto_121) {
        reg_74 = get(reg_18, cns2681);
        reg_31 = get(reg_74, sub(add(reg_19, cns2682), length(reg_6)));
        if (tobool(not(reg_31))) {
          reg_83 = get(reg_4, cns2683);
          reg_84 = newStack();
          push(reg_84, reg_4);
          push(reg_84, get(reg_1.a, cns2684));
          reg_31 = first(call(reg_83, reg_84));
        }
      }
      if ((goto_121 || false)) {
        goto_121 = false;
        goto_129 = !tobool(get(reg_18, cns2685));
        if (!goto_129) {
          reg_31 = reg_18;
          reg_18 = nil();
        }
        if ((goto_129 || false)) {
          goto_129 = false;
          reg_95 = get(reg_4, cns2686);
          reg_96 = newStack();
          push(reg_96, reg_4);
          push(reg_96, get(reg_1.a, cns2687));
          push(reg_96, reg_18);
          reg_31 = first(call(reg_95, reg_96));
        }
      }
    }
    if ((goto_142 || false)) {
      goto_142 = false;
      goto_210 = !tobool(reg_30);
      if (!goto_210) {
        goto_153 = !tobool(get(reg_29, cns2688));
        if (!goto_153) {
          reg_106 = cns2689;
          set(reg_30, reg_106, get(reg_29, cns2690));
        }
        if ((goto_153 || false)) {
          goto_153 = false;
          if (tobool(get(reg_29, cns2691))) {
            reg_112 = cns2692;
            set(reg_30, reg_112, get(reg_29, cns2693));
          }
        }
        reg_115 = lua$false();
        if (tobool(get(reg_29, cns2694))) {
          reg_115 = lua$true();
        }
        reg_122 = get(reg_1.a, cns2695);
        reg_123 = newStack();
        push(reg_123, reg_29);
        if (tobool(eq(first(call(reg_122, reg_123)), cns2696))) {
          reg_130 = get(reg_4, cns2697);
          reg_131 = newStack();
          push(reg_131, reg_4);
          push(reg_131, reg_29);
          reg_133 = first(call(reg_130, reg_131));
          reg_134 = reg_133;
          if (tobool(reg_133)) {
            reg_134 = get(reg_133, cns2698);
          }
          if (tobool(reg_134)) {
            reg_115 = lua$true();
          }
        }
        reg_141 = get(reg_4, cns2699);
        reg_142 = newStack();
        push(reg_142, reg_4);
        push(reg_142, reg_30);
        push(reg_142, reg_115);
        reg_31 = first(call(reg_141, reg_142));
      }
      if ((goto_210 || false)) {
        goto_210 = false;
        reg_146 = get(reg_4, cns2700);
        reg_147 = newStack();
        push(reg_147, reg_4);
        push(reg_147, get(reg_1.a, cns2701));
        reg_31 = first(call(reg_146, reg_147));
      }
    }
    if (tobool(reg_29)) {
      goto_255 = !tobool(get(reg_29, cns2702));
      if (!goto_255) {
        if (tobool(not(get(reg_31, cns2703)))) {
          reg_162 = get(reg_4, cns2704);
          reg_163 = newStack();
          push(reg_163, reg_4);
          reg_164 = newTable();
          set(reg_164, cns2705, cns2706);
          set(reg_164, cns2707, reg_31);
          push(reg_163, reg_164);
          reg_31 = first(call(reg_162, reg_163));
        }
        reg_173 = get(get(reg_4, cns2708), cns2709);
        set(reg_173, get(reg_29, cns2710), reg_31);
      }
      if ((goto_255 || false)) {
        goto_255 = false;
        goto_295 = !tobool(get(reg_29, cns2711));
        if (!goto_295) {
          if (tobool(get(reg_31, cns2712))) {
            reg_184 = get(reg_1.a, cns2713);
            reg_185 = newStack();
            push(reg_185, concat(cns2714, reg_7));
            reg_188 = call(reg_184, reg_185);
          }
          reg_190 = get(reg_4, cns2715);
          reg_191 = newStack();
          push(reg_191, reg_4);
          reg_192 = newTable();
          reg_193 = cns2716;
          set(reg_192, reg_193, get(reg_1.a, cns2717));
          reg_197 = cns2718;
          set(reg_192, reg_197, get(reg_29, cns2719));
          reg_200 = cns2720;
          set(reg_192, reg_200, get(reg_29, cns2721));
          set(reg_192, cns2722, reg_31);
          push(reg_191, reg_192);
          reg_204 = call(reg_190, reg_191);
        }
        if ((goto_295 || false)) {
          goto_295 = false;
          reg_206 = get(reg_4, cns2723);
          reg_207 = newStack();
          push(reg_207, reg_4);
          push(reg_207, reg_29);
          reg_209 = first(call(reg_206, reg_207));
          goto_395 = !tobool(reg_209);
          if (!goto_395) {
            goto_365 = !tobool(get(reg_209, cns2724));
            if (!goto_365) {
              goto_322 = !tobool(not(get(reg_31, cns2725)));
              if (!goto_322) {
                reg_220 = get(reg_1.a, cns2726);
                reg_221 = newStack();
                push(reg_221, concat(cns2727, reg_7));
                reg_224 = call(reg_220, reg_221);
              }
              if ((goto_322 || false)) {
                goto_322 = false;
                reg_226 = get(reg_31, cns2728);
                if (tobool(ne(reg_226, get(reg_209, cns2729)))) {
                  reg_233 = get(reg_1.a, cns2730);
                  reg_240 = get(get(reg_233, add(get(reg_31, cns2731), cns2732)), cns2733);
                  reg_243 = get(reg_1.a, cns2734);
                  reg_250 = get(get(reg_243, add(get(reg_209, cns2735), cns2736)), cns2737);
                  reg_253 = get(reg_1.a, cns2738);
                  reg_254 = newStack();
                  reg_255 = cns2739;
                  reg_256 = cns2740;
                  push(reg_254, concat(reg_255, concat(reg_240, concat(reg_256, concat(reg_250, concat(cns2741, reg_7))))));
                  reg_263 = call(reg_253, reg_254);
                }
              }
            }
            if ((goto_365 || false)) {
              goto_365 = false;
              if (tobool(get(reg_31, cns2742))) {
                reg_269 = get(reg_1.a, cns2743);
                reg_270 = newStack();
                push(reg_270, concat(cns2744, reg_7));
                reg_273 = call(reg_269, reg_270);
              }
            }
            reg_275 = get(reg_4, cns2745);
            reg_276 = newStack();
            push(reg_276, reg_4);
            reg_277 = newTable();
            set(reg_277, cns2746, cns2747);
            set(reg_277, cns2748, reg_209);
            set(reg_277, cns2749, reg_31);
            set(reg_277, cns2750, reg_7);
            push(reg_276, reg_277);
            reg_283 = call(reg_275, reg_276);
          }
          if ((goto_395 || false)) {
            goto_395 = false;
            if (tobool(get(reg_31, cns2751))) {
              reg_289 = get(reg_1.a, cns2752);
              reg_290 = newStack();
              push(reg_290, concat(cns2753, reg_7));
              reg_293 = call(reg_289, reg_290);
            }
            reg_295 = get(reg_4, cns2754);
            reg_296 = newStack();
            push(reg_296, reg_4);
            push(reg_296, cns2755);
            reg_299 = first(call(reg_295, reg_296));
            if (tobool(not(reg_299))) {
              reg_304 = get(reg_1.a, cns2756);
              reg_305 = newStack();
              push(reg_305, cns2757);
              push(reg_305, get(reg_1.a, cns2758));
              reg_310 = call(reg_304, reg_305);
            }
            reg_312 = get(reg_4, cns2759);
            reg_313 = newStack();
            push(reg_313, reg_4);
            reg_314 = newTable();
            set(reg_314, cns2760, cns2761);
            set(reg_314, cns2762, reg_299);
            push(reg_313, reg_314);
            reg_319 = first(call(reg_312, reg_313));
            reg_321 = get(reg_4, cns2763);
            reg_322 = newStack();
            push(reg_322, reg_4);
            reg_323 = newTable();
            set(reg_323, cns2764, cns2765);
            set(reg_323, cns2766, reg_29);
            push(reg_322, reg_323);
            reg_328 = first(call(reg_321, reg_322));
            reg_330 = get(reg_4, cns2767);
            reg_331 = newStack();
            push(reg_331, reg_4);
            push(reg_331, get(reg_1.a, cns2768));
            push(reg_331, reg_319);
            push(reg_331, reg_328);
            push(reg_331, reg_31);
            reg_335 = call(reg_330, reg_331);
          }
        }
      }
    }
    reg_19 = add(reg_19, reg_21);
  }
  reg_337 = newStack();
  return reg_337;
}
var cns2769 = anyStr("Function")
var cns2770 = anyStr("compileLhs")
var cns2771 = anyStr("type")
var cns2772 = anyStr("var")
var cns2773 = anyStr("name")
var cns2774 = anyStr("type")
var cns2775 = anyStr("index")
var cns2776 = anyStr("base")
var cns2777 = anyStr("compileExpr")
var cns2778 = anyStr("base")
var cns2779 = anyStr("key")
var cns2780 = anyStr("compileExpr")
var cns2781 = anyStr("key")
var cns2782 = anyStr("repr")
var cns2783 = anyStr("getRepr")
var cns2784 = anyStr("type")
var cns2785 = anyStr("field")
var cns2786 = anyStr("base")
var cns2787 = anyStr("compileExpr")
var cns2788 = anyStr("base")
var cns2789 = anyStr("key")
var cns2790 = anyStr("compileExpr")
var cns2791 = anyStr("type")
var cns2792 = anyStr("str")
var cns2793 = anyStr("value")
var cns2794 = anyStr("key")
var cns2795 = anyStr("repr")
var cns2796 = anyStr("getRepr")
var cns2797 = anyStr("error")
var cns2798 = anyStr("wtf")
function Function_compileLhs (reg_0, reg_1) {
  var reg_4, reg_5, reg_11, reg_19, reg_20, reg_21, reg_23, reg_24, reg_29, reg_31, reg_32, reg_37, reg_40, reg_41, reg_49, reg_50, reg_51, reg_53, reg_54, reg_59, reg_61, reg_62, reg_63, reg_66, reg_71, reg_74, reg_75, reg_80, reg_81, reg_84;
  var goto_16=false, goto_58=false, goto_106=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  goto_16 = !tobool(eq(get(reg_5, cns2771), cns2772));
  if (!goto_16) {
    reg_11 = newStack();
    push(reg_11, get(reg_5, cns2773));
    return reg_11;
  }
  if ((goto_16 || false)) {
    goto_16 = false;
    goto_58 = !tobool(eq(get(reg_5, cns2774), cns2775));
    if (!goto_58) {
      reg_19 = newStack();
      reg_20 = newTable();
      reg_21 = cns2776;
      reg_23 = get(reg_4, cns2777);
      reg_24 = newStack();
      push(reg_24, reg_4);
      push(reg_24, get(reg_5, cns2778));
      set(reg_20, reg_21, first(call(reg_23, reg_24)));
      reg_29 = cns2779;
      reg_31 = get(reg_4, cns2780);
      reg_32 = newStack();
      push(reg_32, reg_4);
      push(reg_32, get(reg_5, cns2781));
      set(reg_20, reg_29, first(call(reg_31, reg_32)));
      reg_37 = cns2782;
      reg_40 = get(reg_1.a, cns2783);
      reg_41 = newStack();
      push(reg_41, reg_5);
      set(reg_20, reg_37, first(call(reg_40, reg_41)));
      push(reg_19, reg_20);
      return reg_19;
    }
    if ((goto_58 || false)) {
      goto_58 = false;
      goto_106 = !tobool(eq(get(reg_5, cns2784), cns2785));
      if (!goto_106) {
        reg_49 = newStack();
        reg_50 = newTable();
        reg_51 = cns2786;
        reg_53 = get(reg_4, cns2787);
        reg_54 = newStack();
        push(reg_54, reg_4);
        push(reg_54, get(reg_5, cns2788));
        set(reg_50, reg_51, first(call(reg_53, reg_54)));
        reg_59 = cns2789;
        reg_61 = get(reg_4, cns2790);
        reg_62 = newStack();
        push(reg_62, reg_4);
        reg_63 = newTable();
        set(reg_63, cns2791, cns2792);
        reg_66 = cns2793;
        set(reg_63, reg_66, get(reg_5, cns2794));
        push(reg_62, reg_63);
        set(reg_50, reg_59, first(call(reg_61, reg_62)));
        reg_71 = cns2795;
        reg_74 = get(reg_1.a, cns2796);
        reg_75 = newStack();
        push(reg_75, reg_5);
        set(reg_50, reg_71, first(call(reg_74, reg_75)));
        push(reg_49, reg_50);
        return reg_49;
      }
      if ((goto_106 || false)) {
        goto_106 = false;
        reg_80 = get(reg_1.a, cns2797);
        reg_81 = newStack();
        push(reg_81, cns2798);
        reg_83 = call(reg_80, reg_81);
      }
    }
  }
  reg_84 = newStack();
  return reg_84;
}
var cns2799 = anyStr("type")
var cns2800 = anyStr("var")
var cns2801 = anyStr("name")
var cns2802 = anyStr("type")
var cns2803 = anyStr("field")
var cns2804 = anyStr("getRepr")
var cns2805 = anyStr("base")
var cns2806 = anyStr(".")
var cns2807 = anyStr("key")
var cns2808 = anyStr("type")
var cns2809 = anyStr("index")
var cns2810 = anyStr("key")
var cns2811 = anyStr("type")
var cns2812 = anyStr("str")
var cns2813 = anyStr("getRepr")
var cns2814 = anyStr("base")
var cns2815 = anyStr(".")
var cns2816 = anyStr("key")
var cns2817 = anyStr("value")
var cns2818 = anyStr("getRepr")
var cns2819 = anyStr("key")
var cns2820 = anyStr("getRepr")
var cns2821 = anyStr("base")
var cns2822 = anyStr("[")
var cns2823 = anyStr("]")
var cns2824 = anyStr("type")
var cns2825 = anyStr("num")
var cns2826 = anyStr("type")
var cns2827 = anyStr("const")
var cns2828 = anyStr("value")
var cns2829 = anyStr("type")
var cns2830 = anyStr("string")
var cns2831 = anyStr("\"")
var cns2832 = anyStr("value")
var cns2833 = anyStr("\"")
function aulua_codegen$function (reg_0, reg_1) {
  var reg_4, reg_10, reg_18, reg_21, reg_22, reg_26, reg_27, reg_44, reg_47, reg_48, reg_52, reg_53, reg_62, reg_63, reg_67, reg_69, reg_72, reg_73, reg_77, reg_78, reg_86, reg_93, reg_101, reg_102, reg_108;
  var goto_15=false, goto_39=false, goto_73=false, goto_103=false, goto_122=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  goto_15 = !tobool(eq(get(reg_4, cns2799), cns2800));
  if (!goto_15) {
    reg_10 = newStack();
    push(reg_10, get(reg_4, cns2801));
    return reg_10;
  }
  if ((goto_15 || false)) {
    goto_15 = false;
    goto_39 = !tobool(eq(get(reg_4, cns2802), cns2803));
    if (!goto_39) {
      reg_18 = newStack();
      reg_21 = get(reg_1.a, cns2804);
      reg_22 = newStack();
      push(reg_22, get(reg_4, cns2805));
      reg_26 = first(call(reg_21, reg_22));
      reg_27 = cns2806;
      push(reg_18, concat(reg_26, concat(reg_27, get(reg_4, cns2807))));
      return reg_18;
    }
    if ((goto_39 || false)) {
      goto_39 = false;
      goto_103 = !tobool(eq(get(reg_4, cns2808), cns2809));
      if (!goto_103) {
        goto_73 = !tobool(eq(get(get(reg_4, cns2810), cns2811), cns2812));
        if (!goto_73) {
          reg_44 = newStack();
          reg_47 = get(reg_1.a, cns2813);
          reg_48 = newStack();
          push(reg_48, get(reg_4, cns2814));
          reg_52 = first(call(reg_47, reg_48));
          reg_53 = cns2815;
          push(reg_44, concat(reg_52, concat(reg_53, get(get(reg_4, cns2816), cns2817))));
          return reg_44;
        }
        if ((goto_73 || false)) {
          goto_73 = false;
          reg_62 = get(reg_1.a, cns2818);
          reg_63 = newStack();
          push(reg_63, get(reg_4, cns2819));
          reg_67 = first(call(reg_62, reg_63));
          if (tobool(reg_67)) {
            reg_69 = newStack();
            reg_72 = get(reg_1.a, cns2820);
            reg_73 = newStack();
            push(reg_73, get(reg_4, cns2821));
            reg_77 = first(call(reg_72, reg_73));
            reg_78 = cns2822;
            push(reg_69, concat(reg_77, concat(reg_78, concat(reg_67, cns2823))));
            return reg_69;
          }
        }
      }
      if ((goto_103 || false)) {
        goto_103 = false;
        reg_86 = eq(get(reg_4, cns2824), cns2825);
        if (!tobool(reg_86)) {
          reg_86 = eq(get(reg_4, cns2826), cns2827);
        }
        goto_122 = !tobool(reg_86);
        if (!goto_122) {
          reg_93 = newStack();
          push(reg_93, get(reg_4, cns2828));
          return reg_93;
        }
        if ((goto_122 || false)) {
          goto_122 = false;
          if (tobool(eq(get(reg_4, cns2829), cns2830))) {
            reg_101 = newStack();
            reg_102 = cns2831;
            push(reg_101, concat(reg_102, concat(get(reg_4, cns2832), cns2833)));
            return reg_101;
          }
        }
      }
    }
  }
  reg_108 = newStack();
  return reg_108;
}
var cns2834 = anyStr("getRepr")
var cns2835 = anyStr("Function")
var cns2836 = anyStr("compile_numfor")
var cns2837 = anyStr("lbl")
var cns2838 = anyStr("lbl")
var cns2839 = anyStr("lbl")
var cns2840 = anyStr("table")
var cns2841 = anyStr("insert")
var cns2842 = anyStr("loops")
var cns2843 = anyStr("push_scope")
var cns2844 = anyStr("inst")
var cns2845 = parseNum("1")
var cns2846 = anyStr("local")
var cns2847 = parseNum("2")
var cns2848 = anyStr("compileExpr")
var cns2849 = anyStr("init")
var cns2850 = anyStr("inst")
var cns2851 = parseNum("1")
var cns2852 = anyStr("local")
var cns2853 = parseNum("2")
var cns2854 = anyStr("compileExpr")
var cns2855 = anyStr("limit")
var cns2856 = anyStr("step")
var cns2857 = anyStr("compileExpr")
var cns2858 = anyStr("step")
var cns2859 = anyStr("compileExpr")
var cns2860 = anyStr("type")
var cns2861 = anyStr("num")
var cns2862 = anyStr("value")
var cns2863 = anyStr("1")
var cns2864 = anyStr("inst")
var cns2865 = parseNum("1")
var cns2866 = anyStr("local")
var cns2867 = parseNum("2")
var cns2868 = anyStr("scope")
var cns2869 = anyStr("locals")
var cns2870 = anyStr("name")
var cns2871 = anyStr("inst")
var cns2872 = parseNum("1")
var cns2873 = anyStr("binops")
var cns2874 = anyStr("<")
var cns2875 = parseNum("2")
var cns2876 = parseNum("3")
var cns2877 = anyStr("compileExpr")
var cns2878 = anyStr("type")
var cns2879 = anyStr("num")
var cns2880 = anyStr("value")
var cns2881 = anyStr("0")
var cns2882 = anyStr("inst")
var cns2883 = parseNum("1")
var cns2884 = anyStr("label")
var cns2885 = parseNum("2")
var cns2886 = anyStr("lbl")
var cns2887 = anyStr("inst")
var cns2888 = parseNum("1")
var cns2889 = anyStr("jif")
var cns2890 = parseNum("2")
var cns2891 = parseNum("3")
var cns2892 = anyStr("inst")
var cns2893 = parseNum("1")
var cns2894 = anyStr("binops")
var cns2895 = anyStr(">")
var cns2896 = parseNum("2")
var cns2897 = parseNum("3")
var cns2898 = anyStr("inst")
var cns2899 = parseNum("1")
var cns2900 = anyStr("jif")
var cns2901 = parseNum("2")
var cns2902 = parseNum("3")
var cns2903 = anyStr("inst")
var cns2904 = parseNum("1")
var cns2905 = anyStr("jmp")
var cns2906 = parseNum("2")
var cns2907 = anyStr("inst")
var cns2908 = parseNum("1")
var cns2909 = anyStr("label")
var cns2910 = parseNum("2")
var cns2911 = anyStr("inst")
var cns2912 = parseNum("1")
var cns2913 = anyStr("binops")
var cns2914 = anyStr("<")
var cns2915 = parseNum("2")
var cns2916 = parseNum("3")
var cns2917 = anyStr("inst")
var cns2918 = parseNum("1")
var cns2919 = anyStr("jif")
var cns2920 = parseNum("2")
var cns2921 = parseNum("3")
var cns2922 = anyStr("inst")
var cns2923 = parseNum("1")
var cns2924 = anyStr("label")
var cns2925 = parseNum("2")
var cns2926 = anyStr("compileBlock")
var cns2927 = anyStr("body")
var cns2928 = anyStr("inst")
var cns2929 = parseNum("1")
var cns2930 = anyStr("binops")
var cns2931 = anyStr("+")
var cns2932 = parseNum("2")
var cns2933 = parseNum("3")
var cns2934 = anyStr("inst")
var cns2935 = parseNum("1")
var cns2936 = anyStr("set")
var cns2937 = parseNum("2")
var cns2938 = parseNum("3")
var cns2939 = anyStr("inst")
var cns2940 = parseNum("1")
var cns2941 = anyStr("jmp")
var cns2942 = parseNum("2")
var cns2943 = anyStr("inst")
var cns2944 = parseNum("1")
var cns2945 = anyStr("label")
var cns2946 = parseNum("2")
var cns2947 = anyStr("pop_scope")
var cns2948 = anyStr("table")
var cns2949 = anyStr("remove")
var cns2950 = anyStr("loops")
function Function_compile_numfor (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_10, reg_12, reg_13, reg_15, reg_17, reg_18, reg_20, reg_25, reg_26, reg_31, reg_32, reg_35, reg_36, reg_37, reg_40, reg_42, reg_43, reg_48, reg_50, reg_51, reg_52, reg_55, reg_57, reg_58, reg_63, reg_64, reg_69, reg_70, reg_76, reg_77, reg_78, reg_86, reg_87, reg_88, reg_93, reg_97, reg_101, reg_102, reg_103, reg_104, reg_111, reg_113, reg_114, reg_115, reg_122, reg_124, reg_125, reg_126, reg_132, reg_133, reg_135, reg_137, reg_138, reg_139, reg_146, reg_147, reg_148, reg_149, reg_158, reg_160, reg_161, reg_162, reg_169, reg_170, reg_171, reg_177, reg_178, reg_179, reg_185, reg_186, reg_187, reg_188, reg_199, reg_200, reg_201, reg_208, reg_209, reg_210, reg_216, reg_217, reg_222, reg_223, reg_224, reg_225, reg_234, reg_236, reg_237, reg_238, reg_245, reg_246, reg_247, reg_253, reg_254, reg_255, reg_261, reg_262, reg_268, reg_269, reg_273;
  var goto_96=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = get(reg_4, cns2837);
  reg_8 = newStack();
  push(reg_8, reg_4);
  reg_10 = first(call(reg_7, reg_8));
  reg_12 = get(reg_4, cns2838);
  reg_13 = newStack();
  push(reg_13, reg_4);
  reg_15 = first(call(reg_12, reg_13));
  reg_17 = get(reg_4, cns2839);
  reg_18 = newStack();
  push(reg_18, reg_4);
  reg_20 = first(call(reg_17, reg_18));
  reg_25 = get(get(reg_1.a, cns2840), cns2841);
  reg_26 = newStack();
  push(reg_26, get(reg_4, cns2842));
  push(reg_26, reg_15);
  reg_29 = call(reg_25, reg_26);
  reg_31 = get(reg_4, cns2843);
  reg_32 = newStack();
  push(reg_32, reg_4);
  reg_33 = call(reg_31, reg_32);
  reg_35 = get(reg_4, cns2844);
  reg_36 = newStack();
  push(reg_36, reg_4);
  reg_37 = newTable();
  set(reg_37, cns2845, cns2846);
  reg_40 = cns2847;
  reg_42 = get(reg_4, cns2848);
  reg_43 = newStack();
  push(reg_43, reg_4);
  push(reg_43, get(reg_5, cns2849));
  table_append(reg_37, reg_40, call(reg_42, reg_43));
  push(reg_36, reg_37);
  reg_48 = first(call(reg_35, reg_36));
  reg_50 = get(reg_4, cns2850);
  reg_51 = newStack();
  push(reg_51, reg_4);
  reg_52 = newTable();
  set(reg_52, cns2851, cns2852);
  reg_55 = cns2853;
  reg_57 = get(reg_4, cns2854);
  reg_58 = newStack();
  push(reg_58, reg_4);
  push(reg_58, get(reg_5, cns2855));
  table_append(reg_52, reg_55, call(reg_57, reg_58));
  push(reg_51, reg_52);
  reg_63 = first(call(reg_50, reg_51));
  reg_64 = nil();
  goto_96 = !tobool(get(reg_5, cns2856));
  if (!goto_96) {
    reg_69 = get(reg_4, cns2857);
    reg_70 = newStack();
    push(reg_70, reg_4);
    push(reg_70, get(reg_5, cns2858));
    reg_64 = first(call(reg_69, reg_70));
  }
  if ((goto_96 || false)) {
    goto_96 = false;
    reg_76 = get(reg_4, cns2859);
    reg_77 = newStack();
    push(reg_77, reg_4);
    reg_78 = newTable();
    set(reg_78, cns2860, cns2861);
    set(reg_78, cns2862, cns2863);
    push(reg_77, reg_78);
    reg_64 = first(call(reg_76, reg_77));
  }
  reg_86 = get(reg_4, cns2864);
  reg_87 = newStack();
  push(reg_87, reg_4);
  reg_88 = newTable();
  set(reg_88, cns2865, cns2866);
  set(reg_88, cns2867, reg_64);
  push(reg_87, reg_88);
  reg_93 = first(call(reg_86, reg_87));
  reg_97 = get(get(reg_4, cns2868), cns2869);
  set(reg_97, get(reg_5, cns2870), reg_48);
  reg_101 = get(reg_4, cns2871);
  reg_102 = newStack();
  push(reg_102, reg_4);
  reg_103 = newTable();
  reg_104 = cns2872;
  set(reg_103, reg_104, get(get(reg_1.a, cns2873), cns2874));
  set(reg_103, cns2875, reg_93);
  reg_111 = cns2876;
  reg_113 = get(reg_4, cns2877);
  reg_114 = newStack();
  push(reg_114, reg_4);
  reg_115 = newTable();
  set(reg_115, cns2878, cns2879);
  set(reg_115, cns2880, cns2881);
  push(reg_114, reg_115);
  table_append(reg_103, reg_111, call(reg_113, reg_114));
  push(reg_102, reg_103);
  reg_122 = first(call(reg_101, reg_102));
  reg_124 = get(reg_4, cns2882);
  reg_125 = newStack();
  push(reg_125, reg_4);
  reg_126 = newTable();
  set(reg_126, cns2883, cns2884);
  set(reg_126, cns2885, reg_10);
  push(reg_125, reg_126);
  reg_130 = call(reg_124, reg_125);
  reg_132 = get(reg_4, cns2886);
  reg_133 = newStack();
  push(reg_133, reg_4);
  reg_135 = first(call(reg_132, reg_133));
  reg_137 = get(reg_4, cns2887);
  reg_138 = newStack();
  push(reg_138, reg_4);
  reg_139 = newTable();
  set(reg_139, cns2888, cns2889);
  set(reg_139, cns2890, reg_135);
  set(reg_139, cns2891, reg_122);
  push(reg_138, reg_139);
  reg_144 = call(reg_137, reg_138);
  reg_146 = get(reg_4, cns2892);
  reg_147 = newStack();
  push(reg_147, reg_4);
  reg_148 = newTable();
  reg_149 = cns2893;
  set(reg_148, reg_149, get(get(reg_1.a, cns2894), cns2895));
  set(reg_148, cns2896, reg_48);
  set(reg_148, cns2897, reg_63);
  push(reg_147, reg_148);
  reg_158 = first(call(reg_146, reg_147));
  reg_160 = get(reg_4, cns2898);
  reg_161 = newStack();
  push(reg_161, reg_4);
  reg_162 = newTable();
  set(reg_162, cns2899, cns2900);
  set(reg_162, cns2901, reg_15);
  set(reg_162, cns2902, reg_158);
  push(reg_161, reg_162);
  reg_167 = call(reg_160, reg_161);
  reg_169 = get(reg_4, cns2903);
  reg_170 = newStack();
  push(reg_170, reg_4);
  reg_171 = newTable();
  set(reg_171, cns2904, cns2905);
  set(reg_171, cns2906, reg_20);
  push(reg_170, reg_171);
  reg_175 = call(reg_169, reg_170);
  reg_177 = get(reg_4, cns2907);
  reg_178 = newStack();
  push(reg_178, reg_4);
  reg_179 = newTable();
  set(reg_179, cns2908, cns2909);
  set(reg_179, cns2910, reg_135);
  push(reg_178, reg_179);
  reg_183 = call(reg_177, reg_178);
  reg_185 = get(reg_4, cns2911);
  reg_186 = newStack();
  push(reg_186, reg_4);
  reg_187 = newTable();
  reg_188 = cns2912;
  set(reg_187, reg_188, get(get(reg_1.a, cns2913), cns2914));
  set(reg_187, cns2915, reg_48);
  set(reg_187, cns2916, reg_63);
  push(reg_186, reg_187);
  reg_158 = first(call(reg_185, reg_186));
  reg_199 = get(reg_4, cns2917);
  reg_200 = newStack();
  push(reg_200, reg_4);
  reg_201 = newTable();
  set(reg_201, cns2918, cns2919);
  set(reg_201, cns2920, reg_15);
  set(reg_201, cns2921, reg_158);
  push(reg_200, reg_201);
  reg_206 = call(reg_199, reg_200);
  reg_208 = get(reg_4, cns2922);
  reg_209 = newStack();
  push(reg_209, reg_4);
  reg_210 = newTable();
  set(reg_210, cns2923, cns2924);
  set(reg_210, cns2925, reg_20);
  push(reg_209, reg_210);
  reg_214 = call(reg_208, reg_209);
  reg_216 = get(reg_4, cns2926);
  reg_217 = newStack();
  push(reg_217, reg_4);
  push(reg_217, get(reg_5, cns2927));
  reg_220 = call(reg_216, reg_217);
  reg_222 = get(reg_4, cns2928);
  reg_223 = newStack();
  push(reg_223, reg_4);
  reg_224 = newTable();
  reg_225 = cns2929;
  set(reg_224, reg_225, get(get(reg_1.a, cns2930), cns2931));
  set(reg_224, cns2932, reg_48);
  set(reg_224, cns2933, reg_93);
  push(reg_223, reg_224);
  reg_234 = first(call(reg_222, reg_223));
  reg_236 = get(reg_4, cns2934);
  reg_237 = newStack();
  push(reg_237, reg_4);
  reg_238 = newTable();
  set(reg_238, cns2935, cns2936);
  set(reg_238, cns2937, reg_48);
  set(reg_238, cns2938, reg_234);
  push(reg_237, reg_238);
  reg_243 = call(reg_236, reg_237);
  reg_245 = get(reg_4, cns2939);
  reg_246 = newStack();
  push(reg_246, reg_4);
  reg_247 = newTable();
  set(reg_247, cns2940, cns2941);
  set(reg_247, cns2942, reg_10);
  push(reg_246, reg_247);
  reg_251 = call(reg_245, reg_246);
  reg_253 = get(reg_4, cns2943);
  reg_254 = newStack();
  push(reg_254, reg_4);
  reg_255 = newTable();
  set(reg_255, cns2944, cns2945);
  set(reg_255, cns2946, reg_15);
  push(reg_254, reg_255);
  reg_259 = call(reg_253, reg_254);
  reg_261 = get(reg_4, cns2947);
  reg_262 = newStack();
  push(reg_262, reg_4);
  reg_263 = call(reg_261, reg_262);
  reg_268 = get(get(reg_1.a, cns2948), cns2949);
  reg_269 = newStack();
  push(reg_269, get(reg_4, cns2950));
  reg_272 = call(reg_268, reg_269);
  reg_273 = newStack();
  return reg_273;
}
var cns2951 = anyStr("Function")
var cns2952 = anyStr("compile_genfor")
var cns2953 = anyStr("lbl")
var cns2954 = anyStr("lbl")
var cns2955 = anyStr("table")
var cns2956 = anyStr("insert")
var cns2957 = anyStr("loops")
var cns2958 = anyStr("push_scope")
var cns2959 = anyStr("assign")
var cns2960 = parseNum("1")
var cns2961 = anyStr("lcl")
var cns2962 = anyStr(".f")
var cns2963 = parseNum("2")
var cns2964 = anyStr("lcl")
var cns2965 = anyStr(".s")
var cns2966 = parseNum("3")
var cns2967 = anyStr("lcl")
var cns2968 = anyStr(".var")
var cns2969 = anyStr("values")
var cns2970 = anyStr("scope")
var cns2971 = anyStr("locals")
var cns2972 = anyStr(".f")
var cns2973 = anyStr("scope")
var cns2974 = anyStr("locals")
var cns2975 = anyStr(".s")
var cns2976 = anyStr("scope")
var cns2977 = anyStr("locals")
var cns2978 = anyStr(".var")
var cns2979 = anyStr("inst")
var cns2980 = parseNum("1")
var cns2981 = anyStr("label")
var cns2982 = parseNum("2")
var cns2983 = anyStr("inst")
var cns2984 = parseNum("1")
var cns2985 = anyStr("stack_f")
var cns2986 = anyStr("inst")
var cns2987 = parseNum("1")
var cns2988 = anyStr("push_f")
var cns2989 = parseNum("2")
var cns2990 = parseNum("3")
var cns2991 = anyStr("inst")
var cns2992 = parseNum("1")
var cns2993 = anyStr("push_f")
var cns2994 = parseNum("2")
var cns2995 = parseNum("3")
var cns2996 = anyStr("inst")
var cns2997 = parseNum("1")
var cns2998 = anyStr("call_f")
var cns2999 = parseNum("2")
var cns3000 = parseNum("3")
var cns3001 = anyStr("ipairs")
var cns3002 = anyStr("names")
var cns3003 = anyStr("inst")
var cns3004 = parseNum("1")
var cns3005 = anyStr("next_f")
var cns3006 = parseNum("2")
var cns3007 = anyStr("scope")
var cns3008 = anyStr("locals")
var cns3009 = anyStr("inst")
var cns3010 = parseNum("1")
var cns3011 = anyStr("local")
var cns3012 = parseNum("2")
var cns3013 = parseNum("1")
var cns3014 = anyStr("inst")
var cns3015 = parseNum("1")
var cns3016 = anyStr("set")
var cns3017 = parseNum("2")
var cns3018 = parseNum("3")
var cns3019 = anyStr("inst")
var cns3020 = parseNum("1")
var cns3021 = anyStr("binops")
var cns3022 = anyStr("==")
var cns3023 = parseNum("2")
var cns3024 = parseNum("3")
var cns3025 = anyStr("inst")
var cns3026 = parseNum("1")
var cns3027 = anyStr("nil_f")
var cns3028 = anyStr("inst")
var cns3029 = parseNum("1")
var cns3030 = anyStr("jif")
var cns3031 = parseNum("2")
var cns3032 = parseNum("3")
var cns3033 = anyStr("compileBlock")
var cns3034 = anyStr("body")
var cns3035 = anyStr("inst")
var cns3036 = parseNum("1")
var cns3037 = anyStr("jmp")
var cns3038 = parseNum("2")
var cns3039 = anyStr("inst")
var cns3040 = parseNum("1")
var cns3041 = anyStr("label")
var cns3042 = parseNum("2")
var cns3043 = anyStr("pop_scope")
var cns3044 = anyStr("table")
var cns3045 = anyStr("remove")
var cns3046 = anyStr("loops")
function Function_compile_genfor (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_10, reg_12, reg_13, reg_15, reg_20, reg_21, reg_26, reg_27, reg_30, reg_31, reg_32, reg_33, reg_34, reg_37, reg_38, reg_41, reg_42, reg_53, reg_59, reg_65, reg_67, reg_68, reg_69, reg_75, reg_76, reg_77, reg_78, reg_83, reg_85, reg_86, reg_87, reg_88, reg_96, reg_97, reg_98, reg_99, reg_107, reg_108, reg_109, reg_110, reg_117, reg_120, reg_121, reg_124, reg_125, reg_126, reg_127, reg_128, reg_129, reg_130, reg_131, reg_136, reg_137, reg_138, reg_139, reg_145, reg_149, reg_151, reg_152, reg_153, reg_163, reg_164, reg_165, reg_172, reg_173, reg_174, reg_175, reg_182, reg_184, reg_185, reg_186, reg_187, reg_193, reg_195, reg_196, reg_197, reg_204, reg_205, reg_210, reg_211, reg_212, reg_218, reg_219, reg_220, reg_226, reg_227, reg_233, reg_234, reg_238;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = get(reg_4, cns2953);
  reg_8 = newStack();
  push(reg_8, reg_4);
  reg_10 = first(call(reg_7, reg_8));
  reg_12 = get(reg_4, cns2954);
  reg_13 = newStack();
  push(reg_13, reg_4);
  reg_15 = first(call(reg_12, reg_13));
  reg_20 = get(get(reg_1.a, cns2955), cns2956);
  reg_21 = newStack();
  push(reg_21, get(reg_4, cns2957));
  push(reg_21, reg_15);
  reg_24 = call(reg_20, reg_21);
  reg_26 = get(reg_4, cns2958);
  reg_27 = newStack();
  push(reg_27, reg_4);
  reg_28 = call(reg_26, reg_27);
  reg_30 = get(reg_4, cns2959);
  reg_31 = newStack();
  push(reg_31, reg_4);
  reg_32 = newTable();
  reg_33 = cns2960;
  reg_34 = newTable();
  set(reg_34, cns2961, cns2962);
  set(reg_32, reg_33, reg_34);
  reg_37 = cns2963;
  reg_38 = newTable();
  set(reg_38, cns2964, cns2965);
  set(reg_32, reg_37, reg_38);
  reg_41 = cns2966;
  reg_42 = newTable();
  set(reg_42, cns2967, cns2968);
  set(reg_32, reg_41, reg_42);
  push(reg_31, reg_32);
  push(reg_31, get(reg_5, cns2969));
  reg_47 = call(reg_30, reg_31);
  reg_53 = get(get(get(reg_4, cns2970), cns2971), cns2972);
  reg_59 = get(get(get(reg_4, cns2973), cns2974), cns2975);
  reg_65 = get(get(get(reg_4, cns2976), cns2977), cns2978);
  reg_67 = get(reg_4, cns2979);
  reg_68 = newStack();
  push(reg_68, reg_4);
  reg_69 = newTable();
  set(reg_69, cns2980, cns2981);
  set(reg_69, cns2982, reg_10);
  push(reg_68, reg_69);
  reg_73 = call(reg_67, reg_68);
  reg_75 = get(reg_4, cns2983);
  reg_76 = newStack();
  push(reg_76, reg_4);
  reg_77 = newTable();
  reg_78 = cns2984;
  set(reg_77, reg_78, get(reg_1.a, cns2985));
  push(reg_76, reg_77);
  reg_83 = first(call(reg_75, reg_76));
  reg_85 = get(reg_4, cns2986);
  reg_86 = newStack();
  push(reg_86, reg_4);
  reg_87 = newTable();
  reg_88 = cns2987;
  set(reg_87, reg_88, get(reg_1.a, cns2988));
  set(reg_87, cns2989, reg_83);
  set(reg_87, cns2990, reg_59);
  push(reg_86, reg_87);
  reg_94 = call(reg_85, reg_86);
  reg_96 = get(reg_4, cns2991);
  reg_97 = newStack();
  push(reg_97, reg_4);
  reg_98 = newTable();
  reg_99 = cns2992;
  set(reg_98, reg_99, get(reg_1.a, cns2993));
  set(reg_98, cns2994, reg_83);
  set(reg_98, cns2995, reg_65);
  push(reg_97, reg_98);
  reg_105 = call(reg_96, reg_97);
  reg_107 = get(reg_4, cns2996);
  reg_108 = newStack();
  push(reg_108, reg_4);
  reg_109 = newTable();
  reg_110 = cns2997;
  set(reg_109, reg_110, get(reg_1.a, cns2998));
  set(reg_109, cns2999, reg_53);
  set(reg_109, cns3000, reg_83);
  push(reg_108, reg_109);
  reg_117 = first(call(reg_107, reg_108));
  reg_120 = get(reg_1.a, cns3001);
  reg_121 = newStack();
  push(reg_121, get(reg_5, cns3002));
  reg_124 = call(reg_120, reg_121);
  reg_125 = next(reg_124);
  reg_126 = next(reg_124);
  reg_127 = next(reg_124);
  loop_1: while (true) {
    reg_128 = newStack();
    push(reg_128, reg_126);
    push(reg_128, reg_127);
    reg_129 = call(reg_125, reg_128);
    reg_130 = next(reg_129);
    reg_127 = reg_130;
    reg_131 = next(reg_129);
    if (tobool(eq(reg_127, nil()))) break loop_1;
    reg_136 = get(reg_4, cns3003);
    reg_137 = newStack();
    push(reg_137, reg_4);
    reg_138 = newTable();
    reg_139 = cns3004;
    set(reg_138, reg_139, get(reg_1.a, cns3005));
    set(reg_138, cns3006, reg_117);
    push(reg_137, reg_138);
    reg_145 = first(call(reg_136, reg_137));
    reg_149 = get(get(reg_4, cns3007), cns3008);
    reg_151 = get(reg_4, cns3009);
    reg_152 = newStack();
    push(reg_152, reg_4);
    reg_153 = newTable();
    set(reg_153, cns3010, cns3011);
    set(reg_153, cns3012, reg_145);
    push(reg_152, reg_153);
    set(reg_149, reg_131, first(call(reg_151, reg_152)));
    if (tobool(eq(reg_130, cns3013))) {
      reg_163 = get(reg_4, cns3014);
      reg_164 = newStack();
      push(reg_164, reg_4);
      reg_165 = newTable();
      set(reg_165, cns3015, cns3016);
      set(reg_165, cns3017, reg_65);
      set(reg_165, cns3018, reg_145);
      push(reg_164, reg_165);
      reg_170 = call(reg_163, reg_164);
    }
  }
  reg_172 = get(reg_4, cns3019);
  reg_173 = newStack();
  push(reg_173, reg_4);
  reg_174 = newTable();
  reg_175 = cns3020;
  set(reg_174, reg_175, get(get(reg_1.a, cns3021), cns3022));
  set(reg_174, cns3023, reg_65);
  reg_182 = cns3024;
  reg_184 = get(reg_4, cns3025);
  reg_185 = newStack();
  push(reg_185, reg_4);
  reg_186 = newTable();
  reg_187 = cns3026;
  set(reg_186, reg_187, get(reg_1.a, cns3027));
  push(reg_185, reg_186);
  table_append(reg_174, reg_182, call(reg_184, reg_185));
  push(reg_173, reg_174);
  reg_193 = first(call(reg_172, reg_173));
  reg_195 = get(reg_4, cns3028);
  reg_196 = newStack();
  push(reg_196, reg_4);
  reg_197 = newTable();
  set(reg_197, cns3029, cns3030);
  set(reg_197, cns3031, reg_15);
  set(reg_197, cns3032, reg_193);
  push(reg_196, reg_197);
  reg_202 = call(reg_195, reg_196);
  reg_204 = get(reg_4, cns3033);
  reg_205 = newStack();
  push(reg_205, reg_4);
  push(reg_205, get(reg_5, cns3034));
  reg_208 = call(reg_204, reg_205);
  reg_210 = get(reg_4, cns3035);
  reg_211 = newStack();
  push(reg_211, reg_4);
  reg_212 = newTable();
  set(reg_212, cns3036, cns3037);
  set(reg_212, cns3038, reg_10);
  push(reg_211, reg_212);
  reg_216 = call(reg_210, reg_211);
  reg_218 = get(reg_4, cns3039);
  reg_219 = newStack();
  push(reg_219, reg_4);
  reg_220 = newTable();
  set(reg_220, cns3040, cns3041);
  set(reg_220, cns3042, reg_15);
  push(reg_219, reg_220);
  reg_224 = call(reg_218, reg_219);
  reg_226 = get(reg_4, cns3043);
  reg_227 = newStack();
  push(reg_227, reg_4);
  reg_228 = call(reg_226, reg_227);
  reg_233 = get(get(reg_1.a, cns3044), cns3045);
  reg_234 = newStack();
  push(reg_234, get(reg_4, cns3046));
  reg_237 = call(reg_233, reg_234);
  reg_238 = newStack();
  return reg_238;
}
var cns3047 = anyStr("Function")
var cns3048 = anyStr("compileStmt")
var cns3049 = anyStr("type")
var cns3050 = anyStr("local")
var cns3051 = anyStr("ipairs")
var cns3052 = anyStr("names")
var cns3053 = anyStr("lcl")
var cns3054 = anyStr("assign")
var cns3055 = anyStr("values")
var cns3056 = anyStr("line")
var cns3057 = anyStr("call")
var cns3058 = anyStr("compile_call")
var cns3059 = anyStr("assignment")
var cns3060 = anyStr("ipairs")
var cns3061 = anyStr("lhs")
var cns3062 = anyStr("compileLhs")
var cns3063 = anyStr("assign")
var cns3064 = anyStr("values")
var cns3065 = anyStr("line")
var cns3066 = anyStr("return")
var cns3067 = anyStr("call")
var cns3068 = anyStr("stack_f")
var cns3069 = anyStr("ipairs")
var cns3070 = anyStr("values")
var cns3071 = anyStr("values")
var cns3072 = anyStr("type")
var cns3073 = anyStr("call")
var cns3074 = anyStr("compile_call")
var cns3075 = anyStr("call")
var cns3076 = anyStr("append_f")
var cns3077 = anyStr("compileExpr")
var cns3078 = anyStr("call")
var cns3079 = anyStr("push_f")
var cns3080 = anyStr("inst")
var cns3081 = parseNum("1")
var cns3082 = anyStr("end")
var cns3083 = parseNum("2")
var cns3084 = anyStr("funcstat")
var cns3085 = anyStr("method")
var cns3086 = anyStr("table")
var cns3087 = anyStr("insert")
var cns3088 = anyStr("body")
var cns3089 = anyStr("names")
var cns3090 = parseNum("1")
var cns3091 = anyStr("self")
var cns3092 = anyStr("assign")
var cns3093 = parseNum("1")
var cns3094 = anyStr("compileLhs")
var cns3095 = anyStr("lhs")
var cns3096 = parseNum("1")
var cns3097 = anyStr("body")
var cns3098 = anyStr("line")
var cns3099 = anyStr("localfunc")
var cns3100 = parseNum("1")
var cns3101 = anyStr("local")
var cns3102 = anyStr("scope")
var cns3103 = anyStr("locals")
var cns3104 = anyStr("name")
var cns3105 = anyStr("repr")
var cns3106 = anyStr("name")
var cns3107 = parseNum("2")
var cns3108 = anyStr("compileExpr")
var cns3109 = anyStr("body")
var cns3110 = anyStr("inst")
var cns3111 = anyStr("do")
var cns3112 = anyStr("push_scope")
var cns3113 = anyStr("compileBlock")
var cns3114 = anyStr("body")
var cns3115 = anyStr("pop_scope")
var cns3116 = anyStr("if")
var cns3117 = anyStr("lbl")
var cns3118 = anyStr("ipairs")
var cns3119 = anyStr("clauses")
var cns3120 = anyStr("lbl")
var cns3121 = anyStr("compile_bool")
var cns3122 = anyStr("cond")
var cns3123 = anyStr("inst")
var cns3124 = parseNum("1")
var cns3125 = anyStr("nif")
var cns3126 = parseNum("2")
var cns3127 = parseNum("3")
var cns3128 = anyStr("push_scope")
var cns3129 = anyStr("compileBlock")
var cns3130 = anyStr("body")
var cns3131 = anyStr("pop_scope")
var cns3132 = anyStr("inst")
var cns3133 = parseNum("1")
var cns3134 = anyStr("jmp")
var cns3135 = parseNum("2")
var cns3136 = anyStr("inst")
var cns3137 = parseNum("1")
var cns3138 = anyStr("label")
var cns3139 = parseNum("2")
var cns3140 = anyStr("els")
var cns3141 = anyStr("push_scope")
var cns3142 = anyStr("compileBlock")
var cns3143 = anyStr("els")
var cns3144 = anyStr("pop_scope")
var cns3145 = anyStr("inst")
var cns3146 = parseNum("1")
var cns3147 = anyStr("label")
var cns3148 = parseNum("2")
var cns3149 = anyStr("while")
var cns3150 = anyStr("lbl")
var cns3151 = anyStr("lbl")
var cns3152 = anyStr("inst")
var cns3153 = parseNum("1")
var cns3154 = anyStr("label")
var cns3155 = parseNum("2")
var cns3156 = anyStr("compile_bool")
var cns3157 = anyStr("cond")
var cns3158 = anyStr("inst")
var cns3159 = parseNum("1")
var cns3160 = anyStr("nif")
var cns3161 = parseNum("2")
var cns3162 = parseNum("3")
var cns3163 = anyStr("table")
var cns3164 = anyStr("insert")
var cns3165 = anyStr("loops")
var cns3166 = anyStr("push_scope")
var cns3167 = anyStr("compileBlock")
var cns3168 = anyStr("body")
var cns3169 = anyStr("pop_scope")
var cns3170 = anyStr("table")
var cns3171 = anyStr("remove")
var cns3172 = anyStr("loops")
var cns3173 = anyStr("inst")
var cns3174 = parseNum("1")
var cns3175 = anyStr("jmp")
var cns3176 = parseNum("2")
var cns3177 = anyStr("inst")
var cns3178 = parseNum("1")
var cns3179 = anyStr("label")
var cns3180 = parseNum("2")
var cns3181 = anyStr("repeat")
var cns3182 = anyStr("lbl")
var cns3183 = anyStr("lbl")
var cns3184 = anyStr("table")
var cns3185 = anyStr("insert")
var cns3186 = anyStr("loops")
var cns3187 = anyStr("push_scope")
var cns3188 = anyStr("inst")
var cns3189 = parseNum("1")
var cns3190 = anyStr("label")
var cns3191 = parseNum("2")
var cns3192 = anyStr("compileBlock")
var cns3193 = anyStr("body")
var cns3194 = anyStr("compile_bool")
var cns3195 = anyStr("cond")
var cns3196 = anyStr("inst")
var cns3197 = parseNum("1")
var cns3198 = anyStr("nif")
var cns3199 = parseNum("2")
var cns3200 = parseNum("3")
var cns3201 = anyStr("inst")
var cns3202 = parseNum("1")
var cns3203 = anyStr("label")
var cns3204 = parseNum("2")
var cns3205 = anyStr("pop_scope")
var cns3206 = anyStr("table")
var cns3207 = anyStr("remove")
var cns3208 = anyStr("loops")
var cns3209 = anyStr("numfor")
var cns3210 = anyStr("compile_numfor")
var cns3211 = anyStr("genfor")
var cns3212 = anyStr("compile_genfor")
var cns3213 = anyStr("break")
var cns3214 = anyStr("inst")
var cns3215 = parseNum("1")
var cns3216 = anyStr("jmp")
var cns3217 = parseNum("2")
var cns3218 = anyStr("loops")
var cns3219 = anyStr("loops")
var cns3220 = anyStr("label")
var cns3221 = anyStr("inst")
var cns3222 = parseNum("1")
var cns3223 = anyStr("label")
var cns3224 = parseNum("2")
var cns3225 = anyStr("name")
var cns3226 = anyStr("goto")
var cns3227 = anyStr("inst")
var cns3228 = parseNum("1")
var cns3229 = anyStr("jmp")
var cns3230 = parseNum("2")
var cns3231 = anyStr("name")
var cns3232 = anyStr("line")
var cns3233 = anyStr("line")
var cns3234 = anyStr("err")
var cns3235 = anyStr("statement not supported: ")
function Function_compileStmt (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_11, reg_14, reg_15, reg_18, reg_19, reg_20, reg_21, reg_22, reg_23, reg_24, reg_25, reg_29, reg_32, reg_33, reg_43, reg_44, reg_50, reg_53, reg_54, reg_57, reg_58, reg_59, reg_60, reg_61, reg_62, reg_63, reg_64, reg_69, reg_70, reg_74, reg_75, reg_85, reg_86, reg_91, reg_94, reg_95, reg_98, reg_99, reg_100, reg_101, reg_102, reg_103, reg_104, reg_105, reg_112, reg_120, reg_121, reg_123, reg_125, reg_126, reg_132, reg_133, reg_135, reg_137, reg_138, reg_144, reg_145, reg_146, reg_161, reg_162, reg_171, reg_172, reg_173, reg_174, reg_176, reg_177, reg_181, reg_182, reg_191, reg_197, reg_200, reg_203, reg_205, reg_206, reg_212, reg_213, reg_219, reg_220, reg_223, reg_224, reg_229, reg_230, reg_236, reg_237, reg_239, reg_242, reg_243, reg_246, reg_247, reg_248, reg_249, reg_250, reg_251, reg_253, reg_258, reg_259, reg_261, reg_263, reg_264, reg_268, reg_270, reg_271, reg_272, reg_279, reg_280, reg_283, reg_284, reg_289, reg_290, reg_293, reg_294, reg_295, reg_301, reg_302, reg_303, reg_312, reg_313, reg_316, reg_317, reg_322, reg_323, reg_326, reg_327, reg_328, reg_337, reg_338, reg_340, reg_342, reg_343, reg_345, reg_347, reg_348, reg_349, reg_355, reg_356, reg_360, reg_362, reg_363, reg_364, reg_374, reg_375, reg_380, reg_381, reg_384, reg_385, reg_390, reg_391, reg_397, reg_398, reg_403, reg_404, reg_405, reg_411, reg_412, reg_413, reg_422, reg_423, reg_425, reg_427, reg_428, reg_430, reg_435, reg_436, reg_441, reg_442, reg_445, reg_446, reg_447, reg_453, reg_454, reg_459, reg_460, reg_464, reg_466, reg_467, reg_468, reg_475, reg_476, reg_477, reg_483, reg_484, reg_490, reg_491, reg_499, reg_500, reg_506, reg_507, reg_513, reg_514, reg_515, reg_518, reg_520, reg_530, reg_531, reg_532, reg_535, reg_543, reg_544, reg_545, reg_548, reg_551, reg_557, reg_558, reg_562;
  var goto_51=false, goto_64=false, goto_113=false, goto_181=false, goto_213=false, goto_265=false, goto_302=false, goto_325=false, goto_465=false, goto_580=false, goto_683=false, goto_694=false, goto_705=false, goto_728=false, goto_747=false, goto_770=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = get(reg_5, cns3049);
  goto_51 = !tobool(eq(reg_7, cns3050));
  if (!goto_51) {
    reg_11 = newTable();
    reg_14 = get(reg_1.a, cns3051);
    reg_15 = newStack();
    push(reg_15, get(reg_5, cns3052));
    reg_18 = call(reg_14, reg_15);
    reg_19 = next(reg_18);
    reg_20 = next(reg_18);
    reg_21 = next(reg_18);
    loop_4: while (true) {
      reg_22 = newStack();
      push(reg_22, reg_20);
      push(reg_22, reg_21);
      reg_23 = call(reg_19, reg_22);
      reg_24 = next(reg_23);
      reg_21 = reg_24;
      reg_25 = next(reg_23);
      if (tobool(eq(reg_21, nil()))) break loop_4;
      reg_29 = newTable();
      set(reg_29, cns3053, reg_25);
      set(reg_11, reg_24, reg_29);
    }
    reg_32 = get(reg_4, cns3054);
    reg_33 = newStack();
    push(reg_33, reg_4);
    push(reg_33, reg_11);
    push(reg_33, get(reg_5, cns3055));
    push(reg_33, get(reg_5, cns3056));
    reg_38 = call(reg_32, reg_33);
  }
  if ((goto_51 || false)) {
    goto_51 = false;
    goto_64 = !tobool(eq(reg_7, cns3057));
    if (!goto_64) {
      reg_43 = get(reg_4, cns3058);
      reg_44 = newStack();
      push(reg_44, reg_4);
      push(reg_44, reg_5);
      push(reg_44, lua$true());
      reg_46 = call(reg_43, reg_44);
    }
    if ((goto_64 || false)) {
      goto_64 = false;
      goto_113 = !tobool(eq(reg_7, cns3059));
      if (!goto_113) {
        reg_50 = newTable();
        reg_53 = get(reg_1.a, cns3060);
        reg_54 = newStack();
        push(reg_54, get(reg_5, cns3061));
        reg_57 = call(reg_53, reg_54);
        reg_58 = next(reg_57);
        reg_59 = next(reg_57);
        reg_60 = next(reg_57);
        loop_3: while (true) {
          reg_61 = newStack();
          push(reg_61, reg_59);
          push(reg_61, reg_60);
          reg_62 = call(reg_58, reg_61);
          reg_63 = next(reg_62);
          reg_60 = reg_63;
          reg_64 = next(reg_62);
          if (tobool(eq(reg_60, nil()))) break loop_3;
          reg_69 = get(reg_4, cns3062);
          reg_70 = newStack();
          push(reg_70, reg_4);
          push(reg_70, reg_64);
          set(reg_50, reg_63, first(call(reg_69, reg_70)));
        }
        reg_74 = get(reg_4, cns3063);
        reg_75 = newStack();
        push(reg_75, reg_4);
        push(reg_75, reg_50);
        push(reg_75, get(reg_5, cns3064));
        push(reg_75, get(reg_5, cns3065));
        reg_80 = call(reg_74, reg_75);
      }
      if ((goto_113 || false)) {
        goto_113 = false;
        goto_213 = !tobool(eq(reg_7, cns3066));
        if (!goto_213) {
          reg_85 = get(reg_4, cns3067);
          reg_86 = newStack();
          push(reg_86, reg_4);
          push(reg_86, get(reg_1.a, cns3068));
          reg_91 = first(call(reg_85, reg_86));
          reg_94 = get(reg_1.a, cns3069);
          reg_95 = newStack();
          push(reg_95, get(reg_5, cns3070));
          reg_98 = call(reg_94, reg_95);
          reg_99 = next(reg_98);
          reg_100 = next(reg_98);
          reg_101 = next(reg_98);
          loop_2: while (true) {
            reg_102 = newStack();
            push(reg_102, reg_100);
            push(reg_102, reg_101);
            reg_103 = call(reg_99, reg_102);
            reg_104 = next(reg_103);
            reg_101 = reg_104;
            reg_105 = next(reg_103);
            if (tobool(eq(reg_101, nil()))) break loop_2;
            reg_112 = eq(reg_104, length(get(reg_5, cns3071)));
            if (tobool(reg_112)) {
              reg_112 = eq(get(reg_105, cns3072), cns3073);
            }
            goto_181 = !tobool(reg_112);
            if (!goto_181) {
              reg_120 = get(reg_4, cns3074);
              reg_121 = newStack();
              push(reg_121, reg_4);
              push(reg_121, reg_105);
              reg_123 = first(call(reg_120, reg_121));
              reg_125 = get(reg_4, cns3075);
              reg_126 = newStack();
              push(reg_126, reg_4);
              push(reg_126, get(reg_1.a, cns3076));
              push(reg_126, reg_91);
              push(reg_126, reg_123);
              reg_130 = call(reg_125, reg_126);
            }
            if ((goto_181 || false)) {
              goto_181 = false;
              reg_132 = get(reg_4, cns3077);
              reg_133 = newStack();
              push(reg_133, reg_4);
              push(reg_133, reg_105);
              reg_135 = first(call(reg_132, reg_133));
              reg_137 = get(reg_4, cns3078);
              reg_138 = newStack();
              push(reg_138, reg_4);
              push(reg_138, get(reg_1.a, cns3079));
              push(reg_138, reg_91);
              push(reg_138, reg_135);
              reg_142 = call(reg_137, reg_138);
            }
          }
          reg_144 = get(reg_4, cns3080);
          reg_145 = newStack();
          push(reg_145, reg_4);
          reg_146 = newTable();
          set(reg_146, cns3081, cns3082);
          set(reg_146, cns3083, reg_91);
          push(reg_145, reg_146);
          reg_150 = call(reg_144, reg_145);
        }
        if ((goto_213 || false)) {
          goto_213 = false;
          goto_265 = !tobool(eq(reg_7, cns3084));
          if (!goto_265) {
            if (tobool(get(reg_5, cns3085))) {
              reg_161 = get(get(reg_1.a, cns3086), cns3087);
              reg_162 = newStack();
              push(reg_162, get(get(reg_5, cns3088), cns3089));
              push(reg_162, cns3090);
              push(reg_162, cns3091);
              reg_169 = call(reg_161, reg_162);
            }
            reg_171 = get(reg_4, cns3092);
            reg_172 = newStack();
            push(reg_172, reg_4);
            reg_173 = newTable();
            reg_174 = cns3093;
            reg_176 = get(reg_4, cns3094);
            reg_177 = newStack();
            push(reg_177, reg_4);
            push(reg_177, get(reg_5, cns3095));
            table_append(reg_173, reg_174, call(reg_176, reg_177));
            push(reg_172, reg_173);
            reg_181 = newTable();
            reg_182 = cns3096;
            set(reg_181, reg_182, get(reg_5, cns3097));
            push(reg_172, reg_181);
            push(reg_172, get(reg_5, cns3098));
            reg_187 = call(reg_171, reg_172);
          }
          if ((goto_265 || false)) {
            goto_265 = false;
            goto_302 = !tobool(eq(reg_7, cns3099));
            if (!goto_302) {
              reg_191 = newTable();
              set(reg_191, cns3100, cns3101);
              reg_197 = get(get(reg_4, cns3102), cns3103);
              set(reg_197, get(reg_5, cns3104), reg_191);
              reg_200 = cns3105;
              set(reg_5, reg_200, get(reg_5, cns3106));
              reg_203 = cns3107;
              reg_205 = get(reg_4, cns3108);
              reg_206 = newStack();
              push(reg_206, reg_4);
              push(reg_206, get(reg_5, cns3109));
              set(reg_191, reg_203, first(call(reg_205, reg_206)));
              reg_212 = get(reg_4, cns3110);
              reg_213 = newStack();
              push(reg_213, reg_4);
              push(reg_213, reg_191);
              reg_214 = call(reg_212, reg_213);
            }
            if ((goto_302 || false)) {
              goto_302 = false;
              goto_325 = !tobool(eq(reg_7, cns3111));
              if (!goto_325) {
                reg_219 = get(reg_4, cns3112);
                reg_220 = newStack();
                push(reg_220, reg_4);
                reg_221 = call(reg_219, reg_220);
                reg_223 = get(reg_4, cns3113);
                reg_224 = newStack();
                push(reg_224, reg_4);
                push(reg_224, get(reg_5, cns3114));
                reg_227 = call(reg_223, reg_224);
                reg_229 = get(reg_4, cns3115);
                reg_230 = newStack();
                push(reg_230, reg_4);
                reg_231 = call(reg_229, reg_230);
              }
              if ((goto_325 || false)) {
                goto_325 = false;
                goto_465 = !tobool(eq(reg_7, cns3116));
                if (!goto_465) {
                  reg_236 = get(reg_4, cns3117);
                  reg_237 = newStack();
                  push(reg_237, reg_4);
                  reg_239 = first(call(reg_236, reg_237));
                  reg_242 = get(reg_1.a, cns3118);
                  reg_243 = newStack();
                  push(reg_243, get(reg_5, cns3119));
                  reg_246 = call(reg_242, reg_243);
                  reg_247 = next(reg_246);
                  reg_248 = next(reg_246);
                  reg_249 = next(reg_246);
                  loop_1: while (true) {
                    reg_250 = newStack();
                    push(reg_250, reg_248);
                    push(reg_250, reg_249);
                    reg_251 = call(reg_247, reg_250);
                    reg_249 = next(reg_251);
                    reg_253 = next(reg_251);
                    if (tobool(eq(reg_249, nil()))) break loop_1;
                    reg_258 = get(reg_4, cns3120);
                    reg_259 = newStack();
                    push(reg_259, reg_4);
                    reg_261 = first(call(reg_258, reg_259));
                    reg_263 = get(reg_4, cns3121);
                    reg_264 = newStack();
                    push(reg_264, reg_4);
                    push(reg_264, get(reg_253, cns3122));
                    reg_268 = first(call(reg_263, reg_264));
                    reg_270 = get(reg_4, cns3123);
                    reg_271 = newStack();
                    push(reg_271, reg_4);
                    reg_272 = newTable();
                    set(reg_272, cns3124, cns3125);
                    set(reg_272, cns3126, reg_261);
                    set(reg_272, cns3127, reg_268);
                    push(reg_271, reg_272);
                    reg_277 = call(reg_270, reg_271);
                    reg_279 = get(reg_4, cns3128);
                    reg_280 = newStack();
                    push(reg_280, reg_4);
                    reg_281 = call(reg_279, reg_280);
                    reg_283 = get(reg_4, cns3129);
                    reg_284 = newStack();
                    push(reg_284, reg_4);
                    push(reg_284, get(reg_253, cns3130));
                    reg_287 = call(reg_283, reg_284);
                    reg_289 = get(reg_4, cns3131);
                    reg_290 = newStack();
                    push(reg_290, reg_4);
                    reg_291 = call(reg_289, reg_290);
                    reg_293 = get(reg_4, cns3132);
                    reg_294 = newStack();
                    push(reg_294, reg_4);
                    reg_295 = newTable();
                    set(reg_295, cns3133, cns3134);
                    set(reg_295, cns3135, reg_239);
                    push(reg_294, reg_295);
                    reg_299 = call(reg_293, reg_294);
                    reg_301 = get(reg_4, cns3136);
                    reg_302 = newStack();
                    push(reg_302, reg_4);
                    reg_303 = newTable();
                    set(reg_303, cns3137, cns3138);
                    set(reg_303, cns3139, reg_261);
                    push(reg_302, reg_303);
                    reg_307 = call(reg_301, reg_302);
                  }
                  if (tobool(get(reg_5, cns3140))) {
                    reg_312 = get(reg_4, cns3141);
                    reg_313 = newStack();
                    push(reg_313, reg_4);
                    reg_314 = call(reg_312, reg_313);
                    reg_316 = get(reg_4, cns3142);
                    reg_317 = newStack();
                    push(reg_317, reg_4);
                    push(reg_317, get(reg_5, cns3143));
                    reg_320 = call(reg_316, reg_317);
                    reg_322 = get(reg_4, cns3144);
                    reg_323 = newStack();
                    push(reg_323, reg_4);
                    reg_324 = call(reg_322, reg_323);
                  }
                  reg_326 = get(reg_4, cns3145);
                  reg_327 = newStack();
                  push(reg_327, reg_4);
                  reg_328 = newTable();
                  set(reg_328, cns3146, cns3147);
                  set(reg_328, cns3148, reg_239);
                  push(reg_327, reg_328);
                  reg_332 = call(reg_326, reg_327);
                }
                if ((goto_465 || false)) {
                  goto_465 = false;
                  goto_580 = !tobool(eq(reg_7, cns3149));
                  if (!goto_580) {
                    reg_337 = get(reg_4, cns3150);
                    reg_338 = newStack();
                    push(reg_338, reg_4);
                    reg_340 = first(call(reg_337, reg_338));
                    reg_342 = get(reg_4, cns3151);
                    reg_343 = newStack();
                    push(reg_343, reg_4);
                    reg_345 = first(call(reg_342, reg_343));
                    reg_347 = get(reg_4, cns3152);
                    reg_348 = newStack();
                    push(reg_348, reg_4);
                    reg_349 = newTable();
                    set(reg_349, cns3153, cns3154);
                    set(reg_349, cns3155, reg_340);
                    push(reg_348, reg_349);
                    reg_353 = call(reg_347, reg_348);
                    reg_355 = get(reg_4, cns3156);
                    reg_356 = newStack();
                    push(reg_356, reg_4);
                    push(reg_356, get(reg_5, cns3157));
                    reg_360 = first(call(reg_355, reg_356));
                    reg_362 = get(reg_4, cns3158);
                    reg_363 = newStack();
                    push(reg_363, reg_4);
                    reg_364 = newTable();
                    set(reg_364, cns3159, cns3160);
                    set(reg_364, cns3161, reg_345);
                    set(reg_364, cns3162, reg_360);
                    push(reg_363, reg_364);
                    reg_369 = call(reg_362, reg_363);
                    reg_374 = get(get(reg_1.a, cns3163), cns3164);
                    reg_375 = newStack();
                    push(reg_375, get(reg_4, cns3165));
                    push(reg_375, reg_345);
                    reg_378 = call(reg_374, reg_375);
                    reg_380 = get(reg_4, cns3166);
                    reg_381 = newStack();
                    push(reg_381, reg_4);
                    reg_382 = call(reg_380, reg_381);
                    reg_384 = get(reg_4, cns3167);
                    reg_385 = newStack();
                    push(reg_385, reg_4);
                    push(reg_385, get(reg_5, cns3168));
                    reg_388 = call(reg_384, reg_385);
                    reg_390 = get(reg_4, cns3169);
                    reg_391 = newStack();
                    push(reg_391, reg_4);
                    reg_392 = call(reg_390, reg_391);
                    reg_397 = get(get(reg_1.a, cns3170), cns3171);
                    reg_398 = newStack();
                    push(reg_398, get(reg_4, cns3172));
                    reg_401 = call(reg_397, reg_398);
                    reg_403 = get(reg_4, cns3173);
                    reg_404 = newStack();
                    push(reg_404, reg_4);
                    reg_405 = newTable();
                    set(reg_405, cns3174, cns3175);
                    set(reg_405, cns3176, reg_340);
                    push(reg_404, reg_405);
                    reg_409 = call(reg_403, reg_404);
                    reg_411 = get(reg_4, cns3177);
                    reg_412 = newStack();
                    push(reg_412, reg_4);
                    reg_413 = newTable();
                    set(reg_413, cns3178, cns3179);
                    set(reg_413, cns3180, reg_345);
                    push(reg_412, reg_413);
                    reg_417 = call(reg_411, reg_412);
                  }
                  if ((goto_580 || false)) {
                    goto_580 = false;
                    goto_683 = !tobool(eq(reg_7, cns3181));
                    if (!goto_683) {
                      reg_422 = get(reg_4, cns3182);
                      reg_423 = newStack();
                      push(reg_423, reg_4);
                      reg_425 = first(call(reg_422, reg_423));
                      reg_427 = get(reg_4, cns3183);
                      reg_428 = newStack();
                      push(reg_428, reg_4);
                      reg_430 = first(call(reg_427, reg_428));
                      reg_435 = get(get(reg_1.a, cns3184), cns3185);
                      reg_436 = newStack();
                      push(reg_436, get(reg_4, cns3186));
                      push(reg_436, reg_430);
                      reg_439 = call(reg_435, reg_436);
                      reg_441 = get(reg_4, cns3187);
                      reg_442 = newStack();
                      push(reg_442, reg_4);
                      reg_443 = call(reg_441, reg_442);
                      reg_445 = get(reg_4, cns3188);
                      reg_446 = newStack();
                      push(reg_446, reg_4);
                      reg_447 = newTable();
                      set(reg_447, cns3189, cns3190);
                      set(reg_447, cns3191, reg_425);
                      push(reg_446, reg_447);
                      reg_451 = call(reg_445, reg_446);
                      reg_453 = get(reg_4, cns3192);
                      reg_454 = newStack();
                      push(reg_454, reg_4);
                      push(reg_454, get(reg_5, cns3193));
                      reg_457 = call(reg_453, reg_454);
                      reg_459 = get(reg_4, cns3194);
                      reg_460 = newStack();
                      push(reg_460, reg_4);
                      push(reg_460, get(reg_5, cns3195));
                      reg_464 = first(call(reg_459, reg_460));
                      reg_466 = get(reg_4, cns3196);
                      reg_467 = newStack();
                      push(reg_467, reg_4);
                      reg_468 = newTable();
                      set(reg_468, cns3197, cns3198);
                      set(reg_468, cns3199, reg_425);
                      set(reg_468, cns3200, reg_464);
                      push(reg_467, reg_468);
                      reg_473 = call(reg_466, reg_467);
                      reg_475 = get(reg_4, cns3201);
                      reg_476 = newStack();
                      push(reg_476, reg_4);
                      reg_477 = newTable();
                      set(reg_477, cns3202, cns3203);
                      set(reg_477, cns3204, reg_430);
                      push(reg_476, reg_477);
                      reg_481 = call(reg_475, reg_476);
                      reg_483 = get(reg_4, cns3205);
                      reg_484 = newStack();
                      push(reg_484, reg_4);
                      reg_485 = call(reg_483, reg_484);
                      reg_490 = get(get(reg_1.a, cns3206), cns3207);
                      reg_491 = newStack();
                      push(reg_491, get(reg_4, cns3208));
                      reg_494 = call(reg_490, reg_491);
                    }
                    if ((goto_683 || false)) {
                      goto_683 = false;
                      goto_694 = !tobool(eq(reg_7, cns3209));
                      if (!goto_694) {
                        reg_499 = get(reg_4, cns3210);
                        reg_500 = newStack();
                        push(reg_500, reg_4);
                        push(reg_500, reg_5);
                        reg_501 = call(reg_499, reg_500);
                      }
                      if ((goto_694 || false)) {
                        goto_694 = false;
                        goto_705 = !tobool(eq(reg_7, cns3211));
                        if (!goto_705) {
                          reg_506 = get(reg_4, cns3212);
                          reg_507 = newStack();
                          push(reg_507, reg_4);
                          push(reg_507, reg_5);
                          reg_508 = call(reg_506, reg_507);
                        }
                        if ((goto_705 || false)) {
                          goto_705 = false;
                          goto_728 = !tobool(eq(reg_7, cns3213));
                          if (!goto_728) {
                            reg_513 = get(reg_4, cns3214);
                            reg_514 = newStack();
                            push(reg_514, reg_4);
                            reg_515 = newTable();
                            set(reg_515, cns3215, cns3216);
                            reg_518 = cns3217;
                            reg_520 = get(reg_4, cns3218);
                            set(reg_515, reg_518, get(reg_520, length(get(reg_4, cns3219))));
                            push(reg_514, reg_515);
                            reg_525 = call(reg_513, reg_514);
                          }
                          if ((goto_728 || false)) {
                            goto_728 = false;
                            goto_747 = !tobool(eq(reg_7, cns3220));
                            if (!goto_747) {
                              reg_530 = get(reg_4, cns3221);
                              reg_531 = newStack();
                              push(reg_531, reg_4);
                              reg_532 = newTable();
                              set(reg_532, cns3222, cns3223);
                              reg_535 = cns3224;
                              set(reg_532, reg_535, get(reg_5, cns3225));
                              push(reg_531, reg_532);
                              reg_538 = call(reg_530, reg_531);
                            }
                            if ((goto_747 || false)) {
                              goto_747 = false;
                              goto_770 = !tobool(eq(reg_7, cns3226));
                              if (!goto_770) {
                                reg_543 = get(reg_4, cns3227);
                                reg_544 = newStack();
                                push(reg_544, reg_4);
                                reg_545 = newTable();
                                set(reg_545, cns3228, cns3229);
                                reg_548 = cns3230;
                                set(reg_545, reg_548, get(reg_5, cns3231));
                                reg_551 = cns3232;
                                set(reg_545, reg_551, get(reg_5, cns3233));
                                push(reg_544, reg_545);
                                reg_554 = call(reg_543, reg_544);
                              }
                              if ((goto_770 || false)) {
                                goto_770 = false;
                                reg_557 = get(reg_1.a, cns3234);
                                reg_558 = newStack();
                                push(reg_558, concat(cns3235, reg_7));
                                push(reg_558, reg_5);
                                reg_561 = call(reg_557, reg_558);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  reg_562 = newStack();
  return reg_562;
}
var cns3236 = anyStr("Function")
var cns3237 = anyStr("compileBlock")
var cns3238 = anyStr("ipairs")
var cns3239 = anyStr("compileStmt")
function Function_compileBlock (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_10, reg_11, reg_12, reg_13, reg_14, reg_15, reg_17, reg_22, reg_23, reg_25;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = get(reg_1.a, cns3238);
  reg_9 = newStack();
  push(reg_9, reg_5);
  reg_10 = call(reg_8, reg_9);
  reg_11 = next(reg_10);
  reg_12 = next(reg_10);
  reg_13 = next(reg_10);
  loop_1: while (true) {
    reg_14 = newStack();
    push(reg_14, reg_12);
    push(reg_14, reg_13);
    reg_15 = call(reg_11, reg_14);
    reg_13 = next(reg_15);
    reg_17 = next(reg_15);
    if (tobool(eq(reg_13, nil()))) break loop_1;
    reg_22 = get(reg_4, cns3239);
    reg_23 = newStack();
    push(reg_23, reg_4);
    push(reg_23, reg_17);
    reg_24 = call(reg_22, reg_23);
  }
  reg_25 = newStack();
  return reg_25;
}
var cns3240 = anyStr("Function")
var cns3241 = anyStr("transform")
var cns3242 = anyStr("code")
var cns3243 = anyStr("code")
var cns3244 = anyStr("labels")
var cns3245 = anyStr("ins")
var cns3246 = parseNum("1")
function function$39 (reg_0, reg_1) {
  var reg_6, reg_9;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_6 = reg_1.b;
  reg_1.b = add(reg_6, cns3246);
  reg_9 = newStack();
  push(reg_9, reg_6);
  return reg_9;
}
var cns3247 = anyStr("reginc")
var cns3248 = anyStr("ipairs")
var cns3249 = parseNum("1")
var cns3250 = anyStr("local")
var cns3251 = parseNum("2")
var cns3252 = anyStr("is_upval")
var cns3253 = anyStr("upval_level_regs")
var cns3254 = anyStr("level")
var cns3255 = anyStr("upval_accessors")
var cns3256 = anyStr("upval_id")
var cns3257 = anyStr("setter")
var cns3258 = anyStr("inst")
var cns3259 = parseNum("1")
var cns3260 = parseNum("2")
var cns3261 = parseNum("3")
var cns3262 = parseNum("1")
var cns3263 = anyStr("var")
var cns3264 = anyStr("is_upval")
var cns3265 = anyStr("inst")
var cns3266 = parseNum("1")
var cns3267 = anyStr("dup")
var cns3268 = parseNum("2")
var cns3269 = anyStr("reg")
var cns3270 = anyStr("reginc")
var cns3271 = anyStr("reg")
var cns3272 = anyStr("reg")
var cns3273 = anyStr("reg")
var cns3274 = anyStr("print")
var cns3275 = anyStr("argument does not have register")
var cns3276 = anyStr("var")
var cns3277 = parseNum("2")
var cns3278 = anyStr("is_upval")
var cns3279 = anyStr("upval_level_regs")
var cns3280 = anyStr("upval_level")
var cns3281 = anyStr("get_level_ancestor")
var cns3282 = anyStr("upval_level")
var cns3283 = anyStr("upval_accessors")
var cns3284 = anyStr("upval_id")
var cns3285 = anyStr("getter")
var cns3286 = anyStr("inst")
var cns3287 = parseNum("1")
var cns3288 = parseNum("2")
var cns3289 = anyStr("reg")
var cns3290 = anyStr("reginc")
var cns3291 = anyStr("reg")
var cns3292 = parseNum("2")
var cns3293 = anyStr("reg")
var cns3294 = anyStr("set")
var cns3295 = parseNum("2")
var cns3296 = parseNum("3")
var cns3297 = anyStr("is_upval")
var cns3298 = anyStr("upval_level_regs")
var cns3299 = anyStr("upval_level")
var cns3300 = anyStr("get_level_ancestor")
var cns3301 = anyStr("upval_level")
var cns3302 = anyStr("upval_accessors")
var cns3303 = anyStr("upval_id")
var cns3304 = anyStr("setter")
var cns3305 = anyStr("inst")
var cns3306 = parseNum("1")
var cns3307 = parseNum("2")
var cns3308 = parseNum("3")
var cns3309 = anyStr("inst")
var cns3310 = anyStr("label")
var cns3311 = anyStr("labels")
var cns3312 = parseNum("2")
var cns3313 = anyStr("code")
var cns3314 = anyStr("jif")
var cns3315 = anyStr("nif")
var cns3316 = parseNum("3")
var cns3317 = anyStr("au_type")
var cns3318 = parseNum("3")
var cns3319 = anyStr("inst")
var cns3320 = parseNum("1")
var cns3321 = anyStr("bool_f")
var cns3322 = parseNum("2")
var cns3323 = parseNum("3")
var cns3324 = parseNum("3")
var cns3325 = anyStr("reg")
var cns3326 = anyStr("reginc")
var cns3327 = anyStr("inst")
var cns3328 = anyStr("type")
var cns3329 = anyStr("table")
var cns3330 = parseNum("1")
var cns3331 = anyStr("ins")
var cns3332 = anyStr("error")
var cns3333 = anyStr("tostring")
var cns3334 = anyStr(" expects ")
var cns3335 = anyStr("ins")
var cns3336 = anyStr(" arguments, but got ")
var cns3337 = parseNum("1")
var cns3338 = anyStr("outs")
var cns3339 = parseNum("1")
var cns3340 = anyStr("reg")
var cns3341 = anyStr("reginc")
var cns3342 = anyStr("outs")
var cns3343 = parseNum("1")
var cns3344 = anyStr("reg")
var cns3345 = anyStr("outs")
var cns3346 = anyStr("regs")
var cns3347 = anyStr("ipairs")
var cns3348 = anyStr("regs")
var cns3349 = anyStr("reg")
var cns3350 = anyStr("reg")
var cns3351 = parseNum("1")
var cns3352 = anyStr("inst")
var cns3353 = anyStr("inst")
function Function_transform (reg_0, reg_1) {
  var reg_3, reg_4, reg_6, reg_15, reg_20, reg_21, reg_22, reg_23, reg_24, reg_25, reg_26, reg_27, reg_29, reg_34, reg_39, reg_44, reg_47, reg_49, reg_54, reg_56, reg_57, reg_58, reg_66, reg_73, reg_74, reg_75, reg_80, reg_90, reg_95, reg_96, reg_103, reg_108, reg_111, reg_113, reg_114, reg_120, reg_125, reg_127, reg_128, reg_129, reg_133, reg_140, reg_149, reg_151, reg_156, reg_159, reg_161, reg_162, reg_168, reg_173, reg_175, reg_176, reg_177, reg_183, reg_184, reg_190, reg_192, reg_197, reg_208, reg_210, reg_211, reg_212, reg_213, reg_217, reg_223, reg_224, reg_232, reg_233, reg_237, reg_238, reg_246, reg_254, reg_255, reg_258, reg_259, reg_261, reg_262, reg_265, reg_266, reg_281, reg_296, reg_306, reg_307, reg_310, reg_311, reg_312, reg_313, reg_314, reg_315, reg_316, reg_317, reg_321, reg_328, reg_329, reg_332, reg_333, reg_335;
  var goto_78=false, goto_111=false, goto_120=false, goto_128=false, goto_179=false, goto_186=false, goto_233=false, goto_240=false, goto_253=false, goto_306=false, goto_368=false, goto_427=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_4 = next(reg_0);
  reg_6 = get(reg_4, cns3242);
  set(reg_4, cns3243, newTable());
  set(reg_4, cns3244, newTable());
  reg_3.b = length(get(reg_4, cns3245));
  reg_15 = anyFn((function (a) { return function$39(a,this) }).bind(reg_3));
  set(reg_1.a, cns3247, reg_15);
  reg_20 = get(reg_1.a, cns3248);
  reg_21 = newStack();
  push(reg_21, reg_6);
  reg_22 = call(reg_20, reg_21);
  reg_23 = next(reg_22);
  reg_24 = next(reg_22);
  reg_25 = next(reg_22);
  loop_1: while (true) {
    reg_26 = newStack();
    push(reg_26, reg_24);
    push(reg_26, reg_25);
    reg_27 = call(reg_23, reg_26);
    reg_25 = next(reg_27);
    reg_29 = next(reg_27);
    if (tobool(eq(reg_25, nil()))) break loop_1;
    reg_34 = get(reg_29, cns3249);
    goto_128 = !tobool(eq(reg_34, cns3250));
    if (!goto_128) {
      reg_39 = get(reg_29, cns3251);
      goto_78 = !tobool(get(reg_29, cns3252));
      if (!goto_78) {
        reg_44 = get(reg_4, cns3253);
        reg_47 = get(reg_44, get(reg_4, cns3254));
        reg_49 = get(reg_4, cns3255);
        reg_54 = get(get(reg_49, get(reg_29, cns3256)), cns3257);
        reg_56 = get(reg_4, cns3258);
        reg_57 = newStack();
        push(reg_57, reg_4);
        reg_58 = newTable();
        set(reg_58, cns3259, reg_54);
        set(reg_58, cns3260, reg_47);
        set(reg_58, cns3261, reg_39);
        push(reg_57, reg_58);
        reg_62 = call(reg_56, reg_57);
      }
      if ((goto_78 || false)) {
        goto_78 = false;
        reg_66 = eq(get(reg_39, cns3262), cns3263);
        if (tobool(reg_66)) {
          reg_66 = not(get(reg_39, cns3264));
        }
        goto_111 = !tobool(reg_66);
        if (!goto_111) {
          reg_73 = get(reg_4, cns3265);
          reg_74 = newStack();
          push(reg_74, reg_4);
          reg_75 = newTable();
          set(reg_75, cns3266, cns3267);
          set(reg_75, cns3268, reg_39);
          push(reg_74, reg_75);
          reg_79 = call(reg_73, reg_74);
          reg_80 = cns3269;
          set(reg_29, reg_80, first(call(get(reg_1.a, cns3270), newStack())));
        }
        if ((goto_111 || false)) {
          goto_111 = false;
          goto_120 = !tobool(get(reg_39, cns3271));
          if (!goto_120) {
            reg_90 = cns3272;
            set(reg_29, reg_90, get(reg_39, cns3273));
          }
          if ((goto_120 || false)) {
            goto_120 = false;
            reg_95 = get(reg_1.a, cns3274);
            reg_96 = newStack();
            push(reg_96, cns3275);
            reg_98 = call(reg_95, reg_96);
          }
        }
      }
    }
    if ((goto_128 || false)) {
      goto_128 = false;
      goto_186 = !tobool(eq(reg_34, cns3276));
      if (!goto_186) {
        reg_103 = get(reg_29, cns3277);
        goto_179 = !tobool(get(reg_103, cns3278));
        if (!goto_179) {
          reg_108 = get(reg_4, cns3279);
          reg_111 = get(reg_108, get(reg_103, cns3280));
          reg_113 = get(reg_4, cns3281);
          reg_114 = newStack();
          push(reg_114, reg_4);
          push(reg_114, get(reg_103, cns3282));
          reg_120 = get(first(call(reg_113, reg_114)), cns3283);
          reg_125 = get(get(reg_120, get(reg_103, cns3284)), cns3285);
          reg_127 = get(reg_4, cns3286);
          reg_128 = newStack();
          push(reg_128, reg_4);
          reg_129 = newTable();
          set(reg_129, cns3287, reg_125);
          set(reg_129, cns3288, reg_111);
          push(reg_128, reg_129);
          reg_132 = call(reg_127, reg_128);
          reg_133 = cns3289;
          set(reg_29, reg_133, first(call(get(reg_1.a, cns3290), newStack())));
        }
        if ((goto_179 || false)) {
          goto_179 = false;
          reg_140 = cns3291;
          set(reg_29, reg_140, get(get(reg_29, cns3292), cns3293));
        }
      }
      if ((goto_186 || false)) {
        goto_186 = false;
        goto_240 = !tobool(eq(reg_34, cns3294));
        if (!goto_240) {
          reg_149 = get(reg_29, cns3295);
          reg_151 = get(reg_29, cns3296);
          goto_233 = !tobool(get(reg_149, cns3297));
          if (!goto_233) {
            reg_156 = get(reg_4, cns3298);
            reg_159 = get(reg_156, get(reg_149, cns3299));
            reg_161 = get(reg_4, cns3300);
            reg_162 = newStack();
            push(reg_162, reg_4);
            push(reg_162, get(reg_149, cns3301));
            reg_168 = get(first(call(reg_161, reg_162)), cns3302);
            reg_173 = get(get(reg_168, get(reg_149, cns3303)), cns3304);
            reg_175 = get(reg_4, cns3305);
            reg_176 = newStack();
            push(reg_176, reg_4);
            reg_177 = newTable();
            set(reg_177, cns3306, reg_173);
            set(reg_177, cns3307, reg_159);
            set(reg_177, cns3308, reg_151);
            push(reg_176, reg_177);
            reg_181 = call(reg_175, reg_176);
          }
          if ((goto_233 || false)) {
            goto_233 = false;
            reg_183 = get(reg_4, cns3309);
            reg_184 = newStack();
            push(reg_184, reg_4);
            push(reg_184, reg_29);
            reg_185 = call(reg_183, reg_184);
          }
        }
        if ((goto_240 || false)) {
          goto_240 = false;
          goto_253 = !tobool(eq(reg_34, cns3310));
          if (!goto_253) {
            reg_190 = get(reg_4, cns3311);
            reg_192 = get(reg_29, cns3312);
            set(reg_190, reg_192, length(get(reg_4, cns3313)));
          }
          if ((goto_253 || false)) {
            goto_253 = false;
            reg_197 = eq(reg_34, cns3314);
            if (!tobool(reg_197)) {
              reg_197 = eq(reg_34, cns3315);
            }
            goto_306 = !tobool(reg_197);
            if (!goto_306) {
              if (tobool(not(get(get(reg_29, cns3316), cns3317)))) {
                reg_208 = cns3318;
                reg_210 = get(reg_4, cns3319);
                reg_211 = newStack();
                push(reg_211, reg_4);
                reg_212 = newTable();
                reg_213 = cns3320;
                set(reg_212, reg_213, get(reg_1.a, cns3321));
                reg_217 = cns3322;
                set(reg_212, reg_217, get(reg_29, cns3323));
                push(reg_211, reg_212);
                set(reg_29, reg_208, first(call(reg_210, reg_211)));
                reg_223 = get(reg_29, cns3324);
                reg_224 = cns3325;
                set(reg_223, reg_224, first(call(get(reg_1.a, cns3326), newStack())));
              }
              reg_232 = get(reg_4, cns3327);
              reg_233 = newStack();
              push(reg_233, reg_4);
              push(reg_233, reg_29);
              reg_234 = call(reg_232, reg_233);
            }
            if ((goto_306 || false)) {
              goto_306 = false;
              reg_237 = get(reg_1.a, cns3328);
              reg_238 = newStack();
              push(reg_238, reg_34);
              goto_427 = !tobool(eq(first(call(reg_237, reg_238)), cns3329));
              if (!goto_427) {
                reg_246 = sub(length(reg_29), cns3330);
                if (tobool(ne(reg_246, length(get(reg_34, cns3331))))) {
                  reg_254 = get(reg_1.a, cns3332);
                  reg_255 = newStack();
                  reg_258 = get(reg_1.a, cns3333);
                  reg_259 = newStack();
                  push(reg_259, reg_34);
                  reg_261 = first(call(reg_258, reg_259));
                  reg_262 = cns3334;
                  reg_265 = length(get(reg_34, cns3335));
                  reg_266 = cns3336;
                  push(reg_255, concat(reg_261, concat(reg_262, concat(reg_265, concat(reg_266, sub(length(reg_29), cns3337))))));
                  reg_274 = call(reg_254, reg_255);
                }
                goto_368 = !tobool(eq(length(get(reg_34, cns3338)), cns3339));
                if (!goto_368) {
                  reg_281 = cns3340;
                  set(reg_29, reg_281, first(call(get(reg_1.a, cns3341), newStack())));
                }
                if ((goto_368 || false)) {
                  goto_368 = false;
                  if (tobool(gt(length(get(reg_34, cns3342)), cns3343))) {
                    set(reg_29, cns3344, reg_3.b);
                    reg_296 = reg_3.b;
                    reg_3.b = add(reg_296, length(get(reg_34, cns3345)));
                  }
                }
                if (tobool(get(reg_29, cns3346))) {
                  reg_306 = get(reg_1.a, cns3347);
                  reg_307 = newStack();
                  push(reg_307, get(reg_29, cns3348));
                  reg_310 = call(reg_306, reg_307);
                  reg_311 = next(reg_310);
                  reg_312 = next(reg_310);
                  reg_313 = next(reg_310);
                  loop_2: while (true) {
                    reg_314 = newStack();
                    push(reg_314, reg_312);
                    push(reg_314, reg_313);
                    reg_315 = call(reg_311, reg_314);
                    reg_316 = next(reg_315);
                    reg_313 = reg_316;
                    reg_317 = next(reg_315);
                    if (tobool(eq(reg_313, nil()))) break loop_2;
                    reg_321 = cns3349;
                    set(reg_317, reg_321, sub(add(get(reg_29, cns3350), reg_316), cns3351));
                  }
                }
                reg_328 = get(reg_4, cns3352);
                reg_329 = newStack();
                push(reg_329, reg_4);
                push(reg_329, reg_29);
                reg_330 = call(reg_328, reg_329);
              }
              if ((goto_427 || false)) {
                goto_427 = false;
                reg_332 = get(reg_4, cns3353);
                reg_333 = newStack();
                push(reg_333, reg_4);
                push(reg_333, reg_29);
                reg_334 = call(reg_332, reg_333);
              }
            }
          }
        }
      }
    }
  }
  reg_335 = newStack();
  return reg_335;
}
var cns3354 = anyStr("Function")
var cns3355 = anyStr("generate_sourcemap")
var cns3356 = parseNum("1")
var cns3357 = anyStr("code")
var cns3358 = anyStr("ipairs")
var cns3359 = anyStr("code")
var cns3360 = anyStr("line")
var cns3361 = anyStr("table")
var cns3362 = anyStr("insert")
var cns3363 = parseNum("1")
var cns3364 = parseNum("2")
var cns3365 = anyStr("line")
var cns3366 = parseNum("3")
var cns3367 = anyStr("column")
var cns3368 = parseNum("1")
var cns3369 = anyStr("function")
var cns3370 = parseNum("2")
var cns3371 = anyStr("id")
var cns3372 = parseNum("3")
var cns3373 = anyStr("name")
var cns3374 = anyStr("table")
var cns3375 = anyStr("insert")
var cns3376 = parseNum("1")
var cns3377 = anyStr("name")
var cns3378 = parseNum("2")
var cns3379 = anyStr("name")
var cns3380 = anyStr("line")
var cns3381 = anyStr("table")
var cns3382 = anyStr("insert")
var cns3383 = parseNum("1")
var cns3384 = anyStr("line")
var cns3385 = parseNum("2")
var cns3386 = anyStr("line")
var cns3387 = anyStr("table")
var cns3388 = anyStr("insert")
var cns3389 = anyStr("sourcemap")
function Function_generate_sourcemap (reg_0, reg_1) {
  var reg_4, reg_5, reg_10, reg_11, reg_14, reg_15, reg_16, reg_17, reg_18, reg_19, reg_20, reg_21, reg_32, reg_33, reg_34, reg_36, reg_39, reg_43, reg_46, reg_48, reg_49, reg_60, reg_61, reg_62, reg_65, reg_76, reg_77, reg_78, reg_81, reg_89, reg_90, reg_95;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  set(reg_5, cns3356, cns3357);
  reg_10 = get(reg_1.a, cns3358);
  reg_11 = newStack();
  push(reg_11, get(reg_4, cns3359));
  reg_14 = call(reg_10, reg_11);
  reg_15 = next(reg_14);
  reg_16 = next(reg_14);
  reg_17 = next(reg_14);
  loop_1: while (true) {
    reg_18 = newStack();
    push(reg_18, reg_16);
    push(reg_18, reg_17);
    reg_19 = call(reg_15, reg_18);
    reg_20 = next(reg_19);
    reg_17 = reg_20;
    reg_21 = next(reg_19);
    if (tobool(eq(reg_17, nil()))) break loop_1;
    if (tobool(get(reg_21, cns3360))) {
      reg_32 = get(get(reg_1.a, cns3361), cns3362);
      reg_33 = newStack();
      push(reg_33, reg_5);
      reg_34 = newTable();
      set(reg_34, cns3363, reg_20);
      reg_36 = cns3364;
      set(reg_34, reg_36, get(reg_21, cns3365));
      reg_39 = cns3366;
      set(reg_34, reg_39, get(reg_21, cns3367));
      push(reg_33, reg_34);
      reg_42 = call(reg_32, reg_33);
    }
  }
  reg_43 = newTable();
  set(reg_43, cns3368, cns3369);
  reg_46 = cns3370;
  reg_48 = get(reg_4, cns3371);
  reg_49 = newStack();
  push(reg_49, reg_4);
  set(reg_43, reg_46, first(call(reg_48, reg_49)));
  set(reg_43, cns3372, reg_5);
  if (tobool(get(reg_4, cns3373))) {
    reg_60 = get(get(reg_1.a, cns3374), cns3375);
    reg_61 = newStack();
    push(reg_61, reg_43);
    reg_62 = newTable();
    set(reg_62, cns3376, cns3377);
    reg_65 = cns3378;
    set(reg_62, reg_65, get(reg_4, cns3379));
    push(reg_61, reg_62);
    reg_68 = call(reg_60, reg_61);
  }
  if (tobool(get(reg_4, cns3380))) {
    reg_76 = get(get(reg_1.a, cns3381), cns3382);
    reg_77 = newStack();
    push(reg_77, reg_43);
    reg_78 = newTable();
    set(reg_78, cns3383, cns3384);
    reg_81 = cns3385;
    set(reg_78, reg_81, get(reg_4, cns3386));
    push(reg_77, reg_78);
    reg_84 = call(reg_76, reg_77);
  }
  reg_89 = get(get(reg_1.a, cns3387), cns3388);
  reg_90 = newStack();
  push(reg_90, get(reg_1.a, cns3389));
  push(reg_90, reg_43);
  reg_94 = call(reg_89, reg_90);
  reg_95 = newStack();
  return reg_95;
}
var cns3390 = anyStr("create_compiler_state")
var cns3391 = anyStr("code")
var cns3392 = anyStr("lua_main")
var cns3393 = anyStr("lua_main")
var cns3394 = anyStr("lua_main")
var cns3395 = anyStr("ins")
var cns3396 = parseNum("1")
var cns3397 = anyStr("any_t")
var cns3398 = anyStr("id")
var cns3399 = anyStr("lua_main")
var cns3400 = anyStr("outs")
var cns3401 = parseNum("1")
var cns3402 = anyStr("stack_t")
var cns3403 = anyStr("id")
var cns3404 = anyStr("lua_main")
var cns3405 = anyStr("level")
var cns3406 = parseNum("1")
var cns3407 = anyStr("lua_main")
var cns3408 = anyStr("line")
var cns3409 = parseNum("1")
var cns3410 = anyStr("lua_main")
var cns3411 = anyStr("create_upval_info")
var cns3412 = anyStr("lua_main")
var cns3413 = anyStr("scope")
var cns3414 = anyStr("locals")
var cns3415 = anyStr("_ENV")
var cns3416 = anyStr("lua_main")
var cns3417 = anyStr("inst")
var cns3418 = parseNum("1")
var cns3419 = anyStr("local")
var cns3420 = parseNum("2")
var cns3421 = anyStr("reg")
var cns3422 = parseNum("0")
var cns3423 = anyStr("lua_main")
var cns3424 = anyStr("scope")
var cns3425 = anyStr("locals")
var cns3426 = anyStr(".ENV")
var cns3427 = anyStr("lua_main")
var cns3428 = anyStr("inst")
var cns3429 = parseNum("1")
var cns3430 = anyStr("local")
var cns3431 = parseNum("2")
var cns3432 = anyStr("reg")
var cns3433 = parseNum("0")
var cns3434 = anyStr("lua_main")
var cns3435 = anyStr("scope")
var cns3436 = anyStr("locals")
var cns3437 = anyStr("_AU_IMPORT")
var cns3438 = anyStr("au_type")
var cns3439 = anyStr("macro")
var cns3440 = anyStr("macro")
var cns3441 = anyStr("import")
var cns3442 = anyStr("lua_main")
var cns3443 = anyStr("scope")
var cns3444 = anyStr("locals")
var cns3445 = anyStr("_AU_FUNCTION")
var cns3446 = anyStr("au_type")
var cns3447 = anyStr("macro")
var cns3448 = anyStr("macro")
var cns3449 = anyStr("function")
var cns3450 = anyStr("lua_main")
var cns3451 = anyStr("compileBlock")
var cns3452 = parseNum("0")
var cns3453 = anyStr("type")
var cns3454 = anyStr("return")
var cns3455 = anyStr("lua_main")
var cns3456 = anyStr("call")
var cns3457 = anyStr("stack_f")
var cns3458 = anyStr("lua_main")
var cns3459 = anyStr("inst")
var cns3460 = parseNum("1")
var cns3461 = anyStr("end")
var cns3462 = parseNum("2")
var cns3463 = anyStr("lua_main")
var cns3464 = anyStr("build_upvals")
var cns3465 = anyStr("lua_main")
var cns3466 = anyStr("transform")
var cns3467 = anyStr("code")
var cns3468 = anyStr("main")
var cns3469 = anyStr("main")
var cns3470 = anyStr("main")
var cns3471 = anyStr("inst")
var cns3472 = parseNum("1")
var cns3473 = anyStr("global_f")
var cns3474 = anyStr("reg")
var cns3475 = parseNum("0")
var cns3476 = anyStr("main")
var cns3477 = anyStr("inst")
var cns3478 = parseNum("1")
var cns3479 = anyStr("lua_main")
var cns3480 = parseNum("2")
var cns3481 = anyStr("reg")
var cns3482 = parseNum("1")
var cns3483 = anyStr("main")
var cns3484 = anyStr("inst")
var cns3485 = parseNum("1")
var cns3486 = anyStr("end")
var cns3487 = anyStr("table")
var cns3488 = anyStr("insert")
var cns3489 = anyStr("sourcemap")
var cns3490 = parseNum("1")
var cns3491 = anyStr("file")
var cns3492 = parseNum("2")
var cns3493 = anyStr("ipairs")
var cns3494 = anyStr("funcs")
var cns3495 = anyStr("code")
var cns3496 = anyStr("generate_sourcemap")
var cns3497 = anyStr("table")
var cns3498 = anyStr("insert")
var cns3499 = anyStr("metadata")
var cns3500 = anyStr("sourcemap")
function function$40 (reg_0, reg_1) {
  var reg_4, reg_5, reg_13, reg_14, reg_17, reg_22, reg_23, reg_24, reg_25, reg_33, reg_34, reg_35, reg_36, reg_54, reg_56, reg_57, reg_65, reg_66, reg_69, reg_71, reg_72, reg_73, reg_76, reg_77, reg_88, reg_89, reg_92, reg_94, reg_95, reg_96, reg_99, reg_100, reg_111, reg_112, reg_113, reg_124, reg_125, reg_126, reg_133, reg_135, reg_136, reg_140, reg_151, reg_153, reg_154, reg_159, reg_162, reg_164, reg_165, reg_166, reg_173, reg_175, reg_176, reg_180, reg_182, reg_183, reg_187, reg_188, reg_191, reg_196, reg_198, reg_199, reg_200, reg_201, reg_208, reg_211, reg_213, reg_214, reg_215, reg_216, reg_226, reg_228, reg_229, reg_230, reg_239, reg_240, reg_244, reg_251, reg_252, reg_256, reg_257, reg_258, reg_259, reg_260, reg_261, reg_263, reg_271, reg_272, reg_278, reg_279, reg_287;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_10 = call(get(reg_1.a, cns3390), newStack());
  reg_13 = get(reg_1.a, cns3391);
  reg_14 = newStack();
  push(reg_14, cns3392);
  reg_17 = first(call(reg_13, reg_14));
  set(reg_1.a, cns3393, reg_17);
  reg_22 = get(reg_1.a, cns3394);
  reg_23 = cns3395;
  reg_24 = newTable();
  reg_25 = cns3396;
  set(reg_24, reg_25, get(get(reg_1.a, cns3397), cns3398));
  set(reg_22, reg_23, reg_24);
  reg_33 = get(reg_1.a, cns3399);
  reg_34 = cns3400;
  reg_35 = newTable();
  reg_36 = cns3401;
  set(reg_35, reg_36, get(get(reg_1.a, cns3402), cns3403));
  set(reg_33, reg_34, reg_35);
  set(get(reg_1.a, cns3404), cns3405, cns3406);
  set(get(reg_1.a, cns3407), cns3408, cns3409);
  reg_54 = get(reg_1.a, cns3410);
  reg_56 = get(reg_54, cns3411);
  reg_57 = newStack();
  push(reg_57, reg_54);
  reg_58 = call(reg_56, reg_57);
  reg_65 = get(get(get(reg_1.a, cns3412), cns3413), cns3414);
  reg_66 = cns3415;
  reg_69 = get(reg_1.a, cns3416);
  reg_71 = get(reg_69, cns3417);
  reg_72 = newStack();
  push(reg_72, reg_69);
  reg_73 = newTable();
  set(reg_73, cns3418, cns3419);
  reg_76 = cns3420;
  reg_77 = newTable();
  set(reg_77, cns3421, cns3422);
  set(reg_73, reg_76, reg_77);
  push(reg_72, reg_73);
  set(reg_65, reg_66, first(call(reg_71, reg_72)));
  reg_88 = get(get(get(reg_1.a, cns3423), cns3424), cns3425);
  reg_89 = cns3426;
  reg_92 = get(reg_1.a, cns3427);
  reg_94 = get(reg_92, cns3428);
  reg_95 = newStack();
  push(reg_95, reg_92);
  reg_96 = newTable();
  set(reg_96, cns3429, cns3430);
  reg_99 = cns3431;
  reg_100 = newTable();
  set(reg_100, cns3432, cns3433);
  set(reg_96, reg_99, reg_100);
  push(reg_95, reg_96);
  set(reg_88, reg_89, first(call(reg_94, reg_95)));
  reg_111 = get(get(get(reg_1.a, cns3434), cns3435), cns3436);
  reg_112 = cns3437;
  reg_113 = newTable();
  set(reg_113, cns3438, cns3439);
  set(reg_113, cns3440, cns3441);
  set(reg_111, reg_112, reg_113);
  reg_124 = get(get(get(reg_1.a, cns3442), cns3443), cns3444);
  reg_125 = cns3445;
  reg_126 = newTable();
  set(reg_126, cns3446, cns3447);
  set(reg_126, cns3448, cns3449);
  set(reg_124, reg_125, reg_126);
  reg_133 = get(reg_1.a, cns3450);
  reg_135 = get(reg_133, cns3451);
  reg_136 = newStack();
  push(reg_136, reg_133);
  push(reg_136, reg_4);
  reg_137 = call(reg_135, reg_136);
  reg_140 = eq(length(reg_4), cns3452);
  if (!tobool(reg_140)) {
    reg_140 = ne(get(get(reg_4, length(reg_4)), cns3453), cns3454);
  }
  if (tobool(reg_140)) {
    reg_151 = get(reg_1.a, cns3455);
    reg_153 = get(reg_151, cns3456);
    reg_154 = newStack();
    push(reg_154, reg_151);
    push(reg_154, get(reg_1.a, cns3457));
    reg_159 = first(call(reg_153, reg_154));
    reg_162 = get(reg_1.a, cns3458);
    reg_164 = get(reg_162, cns3459);
    reg_165 = newStack();
    push(reg_165, reg_162);
    reg_166 = newTable();
    set(reg_166, cns3460, cns3461);
    set(reg_166, cns3462, reg_159);
    push(reg_165, reg_166);
    reg_170 = call(reg_164, reg_165);
  }
  reg_173 = get(reg_1.a, cns3463);
  reg_175 = get(reg_173, cns3464);
  reg_176 = newStack();
  push(reg_176, reg_173);
  reg_177 = call(reg_175, reg_176);
  reg_180 = get(reg_1.a, cns3465);
  reg_182 = get(reg_180, cns3466);
  reg_183 = newStack();
  push(reg_183, reg_180);
  reg_184 = call(reg_182, reg_183);
  reg_187 = get(reg_1.a, cns3467);
  reg_188 = newStack();
  push(reg_188, cns3468);
  reg_191 = first(call(reg_187, reg_188));
  set(reg_1.a, cns3469, reg_191);
  reg_196 = get(reg_1.a, cns3470);
  reg_198 = get(reg_196, cns3471);
  reg_199 = newStack();
  push(reg_199, reg_196);
  reg_200 = newTable();
  reg_201 = cns3472;
  set(reg_200, reg_201, get(reg_1.a, cns3473));
  set(reg_200, cns3474, cns3475);
  push(reg_199, reg_200);
  reg_208 = first(call(reg_198, reg_199));
  reg_211 = get(reg_1.a, cns3476);
  reg_213 = get(reg_211, cns3477);
  reg_214 = newStack();
  push(reg_214, reg_211);
  reg_215 = newTable();
  reg_216 = cns3478;
  set(reg_215, reg_216, get(reg_1.a, cns3479));
  set(reg_215, cns3480, reg_208);
  set(reg_215, cns3481, cns3482);
  push(reg_214, reg_215);
  reg_223 = call(reg_213, reg_214);
  reg_226 = get(reg_1.a, cns3483);
  reg_228 = get(reg_226, cns3484);
  reg_229 = newStack();
  push(reg_229, reg_226);
  reg_230 = newTable();
  set(reg_230, cns3485, cns3486);
  push(reg_229, reg_230);
  reg_233 = call(reg_228, reg_229);
  if (tobool(reg_5)) {
    reg_239 = get(get(reg_1.a, cns3487), cns3488);
    reg_240 = newStack();
    push(reg_240, get(reg_1.a, cns3489));
    reg_244 = newTable();
    set(reg_244, cns3490, cns3491);
    set(reg_244, cns3492, reg_5);
    push(reg_240, reg_244);
    reg_248 = call(reg_239, reg_240);
  }
  reg_251 = get(reg_1.a, cns3493);
  reg_252 = newStack();
  push(reg_252, get(reg_1.a, cns3494));
  reg_256 = call(reg_251, reg_252);
  reg_257 = next(reg_256);
  reg_258 = next(reg_256);
  reg_259 = next(reg_256);
  loop_1: while (true) {
    reg_260 = newStack();
    push(reg_260, reg_258);
    push(reg_260, reg_259);
    reg_261 = call(reg_257, reg_260);
    reg_259 = next(reg_261);
    reg_263 = next(reg_261);
    if (tobool(eq(reg_259, nil()))) break loop_1;
    if (tobool(get(reg_263, cns3495))) {
      reg_271 = get(reg_263, cns3496);
      reg_272 = newStack();
      push(reg_272, reg_263);
      reg_273 = call(reg_271, reg_272);
    }
  }
  reg_278 = get(get(reg_1.a, cns3497), cns3498);
  reg_279 = newStack();
  push(reg_279, get(reg_1.a, cns3499));
  push(reg_279, get(reg_1.a, cns3500));
  reg_286 = call(reg_278, reg_279);
  reg_287 = newStack();
  return reg_287;
}
function aulua_codegen$lua_main (reg_0) {
  var reg_2, reg_8, reg_9, reg_14, reg_15, reg_20, reg_21, reg_26, reg_27, reg_32, reg_33, reg_38, reg_39, reg_44, reg_45, reg_50, reg_51, reg_56, reg_57, reg_62, reg_63, reg_68, reg_69, reg_74, reg_75, reg_79, reg_84, reg_85, reg_90, reg_91, reg_96, reg_97, reg_102, reg_103, reg_108, reg_109, reg_114, reg_115, reg_118;
  reg_2 = {a: nil()};
  reg_2.a = reg_0;
  reg_3 = aulua_helpers$lua_main(reg_0);
  reg_4 = aulua_basics$lua_main(reg_0);
  reg_5 = aulua_auro_syntax$lua_main(reg_0);
  reg_8 = get(reg_2.a, cns2077);
  reg_9 = cns2078;
  set(reg_8, reg_9, anyFn((function (a) { return Function_create_upval_info(a,this) }).bind(reg_2)));
  reg_14 = get(reg_2.a, cns2128);
  reg_15 = cns2129;
  set(reg_14, reg_15, anyFn((function (a) { return Function_build_upvals(a,this) }).bind(reg_2)));
  reg_20 = get(reg_2.a, cns2183);
  reg_21 = cns2184;
  set(reg_20, reg_21, anyFn((function (a) { return Function_get_vararg(a,this) }).bind(reg_2)));
  reg_26 = get(reg_2.a, cns2189);
  reg_27 = cns2190;
  set(reg_26, reg_27, anyFn((function (a) { return Function_get_local(a,this) }).bind(reg_2)));
  reg_32 = get(reg_2.a, cns2235);
  reg_33 = cns2236;
  set(reg_32, reg_33, anyFn((function (a) { return Function_get_level_ancestor(a,this) }).bind(reg_2)));
  reg_38 = get(reg_2.a, cns2241);
  reg_39 = cns2242;
  set(reg_38, reg_39, anyFn((function (a) { return Function_createFunction(a,this) }).bind(reg_2)));
  reg_44 = get(reg_2.a, cns2326);
  reg_45 = cns2327;
  set(reg_44, reg_45, anyFn((function (a) { return Function_compile_require(a,this) }).bind(reg_2)));
  reg_50 = get(reg_2.a, cns2366);
  reg_51 = cns2367;
  set(reg_50, reg_51, anyFn((function (a) { return Function_compile_call(a,this) }).bind(reg_2)));
  reg_56 = get(reg_2.a, cns2434);
  reg_57 = cns2435;
  set(reg_56, reg_57, anyFn((function (a) { return Function_compile_bool(a,this) }).bind(reg_2)));
  reg_62 = get(reg_2.a, cns2454);
  reg_63 = cns2455;
  set(reg_62, reg_63, anyFn((function (a) { return Function_compileExpr(a,this) }).bind(reg_2)));
  reg_68 = get(reg_2.a, cns2663);
  reg_69 = cns2664;
  set(reg_68, reg_69, anyFn((function (a) { return Function_assign(a,this) }).bind(reg_2)));
  reg_74 = get(reg_2.a, cns2769);
  reg_75 = cns2770;
  set(reg_74, reg_75, anyFn((function (a) { return Function_compileLhs(a,this) }).bind(reg_2)));
  reg_79 = anyFn((function (a) { return aulua_codegen$function(a,this) }).bind(reg_2));
  set(reg_2.a, cns2834, reg_79);
  reg_84 = get(reg_2.a, cns2835);
  reg_85 = cns2836;
  set(reg_84, reg_85, anyFn((function (a) { return Function_compile_numfor(a,this) }).bind(reg_2)));
  reg_90 = get(reg_2.a, cns2951);
  reg_91 = cns2952;
  set(reg_90, reg_91, anyFn((function (a) { return Function_compile_genfor(a,this) }).bind(reg_2)));
  reg_96 = get(reg_2.a, cns3047);
  reg_97 = cns3048;
  set(reg_96, reg_97, anyFn((function (a) { return Function_compileStmt(a,this) }).bind(reg_2)));
  reg_102 = get(reg_2.a, cns3236);
  reg_103 = cns3237;
  set(reg_102, reg_103, anyFn((function (a) { return Function_compileBlock(a,this) }).bind(reg_2)));
  reg_108 = get(reg_2.a, cns3240);
  reg_109 = cns3241;
  set(reg_108, reg_109, anyFn((function (a) { return Function_transform(a,this) }).bind(reg_2)));
  reg_114 = get(reg_2.a, cns3354);
  reg_115 = cns3355;
  set(reg_114, reg_115, anyFn((function (a) { return Function_generate_sourcemap(a,this) }).bind(reg_2)));
  reg_118 = newStack();
  push(reg_118, anyFn((function (a) { return function$40(a,this) }).bind(reg_2)));
  return reg_118;
}
var record_44 = function (val) { this.val = val; }
var record_45 = function (val) { this.val = val; }
var cns3501 = anyStr("ipairs")
var cns3502 = parseNum("1")
function function$41 (reg_0, reg_1) {
  var reg_2, reg_7, reg_8, reg_9, reg_12, reg_13, reg_14, reg_15, reg_16, reg_17, reg_19, reg_23, reg_24, reg_26;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_7 = get(reg_2.a, cns3501);
  reg_8 = newStack();
  reg_9 = newTable();
  table_append(reg_9, cns3502, copy(reg_0));
  push(reg_8, reg_9);
  reg_12 = call(reg_7, reg_8);
  reg_13 = next(reg_12);
  reg_14 = next(reg_12);
  reg_15 = next(reg_12);
  loop_1: while (true) {
    reg_16 = newStack();
    push(reg_16, reg_14);
    push(reg_16, reg_15);
    reg_17 = call(reg_13, reg_16);
    reg_15 = next(reg_17);
    reg_19 = next(reg_17);
    if (tobool(eq(reg_15, nil()))) break loop_1;
    reg_23 = reg_1.b;
    reg_24 = newStack();
    push(reg_24, reg_19);
    reg_25 = call(reg_23, reg_24);
  }
  reg_26 = newStack();
  return reg_26;
}
var record_46 = function (val) { this.val = val; }
var record_47 = function (val) { this.val = val; }
var cns3503 = parseNum("0")
var cns3504 = parseNum("128")
function idiv (reg_0, reg_1) {
  var reg_2, reg_5, reg_6, reg_7, reg_8, reg_9, reg_17, reg_20, reg_21, reg_22, reg_24, reg_25, reg_26, reg_29, reg_31;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_5 = reg_0.val;
    reg_6 = reg_1.val;
    reg_7 = ((reg_5 / reg_6) | 0);
    reg_9 = (reg_7 > 0);
    if (!reg_9) {
      reg_9 = ((reg_5 * reg_6) > 0);
    }
    reg_8 = reg_9;
    if (!reg_8) {
      reg_8 = ((reg_7 * reg_6) == reg_5);
    }
    if (reg_8) {
      reg_17 = new Integer(reg_7);
      return reg_17;
    }
    reg_20 = new Integer((reg_7 - 1));
    return reg_20;
  }
  var _r = getFloats(reg_0, reg_1); reg_24 = _r[0]; reg_25 = _r[1]; reg_26 = _r[2];
  reg_21 = reg_24;
  reg_22 = reg_25;
  if (reg_26) {
    reg_29 = Math.floor((reg_21 / reg_22));
    return reg_29;
  }
  reg_31 = meta_arith(reg_0, reg_1, "__idiv");
  return reg_31;
}
var cns3505 = parseNum("128")
var cns3506 = parseNum("128")
function function$43 (reg_0, reg_1) {
  var reg_2, reg_6, reg_11, reg_12, reg_13, reg_15, reg_16, reg_23;
  reg_2 = reg_1.a;
  reg_3 = reg_2.a;
  reg_4 = nil();
  reg_5 = {a: reg_1};
  reg_6 = next(reg_0);
  if (tobool(gt(reg_6, cns3503))) {
    reg_11 = idiv(reg_6, cns3504);
    reg_12 = reg_1.b;
    reg_13 = newStack();
    push(reg_13, reg_11);
    reg_14 = call(reg_12, reg_13);
    reg_15 = reg_2.c;
    reg_16 = newStack();
    push(reg_16, add(sub(reg_6, mul(reg_11, cns3505)), cns3506));
    reg_22 = call(reg_15, reg_16);
  }
  reg_23 = newStack();
  return reg_23;
}
var cns3507 = parseNum("128")
var cns3508 = parseNum("128")
function function$42 (reg_0, reg_1) {
  var reg_4, reg_5, reg_9, reg_10, reg_11, reg_13, reg_14, reg_19;
  reg_2 = reg_1.a;
  reg_4 = {a: reg_1, b: nil()};
  reg_5 = next(reg_0);
  reg_4.b = anyFn((function (a) { return function$43(a,this) }).bind(reg_4));
  reg_9 = idiv(reg_5, cns3507);
  reg_10 = reg_4.b;
  reg_11 = newStack();
  push(reg_11, reg_9);
  reg_12 = call(reg_10, reg_11);
  reg_13 = reg_1.c;
  reg_14 = newStack();
  push(reg_14, sub(reg_5, mul(reg_9, cns3508)));
  reg_18 = call(reg_13, reg_14);
  reg_19 = newStack();
  return reg_19;
}
var cns3509 = anyStr("byte")
var cns3510 = parseNum("1")
var cns3511 = parseNum("1")
function function$44 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_9, reg_10, reg_16;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = reg_1.c;
  reg_7 = newStack();
  reg_9 = get(reg_5, cns3509);
  reg_10 = newStack();
  push(reg_10, reg_5);
  push(reg_10, cns3510);
  push(reg_10, unm(cns3511));
  append(reg_7, call(reg_9, reg_10));
  reg_15 = call(reg_6, reg_7);
  reg_16 = newStack();
  return reg_16;
}
function function$45 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_10, reg_11, reg_13;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = reg_1.d;
  reg_7 = newStack();
  push(reg_7, length(reg_5));
  reg_9 = call(reg_6, reg_7);
  reg_10 = reg_1.e;
  reg_11 = newStack();
  push(reg_11, reg_5);
  reg_12 = call(reg_10, reg_11);
  reg_13 = newStack();
  return reg_13;
}
var cns3512 = anyStr("Auro 0.6\x00")
var cns3513 = anyStr("modules")
var cns3514 = parseNum("1")
var cns3515 = parseNum("2")
var cns3516 = parseNum("2")
var cns3517 = parseNum("2")
var cns3518 = anyStr("lua_main")
var cns3519 = anyStr("id")
var cns3520 = anyStr("lua_main")
var cns3521 = parseNum("2")
var cns3522 = anyStr("main")
var cns3523 = anyStr("id")
var cns3524 = anyStr("main")
var cns3525 = anyStr("ipairs")
var cns3526 = anyStr("modules")
var cns3527 = anyStr("items")
var cns3528 = parseNum("2")
var cns3529 = anyStr("items")
var cns3530 = anyStr("ipairs")
var cns3531 = anyStr("items")
var cns3532 = anyStr("fn")
var cns3533 = parseNum("2")
var cns3534 = anyStr("fn")
var cns3535 = anyStr("id")
var cns3536 = anyStr("tp")
var cns3537 = parseNum("1")
var cns3538 = anyStr("tp")
var cns3539 = anyStr("id")
var cns3540 = anyStr("err")
var cns3541 = anyStr("Unknown item kind for ")
var cns3542 = anyStr("tostr")
var cns3543 = anyStr("name")
var cns3544 = anyStr("base")
var cns3545 = anyStr("argument")
var cns3546 = parseNum("4")
var cns3547 = anyStr("base")
var cns3548 = anyStr("id")
var cns3549 = anyStr("argument")
var cns3550 = anyStr("id")
var cns3551 = anyStr("from")
var cns3552 = parseNum("3")
var cns3553 = anyStr("from")
var cns3554 = anyStr("id")
var cns3555 = anyStr("name")
var cns3556 = parseNum("1")
var cns3557 = anyStr("name")
var cns3558 = anyStr("types")
var cns3559 = anyStr("ipairs")
var cns3560 = anyStr("types")
var cns3561 = anyStr("module")
var cns3562 = parseNum("1")
var cns3563 = anyStr("name")
var cns3564 = anyStr("funcs")
var cns3565 = anyStr("ipairs")
var cns3566 = anyStr("funcs")
var cns3567 = anyStr("code")
var cns3568 = parseNum("1")
var cns3569 = anyStr("module")
var cns3570 = anyStr("module")
var cns3571 = parseNum("2")
var cns3572 = anyStr("error")
var cns3573 = anyStr("???")
var cns3574 = anyStr("ins")
var cns3575 = anyStr("ipairs")
var cns3576 = anyStr("ins")
var cns3577 = anyStr("outs")
var cns3578 = anyStr("ipairs")
var cns3579 = anyStr("outs")
var cns3580 = anyStr("module")
var cns3581 = anyStr("name")
var cns3582 = anyStr("constants")
var cns3583 = anyStr("ipairs")
var cns3584 = anyStr("constants")
var cns3585 = anyStr("type")
var cns3586 = anyStr("int")
var cns3587 = parseNum("1")
var cns3588 = anyStr("value")
var cns3589 = anyStr("type")
var cns3590 = anyStr("bin")
var cns3591 = parseNum("2")
var cns3592 = anyStr("value")
var cns3593 = anyStr("type")
var cns3594 = anyStr("call")
var cns3595 = anyStr("args")
var cns3596 = anyStr("f")
var cns3597 = anyStr("ins")
var cns3598 = anyStr("error")
var cns3599 = anyStr("f")
var cns3600 = anyStr("name")
var cns3601 = anyStr(" expects ")
var cns3602 = anyStr("f")
var cns3603 = anyStr("ins")
var cns3604 = anyStr(" arguments, but got ")
var cns3605 = anyStr("args")
var cns3606 = anyStr("f")
var cns3607 = anyStr("id")
var cns3608 = parseNum("16")
var cns3609 = anyStr("ipairs")
var cns3610 = anyStr("args")
var cns3611 = anyStr("id")
var cns3612 = anyStr("error")
var cns3613 = anyStr("constant ")
var cns3614 = anyStr("type")
var cns3615 = anyStr(" not supported")
var cns3616 = parseNum("0")
var cns3617 = anyStr("regs")
var cns3618 = anyStr("labels")
var cns3619 = anyStr("error")
var cns3620 = anyStr("no visible label '")
var cns3621 = anyStr("' for <goto> at line ")
function function$47 (reg_0, reg_1) {
  var reg_3, reg_6, reg_7, reg_11, reg_16, reg_17, reg_18, reg_24;
  reg_3 = reg_1.a.a;
  reg_4 = nil();
  reg_5 = {a: reg_1};
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_11 = get(get(reg_1.b, cns3618), reg_6);
  if (tobool(not(reg_11))) {
    reg_16 = get(reg_3.a, cns3619);
    reg_17 = newStack();
    reg_18 = cns3620;
    push(reg_17, concat(reg_18, concat(reg_6, concat(cns3621, reg_7))));
    reg_23 = call(reg_16, reg_17);
  }
  reg_24 = newStack();
  push(reg_24, reg_11);
  return reg_24;
}
var cns3622 = anyStr("code")
var cns3623 = anyStr("ipairs")
var cns3624 = anyStr("code")
var cns3625 = parseNum("1")
var cns3626 = anyStr("end")
var cns3627 = parseNum("1")
var cns3628 = anyStr("outs")
var cns3629 = anyStr("error")
var cns3630 = anyStr("name")
var cns3631 = anyStr(" outputs ")
var cns3632 = anyStr("outs")
var cns3633 = anyStr(" results, but end instrucion has ")
var cns3634 = parseNum("1")
var cns3635 = parseNum("0")
var cns3636 = parseNum("2")
var cns3637 = parseNum("1")
var cns3638 = parseNum("0")
var cns3639 = anyStr("reg")
var cns3640 = anyStr("dup")
var cns3641 = parseNum("3")
var cns3642 = parseNum("2")
var cns3643 = anyStr("reg")
var cns3644 = anyStr("reg")
var cns3645 = parseNum("1")
var cns3646 = anyStr("set")
var cns3647 = parseNum("4")
var cns3648 = parseNum("2")
var cns3649 = anyStr("reg")
var cns3650 = parseNum("3")
var cns3651 = anyStr("reg")
var cns3652 = anyStr("jmp")
var cns3653 = parseNum("5")
var cns3654 = parseNum("2")
var cns3655 = anyStr("line")
var cns3656 = anyStr("jif")
var cns3657 = parseNum("6")
var cns3658 = parseNum("2")
var cns3659 = parseNum("3")
var cns3660 = anyStr("reg")
var cns3661 = anyStr("nif")
var cns3662 = parseNum("7")
var cns3663 = parseNum("2")
var cns3664 = parseNum("3")
var cns3665 = anyStr("reg")
var cns3666 = anyStr("type")
var cns3667 = anyStr("table")
var cns3668 = anyStr("id")
var cns3669 = parseNum("16")
var cns3670 = parseNum("2")
var cns3671 = parseNum("1")
var cns3672 = parseNum("0")
var cns3673 = anyStr("reg")
var cns3674 = anyStr("error")
var cns3675 = anyStr("Unsupported instruction: ")
var cns3676 = anyStr("tostring")
function function$46 (reg_0, reg_1) {
  var reg_2, reg_4, reg_11, reg_13, reg_14, reg_15, reg_23, reg_24, reg_28, reg_29, reg_30, reg_31, reg_32, reg_33, reg_35, reg_40, reg_46, reg_55, reg_56, reg_59, reg_60, reg_64, reg_65, reg_74, reg_75, reg_78, reg_79, reg_80, reg_82, reg_88, reg_89, reg_98, reg_99, reg_102, reg_103, reg_115, reg_116, reg_119, reg_120, reg_126, reg_127, reg_136, reg_137, reg_140, reg_141, reg_142, reg_152, reg_153, reg_156, reg_157, reg_158, reg_163, reg_164, reg_173, reg_174, reg_177, reg_178, reg_179, reg_184, reg_185, reg_193, reg_194, reg_200, reg_201, reg_203, reg_204, reg_210, reg_211, reg_212, reg_214, reg_220, reg_221, reg_229, reg_230, reg_231, reg_234, reg_235, reg_240;
  var goto_99=false, goto_113=false, goto_136=false, goto_162=false, goto_184=false, goto_211=false, goto_238=false, goto_272=false, goto_286=false;
  reg_2 = reg_1.a;
  reg_4 = {a: reg_1, b: nil()};
  reg_4.b = next(reg_0);
  reg_6 = newTable();
  reg_7 = cns3616;
  reg_11 = length(get(reg_4.b, cns3617));
  reg_13 = anyFn((function (a) { return function$47(a,this) }).bind(reg_4));
  reg_14 = reg_1.d;
  reg_15 = newStack();
  push(reg_15, length(get(reg_4.b, cns3622)));
  reg_20 = call(reg_14, reg_15);
  reg_23 = get(reg_2.a, cns3623);
  reg_24 = newStack();
  push(reg_24, get(reg_4.b, cns3624));
  reg_28 = call(reg_23, reg_24);
  reg_29 = next(reg_28);
  reg_30 = next(reg_28);
  reg_31 = next(reg_28);
  loop_1: while (true) {
    reg_32 = newStack();
    push(reg_32, reg_30);
    push(reg_32, reg_31);
    reg_33 = call(reg_29, reg_32);
    reg_31 = next(reg_33);
    reg_35 = next(reg_33);
    if (tobool(eq(reg_31, nil()))) break loop_1;
    reg_40 = get(reg_35, cns3625);
    goto_113 = !tobool(eq(reg_40, cns3626));
    if (!goto_113) {
      reg_46 = sub(length(reg_35), cns3627);
      if (tobool(ne(reg_46, length(get(reg_4.b, cns3628))))) {
        reg_55 = get(reg_2.a, cns3629);
        reg_56 = newStack();
        reg_59 = get(reg_4.b, cns3630);
        reg_60 = cns3631;
        reg_64 = length(get(reg_4.b, cns3632));
        reg_65 = cns3633;
        push(reg_56, concat(reg_59, concat(reg_60, concat(reg_64, concat(reg_65, sub(length(reg_35), cns3634))))));
        reg_73 = call(reg_55, reg_56);
      }
      reg_74 = reg_1.c;
      reg_75 = newStack();
      push(reg_75, cns3635);
      reg_77 = call(reg_74, reg_75);
      reg_78 = cns3636;
      reg_79 = length(reg_35);
      reg_80 = cns3637;
      reg_82 = lt(reg_80, cns3638);
      loop_3: while (true) {
        goto_99 = tobool(reg_82);
        if (!goto_99) {
          if (tobool(gt(reg_78, reg_79))) break loop_3;
        }
        if ((goto_99 || false)) {
          goto_99 = false;
          if (tobool(lt(reg_78, reg_79))) break loop_3;
        }
        reg_88 = reg_1.d;
        reg_89 = newStack();
        push(reg_89, get(get(reg_35, reg_78), cns3639));
        reg_93 = call(reg_88, reg_89);
        reg_78 = add(reg_78, reg_80);
      }
    }
    if ((goto_113 || false)) {
      goto_113 = false;
      goto_136 = !tobool(eq(reg_40, cns3640));
      if (!goto_136) {
        reg_98 = reg_1.c;
        reg_99 = newStack();
        push(reg_99, cns3641);
        reg_101 = call(reg_98, reg_99);
        reg_102 = reg_1.d;
        reg_103 = newStack();
        push(reg_103, get(get(reg_35, cns3642), cns3643));
        reg_108 = call(reg_102, reg_103);
        set(reg_35, cns3644, reg_11);
        reg_11 = add(reg_11, cns3645);
      }
      if ((goto_136 || false)) {
        goto_136 = false;
        goto_162 = !tobool(eq(reg_40, cns3646));
        if (!goto_162) {
          reg_115 = reg_1.c;
          reg_116 = newStack();
          push(reg_116, cns3647);
          reg_118 = call(reg_115, reg_116);
          reg_119 = reg_1.d;
          reg_120 = newStack();
          push(reg_120, get(get(reg_35, cns3648), cns3649));
          reg_125 = call(reg_119, reg_120);
          reg_126 = reg_1.d;
          reg_127 = newStack();
          push(reg_127, get(get(reg_35, cns3650), cns3651));
          reg_132 = call(reg_126, reg_127);
        }
        if ((goto_162 || false)) {
          goto_162 = false;
          goto_184 = !tobool(eq(reg_40, cns3652));
          if (!goto_184) {
            reg_136 = reg_1.c;
            reg_137 = newStack();
            push(reg_137, cns3653);
            reg_139 = call(reg_136, reg_137);
            reg_140 = reg_1.d;
            reg_141 = newStack();
            reg_142 = newStack();
            push(reg_142, get(reg_35, cns3654));
            push(reg_142, get(reg_35, cns3655));
            append(reg_141, call(reg_13, reg_142));
            reg_148 = call(reg_140, reg_141);
          }
          if ((goto_184 || false)) {
            goto_184 = false;
            goto_211 = !tobool(eq(reg_40, cns3656));
            if (!goto_211) {
              reg_152 = reg_1.c;
              reg_153 = newStack();
              push(reg_153, cns3657);
              reg_155 = call(reg_152, reg_153);
              reg_156 = reg_1.d;
              reg_157 = newStack();
              reg_158 = newStack();
              push(reg_158, get(reg_35, cns3658));
              append(reg_157, call(reg_13, reg_158));
              reg_162 = call(reg_156, reg_157);
              reg_163 = reg_1.d;
              reg_164 = newStack();
              push(reg_164, get(get(reg_35, cns3659), cns3660));
              reg_169 = call(reg_163, reg_164);
            }
            if ((goto_211 || false)) {
              goto_211 = false;
              goto_238 = !tobool(eq(reg_40, cns3661));
              if (!goto_238) {
                reg_173 = reg_1.c;
                reg_174 = newStack();
                push(reg_174, cns3662);
                reg_176 = call(reg_173, reg_174);
                reg_177 = reg_1.d;
                reg_178 = newStack();
                reg_179 = newStack();
                push(reg_179, get(reg_35, cns3663));
                append(reg_178, call(reg_13, reg_179));
                reg_183 = call(reg_177, reg_178);
                reg_184 = reg_1.d;
                reg_185 = newStack();
                push(reg_185, get(get(reg_35, cns3664), cns3665));
                reg_190 = call(reg_184, reg_185);
              }
              if ((goto_238 || false)) {
                goto_238 = false;
                reg_193 = get(reg_2.a, cns3666);
                reg_194 = newStack();
                push(reg_194, reg_40);
                goto_286 = !tobool(eq(first(call(reg_193, reg_194)), cns3667));
                if (!goto_286) {
                  reg_200 = reg_1.d;
                  reg_201 = newStack();
                  reg_203 = get(reg_40, cns3668);
                  reg_204 = newStack();
                  push(reg_204, reg_40);
                  push(reg_201, add(first(call(reg_203, reg_204)), cns3669));
                  reg_209 = call(reg_200, reg_201);
                  reg_210 = cns3670;
                  reg_211 = length(reg_35);
                  reg_212 = cns3671;
                  reg_214 = lt(reg_212, cns3672);
                  loop_2: while (true) {
                    goto_272 = tobool(reg_214);
                    if (!goto_272) {
                      if (tobool(gt(reg_210, reg_211))) break loop_2;
                    }
                    if ((goto_272 || false)) {
                      goto_272 = false;
                      if (tobool(lt(reg_210, reg_211))) break loop_2;
                    }
                    reg_220 = reg_1.d;
                    reg_221 = newStack();
                    push(reg_221, get(get(reg_35, reg_210), cns3673));
                    reg_225 = call(reg_220, reg_221);
                    reg_210 = add(reg_210, reg_212);
                  }
                }
                if ((goto_286 || false)) {
                  goto_286 = false;
                  reg_229 = get(reg_2.a, cns3674);
                  reg_230 = newStack();
                  reg_231 = cns3675;
                  reg_234 = get(reg_2.a, cns3676);
                  reg_235 = newStack();
                  push(reg_235, reg_40);
                  push(reg_230, concat(reg_231, first(call(reg_234, reg_235))));
                  reg_239 = call(reg_229, reg_230);
                }
              }
            }
          }
        }
      }
    }
  }
  reg_240 = newStack();
  return reg_240;
}
var cns3677 = anyStr("ipairs")
var cns3678 = anyStr("funcs")
var cns3679 = anyStr("code")
var cns3680 = anyStr("type")
var cns3681 = anyStr("number")
var cns3682 = parseNum("2")
var cns3683 = parseNum("1")
var cns3684 = anyStr("type")
var cns3685 = anyStr("string")
var cns3686 = parseNum("4")
var cns3687 = parseNum("2")
var cns3688 = anyStr("type")
var cns3689 = anyStr("table")
var cns3690 = parseNum("4")
var cns3691 = parseNum("1")
var cns3692 = parseNum("1")
var cns3693 = parseNum("0")
var cns3694 = anyStr("write_node")
var cns3695 = anyStr("error")
var cns3696 = anyStr("wtf: ")
var cns3697 = anyStr("tostring")
function function$48 (reg_0, reg_1) {
  var reg_2, reg_5, reg_8, reg_9, reg_15, reg_16, reg_24, reg_25, reg_31, reg_32, reg_39, reg_40, reg_44, reg_45, reg_51, reg_52, reg_57, reg_58, reg_59, reg_61, reg_69, reg_70, reg_76, reg_77, reg_78, reg_81, reg_82, reg_87;
  var goto_24=false, goto_49=false, goto_78=false, goto_92=false;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_8 = get(reg_2.a, cns3680);
  reg_9 = newStack();
  push(reg_9, reg_5);
  goto_24 = !tobool(eq(first(call(reg_8, reg_9)), cns3681));
  if (!goto_24) {
    reg_15 = reg_1.d;
    reg_16 = newStack();
    push(reg_16, add(mul(reg_5, cns3682), cns3683));
    reg_21 = call(reg_15, reg_16);
  }
  if ((goto_24 || false)) {
    goto_24 = false;
    reg_24 = get(reg_2.a, cns3684);
    reg_25 = newStack();
    push(reg_25, reg_5);
    goto_49 = !tobool(eq(first(call(reg_24, reg_25)), cns3685));
    if (!goto_49) {
      reg_31 = reg_1.d;
      reg_32 = newStack();
      push(reg_32, add(mul(length(reg_5), cns3686), cns3687));
      reg_38 = call(reg_31, reg_32);
      reg_39 = reg_1.e;
      reg_40 = newStack();
      push(reg_40, reg_5);
      reg_41 = call(reg_39, reg_40);
    }
    if ((goto_49 || false)) {
      goto_49 = false;
      reg_44 = get(reg_2.a, cns3688);
      reg_45 = newStack();
      push(reg_45, reg_5);
      goto_92 = !tobool(eq(first(call(reg_44, reg_45)), cns3689));
      if (!goto_92) {
        reg_51 = reg_1.d;
        reg_52 = newStack();
        push(reg_52, mul(length(reg_5), cns3690));
        reg_56 = call(reg_51, reg_52);
        reg_57 = cns3691;
        reg_58 = length(reg_5);
        reg_59 = cns3692;
        reg_61 = lt(reg_59, cns3693);
        loop_1: while (true) {
          goto_78 = tobool(reg_61);
          if (!goto_78) {
            if (tobool(gt(reg_57, reg_58))) break loop_1;
          }
          if ((goto_78 || false)) {
            goto_78 = false;
            if (tobool(lt(reg_57, reg_58))) break loop_1;
          }
          reg_69 = get(reg_2.a, cns3694);
          reg_70 = newStack();
          push(reg_70, get(reg_5, reg_57));
          reg_72 = call(reg_69, reg_70);
          reg_57 = add(reg_57, reg_59);
        }
      }
      if ((goto_92 || false)) {
        goto_92 = false;
        reg_76 = get(reg_2.a, cns3695);
        reg_77 = newStack();
        reg_78 = cns3696;
        reg_81 = get(reg_2.a, cns3697);
        reg_82 = newStack();
        push(reg_82, reg_5);
        push(reg_77, concat(reg_78, first(call(reg_81, reg_82))));
        reg_86 = call(reg_76, reg_77);
      }
    }
  }
  reg_87 = newStack();
  return reg_87;
}
var cns3698 = anyStr("write_node")
var cns3699 = anyStr("write_node")
var cns3700 = anyStr("metadata")
function aulua_write$function (reg_0, reg_1) {
  var reg_2, reg_3, reg_12, reg_13, reg_14, reg_17, reg_18, reg_26, reg_27, reg_31, reg_32, reg_35, reg_36, reg_39, reg_41, reg_42, reg_45, reg_48, reg_49, reg_52, reg_53, reg_56, reg_58, reg_59, reg_62, reg_67, reg_68, reg_72, reg_73, reg_74, reg_75, reg_76, reg_77, reg_79, reg_86, reg_87, reg_90, reg_91, reg_98, reg_99, reg_102, reg_103, reg_104, reg_105, reg_106, reg_107, reg_109, reg_116, reg_117, reg_120, reg_121, reg_123, reg_125, reg_126, reg_132, reg_133, reg_136, reg_137, reg_145, reg_146, reg_147, reg_150, reg_151, reg_156, reg_161, reg_166, reg_167, reg_170, reg_171, reg_177, reg_178, reg_187, reg_188, reg_191, reg_192, reg_198, reg_202, reg_203, reg_206, reg_210, reg_211, reg_219, reg_220, reg_224, reg_225, reg_226, reg_227, reg_228, reg_229, reg_231, reg_235, reg_236, reg_242, reg_246, reg_247, reg_255, reg_256, reg_260, reg_261, reg_262, reg_263, reg_264, reg_265, reg_267, reg_274, reg_275, reg_281, reg_282, reg_290, reg_291, reg_294, reg_295, reg_302, reg_303, reg_306, reg_307, reg_308, reg_309, reg_310, reg_311, reg_313, reg_317, reg_318, reg_320, reg_321, reg_328, reg_329, reg_332, reg_333, reg_334, reg_335, reg_336, reg_337, reg_339, reg_343, reg_344, reg_349, reg_353, reg_354, reg_362, reg_363, reg_367, reg_368, reg_369, reg_370, reg_371, reg_372, reg_374, reg_383, reg_384, reg_387, reg_388, reg_397, reg_398, reg_401, reg_412, reg_422, reg_423, reg_427, reg_428, reg_433, reg_434, reg_443, reg_444, reg_446, reg_448, reg_449, reg_457, reg_458, reg_461, reg_462, reg_463, reg_464, reg_465, reg_466, reg_468, reg_472, reg_473, reg_475, reg_476, reg_481, reg_482, reg_483, reg_491, reg_494, reg_495, reg_499, reg_500, reg_501, reg_502, reg_503, reg_504, reg_506, reg_513, reg_516, reg_521, reg_522, reg_527;
  var goto_161=false, goto_179=false, goto_201=false, goto_232=false, goto_255=false, goto_352=false, goto_365=false, goto_500=false, goto_517=false, goto_606=false;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2, d: reg_2, e: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = anyFn((function (a) { return function$41(a,this) }).bind(reg_3));
  reg_3.d = anyFn((function (a) { return function$42(a,this) }).bind(reg_3));
  reg_3.e = anyFn((function (a) { return function$44(a,this) }).bind(reg_3));
  reg_12 = anyFn((function (a) { return function$45(a,this) }).bind(reg_3));
  reg_13 = reg_3.e;
  reg_14 = newStack();
  push(reg_14, cns3512);
  reg_16 = call(reg_13, reg_14);
  reg_17 = reg_3.d;
  reg_18 = newStack();
  push(reg_18, add(length(get(reg_1.a, cns3513)), cns3514));
  reg_25 = call(reg_17, reg_18);
  reg_26 = reg_3.c;
  reg_27 = newStack();
  push(reg_27, cns3515);
  push(reg_27, cns3516);
  reg_30 = call(reg_26, reg_27);
  reg_31 = reg_3.c;
  reg_32 = newStack();
  push(reg_32, cns3517);
  reg_34 = call(reg_31, reg_32);
  reg_35 = reg_3.d;
  reg_36 = newStack();
  reg_39 = get(reg_1.a, cns3518);
  reg_41 = get(reg_39, cns3519);
  reg_42 = newStack();
  push(reg_42, reg_39);
  append(reg_36, call(reg_41, reg_42));
  reg_44 = call(reg_35, reg_36);
  reg_45 = newStack();
  push(reg_45, cns3520);
  reg_47 = call(reg_12, reg_45);
  reg_48 = reg_3.c;
  reg_49 = newStack();
  push(reg_49, cns3521);
  reg_51 = call(reg_48, reg_49);
  reg_52 = reg_3.d;
  reg_53 = newStack();
  reg_56 = get(reg_1.a, cns3522);
  reg_58 = get(reg_56, cns3523);
  reg_59 = newStack();
  push(reg_59, reg_56);
  append(reg_53, call(reg_58, reg_59));
  reg_61 = call(reg_52, reg_53);
  reg_62 = newStack();
  push(reg_62, cns3524);
  reg_64 = call(reg_12, reg_62);
  reg_67 = get(reg_1.a, cns3525);
  reg_68 = newStack();
  push(reg_68, get(reg_1.a, cns3526));
  reg_72 = call(reg_67, reg_68);
  reg_73 = next(reg_72);
  reg_74 = next(reg_72);
  reg_75 = next(reg_72);
  loop_8: while (true) {
    reg_76 = newStack();
    push(reg_76, reg_74);
    push(reg_76, reg_75);
    reg_77 = call(reg_73, reg_76);
    reg_75 = next(reg_77);
    reg_79 = next(reg_77);
    if (tobool(eq(reg_75, nil()))) break loop_8;
    goto_201 = !tobool(get(reg_79, cns3527));
    if (!goto_201) {
      reg_86 = reg_3.c;
      reg_87 = newStack();
      push(reg_87, cns3528);
      reg_89 = call(reg_86, reg_87);
      reg_90 = reg_3.d;
      reg_91 = newStack();
      push(reg_91, length(get(reg_79, cns3529)));
      reg_95 = call(reg_90, reg_91);
      reg_98 = get(reg_1.a, cns3530);
      reg_99 = newStack();
      push(reg_99, get(reg_79, cns3531));
      reg_102 = call(reg_98, reg_99);
      reg_103 = next(reg_102);
      reg_104 = next(reg_102);
      reg_105 = next(reg_102);
      loop_9: while (true) {
        reg_106 = newStack();
        push(reg_106, reg_104);
        push(reg_106, reg_105);
        reg_107 = call(reg_103, reg_106);
        reg_105 = next(reg_107);
        reg_109 = next(reg_107);
        if (tobool(eq(reg_105, nil()))) break loop_9;
        goto_161 = !tobool(get(reg_109, cns3532));
        if (!goto_161) {
          reg_116 = reg_3.c;
          reg_117 = newStack();
          push(reg_117, cns3533);
          reg_119 = call(reg_116, reg_117);
          reg_120 = reg_3.d;
          reg_121 = newStack();
          reg_123 = get(reg_109, cns3534);
          reg_125 = get(reg_123, cns3535);
          reg_126 = newStack();
          push(reg_126, reg_123);
          append(reg_121, call(reg_125, reg_126));
          reg_128 = call(reg_120, reg_121);
        }
        if ((goto_161 || false)) {
          goto_161 = false;
          goto_179 = !tobool(get(reg_109, cns3536));
          if (!goto_179) {
            reg_132 = reg_3.c;
            reg_133 = newStack();
            push(reg_133, cns3537);
            reg_135 = call(reg_132, reg_133);
            reg_136 = reg_3.d;
            reg_137 = newStack();
            push(reg_137, get(get(reg_109, cns3538), cns3539));
            reg_142 = call(reg_136, reg_137);
          }
          if ((goto_179 || false)) {
            goto_179 = false;
            reg_145 = get(reg_1.a, cns3540);
            reg_146 = newStack();
            reg_147 = cns3541;
            reg_150 = get(reg_1.a, cns3542);
            reg_151 = newStack();
            push(reg_151, reg_109);
            push(reg_146, concat(reg_147, first(call(reg_150, reg_151))));
            reg_155 = call(reg_145, reg_146);
          }
        }
        reg_156 = newStack();
        push(reg_156, get(reg_109, cns3543));
        reg_159 = call(reg_12, reg_156);
      }
    }
    if ((goto_201 || false)) {
      goto_201 = false;
      reg_161 = get(reg_79, cns3544);
      if (tobool(reg_161)) {
        reg_161 = get(reg_79, cns3545);
      }
      goto_232 = !tobool(reg_161);
      if (!goto_232) {
        reg_166 = reg_3.c;
        reg_167 = newStack();
        push(reg_167, cns3546);
        reg_169 = call(reg_166, reg_167);
        reg_170 = reg_3.d;
        reg_171 = newStack();
        push(reg_171, get(get(reg_79, cns3547), cns3548));
        reg_176 = call(reg_170, reg_171);
        reg_177 = reg_3.d;
        reg_178 = newStack();
        push(reg_178, get(get(reg_79, cns3549), cns3550));
        reg_183 = call(reg_177, reg_178);
      }
      if ((goto_232 || false)) {
        goto_232 = false;
        goto_255 = !tobool(get(reg_79, cns3551));
        if (!goto_255) {
          reg_187 = reg_3.c;
          reg_188 = newStack();
          push(reg_188, cns3552);
          reg_190 = call(reg_187, reg_188);
          reg_191 = reg_3.d;
          reg_192 = newStack();
          push(reg_192, get(get(reg_79, cns3553), cns3554));
          reg_197 = call(reg_191, reg_192);
          reg_198 = newStack();
          push(reg_198, get(reg_79, cns3555));
          reg_201 = call(reg_12, reg_198);
        }
        if ((goto_255 || false)) {
          goto_255 = false;
          reg_202 = reg_3.c;
          reg_203 = newStack();
          push(reg_203, cns3556);
          reg_205 = call(reg_202, reg_203);
          reg_206 = newStack();
          push(reg_206, get(reg_79, cns3557));
          reg_209 = call(reg_12, reg_206);
        }
      }
    }
  }
  reg_210 = reg_3.d;
  reg_211 = newStack();
  push(reg_211, length(get(reg_1.a, cns3558)));
  reg_216 = call(reg_210, reg_211);
  reg_219 = get(reg_1.a, cns3559);
  reg_220 = newStack();
  push(reg_220, get(reg_1.a, cns3560));
  reg_224 = call(reg_219, reg_220);
  reg_225 = next(reg_224);
  reg_226 = next(reg_224);
  reg_227 = next(reg_224);
  loop_7: while (true) {
    reg_228 = newStack();
    push(reg_228, reg_226);
    push(reg_228, reg_227);
    reg_229 = call(reg_225, reg_228);
    reg_227 = next(reg_229);
    reg_231 = next(reg_229);
    if (tobool(eq(reg_227, nil()))) break loop_7;
    reg_235 = reg_3.d;
    reg_236 = newStack();
    push(reg_236, add(get(reg_231, cns3561), cns3562));
    reg_241 = call(reg_235, reg_236);
    reg_242 = newStack();
    push(reg_242, get(reg_231, cns3563));
    reg_245 = call(reg_12, reg_242);
  }
  reg_246 = reg_3.d;
  reg_247 = newStack();
  push(reg_247, length(get(reg_1.a, cns3564)));
  reg_252 = call(reg_246, reg_247);
  reg_255 = get(reg_1.a, cns3565);
  reg_256 = newStack();
  push(reg_256, get(reg_1.a, cns3566));
  reg_260 = call(reg_255, reg_256);
  reg_261 = next(reg_260);
  reg_262 = next(reg_260);
  reg_263 = next(reg_260);
  loop_4: while (true) {
    reg_264 = newStack();
    push(reg_264, reg_262);
    push(reg_264, reg_263);
    reg_265 = call(reg_261, reg_264);
    reg_263 = next(reg_265);
    reg_267 = next(reg_265);
    if (tobool(eq(reg_263, nil()))) break loop_4;
    goto_352 = !tobool(get(reg_267, cns3567));
    if (!goto_352) {
      reg_274 = reg_3.c;
      reg_275 = newStack();
      push(reg_275, cns3568);
      reg_277 = call(reg_274, reg_275);
    }
    if ((goto_352 || false)) {
      goto_352 = false;
      goto_365 = !tobool(get(reg_267, cns3569));
      if (!goto_365) {
        reg_281 = reg_3.d;
        reg_282 = newStack();
        push(reg_282, add(get(reg_267, cns3570), cns3571));
        reg_287 = call(reg_281, reg_282);
      }
      if ((goto_365 || false)) {
        goto_365 = false;
        reg_290 = get(reg_1.a, cns3572);
        reg_291 = newStack();
        push(reg_291, cns3573);
        reg_293 = call(reg_290, reg_291);
      }
    }
    reg_294 = reg_3.d;
    reg_295 = newStack();
    push(reg_295, length(get(reg_267, cns3574)));
    reg_299 = call(reg_294, reg_295);
    reg_302 = get(reg_1.a, cns3575);
    reg_303 = newStack();
    push(reg_303, get(reg_267, cns3576));
    reg_306 = call(reg_302, reg_303);
    reg_307 = next(reg_306);
    reg_308 = next(reg_306);
    reg_309 = next(reg_306);
    loop_6: while (true) {
      reg_310 = newStack();
      push(reg_310, reg_308);
      push(reg_310, reg_309);
      reg_311 = call(reg_307, reg_310);
      reg_309 = next(reg_311);
      reg_313 = next(reg_311);
      if (tobool(eq(reg_309, nil()))) break loop_6;
      reg_317 = reg_3.d;
      reg_318 = newStack();
      push(reg_318, reg_313);
      reg_319 = call(reg_317, reg_318);
    }
    reg_320 = reg_3.d;
    reg_321 = newStack();
    push(reg_321, length(get(reg_267, cns3577)));
    reg_325 = call(reg_320, reg_321);
    reg_328 = get(reg_1.a, cns3578);
    reg_329 = newStack();
    push(reg_329, get(reg_267, cns3579));
    reg_332 = call(reg_328, reg_329);
    reg_333 = next(reg_332);
    reg_334 = next(reg_332);
    reg_335 = next(reg_332);
    loop_5: while (true) {
      reg_336 = newStack();
      push(reg_336, reg_334);
      push(reg_336, reg_335);
      reg_337 = call(reg_333, reg_336);
      reg_335 = next(reg_337);
      reg_339 = next(reg_337);
      if (tobool(eq(reg_335, nil()))) break loop_5;
      reg_343 = reg_3.d;
      reg_344 = newStack();
      push(reg_344, reg_339);
      reg_345 = call(reg_343, reg_344);
    }
    if (tobool(get(reg_267, cns3580))) {
      reg_349 = newStack();
      push(reg_349, get(reg_267, cns3581));
      reg_352 = call(reg_12, reg_349);
    }
  }
  reg_353 = reg_3.d;
  reg_354 = newStack();
  push(reg_354, length(get(reg_1.a, cns3582)));
  reg_359 = call(reg_353, reg_354);
  reg_362 = get(reg_1.a, cns3583);
  reg_363 = newStack();
  push(reg_363, get(reg_1.a, cns3584));
  reg_367 = call(reg_362, reg_363);
  reg_368 = next(reg_367);
  reg_369 = next(reg_367);
  reg_370 = next(reg_367);
  loop_2: while (true) {
    reg_371 = newStack();
    push(reg_371, reg_369);
    push(reg_371, reg_370);
    reg_372 = call(reg_368, reg_371);
    reg_370 = next(reg_372);
    reg_374 = next(reg_372);
    if (tobool(eq(reg_370, nil()))) break loop_2;
    goto_500 = !tobool(eq(get(reg_374, cns3585), cns3586));
    if (!goto_500) {
      reg_383 = reg_3.c;
      reg_384 = newStack();
      push(reg_384, cns3587);
      reg_386 = call(reg_383, reg_384);
      reg_387 = reg_3.d;
      reg_388 = newStack();
      push(reg_388, get(reg_374, cns3588));
      reg_391 = call(reg_387, reg_388);
    }
    if ((goto_500 || false)) {
      goto_500 = false;
      goto_517 = !tobool(eq(get(reg_374, cns3589), cns3590));
      if (!goto_517) {
        reg_397 = reg_3.c;
        reg_398 = newStack();
        push(reg_398, cns3591);
        reg_400 = call(reg_397, reg_398);
        reg_401 = newStack();
        push(reg_401, get(reg_374, cns3592));
        reg_404 = call(reg_12, reg_401);
      }
      if ((goto_517 || false)) {
        goto_517 = false;
        goto_606 = !tobool(eq(get(reg_374, cns3593), cns3594));
        if (!goto_606) {
          reg_412 = length(get(reg_374, cns3595));
          if (tobool(ne(reg_412, length(get(get(reg_374, cns3596), cns3597))))) {
            reg_422 = get(reg_1.a, cns3598);
            reg_423 = newStack();
            reg_427 = get(get(reg_374, cns3599), cns3600);
            reg_428 = cns3601;
            reg_433 = length(get(get(reg_374, cns3602), cns3603));
            reg_434 = cns3604;
            push(reg_423, concat(reg_427, concat(reg_428, concat(reg_433, concat(reg_434, length(get(reg_374, cns3605)))))));
            reg_442 = call(reg_422, reg_423);
          }
          reg_443 = reg_3.d;
          reg_444 = newStack();
          reg_446 = get(reg_374, cns3606);
          reg_448 = get(reg_446, cns3607);
          reg_449 = newStack();
          push(reg_449, reg_446);
          push(reg_444, add(first(call(reg_448, reg_449)), cns3608));
          reg_454 = call(reg_443, reg_444);
          reg_457 = get(reg_1.a, cns3609);
          reg_458 = newStack();
          push(reg_458, get(reg_374, cns3610));
          reg_461 = call(reg_457, reg_458);
          reg_462 = next(reg_461);
          reg_463 = next(reg_461);
          reg_464 = next(reg_461);
          loop_3: while (true) {
            reg_465 = newStack();
            push(reg_465, reg_463);
            push(reg_465, reg_464);
            reg_466 = call(reg_462, reg_465);
            reg_464 = next(reg_466);
            reg_468 = next(reg_466);
            if (tobool(eq(reg_464, nil()))) break loop_3;
            reg_472 = reg_3.d;
            reg_473 = newStack();
            reg_475 = get(reg_468, cns3611);
            reg_476 = newStack();
            push(reg_476, reg_468);
            append(reg_473, call(reg_475, reg_476));
            reg_478 = call(reg_472, reg_473);
          }
        }
        if ((goto_606 || false)) {
          goto_606 = false;
          reg_481 = get(reg_1.a, cns3612);
          reg_482 = newStack();
          reg_483 = cns3613;
          push(reg_482, concat(reg_483, concat(get(reg_374, cns3614), cns3615)));
          reg_489 = call(reg_481, reg_482);
        }
      }
    }
  }
  reg_491 = anyFn((function (a) { return function$46(a,this) }).bind(reg_3));
  reg_494 = get(reg_1.a, cns3677);
  reg_495 = newStack();
  push(reg_495, get(reg_1.a, cns3678));
  reg_499 = call(reg_494, reg_495);
  reg_500 = next(reg_499);
  reg_501 = next(reg_499);
  reg_502 = next(reg_499);
  loop_1: while (true) {
    reg_503 = newStack();
    push(reg_503, reg_501);
    push(reg_503, reg_502);
    reg_504 = call(reg_500, reg_503);
    reg_502 = next(reg_504);
    reg_506 = next(reg_504);
    if (tobool(eq(reg_502, nil()))) break loop_1;
    if (tobool(get(reg_506, cns3679))) {
      reg_513 = newStack();
      push(reg_513, reg_506);
      reg_514 = call(reg_491, reg_513);
    }
  }
  reg_516 = anyFn((function (a) { return function$48(a,this) }).bind(reg_3));
  set(reg_1.a, cns3698, reg_516);
  reg_521 = get(reg_1.a, cns3699);
  reg_522 = newStack();
  push(reg_522, get(reg_1.a, cns3700));
  reg_526 = call(reg_521, reg_522);
  reg_527 = newStack();
  return reg_527;
}
function aulua_write$lua_main (reg_0) {
  var reg_2, reg_3;
  reg_2 = {a: nil()};
  reg_2.a = reg_0;
  reg_3 = newStack();
  push(reg_3, anyFn((function (a) { return aulua_write$function(a,this) }).bind(reg_2)));
  return reg_3;
}
var cns3701 = anyStr("open")
var cns3702 = anyStr("parse")
function aulua_compile$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_16, reg_17, reg_18, reg_20, reg_21, reg_22, reg_24, reg_25, reg_27;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_9 = get(reg_1.a, cns3701);
  reg_10 = newStack();
  push(reg_10, reg_4);
  reg_11 = call(reg_9, reg_10);
  reg_16 = call(get(reg_1.a, cns3702), newStack());
  reg_17 = next(reg_16);
  reg_18 = next(reg_16);
  if (tobool(reg_18)) {
    reg_20 = newStack();
    push(reg_20, reg_18);
    return reg_20;
  }
  reg_21 = reg_1.b;
  reg_22 = newStack();
  push(reg_22, reg_17);
  push(reg_22, reg_6);
  reg_23 = call(reg_21, reg_22);
  reg_24 = reg_1.c;
  reg_25 = newStack();
  push(reg_25, reg_5);
  reg_26 = call(reg_24, reg_25);
  reg_27 = newStack();
  return reg_27;
}
function aulua_compile$lua_main (reg_0) {
  var reg_1, reg_2, reg_9;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1, c: reg_1};
  reg_2.a = first(lua_parser_parser$lua_main(reg_0));
  reg_2.b = first(aulua_codegen$lua_main(reg_0));
  reg_2.c = first(aulua_write$lua_main(reg_0));
  reg_9 = newStack();
  push(reg_9, anyFn((function (a) { return aulua_compile$function(a,this) }).bind(reg_2)));
  return reg_9;
}
var cns3703 = anyStr("print")
var cns3704 = anyStr("print")
var cns3705 = anyStr("usage: aulua [options] filename")
var cns3706 = anyStr("print")
var cns3707 = anyStr("Available options are:")
var cns3708 = anyStr("print")
var cns3709 = anyStr("  -o name  output to file 'name'")
var cns3710 = anyStr("print")
var cns3711 = anyStr("           default is 'filename' as it would be passed to 'require'")
var cns3712 = anyStr("print")
var cns3713 = anyStr("           if it's a directory, the file will be written there with")
var cns3714 = anyStr("print")
var cns3715 = anyStr("             the default filename")
var cns3716 = anyStr("print")
var cns3717 = anyStr("  -v       Show version information")
var cns3718 = anyStr("print")
var cns3719 = anyStr("  -h       Show this help message")
var cns3720 = anyStr("os")
var cns3721 = anyStr("exit")
function aulua$function (reg_0, reg_1) {
  var reg_4, reg_8, reg_9, reg_13, reg_14, reg_19, reg_20, reg_25, reg_26, reg_31, reg_32, reg_37, reg_38, reg_43, reg_44, reg_49, reg_50, reg_55, reg_56, reg_66;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  if (tobool(reg_4)) {
    reg_8 = get(reg_1.a, cns3703);
    reg_9 = newStack();
    push(reg_9, reg_4);
    reg_10 = call(reg_8, reg_9);
  }
  reg_13 = get(reg_1.a, cns3704);
  reg_14 = newStack();
  push(reg_14, cns3705);
  reg_16 = call(reg_13, reg_14);
  reg_19 = get(reg_1.a, cns3706);
  reg_20 = newStack();
  push(reg_20, cns3707);
  reg_22 = call(reg_19, reg_20);
  reg_25 = get(reg_1.a, cns3708);
  reg_26 = newStack();
  push(reg_26, cns3709);
  reg_28 = call(reg_25, reg_26);
  reg_31 = get(reg_1.a, cns3710);
  reg_32 = newStack();
  push(reg_32, cns3711);
  reg_34 = call(reg_31, reg_32);
  reg_37 = get(reg_1.a, cns3712);
  reg_38 = newStack();
  push(reg_38, cns3713);
  reg_40 = call(reg_37, reg_38);
  reg_43 = get(reg_1.a, cns3714);
  reg_44 = newStack();
  push(reg_44, cns3715);
  reg_46 = call(reg_43, reg_44);
  reg_49 = get(reg_1.a, cns3716);
  reg_50 = newStack();
  push(reg_50, cns3717);
  reg_52 = call(reg_49, reg_50);
  reg_55 = get(reg_1.a, cns3718);
  reg_56 = newStack();
  push(reg_56, cns3719);
  reg_58 = call(reg_55, reg_56);
  reg_65 = call(get(get(reg_1.a, cns3720), cns3721), newStack());
  reg_66 = newStack();
  return reg_66;
}
var cns3722 = anyStr("help")
var cns3723 = anyStr("arg")
var cns3724 = parseNum("0")
var cns3725 = anyStr("help")
var cns3726 = parseNum("1")
var cns3727 = anyStr("arg")
function le (reg_0, reg_1) {
  var reg_3;
  reg_3 = _le(reg_0, reg_1);
  return reg_3;
}
var cns3728 = anyStr("arg")
var cns3729 = anyStr("-h")
var cns3730 = anyStr("help")
var cns3731 = anyStr("-v")
var cns3732 = anyStr("print")
var cns3733 = anyStr("aulua 0.6")
var cns3734 = anyStr("-o")
var cns3735 = parseNum("1")
var cns3736 = anyStr("arg")
var cns3737 = anyStr("help")
var cns3738 = anyStr("'-o' needs argument")
var cns3739 = anyStr("arg")
var cns3740 = anyStr("sub")
var cns3741 = parseNum("1")
var cns3742 = parseNum("1")
var cns3743 = anyStr("-")
var cns3744 = anyStr("help")
var cns3745 = anyStr("unrecognized option '")
var cns3746 = anyStr("'")
var cns3747 = anyStr("help")
var cns3748 = anyStr("needs only one filename")
var cns3749 = parseNum("1")
var cns3750 = anyStr("help")
var cns3751 = anyStr("needs filename")
var cns3752 = anyStr("gsub")
var cns3753 = anyStr("[$.]lua$")
var cns3754 = anyStr("")
var cns3755 = anyStr("gsub")
var cns3756 = anyStr("[/\\]+")
var cns3757 = anyStr(".")
var cns3758 = anyStr("match")
var cns3759 = anyStr("/$")
var cns3760 = anyStr("io")
var cns3761 = anyStr("open")
var cns3762 = anyStr("r")
var cns3763 = anyStr("input_file")
var cns3764 = anyStr("io_err")
var cns3765 = anyStr("input_file")
var cns3766 = anyStr("error")
var cns3767 = anyStr("io_err")
var cns3768 = anyStr("input_file")
var cns3769 = anyStr("read")
var cns3770 = anyStr("a")
var cns3771 = anyStr("contents")
var cns3772 = anyStr("input_file")
var cns3773 = anyStr("close")
var cns3774 = anyStr("io")
var cns3775 = anyStr("open")
var cns3776 = anyStr("wb")
var cns3777 = anyStr("output_file")
var cns3778 = anyStr("io_err")
var cns3779 = anyStr("output_file")
var cns3780 = anyStr("error")
var cns3781 = anyStr("io_err")
var cns3782 = anyStr("output_file")
var cns3783 = anyStr("writebytes")
var cns3784 = anyStr("output_file")
var cns3785 = anyStr("writebytes")
function function$49 (reg_0, reg_1) {
  var reg_4, reg_7, reg_9, reg_10, reg_12;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns3784);
  reg_9 = get(reg_7, cns3785);
  reg_10 = newStack();
  push(reg_10, reg_7);
  push(reg_10, reg_4);
  reg_11 = call(reg_9, reg_10);
  reg_12 = newStack();
  return reg_12;
}
var cns3786 = anyStr("output_file")
var cns3787 = anyStr("write")
var cns3788 = anyStr("string")
var cns3789 = anyStr("char")
function function$50 (reg_0, reg_1) {
  var reg_4, reg_7, reg_9, reg_10, reg_15, reg_16, reg_19;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns3786);
  reg_9 = get(reg_7, cns3787);
  reg_10 = newStack();
  push(reg_10, reg_7);
  reg_15 = get(get(reg_1.a, cns3788), cns3789);
  reg_16 = newStack();
  push(reg_16, reg_4);
  append(reg_10, call(reg_15, reg_16));
  reg_18 = call(reg_9, reg_10);
  reg_19 = newStack();
  return reg_19;
}
var cns3790 = anyStr("contents")
var cns3791 = anyStr("output_file")
var cns3792 = anyStr("close")
var cns3793 = anyStr("print")
var cns3794 = anyStr("Error: ")
var cns3795 = anyStr("Parser")
var cns3796 = anyStr("error")
var cns3797 = anyStr("os")
var cns3798 = anyStr("exit")
var cns3799 = parseNum("1")
function lua_main (reg_0) {
  var reg_2, reg_4, reg_6, reg_21, reg_22, reg_23, reg_33, reg_47, reg_48, reg_64, reg_65, reg_73, reg_74, reg_84, reg_85, reg_86, reg_94, reg_95, reg_104, reg_105, reg_109, reg_110, reg_114, reg_116, reg_117, reg_121, reg_125, reg_126, reg_136, reg_137, reg_139, reg_140, reg_143, reg_153, reg_154, reg_161, reg_163, reg_164, reg_167, reg_172, reg_174, reg_175, reg_181, reg_182, reg_184, reg_185, reg_188, reg_198, reg_199, reg_204, reg_215, reg_220, reg_223, reg_225, reg_226, reg_231, reg_232, reg_233, reg_245, reg_246, reg_249;
  var goto_48=false, goto_60=false, goto_88=false, goto_113=false, goto_164=false, goto_279=false;
  reg_2 = {a: nil()};
  reg_2.a = reg_0;
  reg_4 = first(aulua_compile$lua_main(reg_0));
  reg_6 = anyFn((function (a) { return aulua$function(a,this) }).bind(reg_2));
  set(reg_2.a, cns3722, reg_6);
  if (tobool(eq(length(get(reg_2.a, cns3723)), cns3724))) {
    reg_20 = call(get(reg_2.a, cns3725), newStack());
  }
  reg_21 = nil();
  reg_22 = nil();
  reg_23 = cns3726;
  loop_1: while (tobool(le(reg_23, length(get(reg_2.a, cns3727))))) {
    reg_33 = get(get(reg_2.a, cns3728), reg_23);
    goto_48 = !tobool(eq(reg_33, cns3729));
    if (!goto_48) {
      reg_41 = call(get(reg_2.a, cns3730), newStack());
    }
    if ((goto_48 || false)) {
      goto_48 = false;
      goto_60 = !tobool(eq(reg_33, cns3731));
      if (!goto_60) {
        reg_47 = get(reg_2.a, cns3732);
        reg_48 = newStack();
        push(reg_48, cns3733);
        reg_50 = call(reg_47, reg_48);
      }
      if ((goto_60 || false)) {
        goto_60 = false;
        goto_88 = !tobool(eq(reg_33, cns3734));
        if (!goto_88) {
          reg_23 = add(reg_23, cns3735);
          if (tobool(not(get(get(reg_2.a, cns3736), reg_23)))) {
            reg_64 = get(reg_2.a, cns3737);
            reg_65 = newStack();
            push(reg_65, cns3738);
            reg_67 = call(reg_64, reg_65);
          }
          reg_22 = get(get(reg_2.a, cns3739), reg_23);
        }
        if ((goto_88 || false)) {
          goto_88 = false;
          reg_73 = get(reg_33, cns3740);
          reg_74 = newStack();
          push(reg_74, reg_33);
          push(reg_74, cns3741);
          push(reg_74, cns3742);
          goto_113 = !tobool(eq(first(call(reg_73, reg_74)), cns3743));
          if (!goto_113) {
            reg_84 = get(reg_2.a, cns3744);
            reg_85 = newStack();
            reg_86 = cns3745;
            push(reg_85, concat(reg_86, concat(reg_33, cns3746)));
            reg_90 = call(reg_84, reg_85);
          }
          if ((goto_113 || false)) {
            goto_113 = false;
            if (tobool(reg_21)) {
              reg_94 = get(reg_2.a, cns3747);
              reg_95 = newStack();
              push(reg_95, cns3748);
              reg_97 = call(reg_94, reg_95);
            }
            reg_21 = reg_33;
          }
        }
      }
    }
    reg_23 = add(reg_23, cns3749);
  }
  if (tobool(not(reg_21))) {
    reg_104 = get(reg_2.a, cns3750);
    reg_105 = newStack();
    push(reg_105, cns3751);
    reg_107 = call(reg_104, reg_105);
  }
  reg_109 = get(reg_21, cns3752);
  reg_110 = newStack();
  push(reg_110, reg_21);
  push(reg_110, cns3753);
  push(reg_110, cns3754);
  reg_114 = first(call(reg_109, reg_110));
  reg_116 = get(reg_114, cns3755);
  reg_117 = newStack();
  push(reg_117, reg_114);
  push(reg_117, cns3756);
  push(reg_117, cns3757);
  reg_121 = first(call(reg_116, reg_117));
  goto_164 = !tobool(not(reg_22));
  if (!goto_164) {
    reg_22 = reg_121;
  }
  if ((goto_164 || false)) {
    goto_164 = false;
    reg_125 = get(reg_22, cns3758);
    reg_126 = newStack();
    push(reg_126, reg_22);
    push(reg_126, cns3759);
    if (tobool(first(call(reg_125, reg_126)))) {
      reg_22 = concat(reg_22, reg_121);
    }
  }
  reg_136 = get(get(reg_2.a, cns3760), cns3761);
  reg_137 = newStack();
  push(reg_137, reg_21);
  push(reg_137, cns3762);
  reg_139 = call(reg_136, reg_137);
  reg_140 = next(reg_139);
  set(reg_2.a, cns3763, reg_140);
  reg_143 = next(reg_139);
  set(reg_2.a, cns3764, reg_143);
  if (tobool(not(get(reg_2.a, cns3765)))) {
    reg_153 = get(reg_2.a, cns3766);
    reg_154 = newStack();
    push(reg_154, get(reg_2.a, cns3767));
    reg_158 = call(reg_153, reg_154);
  }
  reg_161 = get(reg_2.a, cns3768);
  reg_163 = get(reg_161, cns3769);
  reg_164 = newStack();
  push(reg_164, reg_161);
  push(reg_164, cns3770);
  reg_167 = first(call(reg_163, reg_164));
  set(reg_2.a, cns3771, reg_167);
  reg_172 = get(reg_2.a, cns3772);
  reg_174 = get(reg_172, cns3773);
  reg_175 = newStack();
  push(reg_175, reg_172);
  reg_176 = call(reg_174, reg_175);
  reg_181 = get(get(reg_2.a, cns3774), cns3775);
  reg_182 = newStack();
  push(reg_182, reg_22);
  push(reg_182, cns3776);
  reg_184 = call(reg_181, reg_182);
  reg_185 = next(reg_184);
  set(reg_2.a, cns3777, reg_185);
  reg_188 = next(reg_184);
  set(reg_2.a, cns3778, reg_188);
  if (tobool(not(get(reg_2.a, cns3779)))) {
    reg_198 = get(reg_2.a, cns3780);
    reg_199 = newStack();
    push(reg_199, get(reg_2.a, cns3781));
    reg_203 = call(reg_198, reg_199);
  }
  reg_204 = nil();
  goto_279 = !tobool(get(get(reg_2.a, cns3782), cns3783));
  if (!goto_279) {
    reg_204 = anyFn((function (a) { return function$49(a,this) }).bind(reg_2));
  }
  if ((goto_279 || false)) {
    goto_279 = false;
    reg_204 = anyFn((function (a) { return function$50(a,this) }).bind(reg_2));
  }
  reg_215 = newStack();
  push(reg_215, get(reg_2.a, cns3790));
  push(reg_215, reg_204);
  push(reg_215, reg_21);
  reg_220 = first(call(reg_4, reg_215));
  reg_223 = get(reg_2.a, cns3791);
  reg_225 = get(reg_223, cns3792);
  reg_226 = newStack();
  push(reg_226, reg_223);
  reg_227 = call(reg_225, reg_226);
  if (tobool(reg_220)) {
    reg_231 = get(reg_2.a, cns3793);
    reg_232 = newStack();
    reg_233 = cns3794;
    push(reg_232, concat(reg_233, get(get(reg_2.a, cns3795), cns3796)));
    reg_240 = call(reg_231, reg_232);
    reg_245 = get(get(reg_2.a, cns3797), cns3798);
    reg_246 = newStack();
    push(reg_246, cns3799);
    reg_248 = call(reg_245, reg_246);
  }
  reg_249 = newStack();
  return reg_249;
}
function anyTable (reg_0) {
  var reg_1;
  reg_1 = new record_22(reg_0);
  return reg_1;
}
function _assert (reg_0) {
  var reg_1, reg_3, reg_4, reg_5;
  reg_1 = next(reg_0);
  if (tobool(reg_1)) {
    reg_3 = newStack();
    push(reg_3, reg_1);
    return reg_3;
    if (true) { goto(15); };
  }
  reg_4 = next(reg_0);
  reg_5 = tostr(reg_4);
  if (testNil(reg_4)) {
    reg_5 = "assertion failed!";
  }
  throw new Error(reg_5);
}
function _error (reg_0) {
  throw new Error(tostr(next(reg_0)));
}
function _getmeta (reg_0) {
  var reg_1, reg_4, reg_9, reg_11;
  reg_1 = next(reg_0);
  if (testNil(reg_1)) {
    throw new Error("Lua: bad argument #1 to 'getmetatable' (value expected)");
  }
  reg_4 = get_metatable(reg_1);
  if (!(reg_4 == null)) {
    reg_9 = stackof(anyTable(reg_4));
    return reg_9;
  }
  reg_11 = stackof(nil());
  return reg_11;
}
var record_48 = function (val) { this.val = val; }
var Null_record_48 = function (val) { this.val = val; }
function nextKey (reg_0, reg_1) {
  var reg_8, reg_10, reg_11, reg_15, reg_18, reg_23, reg_26, reg_27, reg_29, reg_31, reg_32, reg_38, reg_39, reg_44, reg_45, reg_53, reg_54, reg_62, reg_65, reg_68, reg_70, reg_71, reg_77, reg_80, reg_81, reg_90, reg_95, reg_98, reg_99, reg_102;
  var goto_11=false, goto_37=false, goto_40=false, goto_58=false, goto_68=false, goto_104=false, goto_111=false, goto_118=false;
  goto_40 = !testNil(reg_1);
  if (!goto_40) {
    if ((reg_0.c.length > 0)) {
      reg_8 = new Integer(1);
      return reg_8;
      goto_11 = false;
    }
    goto_11 = false;
  }
  loop_4: while ((goto_40 || true)) {
    goto_40 = goto_40;
    if (!goto_40) {
      reg_10 = new StringMapIter_any(reg_0.b);
      reg_11 = reg_10.next();
      if (!(reg_11 == null)) {
        reg_15 = reg_11.a;
        reg_0.f = reg_10;
        reg_0.g = reg_15;
        reg_18 = reg_15;
        return reg_18;
      }
    }
    loop_2: while ((goto_40 || true)) {
      if (!goto_40) {
        goto_37 = !(reg_0.d.length > 0);
        if (!goto_37) {
          reg_23 = 0;
          reg_26 = reg_0.d[reg_23].a;
          return reg_26;
        }
        if ((goto_37 || false)) {
          goto_37 = false;
          reg_27 = nil();
          return reg_27;
        }
      }
      goto_40 = false;
      if ((reg_1 instanceof Integer)) {
        reg_29 = reg_1.val;
        reg_31 = reg_0.c.length;
        reg_32 = (reg_29 > 0);
        if (reg_32) {
          reg_32 = (reg_29 < reg_31);
        }
        goto_58 = !reg_32;
        if (!goto_58) {
          reg_38 = new Integer((reg_29 + 1));
          return reg_38;
        }
        if ((goto_58 || false)) {
          goto_58 = false;
          reg_39 = (reg_29 == reg_31);
          if (reg_39) {
            reg_39 = (reg_31 > 0);
          }
          goto_68 = !reg_39;
          if (!goto_68) {
            goto_11 = true;
            if (goto_11) break loop_2;
          }
          if ((goto_68 || false)) {
            goto_68 = false;
            goto_118 = true;
            if (goto_118) break loop_2;
          }
        }
      }
      goto_118 = !(typeof reg_1 === 'string');
      if (goto_118) break loop_2;
      reg_44 = reg_1;
      reg_45 = (reg_0.g == null);
      if (!reg_45) {
        reg_45 = !(reg_0.g == reg_44);
      }
      if (reg_45) {
        reg_53 = new StringMapIter_any(reg_0.b);
        reg_54 = reg_53.next();
        loop_3: while (!(reg_54 == null)) {
          if ((reg_54.a == reg_44)) {
            reg_0.f = reg_53;
            reg_0.g = reg_44;
            goto_104 = true;
            if (goto_104) break loop_3;
          }
        }
        if (!goto_104) {
          reg_62 = nil();
          return reg_62;
        }
        goto_104 = false;
      }
      goto_104 = false;
      reg_65 = reg_0.f.next();
      goto_111 = !(reg_65 == null);
      if (goto_111) break loop_2;
    }
    if (!goto_11) break loop_4;
  }
  if (!goto_118) {
    if (!goto_118) {
      goto_111 = goto_111;
      if (!goto_111) {
      }
      if ((goto_111 || false)) {
        goto_111 = false;
        reg_68 = reg_65.a;
        reg_0.g = reg_68;
        reg_70 = reg_68;
        return reg_70;
      }
    }
    goto_118 = false;
  }
  goto_118 = false;
  reg_71 = testNil(reg_1);
  if (reg_71) {
    reg_71 = (reg_0.d.length > 0);
  }
  if (reg_71) {
    reg_77 = 0;
    reg_80 = reg_0.d[reg_77].a;
    return reg_80;
  }
  reg_81 = 0;
  loop_1: while ((reg_81 < reg_0.d.length)) {
    if (equals(reg_1, reg_0.d[reg_81].a)) {
      reg_90 = (reg_81 + 1);
      if ((reg_90 < reg_0.d.length)) {
        reg_95 = (reg_81 + 1);
        reg_98 = reg_0.d[reg_95].a;
        return reg_98;
      }
      reg_99 = nil();
      return reg_99;
    }
    reg_81 = (reg_81 + 1);
  }
  reg_102 = nil();
  return reg_102;
}
function _next (reg_0) {
  var reg_1, reg_9, reg_12;
  reg_1 = next(reg_0);
  if (!testTable(reg_1)) {
    throw new Error((("Lua: bad argument #1 to 'next' (table expected, got " + typestr(reg_1)) + ")"));
  }
  reg_9 = next(reg_0);
  reg_12 = stackof(nextKey(getTable(reg_1), reg_9));
  return reg_12;
}
function _print (reg_0) {
  var reg_1, reg_2, reg_4, reg_10;
  var goto_9=false;
  reg_1 = true;
  reg_2 = "";
  loop_1: while (more(reg_0)) {
    reg_4 = next(reg_0);
    goto_9 = !reg_1;
    if (!goto_9) {
      reg_1 = false;
    }
    if ((goto_9 || false)) {
      goto_9 = false;
      reg_2 = (reg_2 + "\t");
    }
    reg_2 = (reg_2 + tostr(reg_4));
  }
  console.log(reg_2);
  reg_10 = newStack();
  return reg_10;
}
function lua$length (reg_0) {
  var reg_4, reg_7;
  reg_4 = (reg_0.b.length - reg_0.a);
  if ((reg_4 < 0)) {
    reg_7 = 0;
    return reg_7;
  }
  return reg_4;
}
function getNum (reg_0) {
  var reg_1, reg_6, reg_7;
  reg_1 = (reg_0 instanceof Integer);
  if (!reg_1) {
    reg_1 = (typeof reg_0 === 'number');
  }
  if (reg_1) {
    return reg_0;
  }
  if ((typeof reg_0 === 'string')) {
    reg_6 = parseNum(reg_0);
    return reg_6;
  }
  reg_7 = nil();
  return reg_7;
}
function simple_number (reg_0, reg_1, reg_2) {
  var reg_3, reg_5;
  reg_3 = getNum(reg_0);
  if ((reg_3 instanceof Integer)) {
    reg_5 = reg_3.val;
    return reg_5;
  }
  throw new Error((((((("Lua: bad argument #" + reg_1) + " to '") + reg_2) + "' (number expected, got ") + typestr(reg_3)) + ")"));
}
function _select (reg_0) {
  var reg_1, reg_2, reg_9, reg_12;
  reg_1 = next(reg_0);
  reg_2 = testStr(reg_1);
  if (reg_2) {
    reg_2 = (getStr(reg_1) == "#");
  }
  if (reg_2) {
    reg_9 = stackof(anyInt(lua$length(reg_0)));
    return reg_9;
  }
  reg_12 = simple_number(reg_1, "1", "select");
  if ((reg_12 < 1)) {
    throw new Error("bad argument #1 to 'select' (index out of range)");
  }
  reg_0.a = reg_12;
  return reg_0;
}
function _setmeta (reg_0) {
  var reg_1, reg_2, reg_17, reg_21;
  reg_1 = next(reg_0);
  reg_2 = next(reg_0);
  if (!testTable(reg_1)) {
    throw new Error((("Lua: bad argument #1 to 'getmetatable' (table expected, got " + typestr(reg_1)) + ")"));
  }
  if (!testTable(reg_2)) {
    throw new Error((("Lua: bad argument typestr(reg_2) to 'getmetatable' (table expected, got " + typestr(reg_2)) + ")"));
  }
  reg_17 = getTable(reg_1);
  reg_17.e = getTable(reg_2);
  reg_21 = stackof(reg_1);
  return reg_21;
}
function _tostring (reg_0) {
  var reg_4;
  reg_4 = stackof(anyStr(tostr(next(reg_0))));
  return reg_4;
}
function _tonumber (reg_0) {
  var reg_1, reg_2, reg_5, reg_9, reg_10;
  reg_1 = next(reg_0);
  reg_2 = (reg_1 instanceof Integer);
  if (!reg_2) {
    reg_2 = (typeof reg_1 === 'number');
  }
  if (reg_2) {
    reg_5 = stackof(reg_1);
    return reg_5;
  }
  if ((typeof reg_1 === 'string')) {
    reg_9 = stackof(parseNum(reg_1));
    return reg_9;
  }
  reg_10 = newStack();
  return reg_10;
}
function _type (reg_0) {
  var reg_4;
  reg_4 = stackof(anyStr(typestr(next(reg_0))));
  return reg_4;
}
function _pack (reg_0) {
  var reg_1, reg_2, reg_12;
  reg_1 = emptyTable();
  reg_2 = 0;
  loop_1: while (more(reg_0)) {
    reg_2 = (reg_2 + 1);
    lua$set(reg_1, anyInt(reg_2), next(reg_0));
  }
  lua$set(reg_1, anyStr("n"), anyInt(reg_2));
  reg_12 = stackof(anyTable(reg_1));
  return reg_12;
}
function simple_number_or (reg_0, reg_1, reg_2, reg_3) {
  var reg_5;
  if (testNil(reg_0)) {
    return reg_1;
  }
  reg_5 = simple_number(reg_0, reg_2, reg_3);
  return reg_5;
}
function _unpack (reg_0) {
  var reg_1, reg_9, reg_14, reg_15, reg_20, reg_21;
  reg_1 = next(reg_0);
  if (!testTable(reg_1)) {
    throw new Error((("Lua: bad argument #1 to 'table.unpack' (table expected, got " + typestr(reg_1)) + ")"));
  }
  reg_9 = getTable(reg_1);
  reg_14 = simple_number_or(next(reg_0), 1, "2", "table.unpack");
  reg_15 = next(reg_0);
  reg_20 = simple_number_or(reg_15, getInt(length(reg_1)), "3", "table.unpack");
  reg_21 = newStack();
  loop_1: while ((reg_14 <= reg_20)) {
    push(reg_21, lua$get(reg_9, anyInt(reg_14)));
    reg_14 = (reg_14 + 1);
  }
  return reg_21;
}
var record_50 = function (val) { this.val = val; }
var cns3800 = parseNum("1")
function function$51 (reg_0, reg_1) {
  var reg_5, reg_6, reg_9, reg_11, reg_12;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_6 = add(reg_6, cns3800);
  reg_9 = get(reg_5, reg_6);
  if (tobool(reg_9)) {
    reg_11 = newStack();
    push(reg_11, reg_6);
    push(reg_11, reg_9);
    return reg_11;
  }
  reg_12 = newStack();
  return reg_12;
}
var cns3801 = parseNum("0")
function lua_lib_table$function (reg_0, reg_1) {
  var reg_3, reg_4, reg_6, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_6 = anyFn((function (a) { return function$51(a,this) }).bind(reg_3));
  reg_7 = newStack();
  push(reg_7, reg_6);
  push(reg_7, reg_4);
  push(reg_7, cns3801);
  return reg_7;
}
var cns3802 = anyStr("ipairs")
var cns3803 = anyStr("next")
function function$53 (reg_0, reg_1) {
  var reg_2, reg_5, reg_6, reg_9, reg_10, reg_13;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_9 = get(reg_2.a, cns3803);
  reg_10 = newStack();
  push(reg_10, reg_5);
  push(reg_10, reg_6);
  reg_6 = first(call(reg_9, reg_10));
  reg_13 = newStack();
  push(reg_13, reg_6);
  push(reg_13, get(reg_5, reg_6));
  return reg_13;
}
function function$52 (reg_0, reg_1) {
  var reg_3, reg_4, reg_6, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_6 = anyFn((function (a) { return function$53(a,this) }).bind(reg_3));
  reg_7 = newStack();
  push(reg_7, reg_6);
  push(reg_7, reg_4);
  push(reg_7, nil());
  return reg_7;
}
var cns3804 = anyStr("pairs")
var cns3805 = anyStr("type")
var cns3806 = anyStr("number")
var cns3807 = anyStr("tostring")
var cns3808 = anyStr("type")
var cns3809 = anyStr("string")
var cns3810 = anyStr("error")
var cns3811 = anyStr("bad argument #")
var cns3812 = anyStr(" for '")
var cns3813 = anyStr(" (expected string, got ")
var cns3814 = anyStr(")")
function function$54 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_11, reg_13, reg_15, reg_18, reg_24, reg_25, reg_30, reg_31, reg_37, reg_40, reg_41, reg_42, reg_43, reg_44, reg_53;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_10 = get(reg_1.a, cns3805);
  reg_11 = newStack();
  push(reg_11, reg_4);
  reg_13 = first(call(reg_10, reg_11));
  reg_15 = eq(reg_4, nil());
  if (tobool(reg_15)) {
    reg_15 = reg_5;
  }
  if (tobool(reg_15)) {
    reg_18 = newStack();
    push(reg_18, reg_5);
    return reg_18;
  }
  if (tobool(eq(reg_13, cns3806))) {
    reg_24 = get(reg_1.a, cns3807);
    reg_25 = newStack();
    push(reg_25, reg_4);
    reg_4 = first(call(reg_24, reg_25));
  }
  reg_30 = get(reg_1.a, cns3808);
  reg_31 = newStack();
  push(reg_31, reg_4);
  if (tobool(eq(first(call(reg_30, reg_31)), cns3809))) {
    reg_37 = newStack();
    push(reg_37, reg_4);
    return reg_37;
  }
  reg_40 = get(reg_1.a, cns3810);
  reg_41 = newStack();
  reg_42 = cns3811;
  reg_43 = cns3812;
  reg_44 = cns3813;
  push(reg_41, concat(reg_42, concat(reg_6, concat(reg_43, concat(reg_7, concat(reg_44, concat(reg_13, cns3814)))))));
  reg_52 = call(reg_40, reg_41);
  reg_53 = newStack();
  return reg_53;
}
var cns3815 = anyStr("type")
var cns3816 = anyStr("i")
var cns3817 = anyStr("string")
var cns3818 = anyStr("tonumber")
var cns3819 = anyStr("type")
var cns3820 = anyStr("number")
var cns3821 = anyStr("error")
var cns3822 = anyStr("bad argument #")
var cns3823 = anyStr(" for '")
var cns3824 = anyStr(" (expected number, got ")
var cns3825 = anyStr(")")
function function$55 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_11, reg_16, reg_18, reg_21, reg_27, reg_28, reg_33, reg_34, reg_40, reg_43, reg_44, reg_45, reg_46, reg_47, reg_56;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_10 = get(reg_1.a, cns3815);
  reg_11 = newStack();
  push(reg_11, get(reg_1.a, cns3816));
  reg_16 = first(call(reg_10, reg_11));
  reg_18 = eq(reg_4, nil());
  if (tobool(reg_18)) {
    reg_18 = reg_5;
  }
  if (tobool(reg_18)) {
    reg_21 = newStack();
    push(reg_21, reg_5);
    return reg_21;
  }
  if (tobool(eq(reg_16, cns3817))) {
    reg_27 = get(reg_1.a, cns3818);
    reg_28 = newStack();
    push(reg_28, reg_4);
    reg_4 = first(call(reg_27, reg_28));
  }
  reg_33 = get(reg_1.a, cns3819);
  reg_34 = newStack();
  push(reg_34, reg_4);
  if (tobool(eq(first(call(reg_33, reg_34)), cns3820))) {
    reg_40 = newStack();
    push(reg_40, reg_4);
    return reg_40;
  }
  reg_43 = get(reg_1.a, cns3821);
  reg_44 = newStack();
  reg_45 = cns3822;
  reg_46 = cns3823;
  reg_47 = cns3824;
  push(reg_44, concat(reg_45, concat(reg_6, concat(reg_46, concat(reg_7, concat(reg_47, concat(reg_16, cns3825)))))));
  reg_55 = call(reg_43, reg_44);
  reg_56 = newStack();
  return reg_56;
}
var cns3826 = anyStr("table")
var cns3827 = anyStr("concat")
var cns3828 = anyStr("type")
var cns3829 = anyStr("table")
var cns3830 = anyStr("error")
var cns3831 = anyStr("bad argument #1 for 'table.concat' (expected table, got ")
var cns3832 = anyStr("type")
var cns3833 = anyStr(")")
var cns3834 = anyStr("")
var cns3835 = parseNum("1")
var cns3836 = anyStr("table.concat")
var cns3837 = parseNum("1")
var cns3838 = parseNum("2")
var cns3839 = anyStr("table.concat")
var cns3840 = parseNum("3")
var cns3841 = anyStr("table.concat")
var cns3842 = anyStr("")
var cns3843 = parseNum("1")
var cns3844 = parseNum("1")
var cns3845 = parseNum("0")
function table_concat (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_11, reg_19, reg_20, reg_21, reg_24, reg_25, reg_32, reg_33, reg_39, reg_40, reg_46, reg_47, reg_59, reg_61, reg_63, reg_64, reg_65, reg_67, reg_77;
  var goto_99=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_10 = get(reg_1.a, cns3828);
  reg_11 = newStack();
  push(reg_11, reg_4);
  if (tobool(ne(first(call(reg_10, reg_11)), cns3829))) {
    reg_19 = get(reg_1.a, cns3830);
    reg_20 = newStack();
    reg_21 = cns3831;
    reg_24 = get(reg_1.a, cns3832);
    reg_25 = newStack();
    push(reg_25, reg_4);
    push(reg_20, concat(reg_21, concat(first(call(reg_24, reg_25)), cns3833)));
    reg_31 = call(reg_19, reg_20);
  }
  reg_32 = reg_1.b;
  reg_33 = newStack();
  push(reg_33, reg_5);
  push(reg_33, cns3834);
  push(reg_33, cns3835);
  push(reg_33, cns3836);
  reg_5 = first(call(reg_32, reg_33));
  reg_39 = reg_1.c;
  reg_40 = newStack();
  push(reg_40, reg_6);
  push(reg_40, cns3837);
  push(reg_40, cns3838);
  push(reg_40, cns3839);
  reg_6 = first(call(reg_39, reg_40));
  reg_46 = reg_1.c;
  reg_47 = newStack();
  push(reg_47, reg_7);
  push(reg_47, length(reg_4));
  push(reg_47, cns3840);
  push(reg_47, cns3841);
  reg_7 = first(call(reg_46, reg_47));
  if (tobool(gt(reg_7, length(reg_4)))) {
    reg_7 = length(reg_4);
  }
  if (tobool(gt(reg_6, reg_7))) {
    reg_59 = newStack();
    push(reg_59, cns3842);
    return reg_59;
  }
  reg_61 = get(reg_4, reg_6);
  reg_63 = add(reg_6, cns3843);
  reg_64 = reg_7;
  reg_65 = cns3844;
  reg_67 = lt(reg_65, cns3845);
  loop_1: while (true) {
    goto_99 = tobool(reg_67);
    if (!goto_99) {
      if (tobool(gt(reg_63, reg_64))) break loop_1;
    }
    if ((goto_99 || false)) {
      goto_99 = false;
      if (tobool(lt(reg_63, reg_64))) break loop_1;
    }
    reg_61 = concat(reg_61, reg_5);
    reg_61 = concat(reg_61, get(reg_4, reg_63));
    reg_63 = add(reg_63, reg_65);
  }
  reg_77 = newStack();
  push(reg_77, reg_61);
  return reg_77;
}
var cns3846 = anyStr("table")
var cns3847 = anyStr("insert")
var cns3848 = parseNum("1")
var cns3849 = parseNum("2")
var cns3850 = anyStr("table.insert")
var cns3851 = parseNum("1")
var cns3852 = parseNum("0")
var cns3853 = parseNum("1")
function table_insert (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_13, reg_14, reg_23, reg_24, reg_26, reg_28, reg_38;
  var goto_43=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  if (tobool(eq(reg_6, nil()))) {
    reg_6 = reg_5;
    reg_5 = add(length(reg_4), cns3848);
  }
  reg_13 = reg_1.c;
  reg_14 = newStack();
  push(reg_14, reg_5);
  push(reg_14, nil());
  push(reg_14, cns3849);
  push(reg_14, cns3850);
  reg_5 = first(call(reg_13, reg_14));
  if (tobool(le(reg_5, length(reg_4)))) {
    reg_23 = length(reg_4);
    reg_24 = reg_5;
    reg_26 = unm(cns3851);
    reg_28 = lt(reg_26, cns3852);
    loop_1: while (true) {
      goto_43 = tobool(reg_28);
      if (!goto_43) {
        if (tobool(gt(reg_23, reg_24))) break loop_1;
      }
      if ((goto_43 || false)) {
        goto_43 = false;
        if (tobool(lt(reg_23, reg_24))) break loop_1;
      }
      set(reg_4, add(reg_23, cns3853), get(reg_4, reg_23));
      reg_23 = add(reg_23, reg_26);
    }
  }
  set(reg_4, reg_5, reg_6);
  reg_38 = newStack();
  return reg_38;
}
var cns3854 = anyStr("table")
var cns3855 = anyStr("move")
var cns3856 = parseNum("1")
var cns3857 = parseNum("1")
var cns3858 = parseNum("1")
var cns3859 = parseNum("1")
function table_move (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_8, reg_9, reg_11, reg_31;
  var goto_32=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = next(reg_0);
  reg_9 = reg_8;
  if (!tobool(reg_8)) {
    reg_9 = reg_4;
  }
  reg_8 = reg_9;
  reg_11 = gt(reg_7, reg_6);
  if (!tobool(reg_11)) {
    reg_11 = le(reg_7, reg_5);
  }
  goto_32 = !tobool(reg_11);
  if (!goto_32) {
    loop_2: while (tobool(le(reg_5, reg_6))) {
      set(reg_8, reg_7, get(reg_4, reg_5));
      reg_7 = add(reg_7, cns3856);
      reg_5 = add(reg_5, cns3857);
    }
  }
  if ((goto_32 || false)) {
    goto_32 = false;
    reg_7 = add(reg_7, sub(reg_6, reg_5));
    loop_1: while (tobool(ge(reg_6, reg_5))) {
      set(reg_8, reg_7, get(reg_4, reg_6));
      reg_7 = sub(reg_7, cns3858);
      reg_6 = sub(reg_6, cns3859);
    }
  }
  reg_31 = newStack();
  push(reg_31, reg_8);
  return reg_31;
}
var cns3860 = anyStr("table")
var cns3861 = anyStr("remove")
var cns3862 = parseNum("1")
var cns3863 = anyStr("error")
var cns3864 = anyStr("bad argument #1 to 'table.remove' (position out of bounds)")
var cns3865 = anyStr("table")
var cns3866 = anyStr("move")
var cns3867 = parseNum("1")
function table_remove (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_13, reg_18, reg_19, reg_22, reg_29, reg_30, reg_35;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = length(reg_4);
  if (tobool(not(reg_5))) {
    reg_5 = reg_6;
  }
  if (tobool(eq(reg_5, add(reg_6, cns3862)))) {
    reg_13 = newStack();
    return reg_13;
  }
  if (tobool(gt(reg_5, reg_6))) {
    reg_18 = get(reg_1.a, cns3863);
    reg_19 = newStack();
    push(reg_19, cns3864);
    reg_21 = call(reg_18, reg_19);
  }
  reg_22 = get(reg_4, reg_5);
  if (tobool(lt(reg_5, reg_6))) {
    reg_29 = get(get(reg_1.a, cns3865), cns3866);
    reg_30 = newStack();
    push(reg_30, reg_4);
    push(reg_30, add(reg_5, cns3867));
    push(reg_30, reg_6);
    push(reg_30, reg_5);
    reg_33 = call(reg_29, reg_30);
  }
  set(reg_4, reg_6, nil());
  reg_35 = newStack();
  push(reg_35, reg_22);
  return reg_35;
}
function lua_lib_table$lua_main (reg_0) {
  var reg_1, reg_2, reg_4, reg_8, reg_17, reg_18, reg_23, reg_24, reg_29, reg_30, reg_35, reg_36, reg_39;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1, c: reg_1};
  reg_2.a = reg_0;
  reg_4 = anyFn((function (a) { return lua_lib_table$function(a,this) }).bind(reg_2));
  set(reg_2.a, cns3802, reg_4);
  reg_8 = anyFn((function (a) { return function$52(a,this) }).bind(reg_2));
  set(reg_2.a, cns3804, reg_8);
  reg_2.b = anyFn((function (a) { return function$54(a,this) }).bind(reg_2));
  reg_2.c = anyFn((function (a) { return function$55(a,this) }).bind(reg_2));
  reg_17 = get(reg_2.a, cns3826);
  reg_18 = cns3827;
  set(reg_17, reg_18, anyFn((function (a) { return table_concat(a,this) }).bind(reg_2)));
  reg_23 = get(reg_2.a, cns3846);
  reg_24 = cns3847;
  set(reg_23, reg_24, anyFn((function (a) { return table_insert(a,this) }).bind(reg_2)));
  reg_29 = get(reg_2.a, cns3854);
  reg_30 = cns3855;
  set(reg_29, reg_30, anyFn((function (a) { return table_move(a,this) }).bind(reg_2)));
  reg_35 = get(reg_2.a, cns3860);
  reg_36 = cns3861;
  set(reg_35, reg_36, anyFn((function (a) { return table_remove(a,this) }).bind(reg_2)));
  reg_39 = newStack();
  return reg_39;
}
function str_get_nums (reg_0) {
  var reg_2, reg_4, reg_5;
  reg_2 = getNum(next(reg_0));
  reg_4 = getNum(next(reg_0));
  reg_5 = (reg_2 instanceof type_8);
  if (!reg_5) {
    reg_5 = (reg_4 instanceof type_8);
  }
  if (reg_5) {
    throw new Error("attempt to perform arithmetic on a string");
  }
  return [reg_2, reg_4];
}
function _stradd (reg_0) {
  var reg_3, reg_4, reg_6;
  var _r = str_get_nums(reg_0); reg_3 = _r[0]; reg_4 = _r[1];
  reg_6 = stackof(add(reg_3, reg_4));
  return reg_6;
}
function _strsub (reg_0) {
  var reg_3, reg_4, reg_6;
  var _r = str_get_nums(reg_0); reg_3 = _r[0]; reg_4 = _r[1];
  reg_6 = stackof(sub(reg_3, reg_4));
  return reg_6;
}
function _strmul (reg_0) {
  var reg_3, reg_4, reg_6;
  var _r = str_get_nums(reg_0); reg_3 = _r[0]; reg_4 = _r[1];
  reg_6 = stackof(mul(reg_3, reg_4));
  return reg_6;
}
function div (reg_0, reg_1) {
  var reg_2, reg_3, reg_5, reg_6, reg_7, reg_9, reg_11;
  var _r = getFloats(reg_0, reg_1); reg_5 = _r[0]; reg_6 = _r[1]; reg_7 = _r[2];
  reg_2 = reg_5;
  reg_3 = reg_6;
  if (reg_7) {
    reg_9 = (reg_2 / reg_3);
    return reg_9;
  }
  reg_11 = meta_arith(reg_0, reg_1, "__div");
  return reg_11;
}
function _strdiv (reg_0) {
  var reg_3, reg_4, reg_6;
  var _r = str_get_nums(reg_0); reg_3 = _r[0]; reg_4 = _r[1];
  reg_6 = stackof(div(reg_3, reg_4));
  return reg_6;
}
function _strunm (reg_0) {
  var reg_2, reg_6;
  reg_2 = getNum(next(reg_0));
  if ((reg_2 instanceof type_8)) {
    throw new Error("attempt to perform arithmetic on a string");
  }
  reg_6 = stackof(unm(reg_2));
  return reg_6;
}
function simple_string (reg_0, reg_1, reg_2) {
  var reg_4, reg_7;
  if (testStr(reg_0)) {
    reg_4 = getStr(reg_0);
    return reg_4;
  }
  if (testInt(reg_0)) {
    reg_7 = String(getInt(reg_0));
    return reg_7;
  }
  throw new Error((((((("Lua: bad argument #" + reg_1) + " to '") + reg_2) + "' (string expected, got ") + typestr(reg_0)) + ")"));
}
function valid_start_index (reg_0, reg_1) {
  var reg_9;
  var goto_6=false;
  goto_6 = !(reg_0 < 0);
  if (!goto_6) {
    reg_0 = (reg_1 + reg_0);
  }
  if ((goto_6 || false)) {
    goto_6 = false;
    reg_0 = (reg_0 - 1);
  }
  if ((reg_0 < 0)) {
    reg_9 = 0;
    return reg_9;
  }
  return reg_0;
}
function valid_end_index (reg_0, reg_1) {
  var reg_9;
  var goto_6=false;
  goto_6 = !(reg_0 < 0);
  if (!goto_6) {
    reg_0 = (reg_1 + reg_0);
  }
  if ((goto_6 || false)) {
    goto_6 = false;
    reg_0 = (reg_0 - 1);
  }
  if ((reg_0 >= reg_1)) {
    reg_9 = (reg_1 - 1);
    return reg_9;
  }
  return reg_0;
}
function _strsubstr (reg_0) {
  var reg_4, reg_5, reg_10, reg_11, reg_12, reg_23;
  reg_4 = simple_string(next(reg_0), "1", "string.sub");
  reg_5 = reg_4.length;
  reg_10 = valid_start_index(simple_number(next(reg_0), "2", "string.sub"), reg_5);
  reg_11 = reg_5;
  reg_12 = next(reg_0);
  if (!testNil(reg_12)) {
    reg_11 = valid_end_index(simple_number(reg_12, "3", "string.sub"), reg_5);
  }
  reg_23 = stackof(anyStr(reg_4.slice(reg_10, (reg_11 + 1))));
  return reg_23;
}
function _strbyte (reg_0) {
  var reg_4, reg_5, reg_6, reg_7, reg_14, reg_15, reg_25, reg_27, reg_28, reg_29;
  reg_4 = simple_string(next(reg_0), "1", "string.byte");
  reg_5 = reg_4.length;
  reg_6 = 0;
  reg_7 = next(reg_0);
  if (!testNil(reg_7)) {
    reg_6 = valid_start_index(simple_number(reg_7, "2", "string.byte"), reg_5);
  }
  reg_14 = reg_6;
  reg_15 = next(reg_0);
  if (!testNil(reg_15)) {
    reg_14 = valid_end_index(simple_number(reg_15, "2", "string.byte"), reg_5);
  }
  if ((reg_14 >= reg_5)) {
    reg_14 = (reg_5 - 1);
  }
  reg_25 = newStack();
  loop_1: while ((reg_6 <= reg_14)) {
    var _r = Auro.charat(reg_4, reg_6); reg_28 = _r[0]; reg_29 = _r[1];
    reg_27 = reg_28;
    reg_6 = reg_29;
    push(reg_25, anyInt(reg_27.charCodeAt(0)));
  }
  return reg_25;
}
function _strchar (reg_0) {
  var reg_1, reg_2, reg_13;
  reg_1 = "";
  reg_2 = 1;
  loop_1: while (more(reg_0)) {
    reg_1 = (reg_1 + String.fromCharCode(simple_number(next(reg_0), String(reg_2), "string.char")));
    reg_2 = (reg_2 + 1);
  }
  reg_13 = stackof(anyStr(reg_1));
  return reg_13;
}
var cns3868 = parseNum("1")
var cns3869 = anyStr("type")
var cns3870 = anyStr("number")
var cns3871 = anyStr("tostring")
var cns3872 = anyStr("type")
var cns3873 = anyStr("string")
var cns3874 = anyStr("error")
var cns3875 = anyStr("bad argument #")
var cns3876 = anyStr(" to 'string.len' (string expected, got ")
var cns3877 = anyStr(")")
function lua_lib_string$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_11, reg_12, reg_14, reg_20, reg_21, reg_26, reg_27, reg_33, reg_36, reg_37, reg_38, reg_39, reg_46;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = reg_5;
  if (!tobool(reg_5)) {
    reg_6 = cns3868;
  }
  reg_5 = reg_6;
  reg_11 = get(reg_1.a, cns3869);
  reg_12 = newStack();
  push(reg_12, reg_4);
  reg_14 = first(call(reg_11, reg_12));
  if (tobool(eq(reg_14, cns3870))) {
    reg_20 = get(reg_1.a, cns3871);
    reg_21 = newStack();
    push(reg_21, reg_4);
    reg_4 = first(call(reg_20, reg_21));
  }
  reg_26 = get(reg_1.a, cns3872);
  reg_27 = newStack();
  push(reg_27, reg_4);
  if (tobool(eq(first(call(reg_26, reg_27)), cns3873))) {
    reg_33 = newStack();
    push(reg_33, reg_4);
    return reg_33;
  }
  reg_36 = get(reg_1.a, cns3874);
  reg_37 = newStack();
  reg_38 = cns3875;
  reg_39 = cns3876;
  push(reg_37, concat(reg_38, concat(reg_5, concat(reg_39, concat(reg_14, cns3877)))));
  reg_45 = call(reg_36, reg_37);
  reg_46 = newStack();
  return reg_46;
}
var cns3878 = anyStr("string")
var cns3879 = anyStr("len")
function string_len (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.b;
  reg_7 = newStack();
  push(reg_7, reg_4);
  push(reg_5, length(first(call(reg_6, reg_7))));
  return reg_5;
}
var cns3880 = anyStr("string")
var cns3881 = anyStr("upper")
var cns3882 = anyStr("")
var cns3883 = parseNum("1")
var cns3884 = parseNum("1")
var cns3885 = parseNum("0")
var cns3886 = anyStr("byte")
var cns3887 = parseNum("97")
var cns3888 = parseNum("122")
var cns3889 = parseNum("32")
var cns3890 = anyStr("string")
var cns3891 = anyStr("char")
function string_upper (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_11, reg_12, reg_14, reg_21, reg_22, reg_24, reg_26, reg_37, reg_38, reg_43;
  var goto_21=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = reg_1.b;
  reg_6 = newStack();
  push(reg_6, reg_4);
  reg_4 = first(call(reg_5, reg_6));
  reg_9 = cns3882;
  reg_10 = cns3883;
  reg_11 = length(reg_4);
  reg_12 = cns3884;
  reg_14 = lt(reg_12, cns3885);
  loop_1: while (true) {
    goto_21 = tobool(reg_14);
    if (!goto_21) {
      if (tobool(gt(reg_10, reg_11))) break loop_1;
    }
    if ((goto_21 || false)) {
      goto_21 = false;
      if (tobool(lt(reg_10, reg_11))) break loop_1;
    }
    reg_21 = get(reg_4, cns3886);
    reg_22 = newStack();
    push(reg_22, reg_4);
    push(reg_22, reg_10);
    reg_24 = first(call(reg_21, reg_22));
    reg_26 = ge(reg_24, cns3887);
    if (tobool(reg_26)) {
      reg_26 = le(reg_24, cns3888);
    }
    if (tobool(reg_26)) {
      reg_24 = sub(reg_24, cns3889);
    }
    reg_37 = get(get(reg_1.a, cns3890), cns3891);
    reg_38 = newStack();
    push(reg_38, reg_24);
    reg_9 = concat(reg_9, first(call(reg_37, reg_38)));
    reg_10 = add(reg_10, reg_12);
  }
  reg_43 = newStack();
  push(reg_43, reg_9);
  return reg_43;
}
var cns3892 = anyStr("string")
var cns3893 = anyStr("lower")
var cns3894 = anyStr("")
var cns3895 = parseNum("1")
var cns3896 = parseNum("1")
var cns3897 = parseNum("0")
var cns3898 = anyStr("byte")
var cns3899 = parseNum("65")
var cns3900 = parseNum("90")
var cns3901 = parseNum("32")
var cns3902 = anyStr("string")
var cns3903 = anyStr("char")
function string_lower (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_11, reg_12, reg_14, reg_21, reg_22, reg_24, reg_26, reg_37, reg_38, reg_43;
  var goto_21=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = reg_1.b;
  reg_6 = newStack();
  push(reg_6, reg_4);
  reg_4 = first(call(reg_5, reg_6));
  reg_9 = cns3894;
  reg_10 = cns3895;
  reg_11 = length(reg_4);
  reg_12 = cns3896;
  reg_14 = lt(reg_12, cns3897);
  loop_1: while (true) {
    goto_21 = tobool(reg_14);
    if (!goto_21) {
      if (tobool(gt(reg_10, reg_11))) break loop_1;
    }
    if ((goto_21 || false)) {
      goto_21 = false;
      if (tobool(lt(reg_10, reg_11))) break loop_1;
    }
    reg_21 = get(reg_4, cns3898);
    reg_22 = newStack();
    push(reg_22, reg_4);
    push(reg_22, reg_10);
    reg_24 = first(call(reg_21, reg_22));
    reg_26 = ge(reg_24, cns3899);
    if (tobool(reg_26)) {
      reg_26 = le(reg_24, cns3900);
    }
    if (tobool(reg_26)) {
      reg_24 = add(reg_24, cns3901);
    }
    reg_37 = get(get(reg_1.a, cns3902), cns3903);
    reg_38 = newStack();
    push(reg_38, reg_24);
    reg_9 = concat(reg_9, first(call(reg_37, reg_38)));
    reg_10 = add(reg_10, reg_12);
  }
  reg_43 = newStack();
  push(reg_43, reg_9);
  return reg_43;
}
var cns3904 = anyStr("string")
var cns3905 = anyStr("reverse")
var cns3906 = anyStr("")
var cns3907 = parseNum("1")
var cns3908 = parseNum("1")
var cns3909 = parseNum("0")
var cns3910 = anyStr("string")
var cns3911 = anyStr("sub")
function string_reverse (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_11, reg_13, reg_15, reg_23, reg_25, reg_26, reg_31;
  var goto_22=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = reg_1.b;
  reg_6 = newStack();
  push(reg_6, reg_4);
  reg_4 = first(call(reg_5, reg_6));
  reg_9 = cns3906;
  reg_10 = length(reg_4);
  reg_11 = cns3907;
  reg_13 = unm(cns3908);
  reg_15 = lt(reg_13, cns3909);
  loop_1: while (true) {
    goto_22 = tobool(reg_15);
    if (!goto_22) {
      if (tobool(gt(reg_10, reg_11))) break loop_1;
    }
    if ((goto_22 || false)) {
      goto_22 = false;
      if (tobool(lt(reg_10, reg_11))) break loop_1;
    }
    reg_23 = get(reg_1.a, cns3910);
    reg_25 = get(reg_23, cns3911);
    reg_26 = newStack();
    push(reg_26, reg_23);
    push(reg_26, reg_10);
    push(reg_26, reg_10);
    reg_9 = concat(reg_9, first(call(reg_25, reg_26)));
    reg_10 = add(reg_10, reg_13);
  }
  reg_31 = newStack();
  push(reg_31, reg_9);
  return reg_31;
}
var cns3912 = anyStr("string")
var cns3913 = anyStr("rep")
var cns3914 = anyStr("")
var cns3915 = parseNum("1")
var cns3916 = anyStr("")
var cns3917 = parseNum("2")
var cns3918 = parseNum("1")
var cns3919 = parseNum("0")
function string_rep (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_13, reg_15, reg_16, reg_17, reg_18, reg_20, reg_29;
  var goto_32=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = reg_6;
  if (!tobool(reg_6)) {
    reg_7 = cns3914;
  }
  reg_6 = reg_7;
  if (tobool(lt(reg_5, cns3915))) {
    reg_13 = newStack();
    push(reg_13, cns3916);
    return reg_13;
  }
  reg_15 = reg_4;
  reg_16 = cns3917;
  reg_17 = reg_5;
  reg_18 = cns3918;
  reg_20 = lt(reg_18, cns3919);
  loop_1: while (true) {
    goto_32 = tobool(reg_20);
    if (!goto_32) {
      if (tobool(gt(reg_16, reg_17))) break loop_1;
    }
    if ((goto_32 || false)) {
      goto_32 = false;
      if (tobool(lt(reg_16, reg_17))) break loop_1;
    }
    reg_15 = concat(reg_15, concat(reg_6, reg_4));
    reg_16 = add(reg_16, reg_18);
  }
  reg_29 = newStack();
  push(reg_29, reg_15);
  return reg_29;
}
var cns3920 = anyStr("_G")
var cns3921 = anyStr("utf8")
var cns3922 = anyStr("utf8")
var cns3923 = anyStr("char")
var cns3924 = parseNum("1")
var cns3925 = anyStr("ipairs")
var cns3926 = anyStr("tonumber")
var cns3927 = parseNum("127")
var cns3928 = anyStr("error")
var cns3929 = anyStr("Full unicode is not yet supported, only ASCII")
var cns3930 = anyStr("string")
var cns3931 = anyStr("char")
function utf8_char (reg_0, reg_1) {
  var reg_4, reg_9, reg_10, reg_11, reg_12, reg_13, reg_14, reg_15, reg_16, reg_18, reg_24, reg_25, reg_33, reg_34, reg_37, reg_42, reg_43;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newTable();
  table_append(reg_4, cns3924, copy(reg_0));
  reg_9 = get(reg_1.a, cns3925);
  reg_10 = newStack();
  push(reg_10, reg_4);
  reg_11 = call(reg_9, reg_10);
  reg_12 = next(reg_11);
  reg_13 = next(reg_11);
  reg_14 = next(reg_11);
  loop_1: while (true) {
    reg_15 = newStack();
    push(reg_15, reg_13);
    push(reg_15, reg_14);
    reg_16 = call(reg_12, reg_15);
    reg_14 = next(reg_16);
    reg_18 = next(reg_16);
    if (tobool(eq(reg_14, nil()))) break loop_1;
    reg_24 = get(reg_1.a, cns3926);
    reg_25 = newStack();
    push(reg_25, reg_18);
    if (tobool(gt(first(call(reg_24, reg_25)), cns3927))) {
      reg_33 = get(reg_1.a, cns3928);
      reg_34 = newStack();
      push(reg_34, cns3929);
      reg_36 = call(reg_33, reg_34);
    }
  }
  reg_37 = newStack();
  reg_42 = get(get(reg_1.a, cns3930), cns3931);
  reg_43 = newStack();
  append(reg_43, reg_0);
  append(reg_37, call(reg_42, reg_43));
  return reg_37;
}
function lua_lib_string$lua_main (reg_0) {
  var reg_1, reg_2, reg_7, reg_8, reg_13, reg_14, reg_19, reg_20, reg_25, reg_26, reg_31, reg_32, reg_42, reg_43, reg_46;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1};
  reg_2.a = reg_0;
  reg_2.b = anyFn((function (a) { return lua_lib_string$function(a,this) }).bind(reg_2));
  reg_7 = get(reg_2.a, cns3878);
  reg_8 = cns3879;
  set(reg_7, reg_8, anyFn((function (a) { return string_len(a,this) }).bind(reg_2)));
  reg_13 = get(reg_2.a, cns3880);
  reg_14 = cns3881;
  set(reg_13, reg_14, anyFn((function (a) { return string_upper(a,this) }).bind(reg_2)));
  reg_19 = get(reg_2.a, cns3892);
  reg_20 = cns3893;
  set(reg_19, reg_20, anyFn((function (a) { return string_lower(a,this) }).bind(reg_2)));
  reg_25 = get(reg_2.a, cns3904);
  reg_26 = cns3905;
  set(reg_25, reg_26, anyFn((function (a) { return string_reverse(a,this) }).bind(reg_2)));
  reg_31 = get(reg_2.a, cns3912);
  reg_32 = cns3913;
  set(reg_31, reg_32, anyFn((function (a) { return string_rep(a,this) }).bind(reg_2)));
  set(get(reg_2.a, cns3920), cns3921, newTable());
  reg_42 = get(reg_2.a, cns3922);
  reg_43 = cns3923;
  set(reg_42, reg_43, anyFn((function (a) { return utf8_char(a,this) }).bind(reg_2)));
  reg_46 = newStack();
  return reg_46;
}
var record_51 = function (val) { this.val = val; }
var cns3932 = anyStr("string")
var cns3933 = anyStr("charat")
var record_52 = function (val) { this.val = val; }
var cns3934 = anyStr("sub")
function string_charat (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_8, reg_9;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_8 = get(reg_4, cns3934);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, reg_5);
  push(reg_9, reg_5);
  append(reg_6, call(reg_8, reg_9));
  return reg_6;
}
var cns3935 = parseNum("48")
var cns3936 = parseNum("57")
function lua_lib_pattern$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = ge(reg_4, cns3935);
  if (tobool(reg_7)) {
    reg_7 = le(reg_4, cns3936);
  }
  push(reg_5, reg_7);
  return reg_5;
}
var cns3937 = parseNum("97")
var cns3938 = parseNum("122")
function function$56 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = ge(reg_4, cns3937);
  if (tobool(reg_7)) {
    reg_7 = le(reg_4, cns3938);
  }
  push(reg_5, reg_7);
  return reg_5;
}
var cns3939 = parseNum("65")
var cns3940 = parseNum("90")
function function$57 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = ge(reg_4, cns3939);
  if (tobool(reg_7)) {
    reg_7 = le(reg_4, cns3940);
  }
  push(reg_5, reg_7);
  return reg_5;
}
function function$58 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_9, reg_11, reg_12;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.a;
  reg_7 = newStack();
  push(reg_7, reg_4);
  reg_9 = first(call(reg_6, reg_7));
  if (!tobool(reg_9)) {
    reg_11 = reg_1.b;
    reg_12 = newStack();
    push(reg_12, reg_4);
    reg_9 = first(call(reg_11, reg_12));
  }
  push(reg_5, reg_9);
  return reg_5;
}
var cns3941 = parseNum("9")
var cns3942 = parseNum("10")
var cns3943 = parseNum("32")
function function$59 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = eq(reg_4, cns3941);
  if (!tobool(reg_7)) {
    reg_7 = eq(reg_4, cns3942);
  }
  if (!tobool(reg_7)) {
    reg_7 = eq(reg_4, cns3943);
  }
  push(reg_5, reg_7);
  return reg_5;
}
var cns3944 = parseNum("32")
var cns3945 = parseNum("127")
function function$60 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = lt(reg_4, cns3944);
  if (!tobool(reg_7)) {
    reg_7 = eq(reg_4, cns3945);
  }
  push(reg_5, reg_7);
  return reg_5;
}
function function$61 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_12, reg_13;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.c;
  reg_7 = newStack();
  push(reg_7, reg_4);
  reg_10 = not(first(call(reg_6, reg_7)));
  if (tobool(reg_10)) {
    reg_12 = reg_1.d;
    reg_13 = newStack();
    push(reg_13, reg_4);
    reg_10 = not(first(call(reg_12, reg_13)));
  }
  push(reg_5, reg_10);
  return reg_5;
}
function function$62 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_9, reg_11, reg_12;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.e;
  reg_7 = newStack();
  push(reg_7, reg_4);
  reg_9 = first(call(reg_6, reg_7));
  if (!tobool(reg_9)) {
    reg_11 = reg_1.f;
    reg_12 = newStack();
    push(reg_12, reg_4);
    reg_9 = first(call(reg_11, reg_12));
  }
  push(reg_5, reg_9);
  return reg_5;
}
function function$63 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_9, reg_11, reg_12;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.g;
  reg_7 = newStack();
  push(reg_7, reg_4);
  reg_9 = first(call(reg_6, reg_7));
  if (tobool(reg_9)) {
    reg_11 = reg_1.h;
    reg_12 = newStack();
    push(reg_12, reg_4);
    reg_9 = not(first(call(reg_11, reg_12)));
  }
  push(reg_5, reg_9);
  return reg_5;
}
var cns3946 = parseNum("65")
var cns3947 = parseNum("70")
var cns3948 = parseNum("97")
var cns3949 = parseNum("102")
function function$64 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_9, reg_12, reg_18;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.f;
  reg_7 = newStack();
  push(reg_7, reg_4);
  reg_9 = first(call(reg_6, reg_7));
  if (!tobool(reg_9)) {
    reg_12 = ge(reg_4, cns3946);
    if (tobool(reg_12)) {
      reg_12 = le(reg_4, cns3947);
    }
    reg_9 = reg_12;
  }
  if (!tobool(reg_9)) {
    reg_18 = ge(reg_4, cns3948);
    if (tobool(reg_18)) {
      reg_18 = le(reg_4, cns3949);
    }
    reg_9 = reg_18;
  }
  push(reg_5, reg_9);
  return reg_5;
}
function function$65 (reg_0, reg_1) {
  var reg_4;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newStack();
  push(reg_4, lua$true());
  return reg_4;
}
var record_53 = function (val) { this.val = val; }
var record_54 = function (val) { this.val = val; }
function function$67 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_8;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = reg_1.b;
  reg_8 = newStack();
  push(reg_8, reg_5);
  push(reg_6, not(first(call(reg_7, reg_8))));
  return reg_6;
}
function function$66 (reg_0, reg_1) {
  var reg_3, reg_5;
  reg_3 = {a: reg_1, b: nil()};
  reg_3.b = next(reg_0);
  reg_5 = newStack();
  push(reg_5, anyFn((function (a) { return function$67(a,this) }).bind(reg_3)));
  return reg_5;
}
var record_55 = function (val) { this.val = val; }
var cns3950 = anyStr("byte")
var cns3951 = anyStr("byte")
var record_56 = function (val) { this.val = val; }
function function$69 (reg_0, reg_1) {
  var reg_5, reg_6, reg_8;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_8 = ge(reg_5, reg_1.b);
  if (tobool(reg_8)) {
    reg_8 = le(reg_5, reg_1.c);
  }
  push(reg_6, reg_8);
  return reg_6;
}
function function$68 (reg_0, reg_1) {
  var reg_2, reg_3, reg_4, reg_5, reg_7, reg_8, reg_12, reg_13, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = get(reg_4, cns3950);
  reg_8 = newStack();
  push(reg_8, reg_4);
  reg_3.b = first(call(reg_7, reg_8));
  reg_12 = get(reg_5, cns3951);
  reg_13 = newStack();
  push(reg_13, reg_5);
  reg_3.c = first(call(reg_12, reg_13));
  reg_16 = newStack();
  push(reg_16, anyFn((function (a) { return function$69(a,this) }).bind(reg_3)));
  return reg_16;
}
var cns3952 = parseNum("1")
var cns3953 = anyStr("byte")
var cns3954 = parseNum("1")
var cns3955 = anyStr("ipairs")
function function$71 (reg_0, reg_1) {
  var reg_2, reg_5, reg_8, reg_9, reg_11, reg_12, reg_13, reg_14, reg_15, reg_16, reg_18, reg_24, reg_26;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_8 = get(reg_2.i, cns3955);
  reg_9 = newStack();
  push(reg_9, reg_1.b);
  reg_11 = call(reg_8, reg_9);
  reg_12 = next(reg_11);
  reg_13 = next(reg_11);
  reg_14 = next(reg_11);
  loop_1: while (true) {
    reg_15 = newStack();
    push(reg_15, reg_13);
    push(reg_15, reg_14);
    reg_16 = call(reg_12, reg_15);
    reg_14 = next(reg_16);
    reg_18 = next(reg_16);
    if (tobool(eq(reg_14, nil()))) break loop_1;
    if (tobool(eq(reg_5, reg_18))) {
      reg_24 = newStack();
      push(reg_24, lua$true());
      return reg_24;
    }
  }
  reg_26 = newStack();
  push(reg_26, lua$false());
  return reg_26;
}
function function$70 (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_6, reg_8, reg_9, reg_13;
  reg_3 = {a: reg_1, b: nil()};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  reg_6 = cns3952;
  reg_8 = get(reg_4, cns3953);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, cns3954);
  push(reg_9, length(reg_4));
  table_append(reg_5, reg_6, call(reg_8, reg_9));
  reg_3.b = reg_5;
  reg_13 = newStack();
  push(reg_13, anyFn((function (a) { return function$71(a,this) }).bind(reg_3)));
  return reg_13;
}
var cns3956 = anyStr("charat")
var cns3957 = anyStr("%")
var cns3958 = anyStr("charat")
var cns3959 = parseNum("1")
var cns3960 = anyStr("lower")
var cns3961 = anyStr("a")
var cns3962 = anyStr("c")
var cns3963 = anyStr("d")
var cns3964 = anyStr("g")
var cns3965 = anyStr("l")
var cns3966 = anyStr("p")
var cns3967 = anyStr("s")
var cns3968 = anyStr("u")
var cns3969 = anyStr("w")
var cns3970 = anyStr("x")
var cns3971 = parseNum("2")
var cns3972 = parseNum("2")
function function$72 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_14, reg_15, reg_17, reg_18, reg_22, reg_24, reg_25, reg_27, reg_71, reg_72, reg_75, reg_78, reg_79, reg_80, reg_85;
  var goto_41=false, goto_48=false, goto_55=false, goto_62=false, goto_69=false, goto_76=false, goto_83=false, goto_90=false, goto_97=false, goto_123=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = get(reg_4, cns3956);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, reg_5);
  if (tobool(ne(first(call(reg_7, reg_8)), cns3957))) {
    reg_14 = newStack();
    return reg_14;
  }
  reg_15 = nil();
  reg_17 = get(reg_4, cns3958);
  reg_18 = newStack();
  push(reg_18, reg_4);
  push(reg_18, add(reg_5, cns3959));
  reg_22 = first(call(reg_17, reg_18));
  reg_24 = get(reg_22, cns3960);
  reg_25 = newStack();
  push(reg_25, reg_22);
  reg_27 = first(call(reg_24, reg_25));
  goto_41 = !tobool(eq(reg_27, cns3961));
  if (!goto_41) {
    reg_15 = reg_1.e;
  }
  if ((goto_41 || false)) {
    goto_41 = false;
    goto_48 = !tobool(eq(reg_27, cns3962));
    if (!goto_48) {
      reg_15 = reg_1.c;
    }
    if ((goto_48 || false)) {
      goto_48 = false;
      goto_55 = !tobool(eq(reg_27, cns3963));
      if (!goto_55) {
        reg_15 = reg_1.f;
      }
      if ((goto_55 || false)) {
        goto_55 = false;
        goto_62 = !tobool(eq(reg_27, cns3964));
        if (!goto_62) {
          reg_15 = reg_1.g;
        }
        if ((goto_62 || false)) {
          goto_62 = false;
          goto_69 = !tobool(eq(reg_27, cns3965));
          if (!goto_69) {
            reg_15 = reg_1.a;
          }
          if ((goto_69 || false)) {
            goto_69 = false;
            goto_76 = !tobool(eq(reg_27, cns3966));
            if (!goto_76) {
              reg_15 = reg_1.j;
            }
            if ((goto_76 || false)) {
              goto_76 = false;
              goto_83 = !tobool(eq(reg_27, cns3967));
              if (!goto_83) {
                reg_15 = reg_1.d;
              }
              if ((goto_83 || false)) {
                goto_83 = false;
                goto_90 = !tobool(eq(reg_27, cns3968));
                if (!goto_90) {
                  reg_15 = reg_1.b;
                }
                if ((goto_90 || false)) {
                  goto_90 = false;
                  goto_97 = !tobool(eq(reg_27, cns3969));
                  if (!goto_97) {
                    reg_15 = reg_1.h;
                  }
                  if ((goto_97 || false)) {
                    goto_97 = false;
                    if (tobool(eq(reg_27, cns3970))) {
                      reg_15 = reg_1.k;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  goto_123 = !tobool(reg_15);
  if (!goto_123) {
    if (tobool(ne(reg_27, reg_22))) {
      reg_71 = reg_1.l;
      reg_72 = newStack();
      push(reg_72, reg_15);
      reg_15 = first(call(reg_71, reg_72));
    }
    reg_75 = newStack();
    push(reg_75, reg_15);
    push(reg_75, add(reg_5, cns3971));
    return reg_75;
  }
  if ((goto_123 || false)) {
    goto_123 = false;
    reg_78 = newStack();
    reg_79 = reg_1.m;
    reg_80 = newStack();
    push(reg_80, reg_22);
    push(reg_78, first(call(reg_79, reg_80)));
    push(reg_78, add(reg_5, cns3972));
    return reg_78;
  }
  reg_85 = newStack();
  return reg_85;
}
var cns3973 = anyStr("charat")
var cns3974 = anyStr("^")
var cns3975 = parseNum("1")
var cns3976 = anyStr("")
var cns3977 = anyStr("]")
var cns3978 = anyStr("charat")
var cns3979 = parseNum("1")
var cns3980 = anyStr("-")
var cns3981 = anyStr("charat")
var cns3982 = parseNum("2")
var cns3983 = parseNum("2")
var cns3984 = parseNum("1")
var cns3985 = anyStr("table")
var cns3986 = anyStr("insert")
var cns3987 = anyStr("sub")
var cns3988 = anyStr("")
var cns3989 = anyStr("table")
var cns3990 = anyStr("insert")
var cns3991 = anyStr("]")
var cns3992 = anyStr("ipairs")
function f (reg_0, reg_1) {
  var reg_2, reg_5, reg_8, reg_9, reg_11, reg_12, reg_13, reg_14, reg_15, reg_16, reg_18, reg_22, reg_26, reg_28;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_8 = get(reg_2.i, cns3992);
  reg_9 = newStack();
  push(reg_9, reg_1.b);
  reg_11 = call(reg_8, reg_9);
  reg_12 = next(reg_11);
  reg_13 = next(reg_11);
  reg_14 = next(reg_11);
  loop_1: while (true) {
    reg_15 = newStack();
    push(reg_15, reg_13);
    push(reg_15, reg_14);
    reg_16 = call(reg_12, reg_15);
    reg_14 = next(reg_16);
    reg_18 = next(reg_16);
    if (tobool(eq(reg_14, nil()))) break loop_1;
    reg_22 = newStack();
    push(reg_22, reg_5);
    if (tobool(first(call(reg_18, reg_22)))) {
      reg_26 = newStack();
      push(reg_26, lua$true());
      return reg_26;
    }
  }
  reg_28 = newStack();
  push(reg_28, lua$false());
  return reg_28;
}
var cns3993 = parseNum("1")
function function$73 (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_7, reg_8, reg_10, reg_14, reg_15, reg_18, reg_19, reg_20, reg_21, reg_22, reg_23, reg_26, reg_28, reg_33, reg_34, reg_36, reg_37, reg_38, reg_41, reg_42, reg_50, reg_51, reg_53, reg_54, reg_70, reg_71, reg_75, reg_76, reg_86, reg_87, reg_89, reg_90, reg_97, reg_98, reg_101;
  var goto_58=false, goto_90=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = get(reg_4, cns3973);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, reg_5);
  reg_10 = first(call(reg_7, reg_8));
  if (tobool(eq(reg_10, cns3974))) {
    reg_14 = reg_1.n;
    reg_15 = newStack();
    push(reg_15, reg_4);
    push(reg_15, add(reg_5, cns3975));
    reg_18 = call(reg_14, reg_15);
    reg_19 = next(reg_18);
    reg_20 = next(reg_18);
    reg_21 = newStack();
    reg_22 = reg_1.l;
    reg_23 = newStack();
    push(reg_23, reg_19);
    push(reg_21, first(call(reg_22, reg_23)));
    push(reg_21, reg_20);
    return reg_21;
  }
  reg_26 = cns3976;
  reg_3.b = newTable();
  loop_1: while (true) {
    reg_28 = reg_10;
    if (tobool(reg_10)) {
      reg_28 = ne(reg_10, cns3977);
    }
    if (!tobool(reg_28)) break loop_1;
    reg_33 = reg_1.o;
    reg_34 = newStack();
    push(reg_34, reg_4);
    push(reg_34, reg_5);
    push(reg_34, lua$true());
    reg_36 = call(reg_33, reg_34);
    reg_37 = next(reg_36);
    reg_38 = next(reg_36);
    goto_58 = !tobool(reg_37);
    if (!goto_58) {
      reg_5 = reg_38;
    }
    if ((goto_58 || false)) {
      goto_58 = false;
      reg_41 = get(reg_4, cns3978);
      reg_42 = newStack();
      push(reg_42, reg_4);
      push(reg_42, add(reg_5, cns3979));
      goto_90 = !tobool(eq(first(call(reg_41, reg_42)), cns3980));
      if (!goto_90) {
        reg_50 = reg_1.p;
        reg_51 = newStack();
        push(reg_51, reg_10);
        reg_53 = get(reg_4, cns3981);
        reg_54 = newStack();
        push(reg_54, reg_4);
        push(reg_54, add(reg_5, cns3982));
        append(reg_51, call(reg_53, reg_54));
        reg_37 = first(call(reg_50, reg_51));
        reg_5 = add(reg_5, cns3983);
      }
      if ((goto_90 || false)) {
        goto_90 = false;
        reg_26 = concat(reg_26, reg_10);
        reg_5 = add(reg_5, cns3984);
      }
    }
    if (tobool(reg_37)) {
      reg_70 = get(get(reg_1.i, cns3985), cns3986);
      reg_71 = newStack();
      push(reg_71, reg_3.b);
      push(reg_71, reg_37);
      reg_73 = call(reg_70, reg_71);
    }
    reg_75 = get(reg_4, cns3987);
    reg_76 = newStack();
    push(reg_76, reg_4);
    push(reg_76, reg_5);
    push(reg_76, reg_5);
    reg_10 = first(call(reg_75, reg_76));
  }
  if (tobool(ne(reg_26, cns3988))) {
    reg_86 = get(get(reg_1.i, cns3989), cns3990);
    reg_87 = newStack();
    push(reg_87, reg_3.b);
    reg_89 = reg_1.m;
    reg_90 = newStack();
    push(reg_90, reg_26);
    append(reg_87, call(reg_89, reg_90));
    reg_92 = call(reg_86, reg_87);
  }
  if (tobool(eq(reg_10, cns3991))) {
    reg_97 = anyFn((function (a) { return f(a,this) }).bind(reg_3));
    reg_98 = newStack();
    push(reg_98, reg_97);
    push(reg_98, add(reg_5, cns3993));
    return reg_98;
  }
  reg_101 = newStack();
  return reg_101;
}
var cns3994 = anyStr("charat")
var cns3995 = anyStr("[")
var cns3996 = parseNum("1")
var cns3997 = anyStr(".")
var cns3998 = parseNum("1")
function function$74 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_10, reg_14, reg_15, reg_16, reg_23, reg_27, reg_28, reg_29, reg_31;
  var goto_26=false, goto_38=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = get(reg_4, cns3994);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, reg_5);
  reg_10 = first(call(reg_7, reg_8));
  goto_26 = !tobool(eq(reg_10, cns3995));
  if (!goto_26) {
    reg_14 = newStack();
    reg_15 = reg_1.n;
    reg_16 = newStack();
    push(reg_16, reg_4);
    push(reg_16, add(reg_5, cns3996));
    append(reg_14, call(reg_15, reg_16));
    return reg_14;
  }
  if ((goto_26 || false)) {
    goto_26 = false;
    goto_38 = !tobool(eq(reg_10, cns3997));
    if (!goto_38) {
      reg_23 = newStack();
      push(reg_23, reg_1.q);
      push(reg_23, add(reg_5, cns3998));
      return reg_23;
    }
    if ((goto_38 || false)) {
      goto_38 = false;
      reg_27 = newStack();
      reg_28 = reg_1.o;
      reg_29 = newStack();
      push(reg_29, reg_4);
      push(reg_29, reg_5);
      append(reg_27, call(reg_28, reg_29));
      return reg_27;
    }
  }
  reg_31 = newStack();
  return reg_31;
}
var cns3999 = anyStr("sub")
var cns4000 = parseNum("1")
function function$76 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_9, reg_10, reg_21, reg_22, reg_23, reg_28;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_9 = get(reg_5, cns3999);
  reg_10 = newStack();
  push(reg_10, reg_5);
  push(reg_10, reg_6);
  push(reg_10, sub(add(reg_6, length(reg_1.b)), cns4000));
  if (tobool(eq(first(call(reg_9, reg_10)), reg_1.b))) {
    reg_21 = newStack();
    reg_22 = reg_1.c;
    reg_23 = newStack();
    push(reg_23, reg_5);
    push(reg_23, add(reg_6, length(reg_1.b)));
    push(reg_23, reg_7);
    append(reg_21, call(reg_22, reg_23));
    return reg_21;
  }
  reg_28 = newStack();
  return reg_28;
}
function function$75 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$76(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns4001 = anyStr("byte")
var cns4002 = parseNum("1")
function function$78 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_11, reg_12, reg_13, reg_15, reg_16, reg_21, reg_22, reg_23, reg_27;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  if (tobool(gt(reg_6, length(reg_5)))) {
    reg_11 = newStack();
    return reg_11;
  }
  reg_12 = reg_1.b;
  reg_13 = newStack();
  reg_15 = get(reg_5, cns4001);
  reg_16 = newStack();
  push(reg_16, reg_5);
  push(reg_16, reg_6);
  append(reg_13, call(reg_15, reg_16));
  if (tobool(first(call(reg_12, reg_13)))) {
    reg_21 = newStack();
    reg_22 = reg_1.c;
    reg_23 = newStack();
    push(reg_23, reg_5);
    push(reg_23, add(reg_6, cns4002));
    push(reg_23, reg_7);
    append(reg_21, call(reg_22, reg_23));
    return reg_21;
  }
  reg_27 = newStack();
  return reg_27;
}
function function$77 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$78(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns4003 = parseNum("1")
function function$79 (reg_0, reg_1) {
  var reg_5, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = newStack();
  push(reg_6, sub(reg_5, cns4003));
  return reg_6;
}
var cns4004 = anyStr("byte")
var cns4005 = parseNum("1")
var cns4006 = parseNum("1")
function function$81 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_8, reg_10, reg_12, reg_13, reg_15, reg_16, reg_25, reg_26, reg_28, reg_30, reg_33;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = reg_6;
  loop_2: while (true) {
    reg_10 = le(reg_8, length(reg_5));
    if (tobool(reg_10)) {
      reg_12 = reg_1.b;
      reg_13 = newStack();
      reg_15 = get(reg_5, cns4004);
      reg_16 = newStack();
      push(reg_16, reg_5);
      push(reg_16, reg_8);
      append(reg_13, call(reg_15, reg_16));
      reg_10 = first(call(reg_12, reg_13));
    }
    if (!tobool(reg_10)) break loop_2;
    reg_8 = add(reg_8, cns4005);
  }
  loop_1: while (tobool(ge(reg_8, reg_6))) {
    reg_25 = reg_1.c;
    reg_26 = newStack();
    push(reg_26, reg_5);
    push(reg_26, reg_8);
    push(reg_26, reg_7);
    reg_28 = first(call(reg_25, reg_26));
    if (tobool(reg_28)) {
      reg_30 = newStack();
      push(reg_30, reg_28);
      return reg_30;
    }
    reg_8 = sub(reg_8, cns4006);
  }
  reg_33 = newStack();
  return reg_33;
}
function function$80 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$81(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns4007 = anyStr("byte")
var cns4008 = parseNum("1")
function function$83 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_10, reg_11, reg_13, reg_15, reg_17, reg_19, reg_20, reg_22, reg_23, reg_30, reg_31;
  var goto_44=false;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  loop_1: while (tobool(lua$true())) {
    reg_10 = reg_1.b;
    reg_11 = newStack();
    push(reg_11, reg_5);
    push(reg_11, reg_6);
    push(reg_11, reg_7);
    reg_13 = first(call(reg_10, reg_11));
    if (tobool(reg_13)) {
      reg_15 = newStack();
      push(reg_15, reg_13);
      return reg_15;
    }
    reg_17 = le(reg_6, length(reg_5));
    if (tobool(reg_17)) {
      reg_19 = reg_1.c;
      reg_20 = newStack();
      reg_22 = get(reg_5, cns4007);
      reg_23 = newStack();
      push(reg_23, reg_5);
      push(reg_23, reg_6);
      append(reg_20, call(reg_22, reg_23));
      reg_17 = first(call(reg_19, reg_20));
    }
    goto_44 = !tobool(reg_17);
    if (!goto_44) {
      reg_6 = add(reg_6, cns4008);
    }
    if ((goto_44 || false)) {
      goto_44 = false;
      reg_30 = newStack();
      return reg_30;
    }
  }
  reg_31 = newStack();
  return reg_31;
}
function function$82 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.c = next(reg_0);
  reg_3.b = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$83(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns4009 = anyStr("byte")
var cns4010 = parseNum("1")
function function$85 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_8, reg_9, reg_11, reg_12, reg_17, reg_18, reg_22, reg_24, reg_25, reg_26, reg_27;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = reg_1.b;
  reg_9 = newStack();
  reg_11 = get(reg_5, cns4009);
  reg_12 = newStack();
  push(reg_12, reg_5);
  push(reg_12, reg_6);
  append(reg_9, call(reg_11, reg_12));
  if (tobool(first(call(reg_8, reg_9)))) {
    reg_17 = reg_1.c;
    reg_18 = newStack();
    push(reg_18, reg_5);
    push(reg_18, add(reg_6, cns4010));
    push(reg_18, reg_7);
    reg_22 = first(call(reg_17, reg_18));
    if (tobool(reg_22)) {
      reg_24 = newStack();
      push(reg_24, reg_22);
      return reg_24;
    }
  }
  reg_25 = newStack();
  reg_26 = reg_1.c;
  reg_27 = newStack();
  push(reg_27, reg_5);
  push(reg_27, reg_6);
  push(reg_27, reg_7);
  append(reg_25, call(reg_26, reg_27));
  return reg_25;
}
function function$84 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$85(a,this) }).bind(reg_3)));
  return reg_6;
}
function function$87 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_9, reg_10, reg_11;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  set(reg_7, reg_1.b, reg_6);
  reg_9 = newStack();
  reg_10 = reg_1.c;
  reg_11 = newStack();
  push(reg_11, reg_5);
  push(reg_11, reg_6);
  push(reg_11, reg_7);
  append(reg_9, call(reg_10, reg_11));
  return reg_9;
}
function function$86 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$87(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns4011 = anyStr("start")
function function$89 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_8, reg_9, reg_11, reg_12, reg_13;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = reg_1.b;
  reg_9 = newTable();
  set(reg_9, cns4011, reg_6);
  set(reg_7, reg_8, reg_9);
  reg_11 = newStack();
  reg_12 = reg_1.c;
  reg_13 = newStack();
  push(reg_13, reg_5);
  push(reg_13, reg_6);
  push(reg_13, reg_7);
  append(reg_11, call(reg_12, reg_13));
  return reg_11;
}
function function$88 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$89(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns4012 = anyStr("end")
var cns4013 = parseNum("1")
function function$91 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_9, reg_10, reg_13, reg_14, reg_15;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_9 = get(reg_7, reg_1.b);
  reg_10 = cns4012;
  set(reg_9, reg_10, sub(reg_6, cns4013));
  reg_13 = newStack();
  reg_14 = reg_1.c;
  reg_15 = newStack();
  push(reg_15, reg_5);
  push(reg_15, reg_6);
  push(reg_15, reg_7);
  append(reg_13, call(reg_14, reg_15));
  return reg_13;
}
function function$90 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$91(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns4014 = anyStr("table")
var cns4015 = anyStr("insert")
var cns4016 = anyStr("type")
var cns4017 = anyStr("value")
function function$93 (reg_0, reg_1) {
  var reg_2, reg_5, reg_6, reg_11, reg_12, reg_14, reg_18;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_11 = get(get(reg_2.i, cns4014), cns4015);
  reg_12 = newStack();
  push(reg_12, reg_1.b);
  reg_14 = newTable();
  set(reg_14, cns4016, reg_5);
  set(reg_14, cns4017, reg_6);
  push(reg_12, reg_14);
  reg_17 = call(reg_11, reg_12);
  reg_18 = newStack();
  return reg_18;
}
var cns4018 = parseNum("1")
var cns4019 = anyStr("")
var cns4020 = anyStr("sub")
var cns4021 = parseNum("1")
var cns4022 = anyStr("()")
var cns4023 = anyStr("()")
var cns4024 = parseNum("1")
var cns4025 = parseNum("2")
var cns4026 = anyStr("charat")
var cns4027 = anyStr("(")
var cns4028 = anyStr("(")
var cns4029 = anyStr("table")
var cns4030 = anyStr("insert")
var cns4031 = parseNum("1")
var cns4032 = parseNum("1")
var cns4033 = anyStr("charat")
var cns4034 = anyStr(")")
var cns4035 = anyStr(")")
var cns4036 = anyStr("table")
var cns4037 = anyStr("remove")
var cns4038 = parseNum("1")
var cns4039 = anyStr("")
var cns4040 = anyStr("string")
var cns4041 = anyStr("")
var cns4042 = anyStr("charat")
var cns4043 = parseNum("1")
var cns4044 = anyStr("charat")
var cns4045 = anyStr("+")
var cns4046 = anyStr("*")
var cns4047 = anyStr("-")
var cns4048 = anyStr("?")
var cns4049 = anyStr("type")
var cns4050 = anyStr("string")
var cns4051 = parseNum("1")
var cns4052 = anyStr("type")
var cns4053 = anyStr("string")
var cns4054 = anyStr("class")
var cns4055 = anyStr("")
var cns4056 = anyStr("string")
var cns4057 = parseNum("1")
var cns4058 = parseNum("1")
var cns4059 = parseNum("0")
var cns4060 = anyStr("type")
var cns4061 = anyStr("string")
var cns4062 = anyStr("value")
var cns4063 = anyStr("type")
var cns4064 = anyStr("class")
var cns4065 = anyStr("value")
var cns4066 = anyStr("type")
var cns4067 = anyStr("*")
var cns4068 = anyStr("value")
var cns4069 = anyStr("type")
var cns4070 = anyStr("+")
var cns4071 = anyStr("value")
var cns4072 = anyStr("value")
var cns4073 = anyStr("type")
var cns4074 = anyStr("-")
var cns4075 = anyStr("value")
var cns4076 = anyStr("type")
var cns4077 = anyStr("?")
var cns4078 = anyStr("value")
var cns4079 = anyStr("type")
var cns4080 = anyStr("()")
var cns4081 = anyStr("value")
var cns4082 = anyStr("type")
var cns4083 = anyStr("(")
var cns4084 = anyStr("value")
var cns4085 = anyStr("type")
var cns4086 = anyStr(")")
var cns4087 = anyStr("value")
function function$92 (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_8, reg_9, reg_10, reg_11, reg_16, reg_17, reg_25, reg_33, reg_34, reg_40, reg_47, reg_48, reg_55, reg_56, reg_62, reg_68, reg_69, reg_74, reg_75, reg_76, reg_77, reg_78, reg_83, reg_88, reg_89, reg_95, reg_96, reg_98, reg_100, reg_113, reg_114, reg_120, reg_121, reg_124, reg_130, reg_131, reg_138, reg_144, reg_148, reg_150, reg_151, reg_153, reg_155, reg_162, reg_168, reg_169, reg_179, reg_180, reg_190, reg_191, reg_201, reg_202, reg_207, reg_208, reg_218, reg_219, reg_229, reg_230, reg_240, reg_241, reg_251, reg_252, reg_262, reg_263, reg_269;
  var goto_41=false, goto_73=false, goto_101=false, goto_124=false, goto_187=false, goto_201=false, goto_232=false, goto_253=false, goto_269=false, goto_285=false, goto_310=false, goto_326=false, goto_342=false, goto_358=false, goto_374=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_3.b = newTable();
  reg_8 = anyFn((function (a) { return function$93(a,this) }).bind(reg_3));
  reg_9 = cns4018;
  reg_10 = newTable();
  reg_11 = cns4019;
  loop_2: while (tobool(le(reg_5, length(reg_4)))) {
    reg_16 = get(reg_4, cns4020);
    reg_17 = newStack();
    push(reg_17, reg_4);
    push(reg_17, reg_5);
    push(reg_17, add(reg_5, cns4021));
    goto_41 = !tobool(eq(first(call(reg_16, reg_17)), cns4022));
    if (!goto_41) {
      reg_25 = newStack();
      push(reg_25, cns4023);
      push(reg_25, reg_9);
      reg_27 = call(reg_8, reg_25);
      reg_9 = add(reg_9, cns4024);
      reg_5 = add(reg_5, cns4025);
    }
    if ((goto_41 || false)) {
      goto_41 = false;
      reg_33 = get(reg_4, cns4026);
      reg_34 = newStack();
      push(reg_34, reg_4);
      push(reg_34, reg_5);
      goto_73 = !tobool(eq(first(call(reg_33, reg_34)), cns4027));
      if (!goto_73) {
        reg_40 = newStack();
        push(reg_40, cns4028);
        push(reg_40, reg_9);
        reg_42 = call(reg_8, reg_40);
        reg_47 = get(get(reg_1.i, cns4029), cns4030);
        reg_48 = newStack();
        push(reg_48, reg_10);
        push(reg_48, reg_9);
        reg_49 = call(reg_47, reg_48);
        reg_9 = add(reg_9, cns4031);
        reg_5 = add(reg_5, cns4032);
      }
      if ((goto_73 || false)) {
        goto_73 = false;
        reg_55 = get(reg_4, cns4033);
        reg_56 = newStack();
        push(reg_56, reg_4);
        push(reg_56, reg_5);
        goto_101 = !tobool(eq(first(call(reg_55, reg_56)), cns4034));
        if (!goto_101) {
          reg_62 = newStack();
          push(reg_62, cns4035);
          reg_68 = get(get(reg_1.i, cns4036), cns4037);
          reg_69 = newStack();
          push(reg_69, reg_10);
          append(reg_62, call(reg_68, reg_69));
          reg_71 = call(reg_8, reg_62);
          reg_5 = add(reg_5, cns4038);
        }
        if ((goto_101 || false)) {
          goto_101 = false;
          reg_74 = reg_1.r;
          reg_75 = newStack();
          push(reg_75, reg_4);
          push(reg_75, reg_5);
          reg_76 = call(reg_74, reg_75);
          reg_77 = next(reg_76);
          reg_78 = next(reg_76);
          goto_124 = !tobool(reg_77);
          if (!goto_124) {
            if (tobool(ne(reg_11, cns4039))) {
              reg_83 = newStack();
              push(reg_83, cns4040);
              push(reg_83, reg_11);
              reg_85 = call(reg_8, reg_83);
              reg_11 = cns4041;
            }
            reg_5 = reg_78;
          }
          if ((goto_124 || false)) {
            goto_124 = false;
            reg_88 = get(reg_4, cns4042);
            reg_89 = newStack();
            push(reg_89, reg_4);
            push(reg_89, reg_5);
            reg_77 = first(call(reg_88, reg_89));
            reg_5 = add(reg_5, cns4043);
          }
          reg_95 = get(reg_4, cns4044);
          reg_96 = newStack();
          push(reg_96, reg_4);
          push(reg_96, reg_5);
          reg_98 = first(call(reg_95, reg_96));
          reg_100 = eq(reg_98, cns4045);
          if (!tobool(reg_100)) {
            reg_100 = eq(reg_98, cns4046);
          }
          if (!tobool(reg_100)) {
            reg_100 = eq(reg_98, cns4047);
          }
          if (!tobool(reg_100)) {
            reg_100 = eq(reg_98, cns4048);
          }
          goto_187 = !tobool(reg_100);
          if (!goto_187) {
            reg_113 = get(reg_1.i, cns4049);
            reg_114 = newStack();
            push(reg_114, reg_77);
            if (tobool(eq(first(call(reg_113, reg_114)), cns4050))) {
              reg_120 = reg_1.m;
              reg_121 = newStack();
              push(reg_121, reg_77);
              reg_77 = first(call(reg_120, reg_121));
            }
            reg_124 = newStack();
            push(reg_124, reg_98);
            push(reg_124, reg_77);
            reg_125 = call(reg_8, reg_124);
            reg_5 = add(reg_5, cns4051);
          }
          if ((goto_187 || false)) {
            goto_187 = false;
            reg_130 = get(reg_1.i, cns4052);
            reg_131 = newStack();
            push(reg_131, reg_77);
            goto_201 = !tobool(eq(first(call(reg_130, reg_131)), cns4053));
            if (!goto_201) {
              reg_11 = concat(reg_11, reg_77);
            }
            if ((goto_201 || false)) {
              goto_201 = false;
              reg_138 = newStack();
              push(reg_138, cns4054);
              push(reg_138, reg_77);
              reg_140 = call(reg_8, reg_138);
            }
          }
        }
      }
    }
  }
  if (tobool(ne(reg_11, cns4055))) {
    reg_144 = newStack();
    push(reg_144, cns4056);
    push(reg_144, reg_11);
    reg_146 = call(reg_8, reg_144);
  }
  reg_148 = reg_1.s;
  reg_150 = length(reg_3.b);
  reg_151 = cns4057;
  reg_153 = unm(cns4058);
  reg_155 = lt(reg_153, cns4059);
  loop_1: while (true) {
    goto_232 = tobool(reg_155);
    if (!goto_232) {
      if (tobool(gt(reg_150, reg_151))) break loop_1;
    }
    if ((goto_232 || false)) {
      goto_232 = false;
      if (tobool(lt(reg_150, reg_151))) break loop_1;
    }
    reg_162 = get(reg_3.b, reg_150);
    goto_253 = !tobool(eq(get(reg_162, cns4060), cns4061));
    if (!goto_253) {
      reg_168 = reg_1.t;
      reg_169 = newStack();
      push(reg_169, get(reg_162, cns4062));
      push(reg_169, reg_148);
      reg_148 = first(call(reg_168, reg_169));
    }
    if ((goto_253 || false)) {
      goto_253 = false;
      goto_269 = !tobool(eq(get(reg_162, cns4063), cns4064));
      if (!goto_269) {
        reg_179 = reg_1.u;
        reg_180 = newStack();
        push(reg_180, get(reg_162, cns4065));
        push(reg_180, reg_148);
        reg_148 = first(call(reg_179, reg_180));
      }
      if ((goto_269 || false)) {
        goto_269 = false;
        goto_285 = !tobool(eq(get(reg_162, cns4066), cns4067));
        if (!goto_285) {
          reg_190 = reg_1.v;
          reg_191 = newStack();
          push(reg_191, get(reg_162, cns4068));
          push(reg_191, reg_148);
          reg_148 = first(call(reg_190, reg_191));
        }
        if ((goto_285 || false)) {
          goto_285 = false;
          goto_310 = !tobool(eq(get(reg_162, cns4069), cns4070));
          if (!goto_310) {
            reg_201 = reg_1.v;
            reg_202 = newStack();
            push(reg_202, get(reg_162, cns4071));
            push(reg_202, reg_148);
            reg_148 = first(call(reg_201, reg_202));
            reg_207 = reg_1.u;
            reg_208 = newStack();
            push(reg_208, get(reg_162, cns4072));
            push(reg_208, reg_148);
            reg_148 = first(call(reg_207, reg_208));
          }
          if ((goto_310 || false)) {
            goto_310 = false;
            goto_326 = !tobool(eq(get(reg_162, cns4073), cns4074));
            if (!goto_326) {
              reg_218 = reg_1.w;
              reg_219 = newStack();
              push(reg_219, get(reg_162, cns4075));
              push(reg_219, reg_148);
              reg_148 = first(call(reg_218, reg_219));
            }
            if ((goto_326 || false)) {
              goto_326 = false;
              goto_342 = !tobool(eq(get(reg_162, cns4076), cns4077));
              if (!goto_342) {
                reg_229 = reg_1.x;
                reg_230 = newStack();
                push(reg_230, get(reg_162, cns4078));
                push(reg_230, reg_148);
                reg_148 = first(call(reg_229, reg_230));
              }
              if ((goto_342 || false)) {
                goto_342 = false;
                goto_358 = !tobool(eq(get(reg_162, cns4079), cns4080));
                if (!goto_358) {
                  reg_240 = reg_1.y;
                  reg_241 = newStack();
                  push(reg_241, get(reg_162, cns4081));
                  push(reg_241, reg_148);
                  reg_148 = first(call(reg_240, reg_241));
                }
                if ((goto_358 || false)) {
                  goto_358 = false;
                  goto_374 = !tobool(eq(get(reg_162, cns4082), cns4083));
                  if (!goto_374) {
                    reg_251 = reg_1.z;
                    reg_252 = newStack();
                    push(reg_252, get(reg_162, cns4084));
                    push(reg_252, reg_148);
                    reg_148 = first(call(reg_251, reg_252));
                  }
                  if ((goto_374 || false)) {
                    goto_374 = false;
                    if (tobool(eq(get(reg_162, cns4085), cns4086))) {
                      reg_262 = reg_1.aa;
                      reg_263 = newStack();
                      push(reg_263, get(reg_162, cns4087));
                      push(reg_263, reg_148);
                      reg_148 = first(call(reg_262, reg_263));
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    reg_150 = add(reg_150, reg_153);
  }
  reg_269 = newStack();
  push(reg_269, reg_148);
  return reg_269;
}
var cns4088 = parseNum("1")
var cns4089 = parseNum("1")
function function$94 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_8, reg_9, reg_12, reg_13, reg_14, reg_15;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = newTable();
  reg_8 = reg_1.ab;
  reg_9 = newStack();
  push(reg_9, reg_5);
  push(reg_9, cns4088);
  reg_12 = first(call(reg_8, reg_9));
  reg_13 = newStack();
  reg_14 = newStack();
  push(reg_14, reg_4);
  reg_15 = reg_6;
  if (!tobool(reg_6)) {
    reg_15 = cns4089;
  }
  push(reg_14, reg_15);
  append(reg_13, call(reg_12, reg_14));
  return reg_13;
}
var cns4090 = parseNum("1")
var cns4091 = parseNum("1")
var cns4092 = parseNum("0")
var cns4093 = anyStr("type")
var cns4094 = anyStr("table")
var cns4095 = anyStr("sub")
var cns4096 = anyStr("start")
var cns4097 = anyStr("end")
var cns4098 = anyStr("table")
var cns4099 = anyStr("unpack")
function function$95 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_8, reg_10, reg_16, reg_19, reg_20, reg_27, reg_28, reg_36, reg_41, reg_42;
  var goto_15=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = cns4090;
  reg_7 = length(reg_4);
  reg_8 = cns4091;
  reg_10 = lt(reg_8, cns4092);
  loop_1: while (true) {
    goto_15 = tobool(reg_10);
    if (!goto_15) {
      if (tobool(gt(reg_6, reg_7))) break loop_1;
    }
    if ((goto_15 || false)) {
      goto_15 = false;
      if (tobool(lt(reg_6, reg_7))) break loop_1;
    }
    reg_16 = get(reg_4, reg_6);
    reg_19 = get(reg_1.i, cns4093);
    reg_20 = newStack();
    push(reg_20, reg_16);
    if (tobool(eq(first(call(reg_19, reg_20)), cns4094))) {
      reg_27 = get(reg_5, cns4095);
      reg_28 = newStack();
      push(reg_28, reg_5);
      push(reg_28, get(reg_16, cns4096));
      push(reg_28, get(reg_16, cns4097));
      set(reg_4, reg_6, first(call(reg_27, reg_28)));
    }
    reg_6 = add(reg_6, reg_8);
  }
  reg_36 = newStack();
  reg_41 = get(get(reg_1.i, cns4098), cns4099);
  reg_42 = newStack();
  push(reg_42, reg_4);
  append(reg_36, call(reg_41, reg_42));
  return reg_36;
}
var cns4100 = anyStr("string")
var cns4101 = anyStr("find")
var cns4102 = anyStr("error")
var cns4103 = anyStr("bad argument #1 to 'string.find' (value expected)")
var cns4104 = anyStr("type")
var cns4105 = anyStr("number")
var cns4106 = anyStr("tostring")
var cns4107 = anyStr("type")
var cns4108 = anyStr("string")
var cns4109 = anyStr("error")
var cns4110 = anyStr("bad argument #1 to 'string.find' (string expected, got ")
var cns4111 = anyStr("type")
var cns4112 = anyStr(")")
var cns4113 = parseNum("1")
var cns4114 = anyStr("tonumber")
var cns4115 = anyStr("error")
var cns4116 = anyStr("bad argument #2 to 'string.find' (number expected, got ")
var cns4117 = anyStr("type")
var cns4118 = anyStr(")")
var cns4119 = parseNum("0")
var cns4120 = parseNum("1")
var cns4121 = parseNum("1")
var cns4122 = parseNum("1")
var cns4123 = parseNum("1")
var cns4124 = parseNum("1")
var cns4125 = parseNum("0")
var cns4126 = parseNum("1")
var cns4127 = anyStr("sub")
var cns4128 = anyStr("charat")
var cns4129 = parseNum("1")
var cns4130 = anyStr("^")
var cns4131 = parseNum("1")
var cns4132 = anyStr("sub")
var cns4133 = parseNum("2")
var cns4134 = parseNum("1")
var cns4135 = parseNum("0")
var cns4136 = anyStr("charat")
var cns4137 = parseNum("1")
var cns4138 = anyStr("$")
var cns4139 = anyStr("charat")
var cns4140 = parseNum("2")
var cns4141 = anyStr("%")
var cns4142 = anyStr("sub")
var cns4143 = parseNum("1")
var cns4144 = parseNum("2")
var cns4145 = parseNum("1")
var cns4146 = parseNum("1")
var cns4147 = parseNum("0")
function string_find (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_12, reg_13, reg_18, reg_19, reg_27, reg_28, reg_33, reg_34, reg_42, reg_43, reg_44, reg_47, reg_48, reg_55, reg_62, reg_63, reg_70, reg_71, reg_72, reg_75, reg_76, reg_95, reg_100, reg_101, reg_103, reg_112, reg_114, reg_115, reg_120, reg_122, reg_124, reg_125, reg_134, reg_135, reg_141, reg_143, reg_144, reg_150, reg_153, reg_154, reg_164, reg_165, reg_171, reg_172, reg_175, reg_176, reg_177, reg_178, reg_179, reg_181, reg_187, reg_189, reg_190, reg_194, reg_195, reg_196, reg_199;
  var goto_17=false, goto_37=false, goto_137=false, goto_164=false, goto_254=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  goto_17 = !tobool(not(reg_5));
  if (!goto_17) {
    reg_12 = get(reg_1.i, cns4102);
    reg_13 = newStack();
    push(reg_13, cns4103);
    reg_15 = call(reg_12, reg_13);
  }
  if ((goto_17 || false)) {
    goto_17 = false;
    reg_18 = get(reg_1.i, cns4104);
    reg_19 = newStack();
    push(reg_19, reg_5);
    goto_37 = !tobool(eq(first(call(reg_18, reg_19)), cns4105));
    if (!goto_37) {
      reg_27 = get(reg_1.i, cns4106);
      reg_28 = newStack();
      push(reg_28, reg_5);
      reg_5 = first(call(reg_27, reg_28));
    }
    if ((goto_37 || false)) {
      goto_37 = false;
      reg_33 = get(reg_1.i, cns4107);
      reg_34 = newStack();
      push(reg_34, reg_5);
      if (tobool(ne(first(call(reg_33, reg_34)), cns4108))) {
        reg_42 = get(reg_1.i, cns4109);
        reg_43 = newStack();
        reg_44 = cns4110;
        reg_47 = get(reg_1.i, cns4111);
        reg_48 = newStack();
        push(reg_48, reg_5);
        push(reg_43, concat(reg_44, concat(first(call(reg_47, reg_48)), cns4112)));
        reg_54 = call(reg_42, reg_43);
      }
    }
  }
  reg_55 = reg_6;
  if (tobool(eq(reg_6, nil()))) {
    reg_6 = cns4113;
  }
  reg_62 = get(reg_1.i, cns4114);
  reg_63 = newStack();
  push(reg_63, reg_6);
  reg_6 = first(call(reg_62, reg_63));
  if (tobool(not(reg_6))) {
    reg_70 = get(reg_1.i, cns4115);
    reg_71 = newStack();
    reg_72 = cns4116;
    reg_75 = get(reg_1.i, cns4117);
    reg_76 = newStack();
    push(reg_76, reg_55);
    push(reg_71, concat(reg_72, concat(first(call(reg_75, reg_76)), cns4118)));
    reg_82 = call(reg_70, reg_71);
  }
  if (tobool(lt(reg_6, cns4119))) {
    reg_6 = add(add(length(reg_4), cns4120), reg_6);
  }
  if (tobool(lt(reg_6, cns4121))) {
    reg_6 = cns4122;
  }
  goto_164 = !tobool(reg_7);
  if (!goto_164) {
    reg_95 = reg_6;
    reg_100 = sub(add(length(reg_4), cns4123), length(reg_5));
    reg_101 = cns4124;
    reg_103 = lt(reg_101, cns4125);
    loop_2: while (true) {
      goto_137 = tobool(reg_103);
      if (!goto_137) {
        if (tobool(gt(reg_95, reg_100))) break loop_2;
      }
      if ((goto_137 || false)) {
        goto_137 = false;
        if (tobool(lt(reg_95, reg_100))) break loop_2;
      }
      reg_112 = sub(add(reg_95, length(reg_5)), cns4126);
      reg_114 = get(reg_4, cns4127);
      reg_115 = newStack();
      push(reg_115, reg_4);
      push(reg_115, reg_95);
      push(reg_115, reg_112);
      if (tobool(eq(first(call(reg_114, reg_115)), reg_5))) {
        reg_120 = newStack();
        push(reg_120, reg_95);
        push(reg_120, reg_112);
        return reg_120;
      }
      reg_95 = add(reg_95, reg_101);
    }
  }
  if ((goto_164 || false)) {
    goto_164 = false;
    reg_122 = length(reg_4);
    reg_124 = get(reg_5, cns4128);
    reg_125 = newStack();
    push(reg_125, reg_5);
    push(reg_125, cns4129);
    if (tobool(eq(first(call(reg_124, reg_125)), cns4130))) {
      reg_122 = cns4131;
      reg_134 = get(reg_5, cns4132);
      reg_135 = newStack();
      push(reg_135, reg_5);
      push(reg_135, cns4133);
      push(reg_135, unm(cns4134));
      reg_5 = first(call(reg_134, reg_135));
    }
    reg_141 = cns4135;
    reg_143 = get(reg_5, cns4136);
    reg_144 = newStack();
    push(reg_144, reg_5);
    push(reg_144, unm(cns4137));
    reg_150 = eq(first(call(reg_143, reg_144)), cns4138);
    if (tobool(reg_150)) {
      reg_153 = get(reg_5, cns4139);
      reg_154 = newStack();
      push(reg_154, reg_5);
      push(reg_154, unm(cns4140));
      reg_150 = ne(first(call(reg_153, reg_154)), cns4141);
    }
    if (tobool(reg_150)) {
      reg_141 = length(reg_4);
      reg_164 = get(reg_5, cns4142);
      reg_165 = newStack();
      push(reg_165, reg_5);
      push(reg_165, cns4143);
      push(reg_165, unm(cns4144));
      reg_5 = first(call(reg_164, reg_165));
    }
    reg_171 = reg_1.ab;
    reg_172 = newStack();
    push(reg_172, reg_5);
    push(reg_172, cns4145);
    reg_175 = first(call(reg_171, reg_172));
    reg_176 = newTable();
    reg_177 = reg_6;
    reg_178 = reg_122;
    reg_179 = cns4146;
    reg_181 = lt(reg_179, cns4147);
    loop_1: while (true) {
      goto_254 = tobool(reg_181);
      if (!goto_254) {
        if (tobool(gt(reg_177, reg_178))) break loop_1;
      }
      if ((goto_254 || false)) {
        goto_254 = false;
        if (tobool(lt(reg_177, reg_178))) break loop_1;
      }
      reg_187 = newStack();
      push(reg_187, reg_4);
      push(reg_187, reg_177);
      push(reg_187, reg_176);
      reg_189 = first(call(reg_175, reg_187));
      reg_190 = reg_189;
      if (tobool(reg_189)) {
        reg_190 = ge(reg_189, reg_141);
      }
      if (tobool(reg_190)) {
        reg_194 = newStack();
        push(reg_194, reg_177);
        push(reg_194, reg_189);
        reg_195 = reg_1.ac;
        reg_196 = newStack();
        push(reg_196, reg_176);
        push(reg_196, reg_4);
        append(reg_194, call(reg_195, reg_196));
        return reg_194;
      }
      reg_177 = add(reg_177, reg_179);
    }
  }
  reg_199 = newStack();
  return reg_199;
}
var cns4148 = anyStr("string")
var cns4149 = anyStr("match")
var cns4150 = parseNum("1")
var cns4151 = anyStr("find")
var cns4152 = parseNum("2")
var cns4153 = anyStr("table")
var cns4154 = anyStr("unpack")
var cns4155 = parseNum("3")
var cns4156 = parseNum("0")
var cns4157 = anyStr("sub")
var cns4158 = parseNum("1")
var cns4159 = parseNum("2")
function string_match (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_8, reg_9, reg_11, reg_12, reg_18, reg_23, reg_24, reg_31, reg_33, reg_34, reg_40;
  var goto_36=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = newTable();
  reg_9 = cns4150;
  reg_11 = get(reg_4, cns4151);
  reg_12 = newStack();
  push(reg_12, reg_4);
  push(reg_12, reg_5);
  push(reg_12, reg_6);
  push(reg_12, reg_7);
  table_append(reg_8, reg_9, call(reg_11, reg_12));
  goto_36 = !tobool(gt(length(reg_8), cns4152));
  if (!goto_36) {
    reg_18 = newStack();
    reg_23 = get(get(reg_1.i, cns4153), cns4154);
    reg_24 = newStack();
    push(reg_24, reg_8);
    push(reg_24, cns4155);
    append(reg_18, call(reg_23, reg_24));
    return reg_18;
  }
  if ((goto_36 || false)) {
    goto_36 = false;
    if (tobool(gt(length(reg_8), cns4156))) {
      reg_31 = newStack();
      reg_33 = get(reg_4, cns4157);
      reg_34 = newStack();
      push(reg_34, reg_4);
      push(reg_34, get(reg_8, cns4158));
      push(reg_34, get(reg_8, cns4159));
      append(reg_31, call(reg_33, reg_34));
      return reg_31;
    }
  }
  reg_40 = newStack();
  return reg_40;
}
var cns4160 = anyStr("string")
var cns4161 = anyStr("gsub")
var cns4162 = parseNum("1")
function function$96 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_8;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = newStack();
  push(reg_7, reg_5);
  push(reg_7, reg_6);
  reg_8 = newTable();
  table_append(reg_8, cns4162, copy(reg_0));
  push(reg_7, reg_8);
  return reg_7;
}
var cns4163 = anyStr("type")
var cns4164 = anyStr("function")
function function$97 (reg_0, reg_1) {
  var reg_6, reg_7, reg_8;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = reg_1.b;
  reg_8 = newStack();
  append(reg_8, reg_0);
  append(reg_6, call(reg_7, reg_8));
  return reg_6;
}
var cns4165 = anyStr("type")
var cns4166 = anyStr("table")
function function$98 (reg_0, reg_1) {
  var reg_6, reg_7;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = newStack();
  push(reg_7, get(reg_1.b, reg_6));
  return reg_7;
}
var cns4167 = anyStr("type")
var cns4168 = anyStr("string")
var cns4169 = parseNum("1")
var cns4170 = anyStr("%")
var cns4171 = anyStr("%%")
var cns4172 = parseNum("1")
var cns4173 = parseNum("1")
var cns4174 = parseNum("0")
var cns4175 = anyStr("tostring")
var cns4176 = parseNum("1")
var cns4177 = anyStr("gsub")
var cns4178 = anyStr("%%([%d%%])")
function function$99 (reg_0, reg_1) {
  var reg_2, reg_5, reg_8, reg_11, reg_12, reg_13, reg_15, reg_23, reg_24, reg_31, reg_32, reg_34, reg_35;
  var goto_22=false;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = newTable();
  table_append(reg_5, cns4169, copy(reg_0));
  reg_8 = newTable();
  set(reg_8, cns4170, cns4171);
  reg_11 = cns4172;
  reg_12 = length(reg_5);
  reg_13 = cns4173;
  reg_15 = lt(reg_13, cns4174);
  loop_1: while (true) {
    goto_22 = tobool(reg_15);
    if (!goto_22) {
      if (tobool(gt(reg_11, reg_12))) break loop_1;
    }
    if ((goto_22 || false)) {
      goto_22 = false;
      if (tobool(lt(reg_11, reg_12))) break loop_1;
    }
    reg_23 = get(reg_2.i, cns4175);
    reg_24 = newStack();
    push(reg_24, sub(reg_11, cns4176));
    set(reg_8, first(call(reg_23, reg_24)), get(reg_5, reg_11));
    reg_11 = add(reg_11, reg_13);
  }
  reg_31 = newStack();
  reg_32 = reg_1.b;
  reg_34 = get(reg_32, cns4177);
  reg_35 = newStack();
  push(reg_35, reg_32);
  push(reg_35, cns4178);
  push(reg_35, reg_8);
  append(reg_31, call(reg_34, reg_35));
  return reg_31;
}
var cns4179 = anyStr("error")
var cns4180 = anyStr("bad argument #3 to 'string.gsub' (string/function/table expected)")
var cns4181 = parseNum("1")
var cns4182 = anyStr("")
var cns4183 = parseNum("0")
var cns4184 = anyStr("find")
var cns4185 = anyStr("sub")
var cns4186 = parseNum("1")
var cns4187 = anyStr("sub")
var cns4188 = anyStr("table")
var cns4189 = anyStr("unpack")
var cns4190 = parseNum("1")
var cns4191 = parseNum("1")
var cns4192 = anyStr("sub")
function string_gsub (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_7, reg_9, reg_10, reg_13, reg_14, reg_25, reg_26, reg_37, reg_38, reg_49, reg_50, reg_53, reg_54, reg_55, reg_57, reg_59, reg_63, reg_65, reg_66, reg_68, reg_69, reg_70, reg_71, reg_75, reg_76, reg_80, reg_81, reg_83, reg_84, reg_91, reg_92, reg_103, reg_104, reg_108;
  var goto_26=false, goto_42=false, goto_58=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_3.b = next(reg_0);
  reg_7 = next(reg_0);
  reg_9 = anyFn((function (a) { return function$96(a,this) }).bind(reg_3));
  reg_10 = nil();
  reg_13 = get(reg_1.i, cns4163);
  reg_14 = newStack();
  push(reg_14, reg_3.b);
  goto_26 = !tobool(eq(first(call(reg_13, reg_14)), cns4164));
  if (!goto_26) {
    reg_10 = anyFn((function (a) { return function$97(a,this) }).bind(reg_3));
  }
  if ((goto_26 || false)) {
    goto_26 = false;
    reg_25 = get(reg_1.i, cns4165);
    reg_26 = newStack();
    push(reg_26, reg_3.b);
    goto_42 = !tobool(eq(first(call(reg_25, reg_26)), cns4166));
    if (!goto_42) {
      reg_10 = anyFn((function (a) { return function$98(a,this) }).bind(reg_3));
    }
    if ((goto_42 || false)) {
      goto_42 = false;
      reg_37 = get(reg_1.i, cns4167);
      reg_38 = newStack();
      push(reg_38, reg_3.b);
      goto_58 = !tobool(eq(first(call(reg_37, reg_38)), cns4168));
      if (!goto_58) {
        reg_10 = anyFn((function (a) { return function$99(a,this) }).bind(reg_3));
      }
      if ((goto_58 || false)) {
        goto_58 = false;
        reg_49 = get(reg_1.i, cns4179);
        reg_50 = newStack();
        push(reg_50, cns4180);
        reg_52 = call(reg_49, reg_50);
      }
    }
  }
  reg_53 = cns4181;
  reg_54 = cns4182;
  reg_55 = cns4183;
  loop_1: while (true) {
    reg_57 = lt(reg_53, length(reg_4));
    if (tobool(reg_57)) {
      reg_59 = not(reg_7);
      if (!tobool(reg_59)) {
        reg_59 = lt(reg_55, reg_7);
      }
      reg_57 = reg_59;
    }
    if (!tobool(reg_57)) break loop_1;
    reg_63 = newStack();
    reg_65 = get(reg_4, cns4184);
    reg_66 = newStack();
    push(reg_66, reg_4);
    push(reg_66, reg_5);
    push(reg_66, reg_53);
    append(reg_63, call(reg_65, reg_66));
    reg_68 = call(reg_9, reg_63);
    reg_69 = next(reg_68);
    reg_70 = next(reg_68);
    reg_71 = next(reg_68);
    if (tobool(not(reg_69))) {
      if (true) break loop_1;
    }
    reg_75 = get(reg_4, cns4185);
    reg_76 = newStack();
    push(reg_76, reg_4);
    push(reg_76, reg_53);
    push(reg_76, sub(reg_69, cns4186));
    reg_80 = first(call(reg_75, reg_76));
    reg_81 = newStack();
    reg_83 = get(reg_4, cns4187);
    reg_84 = newStack();
    push(reg_84, reg_4);
    push(reg_84, reg_69);
    push(reg_84, reg_70);
    push(reg_81, first(call(reg_83, reg_84)));
    reg_91 = get(get(reg_1.i, cns4188), cns4189);
    reg_92 = newStack();
    push(reg_92, reg_71);
    append(reg_81, call(reg_91, reg_92));
    reg_54 = concat(reg_54, concat(reg_80, first(call(reg_10, reg_81))));
    reg_53 = add(reg_70, cns4190);
    reg_55 = add(reg_55, cns4191);
  }
  reg_103 = get(reg_4, cns4192);
  reg_104 = newStack();
  push(reg_104, reg_4);
  push(reg_104, reg_53);
  reg_54 = concat(reg_54, first(call(reg_103, reg_104)));
  reg_108 = newStack();
  push(reg_108, reg_54);
  push(reg_108, reg_55);
  return reg_108;
}
function lua_lib_pattern$lua_main (reg_0) {
  var reg_1, reg_2, reg_5, reg_6, reg_69, reg_70, reg_75, reg_76, reg_81, reg_82, reg_85;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1, c: reg_1, d: reg_1, e: reg_1, f: reg_1, g: reg_1, h: reg_1, i: reg_1, j: reg_1, k: reg_1, l: reg_1, m: reg_1, n: reg_1, o: reg_1, p: reg_1, q: reg_1, r: reg_1, s: reg_1, t: reg_1, u: reg_1, v: reg_1, w: reg_1, x: reg_1, y: reg_1, z: reg_1, aa: reg_1, ab: reg_1, ac: reg_1};
  reg_2.i = reg_0;
  reg_5 = get(reg_2.i, cns3932);
  reg_6 = cns3933;
  set(reg_5, reg_6, anyFn((function (a) { return string_charat(a,this) }).bind(reg_2)));
  reg_2.f = anyFn((function (a) { return lua_lib_pattern$function(a,this) }).bind(reg_2));
  reg_2.a = anyFn((function (a) { return function$56(a,this) }).bind(reg_2));
  reg_2.b = anyFn((function (a) { return function$57(a,this) }).bind(reg_2));
  reg_2.e = anyFn((function (a) { return function$58(a,this) }).bind(reg_2));
  reg_2.d = anyFn((function (a) { return function$59(a,this) }).bind(reg_2));
  reg_2.c = anyFn((function (a) { return function$60(a,this) }).bind(reg_2));
  reg_2.g = anyFn((function (a) { return function$61(a,this) }).bind(reg_2));
  reg_2.h = anyFn((function (a) { return function$62(a,this) }).bind(reg_2));
  reg_2.j = anyFn((function (a) { return function$63(a,this) }).bind(reg_2));
  reg_2.k = anyFn((function (a) { return function$64(a,this) }).bind(reg_2));
  reg_2.q = anyFn((function (a) { return function$65(a,this) }).bind(reg_2));
  reg_2.l = anyFn((function (a) { return function$66(a,this) }).bind(reg_2));
  reg_2.p = anyFn((function (a) { return function$68(a,this) }).bind(reg_2));
  reg_2.m = anyFn((function (a) { return function$70(a,this) }).bind(reg_2));
  reg_2.o = anyFn((function (a) { return function$72(a,this) }).bind(reg_2));
  reg_2.n = anyFn((function (a) { return function$73(a,this) }).bind(reg_2));
  reg_2.r = anyFn((function (a) { return function$74(a,this) }).bind(reg_2));
  reg_2.t = anyFn((function (a) { return function$75(a,this) }).bind(reg_2));
  reg_2.u = anyFn((function (a) { return function$77(a,this) }).bind(reg_2));
  reg_2.s = anyFn((function (a) { return function$79(a,this) }).bind(reg_2));
  reg_2.v = anyFn((function (a) { return function$80(a,this) }).bind(reg_2));
  reg_2.w = anyFn((function (a) { return function$82(a,this) }).bind(reg_2));
  reg_2.x = anyFn((function (a) { return function$84(a,this) }).bind(reg_2));
  reg_2.y = anyFn((function (a) { return function$86(a,this) }).bind(reg_2));
  reg_2.z = anyFn((function (a) { return function$88(a,this) }).bind(reg_2));
  reg_2.aa = anyFn((function (a) { return function$90(a,this) }).bind(reg_2));
  reg_2.ab = anyFn((function (a) { return function$92(a,this) }).bind(reg_2));
  reg_64 = anyFn((function (a) { return function$94(a,this) }).bind(reg_2));
  reg_2.ac = anyFn((function (a) { return function$95(a,this) }).bind(reg_2));
  reg_69 = get(reg_2.i, cns4100);
  reg_70 = cns4101;
  set(reg_69, reg_70, anyFn((function (a) { return string_find(a,this) }).bind(reg_2)));
  reg_75 = get(reg_2.i, cns4148);
  reg_76 = cns4149;
  set(reg_75, reg_76, anyFn((function (a) { return string_match(a,this) }).bind(reg_2)));
  reg_81 = get(reg_2.i, cns4160);
  reg_82 = cns4161;
  set(reg_81, reg_82, anyFn((function (a) { return string_gsub(a,this) }).bind(reg_2)));
  reg_85 = newStack();
  return reg_85;
}
Auro.io_open = function (path, mode) {
  return {f: Auro.fs.openSync(path, mode), size: Auro.fs.statSync(path).size, pos: 0}
}
function newUserData (reg_0, reg_1) {
  var reg_3, reg_5, reg_8;
  reg_3 = cns2.a;
  reg_5 = (reg_3 + 1);
  cns2.a = reg_5;
  reg_8 = {a: reg_3, b: reg_0, c: reg_1};
  return reg_8;
}
function _open (reg_0) {
  var reg_4, reg_5, reg_6, reg_11, reg_12, reg_18, reg_24, reg_32, reg_38;
  var goto_11=false, goto_29=false, goto_41=false, goto_53=false;
  reg_4 = simple_string(next(reg_0), "1", "io.open");
  reg_5 = next(reg_0);
  reg_6 = "";
  goto_11 = !testNil(reg_5);
  if (!goto_11) {
    reg_6 = "r";
  }
  if ((goto_11 || false)) {
    goto_11 = false;
    if ((typeof reg_5 === 'string')) {
      reg_6 = reg_5;
    }
  }
  reg_12 = (reg_6 == "r");
  if (!reg_12) {
    reg_12 = (reg_6 == "rb");
  }
  goto_29 = !reg_12;
  if (!goto_29) {
    reg_11 = 'r';
  }
  if ((goto_29 || false)) {
    goto_29 = false;
    reg_18 = (reg_6 == "w");
    if (!reg_18) {
      reg_18 = (reg_6 == "wb");
    }
    goto_41 = !reg_18;
    if (!goto_41) {
      reg_11 = 'w';
    }
    if ((goto_41 || false)) {
      goto_41 = false;
      reg_24 = (reg_6 == "a");
      if (!reg_24) {
        reg_24 = (reg_6 == "ab");
      }
      goto_53 = !reg_24;
      if (!goto_53) {
        reg_11 = 'a';
      }
      if ((goto_53 || false)) {
        goto_53 = false;
        throw new Error("bad argument #2 to 'io.open' (invalid mode)");
      }
    }
  }
  reg_32 = new File(Auro.io_open(reg_4, reg_11));
  reg_38 = stackof(new type_25(newUserData(reg_32, cns3.e)));
  return reg_38;
}
function get_file (reg_0) {
  var reg_3, reg_7;
  var goto_11=false;
  if ((reg_0 instanceof type_25)) {
    reg_3 = reg_0.val;
    goto_11 = !(reg_3.b instanceof File);
    if (!goto_11) {
      reg_7 = reg_3.b.val;
      return reg_7;
    }
    if ((goto_11 || false)) {
      goto_11 = false;
      throw new Error("bad argument #1 to 'read' (file expected)");
    }
    if (true) { goto(20); };
  }
  throw new Error((("bad argument #1 to 'read' (file expected, got " + typestr(reg_0)) + ")"));
}
Auro.io_read = function (file, size) {
  var buf = new Uint8Array(size)
  var redd = Auro.fs.readSync(file.f, buf, 0, size, file.pos)
  file.pos += redd
  return buf.slice(0, redd)
}
Auro.string_new = function (buf) {
  if (typeof buf === 'string') return buf
  var codes = []
  for (var j = 0; j < buf.length; j++) {
    var c = buf[j]
    if (c > 0xEF) {
      c = (c & 0xF) << 0x12 | (buf[++j] & 0x3F) << 0xC | (buf[++j] & 0x3F) << 0x6 | (buf[++j] & 0x3F)
    } else if (c > 0xDF) {
      c = (c & 0xF) << 0xC | (buf[++j] & 0x3F) << 0x6 | (buf[++j] & 0x3F)
    } else if (c > 0xBF) {
      c = (c & 0x1F) << 0x6 | (buf[++j] & 0x3F)
    }
    if (c > 0xFFFF) {
      c -= 0x10000
      codes.push(c >>> 10 & 0x3FF | 0xD800)
      codes.push(0xDC00 | c & 0x3FF)
    } else {
      codes.push(c)
    }
  }
  return String.fromCharCode.apply(String, codes)
}
Auro.io_eof = function (file) {
  return file.pos >= file.size
}
function _read (reg_0) {
  var reg_2, reg_3, reg_4, reg_9, reg_35;
  var goto_28=false, goto_29=false, goto_35=false, goto_41=false, goto_47=false, goto_50=false, goto_57=false;
  reg_2 = get_file(next(reg_0));
  reg_3 = next(reg_0);
  if ((reg_3 instanceof type_8)) {
    reg_3 = "l";
  }
  goto_50 = !(typeof reg_3 === 'string');
  if (!goto_50) {
    reg_9 = reg_3;
    goto_29 = !(reg_9 == "a");
    if (!goto_29) {
      reg_4 = "";
      loop_1: while (true) {
        reg_4 = (reg_4 + Auro.string_new(Auro.io_read(reg_2, 128)));
        goto_28 = !!Auro.io_eof(reg_2);
        if (goto_28) break loop_1;
      }
      if (!goto_28) {
      }
      goto_28 = false;
    }
    if ((goto_29 || false)) {
      goto_29 = false;
      goto_35 = !(reg_9 == "n");
      if (!goto_35) {
        throw new Error("format 'n' not yet supported");
      }
      if ((goto_35 || false)) {
        goto_35 = false;
        goto_41 = !(reg_9 == "l");
        if (!goto_41) {
          throw new Error("format 'l' not yet supported");
        }
        if ((goto_41 || false)) {
          goto_41 = false;
          goto_47 = !(reg_9 == "L");
          if (!goto_47) {
            throw new Error("format 'L' not yet supported");
          }
          if ((goto_47 || false)) {
            goto_47 = false;
            throw new Error("bad argument #2 to 'read' (invalid format)");
          }
        }
      }
    }
  }
  if ((goto_50 || false)) {
    goto_50 = false;
    goto_57 = !(reg_3 instanceof Integer);
    if (!goto_57) {
      reg_4 = Auro.string_new(Auro.io_read(reg_2, reg_3.val));
    }
    if ((goto_57 || false)) {
      goto_57 = false;
      throw new Error("bad argument #2 to 'read' (invalid format)");
    }
  }
  reg_35 = stackof(reg_4);
  return reg_35;
}
Auro.str_tobuf = function (str) {
  bytes = []
  for (var i = 0; i < str.length; i++) {
    var c = str.charCodeAt(i)
    if (c >= 0xD800 && c <= 0xDFFF) {
      c = (c - 0xD800 << 10 | str.charCodeAt(++i) - 0xDC00) + 0x10000
    }
    if (c < 0x80) {
      bytes.push(c)
    } else if (c < 0x800) {
      bytes.push(c >> 0x6 | 0xC0, c & 0x3F | 0x80)
    } else if (c < 0x10000) {
      bytes.push(c >> 0xC | 0xE0, c >> 0x6 & 0x3F | 0x80, c & 0x3F | 0x80)
    } else {
      bytes.push(c >> 0x12 | 0xF0, c >> 0xC & 0x3F | 0x80, c >> 0x6 & 0x3F | 0x80, c & 0x3F | 0x80)
    }
  }
  return Uint8Array.from(bytes)
}
Auro.io_write = function (file, buf) {
  var written = Auro.fs.writeSync(file.f, buf, 0, buf.length, file.pos)
  file.pos += written
}
function _write (reg_0) {
  var reg_2, reg_3, reg_4, reg_14;
  reg_2 = get_file(next(reg_0));
  reg_3 = 1;
  reg_4 = "";
  loop_1: while (more(reg_0)) {
    reg_4 = (reg_4 + simple_string(next(reg_0), String(reg_3), "write"));
    reg_3 = (reg_3 + 1);
  }
  Auro.io_write(reg_2, Auro.str_tobuf(reg_4));
  reg_14 = newStack();
  return reg_14;
}
function _writebytes (reg_0) {
  var reg_2, reg_4, reg_5, reg_7, reg_15;
  reg_2 = get_file(next(reg_0));
  reg_4 = new Uint8Array(lua$length(reg_0));
  reg_5 = 0;
  loop_1: while (more(reg_0)) {
    reg_7 = next(reg_0);
    reg_4[reg_5]=simple_number(reg_7, String((reg_5 + 1)), "writebytes");
    reg_5 = (reg_5 + 1);
  }
  Auro.io_write(reg_2, reg_4);
  reg_15 = newStack();
  return reg_15;
}
Auro.io_close = function (file) {
  Auro.fs.closeSync(file.f)
}
function _close (reg_0) {
  var reg_3;
  Auro.io_close(get_file(next(reg_0)));
  reg_3 = newStack();
  return reg_3;
}
function _filestr (reg_0) {
  var reg_1, reg_5, reg_12;
  reg_1 = next(reg_0);
  reg_2 = get_file(reg_1);
  reg_5 = reg_1.val.a;
  reg_12 = stackof((("file (" + String(reg_5)) + ")"));
  return reg_12;
}
Auro.exit = function (code) {
  if (typeof process !== "undefined") process.exit(code)
  else throw "Auro Exit with code " + code
}
function _exit (reg_0) {
  var reg_1, reg_2, reg_11;
  var goto_9=false, goto_12=false;
  reg_1 = next(reg_0);
  reg_2 = 0;
  goto_12 = !(typeof reg_1 === 'boolean');
  if (!goto_12) {
    goto_9 = !reg_1;
    if (!goto_9) {
      reg_2 = 0;
    }
    if ((goto_9 || false)) {
      goto_9 = false;
      reg_2 = 1;
    }
  }
  if ((goto_12 || false)) {
    goto_12 = false;
    reg_2 = simple_number_or(reg_1, 0, "1", "os.exit");
  }
  Auro.exit(reg_2);
  reg_11 = newStack();
  return reg_11;
}
function _tointeger (reg_0) {
  var reg_1, reg_6, reg_8, reg_10, reg_16, reg_17;
  reg_1 = next(reg_0);
  if ((typeof reg_1 === 'string')) {
    reg_1 = parseNum(reg_1);
  }
  if ((reg_1 instanceof Integer)) {
    reg_6 = stackof(reg_1);
    return reg_6;
  }
  if ((typeof reg_1 === 'number')) {
    reg_8 = reg_1;
    reg_10 = (reg_8 - Math.trunc(reg_8));
    if ((reg_10 == 0)) {
      reg_16 = stackof(new Integer(reg_8));
      return reg_16;
    }
  }
  reg_17 = newStack();
  return reg_17;
}
function _tofloat (reg_0) {
  var reg_1, reg_9, reg_11, reg_12;
  reg_1 = next(reg_0);
  if ((typeof reg_1 === 'string')) {
    reg_1 = parseNum(reg_1);
  }
  if ((reg_1 instanceof Integer)) {
    reg_9 = stackof(reg_1.val);
    return reg_9;
  }
  if ((typeof reg_1 === 'number')) {
    reg_11 = stackof(reg_1);
    return reg_11;
  }
  reg_12 = newStack();
  return reg_12;
}
function _mathtype (reg_0) {
  var reg_1, reg_5, reg_9, reg_10;
  reg_1 = next(reg_0);
  if ((reg_1 instanceof Integer)) {
    reg_5 = stackof("integer");
    return reg_5;
  }
  if ((typeof reg_1 === 'number')) {
    reg_9 = stackof("float");
    return reg_9;
  }
  reg_10 = newStack();
  return reg_10;
}
var cns4193 = parseNum("1")
var cns4194 = anyStr("math")
var cns4195 = anyStr("tofloat")
var cns4196 = anyStr("error")
var cns4197 = anyStr("bad argument #")
var cns4198 = anyStr(" to ")
var cns4199 = anyStr(" (number expected, got ")
var cns4200 = anyStr("type")
var cns4201 = anyStr(")")
function lua_lib_math$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_14, reg_15, reg_17, reg_19, reg_22, reg_23, reg_24, reg_25, reg_26, reg_29, reg_30, reg_32, reg_33, reg_43;
  var goto_26=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  if (tobool(not(reg_6))) {
    reg_6 = cns4193;
  }
  reg_14 = get(get(reg_1.a, cns4194), cns4195);
  reg_15 = newStack();
  push(reg_15, reg_4);
  reg_17 = first(call(reg_14, reg_15));
  goto_26 = !tobool(reg_17);
  if (!goto_26) {
    reg_19 = newStack();
    push(reg_19, reg_17);
    return reg_19;
  }
  if ((goto_26 || false)) {
    goto_26 = false;
    reg_22 = get(reg_1.a, cns4196);
    reg_23 = newStack();
    reg_24 = cns4197;
    reg_25 = cns4198;
    reg_26 = cns4199;
    reg_29 = get(reg_1.a, cns4200);
    reg_30 = newStack();
    push(reg_30, reg_4);
    reg_32 = first(call(reg_29, reg_30));
    reg_33 = newStack();
    push(reg_33, cns4201);
    push(reg_23, concat(reg_24, concat(reg_6, concat(reg_25, concat(reg_5, concat(reg_26, first(call(reg_32, reg_33))))))));
    reg_42 = call(reg_22, reg_23);
  }
  reg_43 = newStack();
  return reg_43;
}
var cns4202 = anyStr("getarg")
var cns4203 = anyStr("math")
var cns4204 = anyStr("pi")
var cns4205 = anyStr("math")
var cns4206 = anyStr("huge")
var cns4207 = anyStr("math")
var cns4208 = anyStr("deg")
var cns4209 = anyStr("math")
var cns4210 = anyStr("pi")
var cns4211 = parseNum("180")
function math_deg (reg_0, reg_1) {
  var reg_4, reg_5;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  push(reg_5, mul(div(reg_4, get(get(reg_1.a, cns4209), cns4210)), cns4211));
  return reg_5;
}
var cns4212 = anyStr("math")
var cns4213 = anyStr("rad")
var cns4214 = parseNum("180")
var cns4215 = anyStr("math")
var cns4216 = anyStr("pi")
function math_rad (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = div(reg_4, cns4214);
  push(reg_5, mul(reg_7, get(get(reg_1.a, cns4215), cns4216)));
  return reg_5;
}
var cns4217 = anyStr("math")
var cns4218 = anyStr("max")
var cns4219 = parseNum("1")
var cns4220 = anyStr("ipairs")
function math_max (reg_0, reg_1) {
  var reg_4, reg_5, reg_10, reg_11, reg_12, reg_13, reg_14, reg_15, reg_16, reg_17, reg_19, reg_25;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  table_append(reg_5, cns4219, copy(reg_0));
  reg_10 = get(reg_1.a, cns4220);
  reg_11 = newStack();
  push(reg_11, reg_5);
  reg_12 = call(reg_10, reg_11);
  reg_13 = next(reg_12);
  reg_14 = next(reg_12);
  reg_15 = next(reg_12);
  loop_1: while (true) {
    reg_16 = newStack();
    push(reg_16, reg_14);
    push(reg_16, reg_15);
    reg_17 = call(reg_13, reg_16);
    reg_15 = next(reg_17);
    reg_19 = next(reg_17);
    if (tobool(eq(reg_15, nil()))) break loop_1;
    if (tobool(gt(reg_19, reg_4))) {
      reg_4 = reg_19;
    }
  }
  reg_25 = newStack();
  push(reg_25, reg_4);
  return reg_25;
}
var cns4221 = anyStr("math")
var cns4222 = anyStr("min")
var cns4223 = parseNum("1")
var cns4224 = anyStr("ipairs")
function math_min (reg_0, reg_1) {
  var reg_4, reg_5, reg_10, reg_11, reg_12, reg_13, reg_14, reg_15, reg_16, reg_17, reg_19, reg_25;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  table_append(reg_5, cns4223, copy(reg_0));
  reg_10 = get(reg_1.a, cns4224);
  reg_11 = newStack();
  push(reg_11, reg_5);
  reg_12 = call(reg_10, reg_11);
  reg_13 = next(reg_12);
  reg_14 = next(reg_12);
  reg_15 = next(reg_12);
  loop_1: while (true) {
    reg_16 = newStack();
    push(reg_16, reg_14);
    push(reg_16, reg_15);
    reg_17 = call(reg_13, reg_16);
    reg_15 = next(reg_17);
    reg_19 = next(reg_17);
    if (tobool(eq(reg_15, nil()))) break loop_1;
    if (tobool(lt(reg_19, reg_4))) {
      reg_4 = reg_19;
    }
  }
  reg_25 = newStack();
  push(reg_25, reg_4);
  return reg_25;
}
var cns4225 = anyStr("math")
var cns4226 = anyStr("abs")
var cns4227 = anyStr("tonumber")
var cns4228 = anyStr("error")
var cns4229 = anyStr("bad argument #1 to abs (number expected, got ")
var cns4230 = anyStr("type")
var cns4231 = anyStr(")")
var cns4232 = parseNum("0")
function math_abs (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_10, reg_15, reg_16, reg_17, reg_20, reg_21, reg_31, reg_33, reg_34;
  var goto_40=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns4227);
  reg_8 = newStack();
  push(reg_8, reg_4);
  reg_10 = first(call(reg_7, reg_8));
  if (tobool(not(reg_10))) {
    reg_15 = get(reg_1.a, cns4228);
    reg_16 = newStack();
    reg_17 = cns4229;
    reg_20 = get(reg_1.a, cns4230);
    reg_21 = newStack();
    push(reg_21, reg_4);
    push(reg_16, concat(reg_17, concat(first(call(reg_20, reg_21)), cns4231)));
    reg_27 = call(reg_15, reg_16);
  }
  goto_40 = !tobool(lt(reg_10, cns4232));
  if (!goto_40) {
    reg_31 = newStack();
    push(reg_31, unm(reg_10));
    return reg_31;
  }
  if ((goto_40 || false)) {
    goto_40 = false;
    reg_33 = newStack();
    push(reg_33, reg_10);
    return reg_33;
  }
  reg_34 = newStack();
  return reg_34;
}
var cns4233 = anyStr("math")
var cns4234 = anyStr("ceil")
var cns4235 = anyStr("getarg")
var cns4236 = anyStr("ceil")
function math_ceil (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns4235);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns4236);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.ceil(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns4237 = anyStr("math")
var cns4238 = anyStr("floor")
var cns4239 = anyStr("getarg")
var cns4240 = anyStr("floor")
function math_floor (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns4239);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns4240);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.floor(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns4241 = anyStr("math")
var cns4242 = anyStr("fmod")
var cns4243 = anyStr("getarg")
var cns4244 = anyStr("fmod")
var cns4245 = parseNum("1")
var cns4246 = anyStr("getarg")
var cns4247 = anyStr("fmod")
var cns4248 = parseNum("2")
function math_fmod (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_16, reg_17, reg_22, reg_26, reg_27;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = get(reg_1.a, cns4243);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, cns4244);
  push(reg_9, cns4245);
  reg_4 = first(call(reg_8, reg_9));
  reg_16 = get(reg_1.a, cns4246);
  reg_17 = newStack();
  push(reg_17, reg_5);
  push(reg_17, cns4247);
  push(reg_17, cns4248);
  reg_5 = first(call(reg_16, reg_17));
  reg_22 = newStack();
  reg_26 = (reg_4 % reg_5);
  reg_27 = newStack();
  push(reg_27, reg_26);
  append(reg_22, reg_27);
  return reg_22;
}
var cns4249 = anyStr("math")
var cns4250 = anyStr("modf")
var cns4251 = anyStr("getarg")
var cns4252 = anyStr("modf")
var cns4253 = anyStr("math")
var cns4254 = anyStr("tofloat")
function math_modf (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_14, reg_15, reg_16, reg_17, reg_22, reg_23;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns4251);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns4252);
  reg_4 = first(call(reg_7, reg_8));
  reg_14 = Math.trunc(reg_4);
  reg_15 = newStack();
  push(reg_15, reg_14);
  reg_16 = first(reg_15);
  reg_17 = newStack();
  push(reg_17, reg_16);
  reg_22 = get(get(reg_1.a, cns4253), cns4254);
  reg_23 = newStack();
  push(reg_23, sub(reg_4, reg_16));
  append(reg_17, call(reg_22, reg_23));
  return reg_17;
}
var cns4255 = anyStr("math")
var cns4256 = anyStr("exp")
var cns4257 = anyStr("getarg")
var cns4258 = anyStr("exp")
function math_exp (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns4257);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns4258);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.exp(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns4259 = anyStr("math")
var cns4260 = anyStr("sqrt")
var cns4261 = anyStr("getarg")
var cns4262 = anyStr("sqrt")
function math_sqrt (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns4261);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns4262);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.sqrt(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns4263 = anyStr("math")
var cns4264 = anyStr("log")
var cns4265 = anyStr("getarg")
var cns4266 = anyStr("log")
var cns4267 = anyStr("getarg")
var cns4268 = anyStr("log")
var cns4269 = parseNum("2")
function math_log (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_18, reg_19, reg_24, reg_28, reg_29;
  var goto_20=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = get(reg_1.a, cns4265);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, cns4266);
  reg_4 = first(call(reg_8, reg_9));
  goto_20 = !tobool(not(reg_5));
  if (!goto_20) {
    reg_5 = reg_1.b;
  }
  if ((goto_20 || false)) {
    goto_20 = false;
    reg_18 = get(reg_1.a, cns4267);
    reg_19 = newStack();
    push(reg_19, reg_5);
    push(reg_19, cns4268);
    push(reg_19, cns4269);
    reg_5 = first(call(reg_18, reg_19));
  }
  reg_24 = newStack();
  reg_28 = (Math.log(reg_4) / Math.log(reg_5));
  reg_29 = newStack();
  push(reg_29, reg_28);
  append(reg_24, reg_29);
  return reg_24;
}
var cns4270 = anyStr("math")
var cns4271 = anyStr("sin")
var cns4272 = anyStr("getarg")
var cns4273 = anyStr("sin")
function math_sin (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns4272);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns4273);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.sin(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns4274 = anyStr("math")
var cns4275 = anyStr("cos")
var cns4276 = anyStr("getarg")
var cns4277 = anyStr("cos")
function math_cos (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns4276);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns4277);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.cos(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns4278 = anyStr("math")
var cns4279 = anyStr("tan")
var cns4280 = anyStr("getarg")
var cns4281 = anyStr("tan")
function math_tan (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns4280);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns4281);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.tan(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns4282 = anyStr("math")
var cns4283 = anyStr("asin")
var cns4284 = anyStr("getarg")
var cns4285 = anyStr("asin")
function math_asin (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns4284);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns4285);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.sin(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns4286 = anyStr("math")
var cns4287 = anyStr("acos")
var cns4288 = anyStr("getarg")
var cns4289 = anyStr("acos")
function math_acos (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = get(reg_1.a, cns4288);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns4289);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.cos(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns4290 = anyStr("math")
var cns4291 = anyStr("atan")
var cns4292 = anyStr("getarg")
var cns4293 = anyStr("atan")
var cns4294 = anyStr("getarg")
var cns4295 = anyStr("atan")
var cns4296 = parseNum("2")
var cns4297 = anyStr("math")
var cns4298 = anyStr("tofloat")
var cns4299 = parseNum("1")
function math_atan (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_16, reg_17, reg_26, reg_27, reg_31, reg_35, reg_36;
  var goto_29=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = get(reg_1.a, cns4292);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, cns4293);
  reg_4 = first(call(reg_8, reg_9));
  goto_29 = !tobool(reg_5);
  if (!goto_29) {
    reg_16 = get(reg_1.a, cns4294);
    reg_17 = newStack();
    push(reg_17, reg_5);
    push(reg_17, cns4295);
    push(reg_17, cns4296);
    reg_5 = first(call(reg_16, reg_17));
  }
  if ((goto_29 || false)) {
    goto_29 = false;
    reg_26 = get(get(reg_1.a, cns4297), cns4298);
    reg_27 = newStack();
    push(reg_27, cns4299);
    reg_5 = first(call(reg_26, reg_27));
  }
  reg_31 = newStack();
  reg_35 = Math.atan2(reg_4, reg_5);
  reg_36 = newStack();
  push(reg_36, reg_35);
  append(reg_31, reg_36);
  return reg_31;
}
function lua_lib_math$lua_main (reg_0) {
  var reg_1, reg_2, reg_4, reg_7, reg_8, reg_9, reg_12, reg_13, reg_14, reg_15, reg_19, reg_20, reg_21, reg_22, reg_24, reg_25, reg_29, reg_30, reg_35, reg_36, reg_41, reg_42, reg_47, reg_48, reg_53, reg_54, reg_59, reg_60, reg_65, reg_66, reg_71, reg_72, reg_77, reg_78, reg_83, reg_84, reg_89, reg_90, reg_95, reg_96, reg_101, reg_102, reg_107, reg_108, reg_113, reg_114, reg_119, reg_120, reg_125, reg_126, reg_131, reg_132, reg_135;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1};
  reg_2.a = reg_0;
  reg_4 = anyFn((function (a) { return lua_lib_math$function(a,this) }).bind(reg_2));
  set(reg_2.a, cns4202, reg_4);
  reg_7 = Math.PI;
  reg_8 = Math.E;
  reg_9 = Infinity;
  reg_12 = get(reg_2.a, cns4203);
  reg_13 = cns4204;
  reg_14 = reg_7;
  reg_15 = newStack();
  push(reg_15, reg_14);
  set(reg_12, reg_13, first(reg_15));
  reg_19 = get(reg_2.a, cns4205);
  reg_20 = cns4206;
  reg_21 = reg_9;
  reg_22 = newStack();
  push(reg_22, reg_21);
  set(reg_19, reg_20, first(reg_22));
  reg_24 = reg_8;
  reg_25 = newStack();
  push(reg_25, reg_24);
  reg_2.b = first(reg_25);
  reg_29 = get(reg_2.a, cns4207);
  reg_30 = cns4208;
  set(reg_29, reg_30, anyFn((function (a) { return math_deg(a,this) }).bind(reg_2)));
  reg_35 = get(reg_2.a, cns4212);
  reg_36 = cns4213;
  set(reg_35, reg_36, anyFn((function (a) { return math_rad(a,this) }).bind(reg_2)));
  reg_41 = get(reg_2.a, cns4217);
  reg_42 = cns4218;
  set(reg_41, reg_42, anyFn((function (a) { return math_max(a,this) }).bind(reg_2)));
  reg_47 = get(reg_2.a, cns4221);
  reg_48 = cns4222;
  set(reg_47, reg_48, anyFn((function (a) { return math_min(a,this) }).bind(reg_2)));
  reg_53 = get(reg_2.a, cns4225);
  reg_54 = cns4226;
  set(reg_53, reg_54, anyFn((function (a) { return math_abs(a,this) }).bind(reg_2)));
  reg_59 = get(reg_2.a, cns4233);
  reg_60 = cns4234;
  set(reg_59, reg_60, anyFn((function (a) { return math_ceil(a,this) }).bind(reg_2)));
  reg_65 = get(reg_2.a, cns4237);
  reg_66 = cns4238;
  set(reg_65, reg_66, anyFn((function (a) { return math_floor(a,this) }).bind(reg_2)));
  reg_71 = get(reg_2.a, cns4241);
  reg_72 = cns4242;
  set(reg_71, reg_72, anyFn((function (a) { return math_fmod(a,this) }).bind(reg_2)));
  reg_77 = get(reg_2.a, cns4249);
  reg_78 = cns4250;
  set(reg_77, reg_78, anyFn((function (a) { return math_modf(a,this) }).bind(reg_2)));
  reg_83 = get(reg_2.a, cns4255);
  reg_84 = cns4256;
  set(reg_83, reg_84, anyFn((function (a) { return math_exp(a,this) }).bind(reg_2)));
  reg_89 = get(reg_2.a, cns4259);
  reg_90 = cns4260;
  set(reg_89, reg_90, anyFn((function (a) { return math_sqrt(a,this) }).bind(reg_2)));
  reg_95 = get(reg_2.a, cns4263);
  reg_96 = cns4264;
  set(reg_95, reg_96, anyFn((function (a) { return math_log(a,this) }).bind(reg_2)));
  reg_101 = get(reg_2.a, cns4270);
  reg_102 = cns4271;
  set(reg_101, reg_102, anyFn((function (a) { return math_sin(a,this) }).bind(reg_2)));
  reg_107 = get(reg_2.a, cns4274);
  reg_108 = cns4275;
  set(reg_107, reg_108, anyFn((function (a) { return math_cos(a,this) }).bind(reg_2)));
  reg_113 = get(reg_2.a, cns4278);
  reg_114 = cns4279;
  set(reg_113, reg_114, anyFn((function (a) { return math_tan(a,this) }).bind(reg_2)));
  reg_119 = get(reg_2.a, cns4282);
  reg_120 = cns4283;
  set(reg_119, reg_120, anyFn((function (a) { return math_asin(a,this) }).bind(reg_2)));
  reg_125 = get(reg_2.a, cns4286);
  reg_126 = cns4287;
  set(reg_125, reg_126, anyFn((function (a) { return math_acos(a,this) }).bind(reg_2)));
  reg_131 = get(reg_2.a, cns4290);
  reg_132 = cns4291;
  set(reg_131, reg_132, anyFn((function (a) { return math_atan(a,this) }).bind(reg_2)));
  reg_135 = newStack();
  return reg_135;
}
function get_global () {
  var reg_3, reg_6, reg_11, reg_15, reg_19, reg_23, reg_27, reg_31, reg_35, reg_39, reg_43, reg_47, reg_51, reg_55, reg_58, reg_63, reg_67, reg_75, reg_77, reg_82, reg_84, reg_88, reg_90, reg_94, reg_96, reg_100, reg_102, reg_106, reg_108, reg_112, reg_117, reg_119, reg_123, reg_125, reg_129, reg_131, reg_142, reg_147, reg_151, reg_153, reg_158, reg_160, reg_164, reg_166, reg_170, reg_172, reg_176, reg_178, reg_182, reg_184, reg_187, reg_192, reg_195, reg_200, reg_204, reg_208, reg_215, reg_216, reg_220, reg_224, reg_229;
  if (!cns3.a) {
    reg_3 = true;
    cns3.a = reg_3;
    reg_6 = cns3.b;
    lua$set(reg_6, anyStr("_G"), anyTable(reg_6));
    reg_11 = anyStr("_VERSION");
    lua$set(reg_6, reg_11, anyStr("Lua 5.3"));
    reg_15 = anyStr("_AU_VERSION");
    lua$set(reg_6, reg_15, anyStr("0.6"));
    reg_19 = anyStr("assert");
    lua$set(reg_6, reg_19, anyFn(_assert));
    reg_23 = anyStr("error");
    lua$set(reg_6, reg_23, anyFn(_error));
    reg_27 = anyStr("getmetatable");
    lua$set(reg_6, reg_27, anyFn(_getmeta));
    reg_31 = anyStr("next");
    lua$set(reg_6, reg_31, anyFn(_next));
    reg_35 = anyStr("print");
    lua$set(reg_6, reg_35, anyFn(_print));
    reg_39 = anyStr("select");
    lua$set(reg_6, reg_39, anyFn(_select));
    reg_43 = anyStr("setmetatable");
    lua$set(reg_6, reg_43, anyFn(_setmeta));
    reg_47 = anyStr("tostring");
    lua$set(reg_6, reg_47, anyFn(_tostring));
    reg_51 = anyStr("tonumber");
    lua$set(reg_6, reg_51, anyFn(_tonumber));
    reg_55 = anyStr("type");
    lua$set(reg_6, reg_55, anyFn(_type));
    reg_58 = emptyTable();
    lua$set(reg_6, anyStr("table"), anyTable(reg_58));
    reg_63 = anyStr("pack");
    lua$set(reg_58, reg_63, anyFn(_pack));
    reg_67 = anyStr("unpack");
    lua$set(reg_58, reg_67, anyFn(_unpack));
    reg_73 = lua_lib_table$lua_main(anyTable(cns3.b));
    reg_75 = cns3.d;
    reg_77 = anyStr("__index");
    lua$set(reg_75, reg_77, anyTable(cns3.c));
    reg_82 = cns3.d;
    reg_84 = anyStr("__add");
    lua$set(reg_82, reg_84, new Function$31(_stradd));
    reg_88 = cns3.d;
    reg_90 = anyStr("__sub");
    lua$set(reg_88, reg_90, new Function$31(_strsub));
    reg_94 = cns3.d;
    reg_96 = anyStr("__mul");
    lua$set(reg_94, reg_96, new Function$31(_strmul));
    reg_100 = cns3.d;
    reg_102 = anyStr("__div");
    lua$set(reg_100, reg_102, new Function$31(_strdiv));
    reg_106 = cns3.d;
    reg_108 = anyStr("__unm");
    lua$set(reg_106, reg_108, new Function$31(_strunm));
    reg_112 = anyStr("string");
    lua$set(reg_6, reg_112, anyTable(cns3.c));
    reg_117 = cns3.c;
    reg_119 = anyStr("sub");
    lua$set(reg_117, reg_119, anyFn(_strsubstr));
    reg_123 = cns3.c;
    reg_125 = anyStr("byte");
    lua$set(reg_123, reg_125, anyFn(_strbyte));
    reg_129 = cns3.c;
    reg_131 = anyStr("char");
    lua$set(reg_129, reg_131, anyFn(_strchar));
    reg_137 = lua_lib_string$lua_main(anyTable(cns3.b));
    reg_141 = lua_lib_pattern$lua_main(anyTable(cns3.b));
    reg_142 = emptyTable();
    lua$set(reg_6, "io", new record_22(reg_142));
    reg_147 = "open";
    lua$set(reg_142, reg_147, new Function$31(_open));
    reg_151 = cns3.e;
    reg_153 = "__index";
    lua$set(reg_151, reg_153, new record_22(cns3.e));
    reg_158 = cns3.e;
    reg_160 = "read";
    lua$set(reg_158, reg_160, new Function$31(_read));
    reg_164 = cns3.e;
    reg_166 = "write";
    lua$set(reg_164, reg_166, new Function$31(_write));
    reg_170 = cns3.e;
    reg_172 = "writebytes";
    lua$set(reg_170, reg_172, new Function$31(_writebytes));
    reg_176 = cns3.e;
    reg_178 = "close";
    lua$set(reg_176, reg_178, new Function$31(_close));
    reg_182 = cns3.e;
    reg_184 = "__tostring";
    lua$set(reg_182, reg_184, new Function$31(_filestr));
    reg_187 = emptyTable();
    lua$set(reg_6, "os", new record_22(reg_187));
    reg_192 = "exit";
    lua$set(reg_187, reg_192, new Function$31(_exit));
    reg_195 = emptyTable();
    lua$set(reg_6, "math", new record_22(reg_195));
    reg_200 = "tointeger";
    lua$set(reg_195, reg_200, new Function$31(_tointeger));
    reg_204 = "tofloat";
    lua$set(reg_195, reg_204, new Function$31(_tofloat));
    reg_208 = "type";
    lua$set(reg_195, reg_208, new Function$31(_mathtype));
    reg_214 = lua_lib_math$lua_main(new record_22(cns3.b));
    reg_215 = emptyTable();
    reg_216 = 0;
    loop_1: while ((reg_216 < Auro.args.length)) {
      reg_220 = Auro.args[reg_216];
      lua$set(reg_215, new Integer(reg_216), reg_220);
      reg_216 = (reg_216 + 1);
    }
    reg_224 = new record_22(reg_215);
    lua$set(reg_6, "arg", reg_224);
  }
  reg_229 = anyTable(cns3.b);
  return reg_229;
}
function main () {
  reg_1 = lua_main(get_global());
  return;
}
window["Aulua"] = {
  "lua_main": lua_main,
  "main": main,
};
})(window)
