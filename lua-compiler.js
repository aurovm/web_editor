(function (window) {
var Auro = {};
function IdState () {
  var reg_1;
  reg_1 = {a: 0};
  return reg_1;
}
var cns1 = IdState()
function emptyTable () {
  var reg_1, reg_3, reg_11;
  reg_1 = cns1.a;
  reg_3 = (reg_1 + 1);
  cns1.a = reg_3;
  reg_11 = {a: reg_1, b: {}, c: [], d: [], e: null, f: null, g: null};
  return reg_11;
}
function State () {
  var reg_6;
  reg_6 = {a: false, b: emptyTable(), c: emptyTable(), d: emptyTable(), e: emptyTable(), f: emptyTable()};
  return reg_6;
}
var cns2 = State()
function anyStr (reg_0) {
  var reg_1;
  reg_1 = reg_0;
  return reg_1;
}
var record_18 = function (val) { this.val = val; }
function anyTable (reg_0) {
  var reg_1;
  reg_1 = new record_18(reg_0);
  return reg_1;
}
var Integer = function (val) { this.val = val; }
var type_20 = function (val) { this.val = val; }
function testNil (reg_0) {
  var reg_1;
  reg_1 = (reg_0 instanceof type_20);
  return reg_1;
}
function testInt (reg_0) {
  var reg_1;
  reg_1 = (reg_0 instanceof Integer);
  return reg_1;
}
function getInt (reg_0) {
  var reg_1;
  reg_1 = reg_0.val;
  return reg_1;
}
function testStr (reg_0) {
  var reg_1;
  reg_1 = (typeof reg_0 === 'string');
  return reg_1;
}
function getStr (reg_0) {
  var reg_1;
  reg_1 = reg_0;
  return reg_1;
}
function getFloats (reg_0, reg_1) {
  var reg_2, reg_3, reg_10, reg_12, reg_13, reg_20, reg_22, reg_23, reg_24;
  var goto_7=false, goto_13=false, goto_24=false, goto_30=false;
  goto_7 = !(typeof reg_0 === 'number');
  if (!goto_7) {
    reg_2 = reg_0;
  }
  if ((goto_7 || false)) {
    goto_7 = false;
    goto_13 = !(reg_0 instanceof Integer);
    if (!goto_13) {
      reg_2 = reg_0.val;
    }
    if ((goto_13 || false)) {
      goto_13 = false;
      reg_10 = 0;
      reg_12 = 0;
      reg_13 = false;
      return [reg_10, reg_12, reg_13];
    }
  }
  goto_24 = !(typeof reg_1 === 'number');
  if (!goto_24) {
    reg_3 = reg_1;
  }
  if ((goto_24 || false)) {
    goto_24 = false;
    goto_30 = !(reg_1 instanceof Integer);
    if (!goto_30) {
      reg_3 = reg_1.val;
    }
    if ((goto_30 || false)) {
      goto_30 = false;
      reg_20 = 0;
      reg_22 = 0;
      reg_23 = false;
      return [reg_20, reg_22, reg_23];
    }
  }
  reg_24 = true;
  return [reg_2, reg_3, reg_24];
}
function testBool (reg_0) {
  var reg_1;
  reg_1 = (typeof reg_0 === 'boolean');
  return reg_1;
}
function getBool (reg_0) {
  var reg_1;
  reg_1 = reg_0;
  return reg_1;
}
function testTable (reg_0) {
  var reg_1;
  reg_1 = (reg_0 instanceof record_18);
  return reg_1;
}
function getTable (reg_0) {
  var reg_1;
  reg_1 = reg_0.val;
  return reg_1;
}
var type_23 = function (val) { this.val = val; }
function get_metatable (reg_0) {
  var reg_3, reg_5, reg_8, reg_12, reg_16, reg_17;
  if (testTable(reg_0)) {
    reg_3 = getTable(reg_0).e;
    if ((reg_3 == null)) {
      reg_5 = null;
      return reg_5;
    }
    reg_8 = reg_3;
    return reg_8;
  }
  if ((reg_0 instanceof type_23)) {
    reg_12 = reg_0.val.c;
    return reg_12;
  }
  if (testStr(reg_0)) {
    reg_16 = cns2.d;
    return reg_16;
  }
  reg_17 = null;
  return reg_17;
}
function nil () {
  var reg_3;
  reg_3 = new type_20({a: true});
  return reg_3;
}
function get (reg_0, reg_1) {
  var reg_3, reg_4, reg_11, reg_13, reg_15, reg_17, reg_19, reg_20, reg_21, reg_26, reg_29, reg_32;
  var goto_20=false, goto_30=false;
  goto_20 = !(reg_1 instanceof Integer);
  if (!goto_20) {
    reg_3 = reg_1.val;
    reg_4 = (reg_3 > 0);
    if (reg_4) {
      reg_4 = (reg_3 <= reg_0.c.length);
    }
    if (reg_4) {
      reg_11 = (reg_3 - 1);
      reg_13 = reg_0.c[reg_11];
      return reg_13;
    }
  }
  if ((goto_20 || false)) {
    goto_20 = false;
    if ((typeof reg_1 === 'string')) {
      reg_15 = reg_1;
      reg_17 = reg_0.b[reg_15];
      goto_30 = !(reg_17 == null);
      if (!goto_30) {
        reg_19 = nil();
        return reg_19;
      }
      if ((goto_30 || false)) {
        goto_30 = false;
        reg_20 = reg_17;
        return reg_20;
      }
    }
  }
  reg_21 = 0;
  loop_1: while ((reg_21 < reg_0.d.length)) {
    reg_26 = reg_0.d[reg_21];
    if (equals(reg_1, reg_26.a)) {
      reg_29 = reg_26.b;
      return reg_29;
    }
    reg_21 = (reg_21 + 1);
  }
  reg_32 = nil();
  return reg_32;
}
var Function$29 = function (val) { this.val = val; }
function testFn (reg_0) {
  var reg_1;
  reg_1 = (reg_0 instanceof Function$29);
  return reg_1;
}
function getFn (reg_0) {
  var reg_1;
  reg_1 = reg_0.val;
  return reg_1;
}
function newStack () {
  var reg_2;
  reg_2 = {a: 0, b: []};
  return reg_2;
}
function push (reg_0, reg_1) {
  reg_0.b.push(reg_1);
  return;
}
function more (reg_0) {
  var reg_1, reg_4;
  reg_1 = reg_0.a;
  reg_4 = (reg_1 < reg_0.b.length);
  return reg_4;
}
function first (reg_0) {
  var reg_2, reg_4, reg_5;
  if (more(reg_0)) {
    reg_2 = reg_0.a;
    reg_4 = reg_0.b[reg_2];
    return reg_4;
    if (true) { goto(9); };
  }
  reg_5 = nil();
  return reg_5;
}
function meta_binop (reg_0, reg_1, reg_2) {
  var reg_3, reg_9, reg_11, reg_12, reg_15, reg_16;
  var goto_9=false, goto_10=false;
  reg_3 = get_metatable(reg_0);
  goto_10 = !(reg_3 == null);
  if (!goto_10) {
    reg_3 = get_metatable(reg_1);
    goto_9 = !(reg_3 == null);
    if (!goto_9) {
    }
  }
  if ((goto_10 || (goto_9 || false))) {
    if (!goto_10) {
      if (!goto_9) {
      }
      goto_9 = false;
    }
    goto_10 = false;
    reg_9 = get(reg_3, reg_2);
    if (testFn(reg_9)) {
      reg_11 = getFn(reg_9);
      reg_12 = newStack();
      push(reg_12, reg_0);
      push(reg_12, reg_1);
      reg_15 = first(reg_11(reg_12));
      return reg_15;
    }
  }
  reg_16 = null;
  return reg_16;
}
function tobool (reg_0) {
  var reg_2, reg_4, reg_5;
  if (testBool(reg_0)) {
    reg_2 = getBool(reg_0);
    return reg_2;
    if (true) { goto(12); };
  }
  if (testNil(reg_0)) {
    reg_4 = false;
    return reg_4;
    if (true) { goto(12); };
  }
  reg_5 = true;
  return reg_5;
}
function equals (reg_0, reg_1) {
  var reg_2, reg_5, reg_6, reg_11, reg_12, reg_17, reg_18, reg_19, reg_21, reg_22, reg_23, reg_24, reg_25, reg_28, reg_29, reg_30, reg_31, reg_32, reg_35, reg_38, reg_39, reg_43, reg_45, reg_47, reg_49, reg_50, reg_54, reg_56, reg_60, reg_62, reg_64, reg_66, reg_67;
  reg_2 = testNil(reg_0);
  if (reg_2) {
    reg_2 = testNil(reg_1);
  }
  if (reg_2) {
    reg_5 = true;
    return reg_5;
  }
  reg_6 = testInt(reg_0);
  if (reg_6) {
    reg_6 = testInt(reg_1);
  }
  if (reg_6) {
    reg_11 = (getInt(reg_0) == getInt(reg_1));
    return reg_11;
  }
  reg_12 = testStr(reg_0);
  if (reg_12) {
    reg_12 = testStr(reg_1);
  }
  if (reg_12) {
    reg_17 = (getStr(reg_0) == getStr(reg_1));
    return reg_17;
  }
  var _r = getFloats(reg_0, reg_1); reg_21 = _r[0]; reg_22 = _r[1]; reg_23 = _r[2];
  reg_18 = reg_21;
  reg_19 = reg_22;
  if (reg_23) {
    reg_24 = (reg_18 == reg_19);
    return reg_24;
  }
  reg_25 = testBool(reg_0);
  if (reg_25) {
    reg_25 = testBool(reg_1);
  }
  if (reg_25) {
    reg_28 = getBool(reg_0);
    reg_29 = getBool(reg_1);
    reg_31 = reg_28;
    if (reg_31) {
      reg_31 = reg_29;
    }
    reg_30 = reg_31;
    if (!reg_30) {
      reg_32 = !reg_28;
      if (reg_32) {
        reg_32 = !reg_29;
      }
      reg_30 = reg_32;
    }
    return reg_30;
  }
  reg_35 = testTable(reg_0);
  if (reg_35) {
    reg_35 = testTable(reg_1);
  }
  if (reg_35) {
    reg_38 = getTable(reg_0);
    reg_39 = getTable(reg_1);
    if ((reg_38.a == reg_39.a)) {
      reg_43 = true;
      return reg_43;
    }
    reg_45 = meta_binop(reg_0, reg_1, "__eq");
    if ((reg_45 == null)) {
      reg_47 = false;
      return reg_47;
    }
    reg_49 = tobool(reg_45);
    return reg_49;
  }
  reg_50 = (reg_0 instanceof type_23);
  if (reg_50) {
    reg_50 = (reg_1 instanceof type_23);
  }
  if (reg_50) {
    reg_54 = reg_0.val;
    reg_56 = reg_1.val;
    if ((reg_54.a == reg_56.a)) {
      reg_60 = true;
      return reg_60;
    }
    reg_62 = meta_binop(reg_0, reg_1, "__eq");
    if ((reg_62 == null)) {
      reg_64 = false;
      return reg_64;
    }
    reg_66 = tobool(reg_62);
    return reg_66;
  }
  reg_67 = false;
  return reg_67;
}
function set (reg_0, reg_1, reg_2) {
  var reg_4, reg_11, reg_18, reg_21, reg_23, reg_28, reg_33;
  var goto_12=false, goto_27=false, goto_28=false, goto_29=false, goto_35=false;
  goto_29 = !(reg_1 instanceof Integer);
  if (!goto_29) {
    reg_4 = reg_1.val;
    goto_12 = !(reg_4 == (reg_0.c.length + 1));
    if (!goto_12) {
      reg_0.c.push(reg_2);
      goto_28 = true;
    }
    if ((goto_12 || !goto_28)) {
      goto_12 = false;
      reg_11 = (reg_4 > 0);
      if (reg_11) {
        reg_11 = (reg_4 <= reg_0.c.length);
      }
      goto_27 = !reg_11;
      if (!goto_27) {
        reg_18 = (reg_4 - 1);
        reg_0.c[reg_18]=reg_2;
        goto_28 = true;
      }
      if ((goto_27 || !goto_28)) {
        goto_27 = false;
        goto_35 = true;
      }
    }
    if ((goto_28 || (goto_28 || !goto_35))) {
      if (!goto_28) {
        goto_28 = false;
      }
      goto_28 = false;
    }
  }
  if ((goto_29 || (goto_35 || false))) {
    goto_29 = goto_29;
    if (!goto_29) {
      goto_35 = goto_35;
    }
    if ((goto_29 || !goto_35)) {
      goto_29 = false;
      goto_35 = !(typeof reg_1 === 'string');
      if (!goto_35) {
        reg_21 = reg_1;
        reg_0.b[reg_21] = reg_2;
      }
    }
    if ((goto_35 || (goto_35 || false))) {
      if (!goto_35) {
        goto_35 = false;
      }
      goto_35 = false;
      reg_23 = 0;
      loop_1: while ((reg_23 < reg_0.d.length)) {
        reg_28 = reg_0.d[reg_23];
        if (equals(reg_1, reg_28.a)) {
          reg_28.b = reg_2;
          return;
        }
        reg_23 = (reg_23 + 1);
      }
      reg_33 = {a: reg_1, b: reg_2};
      reg_0.d.push(reg_33);
    }
  }
  return;
}
function next (reg_0) {
  var reg_2, reg_4, reg_8;
  if (more(reg_0)) {
    reg_2 = reg_0.a;
    reg_4 = reg_0.b[reg_2];
    reg_0.a = (reg_0.a + 1);
    return reg_4;
    if (true) { goto(13); };
  }
  reg_8 = nil();
  return reg_8;
}
function typestr (reg_0) {
  var reg_2, reg_4, reg_5, reg_8, reg_10, reg_12, reg_14, reg_15;
  if (testTable(reg_0)) {
    reg_2 = "table";
    return reg_2;
    if (true) { goto(37); };
  }
  if (testStr(reg_0)) {
    reg_4 = "string";
    return reg_4;
    if (true) { goto(37); };
  }
  reg_5 = (typeof reg_0 === 'number');
  if (!reg_5) {
    reg_5 = testInt(reg_0);
  }
  if (reg_5) {
    reg_8 = "number";
    return reg_8;
    if (true) { goto(37); };
  }
  if (testNil(reg_0)) {
    reg_10 = "nil";
    return reg_10;
    if (true) { goto(37); };
  }
  if (testBool(reg_0)) {
    reg_12 = "bool";
    return reg_12;
    if (true) { goto(37); };
  }
  if (testFn(reg_0)) {
    reg_14 = "function";
    return reg_14;
    if (true) { goto(37); };
  }
  reg_15 = "userdata";
  return reg_15;
}
function tostr (reg_0) {
  var reg_2, reg_5, reg_8, reg_11, reg_12, reg_13, reg_16, reg_19, reg_21, reg_22, reg_24, reg_26, reg_29, reg_33, reg_35, reg_40, reg_41;
  var goto_5=false, goto_11=false, goto_17=false, goto_24=false;
  goto_5 = !testStr(reg_0);
  if (!goto_5) {
    reg_2 = getStr(reg_0);
    return reg_2;
  }
  if ((goto_5 || false)) {
    goto_5 = false;
    goto_11 = !(typeof reg_0 === 'number');
    if (!goto_11) {
      reg_5 = String(reg_0);
      return reg_5;
    }
    if ((goto_11 || false)) {
      goto_11 = false;
      goto_17 = !testInt(reg_0);
      if (!goto_17) {
        reg_8 = String(getInt(reg_0));
        return reg_8;
      }
      if ((goto_17 || false)) {
        goto_17 = false;
        if (testBool(reg_0)) {
          goto_24 = !getBool(reg_0);
          if (!goto_24) {
            reg_11 = "true";
            return reg_11;
          }
          if ((goto_24 || false)) {
            goto_24 = false;
            reg_12 = "false";
            return reg_12;
          }
        }
      }
    }
  }
  reg_13 = get_metatable(reg_0);
  if (!(reg_13 == null)) {
    reg_16 = reg_13;
    reg_19 = get(reg_16, "__tostring");
    if (testFn(reg_19)) {
      reg_21 = getFn(reg_19);
      reg_22 = newStack();
      push(reg_22, reg_0);
      reg_24 = first(reg_21(reg_22));
      if ((typeof reg_24 === 'string')) {
        reg_26 = reg_24;
        return reg_26;
      }
      throw new Error("'__tostring' must return a string");
    }
  }
  if ((reg_0 instanceof record_18)) {
    reg_29 = "table:";
    reg_33 = (reg_29 + String(reg_0.val.a));
    return reg_33;
  }
  if ((reg_0 instanceof type_23)) {
    reg_35 = "userdata:";
    reg_40 = (reg_35 + String(reg_0.val.a));
    return reg_40;
  }
  reg_41 = typestr(reg_0);
  return reg_41;
}
function _assert (reg_0) {
  var reg_1, reg_3, reg_4, reg_5;
  reg_1 = next(reg_0);
  if (tobool(reg_1)) {
    reg_3 = newStack();
    push(reg_3, reg_1);
    return reg_3;
    if (true) { goto(15); };
  }
  reg_4 = next(reg_0);
  reg_5 = tostr(reg_4);
  if (testNil(reg_4)) {
    reg_5 = "assertion failed!";
  }
  throw new Error(reg_5);
}
function anyFn (reg_0) {
  var reg_1;
  reg_1 = new Function$29(reg_0);
  return reg_1;
}
function _error (reg_0) {
  throw new Error(tostr(next(reg_0)));
}
function stackof (reg_0) {
  var reg_1;
  reg_1 = newStack();
  push(reg_1, reg_0);
  return reg_1;
}
function _getmeta (reg_0) {
  var reg_1, reg_4, reg_9, reg_11;
  reg_1 = next(reg_0);
  if (testNil(reg_1)) {
    throw new Error("Lua: bad argument #1 to 'getmetatable' (value expected)");
  }
  reg_4 = get_metatable(reg_1);
  if (!(reg_4 == null)) {
    reg_9 = stackof(anyTable(reg_4));
    return reg_9;
  }
  reg_11 = stackof(nil());
  return reg_11;
}
function StringMapIter_any (map) {
  this.map = map
  this.i = 0
  this.keys = Object.keys(map)
}
StringMapIter_any.prototype.next = function () {
  if (this.i >= this.keys.length) return null
  var k = this.keys[this.i++]
  return {a: k, b: this.map[k]}
}
function nextKey (reg_0, reg_1) {
  var reg_8, reg_10, reg_11, reg_15, reg_18, reg_23, reg_26, reg_27, reg_29, reg_31, reg_32, reg_38, reg_39, reg_44, reg_45, reg_53, reg_54, reg_62, reg_65, reg_68, reg_70, reg_71, reg_77, reg_80, reg_81, reg_90, reg_95, reg_98, reg_99, reg_102;
  var goto_11=false, goto_37=false, goto_40=false, goto_58=false, goto_68=false, goto_104=false, goto_111=false, goto_118=false;
  goto_40 = !testNil(reg_1);
  if (!goto_40) {
    if ((reg_0.c.length > 0)) {
      reg_8 = new Integer(1);
      return reg_8;
      goto_11 = false;
    }
    goto_11 = false;
  }
  loop_4: while ((goto_40 || true)) {
    goto_40 = goto_40;
    if (!goto_40) {
      reg_10 = new StringMapIter_any(reg_0.b);
      reg_11 = reg_10.next();
      if (!(reg_11 == null)) {
        reg_15 = reg_11.a;
        reg_0.f = reg_10;
        reg_0.g = reg_15;
        reg_18 = reg_15;
        return reg_18;
      }
    }
    loop_2: while ((goto_40 || true)) {
      if (!goto_40) {
        goto_37 = !(reg_0.d.length > 0);
        if (!goto_37) {
          reg_23 = 0;
          reg_26 = reg_0.d[reg_23].a;
          return reg_26;
        }
        if ((goto_37 || false)) {
          goto_37 = false;
          reg_27 = nil();
          return reg_27;
        }
      }
      goto_40 = false;
      if ((reg_1 instanceof Integer)) {
        reg_29 = reg_1.val;
        reg_31 = reg_0.c.length;
        reg_32 = (reg_29 > 0);
        if (reg_32) {
          reg_32 = (reg_29 < reg_31);
        }
        goto_58 = !reg_32;
        if (!goto_58) {
          reg_38 = new Integer((reg_29 + 1));
          return reg_38;
        }
        if ((goto_58 || false)) {
          goto_58 = false;
          reg_39 = (reg_29 == reg_31);
          if (reg_39) {
            reg_39 = (reg_31 > 0);
          }
          goto_68 = !reg_39;
          if (!goto_68) {
            goto_11 = true;
            if (goto_11) break loop_2;
          }
          if ((goto_68 || false)) {
            goto_68 = false;
            goto_118 = true;
            if (goto_118) break loop_2;
          }
        }
      }
      goto_118 = !(typeof reg_1 === 'string');
      if (goto_118) break loop_2;
      reg_44 = reg_1;
      reg_45 = (reg_0.g == null);
      if (!reg_45) {
        reg_45 = !(reg_0.g == reg_44);
      }
      if (reg_45) {
        reg_53 = new StringMapIter_any(reg_0.b);
        reg_54 = reg_53.next();
        loop_3: while (!(reg_54 == null)) {
          if ((reg_54.a == reg_44)) {
            reg_0.f = reg_53;
            reg_0.g = reg_44;
            goto_104 = true;
            if (goto_104) break loop_3;
          }
        }
        if (!goto_104) {
          reg_62 = nil();
          return reg_62;
        }
        goto_104 = false;
      }
      goto_104 = false;
      reg_65 = reg_0.f.next();
      goto_111 = !(reg_65 == null);
      if (goto_111) break loop_2;
    }
    if (!goto_11) break loop_4;
  }
  if (!goto_118) {
    if (!goto_118) {
      goto_111 = goto_111;
      if (!goto_111) {
      }
      if ((goto_111 || false)) {
        goto_111 = false;
        reg_68 = reg_65.a;
        reg_0.g = reg_68;
        reg_70 = reg_68;
        return reg_70;
      }
    }
    goto_118 = false;
  }
  goto_118 = false;
  reg_71 = testNil(reg_1);
  if (reg_71) {
    reg_71 = (reg_0.d.length > 0);
  }
  if (reg_71) {
    reg_77 = 0;
    reg_80 = reg_0.d[reg_77].a;
    return reg_80;
  }
  reg_81 = 0;
  loop_1: while ((reg_81 < reg_0.d.length)) {
    if (equals(reg_1, reg_0.d[reg_81].a)) {
      reg_90 = (reg_81 + 1);
      if ((reg_90 < reg_0.d.length)) {
        reg_95 = (reg_81 + 1);
        reg_98 = reg_0.d[reg_95].a;
        return reg_98;
      }
      reg_99 = nil();
      return reg_99;
    }
    reg_81 = (reg_81 + 1);
  }
  reg_102 = nil();
  return reg_102;
}
function _next (reg_0) {
  var reg_1, reg_9, reg_12;
  reg_1 = next(reg_0);
  if (!testTable(reg_1)) {
    throw new Error((("Lua: bad argument #1 to 'next' (table expected, got " + typestr(reg_1)) + ")"));
  }
  reg_9 = next(reg_0);
  reg_12 = stackof(nextKey(getTable(reg_1), reg_9));
  return reg_12;
}
function _print (reg_0) {
  var reg_1, reg_2, reg_4, reg_10;
  var goto_9=false;
  reg_1 = true;
  reg_2 = "";
  loop_1: while (more(reg_0)) {
    reg_4 = next(reg_0);
    goto_9 = !reg_1;
    if (!goto_9) {
      reg_1 = false;
    }
    if ((goto_9 || false)) {
      goto_9 = false;
      reg_2 = (reg_2 + "\t");
    }
    reg_2 = (reg_2 + tostr(reg_4));
  }
  console.log(reg_2);
  reg_10 = newStack();
  return reg_10;
}
function length (reg_0) {
  var reg_4, reg_7;
  reg_4 = (reg_0.b.length - reg_0.a);
  if ((reg_4 < 0)) {
    reg_7 = 0;
    return reg_7;
  }
  return reg_4;
}
function anyInt (reg_0) {
  var reg_1;
  reg_1 = new Integer(reg_0);
  return reg_1;
}
Auro.charat = function (str, i) {
  return [str[i], i+1]
}
function isSpace (reg_0) {
  var reg_1, reg_2, reg_3;
  reg_1 = reg_0.charCodeAt(0);
  reg_3 = (reg_1 == 9);
  if (!reg_3) {
    reg_3 = (reg_1 == 10);
  }
  reg_2 = reg_3;
  if (!reg_2) {
    reg_2 = (reg_1 == 32);
  }
  return reg_2;
}
function parseNum (reg_0) {
  var reg_1, reg_2, reg_3, reg_4, reg_5, reg_7, reg_8, reg_9, reg_14, reg_15, reg_16, reg_26, reg_27, reg_28, reg_29, reg_35, reg_39, reg_45, reg_51, reg_57, reg_64, reg_65, reg_66, reg_67, reg_68, reg_75, reg_81, reg_87, reg_88, reg_91, reg_92, reg_93, reg_95, reg_99, reg_101, reg_102, reg_103, reg_111, reg_126, reg_133, reg_134, reg_135, reg_136, reg_140, reg_141, reg_146, reg_147, reg_148, reg_155, reg_157, reg_158, reg_159, reg_167, reg_183, reg_185, reg_186, reg_189, reg_190;
  var goto_64=false, goto_82=false, goto_100=false, goto_107=false, goto_145=false, goto_159=false, goto_210=false, goto_238=false, goto_266=false, goto_298=false;
  reg_1 = reg_0.length;
  reg_2 = 0;
  reg_4 = 0;
  reg_5 = false;
  loop_10: while ((reg_2 < reg_1)) {
    var _r = Auro.charat(reg_0, reg_2); reg_7 = _r[0]; reg_8 = _r[1];
    reg_3 = reg_7;
    reg_2 = reg_8;
    reg_9 = (reg_3.charCodeAt(0) == 48);
    if (reg_9) {
      reg_9 = (reg_2 < reg_1);
    }
    if (reg_9) {
      var _r = Auro.charat(reg_0, reg_2); reg_14 = _r[0]; reg_15 = _r[1];
      reg_3 = reg_14;
      reg_2 = reg_15;
      reg_16 = (reg_3.charCodeAt(0) == 88);
      if (!reg_16) {
        reg_16 = (reg_3.charCodeAt(0) == 120);
      }
      if (reg_16) {
        if (true) break loop_10;
      }
    }
    if (!isSpace(reg_3)) {
      goto_107 = true;
      if (goto_107) break loop_10;
    }
  }
  goto_107 = goto_107;
  if (!goto_107) {
    loop_9: while ((reg_2 < reg_1)) {
      var _r = Auro.charat(reg_0, reg_2); reg_26 = _r[0]; reg_27 = _r[1];
      reg_3 = reg_26;
      reg_2 = reg_27;
      reg_28 = reg_3.charCodeAt(0);
      reg_29 = (reg_28 <= 57);
      if (reg_29) {
        reg_29 = (reg_28 >= 48);
      }
      goto_64 = !reg_29;
      if (!goto_64) {
        reg_35 = (reg_4 * 16);
        reg_4 = (reg_35 + (reg_28 - 48));
      }
      if ((goto_64 || false)) {
        goto_64 = false;
        reg_39 = (reg_28 <= 70);
        if (reg_39) {
          reg_39 = (reg_28 >= 65);
        }
        goto_82 = !reg_39;
        if (!goto_82) {
          reg_45 = (reg_4 * 16);
          reg_4 = ((reg_45 + (reg_28 - 65)) + 10);
        }
        if ((goto_82 || false)) {
          goto_82 = false;
          reg_51 = (reg_28 >= 97);
          if (reg_51) {
            reg_51 = (reg_28 <= 102);
          }
          goto_100 = !reg_51;
          if (!goto_100) {
            reg_57 = (reg_4 * 16);
            reg_4 = ((reg_57 + (reg_28 - 97)) + 10);
          }
          if ((goto_100 || false)) {
            goto_100 = false;
            goto_145 = true;
            if (goto_145) break loop_9;
          }
        }
      }
    }
    goto_145 = goto_145;
  }
  if ((goto_107 || !goto_145)) {
    goto_107 = goto_107;
    if (!goto_107) {
    }
    loop_8: while ((goto_107 || (reg_2 < reg_1))) {
      if (!goto_107) {
        var _r = Auro.charat(reg_0, reg_2); reg_64 = _r[0]; reg_65 = _r[1];
        reg_3 = reg_64;
        reg_2 = reg_65;
      }
      goto_107 = false;
      reg_66 = reg_3.charCodeAt(0);
      reg_68 = (reg_66 == 46);
      if (!reg_68) {
        reg_68 = (reg_66 == 69);
      }
      reg_67 = reg_68;
      if (!reg_67) {
        reg_67 = (reg_66 == 101);
      }
      if (reg_67) {
        goto_159 = true;
        if (goto_159) break loop_8;
      }
      reg_75 = (reg_66 > 57);
      if (!reg_75) {
        reg_75 = (reg_66 < 48);
      }
      if (reg_75) {
        if (true) break loop_8;
      }
      reg_81 = (reg_4 * 10);
      reg_4 = (reg_81 + (reg_66 - 48));
      reg_5 = true;
    }
  }
  if ((goto_145 || !goto_159)) {
    goto_145 = false;
    loop_7: while ((reg_2 < reg_1)) {
      var _r = Auro.charat(reg_0, reg_2); reg_87 = _r[0]; reg_88 = _r[1];
      reg_3 = reg_87;
      reg_2 = reg_88;
      if (!isSpace(reg_3)) {
        reg_91 = nil();
        return reg_91;
      }
    }
    reg_92 = new Integer(reg_4);
    return reg_92;
  }
  goto_159 = false;
  reg_95 = 10;
  goto_210 = !(reg_3.charCodeAt(0) == 46);
  if (!goto_210) {
    reg_99 = 0;
    loop_6: while ((reg_2 < reg_1)) {
      var _r = Auro.charat(reg_0, reg_2); reg_101 = _r[0]; reg_102 = _r[1];
      reg_3 = reg_101;
      reg_2 = reg_102;
      reg_103 = (reg_3.charCodeAt(0) > 57);
      if (!reg_103) {
        reg_103 = (reg_3.charCodeAt(0) < 48);
      }
      if (reg_103) {
        if (true) break loop_6;
      }
      reg_111 = (reg_4 * 10);
      reg_4 = (reg_111 + (reg_3.charCodeAt(0) - 48));
      reg_99 = (reg_99 + 1);
      reg_5 = true;
    }
    reg_93 = reg_4;
    loop_5: while ((reg_99 > 0)) {
      reg_93 = (reg_93 / reg_95);
      reg_99 = (reg_99 - 1);
    }
  }
  if ((goto_210 || false)) {
    goto_210 = false;
    reg_93 = reg_4;
  }
  reg_126 = (reg_3.charCodeAt(0) == 69);
  if (!reg_126) {
    reg_126 = (reg_3.charCodeAt(0) == 101);
  }
  if (reg_126) {
    reg_133 = true;
    reg_134 = 0;
    var _r = Auro.charat(reg_0, reg_2); reg_135 = _r[0]; reg_136 = _r[1];
    reg_3 = reg_135;
    reg_2 = reg_136;
    goto_238 = !(reg_3.charCodeAt(0) == 45);
    if (!goto_238) {
      var _r = Auro.charat(reg_0, reg_2); reg_140 = _r[0]; reg_141 = _r[1];
      reg_3 = reg_140;
      reg_2 = reg_141;
      reg_133 = false;
    }
    if ((goto_238 || false)) {
      goto_238 = false;
      if ((reg_3.charCodeAt(0) == 43)) {
        var _r = Auro.charat(reg_0, reg_2); reg_146 = _r[0]; reg_147 = _r[1];
        reg_3 = reg_146;
        reg_2 = reg_147;
      }
    }
    reg_148 = (reg_3.charCodeAt(0) > 57);
    if (!reg_148) {
      reg_148 = (reg_3.charCodeAt(0) < 48);
    }
    if (reg_148) {
      reg_155 = nil();
      return reg_155;
    }
    goto_266 = true;
    if (!goto_266) {
    }
    loop_4: while ((goto_266 || (reg_2 < reg_1))) {
      if (!goto_266) {
        var _r = Auro.charat(reg_0, reg_2); reg_157 = _r[0]; reg_158 = _r[1];
        reg_3 = reg_157;
        reg_2 = reg_158;
      }
      goto_266 = false;
      reg_159 = (reg_3.charCodeAt(0) > 57);
      if (!reg_159) {
        reg_159 = (reg_3.charCodeAt(0) < 48);
      }
      if (reg_159) {
        if (true) break loop_4;
      }
      reg_167 = (reg_134 * 10);
      reg_134 = (reg_167 + (reg_3.charCodeAt(0) - 48));
    }
    goto_298 = !reg_133;
    if (!goto_298) {
      loop_3: while ((reg_134 > 0)) {
        reg_93 = (reg_93 * reg_95);
        reg_134 = (reg_134 - 1);
      }
    }
    if ((goto_298 || false)) {
      goto_298 = false;
      loop_2: while ((reg_134 > 0)) {
        reg_93 = (reg_93 / reg_95);
        reg_134 = (reg_134 - 1);
      }
    }
  }
  if (!reg_5) {
    reg_183 = nil();
    return reg_183;
  }
  loop_1: while ((reg_2 < reg_1)) {
    var _r = Auro.charat(reg_0, reg_2); reg_185 = _r[0]; reg_186 = _r[1];
    reg_3 = reg_185;
    reg_2 = reg_186;
    if (!isSpace(reg_3)) {
      reg_189 = nil();
      return reg_189;
    }
  }
  reg_190 = reg_93;
  return reg_190;
}
function getNum (reg_0) {
  var reg_1, reg_6, reg_7;
  reg_1 = (reg_0 instanceof Integer);
  if (!reg_1) {
    reg_1 = (typeof reg_0 === 'number');
  }
  if (reg_1) {
    return reg_0;
  }
  if ((typeof reg_0 === 'string')) {
    reg_6 = parseNum(reg_0);
    return reg_6;
  }
  reg_7 = nil();
  return reg_7;
}
function simple_number (reg_0, reg_1, reg_2) {
  var reg_3, reg_5;
  reg_3 = getNum(reg_0);
  if ((reg_3 instanceof Integer)) {
    reg_5 = reg_3.val;
    return reg_5;
  }
  throw new Error((((((("Lua: bad argument #" + reg_1) + " to '") + reg_2) + "' (number expected, got ") + typestr(reg_3)) + ")"));
}
function _select (reg_0) {
  var reg_1, reg_2, reg_9, reg_12;
  reg_1 = next(reg_0);
  reg_2 = testStr(reg_1);
  if (reg_2) {
    reg_2 = (getStr(reg_1) == "#");
  }
  if (reg_2) {
    reg_9 = stackof(anyInt(length(reg_0)));
    return reg_9;
  }
  reg_12 = simple_number(reg_1, "1", "select");
  if ((reg_12 < 1)) {
    throw new Error("bad argument #1 to 'select' (index out of range)");
  }
  reg_0.a = reg_12;
  return reg_0;
}
function _setmeta (reg_0) {
  var reg_1, reg_2, reg_17, reg_21;
  reg_1 = next(reg_0);
  reg_2 = next(reg_0);
  if (!testTable(reg_1)) {
    throw new Error((("Lua: bad argument #1 to 'getmetatable' (table expected, got " + typestr(reg_1)) + ")"));
  }
  if (!testTable(reg_2)) {
    throw new Error((("Lua: bad argument typestr(reg_2) to 'getmetatable' (table expected, got " + typestr(reg_2)) + ")"));
  }
  reg_17 = getTable(reg_1);
  reg_17.e = getTable(reg_2);
  reg_21 = stackof(reg_1);
  return reg_21;
}
function _tostring (reg_0) {
  var reg_4;
  reg_4 = stackof(anyStr(tostr(next(reg_0))));
  return reg_4;
}
function _tonumber (reg_0) {
  var reg_1, reg_2, reg_5, reg_9, reg_10;
  reg_1 = next(reg_0);
  reg_2 = (reg_1 instanceof Integer);
  if (!reg_2) {
    reg_2 = (typeof reg_1 === 'number');
  }
  if (reg_2) {
    reg_5 = stackof(reg_1);
    return reg_5;
  }
  if ((typeof reg_1 === 'string')) {
    reg_9 = stackof(parseNum(reg_1));
    return reg_9;
  }
  reg_10 = newStack();
  return reg_10;
}
function _type (reg_0) {
  var reg_4;
  reg_4 = stackof(anyStr(typestr(next(reg_0))));
  return reg_4;
}
function _pack (reg_0) {
  var reg_1, reg_2, reg_12;
  reg_1 = emptyTable();
  reg_2 = 0;
  loop_1: while (more(reg_0)) {
    reg_2 = (reg_2 + 1);
    set(reg_1, anyInt(reg_2), next(reg_0));
  }
  set(reg_1, anyStr("n"), anyInt(reg_2));
  reg_12 = stackof(anyTable(reg_1));
  return reg_12;
}
function simple_number_or (reg_0, reg_1, reg_2, reg_3) {
  var reg_5;
  if (testNil(reg_0)) {
    return reg_1;
  }
  reg_5 = simple_number(reg_0, reg_2, reg_3);
  return reg_5;
}
function call (reg_0, reg_1) {
  var reg_4;
  if (testFn(reg_0)) {
    reg_4 = getFn(reg_0)(reg_1);
    return reg_4;
    if (true) { goto(8); };
  }
  throw new Error("Lua: attempt to call a non-function value");
}
function lua$length (reg_0) {
  var reg_4, reg_5, reg_8, reg_11, reg_16, reg_18, reg_22, reg_23, reg_38, reg_40, reg_48, reg_57;
  var goto_52=false, goto_61=false, goto_72=false;
  if (testStr(reg_0)) {
    reg_4 = anyInt(getStr(reg_0).length);
    return reg_4;
  }
  reg_5 = get_metatable(reg_0);
  if (!(reg_5 == null)) {
    reg_8 = reg_5;
    reg_11 = get(reg_8, anyStr("__len"));
    if (!testNil(reg_11)) {
      reg_16 = first(call(reg_11, stackof(reg_0)));
      return reg_16;
    }
  }
  if ((reg_0 instanceof record_18)) {
    reg_18 = reg_0.val;
    reg_22 = (reg_18.c.length - 1);
    reg_23 = (reg_22 >= 0);
    if (reg_23) {
      reg_23 = (reg_18.c[reg_22] instanceof type_20);
    }
    goto_61 = !reg_23;
    if (!goto_61) {
      loop_2: while ((reg_22 >= 0)) {
        goto_52 = !testNil(reg_18.c[reg_22]);
        if (!goto_52) {
          reg_22 = (reg_22 - 1);
        }
        if ((goto_52 || false)) {
          goto_52 = false;
          reg_38 = new Integer((reg_22 + 1));
          return reg_38;
        }
      }
      reg_40 = new Integer(0);
      return reg_40;
    }
    if ((goto_61 || false)) {
      goto_61 = false;
      goto_72 = !(get(reg_18, new Integer((reg_22 + 2))) instanceof type_20);
      if (!goto_72) {
        reg_48 = new Integer((reg_22 + 1));
        return reg_48;
      }
      if ((goto_72 || false)) {
        goto_72 = false;
        reg_22 = (reg_22 + 3);
        loop_1: while (true) {
          if ((get(reg_18, anyInt(reg_22)) instanceof type_20)) {
            reg_57 = anyInt((reg_22 - 1));
            return reg_57;
          }
          reg_22 = (reg_22 + 1);
        }
      }
    }
  }
  throw new Error((("Lua: attempt to get length of a " + typestr(reg_0)) + " value"));
}
function _unpack (reg_0) {
  var reg_1, reg_9, reg_14, reg_15, reg_20, reg_21;
  reg_1 = next(reg_0);
  if (!testTable(reg_1)) {
    throw new Error((("Lua: bad argument #1 to 'table.unpack' (table expected, got " + typestr(reg_1)) + ")"));
  }
  reg_9 = getTable(reg_1);
  reg_14 = simple_number_or(next(reg_0), 1, "2", "table.unpack");
  reg_15 = next(reg_0);
  reg_20 = simple_number_or(reg_15, getInt(lua$length(reg_1)), "3", "table.unpack");
  reg_21 = newStack();
  loop_1: while ((reg_14 <= reg_20)) {
    push(reg_21, get(reg_9, anyInt(reg_14)));
    reg_14 = (reg_14 + 1);
  }
  return reg_21;
}
var cns3 = parseNum("1")
function meta_arith (reg_0, reg_1, reg_2) {
  var reg_3, reg_10;
  reg_3 = meta_binop(reg_0, reg_1, reg_2);
  if ((reg_3 == null)) {
    throw new Error((("Lua: attempt to perform arithmetic on a " + typestr(reg_0)) + " value"));
  }
  reg_10 = reg_3;
  return reg_10;
}
function add (reg_0, reg_1) {
  var reg_2, reg_8, reg_9, reg_10, reg_12, reg_13, reg_14, reg_16, reg_18;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_8 = new Integer((reg_0.val + reg_1.val));
    return reg_8;
  }
  var _r = getFloats(reg_0, reg_1); reg_12 = _r[0]; reg_13 = _r[1]; reg_14 = _r[2];
  reg_9 = reg_12;
  reg_10 = reg_13;
  if (reg_14) {
    reg_16 = (reg_9 + reg_10);
    return reg_16;
  }
  reg_18 = meta_arith(reg_0, reg_1, "__add");
  return reg_18;
}
function lua$get (reg_0, reg_1) {
  var reg_4, reg_7, reg_10, reg_13, reg_15, reg_17, reg_18, reg_20, reg_22;
  if (testTable(reg_0)) {
    reg_4 = get(getTable(reg_0), reg_1);
    if (!testNil(reg_4)) {
      return reg_4;
    }
  }
  reg_7 = get_metatable(reg_0);
  if (!(reg_7 == null)) {
    reg_10 = reg_7;
    reg_13 = get(reg_10, anyStr("__index"));
    if (testTable(reg_13)) {
      reg_15 = lua$get(reg_13, reg_1);
      return reg_15;
    }
    if (testFn(reg_13)) {
      reg_17 = getFn(reg_13);
      reg_18 = newStack();
      push(reg_18, reg_0);
      push(reg_18, reg_1);
      reg_20 = first(reg_17(reg_18));
      return reg_20;
    }
  }
  if (testTable(reg_0)) {
    reg_22 = nil();
    return reg_22;
  }
  throw new Error((("Lua: tried to index a non-table value (" + tostr(reg_0)) + ")"));
}
function function$1 (reg_0, reg_1) {
  var reg_5, reg_6, reg_9, reg_11, reg_12;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_6 = add(reg_6, cns3);
  reg_9 = lua$get(reg_5, reg_6);
  if (tobool(reg_9)) {
    reg_11 = newStack();
    push(reg_11, reg_6);
    push(reg_11, reg_9);
    return reg_11;
  }
  reg_12 = newStack();
  return reg_12;
}
var cns4 = parseNum("0")
function lua_lib_table$function (reg_0, reg_1) {
  var reg_3, reg_4, reg_6, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_6 = anyFn((function (a) { return function$1(a,this) }).bind(reg_3));
  reg_7 = newStack();
  push(reg_7, reg_6);
  push(reg_7, reg_4);
  push(reg_7, cns4);
  return reg_7;
}
var cns5 = anyStr("ipairs")
function lua$set (reg_0, reg_1, reg_2) {
  var goto_5=false;
  goto_5 = !(reg_0 instanceof record_18);
  if (!goto_5) {
    set(reg_0.val, reg_1, reg_2);
  }
  if ((goto_5 || false)) {
    goto_5 = false;
    throw new Error((("Lua: tried to index a non-table value (" + tostr(reg_0)) + ")"));
  }
  return;
}
var cns6 = anyStr("next")
function function$3 (reg_0, reg_1) {
  var reg_2, reg_5, reg_6, reg_9, reg_10, reg_13;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_9 = lua$get(reg_2.a, cns6);
  reg_10 = newStack();
  push(reg_10, reg_5);
  push(reg_10, reg_6);
  reg_6 = first(call(reg_9, reg_10));
  reg_13 = newStack();
  push(reg_13, reg_6);
  push(reg_13, lua$get(reg_5, reg_6));
  return reg_13;
}
function function$2 (reg_0, reg_1) {
  var reg_3, reg_4, reg_6, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_6 = anyFn((function (a) { return function$3(a,this) }).bind(reg_3));
  reg_7 = newStack();
  push(reg_7, reg_6);
  push(reg_7, reg_4);
  push(reg_7, nil());
  return reg_7;
}
var cns7 = anyStr("pairs")
var cns8 = anyStr("type")
function eq (reg_0, reg_1) {
  var reg_3;
  reg_3 = equals(reg_0, reg_1);
  return reg_3;
}
var cns9 = anyStr("number")
var cns10 = anyStr("tostring")
var cns11 = anyStr("type")
var cns12 = anyStr("string")
var cns13 = anyStr("error")
var cns14 = anyStr("bad argument #")
var cns15 = anyStr(" for '")
var cns16 = anyStr(" (expected string, got ")
var cns17 = anyStr(")")
function concat (reg_0, reg_1) {
  var reg_5;
  reg_5 = anyStr((tostr(reg_0) + tostr(reg_1)));
  return reg_5;
}
function function$4 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_11, reg_13, reg_15, reg_18, reg_24, reg_25, reg_30, reg_31, reg_37, reg_40, reg_41, reg_42, reg_43, reg_44, reg_53;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_10 = lua$get(reg_1.a, cns8);
  reg_11 = newStack();
  push(reg_11, reg_4);
  reg_13 = first(call(reg_10, reg_11));
  reg_15 = eq(reg_4, nil());
  if (tobool(reg_15)) {
    reg_15 = reg_5;
  }
  if (tobool(reg_15)) {
    reg_18 = newStack();
    push(reg_18, reg_5);
    return reg_18;
  }
  if (tobool(eq(reg_13, cns9))) {
    reg_24 = lua$get(reg_1.a, cns10);
    reg_25 = newStack();
    push(reg_25, reg_4);
    reg_4 = first(call(reg_24, reg_25));
  }
  reg_30 = lua$get(reg_1.a, cns11);
  reg_31 = newStack();
  push(reg_31, reg_4);
  if (tobool(eq(first(call(reg_30, reg_31)), cns12))) {
    reg_37 = newStack();
    push(reg_37, reg_4);
    return reg_37;
  }
  reg_40 = lua$get(reg_1.a, cns13);
  reg_41 = newStack();
  reg_42 = cns14;
  reg_43 = cns15;
  reg_44 = cns16;
  push(reg_41, concat(reg_42, concat(reg_6, concat(reg_43, concat(reg_7, concat(reg_44, concat(reg_13, cns17)))))));
  reg_52 = call(reg_40, reg_41);
  reg_53 = newStack();
  return reg_53;
}
var cns18 = anyStr("type")
var cns19 = anyStr("i")
var cns20 = anyStr("string")
var cns21 = anyStr("tonumber")
var cns22 = anyStr("type")
var cns23 = anyStr("number")
var cns24 = anyStr("error")
var cns25 = anyStr("bad argument #")
var cns26 = anyStr(" for '")
var cns27 = anyStr(" (expected number, got ")
var cns28 = anyStr(")")
function function$5 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_11, reg_16, reg_18, reg_21, reg_27, reg_28, reg_33, reg_34, reg_40, reg_43, reg_44, reg_45, reg_46, reg_47, reg_56;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_10 = lua$get(reg_1.a, cns18);
  reg_11 = newStack();
  push(reg_11, lua$get(reg_1.a, cns19));
  reg_16 = first(call(reg_10, reg_11));
  reg_18 = eq(reg_4, nil());
  if (tobool(reg_18)) {
    reg_18 = reg_5;
  }
  if (tobool(reg_18)) {
    reg_21 = newStack();
    push(reg_21, reg_5);
    return reg_21;
  }
  if (tobool(eq(reg_16, cns20))) {
    reg_27 = lua$get(reg_1.a, cns21);
    reg_28 = newStack();
    push(reg_28, reg_4);
    reg_4 = first(call(reg_27, reg_28));
  }
  reg_33 = lua$get(reg_1.a, cns22);
  reg_34 = newStack();
  push(reg_34, reg_4);
  if (tobool(eq(first(call(reg_33, reg_34)), cns23))) {
    reg_40 = newStack();
    push(reg_40, reg_4);
    return reg_40;
  }
  reg_43 = lua$get(reg_1.a, cns24);
  reg_44 = newStack();
  reg_45 = cns25;
  reg_46 = cns26;
  reg_47 = cns27;
  push(reg_44, concat(reg_45, concat(reg_6, concat(reg_46, concat(reg_7, concat(reg_47, concat(reg_16, cns28)))))));
  reg_55 = call(reg_43, reg_44);
  reg_56 = newStack();
  return reg_56;
}
var cns29 = anyStr("table")
var cns30 = anyStr("concat")
var cns31 = anyStr("type")
var cns32 = anyStr("table")
function anyBool (reg_0) {
  var reg_1;
  reg_1 = reg_0;
  return reg_1;
}
function ne (reg_0, reg_1) {
  var reg_4;
  reg_4 = anyBool(!equals(reg_0, reg_1));
  return reg_4;
}
var cns33 = anyStr("error")
var cns34 = anyStr("bad argument #1 for 'table.concat' (expected table, got ")
var cns35 = anyStr("type")
var cns36 = anyStr(")")
var cns37 = anyStr("")
var cns38 = parseNum("1")
var cns39 = anyStr("table.concat")
var cns40 = parseNum("1")
var cns41 = parseNum("2")
var cns42 = anyStr("table.concat")
var cns43 = parseNum("3")
var cns44 = anyStr("table.concat")
function str_cmp (reg_0, reg_1) {
  var reg_2, reg_3, reg_4, reg_6, reg_8, reg_10, reg_11, reg_13, reg_17, reg_19, reg_25, reg_27, reg_28;
  reg_2 = reg_0.length;
  reg_3 = reg_1.length;
  reg_4 = reg_2;
  if ((reg_3 < reg_2)) {
    reg_4 = reg_3;
  }
  reg_6 = 0;
  loop_1: while ((reg_6 < reg_4)) {
    var _r = Auro.charat(reg_0, reg_6); reg_8 = _r[0]; reg_9 = _r[1];
    reg_10 = reg_8.charCodeAt(0);
    var _r = Auro.charat(reg_1, reg_6); reg_11 = _r[0]; reg_12 = _r[1];
    reg_13 = reg_11.charCodeAt(0);
    if ((reg_10 < reg_13)) {
      reg_17 = (0 - 1);
      return reg_17;
    }
    if ((reg_10 > reg_13)) {
      reg_19 = 1;
      return reg_19;
    }
    reg_6 = (reg_6 + 1);
  }
  if ((reg_2 < reg_3)) {
    reg_25 = (0 - 1);
    return reg_25;
  }
  if ((reg_2 > reg_3)) {
    reg_27 = 1;
    return reg_27;
  }
  reg_28 = 0;
  return reg_28;
}
function meta_cmp (reg_0, reg_1, reg_2) {
  var reg_3, reg_12;
  reg_3 = meta_binop(reg_0, reg_1, reg_2);
  if ((reg_3 == null)) {
    throw new Error(((("Lua: attempt to compare " + typestr(reg_0)) + " with a ") + typestr(reg_1)));
  }
  reg_12 = reg_3;
  return reg_12;
}
function _le (reg_0, reg_1) {
  var reg_2, reg_7, reg_8, reg_9, reg_11, reg_12, reg_13, reg_14, reg_15, reg_22, reg_25;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_7 = (reg_0.val <= reg_1.val);
    return reg_7;
  }
  var _r = getFloats(reg_0, reg_1); reg_11 = _r[0]; reg_12 = _r[1]; reg_13 = _r[2];
  reg_8 = reg_11;
  reg_9 = reg_12;
  if (reg_13) {
    reg_14 = (reg_8 <= reg_9);
    return reg_14;
  }
  reg_15 = (typeof reg_0 === 'string');
  if (reg_15) {
    reg_15 = (typeof reg_1 === 'string');
  }
  if (reg_15) {
    reg_22 = (str_cmp(reg_0, reg_1) <= 0);
    return reg_22;
  }
  reg_25 = tobool(meta_cmp(reg_0, reg_1, "__le"));
  return reg_25;
}
function gt (reg_0, reg_1) {
  var reg_4;
  reg_4 = !_le(reg_0, reg_1);
  return reg_4;
}
var cns45 = anyStr("")
var cns46 = parseNum("1")
var cns47 = parseNum("1")
var cns48 = parseNum("0")
function _lt (reg_0, reg_1) {
  var reg_2, reg_7, reg_8, reg_9, reg_11, reg_12, reg_13, reg_14, reg_15, reg_22, reg_25;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_7 = (reg_0.val < reg_1.val);
    return reg_7;
  }
  var _r = getFloats(reg_0, reg_1); reg_11 = _r[0]; reg_12 = _r[1]; reg_13 = _r[2];
  reg_8 = reg_11;
  reg_9 = reg_12;
  if (reg_13) {
    reg_14 = (reg_8 < reg_9);
    return reg_14;
  }
  reg_15 = (typeof reg_0 === 'string');
  if (reg_15) {
    reg_15 = (typeof reg_1 === 'string');
  }
  if (reg_15) {
    reg_22 = (str_cmp(reg_0, reg_1) < 0);
    return reg_22;
  }
  reg_25 = tobool(meta_cmp(reg_0, reg_1, "__lt"));
  return reg_25;
}
function lt (reg_0, reg_1) {
  var reg_3;
  reg_3 = _lt(reg_0, reg_1);
  return reg_3;
}
function table_concat (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_11, reg_19, reg_20, reg_21, reg_24, reg_25, reg_32, reg_33, reg_39, reg_40, reg_46, reg_47, reg_59, reg_61, reg_63, reg_64, reg_65, reg_67, reg_77;
  var goto_99=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_10 = lua$get(reg_1.a, cns31);
  reg_11 = newStack();
  push(reg_11, reg_4);
  if (tobool(ne(first(call(reg_10, reg_11)), cns32))) {
    reg_19 = lua$get(reg_1.a, cns33);
    reg_20 = newStack();
    reg_21 = cns34;
    reg_24 = lua$get(reg_1.a, cns35);
    reg_25 = newStack();
    push(reg_25, reg_4);
    push(reg_20, concat(reg_21, concat(first(call(reg_24, reg_25)), cns36)));
    reg_31 = call(reg_19, reg_20);
  }
  reg_32 = reg_1.b;
  reg_33 = newStack();
  push(reg_33, reg_5);
  push(reg_33, cns37);
  push(reg_33, cns38);
  push(reg_33, cns39);
  reg_5 = first(call(reg_32, reg_33));
  reg_39 = reg_1.c;
  reg_40 = newStack();
  push(reg_40, reg_6);
  push(reg_40, cns40);
  push(reg_40, cns41);
  push(reg_40, cns42);
  reg_6 = first(call(reg_39, reg_40));
  reg_46 = reg_1.c;
  reg_47 = newStack();
  push(reg_47, reg_7);
  push(reg_47, lua$length(reg_4));
  push(reg_47, cns43);
  push(reg_47, cns44);
  reg_7 = first(call(reg_46, reg_47));
  if (tobool(gt(reg_7, lua$length(reg_4)))) {
    reg_7 = lua$length(reg_4);
  }
  if (tobool(gt(reg_6, reg_7))) {
    reg_59 = newStack();
    push(reg_59, cns45);
    return reg_59;
  }
  reg_61 = lua$get(reg_4, reg_6);
  reg_63 = add(reg_6, cns46);
  reg_64 = reg_7;
  reg_65 = cns47;
  reg_67 = lt(reg_65, cns48);
  loop_1: while (true) {
    goto_99 = tobool(reg_67);
    if (!goto_99) {
      if (tobool(gt(reg_63, reg_64))) break loop_1;
    }
    if ((goto_99 || false)) {
      goto_99 = false;
      if (tobool(lt(reg_63, reg_64))) break loop_1;
    }
    reg_61 = concat(reg_61, reg_5);
    reg_61 = concat(reg_61, lua$get(reg_4, reg_63));
    reg_63 = add(reg_63, reg_65);
  }
  reg_77 = newStack();
  push(reg_77, reg_61);
  return reg_77;
}
var cns49 = anyStr("table")
var cns50 = anyStr("insert")
var cns51 = parseNum("1")
var cns52 = parseNum("2")
var cns53 = anyStr("table.insert")
function le (reg_0, reg_1) {
  var reg_3;
  reg_3 = _le(reg_0, reg_1);
  return reg_3;
}
var cns54 = parseNum("1")
function unm (reg_0) {
  var reg_5, reg_11, reg_12, reg_15, reg_18, reg_20, reg_21, reg_23;
  if ((reg_0 instanceof Integer)) {
    reg_5 = new Integer((0 - reg_0.val));
    return reg_5;
  }
  if ((typeof reg_0 === 'number')) {
    reg_11 = (0 - reg_0);
    return reg_11;
  }
  reg_12 = get_metatable(reg_0);
  if (!(reg_12 == null)) {
    reg_15 = reg_12;
    reg_18 = get(reg_15, "__unm");
    if (testFn(reg_18)) {
      reg_20 = getFn(reg_18);
      reg_21 = newStack();
      push(reg_21, reg_0);
      reg_23 = first(reg_20(reg_21));
      return reg_23;
    }
  }
  throw new Error((("Lua: attempt to perform arithmetic on a " + typestr(reg_0)) + " value"));
}
var cns55 = parseNum("0")
var cns56 = parseNum("1")
function table_insert (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_13, reg_14, reg_23, reg_24, reg_26, reg_28, reg_38;
  var goto_43=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  if (tobool(eq(reg_6, nil()))) {
    reg_6 = reg_5;
    reg_5 = add(lua$length(reg_4), cns51);
  }
  reg_13 = reg_1.c;
  reg_14 = newStack();
  push(reg_14, reg_5);
  push(reg_14, nil());
  push(reg_14, cns52);
  push(reg_14, cns53);
  reg_5 = first(call(reg_13, reg_14));
  if (tobool(le(reg_5, lua$length(reg_4)))) {
    reg_23 = lua$length(reg_4);
    reg_24 = reg_5;
    reg_26 = unm(cns54);
    reg_28 = lt(reg_26, cns55);
    loop_1: while (true) {
      goto_43 = tobool(reg_28);
      if (!goto_43) {
        if (tobool(gt(reg_23, reg_24))) break loop_1;
      }
      if ((goto_43 || false)) {
        goto_43 = false;
        if (tobool(lt(reg_23, reg_24))) break loop_1;
      }
      lua$set(reg_4, add(reg_23, cns56), lua$get(reg_4, reg_23));
      reg_23 = add(reg_23, reg_26);
    }
  }
  lua$set(reg_4, reg_5, reg_6);
  reg_38 = newStack();
  return reg_38;
}
var cns57 = anyStr("table")
var cns58 = anyStr("move")
var cns59 = parseNum("1")
var cns60 = parseNum("1")
function sub (reg_0, reg_1) {
  var reg_2, reg_8, reg_9, reg_10, reg_12, reg_13, reg_14, reg_16, reg_18;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_8 = new Integer((reg_0.val - reg_1.val));
    return reg_8;
  }
  var _r = getFloats(reg_0, reg_1); reg_12 = _r[0]; reg_13 = _r[1]; reg_14 = _r[2];
  reg_9 = reg_12;
  reg_10 = reg_13;
  if (reg_14) {
    reg_16 = (reg_9 - reg_10);
    return reg_16;
  }
  reg_18 = meta_arith(reg_0, reg_1, "__sub");
  return reg_18;
}
function ge (reg_0, reg_1) {
  var reg_4;
  reg_4 = !_lt(reg_0, reg_1);
  return reg_4;
}
var cns61 = parseNum("1")
var cns62 = parseNum("1")
function table_move (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_8, reg_9, reg_11, reg_31;
  var goto_32=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = next(reg_0);
  reg_9 = reg_8;
  if (!tobool(reg_8)) {
    reg_9 = reg_4;
  }
  reg_8 = reg_9;
  reg_11 = gt(reg_7, reg_6);
  if (!tobool(reg_11)) {
    reg_11 = le(reg_7, reg_5);
  }
  goto_32 = !tobool(reg_11);
  if (!goto_32) {
    loop_2: while (tobool(le(reg_5, reg_6))) {
      lua$set(reg_8, reg_7, lua$get(reg_4, reg_5));
      reg_7 = add(reg_7, cns59);
      reg_5 = add(reg_5, cns60);
    }
  }
  if ((goto_32 || false)) {
    goto_32 = false;
    reg_7 = add(reg_7, sub(reg_6, reg_5));
    loop_1: while (tobool(ge(reg_6, reg_5))) {
      lua$set(reg_8, reg_7, lua$get(reg_4, reg_6));
      reg_7 = sub(reg_7, cns61);
      reg_6 = sub(reg_6, cns62);
    }
  }
  reg_31 = newStack();
  push(reg_31, reg_8);
  return reg_31;
}
var cns63 = anyStr("table")
var cns64 = anyStr("remove")
function not (reg_0) {
  var reg_3;
  reg_3 = anyBool(!tobool(reg_0));
  return reg_3;
}
var cns65 = parseNum("1")
var cns66 = anyStr("error")
var cns67 = anyStr("bad argument #1 to 'table.remove' (position out of bounds)")
var cns68 = anyStr("table")
var cns69 = anyStr("move")
var cns70 = parseNum("1")
function table_remove (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_13, reg_18, reg_19, reg_22, reg_29, reg_30, reg_35;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = lua$length(reg_4);
  if (tobool(not(reg_5))) {
    reg_5 = reg_6;
  }
  if (tobool(eq(reg_5, add(reg_6, cns65)))) {
    reg_13 = newStack();
    return reg_13;
  }
  if (tobool(gt(reg_5, reg_6))) {
    reg_18 = lua$get(reg_1.a, cns66);
    reg_19 = newStack();
    push(reg_19, cns67);
    reg_21 = call(reg_18, reg_19);
  }
  reg_22 = lua$get(reg_4, reg_5);
  if (tobool(lt(reg_5, reg_6))) {
    reg_29 = lua$get(lua$get(reg_1.a, cns68), cns69);
    reg_30 = newStack();
    push(reg_30, reg_4);
    push(reg_30, add(reg_5, cns70));
    push(reg_30, reg_6);
    push(reg_30, reg_5);
    reg_33 = call(reg_29, reg_30);
  }
  lua$set(reg_4, reg_6, nil());
  reg_35 = newStack();
  push(reg_35, reg_22);
  return reg_35;
}
function lua_main (reg_0) {
  var reg_1, reg_2, reg_4, reg_8, reg_17, reg_18, reg_23, reg_24, reg_29, reg_30, reg_35, reg_36, reg_39;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1, c: reg_1};
  reg_2.a = reg_0;
  reg_4 = anyFn((function (a) { return lua_lib_table$function(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns5, reg_4);
  reg_8 = anyFn((function (a) { return function$2(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns7, reg_8);
  reg_2.b = anyFn((function (a) { return function$4(a,this) }).bind(reg_2));
  reg_2.c = anyFn((function (a) { return function$5(a,this) }).bind(reg_2));
  reg_17 = lua$get(reg_2.a, cns29);
  reg_18 = cns30;
  lua$set(reg_17, reg_18, anyFn((function (a) { return table_concat(a,this) }).bind(reg_2)));
  reg_23 = lua$get(reg_2.a, cns49);
  reg_24 = cns50;
  lua$set(reg_23, reg_24, anyFn((function (a) { return table_insert(a,this) }).bind(reg_2)));
  reg_29 = lua$get(reg_2.a, cns57);
  reg_30 = cns58;
  lua$set(reg_29, reg_30, anyFn((function (a) { return table_move(a,this) }).bind(reg_2)));
  reg_35 = lua$get(reg_2.a, cns63);
  reg_36 = cns64;
  lua$set(reg_35, reg_36, anyFn((function (a) { return table_remove(a,this) }).bind(reg_2)));
  reg_39 = newStack();
  return reg_39;
}
function str_get_nums (reg_0) {
  var reg_2, reg_4, reg_5;
  reg_2 = getNum(next(reg_0));
  reg_4 = getNum(next(reg_0));
  reg_5 = (reg_2 instanceof type_20);
  if (!reg_5) {
    reg_5 = (reg_4 instanceof type_20);
  }
  if (reg_5) {
    throw new Error("attempt to perform arithmetic on a string");
  }
  return [reg_2, reg_4];
}
function _stradd (reg_0) {
  var reg_3, reg_4, reg_6;
  var _r = str_get_nums(reg_0); reg_3 = _r[0]; reg_4 = _r[1];
  reg_6 = stackof(add(reg_3, reg_4));
  return reg_6;
}
function _strsub (reg_0) {
  var reg_3, reg_4, reg_6;
  var _r = str_get_nums(reg_0); reg_3 = _r[0]; reg_4 = _r[1];
  reg_6 = stackof(sub(reg_3, reg_4));
  return reg_6;
}
function mul (reg_0, reg_1) {
  var reg_2, reg_8, reg_9, reg_10, reg_12, reg_13, reg_14, reg_16, reg_18;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_8 = new Integer((reg_0.val * reg_1.val));
    return reg_8;
  }
  var _r = getFloats(reg_0, reg_1); reg_12 = _r[0]; reg_13 = _r[1]; reg_14 = _r[2];
  reg_9 = reg_12;
  reg_10 = reg_13;
  if (reg_14) {
    reg_16 = (reg_9 * reg_10);
    return reg_16;
  }
  reg_18 = meta_arith(reg_0, reg_1, "__mul");
  return reg_18;
}
function _strmul (reg_0) {
  var reg_3, reg_4, reg_6;
  var _r = str_get_nums(reg_0); reg_3 = _r[0]; reg_4 = _r[1];
  reg_6 = stackof(mul(reg_3, reg_4));
  return reg_6;
}
function div (reg_0, reg_1) {
  var reg_2, reg_3, reg_5, reg_6, reg_7, reg_9, reg_11;
  var _r = getFloats(reg_0, reg_1); reg_5 = _r[0]; reg_6 = _r[1]; reg_7 = _r[2];
  reg_2 = reg_5;
  reg_3 = reg_6;
  if (reg_7) {
    reg_9 = (reg_2 / reg_3);
    return reg_9;
  }
  reg_11 = meta_arith(reg_0, reg_1, "__div");
  return reg_11;
}
function _strdiv (reg_0) {
  var reg_3, reg_4, reg_6;
  var _r = str_get_nums(reg_0); reg_3 = _r[0]; reg_4 = _r[1];
  reg_6 = stackof(div(reg_3, reg_4));
  return reg_6;
}
function _strunm (reg_0) {
  var reg_2, reg_6;
  reg_2 = getNum(next(reg_0));
  if ((reg_2 instanceof type_20)) {
    throw new Error("attempt to perform arithmetic on a string");
  }
  reg_6 = stackof(unm(reg_2));
  return reg_6;
}
function simple_string (reg_0, reg_1, reg_2) {
  var reg_4, reg_7;
  if (testStr(reg_0)) {
    reg_4 = getStr(reg_0);
    return reg_4;
  }
  if (testInt(reg_0)) {
    reg_7 = String(getInt(reg_0));
    return reg_7;
  }
  throw new Error((((((("Lua: bad argument #" + reg_1) + " to '") + reg_2) + "' (string expected, got ") + typestr(reg_0)) + ")"));
}
function valid_start_index (reg_0, reg_1) {
  var reg_9;
  var goto_6=false;
  goto_6 = !(reg_0 < 0);
  if (!goto_6) {
    reg_0 = (reg_1 + reg_0);
  }
  if ((goto_6 || false)) {
    goto_6 = false;
    reg_0 = (reg_0 - 1);
  }
  if ((reg_0 < 0)) {
    reg_9 = 0;
    return reg_9;
  }
  return reg_0;
}
function valid_end_index (reg_0, reg_1) {
  var reg_9;
  var goto_6=false;
  goto_6 = !(reg_0 < 0);
  if (!goto_6) {
    reg_0 = (reg_1 + reg_0);
  }
  if ((goto_6 || false)) {
    goto_6 = false;
    reg_0 = (reg_0 - 1);
  }
  if ((reg_0 >= reg_1)) {
    reg_9 = (reg_1 - 1);
    return reg_9;
  }
  return reg_0;
}
function _strsubstr (reg_0) {
  var reg_4, reg_5, reg_10, reg_11, reg_12, reg_23;
  reg_4 = simple_string(next(reg_0), "1", "string.sub");
  reg_5 = reg_4.length;
  reg_10 = valid_start_index(simple_number(next(reg_0), "2", "string.sub"), reg_5);
  reg_11 = reg_5;
  reg_12 = next(reg_0);
  if (!testNil(reg_12)) {
    reg_11 = valid_end_index(simple_number(reg_12, "3", "string.sub"), reg_5);
  }
  reg_23 = stackof(anyStr(reg_4.slice(reg_10, (reg_11 + 1))));
  return reg_23;
}
function _strbyte (reg_0) {
  var reg_4, reg_5, reg_6, reg_7, reg_14, reg_15, reg_25, reg_27, reg_28, reg_29;
  reg_4 = simple_string(next(reg_0), "1", "string.byte");
  reg_5 = reg_4.length;
  reg_6 = 0;
  reg_7 = next(reg_0);
  if (!testNil(reg_7)) {
    reg_6 = valid_start_index(simple_number(reg_7, "2", "string.byte"), reg_5);
  }
  reg_14 = reg_6;
  reg_15 = next(reg_0);
  if (!testNil(reg_15)) {
    reg_14 = valid_end_index(simple_number(reg_15, "2", "string.byte"), reg_5);
  }
  if ((reg_14 >= reg_5)) {
    reg_14 = (reg_5 - 1);
  }
  reg_25 = newStack();
  loop_1: while ((reg_6 <= reg_14)) {
    var _r = Auro.charat(reg_4, reg_6); reg_28 = _r[0]; reg_29 = _r[1];
    reg_27 = reg_28;
    reg_6 = reg_29;
    push(reg_25, anyInt(reg_27.charCodeAt(0)));
  }
  return reg_25;
}
function _strchar (reg_0) {
  var reg_1, reg_2, reg_13;
  reg_1 = "";
  reg_2 = 1;
  loop_1: while (more(reg_0)) {
    reg_1 = (reg_1 + String.fromCharCode(simple_number(next(reg_0), String(reg_2), "string.char")));
    reg_2 = (reg_2 + 1);
  }
  reg_13 = stackof(anyStr(reg_1));
  return reg_13;
}
var cns71 = parseNum("1")
var cns72 = anyStr("type")
var cns73 = anyStr("number")
var cns74 = anyStr("tostring")
var cns75 = anyStr("type")
var cns76 = anyStr("string")
var cns77 = anyStr("error")
var cns78 = anyStr("bad argument #")
var cns79 = anyStr(" to 'string.len' (string expected, got ")
var cns80 = anyStr(")")
function lua_lib_string$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_11, reg_12, reg_14, reg_20, reg_21, reg_26, reg_27, reg_33, reg_36, reg_37, reg_38, reg_39, reg_46;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = reg_5;
  if (!tobool(reg_5)) {
    reg_6 = cns71;
  }
  reg_5 = reg_6;
  reg_11 = lua$get(reg_1.a, cns72);
  reg_12 = newStack();
  push(reg_12, reg_4);
  reg_14 = first(call(reg_11, reg_12));
  if (tobool(eq(reg_14, cns73))) {
    reg_20 = lua$get(reg_1.a, cns74);
    reg_21 = newStack();
    push(reg_21, reg_4);
    reg_4 = first(call(reg_20, reg_21));
  }
  reg_26 = lua$get(reg_1.a, cns75);
  reg_27 = newStack();
  push(reg_27, reg_4);
  if (tobool(eq(first(call(reg_26, reg_27)), cns76))) {
    reg_33 = newStack();
    push(reg_33, reg_4);
    return reg_33;
  }
  reg_36 = lua$get(reg_1.a, cns77);
  reg_37 = newStack();
  reg_38 = cns78;
  reg_39 = cns79;
  push(reg_37, concat(reg_38, concat(reg_5, concat(reg_39, concat(reg_14, cns80)))));
  reg_45 = call(reg_36, reg_37);
  reg_46 = newStack();
  return reg_46;
}
var cns81 = anyStr("string")
var cns82 = anyStr("len")
function string_len (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.b;
  reg_7 = newStack();
  push(reg_7, reg_4);
  push(reg_5, lua$length(first(call(reg_6, reg_7))));
  return reg_5;
}
var cns83 = anyStr("string")
var cns84 = anyStr("upper")
var cns85 = anyStr("")
var cns86 = parseNum("1")
var cns87 = parseNum("1")
var cns88 = parseNum("0")
var cns89 = anyStr("byte")
var cns90 = parseNum("97")
var cns91 = parseNum("122")
var cns92 = parseNum("32")
var cns93 = anyStr("string")
var cns94 = anyStr("char")
function string_upper (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_11, reg_12, reg_14, reg_21, reg_22, reg_24, reg_26, reg_37, reg_38, reg_43;
  var goto_21=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = reg_1.b;
  reg_6 = newStack();
  push(reg_6, reg_4);
  reg_4 = first(call(reg_5, reg_6));
  reg_9 = cns85;
  reg_10 = cns86;
  reg_11 = lua$length(reg_4);
  reg_12 = cns87;
  reg_14 = lt(reg_12, cns88);
  loop_1: while (true) {
    goto_21 = tobool(reg_14);
    if (!goto_21) {
      if (tobool(gt(reg_10, reg_11))) break loop_1;
    }
    if ((goto_21 || false)) {
      goto_21 = false;
      if (tobool(lt(reg_10, reg_11))) break loop_1;
    }
    reg_21 = lua$get(reg_4, cns89);
    reg_22 = newStack();
    push(reg_22, reg_4);
    push(reg_22, reg_10);
    reg_24 = first(call(reg_21, reg_22));
    reg_26 = ge(reg_24, cns90);
    if (tobool(reg_26)) {
      reg_26 = le(reg_24, cns91);
    }
    if (tobool(reg_26)) {
      reg_24 = sub(reg_24, cns92);
    }
    reg_37 = lua$get(lua$get(reg_1.a, cns93), cns94);
    reg_38 = newStack();
    push(reg_38, reg_24);
    reg_9 = concat(reg_9, first(call(reg_37, reg_38)));
    reg_10 = add(reg_10, reg_12);
  }
  reg_43 = newStack();
  push(reg_43, reg_9);
  return reg_43;
}
var cns95 = anyStr("string")
var cns96 = anyStr("lower")
var cns97 = anyStr("")
var cns98 = parseNum("1")
var cns99 = parseNum("1")
var cns100 = parseNum("0")
var cns101 = anyStr("byte")
var cns102 = parseNum("65")
var cns103 = parseNum("90")
var cns104 = parseNum("32")
var cns105 = anyStr("string")
var cns106 = anyStr("char")
function string_lower (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_11, reg_12, reg_14, reg_21, reg_22, reg_24, reg_26, reg_37, reg_38, reg_43;
  var goto_21=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = reg_1.b;
  reg_6 = newStack();
  push(reg_6, reg_4);
  reg_4 = first(call(reg_5, reg_6));
  reg_9 = cns97;
  reg_10 = cns98;
  reg_11 = lua$length(reg_4);
  reg_12 = cns99;
  reg_14 = lt(reg_12, cns100);
  loop_1: while (true) {
    goto_21 = tobool(reg_14);
    if (!goto_21) {
      if (tobool(gt(reg_10, reg_11))) break loop_1;
    }
    if ((goto_21 || false)) {
      goto_21 = false;
      if (tobool(lt(reg_10, reg_11))) break loop_1;
    }
    reg_21 = lua$get(reg_4, cns101);
    reg_22 = newStack();
    push(reg_22, reg_4);
    push(reg_22, reg_10);
    reg_24 = first(call(reg_21, reg_22));
    reg_26 = ge(reg_24, cns102);
    if (tobool(reg_26)) {
      reg_26 = le(reg_24, cns103);
    }
    if (tobool(reg_26)) {
      reg_24 = add(reg_24, cns104);
    }
    reg_37 = lua$get(lua$get(reg_1.a, cns105), cns106);
    reg_38 = newStack();
    push(reg_38, reg_24);
    reg_9 = concat(reg_9, first(call(reg_37, reg_38)));
    reg_10 = add(reg_10, reg_12);
  }
  reg_43 = newStack();
  push(reg_43, reg_9);
  return reg_43;
}
var cns107 = anyStr("string")
var cns108 = anyStr("reverse")
var cns109 = anyStr("")
var cns110 = parseNum("1")
var cns111 = parseNum("1")
var cns112 = parseNum("0")
var cns113 = anyStr("string")
var cns114 = anyStr("sub")
function string_reverse (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_11, reg_13, reg_15, reg_23, reg_25, reg_26, reg_31;
  var goto_22=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = reg_1.b;
  reg_6 = newStack();
  push(reg_6, reg_4);
  reg_4 = first(call(reg_5, reg_6));
  reg_9 = cns109;
  reg_10 = lua$length(reg_4);
  reg_11 = cns110;
  reg_13 = unm(cns111);
  reg_15 = lt(reg_13, cns112);
  loop_1: while (true) {
    goto_22 = tobool(reg_15);
    if (!goto_22) {
      if (tobool(gt(reg_10, reg_11))) break loop_1;
    }
    if ((goto_22 || false)) {
      goto_22 = false;
      if (tobool(lt(reg_10, reg_11))) break loop_1;
    }
    reg_23 = lua$get(reg_1.a, cns113);
    reg_25 = lua$get(reg_23, cns114);
    reg_26 = newStack();
    push(reg_26, reg_23);
    push(reg_26, reg_10);
    push(reg_26, reg_10);
    reg_9 = concat(reg_9, first(call(reg_25, reg_26)));
    reg_10 = add(reg_10, reg_13);
  }
  reg_31 = newStack();
  push(reg_31, reg_9);
  return reg_31;
}
var cns115 = anyStr("string")
var cns116 = anyStr("rep")
var cns117 = anyStr("")
var cns118 = parseNum("1")
var cns119 = anyStr("")
var cns120 = parseNum("2")
var cns121 = parseNum("1")
var cns122 = parseNum("0")
function string_rep (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_13, reg_15, reg_16, reg_17, reg_18, reg_20, reg_29;
  var goto_32=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = reg_6;
  if (!tobool(reg_6)) {
    reg_7 = cns117;
  }
  reg_6 = reg_7;
  if (tobool(lt(reg_5, cns118))) {
    reg_13 = newStack();
    push(reg_13, cns119);
    return reg_13;
  }
  reg_15 = reg_4;
  reg_16 = cns120;
  reg_17 = reg_5;
  reg_18 = cns121;
  reg_20 = lt(reg_18, cns122);
  loop_1: while (true) {
    goto_32 = tobool(reg_20);
    if (!goto_32) {
      if (tobool(gt(reg_16, reg_17))) break loop_1;
    }
    if ((goto_32 || false)) {
      goto_32 = false;
      if (tobool(lt(reg_16, reg_17))) break loop_1;
    }
    reg_15 = concat(reg_15, concat(reg_6, reg_4));
    reg_16 = add(reg_16, reg_18);
  }
  reg_29 = newStack();
  push(reg_29, reg_15);
  return reg_29;
}
var cns123 = anyStr("_G")
var cns124 = anyStr("utf8")
function newTable () {
  var reg_1;
  reg_1 = new record_18(emptyTable());
  return reg_1;
}
var cns125 = anyStr("utf8")
var cns126 = anyStr("char")
var cns127 = parseNum("1")
function copy (reg_0) {
  var reg_3;
  reg_3 = {a: reg_0.a, b: reg_0.b};
  return reg_3;
}
function table_append (reg_0, reg_1, reg_2) {
  var reg_3, reg_4;
  reg_3 = getTable(reg_0);
  reg_4 = getInt(reg_1);
  loop_1: while (more(reg_2)) {
    set(reg_3, anyInt(reg_4), next(reg_2));
    reg_4 = (reg_4 + 1);
  }
  return;
}
var cns128 = anyStr("ipairs")
var cns129 = anyStr("tonumber")
var cns130 = parseNum("127")
var cns131 = anyStr("error")
var cns132 = anyStr("Full unicode is not yet supported, only ASCII")
var cns133 = anyStr("string")
var cns134 = anyStr("char")
function append (reg_0, reg_1) {
  var reg_2;
  reg_2 = reg_1.a;
  loop_1: while ((reg_2 < reg_1.b.length)) {
    push(reg_0, reg_1.b[reg_2]);
    reg_2 = (reg_2 + 1);
  }
  return;
}
function utf8_char (reg_0, reg_1) {
  var reg_4, reg_9, reg_10, reg_11, reg_12, reg_13, reg_14, reg_15, reg_16, reg_18, reg_24, reg_25, reg_33, reg_34, reg_37, reg_42, reg_43;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newTable();
  table_append(reg_4, cns127, copy(reg_0));
  reg_9 = lua$get(reg_1.a, cns128);
  reg_10 = newStack();
  push(reg_10, reg_4);
  reg_11 = call(reg_9, reg_10);
  reg_12 = next(reg_11);
  reg_13 = next(reg_11);
  reg_14 = next(reg_11);
  loop_1: while (true) {
    reg_15 = newStack();
    push(reg_15, reg_13);
    push(reg_15, reg_14);
    reg_16 = call(reg_12, reg_15);
    reg_14 = next(reg_16);
    reg_18 = next(reg_16);
    if (tobool(eq(reg_14, nil()))) break loop_1;
    reg_24 = lua$get(reg_1.a, cns129);
    reg_25 = newStack();
    push(reg_25, reg_18);
    if (tobool(gt(first(call(reg_24, reg_25)), cns130))) {
      reg_33 = lua$get(reg_1.a, cns131);
      reg_34 = newStack();
      push(reg_34, cns132);
      reg_36 = call(reg_33, reg_34);
    }
  }
  reg_37 = newStack();
  reg_42 = lua$get(lua$get(reg_1.a, cns133), cns134);
  reg_43 = newStack();
  append(reg_43, reg_0);
  append(reg_37, call(reg_42, reg_43));
  return reg_37;
}
function lua_lib_string$lua_main (reg_0) {
  var reg_1, reg_2, reg_7, reg_8, reg_13, reg_14, reg_19, reg_20, reg_25, reg_26, reg_31, reg_32, reg_42, reg_43, reg_46;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1};
  reg_2.a = reg_0;
  reg_2.b = anyFn((function (a) { return lua_lib_string$function(a,this) }).bind(reg_2));
  reg_7 = lua$get(reg_2.a, cns81);
  reg_8 = cns82;
  lua$set(reg_7, reg_8, anyFn((function (a) { return string_len(a,this) }).bind(reg_2)));
  reg_13 = lua$get(reg_2.a, cns83);
  reg_14 = cns84;
  lua$set(reg_13, reg_14, anyFn((function (a) { return string_upper(a,this) }).bind(reg_2)));
  reg_19 = lua$get(reg_2.a, cns95);
  reg_20 = cns96;
  lua$set(reg_19, reg_20, anyFn((function (a) { return string_lower(a,this) }).bind(reg_2)));
  reg_25 = lua$get(reg_2.a, cns107);
  reg_26 = cns108;
  lua$set(reg_25, reg_26, anyFn((function (a) { return string_reverse(a,this) }).bind(reg_2)));
  reg_31 = lua$get(reg_2.a, cns115);
  reg_32 = cns116;
  lua$set(reg_31, reg_32, anyFn((function (a) { return string_rep(a,this) }).bind(reg_2)));
  lua$set(lua$get(reg_2.a, cns123), cns124, newTable());
  reg_42 = lua$get(reg_2.a, cns125);
  reg_43 = cns126;
  lua$set(reg_42, reg_43, anyFn((function (a) { return utf8_char(a,this) }).bind(reg_2)));
  reg_46 = newStack();
  return reg_46;
}
var cns135 = anyStr("string")
var cns136 = anyStr("charat")
var cns137 = anyStr("sub")
function string_charat (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_8, reg_9;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_8 = lua$get(reg_4, cns137);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, reg_5);
  push(reg_9, reg_5);
  append(reg_6, call(reg_8, reg_9));
  return reg_6;
}
var cns138 = parseNum("48")
var cns139 = parseNum("57")
function lua_lib_pattern$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = ge(reg_4, cns138);
  if (tobool(reg_7)) {
    reg_7 = le(reg_4, cns139);
  }
  push(reg_5, reg_7);
  return reg_5;
}
var cns140 = parseNum("97")
var cns141 = parseNum("122")
function function$6 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = ge(reg_4, cns140);
  if (tobool(reg_7)) {
    reg_7 = le(reg_4, cns141);
  }
  push(reg_5, reg_7);
  return reg_5;
}
var cns142 = parseNum("65")
var cns143 = parseNum("90")
function function$7 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = ge(reg_4, cns142);
  if (tobool(reg_7)) {
    reg_7 = le(reg_4, cns143);
  }
  push(reg_5, reg_7);
  return reg_5;
}
function function$8 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_9, reg_11, reg_12;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.a;
  reg_7 = newStack();
  push(reg_7, reg_4);
  reg_9 = first(call(reg_6, reg_7));
  if (!tobool(reg_9)) {
    reg_11 = reg_1.b;
    reg_12 = newStack();
    push(reg_12, reg_4);
    reg_9 = first(call(reg_11, reg_12));
  }
  push(reg_5, reg_9);
  return reg_5;
}
var cns144 = parseNum("9")
var cns145 = parseNum("10")
var cns146 = parseNum("32")
function function$9 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = eq(reg_4, cns144);
  if (!tobool(reg_7)) {
    reg_7 = eq(reg_4, cns145);
  }
  if (!tobool(reg_7)) {
    reg_7 = eq(reg_4, cns146);
  }
  push(reg_5, reg_7);
  return reg_5;
}
var cns147 = parseNum("32")
var cns148 = parseNum("127")
function function$10 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = lt(reg_4, cns147);
  if (!tobool(reg_7)) {
    reg_7 = eq(reg_4, cns148);
  }
  push(reg_5, reg_7);
  return reg_5;
}
function function$11 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_12, reg_13;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.c;
  reg_7 = newStack();
  push(reg_7, reg_4);
  reg_10 = not(first(call(reg_6, reg_7)));
  if (tobool(reg_10)) {
    reg_12 = reg_1.d;
    reg_13 = newStack();
    push(reg_13, reg_4);
    reg_10 = not(first(call(reg_12, reg_13)));
  }
  push(reg_5, reg_10);
  return reg_5;
}
function function$12 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_9, reg_11, reg_12;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.e;
  reg_7 = newStack();
  push(reg_7, reg_4);
  reg_9 = first(call(reg_6, reg_7));
  if (!tobool(reg_9)) {
    reg_11 = reg_1.f;
    reg_12 = newStack();
    push(reg_12, reg_4);
    reg_9 = first(call(reg_11, reg_12));
  }
  push(reg_5, reg_9);
  return reg_5;
}
function function$13 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_9, reg_11, reg_12;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.g;
  reg_7 = newStack();
  push(reg_7, reg_4);
  reg_9 = first(call(reg_6, reg_7));
  if (tobool(reg_9)) {
    reg_11 = reg_1.h;
    reg_12 = newStack();
    push(reg_12, reg_4);
    reg_9 = not(first(call(reg_11, reg_12)));
  }
  push(reg_5, reg_9);
  return reg_5;
}
var cns149 = parseNum("65")
var cns150 = parseNum("70")
var cns151 = parseNum("97")
var cns152 = parseNum("102")
function function$14 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_9, reg_12, reg_18;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = reg_1.f;
  reg_7 = newStack();
  push(reg_7, reg_4);
  reg_9 = first(call(reg_6, reg_7));
  if (!tobool(reg_9)) {
    reg_12 = ge(reg_4, cns149);
    if (tobool(reg_12)) {
      reg_12 = le(reg_4, cns150);
    }
    reg_9 = reg_12;
  }
  if (!tobool(reg_9)) {
    reg_18 = ge(reg_4, cns151);
    if (tobool(reg_18)) {
      reg_18 = le(reg_4, cns152);
    }
    reg_9 = reg_18;
  }
  push(reg_5, reg_9);
  return reg_5;
}
function lua$true () {
  var reg_1;
  reg_1 = true;
  return reg_1;
}
function function$15 (reg_0, reg_1) {
  var reg_4;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newStack();
  push(reg_4, lua$true());
  return reg_4;
}
function function$17 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_8;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = reg_1.b;
  reg_8 = newStack();
  push(reg_8, reg_5);
  push(reg_6, not(first(call(reg_7, reg_8))));
  return reg_6;
}
function function$16 (reg_0, reg_1) {
  var reg_3, reg_5;
  reg_3 = {a: reg_1, b: nil()};
  reg_3.b = next(reg_0);
  reg_5 = newStack();
  push(reg_5, anyFn((function (a) { return function$17(a,this) }).bind(reg_3)));
  return reg_5;
}
var cns153 = anyStr("byte")
var cns154 = anyStr("byte")
function function$19 (reg_0, reg_1) {
  var reg_5, reg_6, reg_8;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_8 = ge(reg_5, reg_1.b);
  if (tobool(reg_8)) {
    reg_8 = le(reg_5, reg_1.c);
  }
  push(reg_6, reg_8);
  return reg_6;
}
function function$18 (reg_0, reg_1) {
  var reg_2, reg_3, reg_4, reg_5, reg_7, reg_8, reg_12, reg_13, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = lua$get(reg_4, cns153);
  reg_8 = newStack();
  push(reg_8, reg_4);
  reg_3.b = first(call(reg_7, reg_8));
  reg_12 = lua$get(reg_5, cns154);
  reg_13 = newStack();
  push(reg_13, reg_5);
  reg_3.c = first(call(reg_12, reg_13));
  reg_16 = newStack();
  push(reg_16, anyFn((function (a) { return function$19(a,this) }).bind(reg_3)));
  return reg_16;
}
var cns155 = parseNum("1")
var cns156 = anyStr("byte")
var cns157 = parseNum("1")
var cns158 = anyStr("ipairs")
function lua$false () {
  var reg_1;
  reg_1 = false;
  return reg_1;
}
function function$21 (reg_0, reg_1) {
  var reg_2, reg_5, reg_8, reg_9, reg_11, reg_12, reg_13, reg_14, reg_15, reg_16, reg_18, reg_24, reg_26;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_2.i, cns158);
  reg_9 = newStack();
  push(reg_9, reg_1.b);
  reg_11 = call(reg_8, reg_9);
  reg_12 = next(reg_11);
  reg_13 = next(reg_11);
  reg_14 = next(reg_11);
  loop_1: while (true) {
    reg_15 = newStack();
    push(reg_15, reg_13);
    push(reg_15, reg_14);
    reg_16 = call(reg_12, reg_15);
    reg_14 = next(reg_16);
    reg_18 = next(reg_16);
    if (tobool(eq(reg_14, nil()))) break loop_1;
    if (tobool(eq(reg_5, reg_18))) {
      reg_24 = newStack();
      push(reg_24, lua$true());
      return reg_24;
    }
  }
  reg_26 = newStack();
  push(reg_26, lua$false());
  return reg_26;
}
function function$20 (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_6, reg_8, reg_9, reg_13;
  reg_3 = {a: reg_1, b: nil()};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  reg_6 = cns155;
  reg_8 = lua$get(reg_4, cns156);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, cns157);
  push(reg_9, lua$length(reg_4));
  table_append(reg_5, reg_6, call(reg_8, reg_9));
  reg_3.b = reg_5;
  reg_13 = newStack();
  push(reg_13, anyFn((function (a) { return function$21(a,this) }).bind(reg_3)));
  return reg_13;
}
var cns159 = anyStr("charat")
var cns160 = anyStr("%")
var cns161 = anyStr("charat")
var cns162 = parseNum("1")
var cns163 = anyStr("lower")
var cns164 = anyStr("a")
var cns165 = anyStr("c")
var cns166 = anyStr("d")
var cns167 = anyStr("g")
var cns168 = anyStr("l")
var cns169 = anyStr("p")
var cns170 = anyStr("s")
var cns171 = anyStr("u")
var cns172 = anyStr("w")
var cns173 = anyStr("x")
var cns174 = parseNum("2")
var cns175 = parseNum("2")
function function$22 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_14, reg_15, reg_17, reg_18, reg_22, reg_24, reg_25, reg_27, reg_71, reg_72, reg_75, reg_78, reg_79, reg_80, reg_85;
  var goto_41=false, goto_48=false, goto_55=false, goto_62=false, goto_69=false, goto_76=false, goto_83=false, goto_90=false, goto_97=false, goto_123=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = lua$get(reg_4, cns159);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, reg_5);
  if (tobool(ne(first(call(reg_7, reg_8)), cns160))) {
    reg_14 = newStack();
    return reg_14;
  }
  reg_15 = nil();
  reg_17 = lua$get(reg_4, cns161);
  reg_18 = newStack();
  push(reg_18, reg_4);
  push(reg_18, add(reg_5, cns162));
  reg_22 = first(call(reg_17, reg_18));
  reg_24 = lua$get(reg_22, cns163);
  reg_25 = newStack();
  push(reg_25, reg_22);
  reg_27 = first(call(reg_24, reg_25));
  goto_41 = !tobool(eq(reg_27, cns164));
  if (!goto_41) {
    reg_15 = reg_1.e;
  }
  if ((goto_41 || false)) {
    goto_41 = false;
    goto_48 = !tobool(eq(reg_27, cns165));
    if (!goto_48) {
      reg_15 = reg_1.c;
    }
    if ((goto_48 || false)) {
      goto_48 = false;
      goto_55 = !tobool(eq(reg_27, cns166));
      if (!goto_55) {
        reg_15 = reg_1.f;
      }
      if ((goto_55 || false)) {
        goto_55 = false;
        goto_62 = !tobool(eq(reg_27, cns167));
        if (!goto_62) {
          reg_15 = reg_1.g;
        }
        if ((goto_62 || false)) {
          goto_62 = false;
          goto_69 = !tobool(eq(reg_27, cns168));
          if (!goto_69) {
            reg_15 = reg_1.a;
          }
          if ((goto_69 || false)) {
            goto_69 = false;
            goto_76 = !tobool(eq(reg_27, cns169));
            if (!goto_76) {
              reg_15 = reg_1.j;
            }
            if ((goto_76 || false)) {
              goto_76 = false;
              goto_83 = !tobool(eq(reg_27, cns170));
              if (!goto_83) {
                reg_15 = reg_1.d;
              }
              if ((goto_83 || false)) {
                goto_83 = false;
                goto_90 = !tobool(eq(reg_27, cns171));
                if (!goto_90) {
                  reg_15 = reg_1.b;
                }
                if ((goto_90 || false)) {
                  goto_90 = false;
                  goto_97 = !tobool(eq(reg_27, cns172));
                  if (!goto_97) {
                    reg_15 = reg_1.h;
                  }
                  if ((goto_97 || false)) {
                    goto_97 = false;
                    if (tobool(eq(reg_27, cns173))) {
                      reg_15 = reg_1.k;
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  goto_123 = !tobool(reg_15);
  if (!goto_123) {
    if (tobool(ne(reg_27, reg_22))) {
      reg_71 = reg_1.l;
      reg_72 = newStack();
      push(reg_72, reg_15);
      reg_15 = first(call(reg_71, reg_72));
    }
    reg_75 = newStack();
    push(reg_75, reg_15);
    push(reg_75, add(reg_5, cns174));
    return reg_75;
  }
  if ((goto_123 || false)) {
    goto_123 = false;
    reg_78 = newStack();
    reg_79 = reg_1.m;
    reg_80 = newStack();
    push(reg_80, reg_22);
    push(reg_78, first(call(reg_79, reg_80)));
    push(reg_78, add(reg_5, cns175));
    return reg_78;
  }
  reg_85 = newStack();
  return reg_85;
}
var cns176 = anyStr("charat")
var cns177 = anyStr("^")
var cns178 = parseNum("1")
var cns179 = anyStr("")
var cns180 = anyStr("]")
var cns181 = anyStr("charat")
var cns182 = parseNum("1")
var cns183 = anyStr("-")
var cns184 = anyStr("charat")
var cns185 = parseNum("2")
var cns186 = parseNum("2")
var cns187 = parseNum("1")
var cns188 = anyStr("table")
var cns189 = anyStr("insert")
var cns190 = anyStr("sub")
var cns191 = anyStr("")
var cns192 = anyStr("table")
var cns193 = anyStr("insert")
var cns194 = anyStr("]")
var cns195 = anyStr("ipairs")
function f (reg_0, reg_1) {
  var reg_2, reg_5, reg_8, reg_9, reg_11, reg_12, reg_13, reg_14, reg_15, reg_16, reg_18, reg_22, reg_26, reg_28;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_2.i, cns195);
  reg_9 = newStack();
  push(reg_9, reg_1.b);
  reg_11 = call(reg_8, reg_9);
  reg_12 = next(reg_11);
  reg_13 = next(reg_11);
  reg_14 = next(reg_11);
  loop_1: while (true) {
    reg_15 = newStack();
    push(reg_15, reg_13);
    push(reg_15, reg_14);
    reg_16 = call(reg_12, reg_15);
    reg_14 = next(reg_16);
    reg_18 = next(reg_16);
    if (tobool(eq(reg_14, nil()))) break loop_1;
    reg_22 = newStack();
    push(reg_22, reg_5);
    if (tobool(first(call(reg_18, reg_22)))) {
      reg_26 = newStack();
      push(reg_26, lua$true());
      return reg_26;
    }
  }
  reg_28 = newStack();
  push(reg_28, lua$false());
  return reg_28;
}
var cns196 = parseNum("1")
function function$23 (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_7, reg_8, reg_10, reg_14, reg_15, reg_18, reg_19, reg_20, reg_21, reg_22, reg_23, reg_26, reg_28, reg_33, reg_34, reg_36, reg_37, reg_38, reg_41, reg_42, reg_50, reg_51, reg_53, reg_54, reg_70, reg_71, reg_75, reg_76, reg_86, reg_87, reg_89, reg_90, reg_97, reg_98, reg_101;
  var goto_58=false, goto_90=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = lua$get(reg_4, cns176);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, reg_5);
  reg_10 = first(call(reg_7, reg_8));
  if (tobool(eq(reg_10, cns177))) {
    reg_14 = reg_1.n;
    reg_15 = newStack();
    push(reg_15, reg_4);
    push(reg_15, add(reg_5, cns178));
    reg_18 = call(reg_14, reg_15);
    reg_19 = next(reg_18);
    reg_20 = next(reg_18);
    reg_21 = newStack();
    reg_22 = reg_1.l;
    reg_23 = newStack();
    push(reg_23, reg_19);
    push(reg_21, first(call(reg_22, reg_23)));
    push(reg_21, reg_20);
    return reg_21;
  }
  reg_26 = cns179;
  reg_3.b = newTable();
  loop_1: while (true) {
    reg_28 = reg_10;
    if (tobool(reg_10)) {
      reg_28 = ne(reg_10, cns180);
    }
    if (!tobool(reg_28)) break loop_1;
    reg_33 = reg_1.o;
    reg_34 = newStack();
    push(reg_34, reg_4);
    push(reg_34, reg_5);
    push(reg_34, lua$true());
    reg_36 = call(reg_33, reg_34);
    reg_37 = next(reg_36);
    reg_38 = next(reg_36);
    goto_58 = !tobool(reg_37);
    if (!goto_58) {
      reg_5 = reg_38;
    }
    if ((goto_58 || false)) {
      goto_58 = false;
      reg_41 = lua$get(reg_4, cns181);
      reg_42 = newStack();
      push(reg_42, reg_4);
      push(reg_42, add(reg_5, cns182));
      goto_90 = !tobool(eq(first(call(reg_41, reg_42)), cns183));
      if (!goto_90) {
        reg_50 = reg_1.p;
        reg_51 = newStack();
        push(reg_51, reg_10);
        reg_53 = lua$get(reg_4, cns184);
        reg_54 = newStack();
        push(reg_54, reg_4);
        push(reg_54, add(reg_5, cns185));
        append(reg_51, call(reg_53, reg_54));
        reg_37 = first(call(reg_50, reg_51));
        reg_5 = add(reg_5, cns186);
      }
      if ((goto_90 || false)) {
        goto_90 = false;
        reg_26 = concat(reg_26, reg_10);
        reg_5 = add(reg_5, cns187);
      }
    }
    if (tobool(reg_37)) {
      reg_70 = lua$get(lua$get(reg_1.i, cns188), cns189);
      reg_71 = newStack();
      push(reg_71, reg_3.b);
      push(reg_71, reg_37);
      reg_73 = call(reg_70, reg_71);
    }
    reg_75 = lua$get(reg_4, cns190);
    reg_76 = newStack();
    push(reg_76, reg_4);
    push(reg_76, reg_5);
    push(reg_76, reg_5);
    reg_10 = first(call(reg_75, reg_76));
  }
  if (tobool(ne(reg_26, cns191))) {
    reg_86 = lua$get(lua$get(reg_1.i, cns192), cns193);
    reg_87 = newStack();
    push(reg_87, reg_3.b);
    reg_89 = reg_1.m;
    reg_90 = newStack();
    push(reg_90, reg_26);
    append(reg_87, call(reg_89, reg_90));
    reg_92 = call(reg_86, reg_87);
  }
  if (tobool(eq(reg_10, cns194))) {
    reg_97 = anyFn((function (a) { return f(a,this) }).bind(reg_3));
    reg_98 = newStack();
    push(reg_98, reg_97);
    push(reg_98, add(reg_5, cns196));
    return reg_98;
  }
  reg_101 = newStack();
  return reg_101;
}
var cns197 = anyStr("charat")
var cns198 = anyStr("[")
var cns199 = parseNum("1")
var cns200 = anyStr(".")
var cns201 = parseNum("1")
function function$24 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_10, reg_14, reg_15, reg_16, reg_23, reg_27, reg_28, reg_29, reg_31;
  var goto_26=false, goto_38=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = lua$get(reg_4, cns197);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, reg_5);
  reg_10 = first(call(reg_7, reg_8));
  goto_26 = !tobool(eq(reg_10, cns198));
  if (!goto_26) {
    reg_14 = newStack();
    reg_15 = reg_1.n;
    reg_16 = newStack();
    push(reg_16, reg_4);
    push(reg_16, add(reg_5, cns199));
    append(reg_14, call(reg_15, reg_16));
    return reg_14;
  }
  if ((goto_26 || false)) {
    goto_26 = false;
    goto_38 = !tobool(eq(reg_10, cns200));
    if (!goto_38) {
      reg_23 = newStack();
      push(reg_23, reg_1.q);
      push(reg_23, add(reg_5, cns201));
      return reg_23;
    }
    if ((goto_38 || false)) {
      goto_38 = false;
      reg_27 = newStack();
      reg_28 = reg_1.o;
      reg_29 = newStack();
      push(reg_29, reg_4);
      push(reg_29, reg_5);
      append(reg_27, call(reg_28, reg_29));
      return reg_27;
    }
  }
  reg_31 = newStack();
  return reg_31;
}
var cns202 = anyStr("sub")
var cns203 = parseNum("1")
function function$26 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_9, reg_10, reg_21, reg_22, reg_23, reg_28;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_9 = lua$get(reg_5, cns202);
  reg_10 = newStack();
  push(reg_10, reg_5);
  push(reg_10, reg_6);
  push(reg_10, sub(add(reg_6, lua$length(reg_1.b)), cns203));
  if (tobool(eq(first(call(reg_9, reg_10)), reg_1.b))) {
    reg_21 = newStack();
    reg_22 = reg_1.c;
    reg_23 = newStack();
    push(reg_23, reg_5);
    push(reg_23, add(reg_6, lua$length(reg_1.b)));
    push(reg_23, reg_7);
    append(reg_21, call(reg_22, reg_23));
    return reg_21;
  }
  reg_28 = newStack();
  return reg_28;
}
function function$25 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$26(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns204 = anyStr("byte")
var cns205 = parseNum("1")
function function$28 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_11, reg_12, reg_13, reg_15, reg_16, reg_21, reg_22, reg_23, reg_27;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  if (tobool(gt(reg_6, lua$length(reg_5)))) {
    reg_11 = newStack();
    return reg_11;
  }
  reg_12 = reg_1.b;
  reg_13 = newStack();
  reg_15 = lua$get(reg_5, cns204);
  reg_16 = newStack();
  push(reg_16, reg_5);
  push(reg_16, reg_6);
  append(reg_13, call(reg_15, reg_16));
  if (tobool(first(call(reg_12, reg_13)))) {
    reg_21 = newStack();
    reg_22 = reg_1.c;
    reg_23 = newStack();
    push(reg_23, reg_5);
    push(reg_23, add(reg_6, cns205));
    push(reg_23, reg_7);
    append(reg_21, call(reg_22, reg_23));
    return reg_21;
  }
  reg_27 = newStack();
  return reg_27;
}
function function$27 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$28(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns206 = parseNum("1")
function function$29 (reg_0, reg_1) {
  var reg_5, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = newStack();
  push(reg_6, sub(reg_5, cns206));
  return reg_6;
}
var cns207 = anyStr("byte")
var cns208 = parseNum("1")
var cns209 = parseNum("1")
function function$31 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_8, reg_10, reg_12, reg_13, reg_15, reg_16, reg_25, reg_26, reg_28, reg_30, reg_33;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = reg_6;
  loop_2: while (true) {
    reg_10 = le(reg_8, lua$length(reg_5));
    if (tobool(reg_10)) {
      reg_12 = reg_1.b;
      reg_13 = newStack();
      reg_15 = lua$get(reg_5, cns207);
      reg_16 = newStack();
      push(reg_16, reg_5);
      push(reg_16, reg_8);
      append(reg_13, call(reg_15, reg_16));
      reg_10 = first(call(reg_12, reg_13));
    }
    if (!tobool(reg_10)) break loop_2;
    reg_8 = add(reg_8, cns208);
  }
  loop_1: while (tobool(ge(reg_8, reg_6))) {
    reg_25 = reg_1.c;
    reg_26 = newStack();
    push(reg_26, reg_5);
    push(reg_26, reg_8);
    push(reg_26, reg_7);
    reg_28 = first(call(reg_25, reg_26));
    if (tobool(reg_28)) {
      reg_30 = newStack();
      push(reg_30, reg_28);
      return reg_30;
    }
    reg_8 = sub(reg_8, cns209);
  }
  reg_33 = newStack();
  return reg_33;
}
function function$30 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$31(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns210 = anyStr("byte")
var cns211 = parseNum("1")
function function$33 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_10, reg_11, reg_13, reg_15, reg_17, reg_19, reg_20, reg_22, reg_23, reg_30, reg_31;
  var goto_44=false;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  loop_1: while (tobool(lua$true())) {
    reg_10 = reg_1.b;
    reg_11 = newStack();
    push(reg_11, reg_5);
    push(reg_11, reg_6);
    push(reg_11, reg_7);
    reg_13 = first(call(reg_10, reg_11));
    if (tobool(reg_13)) {
      reg_15 = newStack();
      push(reg_15, reg_13);
      return reg_15;
    }
    reg_17 = le(reg_6, lua$length(reg_5));
    if (tobool(reg_17)) {
      reg_19 = reg_1.c;
      reg_20 = newStack();
      reg_22 = lua$get(reg_5, cns210);
      reg_23 = newStack();
      push(reg_23, reg_5);
      push(reg_23, reg_6);
      append(reg_20, call(reg_22, reg_23));
      reg_17 = first(call(reg_19, reg_20));
    }
    goto_44 = !tobool(reg_17);
    if (!goto_44) {
      reg_6 = add(reg_6, cns211);
    }
    if ((goto_44 || false)) {
      goto_44 = false;
      reg_30 = newStack();
      return reg_30;
    }
  }
  reg_31 = newStack();
  return reg_31;
}
function function$32 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.c = next(reg_0);
  reg_3.b = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$33(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns212 = anyStr("byte")
var cns213 = parseNum("1")
function function$35 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_8, reg_9, reg_11, reg_12, reg_17, reg_18, reg_22, reg_24, reg_25, reg_26, reg_27;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = reg_1.b;
  reg_9 = newStack();
  reg_11 = lua$get(reg_5, cns212);
  reg_12 = newStack();
  push(reg_12, reg_5);
  push(reg_12, reg_6);
  append(reg_9, call(reg_11, reg_12));
  if (tobool(first(call(reg_8, reg_9)))) {
    reg_17 = reg_1.c;
    reg_18 = newStack();
    push(reg_18, reg_5);
    push(reg_18, add(reg_6, cns213));
    push(reg_18, reg_7);
    reg_22 = first(call(reg_17, reg_18));
    if (tobool(reg_22)) {
      reg_24 = newStack();
      push(reg_24, reg_22);
      return reg_24;
    }
  }
  reg_25 = newStack();
  reg_26 = reg_1.c;
  reg_27 = newStack();
  push(reg_27, reg_5);
  push(reg_27, reg_6);
  push(reg_27, reg_7);
  append(reg_25, call(reg_26, reg_27));
  return reg_25;
}
function function$34 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$35(a,this) }).bind(reg_3)));
  return reg_6;
}
function function$37 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_9, reg_10, reg_11;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  lua$set(reg_7, reg_1.b, reg_6);
  reg_9 = newStack();
  reg_10 = reg_1.c;
  reg_11 = newStack();
  push(reg_11, reg_5);
  push(reg_11, reg_6);
  push(reg_11, reg_7);
  append(reg_9, call(reg_10, reg_11));
  return reg_9;
}
function function$36 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$37(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns214 = anyStr("start")
function function$39 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_8, reg_9, reg_11, reg_12, reg_13;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = reg_1.b;
  reg_9 = newTable();
  lua$set(reg_9, cns214, reg_6);
  lua$set(reg_7, reg_8, reg_9);
  reg_11 = newStack();
  reg_12 = reg_1.c;
  reg_13 = newStack();
  push(reg_13, reg_5);
  push(reg_13, reg_6);
  push(reg_13, reg_7);
  append(reg_11, call(reg_12, reg_13));
  return reg_11;
}
function function$38 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$39(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns215 = anyStr("end")
var cns216 = parseNum("1")
function function$41 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_9, reg_10, reg_13, reg_14, reg_15;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_9 = lua$get(reg_7, reg_1.b);
  reg_10 = cns215;
  lua$set(reg_9, reg_10, sub(reg_6, cns216));
  reg_13 = newStack();
  reg_14 = reg_1.c;
  reg_15 = newStack();
  push(reg_15, reg_5);
  push(reg_15, reg_6);
  push(reg_15, reg_7);
  append(reg_13, call(reg_14, reg_15));
  return reg_13;
}
function function$40 (reg_0, reg_1) {
  var reg_2, reg_3, reg_6;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newStack();
  push(reg_6, anyFn((function (a) { return function$41(a,this) }).bind(reg_3)));
  return reg_6;
}
var cns217 = anyStr("table")
var cns218 = anyStr("insert")
var cns219 = anyStr("type")
var cns220 = anyStr("value")
function function$43 (reg_0, reg_1) {
  var reg_2, reg_5, reg_6, reg_11, reg_12, reg_14, reg_18;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_11 = lua$get(lua$get(reg_2.i, cns217), cns218);
  reg_12 = newStack();
  push(reg_12, reg_1.b);
  reg_14 = newTable();
  lua$set(reg_14, cns219, reg_5);
  lua$set(reg_14, cns220, reg_6);
  push(reg_12, reg_14);
  reg_17 = call(reg_11, reg_12);
  reg_18 = newStack();
  return reg_18;
}
var cns221 = parseNum("1")
var cns222 = anyStr("")
var cns223 = anyStr("sub")
var cns224 = parseNum("1")
var cns225 = anyStr("()")
var cns226 = anyStr("()")
var cns227 = parseNum("1")
var cns228 = parseNum("2")
var cns229 = anyStr("charat")
var cns230 = anyStr("(")
var cns231 = anyStr("(")
var cns232 = anyStr("table")
var cns233 = anyStr("insert")
var cns234 = parseNum("1")
var cns235 = parseNum("1")
var cns236 = anyStr("charat")
var cns237 = anyStr(")")
var cns238 = anyStr(")")
var cns239 = anyStr("table")
var cns240 = anyStr("remove")
var cns241 = parseNum("1")
var cns242 = anyStr("")
var cns243 = anyStr("string")
var cns244 = anyStr("")
var cns245 = anyStr("charat")
var cns246 = parseNum("1")
var cns247 = anyStr("charat")
var cns248 = anyStr("+")
var cns249 = anyStr("*")
var cns250 = anyStr("-")
var cns251 = anyStr("?")
var cns252 = anyStr("type")
var cns253 = anyStr("string")
var cns254 = parseNum("1")
var cns255 = anyStr("type")
var cns256 = anyStr("string")
var cns257 = anyStr("class")
var cns258 = anyStr("")
var cns259 = anyStr("string")
var cns260 = parseNum("1")
var cns261 = parseNum("1")
var cns262 = parseNum("0")
var cns263 = anyStr("type")
var cns264 = anyStr("string")
var cns265 = anyStr("value")
var cns266 = anyStr("type")
var cns267 = anyStr("class")
var cns268 = anyStr("value")
var cns269 = anyStr("type")
var cns270 = anyStr("*")
var cns271 = anyStr("value")
var cns272 = anyStr("type")
var cns273 = anyStr("+")
var cns274 = anyStr("value")
var cns275 = anyStr("value")
var cns276 = anyStr("type")
var cns277 = anyStr("-")
var cns278 = anyStr("value")
var cns279 = anyStr("type")
var cns280 = anyStr("?")
var cns281 = anyStr("value")
var cns282 = anyStr("type")
var cns283 = anyStr("()")
var cns284 = anyStr("value")
var cns285 = anyStr("type")
var cns286 = anyStr("(")
var cns287 = anyStr("value")
var cns288 = anyStr("type")
var cns289 = anyStr(")")
var cns290 = anyStr("value")
function function$42 (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_8, reg_9, reg_10, reg_11, reg_16, reg_17, reg_25, reg_33, reg_34, reg_40, reg_47, reg_48, reg_55, reg_56, reg_62, reg_68, reg_69, reg_74, reg_75, reg_76, reg_77, reg_78, reg_83, reg_88, reg_89, reg_95, reg_96, reg_98, reg_100, reg_113, reg_114, reg_120, reg_121, reg_124, reg_130, reg_131, reg_138, reg_144, reg_148, reg_150, reg_151, reg_153, reg_155, reg_162, reg_168, reg_169, reg_179, reg_180, reg_190, reg_191, reg_201, reg_202, reg_207, reg_208, reg_218, reg_219, reg_229, reg_230, reg_240, reg_241, reg_251, reg_252, reg_262, reg_263, reg_269;
  var goto_41=false, goto_73=false, goto_101=false, goto_124=false, goto_187=false, goto_201=false, goto_232=false, goto_253=false, goto_269=false, goto_285=false, goto_310=false, goto_326=false, goto_342=false, goto_358=false, goto_374=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_3.b = newTable();
  reg_8 = anyFn((function (a) { return function$43(a,this) }).bind(reg_3));
  reg_9 = cns221;
  reg_10 = newTable();
  reg_11 = cns222;
  loop_2: while (tobool(le(reg_5, lua$length(reg_4)))) {
    reg_16 = lua$get(reg_4, cns223);
    reg_17 = newStack();
    push(reg_17, reg_4);
    push(reg_17, reg_5);
    push(reg_17, add(reg_5, cns224));
    goto_41 = !tobool(eq(first(call(reg_16, reg_17)), cns225));
    if (!goto_41) {
      reg_25 = newStack();
      push(reg_25, cns226);
      push(reg_25, reg_9);
      reg_27 = call(reg_8, reg_25);
      reg_9 = add(reg_9, cns227);
      reg_5 = add(reg_5, cns228);
    }
    if ((goto_41 || false)) {
      goto_41 = false;
      reg_33 = lua$get(reg_4, cns229);
      reg_34 = newStack();
      push(reg_34, reg_4);
      push(reg_34, reg_5);
      goto_73 = !tobool(eq(first(call(reg_33, reg_34)), cns230));
      if (!goto_73) {
        reg_40 = newStack();
        push(reg_40, cns231);
        push(reg_40, reg_9);
        reg_42 = call(reg_8, reg_40);
        reg_47 = lua$get(lua$get(reg_1.i, cns232), cns233);
        reg_48 = newStack();
        push(reg_48, reg_10);
        push(reg_48, reg_9);
        reg_49 = call(reg_47, reg_48);
        reg_9 = add(reg_9, cns234);
        reg_5 = add(reg_5, cns235);
      }
      if ((goto_73 || false)) {
        goto_73 = false;
        reg_55 = lua$get(reg_4, cns236);
        reg_56 = newStack();
        push(reg_56, reg_4);
        push(reg_56, reg_5);
        goto_101 = !tobool(eq(first(call(reg_55, reg_56)), cns237));
        if (!goto_101) {
          reg_62 = newStack();
          push(reg_62, cns238);
          reg_68 = lua$get(lua$get(reg_1.i, cns239), cns240);
          reg_69 = newStack();
          push(reg_69, reg_10);
          append(reg_62, call(reg_68, reg_69));
          reg_71 = call(reg_8, reg_62);
          reg_5 = add(reg_5, cns241);
        }
        if ((goto_101 || false)) {
          goto_101 = false;
          reg_74 = reg_1.r;
          reg_75 = newStack();
          push(reg_75, reg_4);
          push(reg_75, reg_5);
          reg_76 = call(reg_74, reg_75);
          reg_77 = next(reg_76);
          reg_78 = next(reg_76);
          goto_124 = !tobool(reg_77);
          if (!goto_124) {
            if (tobool(ne(reg_11, cns242))) {
              reg_83 = newStack();
              push(reg_83, cns243);
              push(reg_83, reg_11);
              reg_85 = call(reg_8, reg_83);
              reg_11 = cns244;
            }
            reg_5 = reg_78;
          }
          if ((goto_124 || false)) {
            goto_124 = false;
            reg_88 = lua$get(reg_4, cns245);
            reg_89 = newStack();
            push(reg_89, reg_4);
            push(reg_89, reg_5);
            reg_77 = first(call(reg_88, reg_89));
            reg_5 = add(reg_5, cns246);
          }
          reg_95 = lua$get(reg_4, cns247);
          reg_96 = newStack();
          push(reg_96, reg_4);
          push(reg_96, reg_5);
          reg_98 = first(call(reg_95, reg_96));
          reg_100 = eq(reg_98, cns248);
          if (!tobool(reg_100)) {
            reg_100 = eq(reg_98, cns249);
          }
          if (!tobool(reg_100)) {
            reg_100 = eq(reg_98, cns250);
          }
          if (!tobool(reg_100)) {
            reg_100 = eq(reg_98, cns251);
          }
          goto_187 = !tobool(reg_100);
          if (!goto_187) {
            reg_113 = lua$get(reg_1.i, cns252);
            reg_114 = newStack();
            push(reg_114, reg_77);
            if (tobool(eq(first(call(reg_113, reg_114)), cns253))) {
              reg_120 = reg_1.m;
              reg_121 = newStack();
              push(reg_121, reg_77);
              reg_77 = first(call(reg_120, reg_121));
            }
            reg_124 = newStack();
            push(reg_124, reg_98);
            push(reg_124, reg_77);
            reg_125 = call(reg_8, reg_124);
            reg_5 = add(reg_5, cns254);
          }
          if ((goto_187 || false)) {
            goto_187 = false;
            reg_130 = lua$get(reg_1.i, cns255);
            reg_131 = newStack();
            push(reg_131, reg_77);
            goto_201 = !tobool(eq(first(call(reg_130, reg_131)), cns256));
            if (!goto_201) {
              reg_11 = concat(reg_11, reg_77);
            }
            if ((goto_201 || false)) {
              goto_201 = false;
              reg_138 = newStack();
              push(reg_138, cns257);
              push(reg_138, reg_77);
              reg_140 = call(reg_8, reg_138);
            }
          }
        }
      }
    }
  }
  if (tobool(ne(reg_11, cns258))) {
    reg_144 = newStack();
    push(reg_144, cns259);
    push(reg_144, reg_11);
    reg_146 = call(reg_8, reg_144);
  }
  reg_148 = reg_1.s;
  reg_150 = lua$length(reg_3.b);
  reg_151 = cns260;
  reg_153 = unm(cns261);
  reg_155 = lt(reg_153, cns262);
  loop_1: while (true) {
    goto_232 = tobool(reg_155);
    if (!goto_232) {
      if (tobool(gt(reg_150, reg_151))) break loop_1;
    }
    if ((goto_232 || false)) {
      goto_232 = false;
      if (tobool(lt(reg_150, reg_151))) break loop_1;
    }
    reg_162 = lua$get(reg_3.b, reg_150);
    goto_253 = !tobool(eq(lua$get(reg_162, cns263), cns264));
    if (!goto_253) {
      reg_168 = reg_1.t;
      reg_169 = newStack();
      push(reg_169, lua$get(reg_162, cns265));
      push(reg_169, reg_148);
      reg_148 = first(call(reg_168, reg_169));
    }
    if ((goto_253 || false)) {
      goto_253 = false;
      goto_269 = !tobool(eq(lua$get(reg_162, cns266), cns267));
      if (!goto_269) {
        reg_179 = reg_1.u;
        reg_180 = newStack();
        push(reg_180, lua$get(reg_162, cns268));
        push(reg_180, reg_148);
        reg_148 = first(call(reg_179, reg_180));
      }
      if ((goto_269 || false)) {
        goto_269 = false;
        goto_285 = !tobool(eq(lua$get(reg_162, cns269), cns270));
        if (!goto_285) {
          reg_190 = reg_1.v;
          reg_191 = newStack();
          push(reg_191, lua$get(reg_162, cns271));
          push(reg_191, reg_148);
          reg_148 = first(call(reg_190, reg_191));
        }
        if ((goto_285 || false)) {
          goto_285 = false;
          goto_310 = !tobool(eq(lua$get(reg_162, cns272), cns273));
          if (!goto_310) {
            reg_201 = reg_1.v;
            reg_202 = newStack();
            push(reg_202, lua$get(reg_162, cns274));
            push(reg_202, reg_148);
            reg_148 = first(call(reg_201, reg_202));
            reg_207 = reg_1.u;
            reg_208 = newStack();
            push(reg_208, lua$get(reg_162, cns275));
            push(reg_208, reg_148);
            reg_148 = first(call(reg_207, reg_208));
          }
          if ((goto_310 || false)) {
            goto_310 = false;
            goto_326 = !tobool(eq(lua$get(reg_162, cns276), cns277));
            if (!goto_326) {
              reg_218 = reg_1.w;
              reg_219 = newStack();
              push(reg_219, lua$get(reg_162, cns278));
              push(reg_219, reg_148);
              reg_148 = first(call(reg_218, reg_219));
            }
            if ((goto_326 || false)) {
              goto_326 = false;
              goto_342 = !tobool(eq(lua$get(reg_162, cns279), cns280));
              if (!goto_342) {
                reg_229 = reg_1.x;
                reg_230 = newStack();
                push(reg_230, lua$get(reg_162, cns281));
                push(reg_230, reg_148);
                reg_148 = first(call(reg_229, reg_230));
              }
              if ((goto_342 || false)) {
                goto_342 = false;
                goto_358 = !tobool(eq(lua$get(reg_162, cns282), cns283));
                if (!goto_358) {
                  reg_240 = reg_1.y;
                  reg_241 = newStack();
                  push(reg_241, lua$get(reg_162, cns284));
                  push(reg_241, reg_148);
                  reg_148 = first(call(reg_240, reg_241));
                }
                if ((goto_358 || false)) {
                  goto_358 = false;
                  goto_374 = !tobool(eq(lua$get(reg_162, cns285), cns286));
                  if (!goto_374) {
                    reg_251 = reg_1.z;
                    reg_252 = newStack();
                    push(reg_252, lua$get(reg_162, cns287));
                    push(reg_252, reg_148);
                    reg_148 = first(call(reg_251, reg_252));
                  }
                  if ((goto_374 || false)) {
                    goto_374 = false;
                    if (tobool(eq(lua$get(reg_162, cns288), cns289))) {
                      reg_262 = reg_1.aa;
                      reg_263 = newStack();
                      push(reg_263, lua$get(reg_162, cns290));
                      push(reg_263, reg_148);
                      reg_148 = first(call(reg_262, reg_263));
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    reg_150 = add(reg_150, reg_153);
  }
  reg_269 = newStack();
  push(reg_269, reg_148);
  return reg_269;
}
var cns291 = parseNum("1")
var cns292 = parseNum("1")
function function$44 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_8, reg_9, reg_12, reg_13, reg_14, reg_15;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = newTable();
  reg_8 = reg_1.ab;
  reg_9 = newStack();
  push(reg_9, reg_5);
  push(reg_9, cns291);
  reg_12 = first(call(reg_8, reg_9));
  reg_13 = newStack();
  reg_14 = newStack();
  push(reg_14, reg_4);
  reg_15 = reg_6;
  if (!tobool(reg_6)) {
    reg_15 = cns292;
  }
  push(reg_14, reg_15);
  append(reg_13, call(reg_12, reg_14));
  return reg_13;
}
var cns293 = parseNum("1")
var cns294 = parseNum("1")
var cns295 = parseNum("0")
var cns296 = anyStr("type")
var cns297 = anyStr("table")
var cns298 = anyStr("sub")
var cns299 = anyStr("start")
var cns300 = anyStr("end")
var cns301 = anyStr("table")
var cns302 = anyStr("unpack")
function function$45 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_8, reg_10, reg_16, reg_19, reg_20, reg_27, reg_28, reg_36, reg_41, reg_42;
  var goto_15=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = cns293;
  reg_7 = lua$length(reg_4);
  reg_8 = cns294;
  reg_10 = lt(reg_8, cns295);
  loop_1: while (true) {
    goto_15 = tobool(reg_10);
    if (!goto_15) {
      if (tobool(gt(reg_6, reg_7))) break loop_1;
    }
    if ((goto_15 || false)) {
      goto_15 = false;
      if (tobool(lt(reg_6, reg_7))) break loop_1;
    }
    reg_16 = lua$get(reg_4, reg_6);
    reg_19 = lua$get(reg_1.i, cns296);
    reg_20 = newStack();
    push(reg_20, reg_16);
    if (tobool(eq(first(call(reg_19, reg_20)), cns297))) {
      reg_27 = lua$get(reg_5, cns298);
      reg_28 = newStack();
      push(reg_28, reg_5);
      push(reg_28, lua$get(reg_16, cns299));
      push(reg_28, lua$get(reg_16, cns300));
      lua$set(reg_4, reg_6, first(call(reg_27, reg_28)));
    }
    reg_6 = add(reg_6, reg_8);
  }
  reg_36 = newStack();
  reg_41 = lua$get(lua$get(reg_1.i, cns301), cns302);
  reg_42 = newStack();
  push(reg_42, reg_4);
  append(reg_36, call(reg_41, reg_42));
  return reg_36;
}
var cns303 = anyStr("string")
var cns304 = anyStr("find")
var cns305 = anyStr("error")
var cns306 = anyStr("bad argument #1 to 'string.find' (value expected)")
var cns307 = anyStr("type")
var cns308 = anyStr("number")
var cns309 = anyStr("tostring")
var cns310 = anyStr("type")
var cns311 = anyStr("string")
var cns312 = anyStr("error")
var cns313 = anyStr("bad argument #1 to 'string.find' (string expected, got ")
var cns314 = anyStr("type")
var cns315 = anyStr(")")
var cns316 = parseNum("1")
var cns317 = anyStr("tonumber")
var cns318 = anyStr("error")
var cns319 = anyStr("bad argument #2 to 'string.find' (number expected, got ")
var cns320 = anyStr("type")
var cns321 = anyStr(")")
var cns322 = parseNum("0")
var cns323 = parseNum("1")
var cns324 = parseNum("1")
var cns325 = parseNum("1")
var cns326 = parseNum("1")
var cns327 = parseNum("1")
var cns328 = parseNum("0")
var cns329 = parseNum("1")
var cns330 = anyStr("sub")
var cns331 = anyStr("charat")
var cns332 = parseNum("1")
var cns333 = anyStr("^")
var cns334 = parseNum("1")
var cns335 = anyStr("sub")
var cns336 = parseNum("2")
var cns337 = parseNum("1")
var cns338 = parseNum("0")
var cns339 = anyStr("charat")
var cns340 = parseNum("1")
var cns341 = anyStr("$")
var cns342 = anyStr("charat")
var cns343 = parseNum("2")
var cns344 = anyStr("%")
var cns345 = anyStr("sub")
var cns346 = parseNum("1")
var cns347 = parseNum("2")
var cns348 = parseNum("1")
var cns349 = parseNum("1")
var cns350 = parseNum("0")
function string_find (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_12, reg_13, reg_18, reg_19, reg_27, reg_28, reg_33, reg_34, reg_42, reg_43, reg_44, reg_47, reg_48, reg_55, reg_62, reg_63, reg_70, reg_71, reg_72, reg_75, reg_76, reg_95, reg_100, reg_101, reg_103, reg_112, reg_114, reg_115, reg_120, reg_122, reg_124, reg_125, reg_134, reg_135, reg_141, reg_143, reg_144, reg_150, reg_153, reg_154, reg_164, reg_165, reg_171, reg_172, reg_175, reg_176, reg_177, reg_178, reg_179, reg_181, reg_187, reg_189, reg_190, reg_194, reg_195, reg_196, reg_199;
  var goto_17=false, goto_37=false, goto_137=false, goto_164=false, goto_254=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  goto_17 = !tobool(not(reg_5));
  if (!goto_17) {
    reg_12 = lua$get(reg_1.i, cns305);
    reg_13 = newStack();
    push(reg_13, cns306);
    reg_15 = call(reg_12, reg_13);
  }
  if ((goto_17 || false)) {
    goto_17 = false;
    reg_18 = lua$get(reg_1.i, cns307);
    reg_19 = newStack();
    push(reg_19, reg_5);
    goto_37 = !tobool(eq(first(call(reg_18, reg_19)), cns308));
    if (!goto_37) {
      reg_27 = lua$get(reg_1.i, cns309);
      reg_28 = newStack();
      push(reg_28, reg_5);
      reg_5 = first(call(reg_27, reg_28));
    }
    if ((goto_37 || false)) {
      goto_37 = false;
      reg_33 = lua$get(reg_1.i, cns310);
      reg_34 = newStack();
      push(reg_34, reg_5);
      if (tobool(ne(first(call(reg_33, reg_34)), cns311))) {
        reg_42 = lua$get(reg_1.i, cns312);
        reg_43 = newStack();
        reg_44 = cns313;
        reg_47 = lua$get(reg_1.i, cns314);
        reg_48 = newStack();
        push(reg_48, reg_5);
        push(reg_43, concat(reg_44, concat(first(call(reg_47, reg_48)), cns315)));
        reg_54 = call(reg_42, reg_43);
      }
    }
  }
  reg_55 = reg_6;
  if (tobool(eq(reg_6, nil()))) {
    reg_6 = cns316;
  }
  reg_62 = lua$get(reg_1.i, cns317);
  reg_63 = newStack();
  push(reg_63, reg_6);
  reg_6 = first(call(reg_62, reg_63));
  if (tobool(not(reg_6))) {
    reg_70 = lua$get(reg_1.i, cns318);
    reg_71 = newStack();
    reg_72 = cns319;
    reg_75 = lua$get(reg_1.i, cns320);
    reg_76 = newStack();
    push(reg_76, reg_55);
    push(reg_71, concat(reg_72, concat(first(call(reg_75, reg_76)), cns321)));
    reg_82 = call(reg_70, reg_71);
  }
  if (tobool(lt(reg_6, cns322))) {
    reg_6 = add(add(lua$length(reg_4), cns323), reg_6);
  }
  if (tobool(lt(reg_6, cns324))) {
    reg_6 = cns325;
  }
  goto_164 = !tobool(reg_7);
  if (!goto_164) {
    reg_95 = reg_6;
    reg_100 = sub(add(lua$length(reg_4), cns326), lua$length(reg_5));
    reg_101 = cns327;
    reg_103 = lt(reg_101, cns328);
    loop_2: while (true) {
      goto_137 = tobool(reg_103);
      if (!goto_137) {
        if (tobool(gt(reg_95, reg_100))) break loop_2;
      }
      if ((goto_137 || false)) {
        goto_137 = false;
        if (tobool(lt(reg_95, reg_100))) break loop_2;
      }
      reg_112 = sub(add(reg_95, lua$length(reg_5)), cns329);
      reg_114 = lua$get(reg_4, cns330);
      reg_115 = newStack();
      push(reg_115, reg_4);
      push(reg_115, reg_95);
      push(reg_115, reg_112);
      if (tobool(eq(first(call(reg_114, reg_115)), reg_5))) {
        reg_120 = newStack();
        push(reg_120, reg_95);
        push(reg_120, reg_112);
        return reg_120;
      }
      reg_95 = add(reg_95, reg_101);
    }
  }
  if ((goto_164 || false)) {
    goto_164 = false;
    reg_122 = lua$length(reg_4);
    reg_124 = lua$get(reg_5, cns331);
    reg_125 = newStack();
    push(reg_125, reg_5);
    push(reg_125, cns332);
    if (tobool(eq(first(call(reg_124, reg_125)), cns333))) {
      reg_122 = cns334;
      reg_134 = lua$get(reg_5, cns335);
      reg_135 = newStack();
      push(reg_135, reg_5);
      push(reg_135, cns336);
      push(reg_135, unm(cns337));
      reg_5 = first(call(reg_134, reg_135));
    }
    reg_141 = cns338;
    reg_143 = lua$get(reg_5, cns339);
    reg_144 = newStack();
    push(reg_144, reg_5);
    push(reg_144, unm(cns340));
    reg_150 = eq(first(call(reg_143, reg_144)), cns341);
    if (tobool(reg_150)) {
      reg_153 = lua$get(reg_5, cns342);
      reg_154 = newStack();
      push(reg_154, reg_5);
      push(reg_154, unm(cns343));
      reg_150 = ne(first(call(reg_153, reg_154)), cns344);
    }
    if (tobool(reg_150)) {
      reg_141 = lua$length(reg_4);
      reg_164 = lua$get(reg_5, cns345);
      reg_165 = newStack();
      push(reg_165, reg_5);
      push(reg_165, cns346);
      push(reg_165, unm(cns347));
      reg_5 = first(call(reg_164, reg_165));
    }
    reg_171 = reg_1.ab;
    reg_172 = newStack();
    push(reg_172, reg_5);
    push(reg_172, cns348);
    reg_175 = first(call(reg_171, reg_172));
    reg_176 = newTable();
    reg_177 = reg_6;
    reg_178 = reg_122;
    reg_179 = cns349;
    reg_181 = lt(reg_179, cns350);
    loop_1: while (true) {
      goto_254 = tobool(reg_181);
      if (!goto_254) {
        if (tobool(gt(reg_177, reg_178))) break loop_1;
      }
      if ((goto_254 || false)) {
        goto_254 = false;
        if (tobool(lt(reg_177, reg_178))) break loop_1;
      }
      reg_187 = newStack();
      push(reg_187, reg_4);
      push(reg_187, reg_177);
      push(reg_187, reg_176);
      reg_189 = first(call(reg_175, reg_187));
      reg_190 = reg_189;
      if (tobool(reg_189)) {
        reg_190 = ge(reg_189, reg_141);
      }
      if (tobool(reg_190)) {
        reg_194 = newStack();
        push(reg_194, reg_177);
        push(reg_194, reg_189);
        reg_195 = reg_1.ac;
        reg_196 = newStack();
        push(reg_196, reg_176);
        push(reg_196, reg_4);
        append(reg_194, call(reg_195, reg_196));
        return reg_194;
      }
      reg_177 = add(reg_177, reg_179);
    }
  }
  reg_199 = newStack();
  return reg_199;
}
var cns351 = anyStr("string")
var cns352 = anyStr("match")
var cns353 = parseNum("1")
var cns354 = anyStr("find")
var cns355 = parseNum("2")
var cns356 = anyStr("table")
var cns357 = anyStr("unpack")
var cns358 = parseNum("3")
var cns359 = parseNum("0")
var cns360 = anyStr("sub")
var cns361 = parseNum("1")
var cns362 = parseNum("2")
function string_match (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_8, reg_9, reg_11, reg_12, reg_18, reg_23, reg_24, reg_31, reg_33, reg_34, reg_40;
  var goto_36=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = newTable();
  reg_9 = cns353;
  reg_11 = lua$get(reg_4, cns354);
  reg_12 = newStack();
  push(reg_12, reg_4);
  push(reg_12, reg_5);
  push(reg_12, reg_6);
  push(reg_12, reg_7);
  table_append(reg_8, reg_9, call(reg_11, reg_12));
  goto_36 = !tobool(gt(lua$length(reg_8), cns355));
  if (!goto_36) {
    reg_18 = newStack();
    reg_23 = lua$get(lua$get(reg_1.i, cns356), cns357);
    reg_24 = newStack();
    push(reg_24, reg_8);
    push(reg_24, cns358);
    append(reg_18, call(reg_23, reg_24));
    return reg_18;
  }
  if ((goto_36 || false)) {
    goto_36 = false;
    if (tobool(gt(lua$length(reg_8), cns359))) {
      reg_31 = newStack();
      reg_33 = lua$get(reg_4, cns360);
      reg_34 = newStack();
      push(reg_34, reg_4);
      push(reg_34, lua$get(reg_8, cns361));
      push(reg_34, lua$get(reg_8, cns362));
      append(reg_31, call(reg_33, reg_34));
      return reg_31;
    }
  }
  reg_40 = newStack();
  return reg_40;
}
var cns363 = anyStr("string")
var cns364 = anyStr("gsub")
var cns365 = parseNum("1")
function function$46 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_8;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = newStack();
  push(reg_7, reg_5);
  push(reg_7, reg_6);
  reg_8 = newTable();
  table_append(reg_8, cns365, copy(reg_0));
  push(reg_7, reg_8);
  return reg_7;
}
var cns366 = anyStr("type")
var cns367 = anyStr("function")
function function$47 (reg_0, reg_1) {
  var reg_6, reg_7, reg_8;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = reg_1.b;
  reg_8 = newStack();
  append(reg_8, reg_0);
  append(reg_6, call(reg_7, reg_8));
  return reg_6;
}
var cns368 = anyStr("type")
var cns369 = anyStr("table")
function function$48 (reg_0, reg_1) {
  var reg_6, reg_7;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = newStack();
  push(reg_7, lua$get(reg_1.b, reg_6));
  return reg_7;
}
var cns370 = anyStr("type")
var cns371 = anyStr("string")
var cns372 = parseNum("1")
var cns373 = anyStr("%")
var cns374 = anyStr("%%")
var cns375 = parseNum("1")
var cns376 = parseNum("1")
var cns377 = parseNum("0")
var cns378 = anyStr("tostring")
var cns379 = parseNum("1")
var cns380 = anyStr("gsub")
var cns381 = anyStr("%%([%d%%])")
function function$49 (reg_0, reg_1) {
  var reg_2, reg_5, reg_8, reg_11, reg_12, reg_13, reg_15, reg_23, reg_24, reg_31, reg_32, reg_34, reg_35;
  var goto_22=false;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = newTable();
  table_append(reg_5, cns372, copy(reg_0));
  reg_8 = newTable();
  lua$set(reg_8, cns373, cns374);
  reg_11 = cns375;
  reg_12 = lua$length(reg_5);
  reg_13 = cns376;
  reg_15 = lt(reg_13, cns377);
  loop_1: while (true) {
    goto_22 = tobool(reg_15);
    if (!goto_22) {
      if (tobool(gt(reg_11, reg_12))) break loop_1;
    }
    if ((goto_22 || false)) {
      goto_22 = false;
      if (tobool(lt(reg_11, reg_12))) break loop_1;
    }
    reg_23 = lua$get(reg_2.i, cns378);
    reg_24 = newStack();
    push(reg_24, sub(reg_11, cns379));
    lua$set(reg_8, first(call(reg_23, reg_24)), lua$get(reg_5, reg_11));
    reg_11 = add(reg_11, reg_13);
  }
  reg_31 = newStack();
  reg_32 = reg_1.b;
  reg_34 = lua$get(reg_32, cns380);
  reg_35 = newStack();
  push(reg_35, reg_32);
  push(reg_35, cns381);
  push(reg_35, reg_8);
  append(reg_31, call(reg_34, reg_35));
  return reg_31;
}
var cns382 = anyStr("error")
var cns383 = anyStr("bad argument #3 to 'string.gsub' (string/function/table expected)")
var cns384 = parseNum("1")
var cns385 = anyStr("")
var cns386 = parseNum("0")
var cns387 = anyStr("find")
var cns388 = anyStr("sub")
var cns389 = parseNum("1")
var cns390 = anyStr("sub")
var cns391 = anyStr("table")
var cns392 = anyStr("unpack")
var cns393 = parseNum("1")
var cns394 = parseNum("1")
var cns395 = anyStr("sub")
function string_gsub (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_7, reg_9, reg_10, reg_13, reg_14, reg_25, reg_26, reg_37, reg_38, reg_49, reg_50, reg_53, reg_54, reg_55, reg_57, reg_59, reg_63, reg_65, reg_66, reg_68, reg_69, reg_70, reg_71, reg_75, reg_76, reg_80, reg_81, reg_83, reg_84, reg_91, reg_92, reg_103, reg_104, reg_108;
  var goto_26=false, goto_42=false, goto_58=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_3.b = next(reg_0);
  reg_7 = next(reg_0);
  reg_9 = anyFn((function (a) { return function$46(a,this) }).bind(reg_3));
  reg_10 = nil();
  reg_13 = lua$get(reg_1.i, cns366);
  reg_14 = newStack();
  push(reg_14, reg_3.b);
  goto_26 = !tobool(eq(first(call(reg_13, reg_14)), cns367));
  if (!goto_26) {
    reg_10 = anyFn((function (a) { return function$47(a,this) }).bind(reg_3));
  }
  if ((goto_26 || false)) {
    goto_26 = false;
    reg_25 = lua$get(reg_1.i, cns368);
    reg_26 = newStack();
    push(reg_26, reg_3.b);
    goto_42 = !tobool(eq(first(call(reg_25, reg_26)), cns369));
    if (!goto_42) {
      reg_10 = anyFn((function (a) { return function$48(a,this) }).bind(reg_3));
    }
    if ((goto_42 || false)) {
      goto_42 = false;
      reg_37 = lua$get(reg_1.i, cns370);
      reg_38 = newStack();
      push(reg_38, reg_3.b);
      goto_58 = !tobool(eq(first(call(reg_37, reg_38)), cns371));
      if (!goto_58) {
        reg_10 = anyFn((function (a) { return function$49(a,this) }).bind(reg_3));
      }
      if ((goto_58 || false)) {
        goto_58 = false;
        reg_49 = lua$get(reg_1.i, cns382);
        reg_50 = newStack();
        push(reg_50, cns383);
        reg_52 = call(reg_49, reg_50);
      }
    }
  }
  reg_53 = cns384;
  reg_54 = cns385;
  reg_55 = cns386;
  loop_1: while (true) {
    reg_57 = lt(reg_53, lua$length(reg_4));
    if (tobool(reg_57)) {
      reg_59 = not(reg_7);
      if (!tobool(reg_59)) {
        reg_59 = lt(reg_55, reg_7);
      }
      reg_57 = reg_59;
    }
    if (!tobool(reg_57)) break loop_1;
    reg_63 = newStack();
    reg_65 = lua$get(reg_4, cns387);
    reg_66 = newStack();
    push(reg_66, reg_4);
    push(reg_66, reg_5);
    push(reg_66, reg_53);
    append(reg_63, call(reg_65, reg_66));
    reg_68 = call(reg_9, reg_63);
    reg_69 = next(reg_68);
    reg_70 = next(reg_68);
    reg_71 = next(reg_68);
    if (tobool(not(reg_69))) {
      if (true) break loop_1;
    }
    reg_75 = lua$get(reg_4, cns388);
    reg_76 = newStack();
    push(reg_76, reg_4);
    push(reg_76, reg_53);
    push(reg_76, sub(reg_69, cns389));
    reg_80 = first(call(reg_75, reg_76));
    reg_81 = newStack();
    reg_83 = lua$get(reg_4, cns390);
    reg_84 = newStack();
    push(reg_84, reg_4);
    push(reg_84, reg_69);
    push(reg_84, reg_70);
    push(reg_81, first(call(reg_83, reg_84)));
    reg_91 = lua$get(lua$get(reg_1.i, cns391), cns392);
    reg_92 = newStack();
    push(reg_92, reg_71);
    append(reg_81, call(reg_91, reg_92));
    reg_54 = concat(reg_54, concat(reg_80, first(call(reg_10, reg_81))));
    reg_53 = add(reg_70, cns393);
    reg_55 = add(reg_55, cns394);
  }
  reg_103 = lua$get(reg_4, cns395);
  reg_104 = newStack();
  push(reg_104, reg_4);
  push(reg_104, reg_53);
  reg_54 = concat(reg_54, first(call(reg_103, reg_104)));
  reg_108 = newStack();
  push(reg_108, reg_54);
  push(reg_108, reg_55);
  return reg_108;
}
function lua_lib_pattern$lua_main (reg_0) {
  var reg_1, reg_2, reg_5, reg_6, reg_69, reg_70, reg_75, reg_76, reg_81, reg_82, reg_85;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1, c: reg_1, d: reg_1, e: reg_1, f: reg_1, g: reg_1, h: reg_1, i: reg_1, j: reg_1, k: reg_1, l: reg_1, m: reg_1, n: reg_1, o: reg_1, p: reg_1, q: reg_1, r: reg_1, s: reg_1, t: reg_1, u: reg_1, v: reg_1, w: reg_1, x: reg_1, y: reg_1, z: reg_1, aa: reg_1, ab: reg_1, ac: reg_1};
  reg_2.i = reg_0;
  reg_5 = lua$get(reg_2.i, cns135);
  reg_6 = cns136;
  lua$set(reg_5, reg_6, anyFn((function (a) { return string_charat(a,this) }).bind(reg_2)));
  reg_2.f = anyFn((function (a) { return lua_lib_pattern$function(a,this) }).bind(reg_2));
  reg_2.a = anyFn((function (a) { return function$6(a,this) }).bind(reg_2));
  reg_2.b = anyFn((function (a) { return function$7(a,this) }).bind(reg_2));
  reg_2.e = anyFn((function (a) { return function$8(a,this) }).bind(reg_2));
  reg_2.d = anyFn((function (a) { return function$9(a,this) }).bind(reg_2));
  reg_2.c = anyFn((function (a) { return function$10(a,this) }).bind(reg_2));
  reg_2.g = anyFn((function (a) { return function$11(a,this) }).bind(reg_2));
  reg_2.h = anyFn((function (a) { return function$12(a,this) }).bind(reg_2));
  reg_2.j = anyFn((function (a) { return function$13(a,this) }).bind(reg_2));
  reg_2.k = anyFn((function (a) { return function$14(a,this) }).bind(reg_2));
  reg_2.q = anyFn((function (a) { return function$15(a,this) }).bind(reg_2));
  reg_2.l = anyFn((function (a) { return function$16(a,this) }).bind(reg_2));
  reg_2.p = anyFn((function (a) { return function$18(a,this) }).bind(reg_2));
  reg_2.m = anyFn((function (a) { return function$20(a,this) }).bind(reg_2));
  reg_2.o = anyFn((function (a) { return function$22(a,this) }).bind(reg_2));
  reg_2.n = anyFn((function (a) { return function$23(a,this) }).bind(reg_2));
  reg_2.r = anyFn((function (a) { return function$24(a,this) }).bind(reg_2));
  reg_2.t = anyFn((function (a) { return function$25(a,this) }).bind(reg_2));
  reg_2.u = anyFn((function (a) { return function$27(a,this) }).bind(reg_2));
  reg_2.s = anyFn((function (a) { return function$29(a,this) }).bind(reg_2));
  reg_2.v = anyFn((function (a) { return function$30(a,this) }).bind(reg_2));
  reg_2.w = anyFn((function (a) { return function$32(a,this) }).bind(reg_2));
  reg_2.x = anyFn((function (a) { return function$34(a,this) }).bind(reg_2));
  reg_2.y = anyFn((function (a) { return function$36(a,this) }).bind(reg_2));
  reg_2.z = anyFn((function (a) { return function$38(a,this) }).bind(reg_2));
  reg_2.aa = anyFn((function (a) { return function$40(a,this) }).bind(reg_2));
  reg_2.ab = anyFn((function (a) { return function$42(a,this) }).bind(reg_2));
  reg_64 = anyFn((function (a) { return function$44(a,this) }).bind(reg_2));
  reg_2.ac = anyFn((function (a) { return function$45(a,this) }).bind(reg_2));
  reg_69 = lua$get(reg_2.i, cns303);
  reg_70 = cns304;
  lua$set(reg_69, reg_70, anyFn((function (a) { return string_find(a,this) }).bind(reg_2)));
  reg_75 = lua$get(reg_2.i, cns351);
  reg_76 = cns352;
  lua$set(reg_75, reg_76, anyFn((function (a) { return string_match(a,this) }).bind(reg_2)));
  reg_81 = lua$get(reg_2.i, cns363);
  reg_82 = cns364;
  lua$set(reg_81, reg_82, anyFn((function (a) { return string_gsub(a,this) }).bind(reg_2)));
  reg_85 = newStack();
  return reg_85;
}
Auro.require = function (name) {
  if (typeof require !== 'function') return null
  try { return require(name) }
  catch (e) {
  if (e.code === 'MODULE_NOT_FOUND') return null
    else throw e
  }
};
Auro.fs = Auro.require('fs');
Auro.io_open = function (path, mode) {
  return {f: Auro.fs.openSync(path, mode), size: Auro.fs.statSync(path).size, pos: 0}
}
var File = function (val) { this.val = val; }
function newUserData (reg_0, reg_1) {
  var reg_3, reg_5, reg_8;
  reg_3 = cns1.a;
  reg_5 = (reg_3 + 1);
  cns1.a = reg_5;
  reg_8 = {a: reg_3, b: reg_0, c: reg_1};
  return reg_8;
}
function _open (reg_0) {
  var reg_4, reg_5, reg_6, reg_11, reg_12, reg_18, reg_24, reg_32, reg_38;
  var goto_11=false, goto_29=false, goto_41=false, goto_53=false;
  reg_4 = simple_string(next(reg_0), "1", "io.open");
  reg_5 = next(reg_0);
  reg_6 = "";
  goto_11 = !testNil(reg_5);
  if (!goto_11) {
    reg_6 = "r";
  }
  if ((goto_11 || false)) {
    goto_11 = false;
    if ((typeof reg_5 === 'string')) {
      reg_6 = reg_5;
    }
  }
  reg_12 = (reg_6 == "r");
  if (!reg_12) {
    reg_12 = (reg_6 == "rb");
  }
  goto_29 = !reg_12;
  if (!goto_29) {
    reg_11 = 'r';
  }
  if ((goto_29 || false)) {
    goto_29 = false;
    reg_18 = (reg_6 == "w");
    if (!reg_18) {
      reg_18 = (reg_6 == "wb");
    }
    goto_41 = !reg_18;
    if (!goto_41) {
      reg_11 = 'w';
    }
    if ((goto_41 || false)) {
      goto_41 = false;
      reg_24 = (reg_6 == "a");
      if (!reg_24) {
        reg_24 = (reg_6 == "ab");
      }
      goto_53 = !reg_24;
      if (!goto_53) {
        reg_11 = 'a';
      }
      if ((goto_53 || false)) {
        goto_53 = false;
        throw new Error("bad argument #2 to 'io.open' (invalid mode)");
      }
    }
  }
  reg_32 = new File(Auro.io_open(reg_4, reg_11));
  reg_38 = stackof(new type_23(newUserData(reg_32, cns2.e)));
  return reg_38;
}
function get_file (reg_0) {
  var reg_3, reg_7;
  var goto_11=false;
  if ((reg_0 instanceof type_23)) {
    reg_3 = reg_0.val;
    goto_11 = !(reg_3.b instanceof File);
    if (!goto_11) {
      reg_7 = reg_3.b.val;
      return reg_7;
    }
    if ((goto_11 || false)) {
      goto_11 = false;
      throw new Error("bad argument #1 to 'read' (file expected)");
    }
    if (true) { goto(20); };
  }
  throw new Error((("bad argument #1 to 'read' (file expected, got " + typestr(reg_0)) + ")"));
}
Auro.io_read = function (file, size) {
  var buf = new Uint8Array(size)
  var redd = Auro.fs.readSync(file.f, buf, 0, size, file.pos)
  file.pos += redd
  return buf.slice(0, redd)
}
Auro.string_new = function (buf) {
  if (typeof buf === 'string') return buf
  var codes = []
  for (var j = 0; j < buf.length; j++) {
    var c = buf[j]
    if (c > 0xEF) {
      c = (c & 0xF) << 0x12 | (buf[++j] & 0x3F) << 0xC | (buf[++j] & 0x3F) << 0x6 | (buf[++j] & 0x3F)
    } else if (c > 0xDF) {
      c = (c & 0xF) << 0xC | (buf[++j] & 0x3F) << 0x6 | (buf[++j] & 0x3F)
    } else if (c > 0xBF) {
      c = (c & 0x1F) << 0x6 | (buf[++j] & 0x3F)
    }
    if (c > 0xFFFF) {
      c -= 0x10000
      codes.push(c >>> 10 & 0x3FF | 0xD800)
      codes.push(0xDC00 | c & 0x3FF)
    } else {
      codes.push(c)
    }
  }
  return String.fromCharCode.apply(String, codes)
}
Auro.io_eof = function (file) {
  return file.pos >= file.size
}
function _read (reg_0) {
  var reg_2, reg_3, reg_4, reg_9, reg_35;
  var goto_28=false, goto_29=false, goto_35=false, goto_41=false, goto_47=false, goto_50=false, goto_57=false;
  reg_2 = get_file(next(reg_0));
  reg_3 = next(reg_0);
  if ((reg_3 instanceof type_20)) {
    reg_3 = "l";
  }
  goto_50 = !(typeof reg_3 === 'string');
  if (!goto_50) {
    reg_9 = reg_3;
    goto_29 = !(reg_9 == "a");
    if (!goto_29) {
      reg_4 = "";
      loop_1: while (true) {
        reg_4 = (reg_4 + Auro.string_new(Auro.io_read(reg_2, 128)));
        goto_28 = !!Auro.io_eof(reg_2);
        if (goto_28) break loop_1;
      }
      if (!goto_28) {
      }
      goto_28 = false;
    }
    if ((goto_29 || false)) {
      goto_29 = false;
      goto_35 = !(reg_9 == "n");
      if (!goto_35) {
        throw new Error("format 'n' not yet supported");
      }
      if ((goto_35 || false)) {
        goto_35 = false;
        goto_41 = !(reg_9 == "l");
        if (!goto_41) {
          throw new Error("format 'l' not yet supported");
        }
        if ((goto_41 || false)) {
          goto_41 = false;
          goto_47 = !(reg_9 == "L");
          if (!goto_47) {
            throw new Error("format 'L' not yet supported");
          }
          if ((goto_47 || false)) {
            goto_47 = false;
            throw new Error("bad argument #2 to 'read' (invalid format)");
          }
        }
      }
    }
  }
  if ((goto_50 || false)) {
    goto_50 = false;
    goto_57 = !(reg_3 instanceof Integer);
    if (!goto_57) {
      reg_4 = Auro.string_new(Auro.io_read(reg_2, reg_3.val));
    }
    if ((goto_57 || false)) {
      goto_57 = false;
      throw new Error("bad argument #2 to 'read' (invalid format)");
    }
  }
  reg_35 = stackof(reg_4);
  return reg_35;
}
Auro.str_tobuf = function (str) {
  bytes = []
  for (var i = 0; i < str.length; i++) {
    var c = str.charCodeAt(i)
    if (c >= 0xD800 && c <= 0xDFFF) {
      c = (c - 0xD800 << 10 | str.charCodeAt(++i) - 0xDC00) + 0x10000
    }
    if (c < 0x80) {
      bytes.push(c)
    } else if (c < 0x800) {
      bytes.push(c >> 0x6 | 0xC0, c & 0x3F | 0x80)
    } else if (c < 0x10000) {
      bytes.push(c >> 0xC | 0xE0, c >> 0x6 & 0x3F | 0x80, c & 0x3F | 0x80)
    } else {
      bytes.push(c >> 0x12 | 0xF0, c >> 0xC & 0x3F | 0x80, c >> 0x6 & 0x3F | 0x80, c & 0x3F | 0x80)
    }
  }
  return Uint8Array.from(bytes)
}
Auro.io_write = function (file, buf) {
  var written = Auro.fs.writeSync(file.f, buf, 0, buf.length, file.pos)
  file.pos += written
}
function _write (reg_0) {
  var reg_2, reg_3, reg_4, reg_14;
  reg_2 = get_file(next(reg_0));
  reg_3 = 1;
  reg_4 = "";
  loop_1: while (more(reg_0)) {
    reg_4 = (reg_4 + simple_string(next(reg_0), String(reg_3), "write"));
    reg_3 = (reg_3 + 1);
  }
  Auro.io_write(reg_2, Auro.str_tobuf(reg_4));
  reg_14 = newStack();
  return reg_14;
}
function _writebytes (reg_0) {
  var reg_2, reg_4, reg_5, reg_7, reg_15;
  reg_2 = get_file(next(reg_0));
  reg_4 = new Uint8Array(length(reg_0));
  reg_5 = 0;
  loop_1: while (more(reg_0)) {
    reg_7 = next(reg_0);
    reg_4[reg_5]=simple_number(reg_7, String((reg_5 + 1)), "writebytes");
    reg_5 = (reg_5 + 1);
  }
  Auro.io_write(reg_2, reg_4);
  reg_15 = newStack();
  return reg_15;
}
Auro.io_close = function (file) {
  Auro.fs.closeSync(file.f)
}
function _close (reg_0) {
  var reg_3;
  Auro.io_close(get_file(next(reg_0)));
  reg_3 = newStack();
  return reg_3;
}
function _filestr (reg_0) {
  var reg_1, reg_5, reg_12;
  reg_1 = next(reg_0);
  reg_2 = get_file(reg_1);
  reg_5 = reg_1.val.a;
  reg_12 = stackof((("file (" + String(reg_5)) + ")"));
  return reg_12;
}
Auro.exit = function (code) {
  if (typeof process !== "undefined") process.exit(code)
  else throw "Auro Exit with code " + code
}
function _exit (reg_0) {
  var reg_1, reg_2, reg_11;
  var goto_9=false, goto_12=false;
  reg_1 = next(reg_0);
  reg_2 = 0;
  goto_12 = !(typeof reg_1 === 'boolean');
  if (!goto_12) {
    goto_9 = !reg_1;
    if (!goto_9) {
      reg_2 = 0;
    }
    if ((goto_9 || false)) {
      goto_9 = false;
      reg_2 = 1;
    }
  }
  if ((goto_12 || false)) {
    goto_12 = false;
    reg_2 = simple_number_or(reg_1, 0, "1", "os.exit");
  }
  Auro.exit(reg_2);
  reg_11 = newStack();
  return reg_11;
}
function _tointeger (reg_0) {
  var reg_1, reg_6, reg_8, reg_10, reg_16, reg_17;
  reg_1 = next(reg_0);
  if ((typeof reg_1 === 'string')) {
    reg_1 = parseNum(reg_1);
  }
  if ((reg_1 instanceof Integer)) {
    reg_6 = stackof(reg_1);
    return reg_6;
  }
  if ((typeof reg_1 === 'number')) {
    reg_8 = reg_1;
    reg_10 = (reg_8 - Math.trunc(reg_8));
    if ((reg_10 == 0)) {
      reg_16 = stackof(new Integer(reg_8));
      return reg_16;
    }
  }
  reg_17 = newStack();
  return reg_17;
}
function _tofloat (reg_0) {
  var reg_1, reg_9, reg_11, reg_12;
  reg_1 = next(reg_0);
  if ((typeof reg_1 === 'string')) {
    reg_1 = parseNum(reg_1);
  }
  if ((reg_1 instanceof Integer)) {
    reg_9 = stackof(reg_1.val);
    return reg_9;
  }
  if ((typeof reg_1 === 'number')) {
    reg_11 = stackof(reg_1);
    return reg_11;
  }
  reg_12 = newStack();
  return reg_12;
}
function _mathtype (reg_0) {
  var reg_1, reg_5, reg_9, reg_10;
  reg_1 = next(reg_0);
  if ((reg_1 instanceof Integer)) {
    reg_5 = stackof("integer");
    return reg_5;
  }
  if ((typeof reg_1 === 'number')) {
    reg_9 = stackof("float");
    return reg_9;
  }
  reg_10 = newStack();
  return reg_10;
}
var cns396 = parseNum("1")
var cns397 = anyStr("math")
var cns398 = anyStr("tofloat")
var cns399 = anyStr("error")
var cns400 = anyStr("bad argument #")
var cns401 = anyStr(" to ")
var cns402 = anyStr(" (number expected, got ")
var cns403 = anyStr("type")
var cns404 = anyStr(")")
function lua_lib_math$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_14, reg_15, reg_17, reg_19, reg_22, reg_23, reg_24, reg_25, reg_26, reg_29, reg_30, reg_32, reg_33, reg_43;
  var goto_26=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  if (tobool(not(reg_6))) {
    reg_6 = cns396;
  }
  reg_14 = lua$get(lua$get(reg_1.a, cns397), cns398);
  reg_15 = newStack();
  push(reg_15, reg_4);
  reg_17 = first(call(reg_14, reg_15));
  goto_26 = !tobool(reg_17);
  if (!goto_26) {
    reg_19 = newStack();
    push(reg_19, reg_17);
    return reg_19;
  }
  if ((goto_26 || false)) {
    goto_26 = false;
    reg_22 = lua$get(reg_1.a, cns399);
    reg_23 = newStack();
    reg_24 = cns400;
    reg_25 = cns401;
    reg_26 = cns402;
    reg_29 = lua$get(reg_1.a, cns403);
    reg_30 = newStack();
    push(reg_30, reg_4);
    reg_32 = first(call(reg_29, reg_30));
    reg_33 = newStack();
    push(reg_33, cns404);
    push(reg_23, concat(reg_24, concat(reg_6, concat(reg_25, concat(reg_5, concat(reg_26, first(call(reg_32, reg_33))))))));
    reg_42 = call(reg_22, reg_23);
  }
  reg_43 = newStack();
  return reg_43;
}
var cns405 = anyStr("getarg")
var cns406 = anyStr("math")
var cns407 = anyStr("pi")
var cns408 = anyStr("math")
var cns409 = anyStr("huge")
var cns410 = anyStr("math")
var cns411 = anyStr("deg")
var cns412 = anyStr("math")
var cns413 = anyStr("pi")
var cns414 = parseNum("180")
function math_deg (reg_0, reg_1) {
  var reg_4, reg_5;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  push(reg_5, mul(div(reg_4, lua$get(lua$get(reg_1.a, cns412), cns413)), cns414));
  return reg_5;
}
var cns415 = anyStr("math")
var cns416 = anyStr("rad")
var cns417 = parseNum("180")
var cns418 = anyStr("math")
var cns419 = anyStr("pi")
function math_rad (reg_0, reg_1) {
  var reg_4, reg_5, reg_7;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = div(reg_4, cns417);
  push(reg_5, mul(reg_7, lua$get(lua$get(reg_1.a, cns418), cns419)));
  return reg_5;
}
var cns420 = anyStr("math")
var cns421 = anyStr("max")
var cns422 = parseNum("1")
var cns423 = anyStr("ipairs")
function math_max (reg_0, reg_1) {
  var reg_4, reg_5, reg_10, reg_11, reg_12, reg_13, reg_14, reg_15, reg_16, reg_17, reg_19, reg_25;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  table_append(reg_5, cns422, copy(reg_0));
  reg_10 = lua$get(reg_1.a, cns423);
  reg_11 = newStack();
  push(reg_11, reg_5);
  reg_12 = call(reg_10, reg_11);
  reg_13 = next(reg_12);
  reg_14 = next(reg_12);
  reg_15 = next(reg_12);
  loop_1: while (true) {
    reg_16 = newStack();
    push(reg_16, reg_14);
    push(reg_16, reg_15);
    reg_17 = call(reg_13, reg_16);
    reg_15 = next(reg_17);
    reg_19 = next(reg_17);
    if (tobool(eq(reg_15, nil()))) break loop_1;
    if (tobool(gt(reg_19, reg_4))) {
      reg_4 = reg_19;
    }
  }
  reg_25 = newStack();
  push(reg_25, reg_4);
  return reg_25;
}
var cns424 = anyStr("math")
var cns425 = anyStr("min")
var cns426 = parseNum("1")
var cns427 = anyStr("ipairs")
function math_min (reg_0, reg_1) {
  var reg_4, reg_5, reg_10, reg_11, reg_12, reg_13, reg_14, reg_15, reg_16, reg_17, reg_19, reg_25;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  table_append(reg_5, cns426, copy(reg_0));
  reg_10 = lua$get(reg_1.a, cns427);
  reg_11 = newStack();
  push(reg_11, reg_5);
  reg_12 = call(reg_10, reg_11);
  reg_13 = next(reg_12);
  reg_14 = next(reg_12);
  reg_15 = next(reg_12);
  loop_1: while (true) {
    reg_16 = newStack();
    push(reg_16, reg_14);
    push(reg_16, reg_15);
    reg_17 = call(reg_13, reg_16);
    reg_15 = next(reg_17);
    reg_19 = next(reg_17);
    if (tobool(eq(reg_15, nil()))) break loop_1;
    if (tobool(lt(reg_19, reg_4))) {
      reg_4 = reg_19;
    }
  }
  reg_25 = newStack();
  push(reg_25, reg_4);
  return reg_25;
}
var cns428 = anyStr("math")
var cns429 = anyStr("abs")
var cns430 = anyStr("tonumber")
var cns431 = anyStr("error")
var cns432 = anyStr("bad argument #1 to abs (number expected, got ")
var cns433 = anyStr("type")
var cns434 = anyStr(")")
var cns435 = parseNum("0")
function math_abs (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_10, reg_15, reg_16, reg_17, reg_20, reg_21, reg_31, reg_33, reg_34;
  var goto_40=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns430);
  reg_8 = newStack();
  push(reg_8, reg_4);
  reg_10 = first(call(reg_7, reg_8));
  if (tobool(not(reg_10))) {
    reg_15 = lua$get(reg_1.a, cns431);
    reg_16 = newStack();
    reg_17 = cns432;
    reg_20 = lua$get(reg_1.a, cns433);
    reg_21 = newStack();
    push(reg_21, reg_4);
    push(reg_16, concat(reg_17, concat(first(call(reg_20, reg_21)), cns434)));
    reg_27 = call(reg_15, reg_16);
  }
  goto_40 = !tobool(lt(reg_10, cns435));
  if (!goto_40) {
    reg_31 = newStack();
    push(reg_31, unm(reg_10));
    return reg_31;
  }
  if ((goto_40 || false)) {
    goto_40 = false;
    reg_33 = newStack();
    push(reg_33, reg_10);
    return reg_33;
  }
  reg_34 = newStack();
  return reg_34;
}
var cns436 = anyStr("math")
var cns437 = anyStr("ceil")
var cns438 = anyStr("getarg")
var cns439 = anyStr("ceil")
function math_ceil (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns438);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns439);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.ceil(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns440 = anyStr("math")
var cns441 = anyStr("floor")
var cns442 = anyStr("getarg")
var cns443 = anyStr("floor")
function math_floor (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns442);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns443);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.floor(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns444 = anyStr("math")
var cns445 = anyStr("fmod")
var cns446 = anyStr("getarg")
var cns447 = anyStr("fmod")
var cns448 = parseNum("1")
var cns449 = anyStr("getarg")
var cns450 = anyStr("fmod")
var cns451 = parseNum("2")
function math_fmod (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_16, reg_17, reg_22, reg_26, reg_27;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_1.a, cns446);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, cns447);
  push(reg_9, cns448);
  reg_4 = first(call(reg_8, reg_9));
  reg_16 = lua$get(reg_1.a, cns449);
  reg_17 = newStack();
  push(reg_17, reg_5);
  push(reg_17, cns450);
  push(reg_17, cns451);
  reg_5 = first(call(reg_16, reg_17));
  reg_22 = newStack();
  reg_26 = (reg_4 % reg_5);
  reg_27 = newStack();
  push(reg_27, reg_26);
  append(reg_22, reg_27);
  return reg_22;
}
var cns452 = anyStr("math")
var cns453 = anyStr("modf")
var cns454 = anyStr("getarg")
var cns455 = anyStr("modf")
var cns456 = anyStr("math")
var cns457 = anyStr("tofloat")
function math_modf (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_14, reg_15, reg_16, reg_17, reg_22, reg_23;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns454);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns455);
  reg_4 = first(call(reg_7, reg_8));
  reg_14 = Math.trunc(reg_4);
  reg_15 = newStack();
  push(reg_15, reg_14);
  reg_16 = first(reg_15);
  reg_17 = newStack();
  push(reg_17, reg_16);
  reg_22 = lua$get(lua$get(reg_1.a, cns456), cns457);
  reg_23 = newStack();
  push(reg_23, sub(reg_4, reg_16));
  append(reg_17, call(reg_22, reg_23));
  return reg_17;
}
var cns458 = anyStr("math")
var cns459 = anyStr("exp")
var cns460 = anyStr("getarg")
var cns461 = anyStr("exp")
function math_exp (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns460);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns461);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.exp(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns462 = anyStr("math")
var cns463 = anyStr("sqrt")
var cns464 = anyStr("getarg")
var cns465 = anyStr("sqrt")
function math_sqrt (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns464);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns465);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.sqrt(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns466 = anyStr("math")
var cns467 = anyStr("log")
var cns468 = anyStr("getarg")
var cns469 = anyStr("log")
var cns470 = anyStr("getarg")
var cns471 = anyStr("log")
var cns472 = parseNum("2")
function math_log (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_18, reg_19, reg_24, reg_28, reg_29;
  var goto_20=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_1.a, cns468);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, cns469);
  reg_4 = first(call(reg_8, reg_9));
  goto_20 = !tobool(not(reg_5));
  if (!goto_20) {
    reg_5 = reg_1.b;
  }
  if ((goto_20 || false)) {
    goto_20 = false;
    reg_18 = lua$get(reg_1.a, cns470);
    reg_19 = newStack();
    push(reg_19, reg_5);
    push(reg_19, cns471);
    push(reg_19, cns472);
    reg_5 = first(call(reg_18, reg_19));
  }
  reg_24 = newStack();
  reg_28 = (Math.log(reg_4) / Math.log(reg_5));
  reg_29 = newStack();
  push(reg_29, reg_28);
  append(reg_24, reg_29);
  return reg_24;
}
var cns473 = anyStr("math")
var cns474 = anyStr("sin")
var cns475 = anyStr("getarg")
var cns476 = anyStr("sin")
function math_sin (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns475);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns476);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.sin(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns477 = anyStr("math")
var cns478 = anyStr("cos")
var cns479 = anyStr("getarg")
var cns480 = anyStr("cos")
function math_cos (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns479);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns480);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.cos(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns481 = anyStr("math")
var cns482 = anyStr("tan")
var cns483 = anyStr("getarg")
var cns484 = anyStr("tan")
function math_tan (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns483);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns484);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.tan(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns485 = anyStr("math")
var cns486 = anyStr("asin")
var cns487 = anyStr("getarg")
var cns488 = anyStr("asin")
function math_asin (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns487);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns488);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.sin(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns489 = anyStr("math")
var cns490 = anyStr("acos")
var cns491 = anyStr("getarg")
var cns492 = anyStr("acos")
function math_acos (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_12, reg_15, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns491);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, cns492);
  reg_4 = first(call(reg_7, reg_8));
  reg_12 = newStack();
  reg_15 = Math.cos(reg_4);
  reg_16 = newStack();
  push(reg_16, reg_15);
  append(reg_12, reg_16);
  return reg_12;
}
var cns493 = anyStr("math")
var cns494 = anyStr("atan")
var cns495 = anyStr("getarg")
var cns496 = anyStr("atan")
var cns497 = anyStr("getarg")
var cns498 = anyStr("atan")
var cns499 = parseNum("2")
var cns500 = anyStr("math")
var cns501 = anyStr("tofloat")
var cns502 = parseNum("1")
function math_atan (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_16, reg_17, reg_26, reg_27, reg_31, reg_35, reg_36;
  var goto_29=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_1.a, cns495);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, cns496);
  reg_4 = first(call(reg_8, reg_9));
  goto_29 = !tobool(reg_5);
  if (!goto_29) {
    reg_16 = lua$get(reg_1.a, cns497);
    reg_17 = newStack();
    push(reg_17, reg_5);
    push(reg_17, cns498);
    push(reg_17, cns499);
    reg_5 = first(call(reg_16, reg_17));
  }
  if ((goto_29 || false)) {
    goto_29 = false;
    reg_26 = lua$get(lua$get(reg_1.a, cns500), cns501);
    reg_27 = newStack();
    push(reg_27, cns502);
    reg_5 = first(call(reg_26, reg_27));
  }
  reg_31 = newStack();
  reg_35 = Math.atan2(reg_4, reg_5);
  reg_36 = newStack();
  push(reg_36, reg_35);
  append(reg_31, reg_36);
  return reg_31;
}
function lua_lib_math$lua_main (reg_0) {
  var reg_1, reg_2, reg_4, reg_7, reg_8, reg_9, reg_12, reg_13, reg_14, reg_15, reg_19, reg_20, reg_21, reg_22, reg_24, reg_25, reg_29, reg_30, reg_35, reg_36, reg_41, reg_42, reg_47, reg_48, reg_53, reg_54, reg_59, reg_60, reg_65, reg_66, reg_71, reg_72, reg_77, reg_78, reg_83, reg_84, reg_89, reg_90, reg_95, reg_96, reg_101, reg_102, reg_107, reg_108, reg_113, reg_114, reg_119, reg_120, reg_125, reg_126, reg_131, reg_132, reg_135;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1};
  reg_2.a = reg_0;
  reg_4 = anyFn((function (a) { return lua_lib_math$function(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns405, reg_4);
  reg_7 = Math.PI;
  reg_8 = Math.E;
  reg_9 = Infinity;
  reg_12 = lua$get(reg_2.a, cns406);
  reg_13 = cns407;
  reg_14 = reg_7;
  reg_15 = newStack();
  push(reg_15, reg_14);
  lua$set(reg_12, reg_13, first(reg_15));
  reg_19 = lua$get(reg_2.a, cns408);
  reg_20 = cns409;
  reg_21 = reg_9;
  reg_22 = newStack();
  push(reg_22, reg_21);
  lua$set(reg_19, reg_20, first(reg_22));
  reg_24 = reg_8;
  reg_25 = newStack();
  push(reg_25, reg_24);
  reg_2.b = first(reg_25);
  reg_29 = lua$get(reg_2.a, cns410);
  reg_30 = cns411;
  lua$set(reg_29, reg_30, anyFn((function (a) { return math_deg(a,this) }).bind(reg_2)));
  reg_35 = lua$get(reg_2.a, cns415);
  reg_36 = cns416;
  lua$set(reg_35, reg_36, anyFn((function (a) { return math_rad(a,this) }).bind(reg_2)));
  reg_41 = lua$get(reg_2.a, cns420);
  reg_42 = cns421;
  lua$set(reg_41, reg_42, anyFn((function (a) { return math_max(a,this) }).bind(reg_2)));
  reg_47 = lua$get(reg_2.a, cns424);
  reg_48 = cns425;
  lua$set(reg_47, reg_48, anyFn((function (a) { return math_min(a,this) }).bind(reg_2)));
  reg_53 = lua$get(reg_2.a, cns428);
  reg_54 = cns429;
  lua$set(reg_53, reg_54, anyFn((function (a) { return math_abs(a,this) }).bind(reg_2)));
  reg_59 = lua$get(reg_2.a, cns436);
  reg_60 = cns437;
  lua$set(reg_59, reg_60, anyFn((function (a) { return math_ceil(a,this) }).bind(reg_2)));
  reg_65 = lua$get(reg_2.a, cns440);
  reg_66 = cns441;
  lua$set(reg_65, reg_66, anyFn((function (a) { return math_floor(a,this) }).bind(reg_2)));
  reg_71 = lua$get(reg_2.a, cns444);
  reg_72 = cns445;
  lua$set(reg_71, reg_72, anyFn((function (a) { return math_fmod(a,this) }).bind(reg_2)));
  reg_77 = lua$get(reg_2.a, cns452);
  reg_78 = cns453;
  lua$set(reg_77, reg_78, anyFn((function (a) { return math_modf(a,this) }).bind(reg_2)));
  reg_83 = lua$get(reg_2.a, cns458);
  reg_84 = cns459;
  lua$set(reg_83, reg_84, anyFn((function (a) { return math_exp(a,this) }).bind(reg_2)));
  reg_89 = lua$get(reg_2.a, cns462);
  reg_90 = cns463;
  lua$set(reg_89, reg_90, anyFn((function (a) { return math_sqrt(a,this) }).bind(reg_2)));
  reg_95 = lua$get(reg_2.a, cns466);
  reg_96 = cns467;
  lua$set(reg_95, reg_96, anyFn((function (a) { return math_log(a,this) }).bind(reg_2)));
  reg_101 = lua$get(reg_2.a, cns473);
  reg_102 = cns474;
  lua$set(reg_101, reg_102, anyFn((function (a) { return math_sin(a,this) }).bind(reg_2)));
  reg_107 = lua$get(reg_2.a, cns477);
  reg_108 = cns478;
  lua$set(reg_107, reg_108, anyFn((function (a) { return math_cos(a,this) }).bind(reg_2)));
  reg_113 = lua$get(reg_2.a, cns481);
  reg_114 = cns482;
  lua$set(reg_113, reg_114, anyFn((function (a) { return math_tan(a,this) }).bind(reg_2)));
  reg_119 = lua$get(reg_2.a, cns485);
  reg_120 = cns486;
  lua$set(reg_119, reg_120, anyFn((function (a) { return math_asin(a,this) }).bind(reg_2)));
  reg_125 = lua$get(reg_2.a, cns489);
  reg_126 = cns490;
  lua$set(reg_125, reg_126, anyFn((function (a) { return math_acos(a,this) }).bind(reg_2)));
  reg_131 = lua$get(reg_2.a, cns493);
  reg_132 = cns494;
  lua$set(reg_131, reg_132, anyFn((function (a) { return math_atan(a,this) }).bind(reg_2)));
  reg_135 = newStack();
  return reg_135;
}
Auro.args = typeof process == "undefined" ? [] : process.argv.slice(1);
function get_global () {
  var reg_3, reg_6, reg_11, reg_15, reg_19, reg_23, reg_27, reg_31, reg_35, reg_39, reg_43, reg_47, reg_51, reg_55, reg_58, reg_63, reg_67, reg_75, reg_77, reg_82, reg_84, reg_88, reg_90, reg_94, reg_96, reg_100, reg_102, reg_106, reg_108, reg_112, reg_117, reg_119, reg_123, reg_125, reg_129, reg_131, reg_142, reg_147, reg_151, reg_153, reg_158, reg_160, reg_164, reg_166, reg_170, reg_172, reg_176, reg_178, reg_182, reg_184, reg_187, reg_192, reg_195, reg_200, reg_204, reg_208, reg_215, reg_216, reg_220, reg_224, reg_229;
  if (!cns2.a) {
    reg_3 = true;
    cns2.a = reg_3;
    reg_6 = cns2.b;
    set(reg_6, anyStr("_G"), anyTable(reg_6));
    reg_11 = anyStr("_VERSION");
    set(reg_6, reg_11, anyStr("Lua 5.3"));
    reg_15 = anyStr("_AU_VERSION");
    set(reg_6, reg_15, anyStr("0.6"));
    reg_19 = anyStr("assert");
    set(reg_6, reg_19, anyFn(_assert));
    reg_23 = anyStr("error");
    set(reg_6, reg_23, anyFn(_error));
    reg_27 = anyStr("getmetatable");
    set(reg_6, reg_27, anyFn(_getmeta));
    reg_31 = anyStr("next");
    set(reg_6, reg_31, anyFn(_next));
    reg_35 = anyStr("print");
    set(reg_6, reg_35, anyFn(_print));
    reg_39 = anyStr("select");
    set(reg_6, reg_39, anyFn(_select));
    reg_43 = anyStr("setmetatable");
    set(reg_6, reg_43, anyFn(_setmeta));
    reg_47 = anyStr("tostring");
    set(reg_6, reg_47, anyFn(_tostring));
    reg_51 = anyStr("tonumber");
    set(reg_6, reg_51, anyFn(_tonumber));
    reg_55 = anyStr("type");
    set(reg_6, reg_55, anyFn(_type));
    reg_58 = emptyTable();
    set(reg_6, anyStr("table"), anyTable(reg_58));
    reg_63 = anyStr("pack");
    set(reg_58, reg_63, anyFn(_pack));
    reg_67 = anyStr("unpack");
    set(reg_58, reg_67, anyFn(_unpack));
    reg_73 = lua_main(anyTable(cns2.b));
    reg_75 = cns2.d;
    reg_77 = anyStr("__index");
    set(reg_75, reg_77, anyTable(cns2.c));
    reg_82 = cns2.d;
    reg_84 = anyStr("__add");
    set(reg_82, reg_84, new Function$29(_stradd));
    reg_88 = cns2.d;
    reg_90 = anyStr("__sub");
    set(reg_88, reg_90, new Function$29(_strsub));
    reg_94 = cns2.d;
    reg_96 = anyStr("__mul");
    set(reg_94, reg_96, new Function$29(_strmul));
    reg_100 = cns2.d;
    reg_102 = anyStr("__div");
    set(reg_100, reg_102, new Function$29(_strdiv));
    reg_106 = cns2.d;
    reg_108 = anyStr("__unm");
    set(reg_106, reg_108, new Function$29(_strunm));
    reg_112 = anyStr("string");
    set(reg_6, reg_112, anyTable(cns2.c));
    reg_117 = cns2.c;
    reg_119 = anyStr("sub");
    set(reg_117, reg_119, anyFn(_strsubstr));
    reg_123 = cns2.c;
    reg_125 = anyStr("byte");
    set(reg_123, reg_125, anyFn(_strbyte));
    reg_129 = cns2.c;
    reg_131 = anyStr("char");
    set(reg_129, reg_131, anyFn(_strchar));
    reg_137 = lua_lib_string$lua_main(anyTable(cns2.b));
    reg_141 = lua_lib_pattern$lua_main(anyTable(cns2.b));
    reg_142 = emptyTable();
    set(reg_6, "io", new record_18(reg_142));
    reg_147 = "open";
    set(reg_142, reg_147, new Function$29(_open));
    reg_151 = cns2.e;
    reg_153 = "__index";
    set(reg_151, reg_153, new record_18(cns2.e));
    reg_158 = cns2.e;
    reg_160 = "read";
    set(reg_158, reg_160, new Function$29(_read));
    reg_164 = cns2.e;
    reg_166 = "write";
    set(reg_164, reg_166, new Function$29(_write));
    reg_170 = cns2.e;
    reg_172 = "writebytes";
    set(reg_170, reg_172, new Function$29(_writebytes));
    reg_176 = cns2.e;
    reg_178 = "close";
    set(reg_176, reg_178, new Function$29(_close));
    reg_182 = cns2.e;
    reg_184 = "__tostring";
    set(reg_182, reg_184, new Function$29(_filestr));
    reg_187 = emptyTable();
    set(reg_6, "os", new record_18(reg_187));
    reg_192 = "exit";
    set(reg_187, reg_192, new Function$29(_exit));
    reg_195 = emptyTable();
    set(reg_6, "math", new record_18(reg_195));
    reg_200 = "tointeger";
    set(reg_195, reg_200, new Function$29(_tointeger));
    reg_204 = "tofloat";
    set(reg_195, reg_204, new Function$29(_tofloat));
    reg_208 = "type";
    set(reg_195, reg_208, new Function$29(_mathtype));
    reg_214 = lua_lib_math$lua_main(new record_18(cns2.b));
    reg_215 = emptyTable();
    reg_216 = 0;
    loop_1: while ((reg_216 < Auro.args.length)) {
      reg_220 = Auro.args[reg_216];
      set(reg_215, new Integer(reg_216), reg_220);
      reg_216 = (reg_216 + 1);
    }
    reg_224 = new record_18(reg_215);
    set(reg_6, "arg", reg_224);
  }
  reg_229 = anyTable(cns2.b);
  return reg_229;
}
var cns503 = anyStr("setmetatable")
var cns504 = anyStr("__index")
var cns505 = anyStr("setmetatable")
var cns506 = anyStr("__index")
var cns507 = anyStr("Lexer")
var cns508 = anyStr("Lexer")
var cns509 = anyStr("open")
var cns510 = anyStr("Lexer")
var cns511 = anyStr("error")
var cns512 = anyStr("source")
var cns513 = anyStr("sub")
var cns514 = parseNum("1")
var cns515 = parseNum("1")
var cns516 = anyStr("char")
var cns517 = anyStr("char")
var cns518 = anyStr("")
var cns519 = anyStr("eof")
var cns520 = anyStr("line")
var cns521 = parseNum("1")
var cns522 = anyStr("col")
var cns523 = parseNum("1")
var cns524 = anyStr("pos")
var cns525 = anyStr("saved_pos")
function Lexer_open (reg_0, reg_1) {
  var reg_4, reg_13, reg_14, reg_18, reg_25, reg_28, reg_35, reg_38;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  lua$set(lua$get(reg_1.a, cns510), cns511, nil());
  lua$set(reg_1.a, cns512, reg_4);
  reg_13 = lua$get(reg_4, cns513);
  reg_14 = newStack();
  push(reg_14, reg_4);
  push(reg_14, cns514);
  push(reg_14, cns515);
  reg_18 = first(call(reg_13, reg_14));
  lua$set(reg_1.a, cns516, reg_18);
  reg_25 = eq(lua$get(reg_1.a, cns517), cns518);
  lua$set(reg_1.a, cns519, reg_25);
  reg_28 = newTable();
  lua$set(reg_28, cns520, cns521);
  lua$set(reg_28, cns522, cns523);
  lua$set(reg_1.a, cns524, reg_28);
  reg_35 = nil();
  lua$set(reg_1.a, cns525, reg_35);
  reg_38 = newStack();
  return reg_38;
}
var cns526 = anyStr("Lexer")
var cns527 = anyStr("error")
var cns528 = anyStr("pos")
var cns529 = anyStr("line")
var cns530 = anyStr(":")
var cns531 = anyStr("pos")
var cns532 = anyStr("col")
var cns533 = anyStr(":")
var cns534 = anyStr("error")
var cns535 = anyStr("Lexer")
var cns536 = anyStr("error")
function lua_parser_lexer$function (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_13, reg_14, reg_19, reg_27, reg_28, reg_35;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns526);
  reg_8 = cns527;
  reg_13 = lua$get(lua$get(reg_1.a, cns528), cns529);
  reg_14 = cns530;
  reg_19 = lua$get(lua$get(reg_1.a, cns531), cns532);
  lua$set(reg_7, reg_8, concat(reg_13, concat(reg_14, concat(reg_19, concat(cns533, reg_4)))));
  reg_27 = lua$get(reg_1.a, cns534);
  reg_28 = newStack();
  push(reg_28, lua$get(lua$get(reg_1.a, cns535), cns536));
  reg_34 = call(reg_27, reg_28);
  reg_35 = newStack();
  return reg_35;
}
var cns537 = anyStr("err")
var cns538 = anyStr("source")
var cns539 = anyStr("sub")
var cns540 = parseNum("1")
var cns541 = anyStr("source")
var cns542 = anyStr("match")
var cns543 = anyStr("^")
function function$50 (reg_0, reg_1) {
  var reg_4, reg_9, reg_11, reg_12, reg_19, reg_20, reg_23, reg_25, reg_26, reg_30;
  var goto_27=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  goto_27 = !tobool(next(reg_0));
  if (!goto_27) {
    reg_9 = lua$get(reg_1.a, cns538);
    reg_11 = lua$get(reg_9, cns539);
    reg_12 = newStack();
    push(reg_12, reg_9);
    push(reg_12, cns540);
    push(reg_12, lua$length(reg_4));
    if (tobool(eq(first(call(reg_11, reg_12)), reg_4))) {
      reg_19 = newStack();
      push(reg_19, reg_4);
      return reg_19;
    }
  }
  if ((goto_27 || false)) {
    goto_27 = false;
    reg_20 = newStack();
    reg_23 = lua$get(reg_1.a, cns541);
    reg_25 = lua$get(reg_23, cns542);
    reg_26 = newStack();
    push(reg_26, reg_23);
    push(reg_26, concat(cns543, reg_4));
    append(reg_20, call(reg_25, reg_26));
    return reg_20;
  }
  reg_30 = newStack();
  return reg_30;
}
var cns544 = anyStr("check")
var cns545 = anyStr("check")
var cns546 = parseNum("0")
var cns547 = anyStr("find")
var cns548 = anyStr("\n")
var cns549 = anyStr("pos")
var cns550 = anyStr("line")
var cns551 = anyStr("pos")
var cns552 = anyStr("line")
var cns553 = parseNum("1")
var cns554 = parseNum("1")
var cns555 = anyStr("find")
var cns556 = anyStr("\n")
var cns557 = anyStr("sub")
var cns558 = anyStr("pos")
var cns559 = anyStr("col")
var cns560 = parseNum("1")
var cns561 = anyStr("pos")
var cns562 = anyStr("col")
var cns563 = anyStr("pos")
var cns564 = anyStr("col")
var cns565 = anyStr("source")
var cns566 = anyStr("sub")
var cns567 = parseNum("1")
var cns568 = parseNum("1")
var cns569 = anyStr("source")
var cns570 = anyStr("source")
var cns571 = anyStr("")
var cns572 = anyStr("eof")
function function$51 (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_11, reg_12, reg_18, reg_20, reg_21, reg_24, reg_28, reg_29, reg_40, reg_41, reg_45, reg_48, reg_49, reg_59, reg_60, reg_70, reg_72, reg_73, reg_80, reg_89, reg_92;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_1.a, cns545);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, reg_5);
  reg_11 = first(call(reg_8, reg_9));
  reg_12 = reg_11;
  if (tobool(reg_11)) {
    reg_12 = gt(lua$length(reg_11), cns546);
  }
  if (tobool(reg_12)) {
    reg_18 = nil();
    reg_20 = lua$get(reg_11, cns547);
    reg_21 = newStack();
    push(reg_21, reg_11);
    push(reg_21, cns548);
    reg_24 = first(call(reg_20, reg_21));
    loop_1: while (tobool(reg_24)) {
      reg_28 = lua$get(reg_1.a, cns549);
      reg_29 = cns550;
      lua$set(reg_28, reg_29, add(lua$get(lua$get(reg_1.a, cns551), cns552), cns553));
      reg_18 = add(reg_24, cns554);
      reg_40 = lua$get(reg_11, cns555);
      reg_41 = newStack();
      push(reg_41, reg_11);
      push(reg_41, cns556);
      push(reg_41, reg_18);
      reg_24 = first(call(reg_40, reg_41));
    }
    reg_45 = reg_11;
    if (tobool(reg_18)) {
      reg_48 = lua$get(reg_11, cns557);
      reg_49 = newStack();
      push(reg_49, reg_11);
      push(reg_49, reg_18);
      reg_45 = first(call(reg_48, reg_49));
      lua$set(lua$get(reg_1.a, cns558), cns559, cns560);
    }
    reg_59 = lua$get(reg_1.a, cns561);
    reg_60 = cns562;
    lua$set(reg_59, reg_60, add(lua$get(lua$get(reg_1.a, cns563), cns564), lua$length(reg_45)));
    reg_70 = lua$get(reg_1.a, cns565);
    reg_72 = lua$get(reg_70, cns566);
    reg_73 = newStack();
    push(reg_73, reg_70);
    push(reg_73, add(lua$length(reg_11), cns567));
    push(reg_73, unm(cns568));
    reg_80 = first(call(reg_72, reg_73));
    lua$set(reg_1.a, cns569, reg_80);
    if (tobool(eq(lua$get(reg_1.a, cns570), cns571))) {
      reg_89 = lua$true();
      lua$set(reg_1.a, cns572, reg_89);
    }
  }
  reg_92 = newStack();
  push(reg_92, reg_11);
  return reg_92;
}
var cns573 = anyStr("match")
var cns574 = anyStr("match")
var cns575 = anyStr("\n\x0d")
var cns576 = anyStr("match")
var cns577 = anyStr("\x0d\n")
var cns578 = anyStr("match")
var cns579 = anyStr("[\n\x0d]")
function function$52 (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_11, reg_15, reg_16, reg_23, reg_24;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newStack();
  reg_7 = lua$get(reg_1.a, cns574);
  reg_8 = newStack();
  push(reg_8, cns575);
  reg_11 = first(call(reg_7, reg_8));
  if (!tobool(reg_11)) {
    reg_15 = lua$get(reg_1.a, cns576);
    reg_16 = newStack();
    push(reg_16, cns577);
    reg_11 = first(call(reg_15, reg_16));
  }
  if (!tobool(reg_11)) {
    reg_23 = lua$get(reg_1.a, cns578);
    reg_24 = newStack();
    push(reg_24, cns579);
    reg_11 = first(call(reg_23, reg_24));
  }
  push(reg_4, reg_11);
  return reg_4;
}
var cns580 = anyStr("match_nl")
var cns581 = anyStr("type")
var cns582 = anyStr("value")
var cns583 = anyStr("line")
var cns584 = anyStr("saved_pos")
var cns585 = anyStr("line")
var cns586 = anyStr("column")
var cns587 = anyStr("saved_pos")
var cns588 = anyStr("col")
function function$53 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_16;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = newTable();
  lua$set(reg_7, cns581, reg_4);
  lua$set(reg_7, cns582, reg_5);
  reg_10 = cns583;
  lua$set(reg_7, reg_10, lua$get(lua$get(reg_1.a, cns584), cns585));
  reg_16 = cns586;
  lua$set(reg_7, reg_16, lua$get(lua$get(reg_1.a, cns587), cns588));
  push(reg_6, reg_7);
  return reg_6;
}
var cns589 = anyStr("TK")
var cns590 = anyStr("  | and break do else elseif end false for function goto if in |\n  | local nil not or repeat return then true until while |\n")
var cns591 = parseNum("1")
var cns592 = anyStr("==")
var cns593 = parseNum("2")
var cns594 = anyStr("~=")
var cns595 = parseNum("3")
var cns596 = anyStr("<=")
var cns597 = parseNum("4")
var cns598 = anyStr(">=")
var cns599 = parseNum("5")
var cns600 = anyStr("<<")
var cns601 = parseNum("6")
var cns602 = anyStr(">>")
var cns603 = parseNum("7")
var cns604 = anyStr("=")
var cns605 = parseNum("8")
var cns606 = anyStr("&")
var cns607 = parseNum("9")
var cns608 = anyStr("~")
var cns609 = parseNum("10")
var cns610 = anyStr("|")
var cns611 = parseNum("11")
var cns612 = anyStr("<")
var cns613 = parseNum("12")
var cns614 = anyStr(">")
var cns615 = parseNum("13")
var cns616 = anyStr("//")
var cns617 = parseNum("14")
var cns618 = anyStr("+")
var cns619 = parseNum("15")
var cns620 = anyStr("-")
var cns621 = parseNum("16")
var cns622 = anyStr("*")
var cns623 = parseNum("17")
var cns624 = anyStr("/")
var cns625 = parseNum("18")
var cns626 = anyStr("%")
var cns627 = parseNum("19")
var cns628 = anyStr("^")
var cns629 = parseNum("20")
var cns630 = anyStr("#")
var cns631 = parseNum("21")
var cns632 = anyStr("(")
var cns633 = parseNum("22")
var cns634 = anyStr(")")
var cns635 = parseNum("23")
var cns636 = anyStr("{")
var cns637 = parseNum("24")
var cns638 = anyStr("}")
var cns639 = parseNum("25")
var cns640 = anyStr("[")
var cns641 = parseNum("26")
var cns642 = anyStr("]")
var cns643 = parseNum("27")
var cns644 = anyStr("::")
var cns645 = parseNum("28")
var cns646 = anyStr(";")
var cns647 = parseNum("29")
var cns648 = anyStr(":")
var cns649 = parseNum("30")
var cns650 = anyStr(",")
var cns651 = parseNum("31")
var cns652 = anyStr("...")
var cns653 = parseNum("32")
var cns654 = anyStr("..")
var cns655 = parseNum("33")
var cns656 = anyStr(".")
var cns657 = anyStr("match")
var cns658 = anyStr("%[%[")
var cns659 = anyStr("")
var cns660 = anyStr("check")
var cns661 = anyStr("%[=+")
var cns662 = anyStr("match")
var cns663 = anyStr("%[")
var cns664 = anyStr("match")
var cns665 = anyStr("=+")
var cns666 = anyStr("match")
var cns667 = anyStr("%[")
var cns668 = anyStr("err")
var cns669 = anyStr("invalid long string delimiter")
var cns670 = anyStr("]")
var cns671 = anyStr("]")
var cns672 = anyStr("match_nl")
var cns673 = anyStr("")
var cns674 = anyStr("str")
var cns675 = anyStr("eof")
var cns676 = anyStr("match")
var cns677 = anyStr("%]=*%]?")
var cns678 = anyStr("str")
var cns679 = anyStr("str")
var cns680 = anyStr("str")
var cns681 = anyStr("str")
var cns682 = anyStr("match")
var cns683 = anyStr("[^%]]*")
var cns684 = anyStr("str")
var cns685 = anyStr("err")
var cns686 = anyStr("unfinished long string")
function function$54 (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_16, reg_17, reg_24, reg_25, reg_30, reg_31, reg_37, reg_38, reg_46, reg_47, reg_50, reg_52, reg_55, reg_61, reg_71, reg_72, reg_75, reg_79, reg_86, reg_91, reg_94, reg_95, reg_99, reg_104, reg_105, reg_108;
  var goto_16=false, goto_62=false, goto_105=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = nil();
  reg_7 = lua$get(reg_1.a, cns657);
  reg_8 = newStack();
  push(reg_8, cns658);
  goto_16 = !tobool(first(call(reg_7, reg_8)));
  if (!goto_16) {
    reg_4 = cns659;
  }
  if ((goto_16 || false)) {
    goto_16 = false;
    reg_16 = lua$get(reg_1.a, cns660);
    reg_17 = newStack();
    push(reg_17, cns661);
    goto_62 = !tobool(first(call(reg_16, reg_17)));
    if (!goto_62) {
      reg_24 = lua$get(reg_1.a, cns662);
      reg_25 = newStack();
      push(reg_25, cns663);
      reg_27 = call(reg_24, reg_25);
      reg_30 = lua$get(reg_1.a, cns664);
      reg_31 = newStack();
      push(reg_31, cns665);
      reg_4 = first(call(reg_30, reg_31));
      reg_37 = lua$get(reg_1.a, cns666);
      reg_38 = newStack();
      push(reg_38, cns667);
      if (tobool(not(first(call(reg_37, reg_38))))) {
        reg_46 = lua$get(reg_1.a, cns668);
        reg_47 = newStack();
        push(reg_47, cns669);
        reg_49 = call(reg_46, reg_47);
      }
    }
    if ((goto_62 || false)) {
      goto_62 = false;
      reg_50 = newStack();
      push(reg_50, nil());
      return reg_50;
    }
  }
  reg_52 = cns670;
  reg_55 = concat(reg_52, concat(reg_4, cns671));
  reg_60 = call(lua$get(reg_1.a, cns672), newStack());
  reg_61 = cns673;
  lua$set(reg_1.a, cns674, reg_61);
  loop_1: while (tobool(not(lua$get(reg_1.a, cns675)))) {
    reg_71 = lua$get(reg_1.a, cns676);
    reg_72 = newStack();
    push(reg_72, cns677);
    reg_75 = first(call(reg_71, reg_72));
    if (tobool(reg_75)) {
      goto_105 = !tobool(eq(reg_75, reg_55));
      if (!goto_105) {
        reg_79 = newStack();
        push(reg_79, lua$get(reg_1.a, cns678));
        return reg_79;
      }
      if ((goto_105 || false)) {
        goto_105 = false;
        reg_86 = concat(lua$get(reg_1.a, cns679), reg_75);
        lua$set(reg_1.a, cns680, reg_86);
      }
    }
    reg_91 = lua$get(reg_1.a, cns681);
    reg_94 = lua$get(reg_1.a, cns682);
    reg_95 = newStack();
    push(reg_95, cns683);
    reg_99 = concat(reg_91, first(call(reg_94, reg_95)));
    lua$set(reg_1.a, cns684, reg_99);
  }
  reg_104 = lua$get(reg_1.a, cns685);
  reg_105 = newStack();
  push(reg_105, cns686);
  reg_107 = call(reg_104, reg_105);
  reg_108 = newStack();
  return reg_108;
}
var cns687 = anyStr("long_string")
var cns688 = anyStr("check")
var cns689 = anyStr("[a-fA-F%d]")
var cns690 = anyStr("match")
var cns691 = anyStr(".")
var cns692 = anyStr("lower")
var cns693 = anyStr("a")
var cns694 = anyStr("byte")
var cns695 = parseNum("97")
var cns696 = parseNum("10")
var cns697 = anyStr("byte")
var cns698 = parseNum("48")
var cns699 = anyStr("err")
var cns700 = anyStr("hexadecimal digit expected")
function function$55 (reg_0, reg_1) {
  var reg_6, reg_7, reg_14, reg_15, reg_18, reg_20, reg_21, reg_23, reg_27, reg_29, reg_30, reg_37, reg_39, reg_40, reg_47, reg_48, reg_51;
  var goto_44=false, goto_56=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = lua$get(reg_1.a, cns688);
  reg_7 = newStack();
  push(reg_7, cns689);
  goto_56 = !tobool(first(call(reg_6, reg_7)));
  if (!goto_56) {
    reg_14 = lua$get(reg_1.a, cns690);
    reg_15 = newStack();
    push(reg_15, cns691);
    reg_18 = first(call(reg_14, reg_15));
    reg_20 = lua$get(reg_18, cns692);
    reg_21 = newStack();
    push(reg_21, reg_18);
    reg_23 = first(call(reg_20, reg_21));
    goto_44 = !tobool(ge(reg_23, cns693));
    if (!goto_44) {
      reg_27 = newStack();
      reg_29 = lua$get(reg_23, cns694);
      reg_30 = newStack();
      push(reg_30, reg_23);
      push(reg_27, add(sub(first(call(reg_29, reg_30)), cns695), cns696));
      return reg_27;
    }
    if ((goto_44 || false)) {
      goto_44 = false;
      reg_37 = newStack();
      reg_39 = lua$get(reg_23, cns697);
      reg_40 = newStack();
      push(reg_40, reg_23);
      push(reg_37, sub(first(call(reg_39, reg_40)), cns698));
      return reg_37;
    }
  }
  if ((goto_56 || false)) {
    goto_56 = false;
    reg_47 = lua$get(reg_1.a, cns699);
    reg_48 = newStack();
    push(reg_48, cns700);
    reg_50 = call(reg_47, reg_48);
  }
  reg_51 = newStack();
  return reg_51;
}
var cns701 = anyStr("get_hex")
var cns702 = anyStr("char")
var cns703 = anyStr("")
var cns704 = anyStr("match")
var cns705 = anyStr("[ \x08\n\x0d\t\x0b]*")
var cns706 = anyStr("match")
var cns707 = anyStr("--")
var cns708 = anyStr("long_string")
var cns709 = anyStr("str")
var cns710 = anyStr("str")
var cns711 = anyStr("match")
var cns712 = anyStr("[^\n\x0d]*")
var cns713 = anyStr("line")
var cns714 = anyStr("pos")
var cns715 = anyStr("line")
var cns716 = anyStr("col")
var cns717 = anyStr("pos")
var cns718 = anyStr("col")
var cns719 = anyStr("saved_pos")
var cns720 = anyStr("eof")
var cns721 = anyStr("long_string")
var cns722 = anyStr("check")
var cns723 = anyStr("['\"]")
var cns724 = anyStr("match")
var cns725 = anyStr(".")
var cns726 = anyStr("")
var cns727 = anyStr("match")
var cns728 = anyStr("eof")
var cns729 = anyStr("check")
var cns730 = anyStr("[\n\x0d]")
var cns731 = anyStr("err")
var cns732 = anyStr("unfinished string")
var cns733 = anyStr("match")
var cns734 = anyStr("\\")
var cns735 = anyStr("a")
var cns736 = anyStr("\x07")
var cns737 = anyStr("b")
var cns738 = anyStr("\x08")
var cns739 = anyStr("f")
var cns740 = anyStr("\x0c")
var cns741 = anyStr("n")
var cns742 = anyStr("\n")
var cns743 = anyStr("r")
var cns744 = anyStr("\x0d")
var cns745 = anyStr("t")
var cns746 = anyStr("\t")
var cns747 = anyStr("v")
var cns748 = anyStr("\x0b")
var cns749 = anyStr("check")
var cns750 = anyStr("[abfnrtv]")
var cns751 = anyStr("match")
var cns752 = anyStr(".")
var cns753 = anyStr("check")
var cns754 = anyStr("['\"\\]")
var cns755 = anyStr("match")
var cns756 = anyStr(".")
var cns757 = anyStr("match_nl")
var cns758 = anyStr("\n")
var cns759 = anyStr("check")
var cns760 = anyStr("%d")
var cns761 = anyStr("tonumber")
var cns762 = anyStr("match")
var cns763 = anyStr("%d%d?%d?")
var cns764 = parseNum("265")
var cns765 = anyStr("err")
var cns766 = anyStr("decimal escape too large")
var cns767 = anyStr("string")
var cns768 = anyStr("char")
var cns769 = anyStr("match")
var cns770 = anyStr("x")
var cns771 = anyStr("get_hex")
var cns772 = parseNum("16")
var cns773 = anyStr("get_hex")
var cns774 = anyStr("string")
var cns775 = anyStr("char")
var cns776 = anyStr("match")
var cns777 = anyStr("u")
var cns778 = anyStr("match")
var cns779 = anyStr("{")
var cns780 = anyStr("err")
var cns781 = anyStr("missing {")
var cns782 = parseNum("0")
var cns783 = anyStr("eof")
var cns784 = anyStr("err")
var cns785 = anyStr("unfinished string")
var cns786 = parseNum("16")
var cns787 = anyStr("get_hex")
var cns788 = parseNum("0x10FFFF")
var cns789 = anyStr("err")
var cns790 = anyStr("UTF-8 value too large")
var cns791 = anyStr("match")
var cns792 = anyStr("}")
var cns793 = anyStr("utf8")
var cns794 = anyStr("char")
var cns795 = anyStr("match")
var cns796 = anyStr("z")
var cns797 = anyStr("match")
var cns798 = anyStr("[ \x08\n\x0d\t\x0b]*")
var cns799 = anyStr("")
var cns800 = anyStr("err")
var cns801 = anyStr("invalid escape sequence")
var cns802 = anyStr("match")
var cns803 = anyStr(".")
var cns804 = anyStr("TK")
var cns805 = anyStr("STR")
var cns806 = anyStr("match")
var cns807 = anyStr("0[xX]")
var cns808 = anyStr("match")
var cns809 = anyStr("[a-fA-F%d]*%.?[a-fA-F%d]*")
var cns810 = anyStr("0x")
var cns811 = anyStr("0x.")
var cns812 = anyStr("err")
var cns813 = anyStr("malformed number")
var cns814 = anyStr("check")
var cns815 = anyStr("[pP]")
var cns816 = anyStr("match")
var cns817 = anyStr("[pP][%+%-]?")
var cns818 = anyStr("check")
var cns819 = anyStr("%d")
var cns820 = anyStr("match")
var cns821 = anyStr("%d+")
var cns822 = anyStr("err")
var cns823 = anyStr("malformed number")
var cns824 = anyStr("TK")
var cns825 = anyStr("NUM")
var cns826 = anyStr("check")
var cns827 = anyStr("%.?%d")
var cns828 = anyStr("match")
var cns829 = anyStr("%d*%.?%d*")
var cns830 = anyStr("check")
var cns831 = anyStr("[eE]")
var cns832 = anyStr("match")
var cns833 = anyStr("[eE][%+%-]?")
var cns834 = anyStr("check")
var cns835 = anyStr("%d")
var cns836 = anyStr("match")
var cns837 = anyStr("%d+")
var cns838 = anyStr("err")
var cns839 = anyStr("malformed number")
var cns840 = anyStr("TK")
var cns841 = anyStr("NUM")
var cns842 = anyStr("match")
var cns843 = anyStr("[_%a][_%w]*")
var cns844 = anyStr("find")
var cns845 = anyStr(" ")
var cns846 = anyStr(" ")
var cns847 = anyStr("TK")
var cns848 = anyStr("TK")
var cns849 = anyStr("NAME")
var cns850 = anyStr("pairs")
var cns851 = anyStr("match")
var cns852 = anyStr("TK")
var cns853 = anyStr("err")
var cns854 = anyStr("Unrecognized token ")
var cns855 = anyStr("check")
var cns856 = anyStr(".")
function function$56 (reg_0, reg_1) {
  var reg_12, reg_13, reg_18, reg_19, reg_30, reg_41, reg_42, reg_45, reg_46, reg_52, reg_64, reg_72, reg_73, reg_77, reg_78, reg_85, reg_86, reg_89, reg_95, reg_96, reg_103, reg_107, reg_108, reg_115, reg_116, reg_121, reg_122, reg_127, reg_142, reg_145, reg_146, reg_153, reg_154, reg_161, reg_162, reg_169, reg_170, reg_184, reg_185, reg_192, reg_193, reg_196, reg_197, reg_201, reg_207, reg_208, reg_215, reg_216, reg_221, reg_222, reg_234, reg_241, reg_246, reg_247, reg_252, reg_253, reg_260, reg_261, reg_269, reg_270, reg_273, reg_280, reg_281, reg_285, reg_298, reg_299, reg_304, reg_305, reg_314, reg_315, reg_320, reg_321, reg_328, reg_329, reg_335, reg_336, reg_342, reg_343, reg_349, reg_352, reg_353, reg_358, reg_359, reg_362, reg_366, reg_367, reg_373, reg_380, reg_381, reg_386, reg_387, reg_394, reg_395, reg_402, reg_403, reg_410, reg_411, reg_418, reg_419, reg_422, reg_425, reg_426, reg_431, reg_432, reg_439, reg_440, reg_446, reg_447, reg_454, reg_455, reg_462, reg_463, reg_470, reg_471, reg_478, reg_479, reg_482, reg_485, reg_486, reg_491, reg_492, reg_495, reg_497, reg_499, reg_500, reg_501, reg_510, reg_513, reg_514, reg_516, reg_519, reg_520, reg_525, reg_526, reg_528, reg_529, reg_530, reg_531, reg_532, reg_533, reg_535, reg_541, reg_542, reg_549, reg_550, reg_552, reg_553, reg_556, reg_557, reg_558, reg_561, reg_562, reg_568;
  var goto_53=false, goto_130=false, goto_154=false, goto_208=false, goto_228=false, goto_239=false, goto_286=false, goto_322=false, goto_408=false, goto_428=false, goto_438=false, goto_501=false, goto_542=false, goto_622=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  loop_4: while (tobool(ne(lua$get(reg_1.a, cns702), cns703))) {
    reg_12 = lua$get(reg_1.a, cns704);
    reg_13 = newStack();
    push(reg_13, cns705);
    reg_15 = call(reg_12, reg_13);
    reg_18 = lua$get(reg_1.a, cns706);
    reg_19 = newStack();
    push(reg_19, cns707);
    push(reg_19, lua$true());
    goto_53 = !tobool(first(call(reg_18, reg_19)));
    if (!goto_53) {
      reg_30 = first(call(lua$get(reg_1.a, cns708), newStack()));
      lua$set(reg_1.a, cns709, reg_30);
      if (tobool(eq(lua$get(reg_1.a, cns710), nil()))) {
        reg_41 = lua$get(reg_1.a, cns711);
        reg_42 = newStack();
        push(reg_42, cns712);
        reg_44 = call(reg_41, reg_42);
      }
    }
    if ((goto_53 || false)) {
      goto_53 = false;
      if (true) break loop_4;
    }
  }
  reg_45 = newTable();
  reg_46 = cns713;
  lua$set(reg_45, reg_46, lua$get(lua$get(reg_1.a, cns714), cns715));
  reg_52 = cns716;
  lua$set(reg_45, reg_52, lua$get(lua$get(reg_1.a, cns717), cns718));
  lua$set(reg_1.a, cns719, reg_45);
  if (tobool(lua$get(reg_1.a, cns720))) {
    reg_64 = newStack();
    push(reg_64, nil());
    return reg_64;
  }
  reg_66 = nil();
  reg_72 = first(call(lua$get(reg_1.a, cns721), newStack()));
  reg_73 = not(reg_72);
  if (tobool(reg_73)) {
    reg_77 = lua$get(reg_1.a, cns722);
    reg_78 = newStack();
    push(reg_78, cns723);
    reg_73 = first(call(reg_77, reg_78));
  }
  if (tobool(reg_73)) {
    reg_85 = lua$get(reg_1.a, cns724);
    reg_86 = newStack();
    push(reg_86, cns725);
    reg_89 = first(call(reg_85, reg_86));
    reg_72 = cns726;
    loop_2: while (tobool(lua$true())) {
      reg_95 = lua$get(reg_1.a, cns727);
      reg_96 = newStack();
      push(reg_96, reg_89);
      push(reg_96, lua$true());
      goto_130 = !tobool(first(call(reg_95, reg_96)));
      if (!goto_130) {
        if (true) break loop_2;
      }
      if ((goto_130 || false)) {
        goto_130 = false;
        reg_103 = lua$get(reg_1.a, cns728);
        if (!tobool(reg_103)) {
          reg_107 = lua$get(reg_1.a, cns729);
          reg_108 = newStack();
          push(reg_108, cns730);
          reg_103 = first(call(reg_107, reg_108));
        }
        goto_154 = !tobool(reg_103);
        if (!goto_154) {
          reg_115 = lua$get(reg_1.a, cns731);
          reg_116 = newStack();
          push(reg_116, cns732);
          reg_118 = call(reg_115, reg_116);
        }
        if ((goto_154 || false)) {
          goto_154 = false;
          reg_121 = lua$get(reg_1.a, cns733);
          reg_122 = newStack();
          push(reg_122, cns734);
          goto_438 = !tobool(first(call(reg_121, reg_122)));
          if (!goto_438) {
            reg_127 = newTable();
            lua$set(reg_127, cns735, cns736);
            lua$set(reg_127, cns737, cns738);
            lua$set(reg_127, cns739, cns740);
            lua$set(reg_127, cns741, cns742);
            lua$set(reg_127, cns743, cns744);
            lua$set(reg_127, cns745, cns746);
            lua$set(reg_127, cns747, cns748);
            reg_142 = nil();
            reg_145 = lua$get(reg_1.a, cns749);
            reg_146 = newStack();
            push(reg_146, cns750);
            goto_208 = !tobool(first(call(reg_145, reg_146)));
            if (!goto_208) {
              reg_153 = lua$get(reg_1.a, cns751);
              reg_154 = newStack();
              push(reg_154, cns752);
              reg_142 = lua$get(reg_127, first(call(reg_153, reg_154)));
            }
            if ((goto_208 || false)) {
              goto_208 = false;
              reg_161 = lua$get(reg_1.a, cns753);
              reg_162 = newStack();
              push(reg_162, cns754);
              goto_228 = !tobool(first(call(reg_161, reg_162)));
              if (!goto_228) {
                reg_169 = lua$get(reg_1.a, cns755);
                reg_170 = newStack();
                push(reg_170, cns756);
                reg_142 = first(call(reg_169, reg_170));
              }
              if ((goto_228 || false)) {
                goto_228 = false;
                goto_239 = !tobool(first(call(lua$get(reg_1.a, cns757), newStack())));
                if (!goto_239) {
                  reg_142 = cns758;
                }
                if ((goto_239 || false)) {
                  goto_239 = false;
                  reg_184 = lua$get(reg_1.a, cns759);
                  reg_185 = newStack();
                  push(reg_185, cns760);
                  goto_286 = !tobool(first(call(reg_184, reg_185)));
                  if (!goto_286) {
                    reg_192 = lua$get(reg_1.a, cns761);
                    reg_193 = newStack();
                    reg_196 = lua$get(reg_1.a, cns762);
                    reg_197 = newStack();
                    push(reg_197, cns763);
                    append(reg_193, call(reg_196, reg_197));
                    reg_201 = first(call(reg_192, reg_193));
                    if (tobool(gt(reg_201, cns764))) {
                      reg_207 = lua$get(reg_1.a, cns765);
                      reg_208 = newStack();
                      push(reg_208, cns766);
                      reg_210 = call(reg_207, reg_208);
                    }
                    reg_215 = lua$get(lua$get(reg_1.a, cns767), cns768);
                    reg_216 = newStack();
                    push(reg_216, reg_201);
                    reg_142 = first(call(reg_215, reg_216));
                  }
                  if ((goto_286 || false)) {
                    goto_286 = false;
                    reg_221 = lua$get(reg_1.a, cns769);
                    reg_222 = newStack();
                    push(reg_222, cns770);
                    goto_322 = !tobool(first(call(reg_221, reg_222)));
                    if (!goto_322) {
                      reg_234 = mul(first(call(lua$get(reg_1.a, cns771), newStack())), cns772);
                      reg_241 = add(reg_234, first(call(lua$get(reg_1.a, cns773), newStack())));
                      reg_246 = lua$get(lua$get(reg_1.a, cns774), cns775);
                      reg_247 = newStack();
                      push(reg_247, reg_241);
                      reg_142 = first(call(reg_246, reg_247));
                    }
                    if ((goto_322 || false)) {
                      goto_322 = false;
                      reg_252 = lua$get(reg_1.a, cns776);
                      reg_253 = newStack();
                      push(reg_253, cns777);
                      goto_408 = !tobool(first(call(reg_252, reg_253)));
                      if (!goto_408) {
                        reg_260 = lua$get(reg_1.a, cns778);
                        reg_261 = newStack();
                        push(reg_261, cns779);
                        if (tobool(not(first(call(reg_260, reg_261))))) {
                          reg_269 = lua$get(reg_1.a, cns780);
                          reg_270 = newStack();
                          push(reg_270, cns781);
                          reg_272 = call(reg_269, reg_270);
                        }
                        reg_273 = cns782;
                        loop_3: while (true) {
                          if (tobool(lua$get(reg_1.a, cns783))) {
                            reg_280 = lua$get(reg_1.a, cns784);
                            reg_281 = newStack();
                            push(reg_281, cns785);
                            reg_283 = call(reg_280, reg_281);
                          }
                          reg_285 = mul(reg_273, cns786);
                          reg_273 = add(reg_285, first(call(lua$get(reg_1.a, cns787), newStack())));
                          if (tobool(gt(reg_273, cns788))) {
                            reg_298 = lua$get(reg_1.a, cns789);
                            reg_299 = newStack();
                            push(reg_299, cns790);
                            reg_301 = call(reg_298, reg_299);
                          }
                          reg_304 = lua$get(reg_1.a, cns791);
                          reg_305 = newStack();
                          push(reg_305, cns792);
                          if (tobool(first(call(reg_304, reg_305)))) break loop_3;
                        }
                        reg_314 = lua$get(lua$get(reg_1.a, cns793), cns794);
                        reg_315 = newStack();
                        push(reg_315, reg_273);
                        reg_142 = first(call(reg_314, reg_315));
                      }
                      if ((goto_408 || false)) {
                        goto_408 = false;
                        reg_320 = lua$get(reg_1.a, cns795);
                        reg_321 = newStack();
                        push(reg_321, cns796);
                        goto_428 = !tobool(first(call(reg_320, reg_321)));
                        if (!goto_428) {
                          reg_328 = lua$get(reg_1.a, cns797);
                          reg_329 = newStack();
                          push(reg_329, cns798);
                          reg_331 = call(reg_328, reg_329);
                          reg_142 = cns799;
                        }
                        if ((goto_428 || false)) {
                          goto_428 = false;
                          reg_335 = lua$get(reg_1.a, cns800);
                          reg_336 = newStack();
                          push(reg_336, cns801);
                          reg_338 = call(reg_335, reg_336);
                        }
                      }
                    }
                  }
                }
              }
            }
            reg_72 = concat(reg_72, reg_142);
          }
          if ((goto_438 || false)) {
            goto_438 = false;
            reg_342 = lua$get(reg_1.a, cns802);
            reg_343 = newStack();
            push(reg_343, cns803);
            reg_72 = concat(reg_72, first(call(reg_342, reg_343)));
          }
        }
      }
    }
  }
  if (tobool(reg_72)) {
    reg_349 = newStack();
    reg_352 = lua$get(reg_1.a, cns804);
    reg_353 = newStack();
    push(reg_353, cns805);
    push(reg_353, reg_72);
    append(reg_349, call(reg_352, reg_353));
    return reg_349;
  }
  reg_358 = lua$get(reg_1.a, cns806);
  reg_359 = newStack();
  push(reg_359, cns807);
  reg_362 = first(call(reg_358, reg_359));
  if (tobool(reg_362)) {
    reg_366 = lua$get(reg_1.a, cns808);
    reg_367 = newStack();
    push(reg_367, cns809);
    reg_362 = concat(reg_362, first(call(reg_366, reg_367)));
    reg_373 = eq(reg_362, cns810);
    if (!tobool(reg_373)) {
      reg_373 = eq(reg_362, cns811);
    }
    goto_501 = !tobool(reg_373);
    if (!goto_501) {
      reg_380 = lua$get(reg_1.a, cns812);
      reg_381 = newStack();
      push(reg_381, cns813);
      reg_383 = call(reg_380, reg_381);
    }
    if ((goto_501 || false)) {
      goto_501 = false;
      reg_386 = lua$get(reg_1.a, cns814);
      reg_387 = newStack();
      push(reg_387, cns815);
      if (tobool(first(call(reg_386, reg_387)))) {
        reg_394 = lua$get(reg_1.a, cns816);
        reg_395 = newStack();
        push(reg_395, cns817);
        reg_362 = concat(reg_362, first(call(reg_394, reg_395)));
        reg_402 = lua$get(reg_1.a, cns818);
        reg_403 = newStack();
        push(reg_403, cns819);
        goto_542 = !tobool(first(call(reg_402, reg_403)));
        if (!goto_542) {
          reg_410 = lua$get(reg_1.a, cns820);
          reg_411 = newStack();
          push(reg_411, cns821);
          reg_362 = concat(reg_362, first(call(reg_410, reg_411)));
        }
        if ((goto_542 || false)) {
          goto_542 = false;
          reg_418 = lua$get(reg_1.a, cns822);
          reg_419 = newStack();
          push(reg_419, cns823);
          reg_421 = call(reg_418, reg_419);
        }
      }
    }
    reg_422 = newStack();
    reg_425 = lua$get(reg_1.a, cns824);
    reg_426 = newStack();
    push(reg_426, cns825);
    push(reg_426, reg_362);
    append(reg_422, call(reg_425, reg_426));
    return reg_422;
  }
  reg_431 = lua$get(reg_1.a, cns826);
  reg_432 = newStack();
  push(reg_432, cns827);
  if (tobool(first(call(reg_431, reg_432)))) {
    reg_439 = lua$get(reg_1.a, cns828);
    reg_440 = newStack();
    push(reg_440, cns829);
    reg_362 = first(call(reg_439, reg_440));
    reg_446 = lua$get(reg_1.a, cns830);
    reg_447 = newStack();
    push(reg_447, cns831);
    if (tobool(first(call(reg_446, reg_447)))) {
      reg_454 = lua$get(reg_1.a, cns832);
      reg_455 = newStack();
      push(reg_455, cns833);
      reg_362 = concat(reg_362, first(call(reg_454, reg_455)));
      reg_462 = lua$get(reg_1.a, cns834);
      reg_463 = newStack();
      push(reg_463, cns835);
      goto_622 = !tobool(first(call(reg_462, reg_463)));
      if (!goto_622) {
        reg_470 = lua$get(reg_1.a, cns836);
        reg_471 = newStack();
        push(reg_471, cns837);
        reg_362 = concat(reg_362, first(call(reg_470, reg_471)));
      }
      if ((goto_622 || false)) {
        goto_622 = false;
        reg_478 = lua$get(reg_1.a, cns838);
        reg_479 = newStack();
        push(reg_479, cns839);
        reg_481 = call(reg_478, reg_479);
      }
    }
    reg_482 = newStack();
    reg_485 = lua$get(reg_1.a, cns840);
    reg_486 = newStack();
    push(reg_486, cns841);
    push(reg_486, reg_362);
    append(reg_482, call(reg_485, reg_486));
    return reg_482;
  }
  reg_491 = lua$get(reg_1.a, cns842);
  reg_492 = newStack();
  push(reg_492, cns843);
  reg_495 = first(call(reg_491, reg_492));
  if (tobool(reg_495)) {
    reg_497 = reg_1.b;
    reg_499 = lua$get(reg_497, cns844);
    reg_500 = newStack();
    push(reg_500, reg_497);
    reg_501 = cns845;
    push(reg_500, concat(reg_501, concat(reg_495, cns846)));
    if (tobool(ne(first(call(reg_499, reg_500)), nil()))) {
      reg_510 = newStack();
      reg_513 = lua$get(reg_1.a, cns847);
      reg_514 = newStack();
      push(reg_514, reg_495);
      append(reg_510, call(reg_513, reg_514));
      return reg_510;
    }
    reg_516 = newStack();
    reg_519 = lua$get(reg_1.a, cns848);
    reg_520 = newStack();
    push(reg_520, cns849);
    push(reg_520, reg_495);
    append(reg_516, call(reg_519, reg_520));
    return reg_516;
  }
  reg_525 = lua$get(reg_1.a, cns850);
  reg_526 = newStack();
  push(reg_526, reg_1.c);
  reg_528 = call(reg_525, reg_526);
  reg_529 = next(reg_528);
  reg_530 = next(reg_528);
  reg_531 = next(reg_528);
  loop_1: while (true) {
    reg_532 = newStack();
    push(reg_532, reg_530);
    push(reg_532, reg_531);
    reg_533 = call(reg_529, reg_532);
    reg_531 = next(reg_533);
    reg_535 = next(reg_533);
    if (tobool(eq(reg_531, nil()))) break loop_1;
    reg_541 = lua$get(reg_1.a, cns851);
    reg_542 = newStack();
    push(reg_542, reg_535);
    push(reg_542, lua$true());
    if (tobool(first(call(reg_541, reg_542)))) {
      reg_549 = lua$get(reg_1.a, cns852);
      reg_550 = newStack();
      push(reg_550, reg_535);
      reg_552 = first(call(reg_549, reg_550));
      reg_553 = newStack();
      push(reg_553, reg_552);
      return reg_553;
    }
  }
  reg_556 = lua$get(reg_1.a, cns853);
  reg_557 = newStack();
  reg_558 = cns854;
  reg_561 = lua$get(reg_1.a, cns855);
  reg_562 = newStack();
  push(reg_562, cns856);
  push(reg_557, concat(reg_558, first(call(reg_561, reg_562))));
  reg_567 = call(reg_556, reg_557);
  reg_568 = newStack();
  return reg_568;
}
var cns857 = anyStr("lex")
var cns858 = anyStr("Lexer")
var cns859 = anyStr("next")
var cns860 = anyStr("xpcall")
function function$57 (reg_0, reg_1) {
  var reg_5, reg_6;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  push(reg_6, lua$true());
  append(reg_6, call(reg_5, newStack()));
  return reg_6;
}
var cns861 = anyStr("xpcall")
var cns862 = anyStr("xpcall")
var cns863 = anyStr("lex")
var cns864 = anyStr("Lexer")
var cns865 = anyStr("error")
var cns866 = anyStr("debug")
var cns867 = anyStr("traceback")
var cns868 = anyStr("\x1b[31mFATAL\x1b[39m ")
function function$58 (reg_0, reg_1) {
  var reg_2, reg_5, reg_18, reg_19, reg_24;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  if (tobool(eq(lua$get(lua$get(reg_2.a, cns864), cns865), nil()))) {
    reg_18 = lua$get(lua$get(reg_2.a, cns866), cns867);
    reg_19 = newStack();
    push(reg_19, concat(cns868, reg_5));
    reg_1.b = first(call(reg_18, reg_19));
  }
  reg_24 = newStack();
  return reg_24;
}
var cns869 = anyStr("status")
var cns870 = anyStr("tk")
var cns871 = anyStr("error")
var cns872 = anyStr("status")
var cns873 = anyStr("tk")
var cns874 = anyStr("Lexer")
var cns875 = anyStr("error")
var cns876 = anyStr("tk")
var cns877 = anyStr("error")
var cns878 = anyStr("tk")
function Lexer_next (reg_0, reg_1) {
  var reg_3, reg_11, reg_16, reg_17, reg_23, reg_24, reg_27, reg_34, reg_35, reg_42, reg_52, reg_59, reg_60, reg_65;
  var goto_59=false, goto_75=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_3.b = nil();
  if (tobool(not(lua$get(reg_1.a, cns860)))) {
    reg_11 = anyFn((function (a) { return function$57(a,this) }).bind(reg_3));
    lua$set(reg_1.a, cns861, reg_11);
  }
  reg_16 = lua$get(reg_1.a, cns862);
  reg_17 = newStack();
  push(reg_17, lua$get(reg_1.a, cns863));
  push(reg_17, anyFn((function (a) { return function$58(a,this) }).bind(reg_3)));
  reg_23 = call(reg_16, reg_17);
  reg_24 = next(reg_23);
  lua$set(reg_1.a, cns869, reg_24);
  reg_27 = next(reg_23);
  lua$set(reg_1.a, cns870, reg_27);
  if (tobool(reg_3.b)) {
    reg_34 = lua$get(reg_1.a, cns871);
    reg_35 = newStack();
    push(reg_35, reg_3.b);
    reg_37 = call(reg_34, reg_35);
  }
  goto_59 = !tobool(lua$get(reg_1.a, cns872));
  if (!goto_59) {
    reg_42 = newStack();
    push(reg_42, lua$get(reg_1.a, cns873));
    return reg_42;
  }
  if ((goto_59 || false)) {
    goto_59 = false;
    goto_75 = !tobool(lua$get(lua$get(reg_1.a, cns874), cns875));
    if (!goto_75) {
      reg_52 = newStack();
      push(reg_52, nil());
      push(reg_52, lua$get(reg_1.a, cns876));
      return reg_52;
    }
    if ((goto_75 || false)) {
      goto_75 = false;
      reg_59 = lua$get(reg_1.a, cns877);
      reg_60 = newStack();
      push(reg_60, lua$get(reg_1.a, cns878));
      reg_64 = call(reg_59, reg_60);
    }
  }
  reg_65 = newStack();
  return reg_65;
}
var cns879 = anyStr("Lexer")
var cns880 = anyStr("tokens")
var cns881 = anyStr("Lexer")
var cns882 = anyStr("next")
var cns883 = anyStr("table")
var cns884 = anyStr("insert")
var cns885 = anyStr("Lexer")
var cns886 = anyStr("next")
var cns887 = anyStr("Lexer")
var cns888 = anyStr("error")
function Lexer_tokens (reg_0, reg_1) {
  var reg_4, reg_12, reg_20, reg_21, reg_38, reg_39;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newTable();
  reg_12 = first(call(lua$get(lua$get(reg_1.a, cns881), cns882), newStack()));
  loop_1: while (tobool(ne(reg_12, nil()))) {
    reg_20 = lua$get(lua$get(reg_1.a, cns883), cns884);
    reg_21 = newStack();
    push(reg_21, reg_4);
    push(reg_21, reg_12);
    reg_22 = call(reg_20, reg_21);
    reg_12 = first(call(lua$get(lua$get(reg_1.a, cns885), cns886), newStack()));
  }
  if (tobool(not(lua$get(lua$get(reg_1.a, cns887), cns888)))) {
    reg_38 = newStack();
    push(reg_38, reg_4);
    return reg_38;
  }
  reg_39 = newStack();
  return reg_39;
}
var cns889 = anyStr("Lexer")
function lua_parser_lexer$lua_main (reg_0) {
  var reg_1, reg_2, reg_5, reg_6, reg_8, reg_13, reg_18, reg_19, reg_23, reg_27, reg_31, reg_35, reg_39, reg_43, reg_111, reg_115, reg_119, reg_124, reg_125, reg_130, reg_131, reg_134;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1, c: reg_1};
  reg_2.a = reg_0;
  reg_5 = lua$get(reg_2.a, cns505);
  reg_6 = newStack();
  push(reg_6, newTable());
  reg_8 = newTable();
  lua$set(reg_8, cns506, reg_2.a);
  push(reg_6, reg_8);
  reg_2.a = first(call(reg_5, reg_6));
  reg_13 = newTable();
  lua$set(reg_2.a, cns507, reg_13);
  reg_18 = lua$get(reg_2.a, cns508);
  reg_19 = cns509;
  lua$set(reg_18, reg_19, anyFn((function (a) { return Lexer_open(a,this) }).bind(reg_2)));
  reg_23 = anyFn((function (a) { return lua_parser_lexer$function(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns537, reg_23);
  reg_27 = anyFn((function (a) { return function$50(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns544, reg_27);
  reg_31 = anyFn((function (a) { return function$51(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns573, reg_31);
  reg_35 = anyFn((function (a) { return function$52(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns580, reg_35);
  reg_39 = anyFn((function (a) { return function$53(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns589, reg_39);
  reg_2.b = cns590;
  reg_43 = newTable();
  lua$set(reg_43, cns591, cns592);
  lua$set(reg_43, cns593, cns594);
  lua$set(reg_43, cns595, cns596);
  lua$set(reg_43, cns597, cns598);
  lua$set(reg_43, cns599, cns600);
  lua$set(reg_43, cns601, cns602);
  lua$set(reg_43, cns603, cns604);
  lua$set(reg_43, cns605, cns606);
  lua$set(reg_43, cns607, cns608);
  lua$set(reg_43, cns609, cns610);
  lua$set(reg_43, cns611, cns612);
  lua$set(reg_43, cns613, cns614);
  lua$set(reg_43, cns615, cns616);
  lua$set(reg_43, cns617, cns618);
  lua$set(reg_43, cns619, cns620);
  lua$set(reg_43, cns621, cns622);
  lua$set(reg_43, cns623, cns624);
  lua$set(reg_43, cns625, cns626);
  lua$set(reg_43, cns627, cns628);
  lua$set(reg_43, cns629, cns630);
  lua$set(reg_43, cns631, cns632);
  lua$set(reg_43, cns633, cns634);
  lua$set(reg_43, cns635, cns636);
  lua$set(reg_43, cns637, cns638);
  lua$set(reg_43, cns639, cns640);
  lua$set(reg_43, cns641, cns642);
  lua$set(reg_43, cns643, cns644);
  lua$set(reg_43, cns645, cns646);
  lua$set(reg_43, cns647, cns648);
  lua$set(reg_43, cns649, cns650);
  lua$set(reg_43, cns651, cns652);
  lua$set(reg_43, cns653, cns654);
  lua$set(reg_43, cns655, cns656);
  reg_2.c = reg_43;
  reg_111 = anyFn((function (a) { return function$54(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns687, reg_111);
  reg_115 = anyFn((function (a) { return function$55(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns701, reg_115);
  reg_119 = anyFn((function (a) { return function$56(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns857, reg_119);
  reg_124 = lua$get(reg_2.a, cns858);
  reg_125 = cns859;
  lua$set(reg_124, reg_125, anyFn((function (a) { return Lexer_next(a,this) }).bind(reg_2)));
  reg_130 = lua$get(reg_2.a, cns879);
  reg_131 = cns880;
  lua$set(reg_130, reg_131, anyFn((function (a) { return Lexer_tokens(a,this) }).bind(reg_2)));
  reg_134 = newStack();
  push(reg_134, lua$get(reg_2.a, cns889));
  return reg_134;
}
var cns890 = anyStr("Lexer")
var cns891 = anyStr("Parser")
var cns892 = anyStr("Parser")
var cns893 = anyStr("open")
var cns894 = anyStr("Lexer")
var cns895 = anyStr("open")
var cns896 = anyStr("Lexer")
var cns897 = anyStr("next")
var cns898 = anyStr("token")
var cns899 = anyStr("Parser")
var cns900 = anyStr("error")
function Parser_open (reg_0, reg_1) {
  var reg_4, reg_9, reg_10, reg_14, reg_16, reg_17, reg_19, reg_27;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_9 = lua$get(lua$get(reg_1.a, cns894), cns895);
  reg_10 = newStack();
  push(reg_10, reg_4);
  reg_11 = call(reg_9, reg_10);
  reg_14 = lua$get(reg_1.a, cns896);
  reg_16 = lua$get(reg_14, cns897);
  reg_17 = newStack();
  push(reg_17, reg_14);
  reg_19 = first(call(reg_16, reg_17));
  lua$set(reg_1.a, cns898, reg_19);
  lua$set(lua$get(reg_1.a, cns899), cns900, nil());
  reg_27 = newStack();
  return reg_27;
}
var cns901 = anyStr("<eof>")
var cns902 = anyStr("token")
var cns903 = anyStr("token")
var cns904 = anyStr("line")
var cns905 = anyStr(":")
var cns906 = anyStr("token")
var cns907 = anyStr("column")
var cns908 = anyStr("Parser")
var cns909 = anyStr("error")
var cns910 = anyStr(": ")
var cns911 = anyStr("error")
var cns912 = anyStr("Parser")
var cns913 = anyStr("error")
function lua_parser_parser$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_14, reg_15, reg_25, reg_26, reg_32, reg_33, reg_40;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = cns901;
  if (tobool(lua$get(reg_1.a, cns902))) {
    reg_14 = lua$get(lua$get(reg_1.a, cns903), cns904);
    reg_15 = cns905;
    reg_5 = concat(reg_14, concat(reg_15, lua$get(lua$get(reg_1.a, cns906), cns907)));
  }
  reg_25 = lua$get(reg_1.a, cns908);
  reg_26 = cns909;
  lua$set(reg_25, reg_26, concat(reg_5, concat(cns910, reg_4)));
  reg_32 = lua$get(reg_1.a, cns911);
  reg_33 = newStack();
  push(reg_33, lua$get(lua$get(reg_1.a, cns912), cns913));
  reg_39 = call(reg_32, reg_33);
  reg_40 = newStack();
  return reg_40;
}
var cns914 = anyStr("err")
var cns915 = anyStr("token")
var cns916 = anyStr("_lookahead")
var cns917 = anyStr("Lexer")
var cns918 = anyStr("next")
var cns919 = anyStr("token")
var cns920 = anyStr("_lookahead")
var cns921 = anyStr("Lexer")
var cns922 = anyStr("error")
var cns923 = anyStr("Parser")
var cns924 = anyStr("error")
var cns925 = anyStr("Lexer")
var cns926 = anyStr("error")
var cns927 = anyStr("error")
var cns928 = anyStr("Parser")
var cns929 = anyStr("error")
function function$59 (reg_0, reg_1) {
  var reg_6, reg_9, reg_21, reg_32, reg_33, reg_41, reg_42, reg_49;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = lua$get(reg_1.a, cns915);
  reg_9 = lua$get(reg_1.a, cns916);
  if (!tobool(reg_9)) {
    reg_9 = first(call(lua$get(lua$get(reg_1.a, cns917), cns918), newStack()));
  }
  lua$set(reg_1.a, cns919, reg_9);
  reg_21 = nil();
  lua$set(reg_1.a, cns920, reg_21);
  if (tobool(lua$get(lua$get(reg_1.a, cns921), cns922))) {
    reg_32 = lua$get(reg_1.a, cns923);
    reg_33 = cns924;
    lua$set(reg_32, reg_33, lua$get(lua$get(reg_1.a, cns925), cns926));
    reg_41 = lua$get(reg_1.a, cns927);
    reg_42 = newStack();
    push(reg_42, lua$get(lua$get(reg_1.a, cns928), cns929));
    reg_48 = call(reg_41, reg_42);
  }
  reg_49 = newStack();
  push(reg_49, reg_6);
  return reg_49;
}
var cns930 = anyStr("next")
var cns931 = anyStr("token")
var cns932 = parseNum("1")
var cns933 = anyStr("ipairs")
var cns934 = anyStr("token")
var cns935 = anyStr("type")
function function$60 (reg_0, reg_1) {
  var reg_9, reg_11, reg_16, reg_17, reg_18, reg_19, reg_20, reg_21, reg_22, reg_23, reg_25, reg_36, reg_38;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  if (tobool(not(lua$get(reg_1.a, cns931)))) {
    reg_9 = newStack();
    push(reg_9, lua$false());
    return reg_9;
  }
  reg_11 = newTable();
  table_append(reg_11, cns932, copy(reg_0));
  reg_16 = lua$get(reg_1.a, cns933);
  reg_17 = newStack();
  push(reg_17, reg_11);
  reg_18 = call(reg_16, reg_17);
  reg_19 = next(reg_18);
  reg_20 = next(reg_18);
  reg_21 = next(reg_18);
  loop_1: while (true) {
    reg_22 = newStack();
    push(reg_22, reg_20);
    push(reg_22, reg_21);
    reg_23 = call(reg_19, reg_22);
    reg_21 = next(reg_23);
    reg_25 = next(reg_23);
    if (tobool(eq(reg_21, nil()))) break loop_1;
    if (tobool(eq(lua$get(lua$get(reg_1.a, cns934), cns935), reg_25))) {
      reg_36 = newStack();
      push(reg_36, lua$true());
      return reg_36;
    }
  }
  reg_38 = newStack();
  push(reg_38, lua$false());
  return reg_38;
}
var cns936 = anyStr("check")
var cns937 = anyStr("token")
var cns938 = anyStr("check")
function function$61 (reg_0, reg_1) {
  var reg_4, reg_7, reg_11, reg_12;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newStack();
  reg_7 = lua$get(reg_1.a, cns937);
  if (tobool(reg_7)) {
    reg_11 = lua$get(reg_1.a, cns938);
    reg_12 = newStack();
    append(reg_12, reg_0);
    reg_7 = not(first(call(reg_11, reg_12)));
  }
  push(reg_4, reg_7);
  return reg_4;
}
var cns939 = anyStr("check_not")
var cns940 = anyStr("check")
var cns941 = anyStr("next")
function function$62 (reg_0, reg_1) {
  var reg_6, reg_7, reg_11, reg_17, reg_19;
  var goto_20=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = lua$get(reg_1.a, cns940);
  reg_7 = newStack();
  append(reg_7, reg_0);
  goto_20 = !tobool(first(call(reg_6, reg_7)));
  if (!goto_20) {
    reg_11 = newStack();
    append(reg_11, call(lua$get(reg_1.a, cns941), newStack()));
    return reg_11;
  }
  if ((goto_20 || false)) {
    goto_20 = false;
    reg_17 = newStack();
    push(reg_17, nil());
    return reg_17;
  }
  reg_19 = newStack();
  return reg_19;
}
var cns942 = anyStr("try")
var cns943 = anyStr("try")
var cns944 = anyStr("err")
var cns945 = anyStr("table")
var cns946 = anyStr("concat")
var cns947 = parseNum("1")
var cns948 = anyStr(" or ")
var cns949 = anyStr("lower")
var cns950 = anyStr(" expected")
function function$63 (reg_0, reg_1) {
  var reg_6, reg_7, reg_9, reg_11, reg_14, reg_15, reg_20, reg_21, reg_22, reg_27, reg_29, reg_30, reg_36;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = lua$get(reg_1.a, cns943);
  reg_7 = newStack();
  append(reg_7, reg_0);
  reg_9 = first(call(reg_6, reg_7));
  if (tobool(reg_9)) {
    reg_11 = newStack();
    push(reg_11, reg_9);
    return reg_11;
  }
  reg_14 = lua$get(reg_1.a, cns944);
  reg_15 = newStack();
  reg_20 = lua$get(lua$get(reg_1.a, cns945), cns946);
  reg_21 = newStack();
  reg_22 = newTable();
  table_append(reg_22, cns947, copy(reg_0));
  push(reg_21, reg_22);
  push(reg_21, cns948);
  reg_27 = first(call(reg_20, reg_21));
  reg_29 = lua$get(reg_27, cns949);
  reg_30 = newStack();
  push(reg_30, reg_27);
  push(reg_15, concat(first(call(reg_29, reg_30)), cns950));
  reg_35 = call(reg_14, reg_15);
  reg_36 = newStack();
  return reg_36;
}
var cns951 = anyStr("expect")
var cns952 = anyStr("try")
var cns953 = anyStr("err")
var cns954 = anyStr(" expected (to close ")
var cns955 = anyStr("type")
var cns956 = anyStr(" at line ")
var cns957 = anyStr("line")
var cns958 = anyStr(")")
function function$64 (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_11, reg_13, reg_16, reg_17, reg_18, reg_20, reg_21, reg_31;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_1.a, cns952);
  reg_9 = newStack();
  push(reg_9, reg_4);
  reg_11 = first(call(reg_8, reg_9));
  if (tobool(reg_11)) {
    reg_13 = newStack();
    push(reg_13, reg_11);
    return reg_13;
  }
  reg_16 = lua$get(reg_1.a, cns953);
  reg_17 = newStack();
  reg_18 = cns954;
  reg_20 = lua$get(reg_5, cns955);
  reg_21 = cns956;
  push(reg_17, concat(reg_4, concat(reg_18, concat(reg_20, concat(reg_21, concat(lua$get(reg_5, cns957), cns958))))));
  reg_30 = call(reg_16, reg_17);
  reg_31 = newStack();
  return reg_31;
}
var cns959 = anyStr("match")
var cns960 = anyStr("expect")
var cns961 = anyStr("NAME")
var cns962 = anyStr("value")
function function$65 (reg_0, reg_1) {
  var reg_6, reg_7, reg_10, reg_12, reg_15, reg_17;
  var goto_18=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = lua$get(reg_1.a, cns960);
  reg_7 = newStack();
  push(reg_7, cns961);
  reg_10 = first(call(reg_6, reg_7));
  goto_18 = !tobool(reg_10);
  if (!goto_18) {
    reg_12 = newStack();
    push(reg_12, lua$get(reg_10, cns962));
    return reg_12;
  }
  if ((goto_18 || false)) {
    goto_18 = false;
    reg_15 = newStack();
    push(reg_15, nil());
    return reg_15;
  }
  reg_17 = newStack();
  return reg_17;
}
var cns963 = anyStr("get_name")
var cns964 = anyStr("_lookahead")
var cns965 = anyStr("Lexer")
var cns966 = anyStr("next")
var cns967 = anyStr("_lookahead")
var cns968 = anyStr("_lookahead")
function function$66 (reg_0, reg_1) {
  var reg_6, reg_18;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = lua$get(reg_1.a, cns964);
  if (!tobool(reg_6)) {
    reg_6 = first(call(lua$get(lua$get(reg_1.a, cns965), cns966), newStack()));
  }
  lua$set(reg_1.a, cns967, reg_6);
  reg_18 = newStack();
  push(reg_18, lua$get(reg_1.a, cns968));
  return reg_18;
}
var cns969 = anyStr("lookahead")
var cns970 = parseNum("0")
var cns971 = parseNum("0")
var cns972 = anyStr("token")
var cns973 = anyStr("token")
var cns974 = anyStr("line")
var cns975 = anyStr("token")
var cns976 = anyStr("column")
var cns977 = anyStr("line")
var cns978 = anyStr("column")
function function$68 (reg_0, reg_1) {
  var reg_5, reg_10;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  lua$set(reg_5, cns977, reg_1.b);
  lua$set(reg_5, cns978, reg_1.c);
  reg_10 = newStack();
  push(reg_10, reg_5);
  return reg_10;
}
function function$67 (reg_0, reg_1) {
  var reg_2, reg_3, reg_20;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = cns970;
  reg_3.c = cns971;
  if (tobool(lua$get(reg_1.a, cns972))) {
    reg_3.b = lua$get(lua$get(reg_1.a, cns973), cns974);
    reg_3.c = lua$get(lua$get(reg_1.a, cns975), cns976);
  }
  reg_20 = newStack();
  push(reg_20, anyFn((function (a) { return function$68(a,this) }).bind(reg_3)));
  return reg_20;
}
var cns979 = anyStr("get_pos")
var cns980 = anyStr("Parser")
var cns981 = anyStr("singlevar")
var cns982 = anyStr("get_pos")
var cns983 = anyStr("get_name")
var cns984 = anyStr("type")
var cns985 = anyStr("var")
var cns986 = anyStr("name")
function Parser_singlevar (reg_0, reg_1) {
  var reg_9, reg_15, reg_16, reg_17, reg_18;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_9 = first(call(lua$get(reg_1.a, cns982), newStack()));
  reg_15 = first(call(lua$get(reg_1.a, cns983), newStack()));
  reg_16 = newStack();
  reg_17 = newStack();
  reg_18 = newTable();
  lua$set(reg_18, cns984, cns985);
  lua$set(reg_18, cns986, reg_15);
  push(reg_17, reg_18);
  append(reg_16, call(reg_9, reg_17));
  return reg_16;
}
var cns987 = anyStr("get_pos")
var cns988 = anyStr("expect")
var cns989 = anyStr("{")
var cns990 = anyStr("check_not")
var cns991 = anyStr("}")
var cns992 = anyStr("try")
var cns993 = anyStr("[")
var cns994 = anyStr("Parser")
var cns995 = anyStr("expr")
var cns996 = anyStr("expect")
var cns997 = anyStr("]")
var cns998 = anyStr("expect")
var cns999 = anyStr("=")
var cns1000 = anyStr("Parser")
var cns1001 = anyStr("expr")
var cns1002 = anyStr("table")
var cns1003 = anyStr("insert")
var cns1004 = anyStr("type")
var cns1005 = anyStr("indexitem")
var cns1006 = anyStr("key")
var cns1007 = anyStr("value")
var cns1008 = anyStr("check")
var cns1009 = anyStr("NAME")
var cns1010 = anyStr("lookahead")
var cns1011 = anyStr("lookahead")
var cns1012 = anyStr("type")
var cns1013 = anyStr("=")
var cns1014 = anyStr("get_name")
var cns1015 = anyStr("expect")
var cns1016 = anyStr("=")
var cns1017 = anyStr("Parser")
var cns1018 = anyStr("expr")
var cns1019 = anyStr("table")
var cns1020 = anyStr("insert")
var cns1021 = anyStr("type")
var cns1022 = anyStr("fielditem")
var cns1023 = anyStr("key")
var cns1024 = anyStr("value")
var cns1025 = anyStr("Parser")
var cns1026 = anyStr("expr")
var cns1027 = anyStr("table")
var cns1028 = anyStr("insert")
var cns1029 = anyStr("type")
var cns1030 = anyStr("item")
var cns1031 = anyStr("value")
var cns1032 = anyStr("try")
var cns1033 = anyStr(",")
var cns1034 = anyStr(";")
var cns1035 = anyStr("match")
var cns1036 = anyStr("}")
var cns1037 = anyStr("type")
var cns1038 = anyStr("constructor")
var cns1039 = anyStr("items")
function function$69 (reg_0, reg_1) {
  var reg_9, reg_12, reg_13, reg_16, reg_17, reg_20, reg_21, reg_28, reg_29, reg_41, reg_44, reg_45, reg_50, reg_51, reg_61, reg_66, reg_67, reg_68, reg_76, reg_77, reg_80, reg_105, reg_108, reg_109, reg_119, reg_124, reg_125, reg_126, reg_139, reg_144, reg_145, reg_146, reg_153, reg_154, reg_160, reg_161, reg_164, reg_165, reg_166;
  var goto_85=false, goto_156=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_9 = first(call(lua$get(reg_1.a, cns987), newStack()));
  reg_12 = lua$get(reg_1.a, cns988);
  reg_13 = newStack();
  push(reg_13, cns989);
  reg_16 = first(call(reg_12, reg_13));
  reg_17 = newTable();
  loop_1: while (true) {
    reg_20 = lua$get(reg_1.a, cns990);
    reg_21 = newStack();
    push(reg_21, cns991);
    if (!tobool(first(call(reg_20, reg_21)))) break loop_1;
    reg_28 = lua$get(reg_1.a, cns992);
    reg_29 = newStack();
    push(reg_29, cns993);
    goto_85 = !tobool(first(call(reg_28, reg_29)));
    if (!goto_85) {
      reg_41 = first(call(lua$get(lua$get(reg_1.a, cns994), cns995), newStack()));
      reg_44 = lua$get(reg_1.a, cns996);
      reg_45 = newStack();
      push(reg_45, cns997);
      reg_47 = call(reg_44, reg_45);
      reg_50 = lua$get(reg_1.a, cns998);
      reg_51 = newStack();
      push(reg_51, cns999);
      reg_53 = call(reg_50, reg_51);
      reg_61 = first(call(lua$get(lua$get(reg_1.a, cns1000), cns1001), newStack()));
      reg_66 = lua$get(lua$get(reg_1.a, cns1002), cns1003);
      reg_67 = newStack();
      push(reg_67, reg_17);
      reg_68 = newTable();
      lua$set(reg_68, cns1004, cns1005);
      lua$set(reg_68, cns1006, reg_41);
      lua$set(reg_68, cns1007, reg_61);
      push(reg_67, reg_68);
      reg_73 = call(reg_66, reg_67);
    }
    if ((goto_85 || false)) {
      goto_85 = false;
      reg_76 = lua$get(reg_1.a, cns1008);
      reg_77 = newStack();
      push(reg_77, cns1009);
      reg_80 = first(call(reg_76, reg_77));
      if (tobool(reg_80)) {
        reg_80 = first(call(lua$get(reg_1.a, cns1010), newStack()));
      }
      if (tobool(reg_80)) {
        reg_80 = eq(lua$get(first(call(lua$get(reg_1.a, cns1011), newStack())), cns1012), cns1013);
      }
      goto_156 = !tobool(reg_80);
      if (!goto_156) {
        reg_105 = first(call(lua$get(reg_1.a, cns1014), newStack()));
        reg_108 = lua$get(reg_1.a, cns1015);
        reg_109 = newStack();
        push(reg_109, cns1016);
        reg_111 = call(reg_108, reg_109);
        reg_119 = first(call(lua$get(lua$get(reg_1.a, cns1017), cns1018), newStack()));
        reg_124 = lua$get(lua$get(reg_1.a, cns1019), cns1020);
        reg_125 = newStack();
        push(reg_125, reg_17);
        reg_126 = newTable();
        lua$set(reg_126, cns1021, cns1022);
        lua$set(reg_126, cns1023, reg_105);
        lua$set(reg_126, cns1024, reg_119);
        push(reg_125, reg_126);
        reg_131 = call(reg_124, reg_125);
      }
      if ((goto_156 || false)) {
        goto_156 = false;
        reg_139 = first(call(lua$get(lua$get(reg_1.a, cns1025), cns1026), newStack()));
        reg_144 = lua$get(lua$get(reg_1.a, cns1027), cns1028);
        reg_145 = newStack();
        push(reg_145, reg_17);
        reg_146 = newTable();
        lua$set(reg_146, cns1029, cns1030);
        lua$set(reg_146, cns1031, reg_139);
        push(reg_145, reg_146);
        reg_150 = call(reg_144, reg_145);
      }
    }
    reg_153 = lua$get(reg_1.a, cns1032);
    reg_154 = newStack();
    push(reg_154, cns1033);
    push(reg_154, cns1034);
    reg_157 = call(reg_153, reg_154);
  }
  reg_160 = lua$get(reg_1.a, cns1035);
  reg_161 = newStack();
  push(reg_161, cns1036);
  push(reg_161, reg_16);
  reg_163 = call(reg_160, reg_161);
  reg_164 = newStack();
  reg_165 = newStack();
  reg_166 = newTable();
  lua$set(reg_166, cns1037, cns1038);
  lua$set(reg_166, cns1039, reg_17);
  push(reg_165, reg_166);
  append(reg_164, call(reg_9, reg_165));
  return reg_164;
}
var cns1040 = anyStr("constructor")
var cns1041 = anyStr("line")
var cns1042 = anyStr("column")
var cns1043 = anyStr("try")
var cns1044 = anyStr(":")
var cns1045 = anyStr("token")
var cns1046 = anyStr("token")
var cns1047 = anyStr("line")
var cns1048 = anyStr("token")
var cns1049 = anyStr("column")
var cns1050 = anyStr("get_name")
var cns1051 = anyStr("check")
var cns1052 = anyStr("STR")
var cns1053 = parseNum("1")
var cns1054 = anyStr("Parser")
var cns1055 = anyStr("simpleexp")
var cns1056 = anyStr("check")
var cns1057 = anyStr("{")
var cns1058 = parseNum("1")
var cns1059 = anyStr("constructor")
var cns1060 = anyStr("expect")
var cns1061 = anyStr("(")
var cns1062 = anyStr("check_not")
var cns1063 = anyStr(")")
var cns1064 = anyStr("Parser")
var cns1065 = anyStr("explist")
var cns1066 = anyStr("match")
var cns1067 = anyStr(")")
var cns1068 = anyStr("type")
var cns1069 = anyStr("call")
var cns1070 = anyStr("base")
var cns1071 = anyStr("values")
var cns1072 = anyStr("key")
var cns1073 = anyStr("line")
var cns1074 = anyStr("column")
function function$70 (reg_0, reg_1) {
  var reg_4, reg_6, reg_8, reg_10, reg_11, reg_14, reg_15, reg_42, reg_43, reg_48, reg_49, reg_59, reg_60, reg_65, reg_66, reg_74, reg_75, reg_78, reg_81, reg_82, reg_97, reg_98, reg_101, reg_102;
  var goto_68=false, goto_88=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = newTable();
  reg_8 = lua$get(reg_4, cns1041);
  reg_10 = lua$get(reg_4, cns1042);
  reg_11 = nil();
  reg_14 = lua$get(reg_1.a, cns1043);
  reg_15 = newStack();
  push(reg_15, cns1044);
  if (tobool(first(call(reg_14, reg_15)))) {
    if (tobool(lua$get(reg_1.a, cns1045))) {
      reg_8 = lua$get(lua$get(reg_1.a, cns1046), cns1047);
      reg_10 = lua$get(lua$get(reg_1.a, cns1048), cns1049);
    }
    reg_11 = first(call(lua$get(reg_1.a, cns1050), newStack()));
  }
  reg_42 = lua$get(reg_1.a, cns1051);
  reg_43 = newStack();
  push(reg_43, cns1052);
  goto_68 = !tobool(first(call(reg_42, reg_43)));
  if (!goto_68) {
    reg_48 = newTable();
    reg_49 = cns1053;
    table_append(reg_48, reg_49, call(lua$get(lua$get(reg_1.a, cns1054), cns1055), newStack()));
    reg_6 = reg_48;
  }
  if ((goto_68 || false)) {
    goto_68 = false;
    reg_59 = lua$get(reg_1.a, cns1056);
    reg_60 = newStack();
    push(reg_60, cns1057);
    goto_88 = !tobool(first(call(reg_59, reg_60)));
    if (!goto_88) {
      reg_65 = newTable();
      reg_66 = cns1058;
      table_append(reg_65, reg_66, call(lua$get(reg_1.a, cns1059), newStack()));
      reg_6 = reg_65;
    }
    if ((goto_88 || false)) {
      goto_88 = false;
      reg_74 = lua$get(reg_1.a, cns1060);
      reg_75 = newStack();
      push(reg_75, cns1061);
      reg_78 = first(call(reg_74, reg_75));
      reg_81 = lua$get(reg_1.a, cns1062);
      reg_82 = newStack();
      push(reg_82, cns1063);
      if (tobool(first(call(reg_81, reg_82)))) {
        reg_6 = first(call(lua$get(lua$get(reg_1.a, cns1064), cns1065), newStack()));
      }
      reg_97 = lua$get(reg_1.a, cns1066);
      reg_98 = newStack();
      push(reg_98, cns1067);
      push(reg_98, reg_78);
      reg_100 = call(reg_97, reg_98);
    }
  }
  reg_101 = newStack();
  reg_102 = newTable();
  lua$set(reg_102, cns1068, cns1069);
  lua$set(reg_102, cns1070, reg_4);
  lua$set(reg_102, cns1071, reg_6);
  lua$set(reg_102, cns1072, reg_11);
  lua$set(reg_102, cns1073, reg_8);
  lua$set(reg_102, cns1074, reg_10);
  push(reg_101, reg_102);
  return reg_101;
}
var cns1075 = anyStr("funcargs")
var cns1076 = anyStr("Parser")
var cns1077 = anyStr("suffixedexp")
var cns1078 = anyStr("check")
var cns1079 = anyStr("(")
var cns1080 = anyStr("expect")
var cns1081 = anyStr("(")
var cns1082 = anyStr("Parser")
var cns1083 = anyStr("expr")
var cns1084 = anyStr("match")
var cns1085 = anyStr(")")
var cns1086 = anyStr("check")
var cns1087 = anyStr("NAME")
var cns1088 = anyStr("Parser")
var cns1089 = anyStr("singlevar")
var cns1090 = anyStr("err")
var cns1091 = anyStr("syntax error")
var cns1092 = anyStr("check")
var cns1093 = anyStr(".")
var cns1094 = anyStr("fieldsel")
var cns1095 = anyStr("try")
var cns1096 = anyStr("[")
var cns1097 = anyStr("get_pos")
var cns1098 = anyStr("Parser")
var cns1099 = anyStr("expr")
var cns1100 = anyStr("expect")
var cns1101 = anyStr("]")
var cns1102 = anyStr("type")
var cns1103 = anyStr("index")
var cns1104 = anyStr("base")
var cns1105 = anyStr("key")
var cns1106 = anyStr("check")
var cns1107 = anyStr(":")
var cns1108 = anyStr("(")
var cns1109 = anyStr("{")
var cns1110 = anyStr("STR")
var cns1111 = anyStr("funcargs")
function Parser_suffixedexp (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_17, reg_18, reg_21, reg_32, reg_33, reg_39, reg_40, reg_55, reg_56, reg_57, reg_65, reg_66, reg_73, reg_74, reg_80, reg_81, reg_91, reg_99, reg_102, reg_103, reg_106, reg_107, reg_117, reg_118, reg_128, reg_129, reg_133, reg_134;
  var goto_43=false, goto_63=false, goto_98=false, goto_145=false, goto_172=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = nil();
  reg_6 = lua$false();
  reg_9 = lua$get(reg_1.a, cns1078);
  reg_10 = newStack();
  push(reg_10, cns1079);
  goto_43 = !tobool(first(call(reg_9, reg_10)));
  if (!goto_43) {
    reg_17 = lua$get(reg_1.a, cns1080);
    reg_18 = newStack();
    push(reg_18, cns1081);
    reg_21 = first(call(reg_17, reg_18));
    reg_5 = first(call(lua$get(lua$get(reg_1.a, cns1082), cns1083), newStack()));
    reg_32 = lua$get(reg_1.a, cns1084);
    reg_33 = newStack();
    push(reg_33, cns1085);
    push(reg_33, reg_21);
    reg_35 = call(reg_32, reg_33);
    reg_6 = lua$true();
  }
  if ((goto_43 || false)) {
    goto_43 = false;
    reg_39 = lua$get(reg_1.a, cns1086);
    reg_40 = newStack();
    push(reg_40, cns1087);
    goto_63 = !tobool(first(call(reg_39, reg_40)));
    if (!goto_63) {
      reg_5 = first(call(lua$get(lua$get(reg_1.a, cns1088), cns1089), newStack()));
    }
    if ((goto_63 || false)) {
      goto_63 = false;
      reg_55 = lua$get(reg_1.a, cns1090);
      reg_56 = newStack();
      reg_57 = reg_4;
      if (!tobool(reg_4)) {
        reg_57 = cns1091;
      }
      push(reg_56, reg_57);
      reg_60 = call(reg_55, reg_56);
    }
  }
  loop_1: while (tobool(lua$true())) {
    reg_65 = lua$get(reg_1.a, cns1092);
    reg_66 = newStack();
    push(reg_66, cns1093);
    goto_98 = !tobool(first(call(reg_65, reg_66)));
    if (!goto_98) {
      reg_73 = lua$get(reg_1.a, cns1094);
      reg_74 = newStack();
      push(reg_74, reg_5);
      reg_5 = first(call(reg_73, reg_74));
      reg_6 = lua$false();
    }
    if ((goto_98 || false)) {
      goto_98 = false;
      reg_80 = lua$get(reg_1.a, cns1095);
      reg_81 = newStack();
      push(reg_81, cns1096);
      goto_145 = !tobool(first(call(reg_80, reg_81)));
      if (!goto_145) {
        reg_91 = first(call(lua$get(reg_1.a, cns1097), newStack()));
        reg_99 = first(call(lua$get(lua$get(reg_1.a, cns1098), cns1099), newStack()));
        reg_102 = lua$get(reg_1.a, cns1100);
        reg_103 = newStack();
        push(reg_103, cns1101);
        reg_105 = call(reg_102, reg_103);
        reg_106 = newStack();
        reg_107 = newTable();
        lua$set(reg_107, cns1102, cns1103);
        lua$set(reg_107, cns1104, reg_5);
        lua$set(reg_107, cns1105, reg_99);
        push(reg_106, reg_107);
        reg_5 = first(call(reg_91, reg_106));
        reg_6 = lua$false();
      }
      if ((goto_145 || false)) {
        goto_145 = false;
        reg_117 = lua$get(reg_1.a, cns1106);
        reg_118 = newStack();
        push(reg_118, cns1107);
        push(reg_118, cns1108);
        push(reg_118, cns1109);
        push(reg_118, cns1110);
        goto_172 = !tobool(first(call(reg_117, reg_118)));
        if (!goto_172) {
          reg_128 = lua$get(reg_1.a, cns1111);
          reg_129 = newStack();
          push(reg_129, reg_5);
          reg_5 = first(call(reg_128, reg_129));
          reg_6 = lua$false();
        }
        if ((goto_172 || false)) {
          goto_172 = false;
          reg_133 = newStack();
          push(reg_133, reg_5);
          push(reg_133, reg_6);
          return reg_133;
        }
      }
    }
  }
  reg_134 = newStack();
  return reg_134;
}
var cns1112 = anyStr("Parser")
var cns1113 = anyStr("simpleexp")
var cns1114 = anyStr("get_pos")
var cns1115 = anyStr("check")
var cns1116 = anyStr("NUM")
var cns1117 = anyStr("type")
var cns1118 = anyStr("num")
var cns1119 = anyStr("value")
var cns1120 = anyStr("next")
var cns1121 = anyStr("value")
var cns1122 = anyStr("check")
var cns1123 = anyStr("true")
var cns1124 = anyStr("false")
var cns1125 = anyStr("nil")
var cns1126 = anyStr("type")
var cns1127 = anyStr("const")
var cns1128 = anyStr("value")
var cns1129 = anyStr("next")
var cns1130 = anyStr("type")
var cns1131 = anyStr("try")
var cns1132 = anyStr("...")
var cns1133 = anyStr("type")
var cns1134 = anyStr("vararg")
var cns1135 = anyStr("check")
var cns1136 = anyStr("STR")
var cns1137 = anyStr("type")
var cns1138 = anyStr("str")
var cns1139 = anyStr("value")
var cns1140 = anyStr("next")
var cns1141 = anyStr("value")
var cns1142 = anyStr("check")
var cns1143 = anyStr("{")
var cns1144 = anyStr("constructor")
var cns1145 = anyStr("check")
var cns1146 = anyStr("function")
var cns1147 = anyStr("next")
var cns1148 = anyStr("Parser")
var cns1149 = anyStr("funcbody")
var cns1150 = anyStr("Parser")
var cns1151 = anyStr("suffixedexp")
var cns1152 = anyStr("invalid expression")
function Parser_simpleexp (reg_0, reg_1) {
  var reg_9, reg_12, reg_13, reg_18, reg_19, reg_20, reg_23, reg_35, reg_36, reg_43, reg_44, reg_45, reg_48, reg_60, reg_61, reg_66, reg_67, reg_68, reg_74, reg_75, reg_80, reg_81, reg_82, reg_85, reg_97, reg_98, reg_103, reg_111, reg_112, reg_122, reg_123, reg_124, reg_129, reg_130, reg_133, reg_138, reg_139, reg_142;
  var goto_39=false, goto_74=false, goto_95=false, goto_126=false, goto_145=false, goto_176=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_9 = first(call(lua$get(reg_1.a, cns1114), newStack()));
  reg_12 = lua$get(reg_1.a, cns1115);
  reg_13 = newStack();
  push(reg_13, cns1116);
  goto_39 = !tobool(first(call(reg_12, reg_13)));
  if (!goto_39) {
    reg_18 = newStack();
    reg_19 = newStack();
    reg_20 = newTable();
    lua$set(reg_20, cns1117, cns1118);
    reg_23 = cns1119;
    lua$set(reg_20, reg_23, lua$get(first(call(lua$get(reg_1.a, cns1120), newStack())), cns1121));
    push(reg_19, reg_20);
    append(reg_18, call(reg_9, reg_19));
    return reg_18;
  }
  if ((goto_39 || false)) {
    goto_39 = false;
    reg_35 = lua$get(reg_1.a, cns1122);
    reg_36 = newStack();
    push(reg_36, cns1123);
    push(reg_36, cns1124);
    push(reg_36, cns1125);
    goto_74 = !tobool(first(call(reg_35, reg_36)));
    if (!goto_74) {
      reg_43 = newStack();
      reg_44 = newStack();
      reg_45 = newTable();
      lua$set(reg_45, cns1126, cns1127);
      reg_48 = cns1128;
      lua$set(reg_45, reg_48, lua$get(first(call(lua$get(reg_1.a, cns1129), newStack())), cns1130));
      push(reg_44, reg_45);
      append(reg_43, call(reg_9, reg_44));
      return reg_43;
    }
    if ((goto_74 || false)) {
      goto_74 = false;
      reg_60 = lua$get(reg_1.a, cns1131);
      reg_61 = newStack();
      push(reg_61, cns1132);
      goto_95 = !tobool(first(call(reg_60, reg_61)));
      if (!goto_95) {
        reg_66 = newStack();
        reg_67 = newStack();
        reg_68 = newTable();
        lua$set(reg_68, cns1133, cns1134);
        push(reg_67, reg_68);
        append(reg_66, call(reg_9, reg_67));
        return reg_66;
      }
      if ((goto_95 || false)) {
        goto_95 = false;
        reg_74 = lua$get(reg_1.a, cns1135);
        reg_75 = newStack();
        push(reg_75, cns1136);
        goto_126 = !tobool(first(call(reg_74, reg_75)));
        if (!goto_126) {
          reg_80 = newStack();
          reg_81 = newStack();
          reg_82 = newTable();
          lua$set(reg_82, cns1137, cns1138);
          reg_85 = cns1139;
          lua$set(reg_82, reg_85, lua$get(first(call(lua$get(reg_1.a, cns1140), newStack())), cns1141));
          push(reg_81, reg_82);
          append(reg_80, call(reg_9, reg_81));
          return reg_80;
        }
        if ((goto_126 || false)) {
          goto_126 = false;
          reg_97 = lua$get(reg_1.a, cns1142);
          reg_98 = newStack();
          push(reg_98, cns1143);
          goto_145 = !tobool(first(call(reg_97, reg_98)));
          if (!goto_145) {
            reg_103 = newStack();
            append(reg_103, call(lua$get(reg_1.a, cns1144), newStack()));
            return reg_103;
          }
          if ((goto_145 || false)) {
            goto_145 = false;
            reg_111 = lua$get(reg_1.a, cns1145);
            reg_112 = newStack();
            push(reg_112, cns1146);
            goto_176 = !tobool(first(call(reg_111, reg_112)));
            if (!goto_176) {
              reg_122 = first(call(lua$get(reg_1.a, cns1147), newStack()));
              reg_123 = newStack();
              reg_124 = newStack();
              reg_129 = lua$get(lua$get(reg_1.a, cns1148), cns1149);
              reg_130 = newStack();
              push(reg_130, reg_122);
              append(reg_124, call(reg_129, reg_130));
              append(reg_123, call(reg_9, reg_124));
              return reg_123;
            }
            if ((goto_176 || false)) {
              goto_176 = false;
              reg_133 = newStack();
              reg_138 = lua$get(lua$get(reg_1.a, cns1150), cns1151);
              reg_139 = newStack();
              push(reg_139, cns1152);
              append(reg_133, call(reg_138, reg_139));
              return reg_133;
            }
          }
        }
      }
    }
  }
  reg_142 = newStack();
  return reg_142;
}
var cns1153 = anyStr("+")
var cns1154 = parseNum("1")
var cns1155 = parseNum("10")
var cns1156 = parseNum("2")
var cns1157 = parseNum("10")
var cns1158 = anyStr("-")
var cns1159 = parseNum("1")
var cns1160 = parseNum("10")
var cns1161 = parseNum("2")
var cns1162 = parseNum("10")
var cns1163 = anyStr("*")
var cns1164 = parseNum("1")
var cns1165 = parseNum("11")
var cns1166 = parseNum("2")
var cns1167 = parseNum("11")
var cns1168 = anyStr("%")
var cns1169 = parseNum("1")
var cns1170 = parseNum("11")
var cns1171 = parseNum("2")
var cns1172 = parseNum("11")
var cns1173 = anyStr("/")
var cns1174 = parseNum("1")
var cns1175 = parseNum("11")
var cns1176 = parseNum("2")
var cns1177 = parseNum("11")
var cns1178 = anyStr("//")
var cns1179 = parseNum("1")
var cns1180 = parseNum("11")
var cns1181 = parseNum("2")
var cns1182 = parseNum("11")
var cns1183 = anyStr("^")
var cns1184 = parseNum("1")
var cns1185 = parseNum("14")
var cns1186 = parseNum("2")
var cns1187 = parseNum("13")
var cns1188 = anyStr("&")
var cns1189 = parseNum("1")
var cns1190 = parseNum("6")
var cns1191 = parseNum("2")
var cns1192 = parseNum("6")
var cns1193 = anyStr("|")
var cns1194 = parseNum("1")
var cns1195 = parseNum("4")
var cns1196 = parseNum("2")
var cns1197 = parseNum("4")
var cns1198 = anyStr("~")
var cns1199 = parseNum("1")
var cns1200 = parseNum("5")
var cns1201 = parseNum("2")
var cns1202 = parseNum("5")
var cns1203 = anyStr("<<")
var cns1204 = parseNum("1")
var cns1205 = parseNum("7")
var cns1206 = parseNum("2")
var cns1207 = parseNum("7")
var cns1208 = anyStr(">>")
var cns1209 = parseNum("1")
var cns1210 = parseNum("7")
var cns1211 = parseNum("2")
var cns1212 = parseNum("7")
var cns1213 = anyStr("..")
var cns1214 = parseNum("1")
var cns1215 = parseNum("9")
var cns1216 = parseNum("2")
var cns1217 = parseNum("8")
var cns1218 = anyStr("<")
var cns1219 = parseNum("1")
var cns1220 = parseNum("3")
var cns1221 = parseNum("2")
var cns1222 = parseNum("3")
var cns1223 = anyStr(">")
var cns1224 = parseNum("1")
var cns1225 = parseNum("3")
var cns1226 = parseNum("2")
var cns1227 = parseNum("3")
var cns1228 = anyStr("==")
var cns1229 = parseNum("1")
var cns1230 = parseNum("3")
var cns1231 = parseNum("2")
var cns1232 = parseNum("3")
var cns1233 = anyStr("~=")
var cns1234 = parseNum("1")
var cns1235 = parseNum("3")
var cns1236 = parseNum("2")
var cns1237 = parseNum("3")
var cns1238 = anyStr("<=")
var cns1239 = parseNum("1")
var cns1240 = parseNum("3")
var cns1241 = parseNum("2")
var cns1242 = parseNum("3")
var cns1243 = anyStr(">=")
var cns1244 = parseNum("1")
var cns1245 = parseNum("3")
var cns1246 = parseNum("2")
var cns1247 = parseNum("3")
var cns1248 = anyStr("and")
var cns1249 = parseNum("1")
var cns1250 = parseNum("2")
var cns1251 = parseNum("2")
var cns1252 = parseNum("2")
var cns1253 = anyStr("or")
var cns1254 = parseNum("1")
var cns1255 = parseNum("1")
var cns1256 = parseNum("2")
var cns1257 = parseNum("1")
var cns1258 = anyStr("priority")
var cns1259 = parseNum("12")
var cns1260 = anyStr("unary_priority")
var cns1261 = anyStr("Parser")
var cns1262 = anyStr("expr")
var cns1263 = parseNum("0")
var cns1264 = anyStr("check")
var cns1265 = anyStr("not")
var cns1266 = anyStr("-")
var cns1267 = anyStr("~")
var cns1268 = anyStr("#")
var cns1269 = anyStr("get_pos")
var cns1270 = anyStr("type")
var cns1271 = anyStr("unop")
var cns1272 = anyStr("op")
var cns1273 = anyStr("next")
var cns1274 = anyStr("type")
var cns1275 = anyStr("value")
var cns1276 = anyStr("Parser")
var cns1277 = anyStr("expr")
var cns1278 = anyStr("unary_priority")
var cns1279 = anyStr("Parser")
var cns1280 = anyStr("simpleexp")
var cns1281 = anyStr("token")
var cns1282 = anyStr("priority")
var cns1283 = anyStr("token")
var cns1284 = anyStr("type")
var cns1285 = parseNum("1")
var cns1286 = anyStr("get_pos")
var cns1287 = anyStr("next")
var cns1288 = anyStr("type")
var cns1289 = anyStr("Parser")
var cns1290 = anyStr("expr")
var cns1291 = parseNum("2")
var cns1292 = anyStr("type")
var cns1293 = anyStr("binop")
var cns1294 = anyStr("op")
var cns1295 = anyStr("left")
var cns1296 = anyStr("right")
var cns1297 = anyStr("token")
var cns1298 = anyStr("priority")
var cns1299 = anyStr("token")
var cns1300 = anyStr("type")
function Parser_expr (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_11, reg_12, reg_25, reg_26, reg_27, reg_30, reg_39, reg_44, reg_45, reg_63, reg_67, reg_74, reg_85, reg_93, reg_98, reg_99, reg_103, reg_104, reg_105, reg_115, reg_119, reg_126;
  var goto_66=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = reg_4;
  if (!tobool(reg_4)) {
    reg_5 = cns1263;
  }
  reg_4 = reg_5;
  reg_8 = nil();
  reg_11 = lua$get(reg_1.a, cns1264);
  reg_12 = newStack();
  push(reg_12, cns1265);
  push(reg_12, cns1266);
  push(reg_12, cns1267);
  push(reg_12, cns1268);
  goto_66 = !tobool(first(call(reg_11, reg_12)));
  if (!goto_66) {
    reg_25 = first(call(lua$get(reg_1.a, cns1269), newStack()));
    reg_26 = newStack();
    reg_27 = newTable();
    lua$set(reg_27, cns1270, cns1271);
    reg_30 = cns1272;
    lua$set(reg_27, reg_30, lua$get(first(call(lua$get(reg_1.a, cns1273), newStack())), cns1274));
    reg_39 = cns1275;
    reg_44 = lua$get(lua$get(reg_1.a, cns1276), cns1277);
    reg_45 = newStack();
    push(reg_45, lua$get(reg_1.a, cns1278));
    lua$set(reg_27, reg_39, first(call(reg_44, reg_45)));
    push(reg_26, reg_27);
    reg_8 = first(call(reg_25, reg_26));
  }
  if ((goto_66 || false)) {
    goto_66 = false;
    reg_8 = first(call(lua$get(lua$get(reg_1.a, cns1279), cns1280), newStack()));
  }
  reg_63 = lua$get(reg_1.a, cns1281);
  if (tobool(reg_63)) {
    reg_67 = lua$get(reg_1.a, cns1282);
    reg_63 = lua$get(reg_67, lua$get(lua$get(reg_1.a, cns1283), cns1284));
  }
  loop_1: while (true) {
    reg_74 = reg_63;
    if (tobool(reg_63)) {
      reg_74 = gt(lua$get(reg_63, cns1285), reg_4);
    }
    if (!tobool(reg_74)) break loop_1;
    reg_85 = first(call(lua$get(reg_1.a, cns1286), newStack()));
    reg_93 = lua$get(first(call(lua$get(reg_1.a, cns1287), newStack())), cns1288);
    reg_98 = lua$get(lua$get(reg_1.a, cns1289), cns1290);
    reg_99 = newStack();
    push(reg_99, lua$get(reg_63, cns1291));
    reg_103 = first(call(reg_98, reg_99));
    reg_104 = newStack();
    reg_105 = newTable();
    lua$set(reg_105, cns1292, cns1293);
    lua$set(reg_105, cns1294, reg_93);
    lua$set(reg_105, cns1295, reg_8);
    lua$set(reg_105, cns1296, reg_103);
    push(reg_104, reg_105);
    reg_8 = first(call(reg_85, reg_104));
    reg_115 = lua$get(reg_1.a, cns1297);
    if (tobool(reg_115)) {
      reg_119 = lua$get(reg_1.a, cns1298);
      reg_115 = lua$get(reg_119, lua$get(lua$get(reg_1.a, cns1299), cns1300));
    }
    reg_63 = reg_115;
  }
  reg_126 = newStack();
  push(reg_126, reg_8);
  return reg_126;
}
var cns1301 = anyStr("Parser")
var cns1302 = anyStr("explist")
var cns1303 = anyStr("Parser")
var cns1304 = anyStr("expr")
var cns1305 = anyStr("table")
var cns1306 = anyStr("insert")
var cns1307 = anyStr("try")
var cns1308 = anyStr(",")
function Parser_explist (reg_0, reg_1) {
  var reg_4, reg_12, reg_17, reg_18, reg_22, reg_23, reg_29;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newTable();
  loop_1: while (true) {
    reg_12 = first(call(lua$get(lua$get(reg_1.a, cns1303), cns1304), newStack()));
    reg_17 = lua$get(lua$get(reg_1.a, cns1305), cns1306);
    reg_18 = newStack();
    push(reg_18, reg_4);
    push(reg_18, reg_12);
    reg_19 = call(reg_17, reg_18);
    reg_22 = lua$get(reg_1.a, cns1307);
    reg_23 = newStack();
    push(reg_23, cns1308);
    if (tobool(not(first(call(reg_22, reg_23))))) break loop_1;
  }
  reg_29 = newStack();
  push(reg_29, reg_4);
  return reg_29;
}
var cns1309 = anyStr("Parser")
var cns1310 = anyStr("funcbody")
var cns1311 = anyStr("expect")
var cns1312 = anyStr("(")
var cns1313 = anyStr("check")
var cns1314 = anyStr(")")
var cns1315 = anyStr("check")
var cns1316 = anyStr("NAME")
var cns1317 = anyStr("table")
var cns1318 = anyStr("insert")
var cns1319 = anyStr("get_name")
var cns1320 = anyStr("try")
var cns1321 = anyStr("...")
var cns1322 = anyStr("expect")
var cns1323 = anyStr("NAME")
var cns1324 = anyStr("...")
var cns1325 = anyStr("try")
var cns1326 = anyStr(",")
var cns1327 = anyStr("expect")
var cns1328 = anyStr(")")
var cns1329 = anyStr("Parser")
var cns1330 = anyStr("statlist")
var cns1331 = anyStr("match")
var cns1332 = anyStr("end")
var cns1333 = anyStr("type")
var cns1334 = anyStr("function")
var cns1335 = anyStr("vararg")
var cns1336 = anyStr("names")
var cns1337 = anyStr("body")
function Parser_funcbody (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_15, reg_16, reg_24, reg_25, reg_34, reg_35, reg_44, reg_45, reg_53, reg_54, reg_60, reg_61, reg_69, reg_70, reg_80, reg_83, reg_84, reg_87, reg_88;
  var goto_48=false, goto_62=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = lua$false();
  reg_6 = newTable();
  reg_9 = lua$get(reg_1.a, cns1311);
  reg_10 = newStack();
  push(reg_10, cns1312);
  reg_12 = call(reg_9, reg_10);
  reg_15 = lua$get(reg_1.a, cns1313);
  reg_16 = newStack();
  push(reg_16, cns1314);
  if (tobool(not(first(call(reg_15, reg_16))))) {
    loop_1: while (true) {
      reg_24 = lua$get(reg_1.a, cns1315);
      reg_25 = newStack();
      push(reg_25, cns1316);
      goto_48 = !tobool(first(call(reg_24, reg_25)));
      if (!goto_48) {
        reg_34 = lua$get(lua$get(reg_1.a, cns1317), cns1318);
        reg_35 = newStack();
        push(reg_35, reg_6);
        append(reg_35, call(lua$get(reg_1.a, cns1319), newStack()));
        reg_41 = call(reg_34, reg_35);
      }
      if ((goto_48 || false)) {
        goto_48 = false;
        reg_44 = lua$get(reg_1.a, cns1320);
        reg_45 = newStack();
        push(reg_45, cns1321);
        goto_62 = !tobool(first(call(reg_44, reg_45)));
        if (!goto_62) {
          reg_5 = lua$true();
          if (true) break loop_1;
        }
        if ((goto_62 || false)) {
          goto_62 = false;
          reg_53 = lua$get(reg_1.a, cns1322);
          reg_54 = newStack();
          push(reg_54, cns1323);
          push(reg_54, cns1324);
          reg_57 = call(reg_53, reg_54);
        }
      }
      reg_60 = lua$get(reg_1.a, cns1325);
      reg_61 = newStack();
      push(reg_61, cns1326);
      if (tobool(not(first(call(reg_60, reg_61))))) break loop_1;
    }
  }
  reg_69 = lua$get(reg_1.a, cns1327);
  reg_70 = newStack();
  push(reg_70, cns1328);
  reg_72 = call(reg_69, reg_70);
  reg_80 = first(call(lua$get(lua$get(reg_1.a, cns1329), cns1330), newStack()));
  reg_83 = lua$get(reg_1.a, cns1331);
  reg_84 = newStack();
  push(reg_84, cns1332);
  push(reg_84, reg_4);
  reg_86 = call(reg_83, reg_84);
  reg_87 = newStack();
  reg_88 = newTable();
  lua$set(reg_88, cns1333, cns1334);
  lua$set(reg_88, cns1335, reg_5);
  lua$set(reg_88, cns1336, reg_6);
  lua$set(reg_88, cns1337, reg_80);
  push(reg_87, reg_88);
  return reg_87;
}
var cns1338 = anyStr("expect")
var cns1339 = anyStr(":")
var cns1340 = anyStr("expect")
var cns1341 = anyStr(".")
var cns1342 = anyStr("get_pos")
var cns1343 = anyStr("get_name")
var cns1344 = anyStr("type")
var cns1345 = anyStr("field")
var cns1346 = anyStr("base")
var cns1347 = anyStr("key")
function function$71 (reg_0, reg_1) {
  var reg_4, reg_9, reg_10, reg_15, reg_16, reg_24, reg_30, reg_31, reg_32, reg_33;
  var goto_14=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  goto_14 = !tobool(next(reg_0));
  if (!goto_14) {
    reg_9 = lua$get(reg_1.a, cns1338);
    reg_10 = newStack();
    push(reg_10, cns1339);
    reg_12 = call(reg_9, reg_10);
  }
  if ((goto_14 || false)) {
    goto_14 = false;
    reg_15 = lua$get(reg_1.a, cns1340);
    reg_16 = newStack();
    push(reg_16, cns1341);
    reg_18 = call(reg_15, reg_16);
  }
  reg_24 = first(call(lua$get(reg_1.a, cns1342), newStack()));
  reg_30 = first(call(lua$get(reg_1.a, cns1343), newStack()));
  reg_31 = newStack();
  reg_32 = newStack();
  reg_33 = newTable();
  lua$set(reg_33, cns1344, cns1345);
  lua$set(reg_33, cns1346, reg_4);
  lua$set(reg_33, cns1347, reg_30);
  push(reg_32, reg_33);
  append(reg_31, call(reg_24, reg_32));
  return reg_31;
}
var cns1348 = anyStr("fieldsel")
var cns1349 = anyStr("Parser")
var cns1350 = anyStr("exprstat")
var cns1351 = anyStr("get_pos")
var cns1352 = anyStr("Parser")
var cns1353 = anyStr("suffixedexp")
var cns1354 = anyStr("invalid statement")
var cns1355 = anyStr("type")
var cns1356 = anyStr("call")
var cns1357 = parseNum("1")
var cns1358 = anyStr("try")
var cns1359 = anyStr(",")
var cns1360 = anyStr("Parser")
var cns1361 = anyStr("suffixedexp")
var cns1362 = anyStr("invalid statement")
var cns1363 = anyStr("table")
var cns1364 = anyStr("insert")
var cns1365 = anyStr("expect")
var cns1366 = anyStr("=")
var cns1367 = anyStr("err")
var cns1368 = anyStr("assignment to a parenthesized expression")
var cns1369 = anyStr("pairs")
var cns1370 = anyStr("type")
var cns1371 = anyStr("var")
var cns1372 = anyStr("type")
var cns1373 = anyStr("field")
var cns1374 = anyStr("type")
var cns1375 = anyStr("index")
var cns1376 = anyStr("err")
var cns1377 = anyStr("assignment to a non lvalue expression")
var cns1378 = anyStr("Parser")
var cns1379 = anyStr("explist")
var cns1380 = anyStr("type")
var cns1381 = anyStr("assignment")
var cns1382 = anyStr("lhs")
var cns1383 = anyStr("values")
function Parser_exprstat (reg_0, reg_1) {
  var reg_9, reg_14, reg_15, reg_17, reg_18, reg_19, reg_20, reg_27, reg_28, reg_32, reg_33, reg_43, reg_44, reg_46, reg_55, reg_56, reg_60, reg_61, reg_67, reg_68, reg_73, reg_74, reg_75, reg_76, reg_77, reg_78, reg_79, reg_80, reg_82, reg_89, reg_103, reg_104, reg_114, reg_115, reg_116, reg_117, reg_123;
  var goto_33=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_9 = first(call(lua$get(reg_1.a, cns1351), newStack()));
  reg_14 = lua$get(lua$get(reg_1.a, cns1352), cns1353);
  reg_15 = newStack();
  push(reg_15, cns1354);
  reg_17 = call(reg_14, reg_15);
  reg_18 = next(reg_17);
  reg_19 = next(reg_17);
  reg_20 = not(reg_19);
  if (tobool(reg_20)) {
    reg_20 = eq(lua$get(reg_18, cns1355), cns1356);
  }
  goto_33 = !tobool(reg_20);
  if (!goto_33) {
    reg_27 = newStack();
    push(reg_27, reg_18);
    return reg_27;
  }
  if ((goto_33 || false)) {
    goto_33 = false;
    reg_28 = newTable();
    lua$set(reg_28, cns1357, reg_18);
    loop_2: while (true) {
      reg_32 = lua$get(reg_1.a, cns1358);
      reg_33 = newStack();
      push(reg_33, cns1359);
      if (!tobool(first(call(reg_32, reg_33)))) break loop_2;
      reg_38 = nil();
      reg_43 = lua$get(lua$get(reg_1.a, cns1360), cns1361);
      reg_44 = newStack();
      push(reg_44, cns1362);
      reg_46 = call(reg_43, reg_44);
      reg_18 = next(reg_46);
      if (tobool(next(reg_46))) {
        reg_19 = lua$true();
      }
      reg_55 = lua$get(lua$get(reg_1.a, cns1363), cns1364);
      reg_56 = newStack();
      push(reg_56, reg_28);
      push(reg_56, reg_18);
      reg_57 = call(reg_55, reg_56);
    }
    reg_60 = lua$get(reg_1.a, cns1365);
    reg_61 = newStack();
    push(reg_61, cns1366);
    reg_63 = call(reg_60, reg_61);
    if (tobool(reg_19)) {
      reg_67 = lua$get(reg_1.a, cns1367);
      reg_68 = newStack();
      push(reg_68, cns1368);
      reg_70 = call(reg_67, reg_68);
    }
    reg_73 = lua$get(reg_1.a, cns1369);
    reg_74 = newStack();
    push(reg_74, reg_28);
    reg_75 = call(reg_73, reg_74);
    reg_76 = next(reg_75);
    reg_77 = next(reg_75);
    reg_78 = next(reg_75);
    loop_1: while (true) {
      reg_79 = newStack();
      push(reg_79, reg_77);
      push(reg_79, reg_78);
      reg_80 = call(reg_76, reg_79);
      reg_78 = next(reg_80);
      reg_82 = next(reg_80);
      if (tobool(eq(reg_78, nil()))) break loop_1;
      reg_89 = ne(lua$get(reg_82, cns1370), cns1371);
      if (tobool(reg_89)) {
        reg_89 = ne(lua$get(reg_82, cns1372), cns1373);
      }
      if (tobool(reg_89)) {
        reg_89 = ne(lua$get(reg_82, cns1374), cns1375);
      }
      if (tobool(reg_89)) {
        reg_103 = lua$get(reg_1.a, cns1376);
        reg_104 = newStack();
        push(reg_104, cns1377);
        reg_106 = call(reg_103, reg_104);
      }
    }
    reg_114 = first(call(lua$get(lua$get(reg_1.a, cns1378), cns1379), newStack()));
    reg_115 = newStack();
    reg_116 = newStack();
    reg_117 = newTable();
    lua$set(reg_117, cns1380, cns1381);
    lua$set(reg_117, cns1382, reg_28);
    lua$set(reg_117, cns1383, reg_114);
    push(reg_116, reg_117);
    append(reg_115, call(reg_9, reg_116));
    return reg_115;
  }
  reg_123 = newStack();
  return reg_123;
}
var cns1384 = anyStr("Parser")
var cns1385 = anyStr("forstat")
var cns1386 = anyStr("expect")
var cns1387 = anyStr("for")
var cns1388 = anyStr("expect")
var cns1389 = anyStr("NAME")
var cns1390 = anyStr("try")
var cns1391 = anyStr("=")
var cns1392 = anyStr("value")
var cns1393 = anyStr("Parser")
var cns1394 = anyStr("expr")
var cns1395 = anyStr("expect")
var cns1396 = anyStr(",")
var cns1397 = anyStr("Parser")
var cns1398 = anyStr("expr")
var cns1399 = anyStr("try")
var cns1400 = anyStr(",")
var cns1401 = anyStr("Parser")
var cns1402 = anyStr("expr")
var cns1403 = anyStr("expect")
var cns1404 = anyStr("do")
var cns1405 = anyStr("Parser")
var cns1406 = anyStr("statlist")
var cns1407 = anyStr("match")
var cns1408 = anyStr("end")
var cns1409 = anyStr("type")
var cns1410 = anyStr("numfor")
var cns1411 = anyStr("name")
var cns1412 = anyStr("body")
var cns1413 = anyStr("init")
var cns1414 = anyStr("limit")
var cns1415 = anyStr("step")
var cns1416 = anyStr("check")
var cns1417 = anyStr(",")
var cns1418 = anyStr("in")
var cns1419 = parseNum("1")
var cns1420 = anyStr("value")
var cns1421 = anyStr("try")
var cns1422 = anyStr(",")
var cns1423 = anyStr("expect")
var cns1424 = anyStr("NAME")
var cns1425 = anyStr("table")
var cns1426 = anyStr("insert")
var cns1427 = anyStr("value")
var cns1428 = anyStr("expect")
var cns1429 = anyStr("in")
var cns1430 = anyStr("Parser")
var cns1431 = anyStr("explist")
var cns1432 = anyStr("expect")
var cns1433 = anyStr("do")
var cns1434 = anyStr("Parser")
var cns1435 = anyStr("statlist")
var cns1436 = anyStr("match")
var cns1437 = anyStr("end")
var cns1438 = anyStr("type")
var cns1439 = anyStr("genfor")
var cns1440 = anyStr("names")
var cns1441 = anyStr("values")
var cns1442 = anyStr("body")
var cns1443 = anyStr("expect")
var cns1444 = anyStr("=")
var cns1445 = anyStr(",")
var cns1446 = anyStr("in")
function Parser_forstat (reg_0, reg_1) {
  var reg_6, reg_7, reg_10, reg_13, reg_14, reg_17, reg_20, reg_23, reg_24, reg_30, reg_38, reg_41, reg_42, reg_52, reg_53, reg_56, reg_57, reg_72, reg_73, reg_83, reg_86, reg_87, reg_90, reg_91, reg_101, reg_102, reg_108, reg_109, reg_114, reg_115, reg_122, reg_123, reg_129, reg_134, reg_135, reg_141, reg_142, reg_152, reg_155, reg_156, reg_166, reg_169, reg_170, reg_173, reg_174, reg_182, reg_183, reg_188;
  var goto_121=false, goto_227=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = lua$get(reg_1.a, cns1386);
  reg_7 = newStack();
  push(reg_7, cns1387);
  reg_10 = first(call(reg_6, reg_7));
  reg_13 = lua$get(reg_1.a, cns1388);
  reg_14 = newStack();
  push(reg_14, cns1389);
  reg_17 = first(call(reg_13, reg_14));
  if (tobool(not(reg_17))) {
    reg_20 = newStack();
    return reg_20;
  }
  reg_23 = lua$get(reg_1.a, cns1390);
  reg_24 = newStack();
  push(reg_24, cns1391);
  goto_121 = !tobool(first(call(reg_23, reg_24)));
  if (!goto_121) {
    reg_30 = lua$get(reg_17, cns1392);
    reg_38 = first(call(lua$get(lua$get(reg_1.a, cns1393), cns1394), newStack()));
    reg_41 = lua$get(reg_1.a, cns1395);
    reg_42 = newStack();
    push(reg_42, cns1396);
    reg_44 = call(reg_41, reg_42);
    reg_52 = first(call(lua$get(lua$get(reg_1.a, cns1397), cns1398), newStack()));
    reg_53 = nil();
    reg_56 = lua$get(reg_1.a, cns1399);
    reg_57 = newStack();
    push(reg_57, cns1400);
    if (tobool(first(call(reg_56, reg_57)))) {
      reg_53 = first(call(lua$get(lua$get(reg_1.a, cns1401), cns1402), newStack()));
    }
    reg_72 = lua$get(reg_1.a, cns1403);
    reg_73 = newStack();
    push(reg_73, cns1404);
    reg_75 = call(reg_72, reg_73);
    reg_83 = first(call(lua$get(lua$get(reg_1.a, cns1405), cns1406), newStack()));
    reg_86 = lua$get(reg_1.a, cns1407);
    reg_87 = newStack();
    push(reg_87, cns1408);
    push(reg_87, reg_10);
    reg_89 = call(reg_86, reg_87);
    reg_90 = newStack();
    reg_91 = newTable();
    lua$set(reg_91, cns1409, cns1410);
    lua$set(reg_91, cns1411, reg_30);
    lua$set(reg_91, cns1412, reg_83);
    lua$set(reg_91, cns1413, reg_38);
    lua$set(reg_91, cns1414, reg_52);
    lua$set(reg_91, cns1415, reg_53);
    push(reg_90, reg_91);
    return reg_90;
  }
  if ((goto_121 || false)) {
    goto_121 = false;
    reg_101 = lua$get(reg_1.a, cns1416);
    reg_102 = newStack();
    push(reg_102, cns1417);
    push(reg_102, cns1418);
    goto_227 = !tobool(first(call(reg_101, reg_102)));
    if (!goto_227) {
      reg_108 = newTable();
      reg_109 = cns1419;
      lua$set(reg_108, reg_109, lua$get(reg_17, cns1420));
      loop_1: while (true) {
        reg_114 = lua$get(reg_1.a, cns1421);
        reg_115 = newStack();
        push(reg_115, cns1422);
        if (!tobool(first(call(reg_114, reg_115)))) break loop_1;
        reg_122 = lua$get(reg_1.a, cns1423);
        reg_123 = newStack();
        push(reg_123, cns1424);
        reg_17 = first(call(reg_122, reg_123));
        if (tobool(not(reg_17))) {
          reg_129 = newStack();
          return reg_129;
        }
        reg_134 = lua$get(lua$get(reg_1.a, cns1425), cns1426);
        reg_135 = newStack();
        push(reg_135, reg_108);
        push(reg_135, lua$get(reg_17, cns1427));
        reg_138 = call(reg_134, reg_135);
      }
      reg_141 = lua$get(reg_1.a, cns1428);
      reg_142 = newStack();
      push(reg_142, cns1429);
      reg_144 = call(reg_141, reg_142);
      reg_152 = first(call(lua$get(lua$get(reg_1.a, cns1430), cns1431), newStack()));
      reg_155 = lua$get(reg_1.a, cns1432);
      reg_156 = newStack();
      push(reg_156, cns1433);
      reg_158 = call(reg_155, reg_156);
      reg_166 = first(call(lua$get(lua$get(reg_1.a, cns1434), cns1435), newStack()));
      reg_169 = lua$get(reg_1.a, cns1436);
      reg_170 = newStack();
      push(reg_170, cns1437);
      push(reg_170, reg_10);
      reg_172 = call(reg_169, reg_170);
      reg_173 = newStack();
      reg_174 = newTable();
      lua$set(reg_174, cns1438, cns1439);
      lua$set(reg_174, cns1440, reg_108);
      lua$set(reg_174, cns1441, reg_152);
      lua$set(reg_174, cns1442, reg_166);
      push(reg_173, reg_174);
      return reg_173;
    }
    if ((goto_227 || false)) {
      goto_227 = false;
      reg_182 = lua$get(reg_1.a, cns1443);
      reg_183 = newStack();
      push(reg_183, cns1444);
      push(reg_183, cns1445);
      push(reg_183, cns1446);
      reg_187 = call(reg_182, reg_183);
    }
  }
  reg_188 = newStack();
  return reg_188;
}
var cns1447 = anyStr("Parser")
var cns1448 = anyStr("ifstat")
var cns1449 = anyStr("expect")
var cns1450 = anyStr("if")
var cns1451 = anyStr("Parser")
var cns1452 = anyStr("expr")
var cns1453 = anyStr("expect")
var cns1454 = anyStr("then")
var cns1455 = anyStr("Parser")
var cns1456 = anyStr("statlist")
var cns1457 = anyStr("table")
var cns1458 = anyStr("insert")
var cns1459 = anyStr("type")
var cns1460 = anyStr("clause")
var cns1461 = anyStr("cond")
var cns1462 = anyStr("body")
var cns1463 = anyStr("try")
var cns1464 = anyStr("elseif")
var cns1465 = anyStr("Parser")
var cns1466 = anyStr("expr")
var cns1467 = anyStr("expect")
var cns1468 = anyStr("then")
var cns1469 = anyStr("Parser")
var cns1470 = anyStr("statlist")
var cns1471 = anyStr("table")
var cns1472 = anyStr("insert")
var cns1473 = anyStr("type")
var cns1474 = anyStr("clause")
var cns1475 = anyStr("cond")
var cns1476 = anyStr("body")
var cns1477 = anyStr("try")
var cns1478 = anyStr("else")
var cns1479 = anyStr("Parser")
var cns1480 = anyStr("statlist")
var cns1481 = anyStr("match")
var cns1482 = anyStr("end")
var cns1483 = anyStr("type")
var cns1484 = anyStr("if")
var cns1485 = anyStr("clauses")
var cns1486 = anyStr("els")
function Parser_ifstat (reg_0, reg_1) {
  var reg_6, reg_7, reg_10, reg_11, reg_19, reg_22, reg_23, reg_33, reg_38, reg_39, reg_40, reg_48, reg_49, reg_64, reg_65, reg_80, reg_81, reg_82, reg_88, reg_91, reg_92, reg_107, reg_108, reg_111, reg_112;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = lua$get(reg_1.a, cns1449);
  reg_7 = newStack();
  push(reg_7, cns1450);
  reg_10 = first(call(reg_6, reg_7));
  reg_11 = newTable();
  reg_19 = first(call(lua$get(lua$get(reg_1.a, cns1451), cns1452), newStack()));
  reg_22 = lua$get(reg_1.a, cns1453);
  reg_23 = newStack();
  push(reg_23, cns1454);
  reg_25 = call(reg_22, reg_23);
  reg_33 = first(call(lua$get(lua$get(reg_1.a, cns1455), cns1456), newStack()));
  reg_38 = lua$get(lua$get(reg_1.a, cns1457), cns1458);
  reg_39 = newStack();
  push(reg_39, reg_11);
  reg_40 = newTable();
  lua$set(reg_40, cns1459, cns1460);
  lua$set(reg_40, cns1461, reg_19);
  lua$set(reg_40, cns1462, reg_33);
  push(reg_39, reg_40);
  reg_45 = call(reg_38, reg_39);
  loop_1: while (true) {
    reg_48 = lua$get(reg_1.a, cns1463);
    reg_49 = newStack();
    push(reg_49, cns1464);
    if (!tobool(first(call(reg_48, reg_49)))) break loop_1;
    reg_19 = first(call(lua$get(lua$get(reg_1.a, cns1465), cns1466), newStack()));
    reg_64 = lua$get(reg_1.a, cns1467);
    reg_65 = newStack();
    push(reg_65, cns1468);
    reg_67 = call(reg_64, reg_65);
    reg_33 = first(call(lua$get(lua$get(reg_1.a, cns1469), cns1470), newStack()));
    reg_80 = lua$get(lua$get(reg_1.a, cns1471), cns1472);
    reg_81 = newStack();
    push(reg_81, reg_11);
    reg_82 = newTable();
    lua$set(reg_82, cns1473, cns1474);
    lua$set(reg_82, cns1475, reg_19);
    lua$set(reg_82, cns1476, reg_33);
    push(reg_81, reg_82);
    reg_87 = call(reg_80, reg_81);
  }
  reg_88 = newTable();
  reg_91 = lua$get(reg_1.a, cns1477);
  reg_92 = newStack();
  push(reg_92, cns1478);
  if (tobool(first(call(reg_91, reg_92)))) {
    reg_88 = first(call(lua$get(lua$get(reg_1.a, cns1479), cns1480), newStack()));
  }
  reg_107 = lua$get(reg_1.a, cns1481);
  reg_108 = newStack();
  push(reg_108, cns1482);
  push(reg_108, reg_10);
  reg_110 = call(reg_107, reg_108);
  reg_111 = newStack();
  reg_112 = newTable();
  lua$set(reg_112, cns1483, cns1484);
  lua$set(reg_112, cns1485, reg_11);
  lua$set(reg_112, cns1486, reg_88);
  push(reg_111, reg_112);
  return reg_111;
}
var cns1487 = anyStr("Parser")
var cns1488 = anyStr("statement")
var cns1489 = anyStr("get_pos")
var cns1490 = anyStr("try")
var cns1491 = anyStr(";")
var cns1492 = anyStr("check")
var cns1493 = anyStr("if")
var cns1494 = anyStr("Parser")
var cns1495 = anyStr("ifstat")
var cns1496 = anyStr("check")
var cns1497 = anyStr("while")
var cns1498 = anyStr("next")
var cns1499 = anyStr("Parser")
var cns1500 = anyStr("expr")
var cns1501 = anyStr("expect")
var cns1502 = anyStr("do")
var cns1503 = anyStr("Parser")
var cns1504 = anyStr("statlist")
var cns1505 = anyStr("match")
var cns1506 = anyStr("end")
var cns1507 = anyStr("type")
var cns1508 = anyStr("while")
var cns1509 = anyStr("cond")
var cns1510 = anyStr("body")
var cns1511 = anyStr("check")
var cns1512 = anyStr("do")
var cns1513 = anyStr("next")
var cns1514 = anyStr("Parser")
var cns1515 = anyStr("statlist")
var cns1516 = anyStr("match")
var cns1517 = anyStr("end")
var cns1518 = anyStr("type")
var cns1519 = anyStr("do")
var cns1520 = anyStr("body")
var cns1521 = anyStr("check")
var cns1522 = anyStr("for")
var cns1523 = anyStr("Parser")
var cns1524 = anyStr("forstat")
var cns1525 = anyStr("check")
var cns1526 = anyStr("repeat")
var cns1527 = anyStr("next")
var cns1528 = anyStr("Parser")
var cns1529 = anyStr("statlist")
var cns1530 = anyStr("match")
var cns1531 = anyStr("until")
var cns1532 = anyStr("Parser")
var cns1533 = anyStr("expr")
var cns1534 = anyStr("type")
var cns1535 = anyStr("repeat")
var cns1536 = anyStr("cond")
var cns1537 = anyStr("body")
var cns1538 = anyStr("check")
var cns1539 = anyStr("function")
var cns1540 = anyStr("expect")
var cns1541 = anyStr("function")
var cns1542 = anyStr("Parser")
var cns1543 = anyStr("singlevar")
var cns1544 = anyStr("check")
var cns1545 = anyStr(".")
var cns1546 = anyStr("fieldsel")
var cns1547 = anyStr("check")
var cns1548 = anyStr(":")
var cns1549 = anyStr("fieldsel")
var cns1550 = anyStr("Parser")
var cns1551 = anyStr("funcbody")
var cns1552 = anyStr("type")
var cns1553 = anyStr("funcstat")
var cns1554 = anyStr("lhs")
var cns1555 = anyStr("body")
var cns1556 = anyStr("method")
var cns1557 = anyStr("try")
var cns1558 = anyStr("local")
var cns1559 = anyStr("check")
var cns1560 = anyStr("function")
var cns1561 = anyStr("expect")
var cns1562 = anyStr("function")
var cns1563 = anyStr("get_name")
var cns1564 = anyStr("Parser")
var cns1565 = anyStr("funcbody")
var cns1566 = anyStr("type")
var cns1567 = anyStr("localfunc")
var cns1568 = anyStr("name")
var cns1569 = anyStr("body")
var cns1570 = anyStr("table")
var cns1571 = anyStr("insert")
var cns1572 = anyStr("get_name")
var cns1573 = anyStr("try")
var cns1574 = anyStr(",")
var cns1575 = anyStr("try")
var cns1576 = anyStr("=")
var cns1577 = anyStr("Parser")
var cns1578 = anyStr("explist")
var cns1579 = anyStr("type")
var cns1580 = anyStr("local")
var cns1581 = anyStr("names")
var cns1582 = anyStr("values")
var cns1583 = anyStr("try")
var cns1584 = anyStr("::")
var cns1585 = anyStr("get_name")
var cns1586 = anyStr("expect")
var cns1587 = anyStr("::")
var cns1588 = anyStr("type")
var cns1589 = anyStr("label")
var cns1590 = anyStr("name")
var cns1591 = anyStr("try")
var cns1592 = anyStr("return")
var cns1593 = anyStr("check_not")
var cns1594 = anyStr(";")
var cns1595 = anyStr("end")
var cns1596 = anyStr("else")
var cns1597 = anyStr("elseif")
var cns1598 = anyStr("until")
var cns1599 = anyStr("Parser")
var cns1600 = anyStr("explist")
var cns1601 = anyStr("try")
var cns1602 = anyStr(";")
var cns1603 = anyStr("type")
var cns1604 = anyStr("return")
var cns1605 = anyStr("values")
var cns1606 = anyStr("try")
var cns1607 = anyStr("break")
var cns1608 = anyStr("type")
var cns1609 = anyStr("break")
var cns1610 = anyStr("try")
var cns1611 = anyStr("goto")
var cns1612 = anyStr("get_name")
var cns1613 = anyStr("type")
var cns1614 = anyStr("goto")
var cns1615 = anyStr("name")
var cns1616 = anyStr("Parser")
var cns1617 = anyStr("exprstat")
function Parser_statement (reg_0, reg_1) {
  var reg_9, reg_12, reg_13, reg_18, reg_22, reg_23, reg_28, reg_29, reg_40, reg_41, reg_51, reg_59, reg_62, reg_63, reg_73, reg_76, reg_77, reg_80, reg_81, reg_82, reg_90, reg_91, reg_101, reg_109, reg_112, reg_113, reg_116, reg_117, reg_118, reg_125, reg_126, reg_131, reg_132, reg_143, reg_144, reg_154, reg_162, reg_165, reg_166, reg_176, reg_177, reg_178, reg_179, reg_187, reg_188, reg_195, reg_196, reg_199, reg_207, reg_208, reg_211, reg_212, reg_219, reg_220, reg_225, reg_226, reg_233, reg_234, reg_243, reg_244, reg_246, reg_247, reg_248, reg_249, reg_258, reg_259, reg_266, reg_267, reg_274, reg_275, reg_278, reg_284, reg_289, reg_290, reg_292, reg_293, reg_294, reg_295, reg_301, reg_306, reg_307, reg_316, reg_317, reg_323, reg_326, reg_327, reg_340, reg_341, reg_342, reg_350, reg_351, reg_361, reg_364, reg_365, reg_368, reg_369, reg_370, reg_377, reg_378, reg_383, reg_386, reg_387, reg_406, reg_407, reg_410, reg_411, reg_412, reg_419, reg_420, reg_425, reg_426, reg_427, reg_433, reg_434, reg_444, reg_445, reg_446, reg_447, reg_452, reg_460;
  var goto_23=false, goto_47=false, goto_109=false, goto_154=false, goto_178=false, goto_233=false, goto_328=false, goto_386=false, goto_448=false, goto_484=false, goto_543=false, goto_564=false, goto_593=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_9 = first(call(lua$get(reg_1.a, cns1489), newStack()));
  reg_12 = lua$get(reg_1.a, cns1490);
  reg_13 = newStack();
  push(reg_13, cns1491);
  goto_23 = !tobool(first(call(reg_12, reg_13)));
  if (!goto_23) {
    reg_18 = newStack();
    push(reg_18, nil());
    return reg_18;
  }
  if ((goto_23 || false)) {
    goto_23 = false;
    reg_22 = lua$get(reg_1.a, cns1492);
    reg_23 = newStack();
    push(reg_23, cns1493);
    goto_47 = !tobool(first(call(reg_22, reg_23)));
    if (!goto_47) {
      reg_28 = newStack();
      reg_29 = newStack();
      append(reg_29, call(lua$get(lua$get(reg_1.a, cns1494), cns1495), newStack()));
      append(reg_28, call(reg_9, reg_29));
      return reg_28;
    }
    if ((goto_47 || false)) {
      goto_47 = false;
      reg_40 = lua$get(reg_1.a, cns1496);
      reg_41 = newStack();
      push(reg_41, cns1497);
      goto_109 = !tobool(first(call(reg_40, reg_41)));
      if (!goto_109) {
        reg_51 = first(call(lua$get(reg_1.a, cns1498), newStack()));
        reg_59 = first(call(lua$get(lua$get(reg_1.a, cns1499), cns1500), newStack()));
        reg_62 = lua$get(reg_1.a, cns1501);
        reg_63 = newStack();
        push(reg_63, cns1502);
        reg_65 = call(reg_62, reg_63);
        reg_73 = first(call(lua$get(lua$get(reg_1.a, cns1503), cns1504), newStack()));
        reg_76 = lua$get(reg_1.a, cns1505);
        reg_77 = newStack();
        push(reg_77, cns1506);
        push(reg_77, reg_51);
        reg_79 = call(reg_76, reg_77);
        reg_80 = newStack();
        reg_81 = newStack();
        reg_82 = newTable();
        lua$set(reg_82, cns1507, cns1508);
        lua$set(reg_82, cns1509, reg_59);
        lua$set(reg_82, cns1510, reg_73);
        push(reg_81, reg_82);
        append(reg_80, call(reg_9, reg_81));
        return reg_80;
      }
      if ((goto_109 || false)) {
        goto_109 = false;
        reg_90 = lua$get(reg_1.a, cns1511);
        reg_91 = newStack();
        push(reg_91, cns1512);
        goto_154 = !tobool(first(call(reg_90, reg_91)));
        if (!goto_154) {
          reg_101 = first(call(lua$get(reg_1.a, cns1513), newStack()));
          reg_109 = first(call(lua$get(lua$get(reg_1.a, cns1514), cns1515), newStack()));
          reg_112 = lua$get(reg_1.a, cns1516);
          reg_113 = newStack();
          push(reg_113, cns1517);
          push(reg_113, reg_101);
          reg_115 = call(reg_112, reg_113);
          reg_116 = newStack();
          reg_117 = newStack();
          reg_118 = newTable();
          lua$set(reg_118, cns1518, cns1519);
          lua$set(reg_118, cns1520, reg_109);
          push(reg_117, reg_118);
          append(reg_116, call(reg_9, reg_117));
          return reg_116;
        }
        if ((goto_154 || false)) {
          goto_154 = false;
          reg_125 = lua$get(reg_1.a, cns1521);
          reg_126 = newStack();
          push(reg_126, cns1522);
          goto_178 = !tobool(first(call(reg_125, reg_126)));
          if (!goto_178) {
            reg_131 = newStack();
            reg_132 = newStack();
            append(reg_132, call(lua$get(lua$get(reg_1.a, cns1523), cns1524), newStack()));
            append(reg_131, call(reg_9, reg_132));
            return reg_131;
          }
          if ((goto_178 || false)) {
            goto_178 = false;
            reg_143 = lua$get(reg_1.a, cns1525);
            reg_144 = newStack();
            push(reg_144, cns1526);
            goto_233 = !tobool(first(call(reg_143, reg_144)));
            if (!goto_233) {
              reg_154 = first(call(lua$get(reg_1.a, cns1527), newStack()));
              reg_162 = first(call(lua$get(lua$get(reg_1.a, cns1528), cns1529), newStack()));
              reg_165 = lua$get(reg_1.a, cns1530);
              reg_166 = newStack();
              push(reg_166, cns1531);
              push(reg_166, reg_154);
              reg_168 = call(reg_165, reg_166);
              reg_176 = first(call(lua$get(lua$get(reg_1.a, cns1532), cns1533), newStack()));
              reg_177 = newStack();
              reg_178 = newStack();
              reg_179 = newTable();
              lua$set(reg_179, cns1534, cns1535);
              lua$set(reg_179, cns1536, reg_176);
              lua$set(reg_179, cns1537, reg_162);
              push(reg_178, reg_179);
              append(reg_177, call(reg_9, reg_178));
              return reg_177;
            }
            if ((goto_233 || false)) {
              goto_233 = false;
              reg_187 = lua$get(reg_1.a, cns1538);
              reg_188 = newStack();
              push(reg_188, cns1539);
              goto_328 = !tobool(first(call(reg_187, reg_188)));
              if (!goto_328) {
                reg_195 = lua$get(reg_1.a, cns1540);
                reg_196 = newStack();
                push(reg_196, cns1541);
                reg_199 = first(call(reg_195, reg_196));
                reg_207 = first(call(lua$get(lua$get(reg_1.a, cns1542), cns1543), newStack()));
                reg_208 = lua$false();
                loop_2: while (true) {
                  reg_211 = lua$get(reg_1.a, cns1544);
                  reg_212 = newStack();
                  push(reg_212, cns1545);
                  if (!tobool(first(call(reg_211, reg_212)))) break loop_2;
                  reg_219 = lua$get(reg_1.a, cns1546);
                  reg_220 = newStack();
                  push(reg_220, reg_207);
                  reg_207 = first(call(reg_219, reg_220));
                }
                reg_225 = lua$get(reg_1.a, cns1547);
                reg_226 = newStack();
                push(reg_226, cns1548);
                if (tobool(first(call(reg_225, reg_226)))) {
                  reg_233 = lua$get(reg_1.a, cns1549);
                  reg_234 = newStack();
                  push(reg_234, reg_207);
                  push(reg_234, lua$true());
                  reg_207 = first(call(reg_233, reg_234));
                  reg_208 = lua$true();
                }
                reg_243 = lua$get(lua$get(reg_1.a, cns1550), cns1551);
                reg_244 = newStack();
                push(reg_244, reg_199);
                reg_246 = first(call(reg_243, reg_244));
                reg_247 = newStack();
                reg_248 = newStack();
                reg_249 = newTable();
                lua$set(reg_249, cns1552, cns1553);
                lua$set(reg_249, cns1554, reg_207);
                lua$set(reg_249, cns1555, reg_246);
                lua$set(reg_249, cns1556, reg_208);
                push(reg_248, reg_249);
                append(reg_247, call(reg_9, reg_248));
                return reg_247;
              }
              if ((goto_328 || false)) {
                goto_328 = false;
                reg_258 = lua$get(reg_1.a, cns1557);
                reg_259 = newStack();
                push(reg_259, cns1558);
                goto_448 = !tobool(first(call(reg_258, reg_259)));
                if (!goto_448) {
                  reg_266 = lua$get(reg_1.a, cns1559);
                  reg_267 = newStack();
                  push(reg_267, cns1560);
                  goto_386 = !tobool(first(call(reg_266, reg_267)));
                  if (!goto_386) {
                    reg_274 = lua$get(reg_1.a, cns1561);
                    reg_275 = newStack();
                    push(reg_275, cns1562);
                    reg_278 = first(call(reg_274, reg_275));
                    reg_284 = first(call(lua$get(reg_1.a, cns1563), newStack()));
                    reg_289 = lua$get(lua$get(reg_1.a, cns1564), cns1565);
                    reg_290 = newStack();
                    push(reg_290, reg_278);
                    reg_292 = first(call(reg_289, reg_290));
                    reg_293 = newStack();
                    reg_294 = newStack();
                    reg_295 = newTable();
                    lua$set(reg_295, cns1566, cns1567);
                    lua$set(reg_295, cns1568, reg_284);
                    lua$set(reg_295, cns1569, reg_292);
                    push(reg_294, reg_295);
                    append(reg_293, call(reg_9, reg_294));
                    return reg_293;
                  }
                  if ((goto_386 || false)) {
                    goto_386 = false;
                    reg_301 = newTable();
                    loop_1: while (true) {
                      reg_306 = lua$get(lua$get(reg_1.a, cns1570), cns1571);
                      reg_307 = newStack();
                      push(reg_307, reg_301);
                      append(reg_307, call(lua$get(reg_1.a, cns1572), newStack()));
                      reg_313 = call(reg_306, reg_307);
                      reg_316 = lua$get(reg_1.a, cns1573);
                      reg_317 = newStack();
                      push(reg_317, cns1574);
                      if (tobool(not(first(call(reg_316, reg_317))))) break loop_1;
                    }
                    reg_323 = newTable();
                    reg_326 = lua$get(reg_1.a, cns1575);
                    reg_327 = newStack();
                    push(reg_327, cns1576);
                    if (tobool(first(call(reg_326, reg_327)))) {
                      reg_323 = first(call(lua$get(lua$get(reg_1.a, cns1577), cns1578), newStack()));
                    }
                    reg_340 = newStack();
                    reg_341 = newStack();
                    reg_342 = newTable();
                    lua$set(reg_342, cns1579, cns1580);
                    lua$set(reg_342, cns1581, reg_301);
                    lua$set(reg_342, cns1582, reg_323);
                    push(reg_341, reg_342);
                    append(reg_340, call(reg_9, reg_341));
                    return reg_340;
                  }
                }
                if ((goto_448 || false)) {
                  goto_448 = false;
                  reg_350 = lua$get(reg_1.a, cns1583);
                  reg_351 = newStack();
                  push(reg_351, cns1584);
                  goto_484 = !tobool(first(call(reg_350, reg_351)));
                  if (!goto_484) {
                    reg_361 = first(call(lua$get(reg_1.a, cns1585), newStack()));
                    reg_364 = lua$get(reg_1.a, cns1586);
                    reg_365 = newStack();
                    push(reg_365, cns1587);
                    reg_367 = call(reg_364, reg_365);
                    reg_368 = newStack();
                    reg_369 = newStack();
                    reg_370 = newTable();
                    lua$set(reg_370, cns1588, cns1589);
                    lua$set(reg_370, cns1590, reg_361);
                    push(reg_369, reg_370);
                    append(reg_368, call(reg_9, reg_369));
                    return reg_368;
                  }
                  if ((goto_484 || false)) {
                    goto_484 = false;
                    reg_377 = lua$get(reg_1.a, cns1591);
                    reg_378 = newStack();
                    push(reg_378, cns1592);
                    goto_543 = !tobool(first(call(reg_377, reg_378)));
                    if (!goto_543) {
                      reg_383 = newTable();
                      reg_386 = lua$get(reg_1.a, cns1593);
                      reg_387 = newStack();
                      push(reg_387, cns1594);
                      push(reg_387, cns1595);
                      push(reg_387, cns1596);
                      push(reg_387, cns1597);
                      push(reg_387, cns1598);
                      if (tobool(first(call(reg_386, reg_387)))) {
                        reg_383 = first(call(lua$get(lua$get(reg_1.a, cns1599), cns1600), newStack()));
                      }
                      reg_406 = lua$get(reg_1.a, cns1601);
                      reg_407 = newStack();
                      push(reg_407, cns1602);
                      reg_409 = call(reg_406, reg_407);
                      reg_410 = newStack();
                      reg_411 = newStack();
                      reg_412 = newTable();
                      lua$set(reg_412, cns1603, cns1604);
                      lua$set(reg_412, cns1605, reg_383);
                      push(reg_411, reg_412);
                      append(reg_410, call(reg_9, reg_411));
                      return reg_410;
                    }
                    if ((goto_543 || false)) {
                      goto_543 = false;
                      reg_419 = lua$get(reg_1.a, cns1606);
                      reg_420 = newStack();
                      push(reg_420, cns1607);
                      goto_564 = !tobool(first(call(reg_419, reg_420)));
                      if (!goto_564) {
                        reg_425 = newStack();
                        reg_426 = newStack();
                        reg_427 = newTable();
                        lua$set(reg_427, cns1608, cns1609);
                        push(reg_426, reg_427);
                        append(reg_425, call(reg_9, reg_426));
                        return reg_425;
                      }
                      if ((goto_564 || false)) {
                        goto_564 = false;
                        reg_433 = lua$get(reg_1.a, cns1610);
                        reg_434 = newStack();
                        push(reg_434, cns1611);
                        goto_593 = !tobool(first(call(reg_433, reg_434)));
                        if (!goto_593) {
                          reg_444 = first(call(lua$get(reg_1.a, cns1612), newStack()));
                          reg_445 = newStack();
                          reg_446 = newStack();
                          reg_447 = newTable();
                          lua$set(reg_447, cns1613, cns1614);
                          lua$set(reg_447, cns1615, reg_444);
                          push(reg_446, reg_447);
                          append(reg_445, call(reg_9, reg_446));
                          return reg_445;
                        }
                        if ((goto_593 || false)) {
                          goto_593 = false;
                          reg_452 = newStack();
                          append(reg_452, call(lua$get(lua$get(reg_1.a, cns1616), cns1617), newStack()));
                          return reg_452;
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  reg_460 = newStack();
  return reg_460;
}
var cns1618 = anyStr("Parser")
var cns1619 = anyStr("statlist")
var cns1620 = anyStr("check_not")
var cns1621 = anyStr("end")
var cns1622 = anyStr("else")
var cns1623 = anyStr("elseif")
var cns1624 = anyStr("until")
var cns1625 = anyStr("Parser")
var cns1626 = anyStr("statement")
var cns1627 = anyStr("table")
var cns1628 = anyStr("insert")
var cns1629 = anyStr("type")
var cns1630 = anyStr("return")
function Parser_statlist (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_23, reg_29, reg_30, reg_37;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newTable();
  loop_1: while (true) {
    reg_7 = lua$get(reg_1.a, cns1620);
    reg_8 = newStack();
    push(reg_8, cns1621);
    push(reg_8, cns1622);
    push(reg_8, cns1623);
    push(reg_8, cns1624);
    if (!tobool(first(call(reg_7, reg_8)))) break loop_1;
    reg_23 = first(call(lua$get(lua$get(reg_1.a, cns1625), cns1626), newStack()));
    if (tobool(reg_23)) {
      reg_29 = lua$get(lua$get(reg_1.a, cns1627), cns1628);
      reg_30 = newStack();
      push(reg_30, reg_4);
      push(reg_30, reg_23);
      reg_31 = call(reg_29, reg_30);
      if (tobool(eq(lua$get(reg_23, cns1629), cns1630))) {
        if (true) break loop_1;
      }
    }
  }
  reg_37 = newStack();
  push(reg_37, reg_4);
  return reg_37;
}
var cns1631 = anyStr("Parser")
var cns1632 = anyStr("statlist")
var cns1633 = anyStr("token")
var cns1634 = anyStr("err")
var cns1635 = anyStr("end of file expected")
function function$72 (reg_0, reg_1) {
  var reg_11, reg_18, reg_19, reg_22;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_11 = first(call(lua$get(lua$get(reg_1.a, cns1631), cns1632), newStack()));
  if (tobool(lua$get(reg_1.a, cns1633))) {
    reg_18 = lua$get(reg_1.a, cns1634);
    reg_19 = newStack();
    push(reg_19, cns1635);
    reg_21 = call(reg_18, reg_19);
  }
  reg_22 = newStack();
  push(reg_22, reg_11);
  return reg_22;
}
var cns1636 = anyStr("parse_program")
var cns1637 = anyStr("Parser")
var cns1638 = anyStr("parse")
var cns1639 = anyStr("xpcall")
function function$73 (reg_0, reg_1) {
  var reg_5, reg_6;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  push(reg_6, lua$true());
  append(reg_6, call(reg_5, newStack()));
  return reg_6;
}
var cns1640 = anyStr("xpcall")
var cns1641 = anyStr("xpcall")
var cns1642 = anyStr("parse_program")
var cns1643 = anyStr("Parser")
var cns1644 = anyStr("error")
var cns1645 = anyStr("debug")
var cns1646 = anyStr("traceback")
var cns1647 = parseNum("2")
var cns1648 = anyStr("Parser")
var cns1649 = anyStr("trace")
var cns1650 = anyStr("debug")
var cns1651 = anyStr("traceback")
var cns1652 = parseNum("4")
function function$74 (reg_0, reg_1) {
  var reg_2, reg_5, reg_18, reg_19, reg_25, reg_26, reg_31, reg_32, reg_36;
  var goto_26=false;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  goto_26 = !tobool(eq(lua$get(lua$get(reg_2.a, cns1643), cns1644), nil()));
  if (!goto_26) {
    reg_18 = lua$get(lua$get(reg_2.a, cns1645), cns1646);
    reg_19 = newStack();
    push(reg_19, reg_5);
    push(reg_19, cns1647);
    reg_1.b = first(call(reg_18, reg_19));
  }
  if ((goto_26 || false)) {
    goto_26 = false;
    reg_25 = lua$get(reg_2.a, cns1648);
    reg_26 = cns1649;
    reg_31 = lua$get(lua$get(reg_2.a, cns1650), cns1651);
    reg_32 = newStack();
    push(reg_32, reg_5);
    push(reg_32, cns1652);
    lua$set(reg_25, reg_26, first(call(reg_31, reg_32)));
  }
  reg_36 = newStack();
  return reg_36;
}
var cns1653 = anyStr("status")
var cns1654 = anyStr("prog")
var cns1655 = anyStr("print")
var cns1656 = anyStr("os")
var cns1657 = anyStr("exit")
var cns1658 = parseNum("1")
var cns1659 = anyStr("status")
var cns1660 = anyStr("prog")
var cns1661 = anyStr("Parser")
var cns1662 = anyStr("error")
var cns1663 = anyStr("tk")
var cns1664 = anyStr("error")
var cns1665 = anyStr("tk")
function Parser_parse (reg_0, reg_1) {
  var reg_3, reg_11, reg_16, reg_17, reg_23, reg_24, reg_27, reg_34, reg_35, reg_42, reg_43, reg_50, reg_60, reg_67, reg_68, reg_73;
  var goto_68=false, goto_84=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_3.b = nil();
  if (tobool(not(lua$get(reg_1.a, cns1639)))) {
    reg_11 = anyFn((function (a) { return function$73(a,this) }).bind(reg_3));
    lua$set(reg_1.a, cns1640, reg_11);
  }
  reg_16 = lua$get(reg_1.a, cns1641);
  reg_17 = newStack();
  push(reg_17, lua$get(reg_1.a, cns1642));
  push(reg_17, anyFn((function (a) { return function$74(a,this) }).bind(reg_3)));
  reg_23 = call(reg_16, reg_17);
  reg_24 = next(reg_23);
  lua$set(reg_1.a, cns1653, reg_24);
  reg_27 = next(reg_23);
  lua$set(reg_1.a, cns1654, reg_27);
  if (tobool(reg_3.b)) {
    reg_34 = lua$get(reg_1.a, cns1655);
    reg_35 = newStack();
    push(reg_35, reg_3.b);
    reg_37 = call(reg_34, reg_35);
    reg_42 = lua$get(lua$get(reg_1.a, cns1656), cns1657);
    reg_43 = newStack();
    push(reg_43, cns1658);
    reg_45 = call(reg_42, reg_43);
  }
  goto_68 = !tobool(lua$get(reg_1.a, cns1659));
  if (!goto_68) {
    reg_50 = newStack();
    push(reg_50, lua$get(reg_1.a, cns1660));
    return reg_50;
  }
  if ((goto_68 || false)) {
    goto_68 = false;
    goto_84 = !tobool(lua$get(lua$get(reg_1.a, cns1661), cns1662));
    if (!goto_84) {
      reg_60 = newStack();
      push(reg_60, nil());
      push(reg_60, lua$get(reg_1.a, cns1663));
      return reg_60;
    }
    if ((goto_84 || false)) {
      goto_84 = false;
      reg_67 = lua$get(reg_1.a, cns1664);
      reg_68 = newStack();
      push(reg_68, lua$get(reg_1.a, cns1665));
      reg_72 = call(reg_67, reg_68);
    }
  }
  reg_73 = newStack();
  return reg_73;
}
var cns1666 = anyStr("Parser")
function lua_parser_parser$lua_main (reg_0) {
  var reg_2, reg_5, reg_6, reg_8, reg_14, reg_17, reg_22, reg_23, reg_27, reg_31, reg_35, reg_39, reg_43, reg_47, reg_51, reg_55, reg_59, reg_63, reg_68, reg_69, reg_73, reg_77, reg_82, reg_83, reg_88, reg_89, reg_92, reg_93, reg_94, reg_99, reg_100, reg_105, reg_106, reg_111, reg_112, reg_117, reg_118, reg_123, reg_124, reg_129, reg_130, reg_135, reg_136, reg_141, reg_142, reg_147, reg_148, reg_153, reg_154, reg_159, reg_160, reg_165, reg_166, reg_171, reg_172, reg_177, reg_178, reg_183, reg_184, reg_189, reg_190, reg_195, reg_196, reg_201, reg_202, reg_207, reg_208, reg_213, reg_214, reg_221, reg_226, reg_227, reg_232, reg_233, reg_238, reg_239, reg_243, reg_248, reg_249, reg_254, reg_255, reg_260, reg_261, reg_266, reg_267, reg_272, reg_273, reg_277, reg_282, reg_283, reg_286;
  reg_2 = {a: nil()};
  reg_2.a = reg_0;
  reg_5 = lua$get(reg_2.a, cns503);
  reg_6 = newStack();
  push(reg_6, newTable());
  reg_8 = newTable();
  lua$set(reg_8, cns504, reg_2.a);
  push(reg_6, reg_8);
  reg_2.a = first(call(reg_5, reg_6));
  reg_14 = first(lua_parser_lexer$lua_main(reg_0));
  lua$set(reg_2.a, cns890, reg_14);
  reg_17 = newTable();
  lua$set(reg_2.a, cns891, reg_17);
  reg_22 = lua$get(reg_2.a, cns892);
  reg_23 = cns893;
  lua$set(reg_22, reg_23, anyFn((function (a) { return Parser_open(a,this) }).bind(reg_2)));
  reg_27 = anyFn((function (a) { return lua_parser_parser$function(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns914, reg_27);
  reg_31 = anyFn((function (a) { return function$59(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns930, reg_31);
  reg_35 = anyFn((function (a) { return function$60(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns936, reg_35);
  reg_39 = anyFn((function (a) { return function$61(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns939, reg_39);
  reg_43 = anyFn((function (a) { return function$62(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns942, reg_43);
  reg_47 = anyFn((function (a) { return function$63(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns951, reg_47);
  reg_51 = anyFn((function (a) { return function$64(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns959, reg_51);
  reg_55 = anyFn((function (a) { return function$65(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns963, reg_55);
  reg_59 = anyFn((function (a) { return function$66(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns969, reg_59);
  reg_63 = anyFn((function (a) { return function$67(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns979, reg_63);
  reg_68 = lua$get(reg_2.a, cns980);
  reg_69 = cns981;
  lua$set(reg_68, reg_69, anyFn((function (a) { return Parser_singlevar(a,this) }).bind(reg_2)));
  reg_73 = anyFn((function (a) { return function$69(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns1040, reg_73);
  reg_77 = anyFn((function (a) { return function$70(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns1075, reg_77);
  reg_82 = lua$get(reg_2.a, cns1076);
  reg_83 = cns1077;
  lua$set(reg_82, reg_83, anyFn((function (a) { return Parser_suffixedexp(a,this) }).bind(reg_2)));
  reg_88 = lua$get(reg_2.a, cns1112);
  reg_89 = cns1113;
  lua$set(reg_88, reg_89, anyFn((function (a) { return Parser_simpleexp(a,this) }).bind(reg_2)));
  reg_92 = newTable();
  reg_93 = cns1153;
  reg_94 = newTable();
  lua$set(reg_94, cns1154, cns1155);
  lua$set(reg_94, cns1156, cns1157);
  lua$set(reg_92, reg_93, reg_94);
  reg_99 = cns1158;
  reg_100 = newTable();
  lua$set(reg_100, cns1159, cns1160);
  lua$set(reg_100, cns1161, cns1162);
  lua$set(reg_92, reg_99, reg_100);
  reg_105 = cns1163;
  reg_106 = newTable();
  lua$set(reg_106, cns1164, cns1165);
  lua$set(reg_106, cns1166, cns1167);
  lua$set(reg_92, reg_105, reg_106);
  reg_111 = cns1168;
  reg_112 = newTable();
  lua$set(reg_112, cns1169, cns1170);
  lua$set(reg_112, cns1171, cns1172);
  lua$set(reg_92, reg_111, reg_112);
  reg_117 = cns1173;
  reg_118 = newTable();
  lua$set(reg_118, cns1174, cns1175);
  lua$set(reg_118, cns1176, cns1177);
  lua$set(reg_92, reg_117, reg_118);
  reg_123 = cns1178;
  reg_124 = newTable();
  lua$set(reg_124, cns1179, cns1180);
  lua$set(reg_124, cns1181, cns1182);
  lua$set(reg_92, reg_123, reg_124);
  reg_129 = cns1183;
  reg_130 = newTable();
  lua$set(reg_130, cns1184, cns1185);
  lua$set(reg_130, cns1186, cns1187);
  lua$set(reg_92, reg_129, reg_130);
  reg_135 = cns1188;
  reg_136 = newTable();
  lua$set(reg_136, cns1189, cns1190);
  lua$set(reg_136, cns1191, cns1192);
  lua$set(reg_92, reg_135, reg_136);
  reg_141 = cns1193;
  reg_142 = newTable();
  lua$set(reg_142, cns1194, cns1195);
  lua$set(reg_142, cns1196, cns1197);
  lua$set(reg_92, reg_141, reg_142);
  reg_147 = cns1198;
  reg_148 = newTable();
  lua$set(reg_148, cns1199, cns1200);
  lua$set(reg_148, cns1201, cns1202);
  lua$set(reg_92, reg_147, reg_148);
  reg_153 = cns1203;
  reg_154 = newTable();
  lua$set(reg_154, cns1204, cns1205);
  lua$set(reg_154, cns1206, cns1207);
  lua$set(reg_92, reg_153, reg_154);
  reg_159 = cns1208;
  reg_160 = newTable();
  lua$set(reg_160, cns1209, cns1210);
  lua$set(reg_160, cns1211, cns1212);
  lua$set(reg_92, reg_159, reg_160);
  reg_165 = cns1213;
  reg_166 = newTable();
  lua$set(reg_166, cns1214, cns1215);
  lua$set(reg_166, cns1216, cns1217);
  lua$set(reg_92, reg_165, reg_166);
  reg_171 = cns1218;
  reg_172 = newTable();
  lua$set(reg_172, cns1219, cns1220);
  lua$set(reg_172, cns1221, cns1222);
  lua$set(reg_92, reg_171, reg_172);
  reg_177 = cns1223;
  reg_178 = newTable();
  lua$set(reg_178, cns1224, cns1225);
  lua$set(reg_178, cns1226, cns1227);
  lua$set(reg_92, reg_177, reg_178);
  reg_183 = cns1228;
  reg_184 = newTable();
  lua$set(reg_184, cns1229, cns1230);
  lua$set(reg_184, cns1231, cns1232);
  lua$set(reg_92, reg_183, reg_184);
  reg_189 = cns1233;
  reg_190 = newTable();
  lua$set(reg_190, cns1234, cns1235);
  lua$set(reg_190, cns1236, cns1237);
  lua$set(reg_92, reg_189, reg_190);
  reg_195 = cns1238;
  reg_196 = newTable();
  lua$set(reg_196, cns1239, cns1240);
  lua$set(reg_196, cns1241, cns1242);
  lua$set(reg_92, reg_195, reg_196);
  reg_201 = cns1243;
  reg_202 = newTable();
  lua$set(reg_202, cns1244, cns1245);
  lua$set(reg_202, cns1246, cns1247);
  lua$set(reg_92, reg_201, reg_202);
  reg_207 = cns1248;
  reg_208 = newTable();
  lua$set(reg_208, cns1249, cns1250);
  lua$set(reg_208, cns1251, cns1252);
  lua$set(reg_92, reg_207, reg_208);
  reg_213 = cns1253;
  reg_214 = newTable();
  lua$set(reg_214, cns1254, cns1255);
  lua$set(reg_214, cns1256, cns1257);
  lua$set(reg_92, reg_213, reg_214);
  lua$set(reg_2.a, cns1258, reg_92);
  reg_221 = cns1259;
  lua$set(reg_2.a, cns1260, reg_221);
  reg_226 = lua$get(reg_2.a, cns1261);
  reg_227 = cns1262;
  lua$set(reg_226, reg_227, anyFn((function (a) { return Parser_expr(a,this) }).bind(reg_2)));
  reg_232 = lua$get(reg_2.a, cns1301);
  reg_233 = cns1302;
  lua$set(reg_232, reg_233, anyFn((function (a) { return Parser_explist(a,this) }).bind(reg_2)));
  reg_238 = lua$get(reg_2.a, cns1309);
  reg_239 = cns1310;
  lua$set(reg_238, reg_239, anyFn((function (a) { return Parser_funcbody(a,this) }).bind(reg_2)));
  reg_243 = anyFn((function (a) { return function$71(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns1348, reg_243);
  reg_248 = lua$get(reg_2.a, cns1349);
  reg_249 = cns1350;
  lua$set(reg_248, reg_249, anyFn((function (a) { return Parser_exprstat(a,this) }).bind(reg_2)));
  reg_254 = lua$get(reg_2.a, cns1384);
  reg_255 = cns1385;
  lua$set(reg_254, reg_255, anyFn((function (a) { return Parser_forstat(a,this) }).bind(reg_2)));
  reg_260 = lua$get(reg_2.a, cns1447);
  reg_261 = cns1448;
  lua$set(reg_260, reg_261, anyFn((function (a) { return Parser_ifstat(a,this) }).bind(reg_2)));
  reg_266 = lua$get(reg_2.a, cns1487);
  reg_267 = cns1488;
  lua$set(reg_266, reg_267, anyFn((function (a) { return Parser_statement(a,this) }).bind(reg_2)));
  reg_272 = lua$get(reg_2.a, cns1618);
  reg_273 = cns1619;
  lua$set(reg_272, reg_273, anyFn((function (a) { return Parser_statlist(a,this) }).bind(reg_2)));
  reg_277 = anyFn((function (a) { return function$72(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns1636, reg_277);
  reg_282 = lua$get(reg_2.a, cns1637);
  reg_283 = cns1638;
  lua$set(reg_282, reg_283, anyFn((function (a) { return Parser_parse(a,this) }).bind(reg_2)));
  reg_286 = newStack();
  push(reg_286, lua$get(reg_2.a, cns1666));
  return reg_286;
}
var cns1667 = parseNum("1")
var cns1668 = anyStr("")
var cns1669 = anyStr("type")
var cns1670 = anyStr("table")
var cns1671 = parseNum("0")
var cns1672 = anyStr("tostring")
var cns1673 = parseNum("0")
var cns1674 = anyStr("[")
var cns1675 = parseNum("1")
var cns1676 = parseNum("1")
var cns1677 = parseNum("0")
var cns1678 = parseNum("1")
var cns1679 = anyStr(",")
var cns1680 = anyStr("\n")
var cns1681 = anyStr("  ")
var cns1682 = anyStr("tostr")
var cns1683 = parseNum("1")
var cns1684 = anyStr("  ")
var cns1685 = anyStr("pairs")
var cns1686 = anyStr("type")
var cns1687 = anyStr("number")
var cns1688 = anyStr(",\n")
var cns1689 = anyStr("  ")
var cns1690 = anyStr("tostring")
var cns1691 = anyStr(" = ")
var cns1692 = anyStr("tostr")
var cns1693 = parseNum("1")
var cns1694 = anyStr("  ")
var cns1695 = anyStr("\n")
var cns1696 = anyStr("]")
var cns1697 = anyStr("{")
var cns1698 = anyStr("pairs")
var cns1699 = anyStr(",")
var cns1700 = anyStr("\n")
var cns1701 = anyStr("  ")
var cns1702 = anyStr("tostring")
var cns1703 = anyStr(" = ")
var cns1704 = anyStr("tostr")
var cns1705 = parseNum("1")
var cns1706 = anyStr("  ")
var cns1707 = anyStr("\n")
var cns1708 = anyStr("}")
var cns1709 = anyStr("[]")
function aulua_helpers$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_15, reg_16, reg_20, reg_25, reg_28, reg_29, reg_35, reg_36, reg_37, reg_38, reg_40, reg_51, reg_52, reg_55, reg_56, reg_71, reg_72, reg_73, reg_74, reg_75, reg_76, reg_77, reg_78, reg_79, reg_80, reg_86, reg_87, reg_93, reg_94, reg_97, reg_98, reg_100, reg_101, reg_104, reg_105, reg_118, reg_119, reg_124, reg_125, reg_128, reg_129, reg_130, reg_131, reg_132, reg_133, reg_134, reg_135, reg_136, reg_137, reg_145, reg_146, reg_149, reg_150, reg_152, reg_153, reg_156, reg_157, reg_170, reg_176, reg_178;
  var goto_60=false, goto_194=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = reg_5;
  if (!tobool(reg_5)) {
    reg_7 = cns1667;
  }
  reg_5 = reg_7;
  reg_10 = reg_6;
  if (!tobool(reg_6)) {
    reg_10 = cns1668;
  }
  reg_6 = reg_10;
  reg_15 = lua$get(reg_1.a, cns1669);
  reg_16 = newStack();
  push(reg_16, reg_4);
  reg_20 = ne(first(call(reg_15, reg_16)), cns1670);
  if (!tobool(reg_20)) {
    reg_20 = eq(reg_5, cns1671);
  }
  if (tobool(reg_20)) {
    reg_25 = newStack();
    reg_28 = lua$get(reg_1.a, cns1672);
    reg_29 = newStack();
    push(reg_29, reg_4);
    append(reg_25, call(reg_28, reg_29));
    return reg_25;
  }
  if (tobool(gt(lua$length(reg_4), cns1673))) {
    reg_35 = cns1674;
    reg_36 = cns1675;
    reg_37 = lua$length(reg_4);
    reg_38 = cns1676;
    reg_40 = lt(reg_38, cns1677);
    loop_3: while (true) {
      goto_60 = tobool(reg_40);
      if (!goto_60) {
        if (tobool(gt(reg_36, reg_37))) break loop_3;
      }
      if ((goto_60 || false)) {
        goto_60 = false;
        if (tobool(lt(reg_36, reg_37))) break loop_3;
      }
      if (tobool(gt(reg_36, cns1678))) {
        reg_35 = concat(reg_35, cns1679);
      }
      reg_51 = cns1680;
      reg_52 = cns1681;
      reg_55 = lua$get(reg_1.a, cns1682);
      reg_56 = newStack();
      push(reg_56, lua$get(reg_4, reg_36));
      push(reg_56, sub(reg_5, cns1683));
      push(reg_56, concat(reg_6, cns1684));
      reg_35 = concat(reg_35, concat(reg_51, concat(reg_6, concat(reg_52, first(call(reg_55, reg_56))))));
      reg_36 = add(reg_36, reg_38);
    }
    reg_71 = lua$get(reg_1.a, cns1685);
    reg_72 = newStack();
    push(reg_72, reg_4);
    reg_73 = call(reg_71, reg_72);
    reg_74 = next(reg_73);
    reg_75 = next(reg_73);
    reg_76 = next(reg_73);
    loop_2: while (true) {
      reg_77 = newStack();
      push(reg_77, reg_75);
      push(reg_77, reg_76);
      reg_78 = call(reg_74, reg_77);
      reg_79 = next(reg_78);
      reg_76 = reg_79;
      reg_80 = next(reg_78);
      if (tobool(eq(reg_76, nil()))) break loop_2;
      reg_86 = lua$get(reg_1.a, cns1686);
      reg_87 = newStack();
      push(reg_87, reg_79);
      if (tobool(ne(first(call(reg_86, reg_87)), cns1687))) {
        reg_93 = cns1688;
        reg_94 = cns1689;
        reg_97 = lua$get(reg_1.a, cns1690);
        reg_98 = newStack();
        push(reg_98, reg_79);
        reg_100 = first(call(reg_97, reg_98));
        reg_101 = cns1691;
        reg_104 = lua$get(reg_1.a, cns1692);
        reg_105 = newStack();
        push(reg_105, reg_80);
        push(reg_105, sub(reg_5, cns1693));
        push(reg_105, concat(reg_6, cns1694));
        reg_35 = concat(reg_35, concat(reg_93, concat(reg_6, concat(reg_94, concat(reg_100, concat(reg_101, first(call(reg_104, reg_105))))))));
      }
    }
    reg_118 = newStack();
    reg_119 = cns1695;
    push(reg_118, concat(reg_35, concat(reg_119, concat(reg_6, cns1696))));
    return reg_118;
  }
  reg_124 = lua$true();
  reg_125 = cns1697;
  reg_128 = lua$get(reg_1.a, cns1698);
  reg_129 = newStack();
  push(reg_129, reg_4);
  reg_130 = call(reg_128, reg_129);
  reg_131 = next(reg_130);
  reg_132 = next(reg_130);
  reg_133 = next(reg_130);
  loop_1: while (true) {
    reg_134 = newStack();
    push(reg_134, reg_132);
    push(reg_134, reg_133);
    reg_135 = call(reg_131, reg_134);
    reg_136 = next(reg_135);
    reg_133 = reg_136;
    reg_137 = next(reg_135);
    if (tobool(eq(reg_133, nil()))) break loop_1;
    goto_194 = !tobool(reg_124);
    if (!goto_194) {
      reg_124 = lua$false();
    }
    if ((goto_194 || false)) {
      goto_194 = false;
      reg_125 = concat(reg_125, cns1699);
    }
    reg_145 = cns1700;
    reg_146 = cns1701;
    reg_149 = lua$get(reg_1.a, cns1702);
    reg_150 = newStack();
    push(reg_150, reg_136);
    reg_152 = first(call(reg_149, reg_150));
    reg_153 = cns1703;
    reg_156 = lua$get(reg_1.a, cns1704);
    reg_157 = newStack();
    push(reg_157, reg_137);
    push(reg_157, sub(reg_5, cns1705));
    push(reg_157, concat(reg_6, cns1706));
    reg_125 = concat(reg_125, concat(reg_145, concat(reg_6, concat(reg_146, concat(reg_152, concat(reg_153, first(call(reg_156, reg_157))))))));
  }
  reg_170 = cns1707;
  reg_125 = concat(reg_125, concat(reg_170, concat(reg_6, cns1708)));
  if (tobool(reg_124)) {
    reg_176 = newStack();
    push(reg_176, cns1709);
    return reg_176;
  }
  reg_178 = newStack();
  push(reg_178, reg_125);
  return reg_178;
}
var cns1710 = anyStr("tostr")
var cns1711 = anyStr(", at ")
var cns1712 = anyStr("line")
var cns1713 = anyStr(":")
var cns1714 = anyStr("column")
var cns1715 = anyStr("error")
function function$75 (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_9, reg_10, reg_19, reg_20, reg_22;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  if (tobool(reg_5)) {
    reg_7 = cns1711;
    reg_9 = lua$get(reg_5, cns1712);
    reg_10 = cns1713;
    reg_4 = concat(reg_4, concat(reg_7, concat(reg_9, concat(reg_10, lua$get(reg_5, cns1714)))));
  }
  reg_19 = lua$get(reg_1.a, cns1715);
  reg_20 = newStack();
  push(reg_20, reg_4);
  reg_21 = call(reg_19, reg_20);
  reg_22 = newStack();
  return reg_22;
}
var cns1716 = anyStr("err")
function aulua_helpers$lua_main (reg_0) {
  var reg_2, reg_4, reg_8, reg_11;
  reg_2 = {a: nil()};
  reg_2.a = reg_0;
  reg_4 = anyFn((function (a) { return aulua_helpers$function(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns1710, reg_4);
  reg_8 = anyFn((function (a) { return function$75(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns1716, reg_8);
  reg_11 = newStack();
  return reg_11;
}
var cns1717 = anyStr("Module")
var cns1718 = anyStr("Module")
var cns1719 = anyStr("__index")
var cns1720 = anyStr("Module")
var cns1721 = anyStr("Module")
var cns1722 = anyStr("type")
var cns1723 = anyStr("id")
var cns1724 = anyStr("types")
var cns1725 = anyStr("name")
var cns1726 = anyStr("module")
var cns1727 = anyStr("id")
var cns1728 = anyStr("table")
var cns1729 = anyStr("insert")
var cns1730 = anyStr("types")
var cns1731 = anyStr("setmetatable")
var cns1732 = anyStr("__tostring")
var cns1733 = anyStr("fullname")
var cns1734 = anyStr(".")
function aulua_basics$function (reg_0, reg_1) {
  var reg_6, reg_7, reg_9, reg_10, reg_12;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = reg_1.b;
  reg_9 = lua$get(reg_7, cns1733);
  reg_10 = newStack();
  push(reg_10, reg_7);
  reg_12 = first(call(reg_9, reg_10));
  push(reg_6, concat(reg_12, concat(cns1734, reg_1.c)));
  return reg_6;
}
function Module_type (reg_0, reg_1) {
  var reg_2, reg_3, reg_6, reg_7, reg_14, reg_22, reg_23, reg_30, reg_31, reg_32, reg_33, reg_37;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = newTable();
  reg_7 = cns1723;
  lua$set(reg_6, reg_7, lua$length(lua$get(reg_1.a, cns1724)));
  lua$set(reg_6, cns1725, reg_3.c);
  reg_14 = cns1726;
  lua$set(reg_6, reg_14, lua$get(reg_3.b, cns1727));
  reg_22 = lua$get(lua$get(reg_1.a, cns1728), cns1729);
  reg_23 = newStack();
  push(reg_23, lua$get(reg_1.a, cns1730));
  push(reg_23, reg_6);
  reg_27 = call(reg_22, reg_23);
  reg_30 = lua$get(reg_1.a, cns1731);
  reg_31 = newStack();
  push(reg_31, reg_6);
  reg_32 = newTable();
  reg_33 = cns1732;
  lua$set(reg_32, reg_33, anyFn((function (a) { return aulua_basics$function(a,this) }).bind(reg_3)));
  push(reg_31, reg_32);
  reg_36 = call(reg_30, reg_31);
  reg_37 = newStack();
  push(reg_37, reg_6);
  return reg_37;
}
var cns1735 = anyStr("Module")
var cns1736 = anyStr("func")
var cns1737 = anyStr("_id")
var cns1738 = anyStr("funcs")
var cns1739 = anyStr("name")
var cns1740 = anyStr("ins")
var cns1741 = anyStr("outs")
var cns1742 = anyStr("module")
var cns1743 = anyStr("id")
var cns1744 = anyStr("id")
var cns1745 = anyStr("_id")
function f_id (reg_0, reg_1) {
  var reg_5, reg_6;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  push(reg_6, lua$get(reg_5, cns1745));
  return reg_6;
}
var cns1746 = anyStr("ipairs")
var cns1747 = anyStr("ins")
var cns1748 = anyStr("id")
var cns1749 = anyStr("ipairs")
var cns1750 = anyStr("outs")
var cns1751 = anyStr("id")
var cns1752 = anyStr("table")
var cns1753 = anyStr("insert")
var cns1754 = anyStr("funcs")
var cns1755 = anyStr("setmetatable")
var cns1756 = anyStr("__tostring")
var cns1757 = anyStr("fullname")
var cns1758 = anyStr(".")
function function$76 (reg_0, reg_1) {
  var reg_6, reg_7, reg_9, reg_10, reg_12;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = reg_1.b;
  reg_9 = lua$get(reg_7, cns1757);
  reg_10 = newStack();
  push(reg_10, reg_7);
  reg_12 = first(call(reg_9, reg_10));
  push(reg_6, concat(reg_12, concat(cns1758, reg_1.c)));
  return reg_6;
}
function Module_func (reg_0, reg_1) {
  var reg_2, reg_3, reg_6, reg_7, reg_8, reg_9, reg_20, reg_24, reg_29, reg_30, reg_31, reg_32, reg_33, reg_34, reg_35, reg_36, reg_37, reg_38, reg_43, reg_48, reg_49, reg_50, reg_51, reg_52, reg_53, reg_54, reg_55, reg_56, reg_57, reg_62, reg_69, reg_70, reg_77, reg_78, reg_79, reg_80, reg_84;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_8 = newTable();
  reg_9 = cns1737;
  lua$set(reg_8, reg_9, lua$length(lua$get(reg_1.a, cns1738)));
  lua$set(reg_8, cns1739, reg_3.c);
  lua$set(reg_8, cns1740, newTable());
  lua$set(reg_8, cns1741, newTable());
  reg_20 = cns1742;
  lua$set(reg_8, reg_20, lua$get(reg_3.b, cns1743));
  reg_24 = cns1744;
  lua$set(reg_8, reg_24, anyFn((function (a) { return f_id(a,this) }).bind(reg_3)));
  reg_29 = lua$get(reg_1.a, cns1746);
  reg_30 = newStack();
  push(reg_30, reg_6);
  reg_31 = call(reg_29, reg_30);
  reg_32 = next(reg_31);
  reg_33 = next(reg_31);
  reg_34 = next(reg_31);
  loop_2: while (true) {
    reg_35 = newStack();
    push(reg_35, reg_33);
    push(reg_35, reg_34);
    reg_36 = call(reg_32, reg_35);
    reg_37 = next(reg_36);
    reg_34 = reg_37;
    reg_38 = next(reg_36);
    if (tobool(eq(reg_34, nil()))) break loop_2;
    reg_43 = lua$get(reg_8, cns1747);
    lua$set(reg_43, reg_37, lua$get(reg_38, cns1748));
  }
  reg_48 = lua$get(reg_1.a, cns1749);
  reg_49 = newStack();
  push(reg_49, reg_7);
  reg_50 = call(reg_48, reg_49);
  reg_51 = next(reg_50);
  reg_52 = next(reg_50);
  reg_53 = next(reg_50);
  loop_1: while (true) {
    reg_54 = newStack();
    push(reg_54, reg_52);
    push(reg_54, reg_53);
    reg_55 = call(reg_51, reg_54);
    reg_56 = next(reg_55);
    reg_53 = reg_56;
    reg_57 = next(reg_55);
    if (tobool(eq(reg_53, nil()))) break loop_1;
    reg_62 = lua$get(reg_8, cns1750);
    lua$set(reg_62, reg_56, lua$get(reg_57, cns1751));
  }
  reg_69 = lua$get(lua$get(reg_1.a, cns1752), cns1753);
  reg_70 = newStack();
  push(reg_70, lua$get(reg_1.a, cns1754));
  push(reg_70, reg_8);
  reg_74 = call(reg_69, reg_70);
  reg_77 = lua$get(reg_1.a, cns1755);
  reg_78 = newStack();
  push(reg_78, reg_8);
  reg_79 = newTable();
  reg_80 = cns1756;
  lua$set(reg_79, reg_80, anyFn((function (a) { return function$76(a,this) }).bind(reg_3)));
  push(reg_78, reg_79);
  reg_83 = call(reg_77, reg_78);
  reg_84 = newStack();
  push(reg_84, reg_8);
  return reg_84;
}
var cns1759 = anyStr("Module")
var cns1760 = anyStr("fullname")
var cns1761 = anyStr("base")
var cns1762 = anyStr("ipairs")
var cns1763 = anyStr("argument")
var cns1764 = anyStr("items")
var cns1765 = anyStr("name")
var cns1766 = anyStr("=")
var cns1767 = anyStr("tostring")
var cns1768 = anyStr("tp")
var cns1769 = anyStr("fn")
var cns1770 = anyStr("¿?")
var cns1771 = anyStr("base")
var cns1772 = anyStr("name")
var cns1773 = anyStr("(")
var cns1774 = anyStr("table")
var cns1775 = anyStr("concat")
var cns1776 = anyStr(" ")
var cns1777 = anyStr(")")
var cns1778 = anyStr("name")
var cns1779 = anyStr("<unknown>")
function Module_fullname (reg_0, reg_1) {
  var reg_4, reg_8, reg_11, reg_12, reg_17, reg_18, reg_19, reg_20, reg_21, reg_22, reg_24, reg_28, reg_30, reg_31, reg_34, reg_35, reg_37, reg_47, reg_51, reg_52, reg_57, reg_58, reg_66, reg_68, reg_71;
  var goto_82=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  goto_82 = !tobool(lua$get(reg_4, cns1761));
  if (!goto_82) {
    reg_8 = newTable();
    reg_11 = lua$get(reg_1.a, cns1762);
    reg_12 = newStack();
    push(reg_12, lua$get(lua$get(reg_4, cns1763), cns1764));
    reg_17 = call(reg_11, reg_12);
    reg_18 = next(reg_17);
    reg_19 = next(reg_17);
    reg_20 = next(reg_17);
    loop_1: while (true) {
      reg_21 = newStack();
      push(reg_21, reg_19);
      push(reg_21, reg_20);
      reg_22 = call(reg_18, reg_21);
      reg_20 = next(reg_22);
      reg_24 = next(reg_22);
      if (tobool(eq(reg_20, nil()))) break loop_1;
      reg_28 = lua$length(reg_8);
      reg_30 = lua$get(reg_24, cns1765);
      reg_31 = cns1766;
      reg_34 = lua$get(reg_1.a, cns1767);
      reg_35 = newStack();
      reg_37 = lua$get(reg_24, cns1768);
      if (!tobool(reg_37)) {
        reg_37 = lua$get(reg_24, cns1769);
      }
      if (!tobool(reg_37)) {
        reg_37 = cns1770;
      }
      push(reg_35, reg_37);
      lua$set(reg_8, reg_28, concat(reg_30, concat(reg_31, first(call(reg_34, reg_35)))));
    }
    reg_47 = newStack();
    reg_51 = lua$get(lua$get(reg_4, cns1771), cns1772);
    reg_52 = cns1773;
    reg_57 = lua$get(lua$get(reg_1.a, cns1774), cns1775);
    reg_58 = newStack();
    push(reg_58, reg_8);
    push(reg_58, cns1776);
    push(reg_47, concat(reg_51, concat(reg_52, concat(first(call(reg_57, reg_58)), cns1777))));
    return reg_47;
  }
  if ((goto_82 || false)) {
    goto_82 = false;
    reg_66 = newStack();
    reg_68 = lua$get(reg_4, cns1778);
    if (!tobool(reg_68)) {
      reg_68 = cns1779;
    }
    push(reg_66, reg_68);
    return reg_66;
  }
  reg_71 = newStack();
  return reg_71;
}
var cns1780 = anyStr("id")
var cns1781 = anyStr("modules")
var cns1782 = parseNum("2")
var cns1783 = anyStr("name")
var cns1784 = anyStr("setmetatable")
var cns1785 = anyStr("Module")
var cns1786 = anyStr("table")
var cns1787 = anyStr("insert")
var cns1788 = anyStr("modules")
function function$77 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_16, reg_17, reg_26, reg_27, reg_32;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  reg_6 = cns1780;
  lua$set(reg_5, reg_6, add(lua$length(lua$get(reg_1.a, cns1781)), cns1782));
  lua$set(reg_5, cns1783, reg_4);
  reg_16 = lua$get(reg_1.a, cns1784);
  reg_17 = newStack();
  push(reg_17, reg_5);
  push(reg_17, lua$get(reg_1.a, cns1785));
  reg_21 = call(reg_16, reg_17);
  reg_26 = lua$get(lua$get(reg_1.a, cns1786), cns1787);
  reg_27 = newStack();
  push(reg_27, lua$get(reg_1.a, cns1788));
  push(reg_27, reg_5);
  reg_31 = call(reg_26, reg_27);
  reg_32 = newStack();
  push(reg_32, reg_5);
  return reg_32;
}
var cns1789 = anyStr("module")
var cns1790 = anyStr("Function")
var cns1791 = anyStr("Function")
var cns1792 = anyStr("__index")
var cns1793 = anyStr("Function")
var cns1794 = anyStr("Function")
var cns1795 = anyStr("id")
var cns1796 = anyStr("_id")
function Function_id (reg_0, reg_1) {
  var reg_4, reg_5;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  push(reg_5, lua$get(reg_4, cns1796));
  return reg_5;
}
var cns1797 = anyStr("Function")
var cns1798 = anyStr("reg")
var cns1799 = anyStr("id")
var cns1800 = anyStr("regs")
var cns1801 = anyStr("table")
var cns1802 = anyStr("insert")
var cns1803 = anyStr("regs")
var cns1804 = anyStr("setmetatable")
var cns1805 = anyStr("__tostring")
var cns1806 = anyStr("reg_")
var cns1807 = anyStr("id")
function function$78 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = cns1806;
  push(reg_6, concat(reg_7, lua$get(reg_5, cns1807)));
  return reg_6;
}
function Function_reg (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_6, reg_14, reg_15, reg_21, reg_22, reg_23, reg_24, reg_28;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  reg_6 = cns1799;
  lua$set(reg_5, reg_6, lua$length(lua$get(reg_4, cns1800)));
  reg_14 = lua$get(lua$get(reg_1.a, cns1801), cns1802);
  reg_15 = newStack();
  push(reg_15, lua$get(reg_4, cns1803));
  push(reg_15, reg_5);
  reg_18 = call(reg_14, reg_15);
  reg_21 = lua$get(reg_1.a, cns1804);
  reg_22 = newStack();
  push(reg_22, reg_5);
  reg_23 = newTable();
  reg_24 = cns1805;
  lua$set(reg_23, reg_24, anyFn((function (a) { return function$78(a,this) }).bind(reg_3)));
  push(reg_22, reg_23);
  reg_27 = call(reg_21, reg_22);
  reg_28 = newStack();
  push(reg_28, reg_5);
  return reg_28;
}
var cns1808 = anyStr("Function")
var cns1809 = anyStr("inst")
var cns1810 = anyStr("setmetatable")
var cns1811 = anyStr("__tostring")
var cns1812 = anyStr("reg")
var cns1813 = anyStr("reg_")
var cns1814 = anyStr("reg")
var cns1815 = anyStr("tostring")
var cns1816 = anyStr("inst")
function function$79 (reg_0, reg_1) {
  var reg_2, reg_5, reg_9, reg_10, reg_14, reg_17, reg_18, reg_22;
  var goto_16=false;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  goto_16 = !tobool(lua$get(reg_5, cns1812));
  if (!goto_16) {
    reg_9 = newStack();
    reg_10 = cns1813;
    push(reg_9, concat(reg_10, lua$get(reg_5, cns1814)));
    return reg_9;
  }
  if ((goto_16 || false)) {
    goto_16 = false;
    reg_14 = newStack();
    reg_17 = lua$get(reg_2.a, cns1815);
    reg_18 = newStack();
    push(reg_18, lua$get(reg_5, cns1816));
    append(reg_14, call(reg_17, reg_18));
    return reg_14;
  }
  reg_22 = newStack();
  return reg_22;
}
var cns1817 = anyStr("table")
var cns1818 = anyStr("insert")
var cns1819 = anyStr("code")
function Function_inst (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_8, reg_9, reg_10, reg_11, reg_19, reg_20, reg_24;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_1.a, cns1810);
  reg_9 = newStack();
  push(reg_9, reg_5);
  reg_10 = newTable();
  reg_11 = cns1811;
  lua$set(reg_10, reg_11, anyFn((function (a) { return function$79(a,this) }).bind(reg_3)));
  push(reg_9, reg_10);
  reg_14 = call(reg_8, reg_9);
  reg_19 = lua$get(lua$get(reg_1.a, cns1817), cns1818);
  reg_20 = newStack();
  push(reg_20, lua$get(reg_4, cns1819));
  push(reg_20, reg_5);
  reg_23 = call(reg_19, reg_20);
  reg_24 = newStack();
  push(reg_24, reg_5);
  return reg_24;
}
var cns1820 = anyStr("Function")
var cns1821 = anyStr("lbl")
var cns1822 = anyStr("label_count")
var cns1823 = parseNum("1")
var cns1824 = anyStr("label_count")
function Function_lbl (reg_0, reg_1) {
  var reg_4, reg_8, reg_10;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_8 = add(lua$get(reg_4, cns1822), cns1823);
  lua$set(reg_4, cns1824, reg_8);
  reg_10 = newStack();
  push(reg_10, reg_8);
  return reg_10;
}
var cns1825 = anyStr("Function")
var cns1826 = anyStr("call")
var cns1827 = anyStr("inst")
var cns1828 = parseNum("1")
function Function_call (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_9;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_7 = lua$get(reg_4, cns1827);
  reg_8 = newStack();
  push(reg_8, reg_4);
  reg_9 = newTable();
  table_append(reg_9, cns1828, copy(reg_0));
  push(reg_8, reg_9);
  append(reg_5, call(reg_7, reg_8));
  return reg_5;
}
var cns1829 = anyStr("Function")
var cns1830 = anyStr("push_scope")
var cns1831 = anyStr("scope")
var cns1832 = anyStr("locals")
var cns1833 = anyStr("labels")
var cns1834 = anyStr("table")
var cns1835 = anyStr("insert")
var cns1836 = anyStr("scopes")
var cns1837 = anyStr("scope")
function Function_push_scope (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_15, reg_16, reg_22;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = cns1831;
  reg_6 = newTable();
  lua$set(reg_6, cns1832, newTable());
  lua$set(reg_6, cns1833, newTable());
  lua$set(reg_4, reg_5, reg_6);
  reg_15 = lua$get(lua$get(reg_1.a, cns1834), cns1835);
  reg_16 = newStack();
  push(reg_16, lua$get(reg_4, cns1836));
  push(reg_16, lua$get(reg_4, cns1837));
  reg_21 = call(reg_15, reg_16);
  reg_22 = newStack();
  return reg_22;
}
var cns1838 = anyStr("Function")
var cns1839 = anyStr("pop_scope")
var cns1840 = anyStr("table")
var cns1841 = anyStr("remove")
var cns1842 = anyStr("scopes")
var cns1843 = anyStr("scope")
var cns1844 = anyStr("scopes")
var cns1845 = anyStr("scopes")
function Function_pop_scope (reg_0, reg_1) {
  var reg_4, reg_9, reg_10, reg_14, reg_16, reg_21;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_9 = lua$get(lua$get(reg_1.a, cns1840), cns1841);
  reg_10 = newStack();
  push(reg_10, lua$get(reg_4, cns1842));
  reg_13 = call(reg_9, reg_10);
  reg_14 = cns1843;
  reg_16 = lua$get(reg_4, cns1844);
  lua$set(reg_4, reg_14, lua$get(reg_16, lua$length(lua$get(reg_4, cns1845))));
  reg_21 = newStack();
  return reg_21;
}
var cns1846 = anyStr("_id")
var cns1847 = anyStr("funcs")
var cns1848 = anyStr("name")
var cns1849 = anyStr("regs")
var cns1850 = anyStr("code")
var cns1851 = anyStr("label_count")
var cns1852 = parseNum("0")
var cns1853 = anyStr("labels")
var cns1854 = anyStr("ins")
var cns1855 = anyStr("outs")
var cns1856 = anyStr("upvals")
var cns1857 = anyStr("scopes")
var cns1858 = anyStr("loops")
var cns1859 = anyStr("setmetatable")
var cns1860 = anyStr("Function")
var cns1861 = anyStr("table")
var cns1862 = anyStr("insert")
var cns1863 = anyStr("funcs")
var cns1864 = anyStr("push_scope")
function function$80 (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_32, reg_33, reg_42, reg_43, reg_49, reg_50, reg_52;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  reg_6 = cns1846;
  lua$set(reg_5, reg_6, lua$length(lua$get(reg_1.a, cns1847)));
  lua$set(reg_5, cns1848, reg_4);
  lua$set(reg_5, cns1849, newTable());
  lua$set(reg_5, cns1850, newTable());
  lua$set(reg_5, cns1851, cns1852);
  lua$set(reg_5, cns1853, newTable());
  lua$set(reg_5, cns1854, newTable());
  lua$set(reg_5, cns1855, newTable());
  lua$set(reg_5, cns1856, newTable());
  lua$set(reg_5, cns1857, newTable());
  lua$set(reg_5, cns1858, newTable());
  reg_32 = lua$get(reg_1.a, cns1859);
  reg_33 = newStack();
  push(reg_33, reg_5);
  push(reg_33, lua$get(reg_1.a, cns1860));
  reg_37 = call(reg_32, reg_33);
  reg_42 = lua$get(lua$get(reg_1.a, cns1861), cns1862);
  reg_43 = newStack();
  push(reg_43, lua$get(reg_1.a, cns1863));
  push(reg_43, reg_5);
  reg_47 = call(reg_42, reg_43);
  reg_49 = lua$get(reg_5, cns1864);
  reg_50 = newStack();
  push(reg_50, reg_5);
  reg_51 = call(reg_49, reg_50);
  reg_52 = newStack();
  push(reg_52, reg_5);
  return reg_52;
}
var cns1865 = anyStr("code")
var cns1866 = anyStr("Function")
var cns1867 = anyStr("__tostring")
var cns1868 = anyStr(":lua:")
var cns1869 = anyStr("name")
var cns1870 = anyStr("")
function Function___tostring (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_8;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newStack();
  reg_6 = cns1868;
  reg_8 = lua$get(reg_4, cns1869);
  if (!tobool(reg_8)) {
    reg_8 = cns1870;
  }
  push(reg_5, concat(reg_6, reg_8));
  return reg_5;
}
var cns1871 = anyStr("_id")
var cns1872 = anyStr("constants")
var cns1873 = anyStr("type")
var cns1874 = anyStr("value")
var cns1875 = anyStr("ins")
var cns1876 = anyStr("outs")
var cns1877 = parseNum("1")
var cns1878 = parseNum("0")
var cns1879 = anyStr("id")
var cns1880 = anyStr("_id")
var cns1881 = anyStr("funcs")
function data_id (reg_0, reg_1) {
  var reg_2, reg_5, reg_6, reg_8;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_8 = lua$get(reg_5, cns1880);
  push(reg_6, add(reg_8, lua$length(lua$get(reg_2.a, cns1881))));
  return reg_6;
}
var cns1882 = anyStr("table")
var cns1883 = anyStr("insert")
var cns1884 = anyStr("constants")
var cns1885 = anyStr("setmetatable")
var cns1886 = anyStr("__tostring")
var cns1887 = anyStr("cns_")
var cns1888 = anyStr("id")
function function$82 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_9, reg_10;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = newStack();
  reg_7 = cns1887;
  reg_9 = lua$get(reg_5, cns1888);
  reg_10 = newStack();
  push(reg_10, reg_5);
  push(reg_6, concat(reg_7, first(call(reg_9, reg_10))));
  return reg_6;
}
function function$81 (reg_0, reg_1) {
  var reg_3, reg_4, reg_5, reg_6, reg_7, reg_16, reg_17, reg_20, reg_27, reg_28, reg_35, reg_36, reg_37, reg_38, reg_42;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = newTable();
  reg_7 = cns1871;
  lua$set(reg_6, reg_7, lua$length(lua$get(reg_1.a, cns1872)));
  lua$set(reg_6, cns1873, reg_4);
  lua$set(reg_6, cns1874, reg_5);
  lua$set(reg_6, cns1875, newTable());
  reg_16 = cns1876;
  reg_17 = newTable();
  lua$set(reg_17, cns1877, cns1878);
  lua$set(reg_6, reg_16, reg_17);
  reg_20 = cns1879;
  lua$set(reg_6, reg_20, anyFn((function (a) { return data_id(a,this) }).bind(reg_3)));
  reg_27 = lua$get(lua$get(reg_1.a, cns1882), cns1883);
  reg_28 = newStack();
  push(reg_28, lua$get(reg_1.a, cns1884));
  push(reg_28, reg_6);
  reg_32 = call(reg_27, reg_28);
  reg_35 = lua$get(reg_1.a, cns1885);
  reg_36 = newStack();
  push(reg_36, reg_6);
  reg_37 = newTable();
  reg_38 = cns1886;
  lua$set(reg_37, reg_38, anyFn((function (a) { return function$82(a,this) }).bind(reg_3)));
  push(reg_36, reg_37);
  reg_41 = call(reg_35, reg_36);
  reg_42 = newStack();
  push(reg_42, reg_6);
  return reg_42;
}
var cns1889 = anyStr("raw_const")
var cns1890 = anyStr("raw_const")
var cns1891 = anyStr("call")
var cns1892 = anyStr("f")
var cns1893 = anyStr("args")
var cns1894 = anyStr("table")
var cns1895 = anyStr("pack")
function function$83 (reg_0, reg_1) {
  var reg_4, reg_7, reg_8, reg_11, reg_13, reg_18, reg_19, reg_22;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_7 = lua$get(reg_1.a, cns1890);
  reg_8 = newStack();
  push(reg_8, cns1891);
  reg_11 = first(call(reg_7, reg_8));
  lua$set(reg_11, cns1892, reg_4);
  reg_13 = cns1893;
  reg_18 = lua$get(lua$get(reg_1.a, cns1894), cns1895);
  reg_19 = newStack();
  append(reg_19, reg_0);
  lua$set(reg_11, reg_13, first(call(reg_18, reg_19)));
  reg_22 = newStack();
  push(reg_22, reg_11);
  return reg_22;
}
var cns1896 = anyStr("const_call")
var cns1897 = anyStr("str_cache")
var cns1898 = anyStr("anystr_f")
var cns1899 = anyStr("number")
var cns1900 = anyStr("num_cache")
var cns1901 = anyStr("parsenum_f")
var cns1902 = anyStr("raw_const")
var cns1903 = anyStr("bin")
var cns1904 = anyStr("tostring")
var cns1905 = anyStr("const_call")
var cns1906 = anyStr("rawstr_f")
var cns1907 = anyStr("const_call")
function function$84 (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_11, reg_21, reg_23, reg_26, reg_27, reg_31, reg_32, reg_35, reg_38, reg_39, reg_44, reg_45, reg_48, reg_49;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_1.a, cns1897);
  reg_11 = lua$get(reg_1.a, cns1898);
  if (tobool(eq(reg_5, cns1899))) {
    reg_8 = lua$get(reg_1.a, cns1900);
    reg_11 = lua$get(reg_1.a, cns1901);
  }
  reg_21 = lua$get(reg_8, reg_4);
  if (tobool(reg_21)) {
    reg_23 = newStack();
    push(reg_23, reg_21);
    return reg_23;
  }
  reg_26 = lua$get(reg_1.a, cns1902);
  reg_27 = newStack();
  push(reg_27, cns1903);
  reg_31 = lua$get(reg_1.a, cns1904);
  reg_32 = newStack();
  push(reg_32, reg_4);
  append(reg_27, call(reg_31, reg_32));
  reg_35 = first(call(reg_26, reg_27));
  reg_38 = lua$get(reg_1.a, cns1905);
  reg_39 = newStack();
  push(reg_39, lua$get(reg_1.a, cns1906));
  push(reg_39, reg_35);
  reg_44 = first(call(reg_38, reg_39));
  reg_45 = newStack();
  reg_48 = lua$get(reg_1.a, cns1907);
  reg_49 = newStack();
  push(reg_49, reg_11);
  push(reg_49, reg_44);
  append(reg_45, call(reg_48, reg_49));
  return reg_45;
}
var cns1908 = anyStr("constant")
var cns1909 = anyStr("module")
var cns1910 = anyStr("auro\x1fint")
var cns1911 = anyStr("int_m")
var cns1912 = anyStr("module")
var cns1913 = anyStr("auro\x1fbool")
var cns1914 = anyStr("bool_m")
var cns1915 = anyStr("module")
var cns1916 = anyStr("auro\x1fstring")
var cns1917 = anyStr("str_m")
var cns1918 = anyStr("module")
var cns1919 = anyStr("lua")
var cns1920 = anyStr("lua_m")
var cns1921 = anyStr("module")
var cns1922 = anyStr("closure")
var cns1923 = anyStr("closure_m")
var cns1924 = anyStr("closure_m")
var cns1925 = anyStr("from")
var cns1926 = anyStr("lua_m")
var cns1927 = anyStr("module")
var cns1928 = anyStr("auro\x1frecord")
var cns1929 = anyStr("record_m")
var cns1930 = anyStr("module")
var cns1931 = anyStr("auro\x1fany")
var cns1932 = anyStr("any_m")
var cns1933 = anyStr("module")
var cns1934 = anyStr("auro\x1fbuffer")
var cns1935 = anyStr("buffer_m")
var cns1936 = anyStr("any_m")
var cns1937 = anyStr("type")
var cns1938 = anyStr("any")
var cns1939 = anyStr("any_t")
var cns1940 = anyStr("bool_m")
var cns1941 = anyStr("type")
var cns1942 = anyStr("bool")
var cns1943 = anyStr("bool_t")
var cns1944 = anyStr("buffer_m")
var cns1945 = anyStr("type")
var cns1946 = anyStr("buffer")
var cns1947 = anyStr("bin_t")
var cns1948 = anyStr("int_m")
var cns1949 = anyStr("type")
var cns1950 = anyStr("int")
var cns1951 = anyStr("int_t")
var cns1952 = anyStr("str_m")
var cns1953 = anyStr("type")
var cns1954 = anyStr("string")
var cns1955 = anyStr("string_t")
var cns1956 = anyStr("lua_m")
var cns1957 = anyStr("type")
var cns1958 = anyStr("Stack")
var cns1959 = anyStr("stack_t")
var cns1960 = anyStr("lua_m")
var cns1961 = anyStr("type")
var cns1962 = anyStr("Function")
var cns1963 = anyStr("func_t")
var cns1964 = anyStr("str_m")
var cns1965 = anyStr("func")
var cns1966 = anyStr("new")
var cns1967 = parseNum("1")
var cns1968 = anyStr("bin_t")
var cns1969 = parseNum("1")
var cns1970 = anyStr("string_t")
var cns1971 = anyStr("rawstr_f")
var cns1972 = anyStr("lua_m")
var cns1973 = anyStr("func")
var cns1974 = anyStr("int")
var cns1975 = parseNum("1")
var cns1976 = anyStr("int_t")
var cns1977 = parseNum("1")
var cns1978 = anyStr("any_t")
var cns1979 = anyStr("anyint_f")
var cns1980 = anyStr("lua_m")
var cns1981 = anyStr("func")
var cns1982 = anyStr("string")
var cns1983 = parseNum("1")
var cns1984 = anyStr("string_t")
var cns1985 = parseNum("1")
var cns1986 = anyStr("any_t")
var cns1987 = anyStr("anystr_f")
var cns1988 = anyStr("lua_m")
var cns1989 = anyStr("func")
var cns1990 = anyStr("nil")
var cns1991 = parseNum("1")
var cns1992 = anyStr("any_t")
var cns1993 = anyStr("nil_f")
var cns1994 = anyStr("lua_m")
var cns1995 = anyStr("func")
var cns1996 = anyStr("true")
var cns1997 = parseNum("1")
var cns1998 = anyStr("any_t")
var cns1999 = anyStr("true_f")
var cns2000 = anyStr("lua_m")
var cns2001 = anyStr("func")
var cns2002 = anyStr("false")
var cns2003 = parseNum("1")
var cns2004 = anyStr("any_t")
var cns2005 = anyStr("false_f")
var cns2006 = anyStr("lua_m")
var cns2007 = anyStr("func")
var cns2008 = anyStr("tobool")
var cns2009 = parseNum("1")
var cns2010 = anyStr("any_t")
var cns2011 = parseNum("1")
var cns2012 = anyStr("bool_t")
var cns2013 = anyStr("bool_f")
var cns2014 = anyStr("lua_m")
var cns2015 = anyStr("func")
var cns2016 = anyStr("function")
var cns2017 = parseNum("1")
var cns2018 = anyStr("func_t")
var cns2019 = parseNum("1")
var cns2020 = anyStr("any_t")
var cns2021 = anyStr("func_f")
var cns2022 = anyStr("lua_m")
var cns2023 = anyStr("func")
var cns2024 = anyStr("call")
var cns2025 = parseNum("1")
var cns2026 = anyStr("any_t")
var cns2027 = parseNum("2")
var cns2028 = anyStr("stack_t")
var cns2029 = parseNum("1")
var cns2030 = anyStr("stack_t")
var cns2031 = anyStr("call_f")
var cns2032 = anyStr("lua_m")
var cns2033 = anyStr("func")
var cns2034 = anyStr("get_global")
var cns2035 = parseNum("1")
var cns2036 = anyStr("any_t")
var cns2037 = anyStr("global_f")
var cns2038 = anyStr("lua_m")
var cns2039 = anyStr("func")
var cns2040 = anyStr("newStack")
var cns2041 = parseNum("1")
var cns2042 = anyStr("stack_t")
var cns2043 = anyStr("stack_f")
var cns2044 = anyStr("lua_m")
var cns2045 = anyStr("func")
var cns2046 = anyStr("push\x1dStack")
var cns2047 = parseNum("1")
var cns2048 = anyStr("stack_t")
var cns2049 = parseNum("2")
var cns2050 = anyStr("any_t")
var cns2051 = anyStr("push_f")
var cns2052 = anyStr("lua_m")
var cns2053 = anyStr("func")
var cns2054 = anyStr("next\x1dStack")
var cns2055 = parseNum("1")
var cns2056 = anyStr("stack_t")
var cns2057 = parseNum("1")
var cns2058 = anyStr("any_t")
var cns2059 = anyStr("next_f")
var cns2060 = anyStr("lua_m")
var cns2061 = anyStr("func")
var cns2062 = anyStr("first\x1dStack")
var cns2063 = parseNum("1")
var cns2064 = anyStr("stack_t")
var cns2065 = parseNum("1")
var cns2066 = anyStr("any_t")
var cns2067 = anyStr("first_f")
var cns2068 = anyStr("lua_m")
var cns2069 = anyStr("func")
var cns2070 = anyStr("append\x1dStack")
var cns2071 = parseNum("1")
var cns2072 = anyStr("stack_t")
var cns2073 = parseNum("2")
var cns2074 = anyStr("stack_t")
var cns2075 = anyStr("append_f")
var cns2076 = anyStr("lua_m")
var cns2077 = anyStr("func")
var cns2078 = anyStr("copy\x1dStack")
var cns2079 = parseNum("1")
var cns2080 = anyStr("stack_t")
var cns2081 = parseNum("1")
var cns2082 = anyStr("stack_t")
var cns2083 = anyStr("copystack_f")
var cns2084 = anyStr("lua_m")
var cns2085 = anyStr("func")
var cns2086 = anyStr("newTable")
var cns2087 = parseNum("1")
var cns2088 = anyStr("any_t")
var cns2089 = anyStr("table_f")
var cns2090 = anyStr("lua_m")
var cns2091 = anyStr("func")
var cns2092 = anyStr("table_append")
var cns2093 = parseNum("1")
var cns2094 = anyStr("any_t")
var cns2095 = parseNum("2")
var cns2096 = anyStr("any_t")
var cns2097 = parseNum("3")
var cns2098 = anyStr("stack_t")
var cns2099 = anyStr("table_append_f")
var cns2100 = anyStr("lua_m")
var cns2101 = anyStr("func")
var cns2102 = anyStr("get")
var cns2103 = parseNum("1")
var cns2104 = anyStr("any_t")
var cns2105 = parseNum("2")
var cns2106 = anyStr("any_t")
var cns2107 = parseNum("1")
var cns2108 = anyStr("any_t")
var cns2109 = anyStr("get_f")
var cns2110 = anyStr("lua_m")
var cns2111 = anyStr("func")
var cns2112 = anyStr("set")
var cns2113 = parseNum("1")
var cns2114 = anyStr("any_t")
var cns2115 = parseNum("2")
var cns2116 = anyStr("any_t")
var cns2117 = parseNum("3")
var cns2118 = anyStr("any_t")
var cns2119 = anyStr("set_f")
var cns2120 = anyStr("lua_m")
var cns2121 = anyStr("func")
var cns2122 = anyStr("parseNum")
var cns2123 = parseNum("1")
var cns2124 = anyStr("string_t")
var cns2125 = parseNum("1")
var cns2126 = anyStr("any_t")
var cns2127 = anyStr("parsenum_f")
var cns2128 = anyStr("+")
var cns2129 = anyStr("lua_m")
var cns2130 = anyStr("func")
var cns2131 = anyStr("add")
var cns2132 = parseNum("1")
var cns2133 = anyStr("any_t")
var cns2134 = parseNum("2")
var cns2135 = anyStr("any_t")
var cns2136 = parseNum("1")
var cns2137 = anyStr("any_t")
var cns2138 = anyStr("-")
var cns2139 = anyStr("lua_m")
var cns2140 = anyStr("func")
var cns2141 = anyStr("sub")
var cns2142 = parseNum("1")
var cns2143 = anyStr("any_t")
var cns2144 = parseNum("2")
var cns2145 = anyStr("any_t")
var cns2146 = parseNum("1")
var cns2147 = anyStr("any_t")
var cns2148 = anyStr("*")
var cns2149 = anyStr("lua_m")
var cns2150 = anyStr("func")
var cns2151 = anyStr("mul")
var cns2152 = parseNum("1")
var cns2153 = anyStr("any_t")
var cns2154 = parseNum("2")
var cns2155 = anyStr("any_t")
var cns2156 = parseNum("1")
var cns2157 = anyStr("any_t")
var cns2158 = anyStr("/")
var cns2159 = anyStr("lua_m")
var cns2160 = anyStr("func")
var cns2161 = anyStr("div")
var cns2162 = parseNum("1")
var cns2163 = anyStr("any_t")
var cns2164 = parseNum("2")
var cns2165 = anyStr("any_t")
var cns2166 = parseNum("1")
var cns2167 = anyStr("any_t")
var cns2168 = anyStr("//")
var cns2169 = anyStr("lua_m")
var cns2170 = anyStr("func")
var cns2171 = anyStr("idiv")
var cns2172 = parseNum("1")
var cns2173 = anyStr("any_t")
var cns2174 = parseNum("2")
var cns2175 = anyStr("any_t")
var cns2176 = parseNum("1")
var cns2177 = anyStr("any_t")
var cns2178 = anyStr("%")
var cns2179 = anyStr("lua_m")
var cns2180 = anyStr("func")
var cns2181 = anyStr("mod")
var cns2182 = parseNum("1")
var cns2183 = anyStr("any_t")
var cns2184 = parseNum("2")
var cns2185 = anyStr("any_t")
var cns2186 = parseNum("1")
var cns2187 = anyStr("any_t")
var cns2188 = anyStr("^")
var cns2189 = anyStr("lua_m")
var cns2190 = anyStr("func")
var cns2191 = anyStr("pow")
var cns2192 = parseNum("1")
var cns2193 = anyStr("any_t")
var cns2194 = parseNum("2")
var cns2195 = anyStr("any_t")
var cns2196 = parseNum("1")
var cns2197 = anyStr("any_t")
var cns2198 = anyStr("..")
var cns2199 = anyStr("lua_m")
var cns2200 = anyStr("func")
var cns2201 = anyStr("concat")
var cns2202 = parseNum("1")
var cns2203 = anyStr("any_t")
var cns2204 = parseNum("2")
var cns2205 = anyStr("any_t")
var cns2206 = parseNum("1")
var cns2207 = anyStr("any_t")
var cns2208 = anyStr("==")
var cns2209 = anyStr("lua_m")
var cns2210 = anyStr("func")
var cns2211 = anyStr("eq")
var cns2212 = parseNum("1")
var cns2213 = anyStr("any_t")
var cns2214 = parseNum("2")
var cns2215 = anyStr("any_t")
var cns2216 = parseNum("1")
var cns2217 = anyStr("any_t")
var cns2218 = anyStr("~=")
var cns2219 = anyStr("lua_m")
var cns2220 = anyStr("func")
var cns2221 = anyStr("ne")
var cns2222 = parseNum("1")
var cns2223 = anyStr("any_t")
var cns2224 = parseNum("2")
var cns2225 = anyStr("any_t")
var cns2226 = parseNum("1")
var cns2227 = anyStr("any_t")
var cns2228 = anyStr("<")
var cns2229 = anyStr("lua_m")
var cns2230 = anyStr("func")
var cns2231 = anyStr("lt")
var cns2232 = parseNum("1")
var cns2233 = anyStr("any_t")
var cns2234 = parseNum("2")
var cns2235 = anyStr("any_t")
var cns2236 = parseNum("1")
var cns2237 = anyStr("any_t")
var cns2238 = anyStr(">")
var cns2239 = anyStr("lua_m")
var cns2240 = anyStr("func")
var cns2241 = anyStr("gt")
var cns2242 = parseNum("1")
var cns2243 = anyStr("any_t")
var cns2244 = parseNum("2")
var cns2245 = anyStr("any_t")
var cns2246 = parseNum("1")
var cns2247 = anyStr("any_t")
var cns2248 = anyStr("<=")
var cns2249 = anyStr("lua_m")
var cns2250 = anyStr("func")
var cns2251 = anyStr("le")
var cns2252 = parseNum("1")
var cns2253 = anyStr("any_t")
var cns2254 = parseNum("2")
var cns2255 = anyStr("any_t")
var cns2256 = parseNum("1")
var cns2257 = anyStr("any_t")
var cns2258 = anyStr(">=")
var cns2259 = anyStr("lua_m")
var cns2260 = anyStr("func")
var cns2261 = anyStr("ge")
var cns2262 = parseNum("1")
var cns2263 = anyStr("any_t")
var cns2264 = parseNum("2")
var cns2265 = anyStr("any_t")
var cns2266 = parseNum("1")
var cns2267 = anyStr("any_t")
var cns2268 = anyStr("binops")
var cns2269 = anyStr("not")
var cns2270 = anyStr("lua_m")
var cns2271 = anyStr("func")
var cns2272 = anyStr("not")
var cns2273 = parseNum("1")
var cns2274 = anyStr("any_t")
var cns2275 = parseNum("1")
var cns2276 = anyStr("any_t")
var cns2277 = anyStr("-")
var cns2278 = anyStr("lua_m")
var cns2279 = anyStr("func")
var cns2280 = anyStr("unm")
var cns2281 = parseNum("1")
var cns2282 = anyStr("any_t")
var cns2283 = parseNum("1")
var cns2284 = anyStr("any_t")
var cns2285 = anyStr("#")
var cns2286 = anyStr("lua_m")
var cns2287 = anyStr("func")
var cns2288 = anyStr("length")
var cns2289 = parseNum("1")
var cns2290 = anyStr("any_t")
var cns2291 = parseNum("1")
var cns2292 = anyStr("any_t")
var cns2293 = anyStr("unops")
function function$85 (reg_0, reg_1) {
  var reg_6, reg_7, reg_10, reg_15, reg_16, reg_19, reg_24, reg_25, reg_28, reg_33, reg_34, reg_37, reg_42, reg_43, reg_46, reg_51, reg_52, reg_58, reg_59, reg_62, reg_67, reg_68, reg_71, reg_76, reg_77, reg_80, reg_85, reg_87, reg_88, reg_91, reg_96, reg_98, reg_99, reg_102, reg_107, reg_109, reg_110, reg_113, reg_118, reg_120, reg_121, reg_124, reg_129, reg_131, reg_132, reg_135, reg_140, reg_142, reg_143, reg_146, reg_151, reg_153, reg_154, reg_157, reg_162, reg_164, reg_165, reg_167, reg_168, reg_172, reg_173, reg_178, reg_183, reg_185, reg_186, reg_188, reg_189, reg_193, reg_194, reg_199, reg_204, reg_206, reg_207, reg_209, reg_210, reg_214, reg_215, reg_220, reg_225, reg_227, reg_228, reg_231, reg_232, reg_237, reg_242, reg_244, reg_245, reg_248, reg_249, reg_254, reg_259, reg_261, reg_262, reg_265, reg_266, reg_271, reg_276, reg_278, reg_279, reg_281, reg_282, reg_286, reg_287, reg_292, reg_297, reg_299, reg_300, reg_302, reg_303, reg_307, reg_308, reg_313, reg_318, reg_320, reg_321, reg_323, reg_324, reg_328, reg_332, reg_333, reg_338, reg_343, reg_345, reg_346, reg_349, reg_350, reg_355, reg_360, reg_362, reg_363, reg_366, reg_367, reg_372, reg_377, reg_379, reg_380, reg_382, reg_383, reg_387, reg_393, reg_398, reg_400, reg_401, reg_403, reg_404, reg_408, reg_409, reg_414, reg_419, reg_421, reg_422, reg_424, reg_425, reg_429, reg_430, reg_435, reg_440, reg_442, reg_443, reg_445, reg_446, reg_450, reg_456, reg_461, reg_463, reg_464, reg_466, reg_467, reg_471, reg_472, reg_477, reg_482, reg_484, reg_485, reg_488, reg_489, reg_494, reg_499, reg_501, reg_502, reg_504, reg_505, reg_509, reg_513, reg_519, reg_524, reg_526, reg_527, reg_529, reg_530, reg_534, reg_538, reg_539, reg_544, reg_549, reg_551, reg_552, reg_554, reg_555, reg_559, reg_563, reg_569, reg_574, reg_576, reg_577, reg_579, reg_580, reg_584, reg_585, reg_590, reg_593, reg_594, reg_597, reg_599, reg_600, reg_602, reg_603, reg_607, reg_611, reg_612, reg_618, reg_621, reg_623, reg_624, reg_626, reg_627, reg_631, reg_635, reg_636, reg_642, reg_645, reg_647, reg_648, reg_650, reg_651, reg_655, reg_659, reg_660, reg_666, reg_669, reg_671, reg_672, reg_674, reg_675, reg_679, reg_683, reg_684, reg_690, reg_693, reg_695, reg_696, reg_698, reg_699, reg_703, reg_707, reg_708, reg_714, reg_717, reg_719, reg_720, reg_722, reg_723, reg_727, reg_731, reg_732, reg_738, reg_741, reg_743, reg_744, reg_746, reg_747, reg_751, reg_755, reg_756, reg_762, reg_765, reg_767, reg_768, reg_770, reg_771, reg_775, reg_779, reg_780, reg_786, reg_789, reg_791, reg_792, reg_794, reg_795, reg_799, reg_803, reg_804, reg_810, reg_813, reg_815, reg_816, reg_818, reg_819, reg_823, reg_827, reg_828, reg_834, reg_837, reg_839, reg_840, reg_842, reg_843, reg_847, reg_851, reg_852, reg_858, reg_861, reg_863, reg_864, reg_866, reg_867, reg_871, reg_875, reg_876, reg_882, reg_885, reg_887, reg_888, reg_890, reg_891, reg_895, reg_899, reg_900, reg_906, reg_909, reg_911, reg_912, reg_914, reg_915, reg_919, reg_923, reg_924, reg_932, reg_933, reg_936, reg_938, reg_939, reg_941, reg_942, reg_946, reg_947, reg_953, reg_956, reg_958, reg_959, reg_961, reg_962, reg_966, reg_967, reg_973, reg_976, reg_978, reg_979, reg_981, reg_982, reg_986, reg_987, reg_995;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_6 = lua$get(reg_1.a, cns1909);
  reg_7 = newStack();
  push(reg_7, cns1910);
  reg_10 = first(call(reg_6, reg_7));
  lua$set(reg_1.a, cns1911, reg_10);
  reg_15 = lua$get(reg_1.a, cns1912);
  reg_16 = newStack();
  push(reg_16, cns1913);
  reg_19 = first(call(reg_15, reg_16));
  lua$set(reg_1.a, cns1914, reg_19);
  reg_24 = lua$get(reg_1.a, cns1915);
  reg_25 = newStack();
  push(reg_25, cns1916);
  reg_28 = first(call(reg_24, reg_25));
  lua$set(reg_1.a, cns1917, reg_28);
  reg_33 = lua$get(reg_1.a, cns1918);
  reg_34 = newStack();
  push(reg_34, cns1919);
  reg_37 = first(call(reg_33, reg_34));
  lua$set(reg_1.a, cns1920, reg_37);
  reg_42 = lua$get(reg_1.a, cns1921);
  reg_43 = newStack();
  push(reg_43, cns1922);
  reg_46 = first(call(reg_42, reg_43));
  lua$set(reg_1.a, cns1923, reg_46);
  reg_51 = lua$get(reg_1.a, cns1924);
  reg_52 = cns1925;
  lua$set(reg_51, reg_52, lua$get(reg_1.a, cns1926));
  reg_58 = lua$get(reg_1.a, cns1927);
  reg_59 = newStack();
  push(reg_59, cns1928);
  reg_62 = first(call(reg_58, reg_59));
  lua$set(reg_1.a, cns1929, reg_62);
  reg_67 = lua$get(reg_1.a, cns1930);
  reg_68 = newStack();
  push(reg_68, cns1931);
  reg_71 = first(call(reg_67, reg_68));
  lua$set(reg_1.a, cns1932, reg_71);
  reg_76 = lua$get(reg_1.a, cns1933);
  reg_77 = newStack();
  push(reg_77, cns1934);
  reg_80 = first(call(reg_76, reg_77));
  lua$set(reg_1.a, cns1935, reg_80);
  reg_85 = lua$get(reg_1.a, cns1936);
  reg_87 = lua$get(reg_85, cns1937);
  reg_88 = newStack();
  push(reg_88, reg_85);
  push(reg_88, cns1938);
  reg_91 = first(call(reg_87, reg_88));
  lua$set(reg_1.a, cns1939, reg_91);
  reg_96 = lua$get(reg_1.a, cns1940);
  reg_98 = lua$get(reg_96, cns1941);
  reg_99 = newStack();
  push(reg_99, reg_96);
  push(reg_99, cns1942);
  reg_102 = first(call(reg_98, reg_99));
  lua$set(reg_1.a, cns1943, reg_102);
  reg_107 = lua$get(reg_1.a, cns1944);
  reg_109 = lua$get(reg_107, cns1945);
  reg_110 = newStack();
  push(reg_110, reg_107);
  push(reg_110, cns1946);
  reg_113 = first(call(reg_109, reg_110));
  lua$set(reg_1.a, cns1947, reg_113);
  reg_118 = lua$get(reg_1.a, cns1948);
  reg_120 = lua$get(reg_118, cns1949);
  reg_121 = newStack();
  push(reg_121, reg_118);
  push(reg_121, cns1950);
  reg_124 = first(call(reg_120, reg_121));
  lua$set(reg_1.a, cns1951, reg_124);
  reg_129 = lua$get(reg_1.a, cns1952);
  reg_131 = lua$get(reg_129, cns1953);
  reg_132 = newStack();
  push(reg_132, reg_129);
  push(reg_132, cns1954);
  reg_135 = first(call(reg_131, reg_132));
  lua$set(reg_1.a, cns1955, reg_135);
  reg_140 = lua$get(reg_1.a, cns1956);
  reg_142 = lua$get(reg_140, cns1957);
  reg_143 = newStack();
  push(reg_143, reg_140);
  push(reg_143, cns1958);
  reg_146 = first(call(reg_142, reg_143));
  lua$set(reg_1.a, cns1959, reg_146);
  reg_151 = lua$get(reg_1.a, cns1960);
  reg_153 = lua$get(reg_151, cns1961);
  reg_154 = newStack();
  push(reg_154, reg_151);
  push(reg_154, cns1962);
  reg_157 = first(call(reg_153, reg_154));
  lua$set(reg_1.a, cns1963, reg_157);
  reg_162 = lua$get(reg_1.a, cns1964);
  reg_164 = lua$get(reg_162, cns1965);
  reg_165 = newStack();
  push(reg_165, reg_162);
  push(reg_165, cns1966);
  reg_167 = newTable();
  reg_168 = cns1967;
  lua$set(reg_167, reg_168, lua$get(reg_1.a, cns1968));
  push(reg_165, reg_167);
  reg_172 = newTable();
  reg_173 = cns1969;
  lua$set(reg_172, reg_173, lua$get(reg_1.a, cns1970));
  push(reg_165, reg_172);
  reg_178 = first(call(reg_164, reg_165));
  lua$set(reg_1.a, cns1971, reg_178);
  reg_183 = lua$get(reg_1.a, cns1972);
  reg_185 = lua$get(reg_183, cns1973);
  reg_186 = newStack();
  push(reg_186, reg_183);
  push(reg_186, cns1974);
  reg_188 = newTable();
  reg_189 = cns1975;
  lua$set(reg_188, reg_189, lua$get(reg_1.a, cns1976));
  push(reg_186, reg_188);
  reg_193 = newTable();
  reg_194 = cns1977;
  lua$set(reg_193, reg_194, lua$get(reg_1.a, cns1978));
  push(reg_186, reg_193);
  reg_199 = first(call(reg_185, reg_186));
  lua$set(reg_1.a, cns1979, reg_199);
  reg_204 = lua$get(reg_1.a, cns1980);
  reg_206 = lua$get(reg_204, cns1981);
  reg_207 = newStack();
  push(reg_207, reg_204);
  push(reg_207, cns1982);
  reg_209 = newTable();
  reg_210 = cns1983;
  lua$set(reg_209, reg_210, lua$get(reg_1.a, cns1984));
  push(reg_207, reg_209);
  reg_214 = newTable();
  reg_215 = cns1985;
  lua$set(reg_214, reg_215, lua$get(reg_1.a, cns1986));
  push(reg_207, reg_214);
  reg_220 = first(call(reg_206, reg_207));
  lua$set(reg_1.a, cns1987, reg_220);
  reg_225 = lua$get(reg_1.a, cns1988);
  reg_227 = lua$get(reg_225, cns1989);
  reg_228 = newStack();
  push(reg_228, reg_225);
  push(reg_228, cns1990);
  push(reg_228, newTable());
  reg_231 = newTable();
  reg_232 = cns1991;
  lua$set(reg_231, reg_232, lua$get(reg_1.a, cns1992));
  push(reg_228, reg_231);
  reg_237 = first(call(reg_227, reg_228));
  lua$set(reg_1.a, cns1993, reg_237);
  reg_242 = lua$get(reg_1.a, cns1994);
  reg_244 = lua$get(reg_242, cns1995);
  reg_245 = newStack();
  push(reg_245, reg_242);
  push(reg_245, cns1996);
  push(reg_245, newTable());
  reg_248 = newTable();
  reg_249 = cns1997;
  lua$set(reg_248, reg_249, lua$get(reg_1.a, cns1998));
  push(reg_245, reg_248);
  reg_254 = first(call(reg_244, reg_245));
  lua$set(reg_1.a, cns1999, reg_254);
  reg_259 = lua$get(reg_1.a, cns2000);
  reg_261 = lua$get(reg_259, cns2001);
  reg_262 = newStack();
  push(reg_262, reg_259);
  push(reg_262, cns2002);
  push(reg_262, newTable());
  reg_265 = newTable();
  reg_266 = cns2003;
  lua$set(reg_265, reg_266, lua$get(reg_1.a, cns2004));
  push(reg_262, reg_265);
  reg_271 = first(call(reg_261, reg_262));
  lua$set(reg_1.a, cns2005, reg_271);
  reg_276 = lua$get(reg_1.a, cns2006);
  reg_278 = lua$get(reg_276, cns2007);
  reg_279 = newStack();
  push(reg_279, reg_276);
  push(reg_279, cns2008);
  reg_281 = newTable();
  reg_282 = cns2009;
  lua$set(reg_281, reg_282, lua$get(reg_1.a, cns2010));
  push(reg_279, reg_281);
  reg_286 = newTable();
  reg_287 = cns2011;
  lua$set(reg_286, reg_287, lua$get(reg_1.a, cns2012));
  push(reg_279, reg_286);
  reg_292 = first(call(reg_278, reg_279));
  lua$set(reg_1.a, cns2013, reg_292);
  reg_297 = lua$get(reg_1.a, cns2014);
  reg_299 = lua$get(reg_297, cns2015);
  reg_300 = newStack();
  push(reg_300, reg_297);
  push(reg_300, cns2016);
  reg_302 = newTable();
  reg_303 = cns2017;
  lua$set(reg_302, reg_303, lua$get(reg_1.a, cns2018));
  push(reg_300, reg_302);
  reg_307 = newTable();
  reg_308 = cns2019;
  lua$set(reg_307, reg_308, lua$get(reg_1.a, cns2020));
  push(reg_300, reg_307);
  reg_313 = first(call(reg_299, reg_300));
  lua$set(reg_1.a, cns2021, reg_313);
  reg_318 = lua$get(reg_1.a, cns2022);
  reg_320 = lua$get(reg_318, cns2023);
  reg_321 = newStack();
  push(reg_321, reg_318);
  push(reg_321, cns2024);
  reg_323 = newTable();
  reg_324 = cns2025;
  lua$set(reg_323, reg_324, lua$get(reg_1.a, cns2026));
  reg_328 = cns2027;
  lua$set(reg_323, reg_328, lua$get(reg_1.a, cns2028));
  push(reg_321, reg_323);
  reg_332 = newTable();
  reg_333 = cns2029;
  lua$set(reg_332, reg_333, lua$get(reg_1.a, cns2030));
  push(reg_321, reg_332);
  reg_338 = first(call(reg_320, reg_321));
  lua$set(reg_1.a, cns2031, reg_338);
  reg_343 = lua$get(reg_1.a, cns2032);
  reg_345 = lua$get(reg_343, cns2033);
  reg_346 = newStack();
  push(reg_346, reg_343);
  push(reg_346, cns2034);
  push(reg_346, newTable());
  reg_349 = newTable();
  reg_350 = cns2035;
  lua$set(reg_349, reg_350, lua$get(reg_1.a, cns2036));
  push(reg_346, reg_349);
  reg_355 = first(call(reg_345, reg_346));
  lua$set(reg_1.a, cns2037, reg_355);
  reg_360 = lua$get(reg_1.a, cns2038);
  reg_362 = lua$get(reg_360, cns2039);
  reg_363 = newStack();
  push(reg_363, reg_360);
  push(reg_363, cns2040);
  push(reg_363, newTable());
  reg_366 = newTable();
  reg_367 = cns2041;
  lua$set(reg_366, reg_367, lua$get(reg_1.a, cns2042));
  push(reg_363, reg_366);
  reg_372 = first(call(reg_362, reg_363));
  lua$set(reg_1.a, cns2043, reg_372);
  reg_377 = lua$get(reg_1.a, cns2044);
  reg_379 = lua$get(reg_377, cns2045);
  reg_380 = newStack();
  push(reg_380, reg_377);
  push(reg_380, cns2046);
  reg_382 = newTable();
  reg_383 = cns2047;
  lua$set(reg_382, reg_383, lua$get(reg_1.a, cns2048));
  reg_387 = cns2049;
  lua$set(reg_382, reg_387, lua$get(reg_1.a, cns2050));
  push(reg_380, reg_382);
  push(reg_380, newTable());
  reg_393 = first(call(reg_379, reg_380));
  lua$set(reg_1.a, cns2051, reg_393);
  reg_398 = lua$get(reg_1.a, cns2052);
  reg_400 = lua$get(reg_398, cns2053);
  reg_401 = newStack();
  push(reg_401, reg_398);
  push(reg_401, cns2054);
  reg_403 = newTable();
  reg_404 = cns2055;
  lua$set(reg_403, reg_404, lua$get(reg_1.a, cns2056));
  push(reg_401, reg_403);
  reg_408 = newTable();
  reg_409 = cns2057;
  lua$set(reg_408, reg_409, lua$get(reg_1.a, cns2058));
  push(reg_401, reg_408);
  reg_414 = first(call(reg_400, reg_401));
  lua$set(reg_1.a, cns2059, reg_414);
  reg_419 = lua$get(reg_1.a, cns2060);
  reg_421 = lua$get(reg_419, cns2061);
  reg_422 = newStack();
  push(reg_422, reg_419);
  push(reg_422, cns2062);
  reg_424 = newTable();
  reg_425 = cns2063;
  lua$set(reg_424, reg_425, lua$get(reg_1.a, cns2064));
  push(reg_422, reg_424);
  reg_429 = newTable();
  reg_430 = cns2065;
  lua$set(reg_429, reg_430, lua$get(reg_1.a, cns2066));
  push(reg_422, reg_429);
  reg_435 = first(call(reg_421, reg_422));
  lua$set(reg_1.a, cns2067, reg_435);
  reg_440 = lua$get(reg_1.a, cns2068);
  reg_442 = lua$get(reg_440, cns2069);
  reg_443 = newStack();
  push(reg_443, reg_440);
  push(reg_443, cns2070);
  reg_445 = newTable();
  reg_446 = cns2071;
  lua$set(reg_445, reg_446, lua$get(reg_1.a, cns2072));
  reg_450 = cns2073;
  lua$set(reg_445, reg_450, lua$get(reg_1.a, cns2074));
  push(reg_443, reg_445);
  push(reg_443, newTable());
  reg_456 = first(call(reg_442, reg_443));
  lua$set(reg_1.a, cns2075, reg_456);
  reg_461 = lua$get(reg_1.a, cns2076);
  reg_463 = lua$get(reg_461, cns2077);
  reg_464 = newStack();
  push(reg_464, reg_461);
  push(reg_464, cns2078);
  reg_466 = newTable();
  reg_467 = cns2079;
  lua$set(reg_466, reg_467, lua$get(reg_1.a, cns2080));
  push(reg_464, reg_466);
  reg_471 = newTable();
  reg_472 = cns2081;
  lua$set(reg_471, reg_472, lua$get(reg_1.a, cns2082));
  push(reg_464, reg_471);
  reg_477 = first(call(reg_463, reg_464));
  lua$set(reg_1.a, cns2083, reg_477);
  reg_482 = lua$get(reg_1.a, cns2084);
  reg_484 = lua$get(reg_482, cns2085);
  reg_485 = newStack();
  push(reg_485, reg_482);
  push(reg_485, cns2086);
  push(reg_485, newTable());
  reg_488 = newTable();
  reg_489 = cns2087;
  lua$set(reg_488, reg_489, lua$get(reg_1.a, cns2088));
  push(reg_485, reg_488);
  reg_494 = first(call(reg_484, reg_485));
  lua$set(reg_1.a, cns2089, reg_494);
  reg_499 = lua$get(reg_1.a, cns2090);
  reg_501 = lua$get(reg_499, cns2091);
  reg_502 = newStack();
  push(reg_502, reg_499);
  push(reg_502, cns2092);
  reg_504 = newTable();
  reg_505 = cns2093;
  lua$set(reg_504, reg_505, lua$get(reg_1.a, cns2094));
  reg_509 = cns2095;
  lua$set(reg_504, reg_509, lua$get(reg_1.a, cns2096));
  reg_513 = cns2097;
  lua$set(reg_504, reg_513, lua$get(reg_1.a, cns2098));
  push(reg_502, reg_504);
  push(reg_502, newTable());
  reg_519 = first(call(reg_501, reg_502));
  lua$set(reg_1.a, cns2099, reg_519);
  reg_524 = lua$get(reg_1.a, cns2100);
  reg_526 = lua$get(reg_524, cns2101);
  reg_527 = newStack();
  push(reg_527, reg_524);
  push(reg_527, cns2102);
  reg_529 = newTable();
  reg_530 = cns2103;
  lua$set(reg_529, reg_530, lua$get(reg_1.a, cns2104));
  reg_534 = cns2105;
  lua$set(reg_529, reg_534, lua$get(reg_1.a, cns2106));
  push(reg_527, reg_529);
  reg_538 = newTable();
  reg_539 = cns2107;
  lua$set(reg_538, reg_539, lua$get(reg_1.a, cns2108));
  push(reg_527, reg_538);
  reg_544 = first(call(reg_526, reg_527));
  lua$set(reg_1.a, cns2109, reg_544);
  reg_549 = lua$get(reg_1.a, cns2110);
  reg_551 = lua$get(reg_549, cns2111);
  reg_552 = newStack();
  push(reg_552, reg_549);
  push(reg_552, cns2112);
  reg_554 = newTable();
  reg_555 = cns2113;
  lua$set(reg_554, reg_555, lua$get(reg_1.a, cns2114));
  reg_559 = cns2115;
  lua$set(reg_554, reg_559, lua$get(reg_1.a, cns2116));
  reg_563 = cns2117;
  lua$set(reg_554, reg_563, lua$get(reg_1.a, cns2118));
  push(reg_552, reg_554);
  push(reg_552, newTable());
  reg_569 = first(call(reg_551, reg_552));
  lua$set(reg_1.a, cns2119, reg_569);
  reg_574 = lua$get(reg_1.a, cns2120);
  reg_576 = lua$get(reg_574, cns2121);
  reg_577 = newStack();
  push(reg_577, reg_574);
  push(reg_577, cns2122);
  reg_579 = newTable();
  reg_580 = cns2123;
  lua$set(reg_579, reg_580, lua$get(reg_1.a, cns2124));
  push(reg_577, reg_579);
  reg_584 = newTable();
  reg_585 = cns2125;
  lua$set(reg_584, reg_585, lua$get(reg_1.a, cns2126));
  push(reg_577, reg_584);
  reg_590 = first(call(reg_576, reg_577));
  lua$set(reg_1.a, cns2127, reg_590);
  reg_593 = newTable();
  reg_594 = cns2128;
  reg_597 = lua$get(reg_1.a, cns2129);
  reg_599 = lua$get(reg_597, cns2130);
  reg_600 = newStack();
  push(reg_600, reg_597);
  push(reg_600, cns2131);
  reg_602 = newTable();
  reg_603 = cns2132;
  lua$set(reg_602, reg_603, lua$get(reg_1.a, cns2133));
  reg_607 = cns2134;
  lua$set(reg_602, reg_607, lua$get(reg_1.a, cns2135));
  push(reg_600, reg_602);
  reg_611 = newTable();
  reg_612 = cns2136;
  lua$set(reg_611, reg_612, lua$get(reg_1.a, cns2137));
  push(reg_600, reg_611);
  lua$set(reg_593, reg_594, first(call(reg_599, reg_600)));
  reg_618 = cns2138;
  reg_621 = lua$get(reg_1.a, cns2139);
  reg_623 = lua$get(reg_621, cns2140);
  reg_624 = newStack();
  push(reg_624, reg_621);
  push(reg_624, cns2141);
  reg_626 = newTable();
  reg_627 = cns2142;
  lua$set(reg_626, reg_627, lua$get(reg_1.a, cns2143));
  reg_631 = cns2144;
  lua$set(reg_626, reg_631, lua$get(reg_1.a, cns2145));
  push(reg_624, reg_626);
  reg_635 = newTable();
  reg_636 = cns2146;
  lua$set(reg_635, reg_636, lua$get(reg_1.a, cns2147));
  push(reg_624, reg_635);
  lua$set(reg_593, reg_618, first(call(reg_623, reg_624)));
  reg_642 = cns2148;
  reg_645 = lua$get(reg_1.a, cns2149);
  reg_647 = lua$get(reg_645, cns2150);
  reg_648 = newStack();
  push(reg_648, reg_645);
  push(reg_648, cns2151);
  reg_650 = newTable();
  reg_651 = cns2152;
  lua$set(reg_650, reg_651, lua$get(reg_1.a, cns2153));
  reg_655 = cns2154;
  lua$set(reg_650, reg_655, lua$get(reg_1.a, cns2155));
  push(reg_648, reg_650);
  reg_659 = newTable();
  reg_660 = cns2156;
  lua$set(reg_659, reg_660, lua$get(reg_1.a, cns2157));
  push(reg_648, reg_659);
  lua$set(reg_593, reg_642, first(call(reg_647, reg_648)));
  reg_666 = cns2158;
  reg_669 = lua$get(reg_1.a, cns2159);
  reg_671 = lua$get(reg_669, cns2160);
  reg_672 = newStack();
  push(reg_672, reg_669);
  push(reg_672, cns2161);
  reg_674 = newTable();
  reg_675 = cns2162;
  lua$set(reg_674, reg_675, lua$get(reg_1.a, cns2163));
  reg_679 = cns2164;
  lua$set(reg_674, reg_679, lua$get(reg_1.a, cns2165));
  push(reg_672, reg_674);
  reg_683 = newTable();
  reg_684 = cns2166;
  lua$set(reg_683, reg_684, lua$get(reg_1.a, cns2167));
  push(reg_672, reg_683);
  lua$set(reg_593, reg_666, first(call(reg_671, reg_672)));
  reg_690 = cns2168;
  reg_693 = lua$get(reg_1.a, cns2169);
  reg_695 = lua$get(reg_693, cns2170);
  reg_696 = newStack();
  push(reg_696, reg_693);
  push(reg_696, cns2171);
  reg_698 = newTable();
  reg_699 = cns2172;
  lua$set(reg_698, reg_699, lua$get(reg_1.a, cns2173));
  reg_703 = cns2174;
  lua$set(reg_698, reg_703, lua$get(reg_1.a, cns2175));
  push(reg_696, reg_698);
  reg_707 = newTable();
  reg_708 = cns2176;
  lua$set(reg_707, reg_708, lua$get(reg_1.a, cns2177));
  push(reg_696, reg_707);
  lua$set(reg_593, reg_690, first(call(reg_695, reg_696)));
  reg_714 = cns2178;
  reg_717 = lua$get(reg_1.a, cns2179);
  reg_719 = lua$get(reg_717, cns2180);
  reg_720 = newStack();
  push(reg_720, reg_717);
  push(reg_720, cns2181);
  reg_722 = newTable();
  reg_723 = cns2182;
  lua$set(reg_722, reg_723, lua$get(reg_1.a, cns2183));
  reg_727 = cns2184;
  lua$set(reg_722, reg_727, lua$get(reg_1.a, cns2185));
  push(reg_720, reg_722);
  reg_731 = newTable();
  reg_732 = cns2186;
  lua$set(reg_731, reg_732, lua$get(reg_1.a, cns2187));
  push(reg_720, reg_731);
  lua$set(reg_593, reg_714, first(call(reg_719, reg_720)));
  reg_738 = cns2188;
  reg_741 = lua$get(reg_1.a, cns2189);
  reg_743 = lua$get(reg_741, cns2190);
  reg_744 = newStack();
  push(reg_744, reg_741);
  push(reg_744, cns2191);
  reg_746 = newTable();
  reg_747 = cns2192;
  lua$set(reg_746, reg_747, lua$get(reg_1.a, cns2193));
  reg_751 = cns2194;
  lua$set(reg_746, reg_751, lua$get(reg_1.a, cns2195));
  push(reg_744, reg_746);
  reg_755 = newTable();
  reg_756 = cns2196;
  lua$set(reg_755, reg_756, lua$get(reg_1.a, cns2197));
  push(reg_744, reg_755);
  lua$set(reg_593, reg_738, first(call(reg_743, reg_744)));
  reg_762 = cns2198;
  reg_765 = lua$get(reg_1.a, cns2199);
  reg_767 = lua$get(reg_765, cns2200);
  reg_768 = newStack();
  push(reg_768, reg_765);
  push(reg_768, cns2201);
  reg_770 = newTable();
  reg_771 = cns2202;
  lua$set(reg_770, reg_771, lua$get(reg_1.a, cns2203));
  reg_775 = cns2204;
  lua$set(reg_770, reg_775, lua$get(reg_1.a, cns2205));
  push(reg_768, reg_770);
  reg_779 = newTable();
  reg_780 = cns2206;
  lua$set(reg_779, reg_780, lua$get(reg_1.a, cns2207));
  push(reg_768, reg_779);
  lua$set(reg_593, reg_762, first(call(reg_767, reg_768)));
  reg_786 = cns2208;
  reg_789 = lua$get(reg_1.a, cns2209);
  reg_791 = lua$get(reg_789, cns2210);
  reg_792 = newStack();
  push(reg_792, reg_789);
  push(reg_792, cns2211);
  reg_794 = newTable();
  reg_795 = cns2212;
  lua$set(reg_794, reg_795, lua$get(reg_1.a, cns2213));
  reg_799 = cns2214;
  lua$set(reg_794, reg_799, lua$get(reg_1.a, cns2215));
  push(reg_792, reg_794);
  reg_803 = newTable();
  reg_804 = cns2216;
  lua$set(reg_803, reg_804, lua$get(reg_1.a, cns2217));
  push(reg_792, reg_803);
  lua$set(reg_593, reg_786, first(call(reg_791, reg_792)));
  reg_810 = cns2218;
  reg_813 = lua$get(reg_1.a, cns2219);
  reg_815 = lua$get(reg_813, cns2220);
  reg_816 = newStack();
  push(reg_816, reg_813);
  push(reg_816, cns2221);
  reg_818 = newTable();
  reg_819 = cns2222;
  lua$set(reg_818, reg_819, lua$get(reg_1.a, cns2223));
  reg_823 = cns2224;
  lua$set(reg_818, reg_823, lua$get(reg_1.a, cns2225));
  push(reg_816, reg_818);
  reg_827 = newTable();
  reg_828 = cns2226;
  lua$set(reg_827, reg_828, lua$get(reg_1.a, cns2227));
  push(reg_816, reg_827);
  lua$set(reg_593, reg_810, first(call(reg_815, reg_816)));
  reg_834 = cns2228;
  reg_837 = lua$get(reg_1.a, cns2229);
  reg_839 = lua$get(reg_837, cns2230);
  reg_840 = newStack();
  push(reg_840, reg_837);
  push(reg_840, cns2231);
  reg_842 = newTable();
  reg_843 = cns2232;
  lua$set(reg_842, reg_843, lua$get(reg_1.a, cns2233));
  reg_847 = cns2234;
  lua$set(reg_842, reg_847, lua$get(reg_1.a, cns2235));
  push(reg_840, reg_842);
  reg_851 = newTable();
  reg_852 = cns2236;
  lua$set(reg_851, reg_852, lua$get(reg_1.a, cns2237));
  push(reg_840, reg_851);
  lua$set(reg_593, reg_834, first(call(reg_839, reg_840)));
  reg_858 = cns2238;
  reg_861 = lua$get(reg_1.a, cns2239);
  reg_863 = lua$get(reg_861, cns2240);
  reg_864 = newStack();
  push(reg_864, reg_861);
  push(reg_864, cns2241);
  reg_866 = newTable();
  reg_867 = cns2242;
  lua$set(reg_866, reg_867, lua$get(reg_1.a, cns2243));
  reg_871 = cns2244;
  lua$set(reg_866, reg_871, lua$get(reg_1.a, cns2245));
  push(reg_864, reg_866);
  reg_875 = newTable();
  reg_876 = cns2246;
  lua$set(reg_875, reg_876, lua$get(reg_1.a, cns2247));
  push(reg_864, reg_875);
  lua$set(reg_593, reg_858, first(call(reg_863, reg_864)));
  reg_882 = cns2248;
  reg_885 = lua$get(reg_1.a, cns2249);
  reg_887 = lua$get(reg_885, cns2250);
  reg_888 = newStack();
  push(reg_888, reg_885);
  push(reg_888, cns2251);
  reg_890 = newTable();
  reg_891 = cns2252;
  lua$set(reg_890, reg_891, lua$get(reg_1.a, cns2253));
  reg_895 = cns2254;
  lua$set(reg_890, reg_895, lua$get(reg_1.a, cns2255));
  push(reg_888, reg_890);
  reg_899 = newTable();
  reg_900 = cns2256;
  lua$set(reg_899, reg_900, lua$get(reg_1.a, cns2257));
  push(reg_888, reg_899);
  lua$set(reg_593, reg_882, first(call(reg_887, reg_888)));
  reg_906 = cns2258;
  reg_909 = lua$get(reg_1.a, cns2259);
  reg_911 = lua$get(reg_909, cns2260);
  reg_912 = newStack();
  push(reg_912, reg_909);
  push(reg_912, cns2261);
  reg_914 = newTable();
  reg_915 = cns2262;
  lua$set(reg_914, reg_915, lua$get(reg_1.a, cns2263));
  reg_919 = cns2264;
  lua$set(reg_914, reg_919, lua$get(reg_1.a, cns2265));
  push(reg_912, reg_914);
  reg_923 = newTable();
  reg_924 = cns2266;
  lua$set(reg_923, reg_924, lua$get(reg_1.a, cns2267));
  push(reg_912, reg_923);
  lua$set(reg_593, reg_906, first(call(reg_911, reg_912)));
  lua$set(reg_1.a, cns2268, reg_593);
  reg_932 = newTable();
  reg_933 = cns2269;
  reg_936 = lua$get(reg_1.a, cns2270);
  reg_938 = lua$get(reg_936, cns2271);
  reg_939 = newStack();
  push(reg_939, reg_936);
  push(reg_939, cns2272);
  reg_941 = newTable();
  reg_942 = cns2273;
  lua$set(reg_941, reg_942, lua$get(reg_1.a, cns2274));
  push(reg_939, reg_941);
  reg_946 = newTable();
  reg_947 = cns2275;
  lua$set(reg_946, reg_947, lua$get(reg_1.a, cns2276));
  push(reg_939, reg_946);
  lua$set(reg_932, reg_933, first(call(reg_938, reg_939)));
  reg_953 = cns2277;
  reg_956 = lua$get(reg_1.a, cns2278);
  reg_958 = lua$get(reg_956, cns2279);
  reg_959 = newStack();
  push(reg_959, reg_956);
  push(reg_959, cns2280);
  reg_961 = newTable();
  reg_962 = cns2281;
  lua$set(reg_961, reg_962, lua$get(reg_1.a, cns2282));
  push(reg_959, reg_961);
  reg_966 = newTable();
  reg_967 = cns2283;
  lua$set(reg_966, reg_967, lua$get(reg_1.a, cns2284));
  push(reg_959, reg_966);
  lua$set(reg_932, reg_953, first(call(reg_958, reg_959)));
  reg_973 = cns2285;
  reg_976 = lua$get(reg_1.a, cns2286);
  reg_978 = lua$get(reg_976, cns2287);
  reg_979 = newStack();
  push(reg_979, reg_976);
  push(reg_979, cns2288);
  reg_981 = newTable();
  reg_982 = cns2289;
  lua$set(reg_981, reg_982, lua$get(reg_1.a, cns2290));
  push(reg_979, reg_981);
  reg_986 = newTable();
  reg_987 = cns2291;
  lua$set(reg_986, reg_987, lua$get(reg_1.a, cns2292));
  push(reg_979, reg_986);
  lua$set(reg_932, reg_973, first(call(reg_978, reg_979)));
  lua$set(reg_1.a, cns2293, reg_932);
  reg_995 = newStack();
  return reg_995;
}
var cns2294 = anyStr("modules")
var cns2295 = anyStr("types")
var cns2296 = anyStr("funcs")
var cns2297 = anyStr("metadata")
var cns2298 = parseNum("1")
var cns2299 = anyStr("source map")
var cns2300 = anyStr("sourcemap")
var cns2301 = anyStr("constants")
var cns2302 = anyStr("num_cache")
var cns2303 = anyStr("str_cache")
function function$86 (reg_0, reg_1) {
  var reg_4, reg_7, reg_10, reg_13, reg_16, reg_21, reg_24, reg_27, reg_33;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = newTable();
  lua$set(reg_1.a, cns2294, reg_4);
  reg_7 = newTable();
  lua$set(reg_1.a, cns2295, reg_7);
  reg_10 = newTable();
  lua$set(reg_1.a, cns2296, reg_10);
  reg_13 = newTable();
  lua$set(reg_1.a, cns2297, reg_13);
  reg_16 = newTable();
  lua$set(reg_16, cns2298, cns2299);
  lua$set(reg_1.a, cns2300, reg_16);
  reg_21 = newTable();
  lua$set(reg_1.a, cns2301, reg_21);
  reg_24 = newTable();
  lua$set(reg_1.a, cns2302, reg_24);
  reg_27 = newTable();
  lua$set(reg_1.a, cns2303, reg_27);
  reg_32 = call(reg_1.b, newStack());
  reg_33 = newStack();
  return reg_33;
}
var cns2304 = anyStr("create_compiler_state")
function aulua_basics$lua_main (reg_0) {
  var reg_1, reg_2, reg_3, reg_8, reg_9, reg_15, reg_16, reg_21, reg_22, reg_27, reg_28, reg_32, reg_35, reg_40, reg_41, reg_47, reg_48, reg_53, reg_54, reg_59, reg_60, reg_65, reg_66, reg_71, reg_72, reg_77, reg_78, reg_83, reg_84, reg_88, reg_93, reg_94, reg_98, reg_102, reg_106, reg_112, reg_115;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1};
  reg_2.a = reg_0;
  reg_3 = newTable();
  lua$set(reg_2.a, cns1717, reg_3);
  reg_8 = lua$get(reg_2.a, cns1718);
  reg_9 = cns1719;
  lua$set(reg_8, reg_9, lua$get(reg_2.a, cns1720));
  reg_15 = lua$get(reg_2.a, cns1721);
  reg_16 = cns1722;
  lua$set(reg_15, reg_16, anyFn((function (a) { return Module_type(a,this) }).bind(reg_2)));
  reg_21 = lua$get(reg_2.a, cns1735);
  reg_22 = cns1736;
  lua$set(reg_21, reg_22, anyFn((function (a) { return Module_func(a,this) }).bind(reg_2)));
  reg_27 = lua$get(reg_2.a, cns1759);
  reg_28 = cns1760;
  lua$set(reg_27, reg_28, anyFn((function (a) { return Module_fullname(a,this) }).bind(reg_2)));
  reg_32 = anyFn((function (a) { return function$77(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns1789, reg_32);
  reg_35 = newTable();
  lua$set(reg_2.a, cns1790, reg_35);
  reg_40 = lua$get(reg_2.a, cns1791);
  reg_41 = cns1792;
  lua$set(reg_40, reg_41, lua$get(reg_2.a, cns1793));
  reg_47 = lua$get(reg_2.a, cns1794);
  reg_48 = cns1795;
  lua$set(reg_47, reg_48, anyFn((function (a) { return Function_id(a,this) }).bind(reg_2)));
  reg_53 = lua$get(reg_2.a, cns1797);
  reg_54 = cns1798;
  lua$set(reg_53, reg_54, anyFn((function (a) { return Function_reg(a,this) }).bind(reg_2)));
  reg_59 = lua$get(reg_2.a, cns1808);
  reg_60 = cns1809;
  lua$set(reg_59, reg_60, anyFn((function (a) { return Function_inst(a,this) }).bind(reg_2)));
  reg_65 = lua$get(reg_2.a, cns1820);
  reg_66 = cns1821;
  lua$set(reg_65, reg_66, anyFn((function (a) { return Function_lbl(a,this) }).bind(reg_2)));
  reg_71 = lua$get(reg_2.a, cns1825);
  reg_72 = cns1826;
  lua$set(reg_71, reg_72, anyFn((function (a) { return Function_call(a,this) }).bind(reg_2)));
  reg_77 = lua$get(reg_2.a, cns1829);
  reg_78 = cns1830;
  lua$set(reg_77, reg_78, anyFn((function (a) { return Function_push_scope(a,this) }).bind(reg_2)));
  reg_83 = lua$get(reg_2.a, cns1838);
  reg_84 = cns1839;
  lua$set(reg_83, reg_84, anyFn((function (a) { return Function_pop_scope(a,this) }).bind(reg_2)));
  reg_88 = anyFn((function (a) { return function$80(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns1865, reg_88);
  reg_93 = lua$get(reg_2.a, cns1866);
  reg_94 = cns1867;
  lua$set(reg_93, reg_94, anyFn((function (a) { return Function___tostring(a,this) }).bind(reg_2)));
  reg_98 = anyFn((function (a) { return function$81(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns1889, reg_98);
  reg_102 = anyFn((function (a) { return function$83(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns1896, reg_102);
  reg_106 = anyFn((function (a) { return function$84(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns1908, reg_106);
  reg_2.b = anyFn((function (a) { return function$85(a,this) }).bind(reg_2));
  reg_112 = anyFn((function (a) { return function$86(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns2304, reg_112);
  reg_115 = newStack();
  return reg_115;
}
var cns2305 = anyStr("Function")
var cns2306 = anyStr("compile_au_call")
var cns2307 = anyStr("au_type")
var cns2308 = anyStr("any_module")
var cns2309 = anyStr("module")
var cns2310 = anyStr("items")
var cns2311 = parseNum("1")
var cns2312 = anyStr("name")
var cns2313 = anyStr("0")
var cns2314 = anyStr("tp")
var cns2315 = anyStr("any_module")
var cns2316 = anyStr("module")
var cns2317 = anyStr("any_module")
var cns2318 = anyStr("base")
var cns2319 = anyStr("any_m")
var cns2320 = anyStr("any_module")
var cns2321 = anyStr("argument")
var cns2322 = anyStr("any_module")
function aulua_auro_syntax$function (reg_0, reg_1) {
  var reg_2, reg_5, reg_15, reg_16, reg_17, reg_18, reg_19, reg_23, reg_31, reg_32, reg_39;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  if (tobool(not(lua$get(reg_5, cns2308)))) {
    reg_15 = first(call(lua$get(reg_2.a, cns2309), newStack()));
    reg_16 = cns2310;
    reg_17 = newTable();
    reg_18 = cns2311;
    reg_19 = newTable();
    lua$set(reg_19, cns2312, cns2313);
    lua$set(reg_19, cns2314, reg_5);
    lua$set(reg_17, reg_18, reg_19);
    lua$set(reg_15, reg_16, reg_17);
    reg_23 = cns2315;
    lua$set(reg_5, reg_23, first(call(lua$get(reg_2.a, cns2316), newStack())));
    reg_31 = lua$get(reg_5, cns2317);
    reg_32 = cns2318;
    lua$set(reg_31, reg_32, lua$get(reg_2.a, cns2319));
    lua$set(lua$get(reg_5, cns2320), cns2321, reg_15);
  }
  reg_39 = newStack();
  push(reg_39, lua$get(reg_5, cns2322));
  return reg_39;
}
var cns2323 = anyStr("get_any_module_of")
var cns2324 = anyStr("macro")
var cns2325 = anyStr("key")
var cns2326 = anyStr("err")
var cns2327 = anyStr("attempt to index '")
var cns2328 = anyStr("key")
var cns2329 = anyStr("' on an auro macro")
var cns2330 = anyStr("macro")
var cns2331 = anyStr("import")
var cns2332 = anyStr("values")
var cns2333 = parseNum("1")
var cns2334 = anyStr("err")
var cns2335 = anyStr("bad argument #1 for _AU_IMPORT (string literal expected)")
var cns2336 = anyStr("ipairs")
var cns2337 = anyStr("values")
var cns2338 = anyStr("type")
var cns2339 = anyStr("str")
var cns2340 = anyStr("err")
var cns2341 = anyStr("bad argument #")
var cns2342 = anyStr(" for _AU_IMPORT (string literal expected)")
var cns2343 = anyStr("table")
var cns2344 = anyStr("insert")
var cns2345 = anyStr("value")
var cns2346 = anyStr("module")
var cns2347 = anyStr("table")
var cns2348 = anyStr("concat")
var cns2349 = anyStr("\x1f")
var cns2350 = anyStr("au_type")
var cns2351 = anyStr("module")
var cns2352 = anyStr("module_id")
var cns2353 = anyStr("id")
var cns2354 = anyStr("macro")
var cns2355 = anyStr("function")
var cns2356 = anyStr("err")
var cns2357 = anyStr("auro function definitions not yet supported")
var cns2358 = anyStr("module")
var cns2359 = anyStr("modules")
var cns2360 = anyStr("module_id")
var cns2361 = parseNum("1")
var cns2362 = anyStr("key")
var cns2363 = anyStr("err")
var cns2364 = anyStr("attempt to call a auro module")
var cns2365 = anyStr("key")
var cns2366 = anyStr("get_type")
var cns2367 = anyStr("values")
var cns2368 = parseNum("1")
var cns2369 = anyStr("values")
var cns2370 = parseNum("1")
var cns2371 = anyStr("type")
var cns2372 = anyStr("str")
var cns2373 = anyStr("err")
var cns2374 = anyStr("bad arguments for get_type (string literal expected)")
var cns2375 = anyStr("type")
var cns2376 = anyStr("values")
var cns2377 = parseNum("1")
var cns2378 = anyStr("value")
var cns2379 = anyStr("au_type")
var cns2380 = anyStr("type")
var cns2381 = anyStr("type_id")
var cns2382 = anyStr("id")
var cns2383 = anyStr("key")
var cns2384 = anyStr("get_function")
var cns2385 = anyStr("values")
var cns2386 = anyStr("type")
var cns2387 = anyStr("constructor")
var cns2388 = anyStr("err")
var cns2389 = anyStr("bad argument #")
var cns2390 = anyStr(" for get_function (table constructor expected)")
var cns2391 = anyStr("ipairs")
var cns2392 = anyStr("items")
var cns2393 = anyStr("type")
var cns2394 = anyStr("item")
var cns2395 = anyStr("err")
var cns2396 = anyStr("bad argument #")
var cns2397 = anyStr(" for get_function (field keys are not allowed)")
var cns2398 = anyStr("compileExpr")
var cns2399 = anyStr("value")
var cns2400 = anyStr("au_type")
var cns2401 = anyStr("type")
var cns2402 = anyStr("err")
var cns2403 = anyStr("bad argument #")
var cns2404 = anyStr(" for get_function (auro type expected at field #")
var cns2405 = anyStr(")")
var cns2406 = anyStr("table")
var cns2407 = anyStr("insert")
var cns2408 = anyStr("types")
var cns2409 = anyStr("type_id")
var cns2410 = parseNum("1")
function function$87 (reg_0, reg_1) {
  var reg_2, reg_5, reg_9, reg_17, reg_18, reg_19, reg_25, reg_28, reg_29, reg_32, reg_33, reg_34, reg_35, reg_36, reg_37, reg_38, reg_39, reg_50, reg_51, reg_52, reg_58, reg_60, reg_61, reg_66, reg_74, reg_75, reg_76, reg_77, reg_89, reg_90, reg_93, reg_100;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_9 = lua$get(lua$get(reg_1.b, cns2385), reg_5);
  if (tobool(ne(lua$get(reg_9, cns2386), cns2387))) {
    reg_17 = lua$get(reg_2.a, cns2388);
    reg_18 = newStack();
    reg_19 = cns2389;
    push(reg_18, concat(reg_19, concat(reg_5, cns2390)));
    push(reg_18, reg_1.b);
    reg_24 = call(reg_17, reg_18);
  }
  reg_25 = newTable();
  reg_28 = lua$get(reg_2.a, cns2391);
  reg_29 = newStack();
  push(reg_29, lua$get(reg_9, cns2392));
  reg_32 = call(reg_28, reg_29);
  reg_33 = next(reg_32);
  reg_34 = next(reg_32);
  reg_35 = next(reg_32);
  loop_1: while (true) {
    reg_36 = newStack();
    push(reg_36, reg_34);
    push(reg_36, reg_35);
    reg_37 = call(reg_33, reg_36);
    reg_38 = next(reg_37);
    reg_35 = reg_38;
    reg_39 = next(reg_37);
    if (tobool(eq(reg_35, nil()))) break loop_1;
    if (tobool(ne(lua$get(reg_39, cns2393), cns2394))) {
      reg_50 = lua$get(reg_2.a, cns2395);
      reg_51 = newStack();
      reg_52 = cns2396;
      push(reg_51, concat(reg_52, concat(reg_5, cns2397)));
      push(reg_51, reg_1.b);
      reg_57 = call(reg_50, reg_51);
    }
    reg_58 = reg_1.c;
    reg_60 = lua$get(reg_58, cns2398);
    reg_61 = newStack();
    push(reg_61, reg_58);
    push(reg_61, lua$get(reg_39, cns2399));
    push(reg_61, lua$true());
    reg_66 = first(call(reg_60, reg_61));
    if (tobool(ne(lua$get(reg_66, cns2400), cns2401))) {
      reg_74 = lua$get(reg_2.a, cns2402);
      reg_75 = newStack();
      reg_76 = cns2403;
      reg_77 = cns2404;
      push(reg_75, concat(reg_76, concat(reg_5, concat(reg_77, concat(reg_38, cns2405)))));
      push(reg_75, reg_1.b);
      reg_84 = call(reg_74, reg_75);
    }
    reg_89 = lua$get(lua$get(reg_2.a, cns2406), cns2407);
    reg_90 = newStack();
    push(reg_90, reg_25);
    reg_93 = lua$get(reg_2.a, cns2408);
    push(reg_90, lua$get(reg_93, add(lua$get(reg_66, cns2409), cns2410)));
    reg_99 = call(reg_89, reg_90);
  }
  reg_100 = newStack();
  push(reg_100, reg_25);
  return reg_100;
}
var cns2411 = anyStr("create_type_list")
var cns2412 = anyStr("values")
var cns2413 = parseNum("3")
var cns2414 = anyStr("err")
var cns2415 = anyStr("bad arguments for get_function (3 arguments expected)")
var cns2416 = anyStr("values")
var cns2417 = parseNum("1")
var cns2418 = anyStr("type")
var cns2419 = anyStr("str")
var cns2420 = anyStr("err")
var cns2421 = anyStr("bad argument #1 for get_function (string literal expected)")
var cns2422 = anyStr("values")
var cns2423 = parseNum("1")
var cns2424 = anyStr("value")
var cns2425 = anyStr("create_type_list")
var cns2426 = parseNum("2")
var cns2427 = anyStr("create_type_list")
var cns2428 = parseNum("3")
var cns2429 = anyStr("func")
var cns2430 = anyStr("au_type")
var cns2431 = anyStr("function")
var cns2432 = anyStr("function_id")
var cns2433 = anyStr("id")
var cns2434 = anyStr("err")
var cns2435 = anyStr("attempt to index '")
var cns2436 = anyStr("key")
var cns2437 = anyStr("' on a auro module")
var cns2438 = anyStr("type")
var cns2439 = anyStr("types")
var cns2440 = anyStr("type_id")
var cns2441 = parseNum("1")
var cns2442 = anyStr("key")
var cns2443 = anyStr("values")
var cns2444 = parseNum("1")
var cns2445 = anyStr("err")
var cns2446 = anyStr("bad arguments for auro type (one argument expected)")
var cns2447 = anyStr("from_any")
var cns2448 = anyStr("get_any_module_of")
var cns2449 = anyStr("from_any")
var cns2450 = anyStr("func")
var cns2451 = anyStr("get")
var cns2452 = parseNum("1")
var cns2453 = anyStr("any_t")
var cns2454 = parseNum("1")
var cns2455 = anyStr("compileExpr")
var cns2456 = anyStr("values")
var cns2457 = parseNum("1")
var cns2458 = anyStr("inst")
var cns2459 = parseNum("1")
var cns2460 = anyStr("from_any")
var cns2461 = parseNum("2")
var cns2462 = anyStr("au_type")
var cns2463 = anyStr("value")
var cns2464 = anyStr("type_id")
var cns2465 = anyStr("id")
var cns2466 = anyStr("key")
var cns2467 = anyStr("test")
var cns2468 = anyStr("test_any")
var cns2469 = anyStr("get_any_module_of")
var cns2470 = anyStr("test_any")
var cns2471 = anyStr("func")
var cns2472 = anyStr("test")
var cns2473 = parseNum("1")
var cns2474 = anyStr("any_t")
var cns2475 = parseNum("1")
var cns2476 = anyStr("bool_t")
var cns2477 = anyStr("compileExpr")
var cns2478 = anyStr("values")
var cns2479 = parseNum("1")
var cns2480 = anyStr("inst")
var cns2481 = parseNum("1")
var cns2482 = anyStr("test_any")
var cns2483 = parseNum("2")
var cns2484 = anyStr("au_type")
var cns2485 = anyStr("value")
var cns2486 = anyStr("type_id")
var cns2487 = anyStr("bool_t")
var cns2488 = anyStr("id")
var cns2489 = anyStr("err")
var cns2490 = anyStr("attempt to index '")
var cns2491 = anyStr("key")
var cns2492 = anyStr("' on a auro type")
var cns2493 = anyStr("value")
var cns2494 = anyStr("types")
var cns2495 = anyStr("type_id")
var cns2496 = parseNum("1")
var cns2497 = anyStr("key")
var cns2498 = anyStr("err")
var cns2499 = anyStr("attempt to call a auro value")
var cns2500 = anyStr("key")
var cns2501 = anyStr("to_lua_value")
var cns2502 = anyStr("values")
var cns2503 = parseNum("0")
var cns2504 = anyStr("err")
var cns2505 = anyStr("bad arguments for to_lua_value (no arguments expected)")
var cns2506 = anyStr("to_any")
var cns2507 = anyStr("get_any_module_of")
var cns2508 = anyStr("to_any")
var cns2509 = anyStr("func")
var cns2510 = anyStr("new")
var cns2511 = parseNum("1")
var cns2512 = parseNum("1")
var cns2513 = anyStr("any_t")
var cns2514 = anyStr("inst")
var cns2515 = parseNum("1")
var cns2516 = anyStr("to_any")
var cns2517 = parseNum("2")
var cns2518 = anyStr("err")
var cns2519 = anyStr("attempt to index '")
var cns2520 = anyStr("key")
var cns2521 = anyStr("' on a auro value")
var cns2522 = anyStr("function")
var cns2523 = anyStr("key")
var cns2524 = anyStr("err")
var cns2525 = anyStr("attempt to index '")
var cns2526 = anyStr("key")
var cns2527 = anyStr("' on a auro function")
var cns2528 = anyStr("funcs")
var cns2529 = anyStr("function_id")
var cns2530 = parseNum("1")
var cns2531 = anyStr("values")
var cns2532 = anyStr("ins")
var cns2533 = anyStr("err")
var cns2534 = anyStr("bad arguments for auro type (")
var cns2535 = anyStr("ins")
var cns2536 = anyStr(" arguments expected)")
var cns2537 = parseNum("1")
var cns2538 = anyStr("au_type")
var cns2539 = anyStr("result")
var cns2540 = anyStr("regs")
var cns2541 = anyStr("ipairs")
var cns2542 = anyStr("values")
var cns2543 = anyStr("compileExpr")
var cns2544 = anyStr("au_type")
var cns2545 = anyStr("lua value")
var cns2546 = anyStr("au_type")
var cns2547 = anyStr("value")
var cns2548 = anyStr("au_type")
var cns2549 = anyStr("type_id")
var cns2550 = anyStr("ins")
var cns2551 = anyStr("types")
var cns2552 = anyStr("type_id")
var cns2553 = parseNum("1")
var cns2554 = anyStr("name")
var cns2555 = anyStr("types")
var cns2556 = anyStr("ins")
var cns2557 = parseNum("1")
var cns2558 = anyStr("name")
var cns2559 = anyStr("err")
var cns2560 = anyStr("bad argument #")
var cns2561 = anyStr(" for ")
var cns2562 = anyStr("name")
var cns2563 = anyStr(" (")
var cns2564 = anyStr(" expected but got ")
var cns2565 = anyStr(")")
var cns2566 = anyStr("table")
var cns2567 = anyStr("insert")
var cns2568 = anyStr("ipairs")
var cns2569 = anyStr("outs")
var cns2570 = anyStr("regs")
var cns2571 = anyStr("au_type")
var cns2572 = anyStr("value")
var cns2573 = anyStr("type_id")
var cns2574 = anyStr("inst")
var cns2575 = anyStr("err")
var cns2576 = anyStr("unknown au_type ")
function Function_compile_au_call (reg_0, reg_1) {
  var reg_2, reg_3, reg_6, reg_8, reg_10, reg_21, reg_22, reg_23, reg_37, reg_47, reg_48, reg_54, reg_55, reg_59, reg_60, reg_61, reg_62, reg_63, reg_64, reg_65, reg_66, reg_77, reg_78, reg_79, reg_89, reg_90, reg_96, reg_97, reg_102, reg_103, reg_107, reg_108, reg_109, reg_112, reg_122, reg_123, reg_131, reg_136, reg_144, reg_145, reg_160, reg_174, reg_175, reg_180, reg_181, reg_190, reg_191, reg_192, reg_195, reg_205, reg_217, reg_218, reg_234, reg_235, reg_245, reg_248, reg_249, reg_252, reg_255, reg_256, reg_259, reg_261, reg_262, reg_264, reg_265, reg_266, reg_269, reg_271, reg_272, reg_277, reg_278, reg_279, reg_293, reg_298, reg_313, reg_314, reg_324, reg_325, reg_327, reg_328, reg_330, reg_331, reg_333, reg_334, reg_338, reg_342, reg_344, reg_345, reg_352, reg_353, reg_355, reg_356, reg_357, reg_358, reg_363, reg_366, reg_369, reg_382, reg_383, reg_385, reg_386, reg_388, reg_389, reg_391, reg_392, reg_396, reg_397, reg_403, reg_405, reg_406, reg_413, reg_414, reg_415, reg_417, reg_418, reg_419, reg_420, reg_426, reg_435, reg_436, reg_437, reg_451, reg_456, reg_464, reg_465, reg_484, reg_485, reg_495, reg_496, reg_498, reg_499, reg_501, reg_502, reg_504, reg_506, reg_507, reg_513, reg_514, reg_516, reg_517, reg_518, reg_519, reg_526, reg_527, reg_528, reg_546, reg_547, reg_548, reg_559, reg_564, reg_568, reg_576, reg_577, reg_578, reg_587, reg_595, reg_596, reg_600, reg_601, reg_602, reg_603, reg_604, reg_605, reg_606, reg_607, reg_611, reg_613, reg_614, reg_617, reg_618, reg_632, reg_640, reg_651, reg_659, reg_662, reg_663, reg_664, reg_665, reg_667, reg_668, reg_669, reg_685, reg_686, reg_690, reg_691, reg_694, reg_695, reg_696, reg_697, reg_698, reg_699, reg_700, reg_701, reg_706, reg_707, reg_711, reg_712, reg_714, reg_715, reg_719, reg_720, reg_725;
  var goto_146=false, goto_161=false, goto_189=false, goto_252=false, goto_351=false, goto_367=false, goto_476=false, goto_561=false, goto_577=false, goto_605=false, goto_682=false, goto_698=false, goto_808=false, goto_818=false;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2};
  reg_3.c = next(reg_0);
  reg_3.b = next(reg_0);
  reg_6 = next(reg_0);
  reg_8 = lua$get(reg_6, cns2307);
  reg_10 = anyFn((function (a) { return aulua_auro_syntax$function(a,this) }).bind(reg_3));
  lua$set(reg_1.a, cns2323, reg_10);
  goto_161 = !tobool(eq(reg_8, cns2324));
  if (!goto_161) {
    if (tobool(lua$get(reg_6, cns2325))) {
      reg_21 = lua$get(reg_1.a, cns2326);
      reg_22 = newStack();
      reg_23 = cns2327;
      push(reg_22, concat(reg_23, concat(lua$get(reg_3.b, cns2328), cns2329)));
      push(reg_22, reg_3.b);
      reg_31 = call(reg_21, reg_22);
    }
    goto_146 = !tobool(eq(lua$get(reg_6, cns2330), cns2331));
    if (!goto_146) {
      reg_37 = newTable();
      if (tobool(lt(lua$length(lua$get(reg_3.b, cns2332)), cns2333))) {
        reg_47 = lua$get(reg_1.a, cns2334);
        reg_48 = newStack();
        push(reg_48, cns2335);
        push(reg_48, reg_3.b);
        reg_51 = call(reg_47, reg_48);
      }
      reg_54 = lua$get(reg_1.a, cns2336);
      reg_55 = newStack();
      push(reg_55, lua$get(reg_3.b, cns2337));
      reg_59 = call(reg_54, reg_55);
      reg_60 = next(reg_59);
      reg_61 = next(reg_59);
      reg_62 = next(reg_59);
      loop_3: while (true) {
        reg_63 = newStack();
        push(reg_63, reg_61);
        push(reg_63, reg_62);
        reg_64 = call(reg_60, reg_63);
        reg_65 = next(reg_64);
        reg_62 = reg_65;
        reg_66 = next(reg_64);
        if (tobool(eq(reg_62, nil()))) break loop_3;
        if (tobool(ne(lua$get(reg_66, cns2338), cns2339))) {
          reg_77 = lua$get(reg_1.a, cns2340);
          reg_78 = newStack();
          reg_79 = cns2341;
          push(reg_78, concat(reg_79, concat(reg_65, cns2342)));
          push(reg_78, reg_3.b);
          reg_84 = call(reg_77, reg_78);
        }
        reg_89 = lua$get(lua$get(reg_1.a, cns2343), cns2344);
        reg_90 = newStack();
        push(reg_90, reg_37);
        push(reg_90, lua$get(reg_66, cns2345));
        reg_93 = call(reg_89, reg_90);
      }
      reg_96 = lua$get(reg_1.a, cns2346);
      reg_97 = newStack();
      reg_102 = lua$get(lua$get(reg_1.a, cns2347), cns2348);
      reg_103 = newStack();
      push(reg_103, reg_37);
      push(reg_103, cns2349);
      append(reg_97, call(reg_102, reg_103));
      reg_107 = first(call(reg_96, reg_97));
      reg_108 = newStack();
      reg_109 = newTable();
      lua$set(reg_109, cns2350, cns2351);
      reg_112 = cns2352;
      lua$set(reg_109, reg_112, lua$get(reg_107, cns2353));
      push(reg_108, reg_109);
      return reg_108;
    }
    if ((goto_146 || false)) {
      goto_146 = false;
      if (tobool(eq(lua$get(reg_6, cns2354), cns2355))) {
        reg_122 = lua$get(reg_1.a, cns2356);
        reg_123 = newStack();
        push(reg_123, cns2357);
        reg_125 = call(reg_122, reg_123);
      }
    }
  }
  if ((goto_161 || false)) {
    goto_161 = false;
    goto_367 = !tobool(eq(reg_8, cns2358));
    if (!goto_367) {
      reg_131 = lua$get(reg_1.a, cns2359);
      reg_136 = lua$get(reg_131, sub(lua$get(reg_6, cns2360), cns2361));
      goto_189 = !tobool(not(lua$get(reg_3.b, cns2362)));
      if (!goto_189) {
        reg_144 = lua$get(reg_1.a, cns2363);
        reg_145 = newStack();
        push(reg_145, cns2364);
        push(reg_145, reg_3.b);
        reg_148 = call(reg_144, reg_145);
      }
      if ((goto_189 || false)) {
        goto_189 = false;
        goto_252 = !tobool(eq(lua$get(reg_3.b, cns2365), cns2366));
        if (!goto_252) {
          reg_160 = ne(lua$length(lua$get(reg_3.b, cns2367)), cns2368);
          if (!tobool(reg_160)) {
            reg_160 = ne(lua$get(lua$get(lua$get(reg_3.b, cns2369), cns2370), cns2371), cns2372);
          }
          if (tobool(reg_160)) {
            reg_174 = lua$get(reg_1.a, cns2373);
            reg_175 = newStack();
            push(reg_175, cns2374);
            push(reg_175, reg_3.b);
            reg_178 = call(reg_174, reg_175);
          }
          reg_180 = lua$get(reg_136, cns2375);
          reg_181 = newStack();
          push(reg_181, reg_136);
          push(reg_181, lua$get(lua$get(lua$get(reg_3.b, cns2376), cns2377), cns2378));
          reg_190 = first(call(reg_180, reg_181));
          reg_191 = newStack();
          reg_192 = newTable();
          lua$set(reg_192, cns2379, cns2380);
          reg_195 = cns2381;
          lua$set(reg_192, reg_195, lua$get(reg_190, cns2382));
          push(reg_191, reg_192);
          return reg_191;
        }
        if ((goto_252 || false)) {
          goto_252 = false;
          goto_351 = !tobool(eq(lua$get(reg_3.b, cns2383), cns2384));
          if (!goto_351) {
            reg_205 = anyFn((function (a) { return function$87(a,this) }).bind(reg_3));
            lua$set(reg_1.a, cns2411, reg_205);
            if (tobool(ne(lua$length(lua$get(reg_3.b, cns2412)), cns2413))) {
              reg_217 = lua$get(reg_1.a, cns2414);
              reg_218 = newStack();
              push(reg_218, cns2415);
              push(reg_218, reg_3.b);
              reg_221 = call(reg_217, reg_218);
            }
            if (tobool(ne(lua$get(lua$get(lua$get(reg_3.b, cns2416), cns2417), cns2418), cns2419))) {
              reg_234 = lua$get(reg_1.a, cns2420);
              reg_235 = newStack();
              push(reg_235, cns2421);
              push(reg_235, reg_3.b);
              reg_238 = call(reg_234, reg_235);
            }
            reg_245 = lua$get(lua$get(lua$get(reg_3.b, cns2422), cns2423), cns2424);
            reg_248 = lua$get(reg_1.a, cns2425);
            reg_249 = newStack();
            push(reg_249, cns2426);
            reg_252 = first(call(reg_248, reg_249));
            reg_255 = lua$get(reg_1.a, cns2427);
            reg_256 = newStack();
            push(reg_256, cns2428);
            reg_259 = first(call(reg_255, reg_256));
            reg_261 = lua$get(reg_136, cns2429);
            reg_262 = newStack();
            push(reg_262, reg_136);
            push(reg_262, reg_245);
            push(reg_262, reg_252);
            push(reg_262, reg_259);
            reg_264 = first(call(reg_261, reg_262));
            reg_265 = newStack();
            reg_266 = newTable();
            lua$set(reg_266, cns2430, cns2431);
            reg_269 = cns2432;
            reg_271 = lua$get(reg_264, cns2433);
            reg_272 = newStack();
            push(reg_272, reg_264);
            lua$set(reg_266, reg_269, first(call(reg_271, reg_272)));
            push(reg_265, reg_266);
            return reg_265;
          }
          if ((goto_351 || false)) {
            goto_351 = false;
            reg_277 = lua$get(reg_1.a, cns2434);
            reg_278 = newStack();
            reg_279 = cns2435;
            push(reg_278, concat(reg_279, concat(lua$get(reg_3.b, cns2436), cns2437)));
            push(reg_278, reg_3.b);
            reg_287 = call(reg_277, reg_278);
          }
        }
      }
    }
    if ((goto_367 || false)) {
      goto_367 = false;
      goto_577 = !tobool(eq(reg_8, cns2438));
      if (!goto_577) {
        reg_293 = lua$get(reg_1.a, cns2439);
        reg_298 = lua$get(reg_293, add(lua$get(reg_6, cns2440), cns2441));
        goto_476 = !tobool(not(lua$get(reg_3.b, cns2442)));
        if (!goto_476) {
          if (tobool(ne(lua$length(lua$get(reg_3.b, cns2443)), cns2444))) {
            reg_313 = lua$get(reg_1.a, cns2445);
            reg_314 = newStack();
            push(reg_314, cns2446);
            push(reg_314, reg_3.b);
            reg_317 = call(reg_313, reg_314);
          }
          if (tobool(not(lua$get(reg_298, cns2447)))) {
            reg_324 = lua$get(reg_1.a, cns2448);
            reg_325 = newStack();
            push(reg_325, reg_298);
            reg_327 = first(call(reg_324, reg_325));
            reg_328 = cns2449;
            reg_330 = lua$get(reg_327, cns2450);
            reg_331 = newStack();
            push(reg_331, reg_327);
            push(reg_331, cns2451);
            reg_333 = newTable();
            reg_334 = cns2452;
            lua$set(reg_333, reg_334, lua$get(reg_1.a, cns2453));
            push(reg_331, reg_333);
            reg_338 = newTable();
            lua$set(reg_338, cns2454, reg_298);
            push(reg_331, reg_338);
            lua$set(reg_298, reg_328, first(call(reg_330, reg_331)));
          }
          reg_342 = reg_3.c;
          reg_344 = lua$get(reg_342, cns2455);
          reg_345 = newStack();
          push(reg_345, reg_342);
          push(reg_345, lua$get(lua$get(reg_3.b, cns2456), cns2457));
          reg_352 = first(call(reg_344, reg_345));
          reg_353 = reg_3.c;
          reg_355 = lua$get(reg_353, cns2458);
          reg_356 = newStack();
          push(reg_356, reg_353);
          reg_357 = newTable();
          reg_358 = cns2459;
          lua$set(reg_357, reg_358, lua$get(reg_298, cns2460));
          lua$set(reg_357, cns2461, reg_352);
          push(reg_356, reg_357);
          reg_363 = first(call(reg_355, reg_356));
          lua$set(reg_363, cns2462, cns2463);
          reg_366 = cns2464;
          lua$set(reg_363, reg_366, lua$get(reg_298, cns2465));
          reg_369 = newStack();
          push(reg_369, reg_363);
          return reg_369;
        }
        if ((goto_476 || false)) {
          goto_476 = false;
          goto_561 = !tobool(eq(lua$get(reg_3.b, cns2466), cns2467));
          if (!goto_561) {
            if (tobool(not(lua$get(reg_298, cns2468)))) {
              reg_382 = lua$get(reg_1.a, cns2469);
              reg_383 = newStack();
              push(reg_383, reg_298);
              reg_385 = first(call(reg_382, reg_383));
              reg_386 = cns2470;
              reg_388 = lua$get(reg_385, cns2471);
              reg_389 = newStack();
              push(reg_389, reg_385);
              push(reg_389, cns2472);
              reg_391 = newTable();
              reg_392 = cns2473;
              lua$set(reg_391, reg_392, lua$get(reg_1.a, cns2474));
              push(reg_389, reg_391);
              reg_396 = newTable();
              reg_397 = cns2475;
              lua$set(reg_396, reg_397, lua$get(reg_1.a, cns2476));
              push(reg_389, reg_396);
              lua$set(reg_298, reg_386, first(call(reg_388, reg_389)));
            }
            reg_403 = reg_3.c;
            reg_405 = lua$get(reg_403, cns2477);
            reg_406 = newStack();
            push(reg_406, reg_403);
            push(reg_406, lua$get(lua$get(reg_3.b, cns2478), cns2479));
            reg_413 = first(call(reg_405, reg_406));
            reg_414 = newStack();
            reg_415 = reg_3.c;
            reg_417 = lua$get(reg_415, cns2480);
            reg_418 = newStack();
            push(reg_418, reg_415);
            reg_419 = newTable();
            reg_420 = cns2481;
            lua$set(reg_419, reg_420, lua$get(reg_298, cns2482));
            lua$set(reg_419, cns2483, reg_413);
            lua$set(reg_419, cns2484, cns2485);
            reg_426 = cns2486;
            lua$set(reg_419, reg_426, lua$get(lua$get(reg_1.a, cns2487), cns2488));
            push(reg_418, reg_419);
            append(reg_414, call(reg_417, reg_418));
            return reg_414;
          }
          if ((goto_561 || false)) {
            goto_561 = false;
            reg_435 = lua$get(reg_1.a, cns2489);
            reg_436 = newStack();
            reg_437 = cns2490;
            push(reg_436, concat(reg_437, concat(lua$get(reg_3.b, cns2491), cns2492)));
            push(reg_436, reg_3.b);
            reg_445 = call(reg_435, reg_436);
          }
        }
      }
      if ((goto_577 || false)) {
        goto_577 = false;
        goto_698 = !tobool(eq(reg_8, cns2493));
        if (!goto_698) {
          reg_451 = lua$get(reg_1.a, cns2494);
          reg_456 = lua$get(reg_451, add(lua$get(reg_6, cns2495), cns2496));
          goto_605 = !tobool(not(lua$get(reg_3.b, cns2497)));
          if (!goto_605) {
            reg_464 = lua$get(reg_1.a, cns2498);
            reg_465 = newStack();
            push(reg_465, cns2499);
            push(reg_465, reg_3.b);
            reg_468 = call(reg_464, reg_465);
          }
          if ((goto_605 || false)) {
            goto_605 = false;
            goto_682 = !tobool(eq(lua$get(reg_3.b, cns2500), cns2501));
            if (!goto_682) {
              if (tobool(gt(lua$length(lua$get(reg_3.b, cns2502)), cns2503))) {
                reg_484 = lua$get(reg_1.a, cns2504);
                reg_485 = newStack();
                push(reg_485, cns2505);
                push(reg_485, reg_3.b);
                reg_488 = call(reg_484, reg_485);
              }
              if (tobool(not(lua$get(reg_456, cns2506)))) {
                reg_495 = lua$get(reg_1.a, cns2507);
                reg_496 = newStack();
                push(reg_496, reg_456);
                reg_498 = first(call(reg_495, reg_496));
                reg_499 = cns2508;
                reg_501 = lua$get(reg_498, cns2509);
                reg_502 = newStack();
                push(reg_502, reg_498);
                push(reg_502, cns2510);
                reg_504 = newTable();
                lua$set(reg_504, cns2511, reg_456);
                push(reg_502, reg_504);
                reg_506 = newTable();
                reg_507 = cns2512;
                lua$set(reg_506, reg_507, lua$get(reg_1.a, cns2513));
                push(reg_502, reg_506);
                lua$set(reg_456, reg_499, first(call(reg_501, reg_502)));
              }
              reg_513 = newStack();
              reg_514 = reg_3.c;
              reg_516 = lua$get(reg_514, cns2514);
              reg_517 = newStack();
              push(reg_517, reg_514);
              reg_518 = newTable();
              reg_519 = cns2515;
              lua$set(reg_518, reg_519, lua$get(reg_456, cns2516));
              lua$set(reg_518, cns2517, reg_6);
              push(reg_517, reg_518);
              append(reg_513, call(reg_516, reg_517));
              return reg_513;
            }
            if ((goto_682 || false)) {
              goto_682 = false;
              reg_526 = lua$get(reg_1.a, cns2518);
              reg_527 = newStack();
              reg_528 = cns2519;
              push(reg_527, concat(reg_528, concat(lua$get(reg_3.b, cns2520), cns2521)));
              push(reg_527, reg_3.b);
              reg_536 = call(reg_526, reg_527);
            }
          }
        }
        if ((goto_698 || false)) {
          goto_698 = false;
          if (tobool(eq(reg_8, cns2522))) {
            if (tobool(lua$get(reg_3.b, cns2523))) {
              reg_546 = lua$get(reg_1.a, cns2524);
              reg_547 = newStack();
              reg_548 = cns2525;
              push(reg_547, concat(reg_548, concat(lua$get(reg_3.b, cns2526), cns2527)));
              push(reg_547, reg_3.b);
              reg_556 = call(reg_546, reg_547);
            }
            reg_559 = lua$get(reg_1.a, cns2528);
            reg_564 = lua$get(reg_559, add(lua$get(reg_6, cns2529), cns2530));
            reg_568 = lua$length(lua$get(reg_3.b, cns2531));
            if (tobool(ne(reg_568, lua$length(lua$get(reg_564, cns2532))))) {
              reg_576 = lua$get(reg_1.a, cns2533);
              reg_577 = newStack();
              reg_578 = cns2534;
              push(reg_577, concat(reg_578, concat(lua$length(lua$get(reg_564, cns2535)), cns2536)));
              push(reg_577, reg_3.b);
              reg_586 = call(reg_576, reg_577);
            }
            reg_587 = newTable();
            lua$set(reg_587, cns2537, reg_564);
            lua$set(reg_587, cns2538, cns2539);
            lua$set(reg_587, cns2540, newTable());
            reg_595 = lua$get(reg_1.a, cns2541);
            reg_596 = newStack();
            push(reg_596, lua$get(reg_3.b, cns2542));
            reg_600 = call(reg_595, reg_596);
            reg_601 = next(reg_600);
            reg_602 = next(reg_600);
            reg_603 = next(reg_600);
            loop_2: while (true) {
              reg_604 = newStack();
              push(reg_604, reg_602);
              push(reg_604, reg_603);
              reg_605 = call(reg_601, reg_604);
              reg_606 = next(reg_605);
              reg_603 = reg_606;
              reg_607 = next(reg_605);
              if (tobool(eq(reg_603, nil()))) break loop_2;
              reg_611 = reg_3.c;
              reg_613 = lua$get(reg_611, cns2543);
              reg_614 = newStack();
              push(reg_614, reg_611);
              push(reg_614, reg_607);
              push(reg_614, lua$true());
              reg_617 = first(call(reg_613, reg_614));
              reg_618 = nil();
              goto_808 = !tobool(not(lua$get(reg_617, cns2544)));
              if (!goto_808) {
                reg_618 = cns2545;
              }
              if ((goto_808 || false)) {
                goto_808 = false;
                goto_818 = !tobool(ne(lua$get(reg_617, cns2546), cns2547));
                if (!goto_818) {
                  reg_618 = lua$get(reg_617, cns2548);
                }
                if ((goto_818 || false)) {
                  goto_818 = false;
                  reg_632 = lua$get(reg_617, cns2549);
                  if (tobool(ne(reg_632, lua$get(lua$get(reg_564, cns2550), reg_606)))) {
                    reg_640 = lua$get(reg_1.a, cns2551);
                    reg_618 = lua$get(lua$get(reg_640, add(lua$get(reg_617, cns2552), cns2553)), cns2554);
                  }
                }
              }
              if (tobool(reg_618)) {
                reg_651 = lua$get(reg_1.a, cns2555);
                reg_659 = lua$get(lua$get(reg_651, add(lua$get(lua$get(reg_564, cns2556), reg_606), cns2557)), cns2558);
                reg_662 = lua$get(reg_1.a, cns2559);
                reg_663 = newStack();
                reg_664 = cns2560;
                reg_665 = cns2561;
                reg_667 = lua$get(reg_564, cns2562);
                reg_668 = cns2563;
                reg_669 = cns2564;
                push(reg_663, concat(reg_664, concat(reg_606, concat(reg_665, concat(reg_667, concat(reg_668, concat(reg_659, concat(reg_669, concat(reg_618, cns2565)))))))));
                push(reg_663, reg_3.b);
                reg_680 = call(reg_662, reg_663);
              }
              reg_685 = lua$get(lua$get(reg_1.a, cns2566), cns2567);
              reg_686 = newStack();
              push(reg_686, reg_587);
              push(reg_686, reg_617);
              reg_687 = call(reg_685, reg_686);
            }
            reg_690 = lua$get(reg_1.a, cns2568);
            reg_691 = newStack();
            push(reg_691, lua$get(reg_564, cns2569));
            reg_694 = call(reg_690, reg_691);
            reg_695 = next(reg_694);
            reg_696 = next(reg_694);
            reg_697 = next(reg_694);
            loop_1: while (true) {
              reg_698 = newStack();
              push(reg_698, reg_696);
              push(reg_698, reg_697);
              reg_699 = call(reg_695, reg_698);
              reg_700 = next(reg_699);
              reg_697 = reg_700;
              reg_701 = next(reg_699);
              if (tobool(eq(reg_697, nil()))) break loop_1;
              reg_706 = lua$get(reg_587, cns2570);
              reg_707 = newTable();
              lua$set(reg_707, cns2571, cns2572);
              lua$set(reg_707, cns2573, reg_701);
              lua$set(reg_706, reg_700, reg_707);
            }
            reg_711 = newStack();
            reg_712 = reg_3.c;
            reg_714 = lua$get(reg_712, cns2574);
            reg_715 = newStack();
            push(reg_715, reg_712);
            push(reg_715, reg_587);
            append(reg_711, call(reg_714, reg_715));
            return reg_711;
          }
        }
      }
    }
  }
  reg_719 = lua$get(reg_1.a, cns2575);
  reg_720 = newStack();
  push(reg_720, concat(cns2576, reg_8));
  push(reg_720, reg_3.b);
  reg_724 = call(reg_719, reg_720);
  reg_725 = newStack();
  return reg_725;
}
function aulua_auro_syntax$lua_main (reg_0) {
  var reg_2, reg_5, reg_6, reg_9;
  reg_2 = {a: nil()};
  reg_2.a = reg_0;
  reg_5 = lua$get(reg_2.a, cns2305);
  reg_6 = cns2306;
  lua$set(reg_5, reg_6, anyFn((function (a) { return Function_compile_au_call(a,this) }).bind(reg_2)));
  reg_9 = newStack();
  return reg_9;
}
var cns2577 = anyStr("Function")
var cns2578 = anyStr("create_upval_info")
var cns2579 = anyStr("upval_module")
var cns2580 = anyStr("module")
var cns2581 = anyStr("upval_module")
var cns2582 = anyStr("base")
var cns2583 = anyStr("record_m")
var cns2584 = anyStr("module")
var cns2585 = anyStr("items")
var cns2586 = anyStr("upval_module")
var cns2587 = anyStr("argument")
var cns2588 = anyStr("upval_type")
var cns2589 = anyStr("upval_module")
var cns2590 = anyStr("type")
var cns2591 = anyStr("")
var cns2592 = anyStr("upval_new_fn")
var cns2593 = anyStr("upval_module")
var cns2594 = anyStr("func")
var cns2595 = anyStr("new")
var cns2596 = parseNum("1")
var cns2597 = anyStr("upval_type")
var cns2598 = anyStr("upval_new_call")
var cns2599 = anyStr("inst")
var cns2600 = parseNum("1")
var cns2601 = anyStr("upval_new_fn")
var cns2602 = anyStr("parent")
var cns2603 = anyStr("parent")
var cns2604 = anyStr("upval_type")
var cns2605 = anyStr("table")
var cns2606 = anyStr("insert")
var cns2607 = anyStr("items")
var cns2608 = anyStr("name")
var cns2609 = anyStr("0")
var cns2610 = anyStr("tp")
var cns2611 = anyStr("table")
var cns2612 = anyStr("insert")
var cns2613 = anyStr("upval_new_fn")
var cns2614 = anyStr("ins")
var cns2615 = anyStr("id")
var cns2616 = anyStr("table")
var cns2617 = anyStr("insert")
var cns2618 = anyStr("upval_new_call")
var cns2619 = anyStr("upval_arg")
var cns2620 = anyStr("parent_upval_getter")
var cns2621 = anyStr("upval_module")
var cns2622 = anyStr("func")
var cns2623 = anyStr("get0")
var cns2624 = parseNum("1")
var cns2625 = anyStr("upval_type")
var cns2626 = parseNum("1")
var cns2627 = anyStr("upval_accessors")
function Function_create_upval_info (reg_0, reg_1) {
  var reg_4, reg_5, reg_13, reg_14, reg_23, reg_29, reg_31, reg_33, reg_34, reg_38, reg_40, reg_42, reg_43, reg_46, reg_47, reg_52, reg_54, reg_55, reg_56, reg_57, reg_68, reg_73, reg_74, reg_77, reg_86, reg_87, reg_99, reg_100, reg_106, reg_108, reg_110, reg_111, reg_113, reg_114, reg_117, reg_123;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = cns2579;
  lua$set(reg_4, reg_5, first(call(lua$get(reg_1.a, cns2580), newStack())));
  reg_13 = lua$get(reg_4, cns2581);
  reg_14 = cns2582;
  lua$set(reg_13, reg_14, lua$get(reg_1.a, cns2583));
  reg_23 = first(call(lua$get(reg_1.a, cns2584), newStack()));
  lua$set(reg_23, cns2585, newTable());
  lua$set(lua$get(reg_4, cns2586), cns2587, reg_23);
  reg_29 = cns2588;
  reg_31 = lua$get(reg_4, cns2589);
  reg_33 = lua$get(reg_31, cns2590);
  reg_34 = newStack();
  push(reg_34, reg_31);
  push(reg_34, cns2591);
  lua$set(reg_4, reg_29, first(call(reg_33, reg_34)));
  reg_38 = cns2592;
  reg_40 = lua$get(reg_4, cns2593);
  reg_42 = lua$get(reg_40, cns2594);
  reg_43 = newStack();
  push(reg_43, reg_40);
  push(reg_43, cns2595);
  push(reg_43, newTable());
  reg_46 = newTable();
  reg_47 = cns2596;
  lua$set(reg_46, reg_47, lua$get(reg_4, cns2597));
  push(reg_43, reg_46);
  lua$set(reg_4, reg_38, first(call(reg_42, reg_43)));
  reg_52 = cns2598;
  reg_54 = lua$get(reg_4, cns2599);
  reg_55 = newStack();
  push(reg_55, reg_4);
  reg_56 = newTable();
  reg_57 = cns2600;
  lua$set(reg_56, reg_57, lua$get(reg_4, cns2601));
  push(reg_55, reg_56);
  lua$set(reg_4, reg_52, first(call(reg_54, reg_55)));
  if (tobool(lua$get(reg_4, cns2602))) {
    reg_68 = lua$get(lua$get(reg_4, cns2603), cns2604);
    reg_73 = lua$get(lua$get(reg_1.a, cns2605), cns2606);
    reg_74 = newStack();
    push(reg_74, lua$get(reg_23, cns2607));
    reg_77 = newTable();
    lua$set(reg_77, cns2608, cns2609);
    lua$set(reg_77, cns2610, reg_68);
    push(reg_74, reg_77);
    reg_81 = call(reg_73, reg_74);
    reg_86 = lua$get(lua$get(reg_1.a, cns2611), cns2612);
    reg_87 = newStack();
    push(reg_87, lua$get(lua$get(reg_4, cns2613), cns2614));
    push(reg_87, lua$get(reg_68, cns2615));
    reg_94 = call(reg_86, reg_87);
    reg_99 = lua$get(lua$get(reg_1.a, cns2616), cns2617);
    reg_100 = newStack();
    push(reg_100, lua$get(reg_4, cns2618));
    push(reg_100, lua$get(reg_4, cns2619));
    reg_105 = call(reg_99, reg_100);
    reg_106 = cns2620;
    reg_108 = lua$get(reg_4, cns2621);
    reg_110 = lua$get(reg_108, cns2622);
    reg_111 = newStack();
    push(reg_111, reg_108);
    push(reg_111, cns2623);
    reg_113 = newTable();
    reg_114 = cns2624;
    lua$set(reg_113, reg_114, lua$get(reg_4, cns2625));
    push(reg_111, reg_113);
    reg_117 = newTable();
    lua$set(reg_117, cns2626, reg_68);
    push(reg_111, reg_117);
    lua$set(reg_4, reg_106, first(call(reg_110, reg_111)));
  }
  lua$set(reg_4, cns2627, newTable());
  reg_123 = newStack();
  return reg_123;
}
var cns2628 = anyStr("Function")
var cns2629 = anyStr("build_upvals")
var cns2630 = anyStr("upval_module")
var cns2631 = anyStr("argument")
var cns2632 = anyStr("items")
var cns2633 = anyStr("code")
var cns2634 = anyStr("code")
var cns2635 = anyStr("upval_level_regs")
var cns2636 = anyStr("level")
var cns2637 = anyStr("upval_new_call")
var cns2638 = anyStr("parent")
var cns2639 = anyStr("upval_arg")
var cns2640 = anyStr("upval_level_regs")
var cns2641 = anyStr("parent")
var cns2642 = anyStr("level")
var cns2643 = anyStr("parent")
var cns2644 = anyStr("parent")
var cns2645 = anyStr("inst")
var cns2646 = parseNum("1")
var cns2647 = anyStr("parent_upval_getter")
var cns2648 = parseNum("2")
var cns2649 = anyStr("upval_level_regs")
var cns2650 = anyStr("parent")
var cns2651 = anyStr("level")
var cns2652 = anyStr("parent")
var cns2653 = anyStr("inst")
var cns2654 = parseNum("1")
var cns2655 = anyStr("nil_f")
var cns2656 = anyStr("ipairs")
var cns2657 = anyStr("upvals")
var cns2658 = anyStr("upval_id")
var cns2659 = anyStr("any_t")
var cns2660 = anyStr("au_type")
var cns2661 = anyStr("value")
var cns2662 = anyStr("types")
var cns2663 = anyStr("type_id")
var cns2664 = anyStr("table")
var cns2665 = anyStr("insert")
var cns2666 = anyStr("name")
var cns2667 = anyStr("tostring")
var cns2668 = anyStr("tp")
var cns2669 = anyStr("table")
var cns2670 = anyStr("insert")
var cns2671 = anyStr("upval_new_fn")
var cns2672 = anyStr("ins")
var cns2673 = anyStr("id")
var cns2674 = anyStr("table")
var cns2675 = anyStr("insert")
var cns2676 = anyStr("upval_new_call")
var cns2677 = anyStr("table")
var cns2678 = anyStr("move")
var cns2679 = parseNum("1")
var cns2680 = anyStr("code")
var cns2681 = parseNum("1")
var cns2682 = anyStr("code")
function Function_build_upvals (reg_0, reg_1) {
  var reg_4, reg_10, reg_12, reg_15, reg_16, reg_18, reg_25, reg_27, reg_33, reg_38, reg_39, reg_40, reg_41, reg_48, reg_56, reg_57, reg_58, reg_59, reg_64, reg_67, reg_68, reg_71, reg_72, reg_73, reg_74, reg_75, reg_76, reg_78, reg_83, reg_86, reg_94, reg_102, reg_103, reg_104, reg_105, reg_108, reg_109, reg_118, reg_119, reg_131, reg_132, reg_140, reg_141, reg_152;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_10 = lua$get(lua$get(lua$get(reg_4, cns2630), cns2631), cns2632);
  reg_12 = lua$get(reg_4, cns2633);
  lua$set(reg_4, cns2634, newTable());
  reg_15 = cns2635;
  reg_16 = newTable();
  reg_18 = lua$get(reg_4, cns2636);
  lua$set(reg_16, reg_18, lua$get(reg_4, cns2637));
  lua$set(reg_4, reg_15, reg_16);
  if (tobool(lua$get(reg_4, cns2638))) {
    reg_25 = lua$get(reg_4, cns2639);
    reg_27 = lua$get(reg_4, cns2640);
    lua$set(reg_27, lua$get(lua$get(reg_4, cns2641), cns2642), reg_25);
    reg_33 = lua$get(reg_4, cns2643);
    loop_2: while (tobool(lua$get(reg_33, cns2644))) {
      reg_38 = lua$get(reg_4, cns2645);
      reg_39 = newStack();
      push(reg_39, reg_4);
      reg_40 = newTable();
      reg_41 = cns2646;
      lua$set(reg_40, reg_41, lua$get(reg_33, cns2647));
      lua$set(reg_40, cns2648, reg_25);
      push(reg_39, reg_40);
      reg_25 = first(call(reg_38, reg_39));
      reg_48 = lua$get(reg_4, cns2649);
      lua$set(reg_48, lua$get(lua$get(reg_33, cns2650), cns2651), reg_25);
      reg_33 = lua$get(reg_33, cns2652);
    }
  }
  reg_56 = lua$get(reg_4, cns2653);
  reg_57 = newStack();
  push(reg_57, reg_4);
  reg_58 = newTable();
  reg_59 = cns2654;
  lua$set(reg_58, reg_59, lua$get(reg_1.a, cns2655));
  push(reg_57, reg_58);
  reg_64 = first(call(reg_56, reg_57));
  reg_67 = lua$get(reg_1.a, cns2656);
  reg_68 = newStack();
  push(reg_68, lua$get(reg_4, cns2657));
  reg_71 = call(reg_67, reg_68);
  reg_72 = next(reg_71);
  reg_73 = next(reg_71);
  reg_74 = next(reg_71);
  loop_1: while (true) {
    reg_75 = newStack();
    push(reg_75, reg_73);
    push(reg_75, reg_74);
    reg_76 = call(reg_72, reg_75);
    reg_74 = next(reg_76);
    reg_78 = next(reg_76);
    if (tobool(eq(reg_74, nil()))) break loop_1;
    reg_83 = lua$get(reg_78, cns2658);
    reg_86 = lua$get(reg_1.a, cns2659);
    if (tobool(eq(lua$get(reg_78, cns2660), cns2661))) {
      reg_94 = lua$get(reg_1.a, cns2662);
      reg_86 = lua$get(reg_94, lua$get(reg_78, cns2663));
    }
    reg_102 = lua$get(lua$get(reg_1.a, cns2664), cns2665);
    reg_103 = newStack();
    push(reg_103, reg_10);
    reg_104 = newTable();
    reg_105 = cns2666;
    reg_108 = lua$get(reg_1.a, cns2667);
    reg_109 = newStack();
    push(reg_109, reg_83);
    lua$set(reg_104, reg_105, first(call(reg_108, reg_109)));
    lua$set(reg_104, cns2668, reg_86);
    push(reg_103, reg_104);
    reg_113 = call(reg_102, reg_103);
    reg_118 = lua$get(lua$get(reg_1.a, cns2669), cns2670);
    reg_119 = newStack();
    push(reg_119, lua$get(lua$get(reg_4, cns2671), cns2672));
    push(reg_119, lua$get(reg_86, cns2673));
    reg_126 = call(reg_118, reg_119);
    reg_131 = lua$get(lua$get(reg_1.a, cns2674), cns2675);
    reg_132 = newStack();
    push(reg_132, lua$get(reg_4, cns2676));
    push(reg_132, reg_64);
    reg_135 = call(reg_131, reg_132);
  }
  reg_140 = lua$get(lua$get(reg_1.a, cns2677), cns2678);
  reg_141 = newStack();
  push(reg_141, reg_12);
  push(reg_141, cns2679);
  push(reg_141, lua$length(reg_12));
  push(reg_141, add(lua$length(lua$get(reg_4, cns2680)), cns2681));
  push(reg_141, lua$get(reg_4, cns2682));
  reg_151 = call(reg_140, reg_141);
  reg_152 = newStack();
  return reg_152;
}
var cns2683 = anyStr("Function")
var cns2684 = anyStr("get_vararg")
var cns2685 = anyStr("vararg")
var cns2686 = anyStr("vararg")
var cns2687 = anyStr("error")
var cns2688 = anyStr("cannot use '...' outside a vararg function")
function Function_get_vararg (reg_0, reg_1) {
  var reg_4, reg_8, reg_13, reg_14, reg_17;
  var goto_13=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  goto_13 = !tobool(lua$get(reg_4, cns2685));
  if (!goto_13) {
    reg_8 = newStack();
    push(reg_8, lua$get(reg_4, cns2686));
    return reg_8;
  }
  if ((goto_13 || false)) {
    goto_13 = false;
    reg_13 = lua$get(reg_1.a, cns2687);
    reg_14 = newStack();
    push(reg_14, cns2688);
    reg_16 = call(reg_13, reg_14);
  }
  reg_17 = newStack();
  return reg_17;
}
var cns2689 = anyStr("Function")
var cns2690 = anyStr("get_local")
var cns2691 = anyStr("scopes")
var cns2692 = parseNum("1")
var cns2693 = parseNum("1")
var cns2694 = parseNum("0")
var cns2695 = anyStr("scopes")
var cns2696 = anyStr("locals")
var cns2697 = anyStr("au_type")
var cns2698 = anyStr("au_type")
var cns2699 = anyStr("value")
var cns2700 = anyStr("is_upval")
var cns2701 = anyStr("upvals")
var cns2702 = anyStr("parent")
var cns2703 = parseNum("1")
var cns2704 = anyStr("is_upval")
var cns2705 = anyStr("upval_level")
var cns2706 = anyStr("level")
var cns2707 = anyStr("upval_id")
var cns2708 = anyStr("any_t")
var cns2709 = anyStr("au_type")
var cns2710 = anyStr("value")
var cns2711 = anyStr("types")
var cns2712 = anyStr("reg")
var cns2713 = anyStr("type_id")
var cns2714 = anyStr("upval_accessors")
var cns2715 = anyStr("getter")
var cns2716 = anyStr("upval_module")
var cns2717 = anyStr("func")
var cns2718 = anyStr("get")
var cns2719 = parseNum("1")
var cns2720 = anyStr("upval_type")
var cns2721 = parseNum("1")
var cns2722 = anyStr("setter")
var cns2723 = anyStr("upval_module")
var cns2724 = anyStr("func")
var cns2725 = anyStr("set")
var cns2726 = parseNum("1")
var cns2727 = anyStr("upval_type")
var cns2728 = parseNum("2")
var cns2729 = anyStr("table")
var cns2730 = anyStr("insert")
var cns2731 = anyStr("upvals")
var cns2732 = anyStr("parent")
var cns2733 = anyStr("parent")
var cns2734 = anyStr("get_local")
function Function_get_local (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_10, reg_11, reg_13, reg_15, reg_31, reg_37, reg_47, reg_55, reg_61, reg_69, reg_77, reg_78, reg_79, reg_81, reg_83, reg_84, reg_87, reg_88, reg_91, reg_95, reg_97, reg_99, reg_100, reg_103, reg_104, reg_115, reg_116, reg_120, reg_124, reg_126, reg_128, reg_129, reg_132;
  var goto_20=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = nil();
  reg_10 = lua$length(lua$get(reg_4, cns2691));
  reg_11 = cns2692;
  reg_13 = unm(cns2693);
  reg_15 = lt(reg_13, cns2694);
  loop_1: while (true) {
    goto_20 = tobool(reg_15);
    if (!goto_20) {
      if (tobool(gt(reg_10, reg_11))) break loop_1;
    }
    if ((goto_20 || false)) {
      goto_20 = false;
      if (tobool(lt(reg_10, reg_11))) break loop_1;
    }
    reg_7 = lua$get(lua$get(lua$get(lua$get(reg_4, cns2695), reg_10), cns2696), reg_5);
    if (tobool(reg_7)) {
      if (true) break loop_1;
    }
    reg_10 = add(reg_10, reg_13);
  }
  if (tobool(reg_7)) {
    reg_31 = lua$get(reg_7, cns2697);
    if (tobool(reg_31)) {
      reg_31 = ne(lua$get(reg_7, cns2698), cns2699);
    }
    reg_37 = reg_6;
    if (tobool(reg_6)) {
      reg_37 = not(lua$get(reg_7, cns2700));
    }
    if (tobool(reg_37)) {
      reg_37 = not(reg_31);
    }
    if (tobool(reg_37)) {
      reg_47 = lua$length(lua$get(reg_4, cns2701));
      if (tobool(lua$get(reg_4, cns2702))) {
        reg_47 = add(reg_47, cns2703);
      }
      lua$set(reg_7, cns2704, lua$true());
      reg_55 = cns2705;
      lua$set(reg_7, reg_55, lua$get(reg_4, cns2706));
      lua$set(reg_7, cns2707, reg_47);
      reg_61 = lua$get(reg_1.a, cns2708);
      if (tobool(eq(lua$get(reg_7, cns2709), cns2710))) {
        reg_69 = lua$get(reg_1.a, cns2711);
        reg_61 = lua$get(reg_69, lua$get(lua$get(reg_1.a, cns2712), cns2713));
      }
      reg_77 = lua$get(reg_4, cns2714);
      reg_78 = newTable();
      reg_79 = cns2715;
      reg_81 = lua$get(reg_4, cns2716);
      reg_83 = lua$get(reg_81, cns2717);
      reg_84 = newStack();
      push(reg_84, reg_81);
      push(reg_84, concat(cns2718, reg_47));
      reg_87 = newTable();
      reg_88 = cns2719;
      lua$set(reg_87, reg_88, lua$get(reg_4, cns2720));
      push(reg_84, reg_87);
      reg_91 = newTable();
      lua$set(reg_91, cns2721, reg_61);
      push(reg_84, reg_91);
      lua$set(reg_78, reg_79, first(call(reg_83, reg_84)));
      reg_95 = cns2722;
      reg_97 = lua$get(reg_4, cns2723);
      reg_99 = lua$get(reg_97, cns2724);
      reg_100 = newStack();
      push(reg_100, reg_97);
      push(reg_100, concat(cns2725, reg_47));
      reg_103 = newTable();
      reg_104 = cns2726;
      lua$set(reg_103, reg_104, lua$get(reg_4, cns2727));
      lua$set(reg_103, cns2728, reg_61);
      push(reg_100, reg_103);
      push(reg_100, newTable());
      lua$set(reg_78, reg_95, first(call(reg_99, reg_100)));
      lua$set(reg_77, reg_47, reg_78);
      reg_115 = lua$get(lua$get(reg_1.a, cns2729), cns2730);
      reg_116 = newStack();
      push(reg_116, lua$get(reg_4, cns2731));
      push(reg_116, reg_7);
      reg_119 = call(reg_115, reg_116);
    }
    reg_120 = newStack();
    push(reg_120, reg_7);
    return reg_120;
  }
  if (tobool(lua$get(reg_4, cns2732))) {
    reg_124 = newStack();
    reg_126 = lua$get(reg_4, cns2733);
    reg_128 = lua$get(reg_126, cns2734);
    reg_129 = newStack();
    push(reg_129, reg_126);
    push(reg_129, reg_5);
    push(reg_129, lua$true());
    append(reg_124, call(reg_128, reg_129));
    return reg_124;
  }
  reg_132 = newStack();
  return reg_132;
}
var cns2735 = anyStr("Function")
var cns2736 = anyStr("get_level_ancestor")
var cns2737 = anyStr("level")
var cns2738 = anyStr("parent")
var cns2739 = anyStr("error")
var cns2740 = anyStr("Could not find ancestor of level ")
function Function_get_level_ancestor (reg_0, reg_1) {
  var reg_4, reg_5, reg_11, reg_16, reg_17, reg_21;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  loop_1: while (tobool(reg_4)) {
    if (tobool(eq(lua$get(reg_4, cns2737), reg_5))) {
      reg_11 = newStack();
      push(reg_11, reg_4);
      return reg_11;
    }
    reg_4 = lua$get(reg_4, cns2738);
  }
  reg_16 = lua$get(reg_1.a, cns2739);
  reg_17 = newStack();
  push(reg_17, concat(cns2740, reg_5));
  reg_20 = call(reg_16, reg_17);
  reg_21 = newStack();
  return reg_21;
}
var cns2741 = anyStr("Function")
var cns2742 = anyStr("createFunction")
var cns2743 = anyStr("code")
var cns2744 = anyStr("repr")
var cns2745 = anyStr("function")
var cns2746 = anyStr("node")
var cns2747 = anyStr("parent")
var cns2748 = anyStr("level")
var cns2749 = anyStr("level")
var cns2750 = parseNum("1")
var cns2751 = anyStr("ins")
var cns2752 = parseNum("1")
var cns2753 = anyStr("stack_t")
var cns2754 = anyStr("id")
var cns2755 = parseNum("2")
var cns2756 = anyStr("upval_type")
var cns2757 = anyStr("id")
var cns2758 = anyStr("outs")
var cns2759 = parseNum("1")
var cns2760 = anyStr("stack_t")
var cns2761 = anyStr("id")
var cns2762 = anyStr("line")
var cns2763 = anyStr("line")
var cns2764 = anyStr("reg")
var cns2765 = parseNum("0")
var cns2766 = anyStr("vararg")
var cns2767 = anyStr("vararg")
var cns2768 = anyStr("upval_arg")
var cns2769 = anyStr("reg")
var cns2770 = parseNum("1")
var cns2771 = anyStr("create_upval_info")
var cns2772 = anyStr("method")
var cns2773 = anyStr("table")
var cns2774 = anyStr("insert")
var cns2775 = anyStr("names")
var cns2776 = parseNum("1")
var cns2777 = anyStr("self")
var cns2778 = anyStr("ipairs")
var cns2779 = anyStr("names")
var cns2780 = anyStr("inst")
var cns2781 = parseNum("1")
var cns2782 = anyStr("next_f")
var cns2783 = parseNum("2")
var cns2784 = anyStr("scope")
var cns2785 = anyStr("locals")
var cns2786 = anyStr("inst")
var cns2787 = parseNum("1")
var cns2788 = anyStr("local")
var cns2789 = parseNum("2")
var cns2790 = anyStr("compileBlock")
var cns2791 = anyStr("body")
var cns2792 = anyStr("body")
var cns2793 = parseNum("0")
var cns2794 = anyStr("body")
var cns2795 = anyStr("body")
var cns2796 = anyStr("type")
var cns2797 = anyStr("return")
var cns2798 = anyStr("call")
var cns2799 = anyStr("stack_f")
var cns2800 = anyStr("inst")
var cns2801 = parseNum("1")
var cns2802 = anyStr("end")
var cns2803 = parseNum("2")
var cns2804 = anyStr("build_upvals")
var cns2805 = anyStr("transform")
var cns2806 = anyStr("module")
var cns2807 = anyStr("items")
var cns2808 = parseNum("1")
var cns2809 = anyStr("name")
var cns2810 = anyStr("0")
var cns2811 = anyStr("fn")
var cns2812 = anyStr("module")
var cns2813 = anyStr("base")
var cns2814 = anyStr("closure_m")
var cns2815 = anyStr("argument")
var cns2816 = anyStr("func")
var cns2817 = anyStr("new")
var cns2818 = parseNum("1")
var cns2819 = anyStr("upval_type")
var cns2820 = parseNum("1")
var cns2821 = anyStr("func_t")
var cns2822 = anyStr("call")
var cns2823 = anyStr("upval_new_call")
var cns2824 = anyStr("call")
var cns2825 = anyStr("func_f")
function Function_createFunction (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_11, reg_15, reg_18, reg_23, reg_24, reg_25, reg_31, reg_36, reg_37, reg_38, reg_44, reg_47, reg_54, reg_55, reg_59, reg_60, reg_69, reg_70, reg_78, reg_79, reg_82, reg_83, reg_84, reg_85, reg_86, reg_87, reg_89, reg_94, reg_95, reg_96, reg_97, reg_103, reg_107, reg_109, reg_110, reg_111, reg_118, reg_119, reg_127, reg_130, reg_141, reg_142, reg_147, reg_149, reg_150, reg_151, reg_157, reg_158, reg_161, reg_162, reg_169, reg_170, reg_171, reg_172, reg_173, reg_182, reg_183, reg_189, reg_190, reg_192, reg_193, reg_196, reg_197, reg_202, reg_204, reg_205, reg_209, reg_210, reg_212, reg_213;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_1.a, cns2743);
  reg_9 = newStack();
  reg_11 = lua$get(reg_5, cns2744);
  if (!tobool(reg_11)) {
    reg_11 = cns2745;
  }
  push(reg_9, reg_11);
  reg_15 = first(call(reg_8, reg_9));
  lua$set(reg_15, cns2746, reg_5);
  lua$set(reg_15, cns2747, reg_4);
  reg_18 = cns2748;
  lua$set(reg_15, reg_18, add(lua$get(reg_4, cns2749), cns2750));
  reg_23 = cns2751;
  reg_24 = newTable();
  reg_25 = cns2752;
  lua$set(reg_24, reg_25, lua$get(lua$get(reg_1.a, cns2753), cns2754));
  reg_31 = cns2755;
  lua$set(reg_24, reg_31, lua$get(lua$get(reg_4, cns2756), cns2757));
  lua$set(reg_15, reg_23, reg_24);
  reg_36 = cns2758;
  reg_37 = newTable();
  reg_38 = cns2759;
  lua$set(reg_37, reg_38, lua$get(lua$get(reg_1.a, cns2760), cns2761));
  lua$set(reg_15, reg_36, reg_37);
  reg_44 = cns2762;
  lua$set(reg_15, reg_44, lua$get(reg_5, cns2763));
  reg_47 = newTable();
  lua$set(reg_47, cns2764, cns2765);
  if (tobool(lua$get(reg_5, cns2766))) {
    lua$set(reg_15, cns2767, reg_47);
  }
  reg_54 = cns2768;
  reg_55 = newTable();
  lua$set(reg_55, cns2769, cns2770);
  lua$set(reg_15, reg_54, reg_55);
  reg_59 = lua$get(reg_15, cns2771);
  reg_60 = newStack();
  push(reg_60, reg_15);
  reg_61 = call(reg_59, reg_60);
  if (tobool(lua$get(reg_5, cns2772))) {
    reg_69 = lua$get(lua$get(reg_1.a, cns2773), cns2774);
    reg_70 = newStack();
    push(reg_70, lua$get(reg_5, cns2775));
    push(reg_70, cns2776);
    push(reg_70, cns2777);
    reg_75 = call(reg_69, reg_70);
  }
  reg_78 = lua$get(reg_1.a, cns2778);
  reg_79 = newStack();
  push(reg_79, lua$get(reg_5, cns2779));
  reg_82 = call(reg_78, reg_79);
  reg_83 = next(reg_82);
  reg_84 = next(reg_82);
  reg_85 = next(reg_82);
  loop_1: while (true) {
    reg_86 = newStack();
    push(reg_86, reg_84);
    push(reg_86, reg_85);
    reg_87 = call(reg_83, reg_86);
    reg_85 = next(reg_87);
    reg_89 = next(reg_87);
    if (tobool(eq(reg_85, nil()))) break loop_1;
    reg_94 = lua$get(reg_15, cns2780);
    reg_95 = newStack();
    push(reg_95, reg_15);
    reg_96 = newTable();
    reg_97 = cns2781;
    lua$set(reg_96, reg_97, lua$get(reg_1.a, cns2782));
    lua$set(reg_96, cns2783, reg_47);
    push(reg_95, reg_96);
    reg_103 = first(call(reg_94, reg_95));
    reg_107 = lua$get(lua$get(reg_15, cns2784), cns2785);
    reg_109 = lua$get(reg_15, cns2786);
    reg_110 = newStack();
    push(reg_110, reg_15);
    reg_111 = newTable();
    lua$set(reg_111, cns2787, cns2788);
    lua$set(reg_111, cns2789, reg_103);
    push(reg_110, reg_111);
    lua$set(reg_107, reg_89, first(call(reg_109, reg_110)));
  }
  reg_118 = lua$get(reg_15, cns2790);
  reg_119 = newStack();
  push(reg_119, reg_15);
  push(reg_119, lua$get(reg_5, cns2791));
  reg_122 = call(reg_118, reg_119);
  reg_127 = eq(lua$length(lua$get(reg_5, cns2792)), cns2793);
  if (!tobool(reg_127)) {
    reg_130 = lua$get(reg_5, cns2794);
    reg_127 = ne(lua$get(lua$get(reg_130, lua$length(lua$get(reg_5, cns2795))), cns2796), cns2797);
  }
  if (tobool(reg_127)) {
    reg_141 = lua$get(reg_15, cns2798);
    reg_142 = newStack();
    push(reg_142, reg_15);
    push(reg_142, lua$get(reg_1.a, cns2799));
    reg_147 = first(call(reg_141, reg_142));
    reg_149 = lua$get(reg_15, cns2800);
    reg_150 = newStack();
    push(reg_150, reg_15);
    reg_151 = newTable();
    lua$set(reg_151, cns2801, cns2802);
    lua$set(reg_151, cns2803, reg_147);
    push(reg_150, reg_151);
    reg_155 = call(reg_149, reg_150);
  }
  reg_157 = lua$get(reg_15, cns2804);
  reg_158 = newStack();
  push(reg_158, reg_15);
  reg_159 = call(reg_157, reg_158);
  reg_161 = lua$get(reg_15, cns2805);
  reg_162 = newStack();
  push(reg_162, reg_15);
  reg_163 = call(reg_161, reg_162);
  reg_169 = first(call(lua$get(reg_1.a, cns2806), newStack()));
  reg_170 = cns2807;
  reg_171 = newTable();
  reg_172 = cns2808;
  reg_173 = newTable();
  lua$set(reg_173, cns2809, cns2810);
  lua$set(reg_173, cns2811, reg_15);
  lua$set(reg_171, reg_172, reg_173);
  lua$set(reg_169, reg_170, reg_171);
  reg_182 = first(call(lua$get(reg_1.a, cns2812), newStack()));
  reg_183 = cns2813;
  lua$set(reg_182, reg_183, lua$get(reg_1.a, cns2814));
  lua$set(reg_182, cns2815, reg_169);
  reg_189 = lua$get(reg_182, cns2816);
  reg_190 = newStack();
  push(reg_190, reg_182);
  push(reg_190, cns2817);
  reg_192 = newTable();
  reg_193 = cns2818;
  lua$set(reg_192, reg_193, lua$get(reg_4, cns2819));
  push(reg_190, reg_192);
  reg_196 = newTable();
  reg_197 = cns2820;
  lua$set(reg_196, reg_197, lua$get(reg_1.a, cns2821));
  push(reg_190, reg_196);
  reg_202 = first(call(reg_189, reg_190));
  reg_204 = lua$get(reg_4, cns2822);
  reg_205 = newStack();
  push(reg_205, reg_4);
  push(reg_205, reg_202);
  push(reg_205, lua$get(reg_4, cns2823));
  reg_209 = first(call(reg_204, reg_205));
  reg_210 = newStack();
  reg_212 = lua$get(reg_4, cns2824);
  reg_213 = newStack();
  push(reg_213, reg_4);
  push(reg_213, lua$get(reg_1.a, cns2825));
  push(reg_213, reg_209);
  append(reg_210, call(reg_212, reg_213));
  return reg_210;
}
var cns2826 = anyStr("Function")
var cns2827 = anyStr("compile_require")
var cns2828 = anyStr("key")
var cns2829 = anyStr("base")
var cns2830 = anyStr("type")
var cns2831 = anyStr("var")
var cns2832 = anyStr("base")
var cns2833 = anyStr("name")
var cns2834 = anyStr("require")
var cns2835 = anyStr("get_local")
var cns2836 = anyStr("require")
var cns2837 = anyStr("values")
var cns2838 = parseNum("1")
var cns2839 = anyStr("error")
var cns2840 = anyStr("lua:")
var cns2841 = anyStr("line")
var cns2842 = anyStr(": require expects exactly one argument")
var cns2843 = anyStr("values")
var cns2844 = parseNum("1")
var cns2845 = anyStr("type")
var cns2846 = anyStr("str")
var cns2847 = anyStr("error")
var cns2848 = anyStr("lua:")
var cns2849 = anyStr("line")
var cns2850 = anyStr(": module name can only be a string literal")
var cns2851 = anyStr("module")
var cns2852 = anyStr("values")
var cns2853 = parseNum("1")
var cns2854 = anyStr("value")
var cns2855 = anyStr("func")
var cns2856 = anyStr("lua_main")
var cns2857 = parseNum("1")
var cns2858 = anyStr("any_t")
var cns2859 = parseNum("1")
var cns2860 = anyStr("stack_t")
var cns2861 = anyStr("get_local")
var cns2862 = anyStr(".ENV")
var cns2863 = anyStr("inst")
var cns2864 = parseNum("1")
var cns2865 = parseNum("2")
function Function_compile_require (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_24, reg_25, reg_30, reg_39, reg_40, reg_41, reg_59, reg_60, reg_61, reg_70, reg_71, reg_79, reg_81, reg_82, reg_84, reg_85, reg_89, reg_90, reg_95, reg_97, reg_98, reg_101, reg_102, reg_104, reg_105, reg_106;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = lua$get(reg_5, cns2828);
  if (!tobool(reg_7)) {
    reg_7 = ne(lua$get(lua$get(reg_5, cns2829), cns2830), cns2831);
  }
  if (!tobool(reg_7)) {
    reg_7 = ne(lua$get(lua$get(reg_5, cns2832), cns2833), cns2834);
  }
  if (!tobool(reg_7)) {
    reg_24 = lua$get(reg_4, cns2835);
    reg_25 = newStack();
    push(reg_25, reg_4);
    push(reg_25, cns2836);
    reg_7 = first(call(reg_24, reg_25));
  }
  if (tobool(reg_7)) {
    reg_30 = newStack();
    return reg_30;
  }
  if (tobool(ne(lua$length(lua$get(reg_5, cns2837)), cns2838))) {
    reg_39 = lua$get(reg_1.a, cns2839);
    reg_40 = newStack();
    reg_41 = cns2840;
    push(reg_40, concat(reg_41, concat(lua$get(reg_5, cns2841), cns2842)));
    reg_47 = call(reg_39, reg_40);
  }
  if (tobool(ne(lua$get(lua$get(lua$get(reg_5, cns2843), cns2844), cns2845), cns2846))) {
    reg_59 = lua$get(reg_1.a, cns2847);
    reg_60 = newStack();
    reg_61 = cns2848;
    push(reg_60, concat(reg_61, concat(lua$get(reg_5, cns2849), cns2850)));
    reg_67 = call(reg_59, reg_60);
  }
  reg_70 = lua$get(reg_1.a, cns2851);
  reg_71 = newStack();
  push(reg_71, lua$get(lua$get(lua$get(reg_5, cns2852), cns2853), cns2854));
  reg_79 = first(call(reg_70, reg_71));
  reg_81 = lua$get(reg_79, cns2855);
  reg_82 = newStack();
  push(reg_82, reg_79);
  push(reg_82, cns2856);
  reg_84 = newTable();
  reg_85 = cns2857;
  lua$set(reg_84, reg_85, lua$get(reg_1.a, cns2858));
  push(reg_82, reg_84);
  reg_89 = newTable();
  reg_90 = cns2859;
  lua$set(reg_89, reg_90, lua$get(reg_1.a, cns2860));
  push(reg_82, reg_89);
  reg_95 = first(call(reg_81, reg_82));
  reg_97 = lua$get(reg_4, cns2861);
  reg_98 = newStack();
  push(reg_98, reg_4);
  push(reg_98, cns2862);
  reg_101 = first(call(reg_97, reg_98));
  reg_102 = newStack();
  reg_104 = lua$get(reg_4, cns2863);
  reg_105 = newStack();
  push(reg_105, reg_4);
  reg_106 = newTable();
  lua$set(reg_106, cns2864, reg_95);
  lua$set(reg_106, cns2865, reg_101);
  push(reg_105, reg_106);
  append(reg_102, call(reg_104, reg_105));
  return reg_102;
}
var cns2866 = anyStr("Function")
var cns2867 = anyStr("compile_call")
var cns2868 = anyStr("compile_require")
var cns2869 = anyStr("compileExpr")
var cns2870 = anyStr("base")
var cns2871 = anyStr("au_type")
var cns2872 = anyStr("compile_au_call")
var cns2873 = anyStr("au_type")
var cns2874 = anyStr("call")
var cns2875 = anyStr("stack_f")
var cns2876 = anyStr("inst")
var cns2877 = parseNum("1")
var cns2878 = anyStr("push_f")
var cns2879 = parseNum("2")
var cns2880 = parseNum("3")
var cns2881 = anyStr("err")
var cns2882 = anyStr("cannot use a auro expression as value")
var cns2883 = anyStr("key")
var cns2884 = anyStr("compileExpr")
var cns2885 = anyStr("type")
var cns2886 = anyStr("str")
var cns2887 = anyStr("value")
var cns2888 = anyStr("key")
var cns2889 = anyStr("inst")
var cns2890 = parseNum("1")
var cns2891 = anyStr("get_f")
var cns2892 = parseNum("2")
var cns2893 = parseNum("3")
var cns2894 = anyStr("call")
var cns2895 = anyStr("stack_f")
var cns2896 = anyStr("key")
var cns2897 = anyStr("inst")
var cns2898 = parseNum("1")
var cns2899 = anyStr("push_f")
var cns2900 = parseNum("2")
var cns2901 = parseNum("3")
var cns2902 = anyStr("ipairs")
var cns2903 = anyStr("values")
var cns2904 = anyStr("values")
var cns2905 = anyStr("type")
var cns2906 = anyStr("call")
var cns2907 = anyStr("compile_call")
var cns2908 = anyStr("inst")
var cns2909 = parseNum("1")
var cns2910 = anyStr("append_f")
var cns2911 = parseNum("2")
var cns2912 = parseNum("3")
var cns2913 = anyStr("type")
var cns2914 = anyStr("vararg")
var cns2915 = anyStr("inst")
var cns2916 = parseNum("1")
var cns2917 = anyStr("append_f")
var cns2918 = parseNum("2")
var cns2919 = parseNum("3")
var cns2920 = anyStr("get_vararg")
var cns2921 = anyStr("compileExpr")
var cns2922 = anyStr("inst")
var cns2923 = parseNum("1")
var cns2924 = anyStr("push_f")
var cns2925 = parseNum("2")
var cns2926 = parseNum("3")
var cns2927 = anyStr("inst")
var cns2928 = parseNum("1")
var cns2929 = anyStr("call_f")
var cns2930 = parseNum("2")
var cns2931 = parseNum("3")
var cns2932 = anyStr("line")
var cns2933 = anyStr("line")
function Function_compile_call (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_8, reg_9, reg_11, reg_13, reg_15, reg_16, reg_21, reg_26, reg_27, reg_29, reg_35, reg_36, reg_41, reg_43, reg_44, reg_45, reg_46, reg_53, reg_55, reg_58, reg_59, reg_62, reg_67, reg_68, reg_69, reg_72, reg_76, reg_78, reg_79, reg_80, reg_81, reg_90, reg_91, reg_96, reg_101, reg_102, reg_103, reg_104, reg_113, reg_114, reg_117, reg_118, reg_119, reg_120, reg_121, reg_122, reg_123, reg_124, reg_131, reg_132, reg_140, reg_141, reg_143, reg_145, reg_146, reg_147, reg_148, reg_155, reg_163, reg_164, reg_165, reg_166, reg_171, reg_173, reg_174, reg_178, reg_179, reg_181, reg_183, reg_184, reg_185, reg_186, reg_193, reg_195, reg_196, reg_197, reg_198, reg_204;
  var goto_76=false, goto_82=false, goto_221=false, goto_253=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_8 = lua$get(reg_4, cns2868);
  reg_9 = newStack();
  push(reg_9, reg_4);
  push(reg_9, reg_5);
  reg_11 = first(call(reg_8, reg_9));
  if (tobool(reg_11)) {
    reg_13 = newStack();
    push(reg_13, reg_11);
    return reg_13;
  }
  reg_15 = lua$get(reg_4, cns2869);
  reg_16 = newStack();
  push(reg_16, reg_4);
  push(reg_16, lua$get(reg_5, cns2870));
  push(reg_16, lua$true());
  reg_21 = first(call(reg_15, reg_16));
  if (tobool(lua$get(reg_21, cns2871))) {
    reg_26 = lua$get(reg_4, cns2872);
    reg_27 = newStack();
    push(reg_27, reg_4);
    push(reg_27, reg_5);
    push(reg_27, reg_21);
    reg_29 = first(call(reg_26, reg_27));
    goto_76 = !tobool(not(lua$get(reg_29, cns2873)));
    if (!goto_76) {
      reg_35 = lua$get(reg_4, cns2874);
      reg_36 = newStack();
      push(reg_36, reg_4);
      push(reg_36, lua$get(reg_1.a, cns2875));
      reg_41 = first(call(reg_35, reg_36));
      reg_43 = lua$get(reg_4, cns2876);
      reg_44 = newStack();
      push(reg_44, reg_4);
      reg_45 = newTable();
      reg_46 = cns2877;
      lua$set(reg_45, reg_46, lua$get(reg_1.a, cns2878));
      lua$set(reg_45, cns2879, reg_41);
      lua$set(reg_45, cns2880, reg_29);
      push(reg_44, reg_45);
      reg_52 = call(reg_43, reg_44);
      reg_53 = newStack();
      push(reg_53, reg_41);
      return reg_53;
    }
    if ((goto_76 || false)) {
      goto_76 = false;
      goto_82 = !tobool(reg_6);
      if (!goto_82) {
        reg_55 = newStack();
        push(reg_55, reg_29);
        return reg_55;
      }
      if ((goto_82 || false)) {
        goto_82 = false;
        reg_58 = lua$get(reg_1.a, cns2881);
        reg_59 = newStack();
        push(reg_59, cns2882);
        push(reg_59, reg_5);
        reg_61 = call(reg_58, reg_59);
      }
    }
  }
  reg_62 = reg_21;
  if (tobool(lua$get(reg_5, cns2883))) {
    reg_67 = lua$get(reg_4, cns2884);
    reg_68 = newStack();
    push(reg_68, reg_4);
    reg_69 = newTable();
    lua$set(reg_69, cns2885, cns2886);
    reg_72 = cns2887;
    lua$set(reg_69, reg_72, lua$get(reg_5, cns2888));
    push(reg_68, reg_69);
    reg_76 = first(call(reg_67, reg_68));
    reg_78 = lua$get(reg_4, cns2889);
    reg_79 = newStack();
    push(reg_79, reg_4);
    reg_80 = newTable();
    reg_81 = cns2890;
    lua$set(reg_80, reg_81, lua$get(reg_1.a, cns2891));
    lua$set(reg_80, cns2892, reg_21);
    lua$set(reg_80, cns2893, reg_76);
    push(reg_79, reg_80);
    reg_62 = first(call(reg_78, reg_79));
  }
  reg_90 = lua$get(reg_4, cns2894);
  reg_91 = newStack();
  push(reg_91, reg_4);
  push(reg_91, lua$get(reg_1.a, cns2895));
  reg_96 = first(call(reg_90, reg_91));
  if (tobool(lua$get(reg_5, cns2896))) {
    reg_101 = lua$get(reg_4, cns2897);
    reg_102 = newStack();
    push(reg_102, reg_4);
    reg_103 = newTable();
    reg_104 = cns2898;
    lua$set(reg_103, reg_104, lua$get(reg_1.a, cns2899));
    lua$set(reg_103, cns2900, reg_96);
    lua$set(reg_103, cns2901, reg_21);
    push(reg_102, reg_103);
    reg_110 = call(reg_101, reg_102);
  }
  reg_113 = lua$get(reg_1.a, cns2902);
  reg_114 = newStack();
  push(reg_114, lua$get(reg_5, cns2903));
  reg_117 = call(reg_113, reg_114);
  reg_118 = next(reg_117);
  reg_119 = next(reg_117);
  reg_120 = next(reg_117);
  loop_1: while (true) {
    reg_121 = newStack();
    push(reg_121, reg_119);
    push(reg_121, reg_120);
    reg_122 = call(reg_118, reg_121);
    reg_123 = next(reg_122);
    reg_120 = reg_123;
    reg_124 = next(reg_122);
    if (tobool(eq(reg_120, nil()))) break loop_1;
    reg_131 = eq(reg_123, lua$length(lua$get(reg_5, cns2904)));
    reg_132 = reg_131;
    if (tobool(reg_131)) {
      reg_132 = eq(lua$get(reg_124, cns2905), cns2906);
    }
    goto_221 = !tobool(reg_132);
    if (!goto_221) {
      reg_140 = lua$get(reg_4, cns2907);
      reg_141 = newStack();
      push(reg_141, reg_4);
      push(reg_141, reg_124);
      reg_143 = first(call(reg_140, reg_141));
      reg_145 = lua$get(reg_4, cns2908);
      reg_146 = newStack();
      push(reg_146, reg_4);
      reg_147 = newTable();
      reg_148 = cns2909;
      lua$set(reg_147, reg_148, lua$get(reg_1.a, cns2910));
      lua$set(reg_147, cns2911, reg_96);
      lua$set(reg_147, cns2912, reg_143);
      push(reg_146, reg_147);
      reg_154 = call(reg_145, reg_146);
    }
    if ((goto_221 || false)) {
      goto_221 = false;
      reg_155 = reg_131;
      if (tobool(reg_131)) {
        reg_155 = eq(lua$get(reg_124, cns2913), cns2914);
      }
      goto_253 = !tobool(reg_155);
      if (!goto_253) {
        reg_163 = lua$get(reg_4, cns2915);
        reg_164 = newStack();
        push(reg_164, reg_4);
        reg_165 = newTable();
        reg_166 = cns2916;
        lua$set(reg_165, reg_166, lua$get(reg_1.a, cns2917));
        lua$set(reg_165, cns2918, reg_96);
        reg_171 = cns2919;
        reg_173 = lua$get(reg_4, cns2920);
        reg_174 = newStack();
        push(reg_174, reg_4);
        table_append(reg_165, reg_171, call(reg_173, reg_174));
        push(reg_164, reg_165);
        reg_176 = call(reg_163, reg_164);
      }
      if ((goto_253 || false)) {
        goto_253 = false;
        reg_178 = lua$get(reg_4, cns2921);
        reg_179 = newStack();
        push(reg_179, reg_4);
        push(reg_179, reg_124);
        reg_181 = first(call(reg_178, reg_179));
        reg_183 = lua$get(reg_4, cns2922);
        reg_184 = newStack();
        push(reg_184, reg_4);
        reg_185 = newTable();
        reg_186 = cns2923;
        lua$set(reg_185, reg_186, lua$get(reg_1.a, cns2924));
        lua$set(reg_185, cns2925, reg_96);
        lua$set(reg_185, cns2926, reg_181);
        push(reg_184, reg_185);
        reg_192 = call(reg_183, reg_184);
      }
    }
  }
  reg_193 = newStack();
  reg_195 = lua$get(reg_4, cns2927);
  reg_196 = newStack();
  push(reg_196, reg_4);
  reg_197 = newTable();
  reg_198 = cns2928;
  lua$set(reg_197, reg_198, lua$get(reg_1.a, cns2929));
  lua$set(reg_197, cns2930, reg_62);
  lua$set(reg_197, cns2931, reg_96);
  reg_204 = cns2932;
  lua$set(reg_197, reg_204, lua$get(reg_5, cns2933));
  push(reg_196, reg_197);
  append(reg_193, call(reg_195, reg_196));
  return reg_193;
}
var cns2934 = anyStr("Function")
var cns2935 = anyStr("compile_bool")
var cns2936 = anyStr("compileExpr")
var cns2937 = anyStr("au_type")
var cns2938 = anyStr("inst")
var cns2939 = parseNum("1")
var cns2940 = anyStr("bool_f")
var cns2941 = parseNum("2")
var cns2942 = anyStr("au_type")
var cns2943 = anyStr("value")
var cns2944 = anyStr("type_id")
var cns2945 = anyStr("bool_t")
var cns2946 = anyStr("id")
var cns2947 = anyStr("au_type")
var cns2948 = anyStr("value")
var cns2949 = anyStr("type_id")
var cns2950 = anyStr("bool_t")
var cns2951 = anyStr("id")
var cns2952 = anyStr("err")
var cns2953 = anyStr("cannot use a auro expression as value")
function Function_compile_bool (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_11, reg_17, reg_18, reg_19, reg_20, reg_27, reg_34, reg_35, reg_39, reg_42, reg_50, reg_53, reg_54, reg_57;
  var goto_47=false, goto_68=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = lua$get(reg_4, cns2936);
  reg_8 = newStack();
  push(reg_8, reg_4);
  push(reg_8, reg_5);
  push(reg_8, lua$true());
  reg_11 = first(call(reg_7, reg_8));
  goto_47 = !tobool(not(lua$get(reg_11, cns2937)));
  if (!goto_47) {
    reg_17 = lua$get(reg_4, cns2938);
    reg_18 = newStack();
    push(reg_18, reg_4);
    reg_19 = newTable();
    reg_20 = cns2939;
    lua$set(reg_19, reg_20, lua$get(reg_1.a, cns2940));
    lua$set(reg_19, cns2941, reg_11);
    lua$set(reg_19, cns2942, cns2943);
    reg_27 = cns2944;
    lua$set(reg_19, reg_27, lua$get(lua$get(reg_1.a, cns2945), cns2946));
    push(reg_18, reg_19);
    reg_34 = first(call(reg_17, reg_18));
    reg_35 = newStack();
    push(reg_35, reg_34);
    return reg_35;
  }
  if ((goto_47 || false)) {
    goto_47 = false;
    reg_39 = eq(lua$get(reg_11, cns2947), cns2948);
    if (tobool(reg_39)) {
      reg_42 = lua$get(reg_11, cns2949);
      reg_39 = eq(reg_42, lua$get(lua$get(reg_1.a, cns2950), cns2951));
    }
    goto_68 = !tobool(reg_39);
    if (!goto_68) {
      reg_50 = newStack();
      push(reg_50, reg_11);
      return reg_50;
    }
    if ((goto_68 || false)) {
      goto_68 = false;
      reg_53 = lua$get(reg_1.a, cns2952);
      reg_54 = newStack();
      push(reg_54, cns2953);
      push(reg_54, reg_5);
      reg_56 = call(reg_53, reg_54);
    }
  }
  reg_57 = newStack();
  return reg_57;
}
var cns2954 = anyStr("Function")
var cns2955 = anyStr("compileExpr")
var cns2956 = anyStr("type")
var cns2957 = anyStr("const")
var cns2958 = anyStr("value")
var cns2959 = anyStr("nil")
var cns2960 = anyStr("nil_f")
var cns2961 = anyStr("value")
var cns2962 = anyStr("true")
var cns2963 = anyStr("true_f")
var cns2964 = anyStr("value")
var cns2965 = anyStr("false")
var cns2966 = anyStr("false_f")
var cns2967 = anyStr("inst")
var cns2968 = parseNum("1")
var cns2969 = anyStr("num")
var cns2970 = anyStr("constant")
var cns2971 = anyStr("value")
var cns2972 = anyStr("number")
var cns2973 = anyStr("inst")
var cns2974 = parseNum("1")
var cns2975 = anyStr("str")
var cns2976 = anyStr("constant")
var cns2977 = anyStr("value")
var cns2978 = anyStr("inst")
var cns2979 = parseNum("1")
var cns2980 = anyStr("vararg")
var cns2981 = anyStr("inst")
var cns2982 = parseNum("1")
var cns2983 = anyStr("first_f")
var cns2984 = parseNum("2")
var cns2985 = anyStr("get_vararg")
var cns2986 = anyStr("var")
var cns2987 = anyStr("get_local")
var cns2988 = anyStr("name")
var cns2989 = anyStr("au_type")
var cns2990 = anyStr("err")
var cns2991 = anyStr("cannot use a auro expression as value")
var cns2992 = anyStr("au_type")
var cns2993 = anyStr("value")
var cns2994 = anyStr("inst")
var cns2995 = parseNum("1")
var cns2996 = anyStr("var")
var cns2997 = parseNum("2")
var cns2998 = anyStr("au_type")
var cns2999 = anyStr("au_type")
var cns3000 = anyStr("type_id")
var cns3001 = anyStr("type_id")
var cns3002 = anyStr("get_local")
var cns3003 = anyStr("_ENV")
var cns3004 = anyStr("err")
var cns3005 = anyStr("local \"_ENV\" not in sight")
var cns3006 = anyStr("inst")
var cns3007 = parseNum("1")
var cns3008 = anyStr("var")
var cns3009 = parseNum("2")
var cns3010 = anyStr("compileExpr")
var cns3011 = anyStr("type")
var cns3012 = anyStr("str")
var cns3013 = anyStr("value")
var cns3014 = anyStr("name")
var cns3015 = anyStr("call")
var cns3016 = anyStr("get_f")
var cns3017 = anyStr("unop")
var cns3018 = anyStr("unops")
var cns3019 = anyStr("op")
var cns3020 = anyStr("compileExpr")
var cns3021 = anyStr("value")
var cns3022 = anyStr("inst")
var cns3023 = parseNum("1")
var cns3024 = parseNum("2")
var cns3025 = anyStr("line")
var cns3026 = anyStr("line")
var cns3027 = anyStr("binop")
var cns3028 = anyStr("op")
var cns3029 = anyStr("and")
var cns3030 = anyStr("op")
var cns3031 = anyStr("or")
var cns3032 = anyStr("lbl")
var cns3033 = anyStr("compileExpr")
var cns3034 = anyStr("left")
var cns3035 = anyStr("inst")
var cns3036 = parseNum("1")
var cns3037 = anyStr("local")
var cns3038 = parseNum("2")
var cns3039 = anyStr("op")
var cns3040 = anyStr("or")
var cns3041 = anyStr("inst")
var cns3042 = parseNum("1")
var cns3043 = anyStr("jif")
var cns3044 = parseNum("2")
var cns3045 = parseNum("3")
var cns3046 = anyStr("inst")
var cns3047 = parseNum("1")
var cns3048 = anyStr("nif")
var cns3049 = parseNum("2")
var cns3050 = parseNum("3")
var cns3051 = anyStr("compileExpr")
var cns3052 = anyStr("right")
var cns3053 = anyStr("inst")
var cns3054 = parseNum("1")
var cns3055 = anyStr("set")
var cns3056 = parseNum("2")
var cns3057 = parseNum("3")
var cns3058 = anyStr("inst")
var cns3059 = parseNum("1")
var cns3060 = anyStr("label")
var cns3061 = parseNum("2")
var cns3062 = anyStr("binops")
var cns3063 = anyStr("op")
var cns3064 = anyStr("compileExpr")
var cns3065 = anyStr("left")
var cns3066 = anyStr("compileExpr")
var cns3067 = anyStr("right")
var cns3068 = anyStr("inst")
var cns3069 = parseNum("1")
var cns3070 = parseNum("2")
var cns3071 = parseNum("3")
var cns3072 = anyStr("line")
var cns3073 = anyStr("line")
var cns3074 = anyStr("index")
var cns3075 = anyStr("compileExpr")
var cns3076 = anyStr("base")
var cns3077 = anyStr("compileExpr")
var cns3078 = anyStr("key")
var cns3079 = anyStr("call")
var cns3080 = anyStr("get_f")
var cns3081 = anyStr("field")
var cns3082 = anyStr("type")
var cns3083 = anyStr("str")
var cns3084 = anyStr("value")
var cns3085 = anyStr("key")
var cns3086 = anyStr("compileExpr")
var cns3087 = anyStr("type")
var cns3088 = anyStr("index")
var cns3089 = anyStr("base")
var cns3090 = anyStr("base")
var cns3091 = anyStr("key")
var cns3092 = anyStr("line")
var cns3093 = anyStr("line")
var cns3094 = anyStr("function")
var cns3095 = anyStr("createFunction")
var cns3096 = anyStr("constructor")
var cns3097 = anyStr("call")
var cns3098 = anyStr("table_f")
var cns3099 = parseNum("1")
var cns3100 = anyStr("ipairs")
var cns3101 = anyStr("items")
var cns3102 = anyStr("type")
var cns3103 = anyStr("indexitem")
var cns3104 = anyStr("compileExpr")
var cns3105 = anyStr("key")
var cns3106 = anyStr("type")
var cns3107 = anyStr("fielditem")
var cns3108 = anyStr("compileExpr")
var cns3109 = anyStr("type")
var cns3110 = anyStr("str")
var cns3111 = anyStr("value")
var cns3112 = anyStr("key")
var cns3113 = anyStr("type")
var cns3114 = anyStr("item")
var cns3115 = anyStr("compileExpr")
var cns3116 = anyStr("type")
var cns3117 = anyStr("num")
var cns3118 = anyStr("value")
var cns3119 = parseNum("1")
var cns3120 = anyStr("items")
var cns3121 = anyStr("value")
var cns3122 = anyStr("type")
var cns3123 = anyStr("call")
var cns3124 = anyStr("compile_call")
var cns3125 = anyStr("value")
var cns3126 = anyStr("value")
var cns3127 = anyStr("type")
var cns3128 = anyStr("vararg")
var cns3129 = anyStr("inst")
var cns3130 = parseNum("1")
var cns3131 = anyStr("copystack_f")
var cns3132 = parseNum("2")
var cns3133 = anyStr("get_vararg")
var cns3134 = anyStr("inst")
var cns3135 = parseNum("1")
var cns3136 = anyStr("table_append_f")
var cns3137 = parseNum("2")
var cns3138 = parseNum("3")
var cns3139 = parseNum("4")
var cns3140 = anyStr("compileExpr")
var cns3141 = anyStr("value")
var cns3142 = anyStr("inst")
var cns3143 = parseNum("1")
var cns3144 = anyStr("set_f")
var cns3145 = parseNum("2")
var cns3146 = parseNum("3")
var cns3147 = parseNum("4")
var cns3148 = anyStr("call")
var cns3149 = anyStr("compile_call")
var cns3150 = anyStr("regs")
var cns3151 = anyStr("regs")
var cns3152 = parseNum("0")
var cns3153 = anyStr("regs")
var cns3154 = parseNum("1")
var cns3155 = anyStr("au_type")
var cns3156 = anyStr("inst")
var cns3157 = parseNum("1")
var cns3158 = anyStr("first_f")
var cns3159 = parseNum("2")
var cns3160 = anyStr("err")
var cns3161 = anyStr("expression ")
var cns3162 = anyStr(" not supported")
function Function_compileExpr (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_8, reg_12, reg_37, reg_39, reg_40, reg_41, reg_49, reg_50, reg_55, reg_56, reg_58, reg_59, reg_60, reg_68, reg_69, reg_73, reg_74, reg_76, reg_77, reg_78, reg_84, reg_86, reg_87, reg_88, reg_89, reg_93, reg_95, reg_96, reg_103, reg_104, reg_108, reg_117, reg_118, reg_126, reg_127, reg_129, reg_130, reg_131, reg_135, reg_138, reg_143, reg_144, reg_147, reg_152, reg_153, reg_157, reg_158, reg_159, reg_164, reg_166, reg_167, reg_168, reg_171, reg_175, reg_176, reg_178, reg_179, reg_189, reg_192, reg_194, reg_195, reg_199, reg_200, reg_202, reg_203, reg_204, reg_207, reg_217, reg_225, reg_226, reg_228, reg_230, reg_231, reg_235, reg_237, reg_238, reg_239, reg_244, reg_251, reg_252, reg_253, reg_260, reg_261, reg_262, reg_269, reg_270, reg_274, reg_276, reg_277, reg_278, reg_285, reg_286, reg_287, reg_292, reg_295, reg_298, reg_300, reg_301, reg_305, reg_307, reg_308, reg_312, reg_313, reg_315, reg_316, reg_317, reg_321, reg_329, reg_330, reg_334, reg_336, reg_337, reg_341, reg_342, reg_344, reg_345, reg_353, reg_356, reg_359, reg_361, reg_362, reg_363, reg_366, reg_370, reg_377, reg_379, reg_380, reg_386, reg_387, reg_392, reg_393, reg_396, reg_397, reg_400, reg_401, reg_402, reg_403, reg_404, reg_405, reg_406, reg_407, reg_411, reg_418, reg_419, reg_430, reg_431, reg_432, reg_435, reg_446, reg_447, reg_448, reg_461, reg_470, reg_471, reg_484, reg_485, reg_486, reg_487, reg_491, reg_493, reg_494, reg_500, reg_501, reg_502, reg_503, reg_512, reg_513, reg_517, reg_519, reg_520, reg_521, reg_522, reg_530, reg_535, reg_536, reg_538, reg_540, reg_548, reg_556, reg_557, reg_559, reg_560, reg_561, reg_562, reg_570, reg_571, reg_572, reg_577;
  var goto_23=false, goto_34=false, goto_58=false, goto_86=false, goto_112=false, goto_139=false, goto_205=false, goto_268=false, goto_306=false, goto_372=false, goto_425=false, goto_470=false, goto_507=false, goto_543=false, goto_557=false, goto_612=false, goto_635=false, goto_684=false, goto_770=false, goto_828=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_8 = lua$get(reg_5, cns2956);
  goto_58 = !tobool(eq(reg_8, cns2957));
  if (!goto_58) {
    reg_12 = nil();
    goto_23 = !tobool(eq(lua$get(reg_5, cns2958), cns2959));
    if (!goto_23) {
      reg_12 = lua$get(reg_1.a, cns2960);
    }
    if ((goto_23 || false)) {
      goto_23 = false;
      goto_34 = !tobool(eq(lua$get(reg_5, cns2961), cns2962));
      if (!goto_34) {
        reg_12 = lua$get(reg_1.a, cns2963);
      }
      if ((goto_34 || false)) {
        goto_34 = false;
        if (tobool(eq(lua$get(reg_5, cns2964), cns2965))) {
          reg_12 = lua$get(reg_1.a, cns2966);
        }
      }
    }
    reg_37 = newStack();
    reg_39 = lua$get(reg_4, cns2967);
    reg_40 = newStack();
    push(reg_40, reg_4);
    reg_41 = newTable();
    lua$set(reg_41, cns2968, reg_12);
    push(reg_40, reg_41);
    append(reg_37, call(reg_39, reg_40));
    return reg_37;
  }
  if ((goto_58 || false)) {
    goto_58 = false;
    goto_86 = !tobool(eq(reg_8, cns2969));
    if (!goto_86) {
      reg_49 = lua$get(reg_1.a, cns2970);
      reg_50 = newStack();
      push(reg_50, lua$get(reg_5, cns2971));
      push(reg_50, cns2972);
      reg_55 = first(call(reg_49, reg_50));
      reg_56 = newStack();
      reg_58 = lua$get(reg_4, cns2973);
      reg_59 = newStack();
      push(reg_59, reg_4);
      reg_60 = newTable();
      lua$set(reg_60, cns2974, reg_55);
      push(reg_59, reg_60);
      append(reg_56, call(reg_58, reg_59));
      return reg_56;
    }
    if ((goto_86 || false)) {
      goto_86 = false;
      goto_112 = !tobool(eq(reg_8, cns2975));
      if (!goto_112) {
        reg_68 = lua$get(reg_1.a, cns2976);
        reg_69 = newStack();
        push(reg_69, lua$get(reg_5, cns2977));
        reg_73 = first(call(reg_68, reg_69));
        reg_74 = newStack();
        reg_76 = lua$get(reg_4, cns2978);
        reg_77 = newStack();
        push(reg_77, reg_4);
        reg_78 = newTable();
        lua$set(reg_78, cns2979, reg_73);
        push(reg_77, reg_78);
        append(reg_74, call(reg_76, reg_77));
        return reg_74;
      }
      if ((goto_112 || false)) {
        goto_112 = false;
        goto_139 = !tobool(eq(reg_8, cns2980));
        if (!goto_139) {
          reg_84 = newStack();
          reg_86 = lua$get(reg_4, cns2981);
          reg_87 = newStack();
          push(reg_87, reg_4);
          reg_88 = newTable();
          reg_89 = cns2982;
          lua$set(reg_88, reg_89, lua$get(reg_1.a, cns2983));
          reg_93 = cns2984;
          reg_95 = lua$get(reg_4, cns2985);
          reg_96 = newStack();
          push(reg_96, reg_4);
          table_append(reg_88, reg_93, call(reg_95, reg_96));
          push(reg_87, reg_88);
          append(reg_84, call(reg_86, reg_87));
          return reg_84;
        }
        if ((goto_139 || false)) {
          goto_139 = false;
          goto_268 = !tobool(eq(reg_8, cns2986));
          if (!goto_268) {
            reg_103 = lua$get(reg_4, cns2987);
            reg_104 = newStack();
            push(reg_104, reg_4);
            push(reg_104, lua$get(reg_5, cns2988));
            reg_108 = first(call(reg_103, reg_104));
            goto_205 = !tobool(reg_108);
            if (!goto_205) {
              if (tobool(lua$get(reg_108, cns2989))) {
                if (tobool(not(reg_6))) {
                  reg_117 = lua$get(reg_1.a, cns2990);
                  reg_118 = newStack();
                  push(reg_118, cns2991);
                  push(reg_118, reg_5);
                  reg_120 = call(reg_117, reg_118);
                }
                if (tobool(ne(lua$get(reg_108, cns2992), cns2993))) {
                  reg_126 = newStack();
                  push(reg_126, reg_108);
                  return reg_126;
                }
              }
              reg_127 = newStack();
              reg_129 = lua$get(reg_4, cns2994);
              reg_130 = newStack();
              push(reg_130, reg_4);
              reg_131 = newTable();
              lua$set(reg_131, cns2995, cns2996);
              lua$set(reg_131, cns2997, reg_108);
              reg_135 = cns2998;
              lua$set(reg_131, reg_135, lua$get(reg_108, cns2999));
              reg_138 = cns3000;
              lua$set(reg_131, reg_138, lua$get(reg_108, cns3001));
              push(reg_130, reg_131);
              append(reg_127, call(reg_129, reg_130));
              return reg_127;
            }
            if ((goto_205 || false)) {
              goto_205 = false;
              reg_143 = lua$get(reg_4, cns3002);
              reg_144 = newStack();
              push(reg_144, reg_4);
              push(reg_144, cns3003);
              reg_147 = first(call(reg_143, reg_144));
              if (tobool(not(reg_147))) {
                reg_152 = lua$get(reg_1.a, cns3004);
                reg_153 = newStack();
                push(reg_153, cns3005);
                push(reg_153, reg_5);
                reg_155 = call(reg_152, reg_153);
              }
              reg_157 = lua$get(reg_4, cns3006);
              reg_158 = newStack();
              push(reg_158, reg_4);
              reg_159 = newTable();
              lua$set(reg_159, cns3007, cns3008);
              lua$set(reg_159, cns3009, reg_147);
              push(reg_158, reg_159);
              reg_164 = first(call(reg_157, reg_158));
              reg_166 = lua$get(reg_4, cns3010);
              reg_167 = newStack();
              push(reg_167, reg_4);
              reg_168 = newTable();
              lua$set(reg_168, cns3011, cns3012);
              reg_171 = cns3013;
              lua$set(reg_168, reg_171, lua$get(reg_5, cns3014));
              push(reg_167, reg_168);
              reg_175 = first(call(reg_166, reg_167));
              reg_176 = newStack();
              reg_178 = lua$get(reg_4, cns3015);
              reg_179 = newStack();
              push(reg_179, reg_4);
              push(reg_179, lua$get(reg_1.a, cns3016));
              push(reg_179, reg_164);
              push(reg_179, reg_175);
              append(reg_176, call(reg_178, reg_179));
              return reg_176;
            }
          }
          if ((goto_268 || false)) {
            goto_268 = false;
            goto_306 = !tobool(eq(reg_8, cns3017));
            if (!goto_306) {
              reg_189 = lua$get(reg_1.a, cns3018);
              reg_192 = lua$get(reg_189, lua$get(reg_5, cns3019));
              reg_194 = lua$get(reg_4, cns3020);
              reg_195 = newStack();
              push(reg_195, reg_4);
              push(reg_195, lua$get(reg_5, cns3021));
              reg_199 = first(call(reg_194, reg_195));
              reg_200 = newStack();
              reg_202 = lua$get(reg_4, cns3022);
              reg_203 = newStack();
              push(reg_203, reg_4);
              reg_204 = newTable();
              lua$set(reg_204, cns3023, reg_192);
              lua$set(reg_204, cns3024, reg_199);
              reg_207 = cns3025;
              lua$set(reg_204, reg_207, lua$get(reg_5, cns3026));
              push(reg_203, reg_204);
              append(reg_200, call(reg_202, reg_203));
              return reg_200;
            }
            if ((goto_306 || false)) {
              goto_306 = false;
              goto_470 = !tobool(eq(reg_8, cns3027));
              if (!goto_470) {
                reg_217 = eq(lua$get(reg_5, cns3028), cns3029);
                if (!tobool(reg_217)) {
                  reg_217 = eq(lua$get(reg_5, cns3030), cns3031);
                }
                goto_425 = !tobool(reg_217);
                if (!goto_425) {
                  reg_225 = lua$get(reg_4, cns3032);
                  reg_226 = newStack();
                  push(reg_226, reg_4);
                  reg_228 = first(call(reg_225, reg_226));
                  reg_230 = lua$get(reg_4, cns3033);
                  reg_231 = newStack();
                  push(reg_231, reg_4);
                  push(reg_231, lua$get(reg_5, cns3034));
                  reg_235 = first(call(reg_230, reg_231));
                  reg_237 = lua$get(reg_4, cns3035);
                  reg_238 = newStack();
                  push(reg_238, reg_4);
                  reg_239 = newTable();
                  lua$set(reg_239, cns3036, cns3037);
                  lua$set(reg_239, cns3038, reg_235);
                  push(reg_238, reg_239);
                  reg_244 = first(call(reg_237, reg_238));
                  goto_372 = !tobool(eq(lua$get(reg_5, cns3039), cns3040));
                  if (!goto_372) {
                    reg_251 = lua$get(reg_4, cns3041);
                    reg_252 = newStack();
                    push(reg_252, reg_4);
                    reg_253 = newTable();
                    lua$set(reg_253, cns3042, cns3043);
                    lua$set(reg_253, cns3044, reg_228);
                    lua$set(reg_253, cns3045, reg_235);
                    push(reg_252, reg_253);
                    reg_258 = call(reg_251, reg_252);
                  }
                  if ((goto_372 || false)) {
                    goto_372 = false;
                    reg_260 = lua$get(reg_4, cns3046);
                    reg_261 = newStack();
                    push(reg_261, reg_4);
                    reg_262 = newTable();
                    lua$set(reg_262, cns3047, cns3048);
                    lua$set(reg_262, cns3049, reg_228);
                    lua$set(reg_262, cns3050, reg_235);
                    push(reg_261, reg_262);
                    reg_267 = call(reg_260, reg_261);
                  }
                  reg_269 = lua$get(reg_4, cns3051);
                  reg_270 = newStack();
                  push(reg_270, reg_4);
                  push(reg_270, lua$get(reg_5, cns3052));
                  reg_274 = first(call(reg_269, reg_270));
                  reg_276 = lua$get(reg_4, cns3053);
                  reg_277 = newStack();
                  push(reg_277, reg_4);
                  reg_278 = newTable();
                  lua$set(reg_278, cns3054, cns3055);
                  lua$set(reg_278, cns3056, reg_244);
                  lua$set(reg_278, cns3057, reg_274);
                  push(reg_277, reg_278);
                  reg_283 = call(reg_276, reg_277);
                  reg_285 = lua$get(reg_4, cns3058);
                  reg_286 = newStack();
                  push(reg_286, reg_4);
                  reg_287 = newTable();
                  lua$set(reg_287, cns3059, cns3060);
                  lua$set(reg_287, cns3061, reg_228);
                  push(reg_286, reg_287);
                  reg_291 = call(reg_285, reg_286);
                  reg_292 = newStack();
                  push(reg_292, reg_244);
                  return reg_292;
                }
                if ((goto_425 || false)) {
                  goto_425 = false;
                  reg_295 = lua$get(reg_1.a, cns3062);
                  reg_298 = lua$get(reg_295, lua$get(reg_5, cns3063));
                  reg_300 = lua$get(reg_4, cns3064);
                  reg_301 = newStack();
                  push(reg_301, reg_4);
                  push(reg_301, lua$get(reg_5, cns3065));
                  reg_305 = first(call(reg_300, reg_301));
                  reg_307 = lua$get(reg_4, cns3066);
                  reg_308 = newStack();
                  push(reg_308, reg_4);
                  push(reg_308, lua$get(reg_5, cns3067));
                  reg_312 = first(call(reg_307, reg_308));
                  reg_313 = newStack();
                  reg_315 = lua$get(reg_4, cns3068);
                  reg_316 = newStack();
                  push(reg_316, reg_4);
                  reg_317 = newTable();
                  lua$set(reg_317, cns3069, reg_298);
                  lua$set(reg_317, cns3070, reg_305);
                  lua$set(reg_317, cns3071, reg_312);
                  reg_321 = cns3072;
                  lua$set(reg_317, reg_321, lua$get(reg_5, cns3073));
                  push(reg_316, reg_317);
                  append(reg_313, call(reg_315, reg_316));
                  return reg_313;
                }
              }
              if ((goto_470 || false)) {
                goto_470 = false;
                goto_507 = !tobool(eq(reg_8, cns3074));
                if (!goto_507) {
                  reg_329 = lua$get(reg_4, cns3075);
                  reg_330 = newStack();
                  push(reg_330, reg_4);
                  push(reg_330, lua$get(reg_5, cns3076));
                  reg_334 = first(call(reg_329, reg_330));
                  reg_336 = lua$get(reg_4, cns3077);
                  reg_337 = newStack();
                  push(reg_337, reg_4);
                  push(reg_337, lua$get(reg_5, cns3078));
                  reg_341 = first(call(reg_336, reg_337));
                  reg_342 = newStack();
                  reg_344 = lua$get(reg_4, cns3079);
                  reg_345 = newStack();
                  push(reg_345, reg_4);
                  push(reg_345, lua$get(reg_1.a, cns3080));
                  push(reg_345, reg_334);
                  push(reg_345, reg_341);
                  append(reg_342, call(reg_344, reg_345));
                  return reg_342;
                }
                if ((goto_507 || false)) {
                  goto_507 = false;
                  goto_543 = !tobool(eq(reg_8, cns3081));
                  if (!goto_543) {
                    reg_353 = newTable();
                    lua$set(reg_353, cns3082, cns3083);
                    reg_356 = cns3084;
                    lua$set(reg_353, reg_356, lua$get(reg_5, cns3085));
                    reg_359 = newStack();
                    reg_361 = lua$get(reg_4, cns3086);
                    reg_362 = newStack();
                    push(reg_362, reg_4);
                    reg_363 = newTable();
                    lua$set(reg_363, cns3087, cns3088);
                    reg_366 = cns3089;
                    lua$set(reg_363, reg_366, lua$get(reg_5, cns3090));
                    lua$set(reg_363, cns3091, reg_353);
                    reg_370 = cns3092;
                    lua$set(reg_363, reg_370, lua$get(reg_5, cns3093));
                    push(reg_362, reg_363);
                    append(reg_359, call(reg_361, reg_362));
                    return reg_359;
                  }
                  if ((goto_543 || false)) {
                    goto_543 = false;
                    goto_557 = !tobool(eq(reg_8, cns3094));
                    if (!goto_557) {
                      reg_377 = newStack();
                      reg_379 = lua$get(reg_4, cns3095);
                      reg_380 = newStack();
                      push(reg_380, reg_4);
                      push(reg_380, reg_5);
                      append(reg_377, call(reg_379, reg_380));
                      return reg_377;
                    }
                    if ((goto_557 || false)) {
                      goto_557 = false;
                      goto_770 = !tobool(eq(reg_8, cns3096));
                      if (!goto_770) {
                        reg_386 = lua$get(reg_4, cns3097);
                        reg_387 = newStack();
                        push(reg_387, reg_4);
                        push(reg_387, lua$get(reg_1.a, cns3098));
                        reg_392 = first(call(reg_386, reg_387));
                        reg_393 = cns3099;
                        reg_396 = lua$get(reg_1.a, cns3100);
                        reg_397 = newStack();
                        push(reg_397, lua$get(reg_5, cns3101));
                        reg_400 = call(reg_396, reg_397);
                        reg_401 = next(reg_400);
                        reg_402 = next(reg_400);
                        reg_403 = next(reg_400);
                        loop_1: while (true) {
                          reg_404 = newStack();
                          push(reg_404, reg_402);
                          push(reg_404, reg_403);
                          reg_405 = call(reg_401, reg_404);
                          reg_406 = next(reg_405);
                          reg_403 = reg_406;
                          reg_407 = next(reg_405);
                          if (tobool(eq(reg_403, nil()))) break loop_1;
                          reg_411 = nil();
                          goto_612 = !tobool(eq(lua$get(reg_407, cns3102), cns3103));
                          if (!goto_612) {
                            reg_418 = lua$get(reg_4, cns3104);
                            reg_419 = newStack();
                            push(reg_419, reg_4);
                            push(reg_419, lua$get(reg_407, cns3105));
                            reg_411 = first(call(reg_418, reg_419));
                          }
                          if ((goto_612 || false)) {
                            goto_612 = false;
                            goto_635 = !tobool(eq(lua$get(reg_407, cns3106), cns3107));
                            if (!goto_635) {
                              reg_430 = lua$get(reg_4, cns3108);
                              reg_431 = newStack();
                              push(reg_431, reg_4);
                              reg_432 = newTable();
                              lua$set(reg_432, cns3109, cns3110);
                              reg_435 = cns3111;
                              lua$set(reg_432, reg_435, lua$get(reg_407, cns3112));
                              push(reg_431, reg_432);
                              reg_411 = first(call(reg_430, reg_431));
                            }
                            if ((goto_635 || false)) {
                              goto_635 = false;
                              if (tobool(eq(lua$get(reg_407, cns3113), cns3114))) {
                                reg_446 = lua$get(reg_4, cns3115);
                                reg_447 = newStack();
                                push(reg_447, reg_4);
                                reg_448 = newTable();
                                lua$set(reg_448, cns3116, cns3117);
                                lua$set(reg_448, cns3118, reg_393);
                                push(reg_447, reg_448);
                                reg_411 = first(call(reg_446, reg_447));
                                reg_393 = add(reg_393, cns3119);
                                if (tobool(eq(reg_406, lua$length(lua$get(reg_5, cns3120))))) {
                                  reg_461 = nil();
                                  goto_684 = !tobool(eq(lua$get(lua$get(reg_407, cns3121), cns3122), cns3123));
                                  if (!goto_684) {
                                    reg_470 = lua$get(reg_4, cns3124);
                                    reg_471 = newStack();
                                    push(reg_471, reg_4);
                                    push(reg_471, lua$get(reg_407, cns3125));
                                    reg_461 = first(call(reg_470, reg_471));
                                  }
                                  if ((goto_684 || false)) {
                                    goto_684 = false;
                                    if (tobool(eq(lua$get(lua$get(reg_407, cns3126), cns3127), cns3128))) {
                                      reg_484 = lua$get(reg_4, cns3129);
                                      reg_485 = newStack();
                                      push(reg_485, reg_4);
                                      reg_486 = newTable();
                                      reg_487 = cns3130;
                                      lua$set(reg_486, reg_487, lua$get(reg_1.a, cns3131));
                                      reg_491 = cns3132;
                                      reg_493 = lua$get(reg_4, cns3133);
                                      reg_494 = newStack();
                                      push(reg_494, reg_4);
                                      table_append(reg_486, reg_491, call(reg_493, reg_494));
                                      push(reg_485, reg_486);
                                      reg_461 = first(call(reg_484, reg_485));
                                    }
                                  }
                                  if (tobool(reg_461)) {
                                    reg_500 = lua$get(reg_4, cns3134);
                                    reg_501 = newStack();
                                    push(reg_501, reg_4);
                                    reg_502 = newTable();
                                    reg_503 = cns3135;
                                    lua$set(reg_502, reg_503, lua$get(reg_1.a, cns3136));
                                    lua$set(reg_502, cns3137, reg_392);
                                    lua$set(reg_502, cns3138, reg_411);
                                    lua$set(reg_502, cns3139, reg_461);
                                    push(reg_501, reg_502);
                                    reg_510 = call(reg_500, reg_501);
                                    if (true) break loop_1;
                                  }
                                }
                              }
                            }
                          }
                          reg_512 = lua$get(reg_4, cns3140);
                          reg_513 = newStack();
                          push(reg_513, reg_4);
                          push(reg_513, lua$get(reg_407, cns3141));
                          reg_517 = first(call(reg_512, reg_513));
                          reg_519 = lua$get(reg_4, cns3142);
                          reg_520 = newStack();
                          push(reg_520, reg_4);
                          reg_521 = newTable();
                          reg_522 = cns3143;
                          lua$set(reg_521, reg_522, lua$get(reg_1.a, cns3144));
                          lua$set(reg_521, cns3145, reg_392);
                          lua$set(reg_521, cns3146, reg_411);
                          lua$set(reg_521, cns3147, reg_517);
                          push(reg_520, reg_521);
                          reg_529 = call(reg_519, reg_520);
                        }
                        reg_530 = newStack();
                        push(reg_530, reg_392);
                        return reg_530;
                      }
                      if ((goto_770 || false)) {
                        goto_770 = false;
                        goto_828 = !tobool(eq(reg_8, cns3148));
                        if (!goto_828) {
                          reg_535 = lua$get(reg_4, cns3149);
                          reg_536 = newStack();
                          push(reg_536, reg_4);
                          push(reg_536, reg_5);
                          push(reg_536, reg_6);
                          reg_538 = first(call(reg_535, reg_536));
                          reg_540 = lua$get(reg_538, cns3150);
                          if (tobool(reg_540)) {
                            reg_540 = gt(lua$length(lua$get(reg_538, cns3151)), cns3152);
                          }
                          if (tobool(reg_540)) {
                            reg_548 = newStack();
                            push(reg_548, lua$get(lua$get(reg_538, cns3153), cns3154));
                            return reg_548;
                          }
                          if (tobool(lua$get(reg_538, cns3155))) {
                            reg_556 = newStack();
                            push(reg_556, reg_538);
                            return reg_556;
                          }
                          reg_557 = newStack();
                          reg_559 = lua$get(reg_4, cns3156);
                          reg_560 = newStack();
                          push(reg_560, reg_4);
                          reg_561 = newTable();
                          reg_562 = cns3157;
                          lua$set(reg_561, reg_562, lua$get(reg_1.a, cns3158));
                          lua$set(reg_561, cns3159, reg_538);
                          push(reg_560, reg_561);
                          append(reg_557, call(reg_559, reg_560));
                          return reg_557;
                        }
                        if ((goto_828 || false)) {
                          goto_828 = false;
                          reg_570 = lua$get(reg_1.a, cns3160);
                          reg_571 = newStack();
                          reg_572 = cns3161;
                          push(reg_571, concat(reg_572, concat(reg_8, cns3162)));
                          push(reg_571, reg_5);
                          reg_576 = call(reg_570, reg_571);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  reg_577 = newStack();
  return reg_577;
}
var cns3163 = anyStr("Function")
var cns3164 = anyStr("assign")
var cns3165 = anyStr("math")
var cns3166 = anyStr("max")
var cns3167 = parseNum("1")
var cns3168 = parseNum("1")
var cns3169 = parseNum("0")
var cns3170 = anyStr("type")
var cns3171 = anyStr("call")
var cns3172 = anyStr("compile_call")
var cns3173 = anyStr("type")
var cns3174 = anyStr("vararg")
var cns3175 = anyStr("inst")
var cns3176 = parseNum("1")
var cns3177 = anyStr("copystack_f")
var cns3178 = parseNum("2")
var cns3179 = anyStr("get_vararg")
var cns3180 = anyStr("regs")
var cns3181 = anyStr("regs")
var cns3182 = parseNum("1")
var cns3183 = anyStr("call")
var cns3184 = anyStr("nil_f")
var cns3185 = anyStr("au_type")
var cns3186 = anyStr("call")
var cns3187 = anyStr("next_f")
var cns3188 = anyStr("repr")
var cns3189 = anyStr("repr")
var cns3190 = anyStr("repr")
var cns3191 = anyStr("lcl")
var cns3192 = anyStr("repr")
var cns3193 = anyStr("lcl")
var cns3194 = anyStr("lcl")
var cns3195 = anyStr("type")
var cns3196 = anyStr("string")
var cns3197 = anyStr("get_local")
var cns3198 = anyStr("au_type")
var cns3199 = anyStr("compileExpr")
var cns3200 = anyStr("call")
var cns3201 = anyStr("nil_f")
var cns3202 = anyStr("lcl")
var cns3203 = anyStr("au_type")
var cns3204 = anyStr("inst")
var cns3205 = parseNum("1")
var cns3206 = anyStr("local")
var cns3207 = parseNum("2")
var cns3208 = anyStr("scope")
var cns3209 = anyStr("locals")
var cns3210 = anyStr("lcl")
var cns3211 = anyStr("base")
var cns3212 = anyStr("au_type")
var cns3213 = anyStr("error")
var cns3214 = anyStr("attempt to assign a typed expression to a table field, at line ")
var cns3215 = anyStr("inst")
var cns3216 = parseNum("1")
var cns3217 = anyStr("set_f")
var cns3218 = parseNum("2")
var cns3219 = anyStr("base")
var cns3220 = parseNum("3")
var cns3221 = anyStr("key")
var cns3222 = parseNum("4")
var cns3223 = anyStr("get_local")
var cns3224 = anyStr("au_type")
var cns3225 = anyStr("au_type")
var cns3226 = anyStr("error")
var cns3227 = anyStr("attempt to assign a lua expression to a typed local, at line ")
var cns3228 = anyStr("type_id")
var cns3229 = anyStr("type_id")
var cns3230 = anyStr("types")
var cns3231 = anyStr("type_id")
var cns3232 = parseNum("1")
var cns3233 = anyStr("name")
var cns3234 = anyStr("types")
var cns3235 = anyStr("type_id")
var cns3236 = parseNum("1")
var cns3237 = anyStr("name")
var cns3238 = anyStr("error")
var cns3239 = anyStr("attempt to assign a ")
var cns3240 = anyStr(" to a ")
var cns3241 = anyStr(" local, at line ")
var cns3242 = anyStr("au_type")
var cns3243 = anyStr("error")
var cns3244 = anyStr("attempt to assign a typed expression to a lua local, at line ")
var cns3245 = anyStr("inst")
var cns3246 = parseNum("1")
var cns3247 = anyStr("set")
var cns3248 = parseNum("2")
var cns3249 = parseNum("3")
var cns3250 = anyStr("line")
var cns3251 = anyStr("au_type")
var cns3252 = anyStr("error")
var cns3253 = anyStr("attempt to assign a typed expression to a lua global, at line ")
var cns3254 = anyStr("get_local")
var cns3255 = anyStr("_ENV")
var cns3256 = anyStr("err")
var cns3257 = anyStr("local \"_ENV\" not in sight")
var cns3258 = anyStr("node")
var cns3259 = anyStr("inst")
var cns3260 = parseNum("1")
var cns3261 = anyStr("var")
var cns3262 = parseNum("2")
var cns3263 = anyStr("compileExpr")
var cns3264 = anyStr("type")
var cns3265 = anyStr("str")
var cns3266 = anyStr("value")
var cns3267 = anyStr("call")
var cns3268 = anyStr("set_f")
function Function_assign (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_7, reg_12, reg_13, reg_17, reg_18, reg_19, reg_20, reg_21, reg_23, reg_29, reg_30, reg_31, reg_33, reg_44, reg_45, reg_55, reg_56, reg_57, reg_58, reg_62, reg_64, reg_65, reg_74, reg_83, reg_84, reg_95, reg_96, reg_106, reg_112, reg_115, reg_122, reg_123, reg_130, reg_131, reg_133, reg_134, reg_141, reg_142, reg_146, reg_147, reg_162, reg_163, reg_164, reg_173, reg_184, reg_185, reg_190, reg_191, reg_192, reg_193, reg_197, reg_200, reg_206, reg_207, reg_209, reg_220, reg_221, reg_226, reg_233, reg_240, reg_243, reg_250, reg_253, reg_254, reg_255, reg_256, reg_269, reg_270, reg_275, reg_276, reg_277, reg_289, reg_290, reg_295, reg_296, reg_299, reg_304, reg_305, reg_312, reg_313, reg_314, reg_319, reg_321, reg_322, reg_323, reg_328, reg_330, reg_331, reg_337;
  var goto_30=false, goto_62=false, goto_121=false, goto_129=false, goto_142=false, goto_153=false, goto_210=false, goto_255=false, goto_295=false, goto_322=false, goto_365=false, goto_395=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_12 = lua$get(lua$get(reg_1.a, cns3165), cns3166);
  reg_13 = newStack();
  push(reg_13, lua$length(reg_5));
  push(reg_13, lua$length(reg_6));
  reg_17 = first(call(reg_12, reg_13));
  reg_18 = nil();
  reg_19 = cns3167;
  reg_20 = reg_17;
  reg_21 = cns3168;
  reg_23 = lt(reg_21, cns3169);
  loop_1: while (true) {
    goto_30 = tobool(reg_23);
    if (!goto_30) {
      if (tobool(gt(reg_19, reg_20))) break loop_1;
    }
    if ((goto_30 || false)) {
      goto_30 = false;
      if (tobool(lt(reg_19, reg_20))) break loop_1;
    }
    reg_29 = lua$get(reg_5, reg_19);
    reg_30 = lua$get(reg_6, reg_19);
    reg_31 = nil();
    reg_33 = eq(reg_19, lua$length(reg_6));
    if (tobool(reg_33)) {
      reg_33 = lt(reg_19, lua$length(reg_5));
    }
    if (tobool(reg_33)) {
      goto_62 = !tobool(eq(lua$get(reg_30, cns3170), cns3171));
      if (!goto_62) {
        reg_44 = lua$get(reg_4, cns3172);
        reg_45 = newStack();
        push(reg_45, reg_4);
        push(reg_45, reg_30);
        push(reg_45, lua$true());
        reg_18 = first(call(reg_44, reg_45));
      }
      if ((goto_62 || false)) {
        goto_62 = false;
        if (tobool(eq(lua$get(reg_30, cns3173), cns3174))) {
          reg_55 = lua$get(reg_4, cns3175);
          reg_56 = newStack();
          push(reg_56, reg_4);
          reg_57 = newTable();
          reg_58 = cns3176;
          lua$set(reg_57, reg_58, lua$get(reg_1.a, cns3177));
          reg_62 = cns3178;
          reg_64 = lua$get(reg_4, cns3179);
          reg_65 = newStack();
          push(reg_65, reg_4);
          table_append(reg_57, reg_62, call(reg_64, reg_65));
          push(reg_56, reg_57);
          reg_18 = first(call(reg_55, reg_56));
        }
      }
    }
    goto_142 = !tobool(reg_18);
    if (!goto_142) {
      goto_121 = !tobool(lua$get(reg_18, cns3180));
      if (!goto_121) {
        reg_74 = lua$get(reg_18, cns3181);
        reg_31 = lua$get(reg_74, sub(add(reg_19, cns3182), lua$length(reg_6)));
        if (tobool(not(reg_31))) {
          reg_83 = lua$get(reg_4, cns3183);
          reg_84 = newStack();
          push(reg_84, reg_4);
          push(reg_84, lua$get(reg_1.a, cns3184));
          reg_31 = first(call(reg_83, reg_84));
        }
      }
      if ((goto_121 || false)) {
        goto_121 = false;
        goto_129 = !tobool(lua$get(reg_18, cns3185));
        if (!goto_129) {
          reg_31 = reg_18;
          reg_18 = nil();
        }
        if ((goto_129 || false)) {
          goto_129 = false;
          reg_95 = lua$get(reg_4, cns3186);
          reg_96 = newStack();
          push(reg_96, reg_4);
          push(reg_96, lua$get(reg_1.a, cns3187));
          push(reg_96, reg_18);
          reg_31 = first(call(reg_95, reg_96));
        }
      }
    }
    if ((goto_142 || false)) {
      goto_142 = false;
      goto_210 = !tobool(reg_30);
      if (!goto_210) {
        goto_153 = !tobool(lua$get(reg_29, cns3188));
        if (!goto_153) {
          reg_106 = cns3189;
          lua$set(reg_30, reg_106, lua$get(reg_29, cns3190));
        }
        if ((goto_153 || false)) {
          goto_153 = false;
          if (tobool(lua$get(reg_29, cns3191))) {
            reg_112 = cns3192;
            lua$set(reg_30, reg_112, lua$get(reg_29, cns3193));
          }
        }
        reg_115 = lua$false();
        if (tobool(lua$get(reg_29, cns3194))) {
          reg_115 = lua$true();
        }
        reg_122 = lua$get(reg_1.a, cns3195);
        reg_123 = newStack();
        push(reg_123, reg_29);
        if (tobool(eq(first(call(reg_122, reg_123)), cns3196))) {
          reg_130 = lua$get(reg_4, cns3197);
          reg_131 = newStack();
          push(reg_131, reg_4);
          push(reg_131, reg_29);
          reg_133 = first(call(reg_130, reg_131));
          reg_134 = reg_133;
          if (tobool(reg_133)) {
            reg_134 = lua$get(reg_133, cns3198);
          }
          if (tobool(reg_134)) {
            reg_115 = lua$true();
          }
        }
        reg_141 = lua$get(reg_4, cns3199);
        reg_142 = newStack();
        push(reg_142, reg_4);
        push(reg_142, reg_30);
        push(reg_142, reg_115);
        reg_31 = first(call(reg_141, reg_142));
      }
      if ((goto_210 || false)) {
        goto_210 = false;
        reg_146 = lua$get(reg_4, cns3200);
        reg_147 = newStack();
        push(reg_147, reg_4);
        push(reg_147, lua$get(reg_1.a, cns3201));
        reg_31 = first(call(reg_146, reg_147));
      }
    }
    if (tobool(reg_29)) {
      goto_255 = !tobool(lua$get(reg_29, cns3202));
      if (!goto_255) {
        if (tobool(not(lua$get(reg_31, cns3203)))) {
          reg_162 = lua$get(reg_4, cns3204);
          reg_163 = newStack();
          push(reg_163, reg_4);
          reg_164 = newTable();
          lua$set(reg_164, cns3205, cns3206);
          lua$set(reg_164, cns3207, reg_31);
          push(reg_163, reg_164);
          reg_31 = first(call(reg_162, reg_163));
        }
        reg_173 = lua$get(lua$get(reg_4, cns3208), cns3209);
        lua$set(reg_173, lua$get(reg_29, cns3210), reg_31);
      }
      if ((goto_255 || false)) {
        goto_255 = false;
        goto_295 = !tobool(lua$get(reg_29, cns3211));
        if (!goto_295) {
          if (tobool(lua$get(reg_31, cns3212))) {
            reg_184 = lua$get(reg_1.a, cns3213);
            reg_185 = newStack();
            push(reg_185, concat(cns3214, reg_7));
            reg_188 = call(reg_184, reg_185);
          }
          reg_190 = lua$get(reg_4, cns3215);
          reg_191 = newStack();
          push(reg_191, reg_4);
          reg_192 = newTable();
          reg_193 = cns3216;
          lua$set(reg_192, reg_193, lua$get(reg_1.a, cns3217));
          reg_197 = cns3218;
          lua$set(reg_192, reg_197, lua$get(reg_29, cns3219));
          reg_200 = cns3220;
          lua$set(reg_192, reg_200, lua$get(reg_29, cns3221));
          lua$set(reg_192, cns3222, reg_31);
          push(reg_191, reg_192);
          reg_204 = call(reg_190, reg_191);
        }
        if ((goto_295 || false)) {
          goto_295 = false;
          reg_206 = lua$get(reg_4, cns3223);
          reg_207 = newStack();
          push(reg_207, reg_4);
          push(reg_207, reg_29);
          reg_209 = first(call(reg_206, reg_207));
          goto_395 = !tobool(reg_209);
          if (!goto_395) {
            goto_365 = !tobool(lua$get(reg_209, cns3224));
            if (!goto_365) {
              goto_322 = !tobool(not(lua$get(reg_31, cns3225)));
              if (!goto_322) {
                reg_220 = lua$get(reg_1.a, cns3226);
                reg_221 = newStack();
                push(reg_221, concat(cns3227, reg_7));
                reg_224 = call(reg_220, reg_221);
              }
              if ((goto_322 || false)) {
                goto_322 = false;
                reg_226 = lua$get(reg_31, cns3228);
                if (tobool(ne(reg_226, lua$get(reg_209, cns3229)))) {
                  reg_233 = lua$get(reg_1.a, cns3230);
                  reg_240 = lua$get(lua$get(reg_233, add(lua$get(reg_31, cns3231), cns3232)), cns3233);
                  reg_243 = lua$get(reg_1.a, cns3234);
                  reg_250 = lua$get(lua$get(reg_243, add(lua$get(reg_209, cns3235), cns3236)), cns3237);
                  reg_253 = lua$get(reg_1.a, cns3238);
                  reg_254 = newStack();
                  reg_255 = cns3239;
                  reg_256 = cns3240;
                  push(reg_254, concat(reg_255, concat(reg_240, concat(reg_256, concat(reg_250, concat(cns3241, reg_7))))));
                  reg_263 = call(reg_253, reg_254);
                }
              }
            }
            if ((goto_365 || false)) {
              goto_365 = false;
              if (tobool(lua$get(reg_31, cns3242))) {
                reg_269 = lua$get(reg_1.a, cns3243);
                reg_270 = newStack();
                push(reg_270, concat(cns3244, reg_7));
                reg_273 = call(reg_269, reg_270);
              }
            }
            reg_275 = lua$get(reg_4, cns3245);
            reg_276 = newStack();
            push(reg_276, reg_4);
            reg_277 = newTable();
            lua$set(reg_277, cns3246, cns3247);
            lua$set(reg_277, cns3248, reg_209);
            lua$set(reg_277, cns3249, reg_31);
            lua$set(reg_277, cns3250, reg_7);
            push(reg_276, reg_277);
            reg_283 = call(reg_275, reg_276);
          }
          if ((goto_395 || false)) {
            goto_395 = false;
            if (tobool(lua$get(reg_31, cns3251))) {
              reg_289 = lua$get(reg_1.a, cns3252);
              reg_290 = newStack();
              push(reg_290, concat(cns3253, reg_7));
              reg_293 = call(reg_289, reg_290);
            }
            reg_295 = lua$get(reg_4, cns3254);
            reg_296 = newStack();
            push(reg_296, reg_4);
            push(reg_296, cns3255);
            reg_299 = first(call(reg_295, reg_296));
            if (tobool(not(reg_299))) {
              reg_304 = lua$get(reg_1.a, cns3256);
              reg_305 = newStack();
              push(reg_305, cns3257);
              push(reg_305, lua$get(reg_1.a, cns3258));
              reg_310 = call(reg_304, reg_305);
            }
            reg_312 = lua$get(reg_4, cns3259);
            reg_313 = newStack();
            push(reg_313, reg_4);
            reg_314 = newTable();
            lua$set(reg_314, cns3260, cns3261);
            lua$set(reg_314, cns3262, reg_299);
            push(reg_313, reg_314);
            reg_319 = first(call(reg_312, reg_313));
            reg_321 = lua$get(reg_4, cns3263);
            reg_322 = newStack();
            push(reg_322, reg_4);
            reg_323 = newTable();
            lua$set(reg_323, cns3264, cns3265);
            lua$set(reg_323, cns3266, reg_29);
            push(reg_322, reg_323);
            reg_328 = first(call(reg_321, reg_322));
            reg_330 = lua$get(reg_4, cns3267);
            reg_331 = newStack();
            push(reg_331, reg_4);
            push(reg_331, lua$get(reg_1.a, cns3268));
            push(reg_331, reg_319);
            push(reg_331, reg_328);
            push(reg_331, reg_31);
            reg_335 = call(reg_330, reg_331);
          }
        }
      }
    }
    reg_19 = add(reg_19, reg_21);
  }
  reg_337 = newStack();
  return reg_337;
}
var cns3269 = anyStr("Function")
var cns3270 = anyStr("compileLhs")
var cns3271 = anyStr("type")
var cns3272 = anyStr("var")
var cns3273 = anyStr("name")
var cns3274 = anyStr("type")
var cns3275 = anyStr("index")
var cns3276 = anyStr("base")
var cns3277 = anyStr("compileExpr")
var cns3278 = anyStr("base")
var cns3279 = anyStr("key")
var cns3280 = anyStr("compileExpr")
var cns3281 = anyStr("key")
var cns3282 = anyStr("repr")
var cns3283 = anyStr("getRepr")
var cns3284 = anyStr("type")
var cns3285 = anyStr("field")
var cns3286 = anyStr("base")
var cns3287 = anyStr("compileExpr")
var cns3288 = anyStr("base")
var cns3289 = anyStr("key")
var cns3290 = anyStr("compileExpr")
var cns3291 = anyStr("type")
var cns3292 = anyStr("str")
var cns3293 = anyStr("value")
var cns3294 = anyStr("key")
var cns3295 = anyStr("repr")
var cns3296 = anyStr("getRepr")
var cns3297 = anyStr("error")
var cns3298 = anyStr("wtf")
function Function_compileLhs (reg_0, reg_1) {
  var reg_4, reg_5, reg_11, reg_19, reg_20, reg_21, reg_23, reg_24, reg_29, reg_31, reg_32, reg_37, reg_40, reg_41, reg_49, reg_50, reg_51, reg_53, reg_54, reg_59, reg_61, reg_62, reg_63, reg_66, reg_71, reg_74, reg_75, reg_80, reg_81, reg_84;
  var goto_16=false, goto_58=false, goto_106=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  goto_16 = !tobool(eq(lua$get(reg_5, cns3271), cns3272));
  if (!goto_16) {
    reg_11 = newStack();
    push(reg_11, lua$get(reg_5, cns3273));
    return reg_11;
  }
  if ((goto_16 || false)) {
    goto_16 = false;
    goto_58 = !tobool(eq(lua$get(reg_5, cns3274), cns3275));
    if (!goto_58) {
      reg_19 = newStack();
      reg_20 = newTable();
      reg_21 = cns3276;
      reg_23 = lua$get(reg_4, cns3277);
      reg_24 = newStack();
      push(reg_24, reg_4);
      push(reg_24, lua$get(reg_5, cns3278));
      lua$set(reg_20, reg_21, first(call(reg_23, reg_24)));
      reg_29 = cns3279;
      reg_31 = lua$get(reg_4, cns3280);
      reg_32 = newStack();
      push(reg_32, reg_4);
      push(reg_32, lua$get(reg_5, cns3281));
      lua$set(reg_20, reg_29, first(call(reg_31, reg_32)));
      reg_37 = cns3282;
      reg_40 = lua$get(reg_1.a, cns3283);
      reg_41 = newStack();
      push(reg_41, reg_5);
      lua$set(reg_20, reg_37, first(call(reg_40, reg_41)));
      push(reg_19, reg_20);
      return reg_19;
    }
    if ((goto_58 || false)) {
      goto_58 = false;
      goto_106 = !tobool(eq(lua$get(reg_5, cns3284), cns3285));
      if (!goto_106) {
        reg_49 = newStack();
        reg_50 = newTable();
        reg_51 = cns3286;
        reg_53 = lua$get(reg_4, cns3287);
        reg_54 = newStack();
        push(reg_54, reg_4);
        push(reg_54, lua$get(reg_5, cns3288));
        lua$set(reg_50, reg_51, first(call(reg_53, reg_54)));
        reg_59 = cns3289;
        reg_61 = lua$get(reg_4, cns3290);
        reg_62 = newStack();
        push(reg_62, reg_4);
        reg_63 = newTable();
        lua$set(reg_63, cns3291, cns3292);
        reg_66 = cns3293;
        lua$set(reg_63, reg_66, lua$get(reg_5, cns3294));
        push(reg_62, reg_63);
        lua$set(reg_50, reg_59, first(call(reg_61, reg_62)));
        reg_71 = cns3295;
        reg_74 = lua$get(reg_1.a, cns3296);
        reg_75 = newStack();
        push(reg_75, reg_5);
        lua$set(reg_50, reg_71, first(call(reg_74, reg_75)));
        push(reg_49, reg_50);
        return reg_49;
      }
      if ((goto_106 || false)) {
        goto_106 = false;
        reg_80 = lua$get(reg_1.a, cns3297);
        reg_81 = newStack();
        push(reg_81, cns3298);
        reg_83 = call(reg_80, reg_81);
      }
    }
  }
  reg_84 = newStack();
  return reg_84;
}
var cns3299 = anyStr("type")
var cns3300 = anyStr("var")
var cns3301 = anyStr("name")
var cns3302 = anyStr("type")
var cns3303 = anyStr("field")
var cns3304 = anyStr("getRepr")
var cns3305 = anyStr("base")
var cns3306 = anyStr(".")
var cns3307 = anyStr("key")
var cns3308 = anyStr("type")
var cns3309 = anyStr("index")
var cns3310 = anyStr("key")
var cns3311 = anyStr("type")
var cns3312 = anyStr("str")
var cns3313 = anyStr("getRepr")
var cns3314 = anyStr("base")
var cns3315 = anyStr(".")
var cns3316 = anyStr("key")
var cns3317 = anyStr("value")
var cns3318 = anyStr("getRepr")
var cns3319 = anyStr("key")
var cns3320 = anyStr("getRepr")
var cns3321 = anyStr("base")
var cns3322 = anyStr("[")
var cns3323 = anyStr("]")
var cns3324 = anyStr("type")
var cns3325 = anyStr("num")
var cns3326 = anyStr("type")
var cns3327 = anyStr("const")
var cns3328 = anyStr("value")
var cns3329 = anyStr("type")
var cns3330 = anyStr("string")
var cns3331 = anyStr("\"")
var cns3332 = anyStr("value")
var cns3333 = anyStr("\"")
function aulua_codegen$function (reg_0, reg_1) {
  var reg_4, reg_10, reg_18, reg_21, reg_22, reg_26, reg_27, reg_44, reg_47, reg_48, reg_52, reg_53, reg_62, reg_63, reg_67, reg_69, reg_72, reg_73, reg_77, reg_78, reg_86, reg_93, reg_101, reg_102, reg_108;
  var goto_15=false, goto_39=false, goto_73=false, goto_103=false, goto_122=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  goto_15 = !tobool(eq(lua$get(reg_4, cns3299), cns3300));
  if (!goto_15) {
    reg_10 = newStack();
    push(reg_10, lua$get(reg_4, cns3301));
    return reg_10;
  }
  if ((goto_15 || false)) {
    goto_15 = false;
    goto_39 = !tobool(eq(lua$get(reg_4, cns3302), cns3303));
    if (!goto_39) {
      reg_18 = newStack();
      reg_21 = lua$get(reg_1.a, cns3304);
      reg_22 = newStack();
      push(reg_22, lua$get(reg_4, cns3305));
      reg_26 = first(call(reg_21, reg_22));
      reg_27 = cns3306;
      push(reg_18, concat(reg_26, concat(reg_27, lua$get(reg_4, cns3307))));
      return reg_18;
    }
    if ((goto_39 || false)) {
      goto_39 = false;
      goto_103 = !tobool(eq(lua$get(reg_4, cns3308), cns3309));
      if (!goto_103) {
        goto_73 = !tobool(eq(lua$get(lua$get(reg_4, cns3310), cns3311), cns3312));
        if (!goto_73) {
          reg_44 = newStack();
          reg_47 = lua$get(reg_1.a, cns3313);
          reg_48 = newStack();
          push(reg_48, lua$get(reg_4, cns3314));
          reg_52 = first(call(reg_47, reg_48));
          reg_53 = cns3315;
          push(reg_44, concat(reg_52, concat(reg_53, lua$get(lua$get(reg_4, cns3316), cns3317))));
          return reg_44;
        }
        if ((goto_73 || false)) {
          goto_73 = false;
          reg_62 = lua$get(reg_1.a, cns3318);
          reg_63 = newStack();
          push(reg_63, lua$get(reg_4, cns3319));
          reg_67 = first(call(reg_62, reg_63));
          if (tobool(reg_67)) {
            reg_69 = newStack();
            reg_72 = lua$get(reg_1.a, cns3320);
            reg_73 = newStack();
            push(reg_73, lua$get(reg_4, cns3321));
            reg_77 = first(call(reg_72, reg_73));
            reg_78 = cns3322;
            push(reg_69, concat(reg_77, concat(reg_78, concat(reg_67, cns3323))));
            return reg_69;
          }
        }
      }
      if ((goto_103 || false)) {
        goto_103 = false;
        reg_86 = eq(lua$get(reg_4, cns3324), cns3325);
        if (!tobool(reg_86)) {
          reg_86 = eq(lua$get(reg_4, cns3326), cns3327);
        }
        goto_122 = !tobool(reg_86);
        if (!goto_122) {
          reg_93 = newStack();
          push(reg_93, lua$get(reg_4, cns3328));
          return reg_93;
        }
        if ((goto_122 || false)) {
          goto_122 = false;
          if (tobool(eq(lua$get(reg_4, cns3329), cns3330))) {
            reg_101 = newStack();
            reg_102 = cns3331;
            push(reg_101, concat(reg_102, concat(lua$get(reg_4, cns3332), cns3333)));
            return reg_101;
          }
        }
      }
    }
  }
  reg_108 = newStack();
  return reg_108;
}
var cns3334 = anyStr("getRepr")
var cns3335 = anyStr("Function")
var cns3336 = anyStr("compile_numfor")
var cns3337 = anyStr("lbl")
var cns3338 = anyStr("lbl")
var cns3339 = anyStr("lbl")
var cns3340 = anyStr("table")
var cns3341 = anyStr("insert")
var cns3342 = anyStr("loops")
var cns3343 = anyStr("push_scope")
var cns3344 = anyStr("inst")
var cns3345 = parseNum("1")
var cns3346 = anyStr("local")
var cns3347 = parseNum("2")
var cns3348 = anyStr("compileExpr")
var cns3349 = anyStr("init")
var cns3350 = anyStr("inst")
var cns3351 = parseNum("1")
var cns3352 = anyStr("local")
var cns3353 = parseNum("2")
var cns3354 = anyStr("compileExpr")
var cns3355 = anyStr("limit")
var cns3356 = anyStr("step")
var cns3357 = anyStr("compileExpr")
var cns3358 = anyStr("step")
var cns3359 = anyStr("compileExpr")
var cns3360 = anyStr("type")
var cns3361 = anyStr("num")
var cns3362 = anyStr("value")
var cns3363 = anyStr("1")
var cns3364 = anyStr("inst")
var cns3365 = parseNum("1")
var cns3366 = anyStr("local")
var cns3367 = parseNum("2")
var cns3368 = anyStr("scope")
var cns3369 = anyStr("locals")
var cns3370 = anyStr("name")
var cns3371 = anyStr("inst")
var cns3372 = parseNum("1")
var cns3373 = anyStr("binops")
var cns3374 = anyStr("<")
var cns3375 = parseNum("2")
var cns3376 = parseNum("3")
var cns3377 = anyStr("compileExpr")
var cns3378 = anyStr("type")
var cns3379 = anyStr("num")
var cns3380 = anyStr("value")
var cns3381 = anyStr("0")
var cns3382 = anyStr("inst")
var cns3383 = parseNum("1")
var cns3384 = anyStr("label")
var cns3385 = parseNum("2")
var cns3386 = anyStr("lbl")
var cns3387 = anyStr("inst")
var cns3388 = parseNum("1")
var cns3389 = anyStr("jif")
var cns3390 = parseNum("2")
var cns3391 = parseNum("3")
var cns3392 = anyStr("inst")
var cns3393 = parseNum("1")
var cns3394 = anyStr("binops")
var cns3395 = anyStr(">")
var cns3396 = parseNum("2")
var cns3397 = parseNum("3")
var cns3398 = anyStr("inst")
var cns3399 = parseNum("1")
var cns3400 = anyStr("jif")
var cns3401 = parseNum("2")
var cns3402 = parseNum("3")
var cns3403 = anyStr("inst")
var cns3404 = parseNum("1")
var cns3405 = anyStr("jmp")
var cns3406 = parseNum("2")
var cns3407 = anyStr("inst")
var cns3408 = parseNum("1")
var cns3409 = anyStr("label")
var cns3410 = parseNum("2")
var cns3411 = anyStr("inst")
var cns3412 = parseNum("1")
var cns3413 = anyStr("binops")
var cns3414 = anyStr("<")
var cns3415 = parseNum("2")
var cns3416 = parseNum("3")
var cns3417 = anyStr("inst")
var cns3418 = parseNum("1")
var cns3419 = anyStr("jif")
var cns3420 = parseNum("2")
var cns3421 = parseNum("3")
var cns3422 = anyStr("inst")
var cns3423 = parseNum("1")
var cns3424 = anyStr("label")
var cns3425 = parseNum("2")
var cns3426 = anyStr("compileBlock")
var cns3427 = anyStr("body")
var cns3428 = anyStr("inst")
var cns3429 = parseNum("1")
var cns3430 = anyStr("binops")
var cns3431 = anyStr("+")
var cns3432 = parseNum("2")
var cns3433 = parseNum("3")
var cns3434 = anyStr("inst")
var cns3435 = parseNum("1")
var cns3436 = anyStr("set")
var cns3437 = parseNum("2")
var cns3438 = parseNum("3")
var cns3439 = anyStr("inst")
var cns3440 = parseNum("1")
var cns3441 = anyStr("jmp")
var cns3442 = parseNum("2")
var cns3443 = anyStr("inst")
var cns3444 = parseNum("1")
var cns3445 = anyStr("label")
var cns3446 = parseNum("2")
var cns3447 = anyStr("pop_scope")
var cns3448 = anyStr("table")
var cns3449 = anyStr("remove")
var cns3450 = anyStr("loops")
function Function_compile_numfor (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_10, reg_12, reg_13, reg_15, reg_17, reg_18, reg_20, reg_25, reg_26, reg_31, reg_32, reg_35, reg_36, reg_37, reg_40, reg_42, reg_43, reg_48, reg_50, reg_51, reg_52, reg_55, reg_57, reg_58, reg_63, reg_64, reg_69, reg_70, reg_76, reg_77, reg_78, reg_86, reg_87, reg_88, reg_93, reg_97, reg_101, reg_102, reg_103, reg_104, reg_111, reg_113, reg_114, reg_115, reg_122, reg_124, reg_125, reg_126, reg_132, reg_133, reg_135, reg_137, reg_138, reg_139, reg_146, reg_147, reg_148, reg_149, reg_158, reg_160, reg_161, reg_162, reg_169, reg_170, reg_171, reg_177, reg_178, reg_179, reg_185, reg_186, reg_187, reg_188, reg_199, reg_200, reg_201, reg_208, reg_209, reg_210, reg_216, reg_217, reg_222, reg_223, reg_224, reg_225, reg_234, reg_236, reg_237, reg_238, reg_245, reg_246, reg_247, reg_253, reg_254, reg_255, reg_261, reg_262, reg_268, reg_269, reg_273;
  var goto_96=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = lua$get(reg_4, cns3337);
  reg_8 = newStack();
  push(reg_8, reg_4);
  reg_10 = first(call(reg_7, reg_8));
  reg_12 = lua$get(reg_4, cns3338);
  reg_13 = newStack();
  push(reg_13, reg_4);
  reg_15 = first(call(reg_12, reg_13));
  reg_17 = lua$get(reg_4, cns3339);
  reg_18 = newStack();
  push(reg_18, reg_4);
  reg_20 = first(call(reg_17, reg_18));
  reg_25 = lua$get(lua$get(reg_1.a, cns3340), cns3341);
  reg_26 = newStack();
  push(reg_26, lua$get(reg_4, cns3342));
  push(reg_26, reg_15);
  reg_29 = call(reg_25, reg_26);
  reg_31 = lua$get(reg_4, cns3343);
  reg_32 = newStack();
  push(reg_32, reg_4);
  reg_33 = call(reg_31, reg_32);
  reg_35 = lua$get(reg_4, cns3344);
  reg_36 = newStack();
  push(reg_36, reg_4);
  reg_37 = newTable();
  lua$set(reg_37, cns3345, cns3346);
  reg_40 = cns3347;
  reg_42 = lua$get(reg_4, cns3348);
  reg_43 = newStack();
  push(reg_43, reg_4);
  push(reg_43, lua$get(reg_5, cns3349));
  table_append(reg_37, reg_40, call(reg_42, reg_43));
  push(reg_36, reg_37);
  reg_48 = first(call(reg_35, reg_36));
  reg_50 = lua$get(reg_4, cns3350);
  reg_51 = newStack();
  push(reg_51, reg_4);
  reg_52 = newTable();
  lua$set(reg_52, cns3351, cns3352);
  reg_55 = cns3353;
  reg_57 = lua$get(reg_4, cns3354);
  reg_58 = newStack();
  push(reg_58, reg_4);
  push(reg_58, lua$get(reg_5, cns3355));
  table_append(reg_52, reg_55, call(reg_57, reg_58));
  push(reg_51, reg_52);
  reg_63 = first(call(reg_50, reg_51));
  reg_64 = nil();
  goto_96 = !tobool(lua$get(reg_5, cns3356));
  if (!goto_96) {
    reg_69 = lua$get(reg_4, cns3357);
    reg_70 = newStack();
    push(reg_70, reg_4);
    push(reg_70, lua$get(reg_5, cns3358));
    reg_64 = first(call(reg_69, reg_70));
  }
  if ((goto_96 || false)) {
    goto_96 = false;
    reg_76 = lua$get(reg_4, cns3359);
    reg_77 = newStack();
    push(reg_77, reg_4);
    reg_78 = newTable();
    lua$set(reg_78, cns3360, cns3361);
    lua$set(reg_78, cns3362, cns3363);
    push(reg_77, reg_78);
    reg_64 = first(call(reg_76, reg_77));
  }
  reg_86 = lua$get(reg_4, cns3364);
  reg_87 = newStack();
  push(reg_87, reg_4);
  reg_88 = newTable();
  lua$set(reg_88, cns3365, cns3366);
  lua$set(reg_88, cns3367, reg_64);
  push(reg_87, reg_88);
  reg_93 = first(call(reg_86, reg_87));
  reg_97 = lua$get(lua$get(reg_4, cns3368), cns3369);
  lua$set(reg_97, lua$get(reg_5, cns3370), reg_48);
  reg_101 = lua$get(reg_4, cns3371);
  reg_102 = newStack();
  push(reg_102, reg_4);
  reg_103 = newTable();
  reg_104 = cns3372;
  lua$set(reg_103, reg_104, lua$get(lua$get(reg_1.a, cns3373), cns3374));
  lua$set(reg_103, cns3375, reg_93);
  reg_111 = cns3376;
  reg_113 = lua$get(reg_4, cns3377);
  reg_114 = newStack();
  push(reg_114, reg_4);
  reg_115 = newTable();
  lua$set(reg_115, cns3378, cns3379);
  lua$set(reg_115, cns3380, cns3381);
  push(reg_114, reg_115);
  table_append(reg_103, reg_111, call(reg_113, reg_114));
  push(reg_102, reg_103);
  reg_122 = first(call(reg_101, reg_102));
  reg_124 = lua$get(reg_4, cns3382);
  reg_125 = newStack();
  push(reg_125, reg_4);
  reg_126 = newTable();
  lua$set(reg_126, cns3383, cns3384);
  lua$set(reg_126, cns3385, reg_10);
  push(reg_125, reg_126);
  reg_130 = call(reg_124, reg_125);
  reg_132 = lua$get(reg_4, cns3386);
  reg_133 = newStack();
  push(reg_133, reg_4);
  reg_135 = first(call(reg_132, reg_133));
  reg_137 = lua$get(reg_4, cns3387);
  reg_138 = newStack();
  push(reg_138, reg_4);
  reg_139 = newTable();
  lua$set(reg_139, cns3388, cns3389);
  lua$set(reg_139, cns3390, reg_135);
  lua$set(reg_139, cns3391, reg_122);
  push(reg_138, reg_139);
  reg_144 = call(reg_137, reg_138);
  reg_146 = lua$get(reg_4, cns3392);
  reg_147 = newStack();
  push(reg_147, reg_4);
  reg_148 = newTable();
  reg_149 = cns3393;
  lua$set(reg_148, reg_149, lua$get(lua$get(reg_1.a, cns3394), cns3395));
  lua$set(reg_148, cns3396, reg_48);
  lua$set(reg_148, cns3397, reg_63);
  push(reg_147, reg_148);
  reg_158 = first(call(reg_146, reg_147));
  reg_160 = lua$get(reg_4, cns3398);
  reg_161 = newStack();
  push(reg_161, reg_4);
  reg_162 = newTable();
  lua$set(reg_162, cns3399, cns3400);
  lua$set(reg_162, cns3401, reg_15);
  lua$set(reg_162, cns3402, reg_158);
  push(reg_161, reg_162);
  reg_167 = call(reg_160, reg_161);
  reg_169 = lua$get(reg_4, cns3403);
  reg_170 = newStack();
  push(reg_170, reg_4);
  reg_171 = newTable();
  lua$set(reg_171, cns3404, cns3405);
  lua$set(reg_171, cns3406, reg_20);
  push(reg_170, reg_171);
  reg_175 = call(reg_169, reg_170);
  reg_177 = lua$get(reg_4, cns3407);
  reg_178 = newStack();
  push(reg_178, reg_4);
  reg_179 = newTable();
  lua$set(reg_179, cns3408, cns3409);
  lua$set(reg_179, cns3410, reg_135);
  push(reg_178, reg_179);
  reg_183 = call(reg_177, reg_178);
  reg_185 = lua$get(reg_4, cns3411);
  reg_186 = newStack();
  push(reg_186, reg_4);
  reg_187 = newTable();
  reg_188 = cns3412;
  lua$set(reg_187, reg_188, lua$get(lua$get(reg_1.a, cns3413), cns3414));
  lua$set(reg_187, cns3415, reg_48);
  lua$set(reg_187, cns3416, reg_63);
  push(reg_186, reg_187);
  reg_158 = first(call(reg_185, reg_186));
  reg_199 = lua$get(reg_4, cns3417);
  reg_200 = newStack();
  push(reg_200, reg_4);
  reg_201 = newTable();
  lua$set(reg_201, cns3418, cns3419);
  lua$set(reg_201, cns3420, reg_15);
  lua$set(reg_201, cns3421, reg_158);
  push(reg_200, reg_201);
  reg_206 = call(reg_199, reg_200);
  reg_208 = lua$get(reg_4, cns3422);
  reg_209 = newStack();
  push(reg_209, reg_4);
  reg_210 = newTable();
  lua$set(reg_210, cns3423, cns3424);
  lua$set(reg_210, cns3425, reg_20);
  push(reg_209, reg_210);
  reg_214 = call(reg_208, reg_209);
  reg_216 = lua$get(reg_4, cns3426);
  reg_217 = newStack();
  push(reg_217, reg_4);
  push(reg_217, lua$get(reg_5, cns3427));
  reg_220 = call(reg_216, reg_217);
  reg_222 = lua$get(reg_4, cns3428);
  reg_223 = newStack();
  push(reg_223, reg_4);
  reg_224 = newTable();
  reg_225 = cns3429;
  lua$set(reg_224, reg_225, lua$get(lua$get(reg_1.a, cns3430), cns3431));
  lua$set(reg_224, cns3432, reg_48);
  lua$set(reg_224, cns3433, reg_93);
  push(reg_223, reg_224);
  reg_234 = first(call(reg_222, reg_223));
  reg_236 = lua$get(reg_4, cns3434);
  reg_237 = newStack();
  push(reg_237, reg_4);
  reg_238 = newTable();
  lua$set(reg_238, cns3435, cns3436);
  lua$set(reg_238, cns3437, reg_48);
  lua$set(reg_238, cns3438, reg_234);
  push(reg_237, reg_238);
  reg_243 = call(reg_236, reg_237);
  reg_245 = lua$get(reg_4, cns3439);
  reg_246 = newStack();
  push(reg_246, reg_4);
  reg_247 = newTable();
  lua$set(reg_247, cns3440, cns3441);
  lua$set(reg_247, cns3442, reg_10);
  push(reg_246, reg_247);
  reg_251 = call(reg_245, reg_246);
  reg_253 = lua$get(reg_4, cns3443);
  reg_254 = newStack();
  push(reg_254, reg_4);
  reg_255 = newTable();
  lua$set(reg_255, cns3444, cns3445);
  lua$set(reg_255, cns3446, reg_15);
  push(reg_254, reg_255);
  reg_259 = call(reg_253, reg_254);
  reg_261 = lua$get(reg_4, cns3447);
  reg_262 = newStack();
  push(reg_262, reg_4);
  reg_263 = call(reg_261, reg_262);
  reg_268 = lua$get(lua$get(reg_1.a, cns3448), cns3449);
  reg_269 = newStack();
  push(reg_269, lua$get(reg_4, cns3450));
  reg_272 = call(reg_268, reg_269);
  reg_273 = newStack();
  return reg_273;
}
var cns3451 = anyStr("Function")
var cns3452 = anyStr("compile_genfor")
var cns3453 = anyStr("lbl")
var cns3454 = anyStr("lbl")
var cns3455 = anyStr("table")
var cns3456 = anyStr("insert")
var cns3457 = anyStr("loops")
var cns3458 = anyStr("push_scope")
var cns3459 = anyStr("assign")
var cns3460 = parseNum("1")
var cns3461 = anyStr("lcl")
var cns3462 = anyStr(".f")
var cns3463 = parseNum("2")
var cns3464 = anyStr("lcl")
var cns3465 = anyStr(".s")
var cns3466 = parseNum("3")
var cns3467 = anyStr("lcl")
var cns3468 = anyStr(".var")
var cns3469 = anyStr("values")
var cns3470 = anyStr("scope")
var cns3471 = anyStr("locals")
var cns3472 = anyStr(".f")
var cns3473 = anyStr("scope")
var cns3474 = anyStr("locals")
var cns3475 = anyStr(".s")
var cns3476 = anyStr("scope")
var cns3477 = anyStr("locals")
var cns3478 = anyStr(".var")
var cns3479 = anyStr("inst")
var cns3480 = parseNum("1")
var cns3481 = anyStr("label")
var cns3482 = parseNum("2")
var cns3483 = anyStr("inst")
var cns3484 = parseNum("1")
var cns3485 = anyStr("stack_f")
var cns3486 = anyStr("inst")
var cns3487 = parseNum("1")
var cns3488 = anyStr("push_f")
var cns3489 = parseNum("2")
var cns3490 = parseNum("3")
var cns3491 = anyStr("inst")
var cns3492 = parseNum("1")
var cns3493 = anyStr("push_f")
var cns3494 = parseNum("2")
var cns3495 = parseNum("3")
var cns3496 = anyStr("inst")
var cns3497 = parseNum("1")
var cns3498 = anyStr("call_f")
var cns3499 = parseNum("2")
var cns3500 = parseNum("3")
var cns3501 = anyStr("ipairs")
var cns3502 = anyStr("names")
var cns3503 = anyStr("inst")
var cns3504 = parseNum("1")
var cns3505 = anyStr("next_f")
var cns3506 = parseNum("2")
var cns3507 = anyStr("scope")
var cns3508 = anyStr("locals")
var cns3509 = anyStr("inst")
var cns3510 = parseNum("1")
var cns3511 = anyStr("local")
var cns3512 = parseNum("2")
var cns3513 = parseNum("1")
var cns3514 = anyStr("inst")
var cns3515 = parseNum("1")
var cns3516 = anyStr("set")
var cns3517 = parseNum("2")
var cns3518 = parseNum("3")
var cns3519 = anyStr("inst")
var cns3520 = parseNum("1")
var cns3521 = anyStr("binops")
var cns3522 = anyStr("==")
var cns3523 = parseNum("2")
var cns3524 = parseNum("3")
var cns3525 = anyStr("inst")
var cns3526 = parseNum("1")
var cns3527 = anyStr("nil_f")
var cns3528 = anyStr("inst")
var cns3529 = parseNum("1")
var cns3530 = anyStr("jif")
var cns3531 = parseNum("2")
var cns3532 = parseNum("3")
var cns3533 = anyStr("compileBlock")
var cns3534 = anyStr("body")
var cns3535 = anyStr("inst")
var cns3536 = parseNum("1")
var cns3537 = anyStr("jmp")
var cns3538 = parseNum("2")
var cns3539 = anyStr("inst")
var cns3540 = parseNum("1")
var cns3541 = anyStr("label")
var cns3542 = parseNum("2")
var cns3543 = anyStr("pop_scope")
var cns3544 = anyStr("table")
var cns3545 = anyStr("remove")
var cns3546 = anyStr("loops")
function Function_compile_genfor (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_8, reg_10, reg_12, reg_13, reg_15, reg_20, reg_21, reg_26, reg_27, reg_30, reg_31, reg_32, reg_33, reg_34, reg_37, reg_38, reg_41, reg_42, reg_53, reg_59, reg_65, reg_67, reg_68, reg_69, reg_75, reg_76, reg_77, reg_78, reg_83, reg_85, reg_86, reg_87, reg_88, reg_96, reg_97, reg_98, reg_99, reg_107, reg_108, reg_109, reg_110, reg_117, reg_120, reg_121, reg_124, reg_125, reg_126, reg_127, reg_128, reg_129, reg_130, reg_131, reg_136, reg_137, reg_138, reg_139, reg_145, reg_149, reg_151, reg_152, reg_153, reg_163, reg_164, reg_165, reg_172, reg_173, reg_174, reg_175, reg_182, reg_184, reg_185, reg_186, reg_187, reg_193, reg_195, reg_196, reg_197, reg_204, reg_205, reg_210, reg_211, reg_212, reg_218, reg_219, reg_220, reg_226, reg_227, reg_233, reg_234, reg_238;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = lua$get(reg_4, cns3453);
  reg_8 = newStack();
  push(reg_8, reg_4);
  reg_10 = first(call(reg_7, reg_8));
  reg_12 = lua$get(reg_4, cns3454);
  reg_13 = newStack();
  push(reg_13, reg_4);
  reg_15 = first(call(reg_12, reg_13));
  reg_20 = lua$get(lua$get(reg_1.a, cns3455), cns3456);
  reg_21 = newStack();
  push(reg_21, lua$get(reg_4, cns3457));
  push(reg_21, reg_15);
  reg_24 = call(reg_20, reg_21);
  reg_26 = lua$get(reg_4, cns3458);
  reg_27 = newStack();
  push(reg_27, reg_4);
  reg_28 = call(reg_26, reg_27);
  reg_30 = lua$get(reg_4, cns3459);
  reg_31 = newStack();
  push(reg_31, reg_4);
  reg_32 = newTable();
  reg_33 = cns3460;
  reg_34 = newTable();
  lua$set(reg_34, cns3461, cns3462);
  lua$set(reg_32, reg_33, reg_34);
  reg_37 = cns3463;
  reg_38 = newTable();
  lua$set(reg_38, cns3464, cns3465);
  lua$set(reg_32, reg_37, reg_38);
  reg_41 = cns3466;
  reg_42 = newTable();
  lua$set(reg_42, cns3467, cns3468);
  lua$set(reg_32, reg_41, reg_42);
  push(reg_31, reg_32);
  push(reg_31, lua$get(reg_5, cns3469));
  reg_47 = call(reg_30, reg_31);
  reg_53 = lua$get(lua$get(lua$get(reg_4, cns3470), cns3471), cns3472);
  reg_59 = lua$get(lua$get(lua$get(reg_4, cns3473), cns3474), cns3475);
  reg_65 = lua$get(lua$get(lua$get(reg_4, cns3476), cns3477), cns3478);
  reg_67 = lua$get(reg_4, cns3479);
  reg_68 = newStack();
  push(reg_68, reg_4);
  reg_69 = newTable();
  lua$set(reg_69, cns3480, cns3481);
  lua$set(reg_69, cns3482, reg_10);
  push(reg_68, reg_69);
  reg_73 = call(reg_67, reg_68);
  reg_75 = lua$get(reg_4, cns3483);
  reg_76 = newStack();
  push(reg_76, reg_4);
  reg_77 = newTable();
  reg_78 = cns3484;
  lua$set(reg_77, reg_78, lua$get(reg_1.a, cns3485));
  push(reg_76, reg_77);
  reg_83 = first(call(reg_75, reg_76));
  reg_85 = lua$get(reg_4, cns3486);
  reg_86 = newStack();
  push(reg_86, reg_4);
  reg_87 = newTable();
  reg_88 = cns3487;
  lua$set(reg_87, reg_88, lua$get(reg_1.a, cns3488));
  lua$set(reg_87, cns3489, reg_83);
  lua$set(reg_87, cns3490, reg_59);
  push(reg_86, reg_87);
  reg_94 = call(reg_85, reg_86);
  reg_96 = lua$get(reg_4, cns3491);
  reg_97 = newStack();
  push(reg_97, reg_4);
  reg_98 = newTable();
  reg_99 = cns3492;
  lua$set(reg_98, reg_99, lua$get(reg_1.a, cns3493));
  lua$set(reg_98, cns3494, reg_83);
  lua$set(reg_98, cns3495, reg_65);
  push(reg_97, reg_98);
  reg_105 = call(reg_96, reg_97);
  reg_107 = lua$get(reg_4, cns3496);
  reg_108 = newStack();
  push(reg_108, reg_4);
  reg_109 = newTable();
  reg_110 = cns3497;
  lua$set(reg_109, reg_110, lua$get(reg_1.a, cns3498));
  lua$set(reg_109, cns3499, reg_53);
  lua$set(reg_109, cns3500, reg_83);
  push(reg_108, reg_109);
  reg_117 = first(call(reg_107, reg_108));
  reg_120 = lua$get(reg_1.a, cns3501);
  reg_121 = newStack();
  push(reg_121, lua$get(reg_5, cns3502));
  reg_124 = call(reg_120, reg_121);
  reg_125 = next(reg_124);
  reg_126 = next(reg_124);
  reg_127 = next(reg_124);
  loop_1: while (true) {
    reg_128 = newStack();
    push(reg_128, reg_126);
    push(reg_128, reg_127);
    reg_129 = call(reg_125, reg_128);
    reg_130 = next(reg_129);
    reg_127 = reg_130;
    reg_131 = next(reg_129);
    if (tobool(eq(reg_127, nil()))) break loop_1;
    reg_136 = lua$get(reg_4, cns3503);
    reg_137 = newStack();
    push(reg_137, reg_4);
    reg_138 = newTable();
    reg_139 = cns3504;
    lua$set(reg_138, reg_139, lua$get(reg_1.a, cns3505));
    lua$set(reg_138, cns3506, reg_117);
    push(reg_137, reg_138);
    reg_145 = first(call(reg_136, reg_137));
    reg_149 = lua$get(lua$get(reg_4, cns3507), cns3508);
    reg_151 = lua$get(reg_4, cns3509);
    reg_152 = newStack();
    push(reg_152, reg_4);
    reg_153 = newTable();
    lua$set(reg_153, cns3510, cns3511);
    lua$set(reg_153, cns3512, reg_145);
    push(reg_152, reg_153);
    lua$set(reg_149, reg_131, first(call(reg_151, reg_152)));
    if (tobool(eq(reg_130, cns3513))) {
      reg_163 = lua$get(reg_4, cns3514);
      reg_164 = newStack();
      push(reg_164, reg_4);
      reg_165 = newTable();
      lua$set(reg_165, cns3515, cns3516);
      lua$set(reg_165, cns3517, reg_65);
      lua$set(reg_165, cns3518, reg_145);
      push(reg_164, reg_165);
      reg_170 = call(reg_163, reg_164);
    }
  }
  reg_172 = lua$get(reg_4, cns3519);
  reg_173 = newStack();
  push(reg_173, reg_4);
  reg_174 = newTable();
  reg_175 = cns3520;
  lua$set(reg_174, reg_175, lua$get(lua$get(reg_1.a, cns3521), cns3522));
  lua$set(reg_174, cns3523, reg_65);
  reg_182 = cns3524;
  reg_184 = lua$get(reg_4, cns3525);
  reg_185 = newStack();
  push(reg_185, reg_4);
  reg_186 = newTable();
  reg_187 = cns3526;
  lua$set(reg_186, reg_187, lua$get(reg_1.a, cns3527));
  push(reg_185, reg_186);
  table_append(reg_174, reg_182, call(reg_184, reg_185));
  push(reg_173, reg_174);
  reg_193 = first(call(reg_172, reg_173));
  reg_195 = lua$get(reg_4, cns3528);
  reg_196 = newStack();
  push(reg_196, reg_4);
  reg_197 = newTable();
  lua$set(reg_197, cns3529, cns3530);
  lua$set(reg_197, cns3531, reg_15);
  lua$set(reg_197, cns3532, reg_193);
  push(reg_196, reg_197);
  reg_202 = call(reg_195, reg_196);
  reg_204 = lua$get(reg_4, cns3533);
  reg_205 = newStack();
  push(reg_205, reg_4);
  push(reg_205, lua$get(reg_5, cns3534));
  reg_208 = call(reg_204, reg_205);
  reg_210 = lua$get(reg_4, cns3535);
  reg_211 = newStack();
  push(reg_211, reg_4);
  reg_212 = newTable();
  lua$set(reg_212, cns3536, cns3537);
  lua$set(reg_212, cns3538, reg_10);
  push(reg_211, reg_212);
  reg_216 = call(reg_210, reg_211);
  reg_218 = lua$get(reg_4, cns3539);
  reg_219 = newStack();
  push(reg_219, reg_4);
  reg_220 = newTable();
  lua$set(reg_220, cns3540, cns3541);
  lua$set(reg_220, cns3542, reg_15);
  push(reg_219, reg_220);
  reg_224 = call(reg_218, reg_219);
  reg_226 = lua$get(reg_4, cns3543);
  reg_227 = newStack();
  push(reg_227, reg_4);
  reg_228 = call(reg_226, reg_227);
  reg_233 = lua$get(lua$get(reg_1.a, cns3544), cns3545);
  reg_234 = newStack();
  push(reg_234, lua$get(reg_4, cns3546));
  reg_237 = call(reg_233, reg_234);
  reg_238 = newStack();
  return reg_238;
}
var cns3547 = anyStr("Function")
var cns3548 = anyStr("compileStmt")
var cns3549 = anyStr("type")
var cns3550 = anyStr("local")
var cns3551 = anyStr("ipairs")
var cns3552 = anyStr("names")
var cns3553 = anyStr("lcl")
var cns3554 = anyStr("assign")
var cns3555 = anyStr("values")
var cns3556 = anyStr("line")
var cns3557 = anyStr("call")
var cns3558 = anyStr("compile_call")
var cns3559 = anyStr("assignment")
var cns3560 = anyStr("ipairs")
var cns3561 = anyStr("lhs")
var cns3562 = anyStr("compileLhs")
var cns3563 = anyStr("assign")
var cns3564 = anyStr("values")
var cns3565 = anyStr("line")
var cns3566 = anyStr("return")
var cns3567 = anyStr("call")
var cns3568 = anyStr("stack_f")
var cns3569 = anyStr("ipairs")
var cns3570 = anyStr("values")
var cns3571 = anyStr("values")
var cns3572 = anyStr("type")
var cns3573 = anyStr("call")
var cns3574 = anyStr("compile_call")
var cns3575 = anyStr("call")
var cns3576 = anyStr("append_f")
var cns3577 = anyStr("compileExpr")
var cns3578 = anyStr("call")
var cns3579 = anyStr("push_f")
var cns3580 = anyStr("inst")
var cns3581 = parseNum("1")
var cns3582 = anyStr("end")
var cns3583 = parseNum("2")
var cns3584 = anyStr("funcstat")
var cns3585 = anyStr("method")
var cns3586 = anyStr("table")
var cns3587 = anyStr("insert")
var cns3588 = anyStr("body")
var cns3589 = anyStr("names")
var cns3590 = parseNum("1")
var cns3591 = anyStr("self")
var cns3592 = anyStr("assign")
var cns3593 = parseNum("1")
var cns3594 = anyStr("compileLhs")
var cns3595 = anyStr("lhs")
var cns3596 = parseNum("1")
var cns3597 = anyStr("body")
var cns3598 = anyStr("line")
var cns3599 = anyStr("localfunc")
var cns3600 = parseNum("1")
var cns3601 = anyStr("local")
var cns3602 = anyStr("scope")
var cns3603 = anyStr("locals")
var cns3604 = anyStr("name")
var cns3605 = anyStr("repr")
var cns3606 = anyStr("name")
var cns3607 = parseNum("2")
var cns3608 = anyStr("compileExpr")
var cns3609 = anyStr("body")
var cns3610 = anyStr("inst")
var cns3611 = anyStr("do")
var cns3612 = anyStr("push_scope")
var cns3613 = anyStr("compileBlock")
var cns3614 = anyStr("body")
var cns3615 = anyStr("pop_scope")
var cns3616 = anyStr("if")
var cns3617 = anyStr("lbl")
var cns3618 = anyStr("ipairs")
var cns3619 = anyStr("clauses")
var cns3620 = anyStr("lbl")
var cns3621 = anyStr("compile_bool")
var cns3622 = anyStr("cond")
var cns3623 = anyStr("inst")
var cns3624 = parseNum("1")
var cns3625 = anyStr("nif")
var cns3626 = parseNum("2")
var cns3627 = parseNum("3")
var cns3628 = anyStr("push_scope")
var cns3629 = anyStr("compileBlock")
var cns3630 = anyStr("body")
var cns3631 = anyStr("pop_scope")
var cns3632 = anyStr("inst")
var cns3633 = parseNum("1")
var cns3634 = anyStr("jmp")
var cns3635 = parseNum("2")
var cns3636 = anyStr("inst")
var cns3637 = parseNum("1")
var cns3638 = anyStr("label")
var cns3639 = parseNum("2")
var cns3640 = anyStr("els")
var cns3641 = anyStr("push_scope")
var cns3642 = anyStr("compileBlock")
var cns3643 = anyStr("els")
var cns3644 = anyStr("pop_scope")
var cns3645 = anyStr("inst")
var cns3646 = parseNum("1")
var cns3647 = anyStr("label")
var cns3648 = parseNum("2")
var cns3649 = anyStr("while")
var cns3650 = anyStr("lbl")
var cns3651 = anyStr("lbl")
var cns3652 = anyStr("inst")
var cns3653 = parseNum("1")
var cns3654 = anyStr("label")
var cns3655 = parseNum("2")
var cns3656 = anyStr("compile_bool")
var cns3657 = anyStr("cond")
var cns3658 = anyStr("inst")
var cns3659 = parseNum("1")
var cns3660 = anyStr("nif")
var cns3661 = parseNum("2")
var cns3662 = parseNum("3")
var cns3663 = anyStr("table")
var cns3664 = anyStr("insert")
var cns3665 = anyStr("loops")
var cns3666 = anyStr("push_scope")
var cns3667 = anyStr("compileBlock")
var cns3668 = anyStr("body")
var cns3669 = anyStr("pop_scope")
var cns3670 = anyStr("table")
var cns3671 = anyStr("remove")
var cns3672 = anyStr("loops")
var cns3673 = anyStr("inst")
var cns3674 = parseNum("1")
var cns3675 = anyStr("jmp")
var cns3676 = parseNum("2")
var cns3677 = anyStr("inst")
var cns3678 = parseNum("1")
var cns3679 = anyStr("label")
var cns3680 = parseNum("2")
var cns3681 = anyStr("repeat")
var cns3682 = anyStr("lbl")
var cns3683 = anyStr("lbl")
var cns3684 = anyStr("table")
var cns3685 = anyStr("insert")
var cns3686 = anyStr("loops")
var cns3687 = anyStr("push_scope")
var cns3688 = anyStr("inst")
var cns3689 = parseNum("1")
var cns3690 = anyStr("label")
var cns3691 = parseNum("2")
var cns3692 = anyStr("compileBlock")
var cns3693 = anyStr("body")
var cns3694 = anyStr("compile_bool")
var cns3695 = anyStr("cond")
var cns3696 = anyStr("inst")
var cns3697 = parseNum("1")
var cns3698 = anyStr("nif")
var cns3699 = parseNum("2")
var cns3700 = parseNum("3")
var cns3701 = anyStr("inst")
var cns3702 = parseNum("1")
var cns3703 = anyStr("label")
var cns3704 = parseNum("2")
var cns3705 = anyStr("pop_scope")
var cns3706 = anyStr("table")
var cns3707 = anyStr("remove")
var cns3708 = anyStr("loops")
var cns3709 = anyStr("numfor")
var cns3710 = anyStr("compile_numfor")
var cns3711 = anyStr("genfor")
var cns3712 = anyStr("compile_genfor")
var cns3713 = anyStr("break")
var cns3714 = anyStr("inst")
var cns3715 = parseNum("1")
var cns3716 = anyStr("jmp")
var cns3717 = parseNum("2")
var cns3718 = anyStr("loops")
var cns3719 = anyStr("loops")
var cns3720 = anyStr("label")
var cns3721 = anyStr("inst")
var cns3722 = parseNum("1")
var cns3723 = anyStr("label")
var cns3724 = parseNum("2")
var cns3725 = anyStr("name")
var cns3726 = anyStr("goto")
var cns3727 = anyStr("inst")
var cns3728 = parseNum("1")
var cns3729 = anyStr("jmp")
var cns3730 = parseNum("2")
var cns3731 = anyStr("name")
var cns3732 = anyStr("line")
var cns3733 = anyStr("line")
var cns3734 = anyStr("err")
var cns3735 = anyStr("statement not supported: ")
function Function_compileStmt (reg_0, reg_1) {
  var reg_4, reg_5, reg_7, reg_11, reg_14, reg_15, reg_18, reg_19, reg_20, reg_21, reg_22, reg_23, reg_24, reg_25, reg_29, reg_32, reg_33, reg_43, reg_44, reg_50, reg_53, reg_54, reg_57, reg_58, reg_59, reg_60, reg_61, reg_62, reg_63, reg_64, reg_69, reg_70, reg_74, reg_75, reg_85, reg_86, reg_91, reg_94, reg_95, reg_98, reg_99, reg_100, reg_101, reg_102, reg_103, reg_104, reg_105, reg_112, reg_120, reg_121, reg_123, reg_125, reg_126, reg_132, reg_133, reg_135, reg_137, reg_138, reg_144, reg_145, reg_146, reg_161, reg_162, reg_171, reg_172, reg_173, reg_174, reg_176, reg_177, reg_181, reg_182, reg_191, reg_197, reg_200, reg_203, reg_205, reg_206, reg_212, reg_213, reg_219, reg_220, reg_223, reg_224, reg_229, reg_230, reg_236, reg_237, reg_239, reg_242, reg_243, reg_246, reg_247, reg_248, reg_249, reg_250, reg_251, reg_253, reg_258, reg_259, reg_261, reg_263, reg_264, reg_268, reg_270, reg_271, reg_272, reg_279, reg_280, reg_283, reg_284, reg_289, reg_290, reg_293, reg_294, reg_295, reg_301, reg_302, reg_303, reg_312, reg_313, reg_316, reg_317, reg_322, reg_323, reg_326, reg_327, reg_328, reg_337, reg_338, reg_340, reg_342, reg_343, reg_345, reg_347, reg_348, reg_349, reg_355, reg_356, reg_360, reg_362, reg_363, reg_364, reg_374, reg_375, reg_380, reg_381, reg_384, reg_385, reg_390, reg_391, reg_397, reg_398, reg_403, reg_404, reg_405, reg_411, reg_412, reg_413, reg_422, reg_423, reg_425, reg_427, reg_428, reg_430, reg_435, reg_436, reg_441, reg_442, reg_445, reg_446, reg_447, reg_453, reg_454, reg_459, reg_460, reg_464, reg_466, reg_467, reg_468, reg_475, reg_476, reg_477, reg_483, reg_484, reg_490, reg_491, reg_499, reg_500, reg_506, reg_507, reg_513, reg_514, reg_515, reg_518, reg_520, reg_530, reg_531, reg_532, reg_535, reg_543, reg_544, reg_545, reg_548, reg_551, reg_557, reg_558, reg_562;
  var goto_51=false, goto_64=false, goto_113=false, goto_181=false, goto_213=false, goto_265=false, goto_302=false, goto_325=false, goto_465=false, goto_580=false, goto_683=false, goto_694=false, goto_705=false, goto_728=false, goto_747=false, goto_770=false;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_7 = lua$get(reg_5, cns3549);
  goto_51 = !tobool(eq(reg_7, cns3550));
  if (!goto_51) {
    reg_11 = newTable();
    reg_14 = lua$get(reg_1.a, cns3551);
    reg_15 = newStack();
    push(reg_15, lua$get(reg_5, cns3552));
    reg_18 = call(reg_14, reg_15);
    reg_19 = next(reg_18);
    reg_20 = next(reg_18);
    reg_21 = next(reg_18);
    loop_4: while (true) {
      reg_22 = newStack();
      push(reg_22, reg_20);
      push(reg_22, reg_21);
      reg_23 = call(reg_19, reg_22);
      reg_24 = next(reg_23);
      reg_21 = reg_24;
      reg_25 = next(reg_23);
      if (tobool(eq(reg_21, nil()))) break loop_4;
      reg_29 = newTable();
      lua$set(reg_29, cns3553, reg_25);
      lua$set(reg_11, reg_24, reg_29);
    }
    reg_32 = lua$get(reg_4, cns3554);
    reg_33 = newStack();
    push(reg_33, reg_4);
    push(reg_33, reg_11);
    push(reg_33, lua$get(reg_5, cns3555));
    push(reg_33, lua$get(reg_5, cns3556));
    reg_38 = call(reg_32, reg_33);
  }
  if ((goto_51 || false)) {
    goto_51 = false;
    goto_64 = !tobool(eq(reg_7, cns3557));
    if (!goto_64) {
      reg_43 = lua$get(reg_4, cns3558);
      reg_44 = newStack();
      push(reg_44, reg_4);
      push(reg_44, reg_5);
      push(reg_44, lua$true());
      reg_46 = call(reg_43, reg_44);
    }
    if ((goto_64 || false)) {
      goto_64 = false;
      goto_113 = !tobool(eq(reg_7, cns3559));
      if (!goto_113) {
        reg_50 = newTable();
        reg_53 = lua$get(reg_1.a, cns3560);
        reg_54 = newStack();
        push(reg_54, lua$get(reg_5, cns3561));
        reg_57 = call(reg_53, reg_54);
        reg_58 = next(reg_57);
        reg_59 = next(reg_57);
        reg_60 = next(reg_57);
        loop_3: while (true) {
          reg_61 = newStack();
          push(reg_61, reg_59);
          push(reg_61, reg_60);
          reg_62 = call(reg_58, reg_61);
          reg_63 = next(reg_62);
          reg_60 = reg_63;
          reg_64 = next(reg_62);
          if (tobool(eq(reg_60, nil()))) break loop_3;
          reg_69 = lua$get(reg_4, cns3562);
          reg_70 = newStack();
          push(reg_70, reg_4);
          push(reg_70, reg_64);
          lua$set(reg_50, reg_63, first(call(reg_69, reg_70)));
        }
        reg_74 = lua$get(reg_4, cns3563);
        reg_75 = newStack();
        push(reg_75, reg_4);
        push(reg_75, reg_50);
        push(reg_75, lua$get(reg_5, cns3564));
        push(reg_75, lua$get(reg_5, cns3565));
        reg_80 = call(reg_74, reg_75);
      }
      if ((goto_113 || false)) {
        goto_113 = false;
        goto_213 = !tobool(eq(reg_7, cns3566));
        if (!goto_213) {
          reg_85 = lua$get(reg_4, cns3567);
          reg_86 = newStack();
          push(reg_86, reg_4);
          push(reg_86, lua$get(reg_1.a, cns3568));
          reg_91 = first(call(reg_85, reg_86));
          reg_94 = lua$get(reg_1.a, cns3569);
          reg_95 = newStack();
          push(reg_95, lua$get(reg_5, cns3570));
          reg_98 = call(reg_94, reg_95);
          reg_99 = next(reg_98);
          reg_100 = next(reg_98);
          reg_101 = next(reg_98);
          loop_2: while (true) {
            reg_102 = newStack();
            push(reg_102, reg_100);
            push(reg_102, reg_101);
            reg_103 = call(reg_99, reg_102);
            reg_104 = next(reg_103);
            reg_101 = reg_104;
            reg_105 = next(reg_103);
            if (tobool(eq(reg_101, nil()))) break loop_2;
            reg_112 = eq(reg_104, lua$length(lua$get(reg_5, cns3571)));
            if (tobool(reg_112)) {
              reg_112 = eq(lua$get(reg_105, cns3572), cns3573);
            }
            goto_181 = !tobool(reg_112);
            if (!goto_181) {
              reg_120 = lua$get(reg_4, cns3574);
              reg_121 = newStack();
              push(reg_121, reg_4);
              push(reg_121, reg_105);
              reg_123 = first(call(reg_120, reg_121));
              reg_125 = lua$get(reg_4, cns3575);
              reg_126 = newStack();
              push(reg_126, reg_4);
              push(reg_126, lua$get(reg_1.a, cns3576));
              push(reg_126, reg_91);
              push(reg_126, reg_123);
              reg_130 = call(reg_125, reg_126);
            }
            if ((goto_181 || false)) {
              goto_181 = false;
              reg_132 = lua$get(reg_4, cns3577);
              reg_133 = newStack();
              push(reg_133, reg_4);
              push(reg_133, reg_105);
              reg_135 = first(call(reg_132, reg_133));
              reg_137 = lua$get(reg_4, cns3578);
              reg_138 = newStack();
              push(reg_138, reg_4);
              push(reg_138, lua$get(reg_1.a, cns3579));
              push(reg_138, reg_91);
              push(reg_138, reg_135);
              reg_142 = call(reg_137, reg_138);
            }
          }
          reg_144 = lua$get(reg_4, cns3580);
          reg_145 = newStack();
          push(reg_145, reg_4);
          reg_146 = newTable();
          lua$set(reg_146, cns3581, cns3582);
          lua$set(reg_146, cns3583, reg_91);
          push(reg_145, reg_146);
          reg_150 = call(reg_144, reg_145);
        }
        if ((goto_213 || false)) {
          goto_213 = false;
          goto_265 = !tobool(eq(reg_7, cns3584));
          if (!goto_265) {
            if (tobool(lua$get(reg_5, cns3585))) {
              reg_161 = lua$get(lua$get(reg_1.a, cns3586), cns3587);
              reg_162 = newStack();
              push(reg_162, lua$get(lua$get(reg_5, cns3588), cns3589));
              push(reg_162, cns3590);
              push(reg_162, cns3591);
              reg_169 = call(reg_161, reg_162);
            }
            reg_171 = lua$get(reg_4, cns3592);
            reg_172 = newStack();
            push(reg_172, reg_4);
            reg_173 = newTable();
            reg_174 = cns3593;
            reg_176 = lua$get(reg_4, cns3594);
            reg_177 = newStack();
            push(reg_177, reg_4);
            push(reg_177, lua$get(reg_5, cns3595));
            table_append(reg_173, reg_174, call(reg_176, reg_177));
            push(reg_172, reg_173);
            reg_181 = newTable();
            reg_182 = cns3596;
            lua$set(reg_181, reg_182, lua$get(reg_5, cns3597));
            push(reg_172, reg_181);
            push(reg_172, lua$get(reg_5, cns3598));
            reg_187 = call(reg_171, reg_172);
          }
          if ((goto_265 || false)) {
            goto_265 = false;
            goto_302 = !tobool(eq(reg_7, cns3599));
            if (!goto_302) {
              reg_191 = newTable();
              lua$set(reg_191, cns3600, cns3601);
              reg_197 = lua$get(lua$get(reg_4, cns3602), cns3603);
              lua$set(reg_197, lua$get(reg_5, cns3604), reg_191);
              reg_200 = cns3605;
              lua$set(reg_5, reg_200, lua$get(reg_5, cns3606));
              reg_203 = cns3607;
              reg_205 = lua$get(reg_4, cns3608);
              reg_206 = newStack();
              push(reg_206, reg_4);
              push(reg_206, lua$get(reg_5, cns3609));
              lua$set(reg_191, reg_203, first(call(reg_205, reg_206)));
              reg_212 = lua$get(reg_4, cns3610);
              reg_213 = newStack();
              push(reg_213, reg_4);
              push(reg_213, reg_191);
              reg_214 = call(reg_212, reg_213);
            }
            if ((goto_302 || false)) {
              goto_302 = false;
              goto_325 = !tobool(eq(reg_7, cns3611));
              if (!goto_325) {
                reg_219 = lua$get(reg_4, cns3612);
                reg_220 = newStack();
                push(reg_220, reg_4);
                reg_221 = call(reg_219, reg_220);
                reg_223 = lua$get(reg_4, cns3613);
                reg_224 = newStack();
                push(reg_224, reg_4);
                push(reg_224, lua$get(reg_5, cns3614));
                reg_227 = call(reg_223, reg_224);
                reg_229 = lua$get(reg_4, cns3615);
                reg_230 = newStack();
                push(reg_230, reg_4);
                reg_231 = call(reg_229, reg_230);
              }
              if ((goto_325 || false)) {
                goto_325 = false;
                goto_465 = !tobool(eq(reg_7, cns3616));
                if (!goto_465) {
                  reg_236 = lua$get(reg_4, cns3617);
                  reg_237 = newStack();
                  push(reg_237, reg_4);
                  reg_239 = first(call(reg_236, reg_237));
                  reg_242 = lua$get(reg_1.a, cns3618);
                  reg_243 = newStack();
                  push(reg_243, lua$get(reg_5, cns3619));
                  reg_246 = call(reg_242, reg_243);
                  reg_247 = next(reg_246);
                  reg_248 = next(reg_246);
                  reg_249 = next(reg_246);
                  loop_1: while (true) {
                    reg_250 = newStack();
                    push(reg_250, reg_248);
                    push(reg_250, reg_249);
                    reg_251 = call(reg_247, reg_250);
                    reg_249 = next(reg_251);
                    reg_253 = next(reg_251);
                    if (tobool(eq(reg_249, nil()))) break loop_1;
                    reg_258 = lua$get(reg_4, cns3620);
                    reg_259 = newStack();
                    push(reg_259, reg_4);
                    reg_261 = first(call(reg_258, reg_259));
                    reg_263 = lua$get(reg_4, cns3621);
                    reg_264 = newStack();
                    push(reg_264, reg_4);
                    push(reg_264, lua$get(reg_253, cns3622));
                    reg_268 = first(call(reg_263, reg_264));
                    reg_270 = lua$get(reg_4, cns3623);
                    reg_271 = newStack();
                    push(reg_271, reg_4);
                    reg_272 = newTable();
                    lua$set(reg_272, cns3624, cns3625);
                    lua$set(reg_272, cns3626, reg_261);
                    lua$set(reg_272, cns3627, reg_268);
                    push(reg_271, reg_272);
                    reg_277 = call(reg_270, reg_271);
                    reg_279 = lua$get(reg_4, cns3628);
                    reg_280 = newStack();
                    push(reg_280, reg_4);
                    reg_281 = call(reg_279, reg_280);
                    reg_283 = lua$get(reg_4, cns3629);
                    reg_284 = newStack();
                    push(reg_284, reg_4);
                    push(reg_284, lua$get(reg_253, cns3630));
                    reg_287 = call(reg_283, reg_284);
                    reg_289 = lua$get(reg_4, cns3631);
                    reg_290 = newStack();
                    push(reg_290, reg_4);
                    reg_291 = call(reg_289, reg_290);
                    reg_293 = lua$get(reg_4, cns3632);
                    reg_294 = newStack();
                    push(reg_294, reg_4);
                    reg_295 = newTable();
                    lua$set(reg_295, cns3633, cns3634);
                    lua$set(reg_295, cns3635, reg_239);
                    push(reg_294, reg_295);
                    reg_299 = call(reg_293, reg_294);
                    reg_301 = lua$get(reg_4, cns3636);
                    reg_302 = newStack();
                    push(reg_302, reg_4);
                    reg_303 = newTable();
                    lua$set(reg_303, cns3637, cns3638);
                    lua$set(reg_303, cns3639, reg_261);
                    push(reg_302, reg_303);
                    reg_307 = call(reg_301, reg_302);
                  }
                  if (tobool(lua$get(reg_5, cns3640))) {
                    reg_312 = lua$get(reg_4, cns3641);
                    reg_313 = newStack();
                    push(reg_313, reg_4);
                    reg_314 = call(reg_312, reg_313);
                    reg_316 = lua$get(reg_4, cns3642);
                    reg_317 = newStack();
                    push(reg_317, reg_4);
                    push(reg_317, lua$get(reg_5, cns3643));
                    reg_320 = call(reg_316, reg_317);
                    reg_322 = lua$get(reg_4, cns3644);
                    reg_323 = newStack();
                    push(reg_323, reg_4);
                    reg_324 = call(reg_322, reg_323);
                  }
                  reg_326 = lua$get(reg_4, cns3645);
                  reg_327 = newStack();
                  push(reg_327, reg_4);
                  reg_328 = newTable();
                  lua$set(reg_328, cns3646, cns3647);
                  lua$set(reg_328, cns3648, reg_239);
                  push(reg_327, reg_328);
                  reg_332 = call(reg_326, reg_327);
                }
                if ((goto_465 || false)) {
                  goto_465 = false;
                  goto_580 = !tobool(eq(reg_7, cns3649));
                  if (!goto_580) {
                    reg_337 = lua$get(reg_4, cns3650);
                    reg_338 = newStack();
                    push(reg_338, reg_4);
                    reg_340 = first(call(reg_337, reg_338));
                    reg_342 = lua$get(reg_4, cns3651);
                    reg_343 = newStack();
                    push(reg_343, reg_4);
                    reg_345 = first(call(reg_342, reg_343));
                    reg_347 = lua$get(reg_4, cns3652);
                    reg_348 = newStack();
                    push(reg_348, reg_4);
                    reg_349 = newTable();
                    lua$set(reg_349, cns3653, cns3654);
                    lua$set(reg_349, cns3655, reg_340);
                    push(reg_348, reg_349);
                    reg_353 = call(reg_347, reg_348);
                    reg_355 = lua$get(reg_4, cns3656);
                    reg_356 = newStack();
                    push(reg_356, reg_4);
                    push(reg_356, lua$get(reg_5, cns3657));
                    reg_360 = first(call(reg_355, reg_356));
                    reg_362 = lua$get(reg_4, cns3658);
                    reg_363 = newStack();
                    push(reg_363, reg_4);
                    reg_364 = newTable();
                    lua$set(reg_364, cns3659, cns3660);
                    lua$set(reg_364, cns3661, reg_345);
                    lua$set(reg_364, cns3662, reg_360);
                    push(reg_363, reg_364);
                    reg_369 = call(reg_362, reg_363);
                    reg_374 = lua$get(lua$get(reg_1.a, cns3663), cns3664);
                    reg_375 = newStack();
                    push(reg_375, lua$get(reg_4, cns3665));
                    push(reg_375, reg_345);
                    reg_378 = call(reg_374, reg_375);
                    reg_380 = lua$get(reg_4, cns3666);
                    reg_381 = newStack();
                    push(reg_381, reg_4);
                    reg_382 = call(reg_380, reg_381);
                    reg_384 = lua$get(reg_4, cns3667);
                    reg_385 = newStack();
                    push(reg_385, reg_4);
                    push(reg_385, lua$get(reg_5, cns3668));
                    reg_388 = call(reg_384, reg_385);
                    reg_390 = lua$get(reg_4, cns3669);
                    reg_391 = newStack();
                    push(reg_391, reg_4);
                    reg_392 = call(reg_390, reg_391);
                    reg_397 = lua$get(lua$get(reg_1.a, cns3670), cns3671);
                    reg_398 = newStack();
                    push(reg_398, lua$get(reg_4, cns3672));
                    reg_401 = call(reg_397, reg_398);
                    reg_403 = lua$get(reg_4, cns3673);
                    reg_404 = newStack();
                    push(reg_404, reg_4);
                    reg_405 = newTable();
                    lua$set(reg_405, cns3674, cns3675);
                    lua$set(reg_405, cns3676, reg_340);
                    push(reg_404, reg_405);
                    reg_409 = call(reg_403, reg_404);
                    reg_411 = lua$get(reg_4, cns3677);
                    reg_412 = newStack();
                    push(reg_412, reg_4);
                    reg_413 = newTable();
                    lua$set(reg_413, cns3678, cns3679);
                    lua$set(reg_413, cns3680, reg_345);
                    push(reg_412, reg_413);
                    reg_417 = call(reg_411, reg_412);
                  }
                  if ((goto_580 || false)) {
                    goto_580 = false;
                    goto_683 = !tobool(eq(reg_7, cns3681));
                    if (!goto_683) {
                      reg_422 = lua$get(reg_4, cns3682);
                      reg_423 = newStack();
                      push(reg_423, reg_4);
                      reg_425 = first(call(reg_422, reg_423));
                      reg_427 = lua$get(reg_4, cns3683);
                      reg_428 = newStack();
                      push(reg_428, reg_4);
                      reg_430 = first(call(reg_427, reg_428));
                      reg_435 = lua$get(lua$get(reg_1.a, cns3684), cns3685);
                      reg_436 = newStack();
                      push(reg_436, lua$get(reg_4, cns3686));
                      push(reg_436, reg_430);
                      reg_439 = call(reg_435, reg_436);
                      reg_441 = lua$get(reg_4, cns3687);
                      reg_442 = newStack();
                      push(reg_442, reg_4);
                      reg_443 = call(reg_441, reg_442);
                      reg_445 = lua$get(reg_4, cns3688);
                      reg_446 = newStack();
                      push(reg_446, reg_4);
                      reg_447 = newTable();
                      lua$set(reg_447, cns3689, cns3690);
                      lua$set(reg_447, cns3691, reg_425);
                      push(reg_446, reg_447);
                      reg_451 = call(reg_445, reg_446);
                      reg_453 = lua$get(reg_4, cns3692);
                      reg_454 = newStack();
                      push(reg_454, reg_4);
                      push(reg_454, lua$get(reg_5, cns3693));
                      reg_457 = call(reg_453, reg_454);
                      reg_459 = lua$get(reg_4, cns3694);
                      reg_460 = newStack();
                      push(reg_460, reg_4);
                      push(reg_460, lua$get(reg_5, cns3695));
                      reg_464 = first(call(reg_459, reg_460));
                      reg_466 = lua$get(reg_4, cns3696);
                      reg_467 = newStack();
                      push(reg_467, reg_4);
                      reg_468 = newTable();
                      lua$set(reg_468, cns3697, cns3698);
                      lua$set(reg_468, cns3699, reg_425);
                      lua$set(reg_468, cns3700, reg_464);
                      push(reg_467, reg_468);
                      reg_473 = call(reg_466, reg_467);
                      reg_475 = lua$get(reg_4, cns3701);
                      reg_476 = newStack();
                      push(reg_476, reg_4);
                      reg_477 = newTable();
                      lua$set(reg_477, cns3702, cns3703);
                      lua$set(reg_477, cns3704, reg_430);
                      push(reg_476, reg_477);
                      reg_481 = call(reg_475, reg_476);
                      reg_483 = lua$get(reg_4, cns3705);
                      reg_484 = newStack();
                      push(reg_484, reg_4);
                      reg_485 = call(reg_483, reg_484);
                      reg_490 = lua$get(lua$get(reg_1.a, cns3706), cns3707);
                      reg_491 = newStack();
                      push(reg_491, lua$get(reg_4, cns3708));
                      reg_494 = call(reg_490, reg_491);
                    }
                    if ((goto_683 || false)) {
                      goto_683 = false;
                      goto_694 = !tobool(eq(reg_7, cns3709));
                      if (!goto_694) {
                        reg_499 = lua$get(reg_4, cns3710);
                        reg_500 = newStack();
                        push(reg_500, reg_4);
                        push(reg_500, reg_5);
                        reg_501 = call(reg_499, reg_500);
                      }
                      if ((goto_694 || false)) {
                        goto_694 = false;
                        goto_705 = !tobool(eq(reg_7, cns3711));
                        if (!goto_705) {
                          reg_506 = lua$get(reg_4, cns3712);
                          reg_507 = newStack();
                          push(reg_507, reg_4);
                          push(reg_507, reg_5);
                          reg_508 = call(reg_506, reg_507);
                        }
                        if ((goto_705 || false)) {
                          goto_705 = false;
                          goto_728 = !tobool(eq(reg_7, cns3713));
                          if (!goto_728) {
                            reg_513 = lua$get(reg_4, cns3714);
                            reg_514 = newStack();
                            push(reg_514, reg_4);
                            reg_515 = newTable();
                            lua$set(reg_515, cns3715, cns3716);
                            reg_518 = cns3717;
                            reg_520 = lua$get(reg_4, cns3718);
                            lua$set(reg_515, reg_518, lua$get(reg_520, lua$length(lua$get(reg_4, cns3719))));
                            push(reg_514, reg_515);
                            reg_525 = call(reg_513, reg_514);
                          }
                          if ((goto_728 || false)) {
                            goto_728 = false;
                            goto_747 = !tobool(eq(reg_7, cns3720));
                            if (!goto_747) {
                              reg_530 = lua$get(reg_4, cns3721);
                              reg_531 = newStack();
                              push(reg_531, reg_4);
                              reg_532 = newTable();
                              lua$set(reg_532, cns3722, cns3723);
                              reg_535 = cns3724;
                              lua$set(reg_532, reg_535, lua$get(reg_5, cns3725));
                              push(reg_531, reg_532);
                              reg_538 = call(reg_530, reg_531);
                            }
                            if ((goto_747 || false)) {
                              goto_747 = false;
                              goto_770 = !tobool(eq(reg_7, cns3726));
                              if (!goto_770) {
                                reg_543 = lua$get(reg_4, cns3727);
                                reg_544 = newStack();
                                push(reg_544, reg_4);
                                reg_545 = newTable();
                                lua$set(reg_545, cns3728, cns3729);
                                reg_548 = cns3730;
                                lua$set(reg_545, reg_548, lua$get(reg_5, cns3731));
                                reg_551 = cns3732;
                                lua$set(reg_545, reg_551, lua$get(reg_5, cns3733));
                                push(reg_544, reg_545);
                                reg_554 = call(reg_543, reg_544);
                              }
                              if ((goto_770 || false)) {
                                goto_770 = false;
                                reg_557 = lua$get(reg_1.a, cns3734);
                                reg_558 = newStack();
                                push(reg_558, concat(cns3735, reg_7));
                                push(reg_558, reg_5);
                                reg_561 = call(reg_557, reg_558);
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  reg_562 = newStack();
  return reg_562;
}
var cns3736 = anyStr("Function")
var cns3737 = anyStr("compileBlock")
var cns3738 = anyStr("ipairs")
var cns3739 = anyStr("compileStmt")
function Function_compileBlock (reg_0, reg_1) {
  var reg_4, reg_5, reg_8, reg_9, reg_10, reg_11, reg_12, reg_13, reg_14, reg_15, reg_17, reg_22, reg_23, reg_25;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_1.a, cns3738);
  reg_9 = newStack();
  push(reg_9, reg_5);
  reg_10 = call(reg_8, reg_9);
  reg_11 = next(reg_10);
  reg_12 = next(reg_10);
  reg_13 = next(reg_10);
  loop_1: while (true) {
    reg_14 = newStack();
    push(reg_14, reg_12);
    push(reg_14, reg_13);
    reg_15 = call(reg_11, reg_14);
    reg_13 = next(reg_15);
    reg_17 = next(reg_15);
    if (tobool(eq(reg_13, nil()))) break loop_1;
    reg_22 = lua$get(reg_4, cns3739);
    reg_23 = newStack();
    push(reg_23, reg_4);
    push(reg_23, reg_17);
    reg_24 = call(reg_22, reg_23);
  }
  reg_25 = newStack();
  return reg_25;
}
var cns3740 = anyStr("Function")
var cns3741 = anyStr("transform")
var cns3742 = anyStr("code")
var cns3743 = anyStr("code")
var cns3744 = anyStr("labels")
var cns3745 = anyStr("ins")
var cns3746 = parseNum("1")
function function$88 (reg_0, reg_1) {
  var reg_6, reg_9;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_6 = reg_1.b;
  reg_1.b = add(reg_6, cns3746);
  reg_9 = newStack();
  push(reg_9, reg_6);
  return reg_9;
}
var cns3747 = anyStr("reginc")
var cns3748 = anyStr("ipairs")
var cns3749 = parseNum("1")
var cns3750 = anyStr("local")
var cns3751 = parseNum("2")
var cns3752 = anyStr("is_upval")
var cns3753 = anyStr("upval_level_regs")
var cns3754 = anyStr("level")
var cns3755 = anyStr("upval_accessors")
var cns3756 = anyStr("upval_id")
var cns3757 = anyStr("setter")
var cns3758 = anyStr("inst")
var cns3759 = parseNum("1")
var cns3760 = parseNum("2")
var cns3761 = parseNum("3")
var cns3762 = parseNum("1")
var cns3763 = anyStr("var")
var cns3764 = anyStr("is_upval")
var cns3765 = anyStr("inst")
var cns3766 = parseNum("1")
var cns3767 = anyStr("dup")
var cns3768 = parseNum("2")
var cns3769 = anyStr("reg")
var cns3770 = anyStr("reginc")
var cns3771 = anyStr("reg")
var cns3772 = anyStr("reg")
var cns3773 = anyStr("reg")
var cns3774 = anyStr("print")
var cns3775 = anyStr("argument does not have register")
var cns3776 = anyStr("var")
var cns3777 = parseNum("2")
var cns3778 = anyStr("is_upval")
var cns3779 = anyStr("upval_level_regs")
var cns3780 = anyStr("upval_level")
var cns3781 = anyStr("get_level_ancestor")
var cns3782 = anyStr("upval_level")
var cns3783 = anyStr("upval_accessors")
var cns3784 = anyStr("upval_id")
var cns3785 = anyStr("getter")
var cns3786 = anyStr("inst")
var cns3787 = parseNum("1")
var cns3788 = parseNum("2")
var cns3789 = anyStr("reg")
var cns3790 = anyStr("reginc")
var cns3791 = anyStr("reg")
var cns3792 = parseNum("2")
var cns3793 = anyStr("reg")
var cns3794 = anyStr("set")
var cns3795 = parseNum("2")
var cns3796 = parseNum("3")
var cns3797 = anyStr("is_upval")
var cns3798 = anyStr("upval_level_regs")
var cns3799 = anyStr("upval_level")
var cns3800 = anyStr("get_level_ancestor")
var cns3801 = anyStr("upval_level")
var cns3802 = anyStr("upval_accessors")
var cns3803 = anyStr("upval_id")
var cns3804 = anyStr("setter")
var cns3805 = anyStr("inst")
var cns3806 = parseNum("1")
var cns3807 = parseNum("2")
var cns3808 = parseNum("3")
var cns3809 = anyStr("inst")
var cns3810 = anyStr("label")
var cns3811 = anyStr("labels")
var cns3812 = parseNum("2")
var cns3813 = anyStr("code")
var cns3814 = anyStr("jif")
var cns3815 = anyStr("nif")
var cns3816 = parseNum("3")
var cns3817 = anyStr("au_type")
var cns3818 = parseNum("3")
var cns3819 = anyStr("inst")
var cns3820 = parseNum("1")
var cns3821 = anyStr("bool_f")
var cns3822 = parseNum("2")
var cns3823 = parseNum("3")
var cns3824 = parseNum("3")
var cns3825 = anyStr("reg")
var cns3826 = anyStr("reginc")
var cns3827 = anyStr("inst")
var cns3828 = anyStr("type")
var cns3829 = anyStr("table")
var cns3830 = parseNum("1")
var cns3831 = anyStr("ins")
var cns3832 = anyStr("error")
var cns3833 = anyStr("tostring")
var cns3834 = anyStr(" expects ")
var cns3835 = anyStr("ins")
var cns3836 = anyStr(" arguments, but got ")
var cns3837 = parseNum("1")
var cns3838 = anyStr("outs")
var cns3839 = parseNum("1")
var cns3840 = anyStr("reg")
var cns3841 = anyStr("reginc")
var cns3842 = anyStr("outs")
var cns3843 = parseNum("1")
var cns3844 = anyStr("reg")
var cns3845 = anyStr("outs")
var cns3846 = anyStr("regs")
var cns3847 = anyStr("ipairs")
var cns3848 = anyStr("regs")
var cns3849 = anyStr("reg")
var cns3850 = anyStr("reg")
var cns3851 = parseNum("1")
var cns3852 = anyStr("inst")
var cns3853 = anyStr("inst")
function Function_transform (reg_0, reg_1) {
  var reg_3, reg_4, reg_6, reg_15, reg_20, reg_21, reg_22, reg_23, reg_24, reg_25, reg_26, reg_27, reg_29, reg_34, reg_39, reg_44, reg_47, reg_49, reg_54, reg_56, reg_57, reg_58, reg_66, reg_73, reg_74, reg_75, reg_80, reg_90, reg_95, reg_96, reg_103, reg_108, reg_111, reg_113, reg_114, reg_120, reg_125, reg_127, reg_128, reg_129, reg_133, reg_140, reg_149, reg_151, reg_156, reg_159, reg_161, reg_162, reg_168, reg_173, reg_175, reg_176, reg_177, reg_183, reg_184, reg_190, reg_192, reg_197, reg_208, reg_210, reg_211, reg_212, reg_213, reg_217, reg_223, reg_224, reg_232, reg_233, reg_237, reg_238, reg_246, reg_254, reg_255, reg_258, reg_259, reg_261, reg_262, reg_265, reg_266, reg_281, reg_296, reg_306, reg_307, reg_310, reg_311, reg_312, reg_313, reg_314, reg_315, reg_316, reg_317, reg_321, reg_328, reg_329, reg_332, reg_333, reg_335;
  var goto_78=false, goto_111=false, goto_120=false, goto_128=false, goto_179=false, goto_186=false, goto_233=false, goto_240=false, goto_253=false, goto_306=false, goto_368=false, goto_427=false;
  reg_3 = {a: reg_1, b: nil()};
  reg_4 = next(reg_0);
  reg_6 = lua$get(reg_4, cns3742);
  lua$set(reg_4, cns3743, newTable());
  lua$set(reg_4, cns3744, newTable());
  reg_3.b = lua$length(lua$get(reg_4, cns3745));
  reg_15 = anyFn((function (a) { return function$88(a,this) }).bind(reg_3));
  lua$set(reg_1.a, cns3747, reg_15);
  reg_20 = lua$get(reg_1.a, cns3748);
  reg_21 = newStack();
  push(reg_21, reg_6);
  reg_22 = call(reg_20, reg_21);
  reg_23 = next(reg_22);
  reg_24 = next(reg_22);
  reg_25 = next(reg_22);
  loop_1: while (true) {
    reg_26 = newStack();
    push(reg_26, reg_24);
    push(reg_26, reg_25);
    reg_27 = call(reg_23, reg_26);
    reg_25 = next(reg_27);
    reg_29 = next(reg_27);
    if (tobool(eq(reg_25, nil()))) break loop_1;
    reg_34 = lua$get(reg_29, cns3749);
    goto_128 = !tobool(eq(reg_34, cns3750));
    if (!goto_128) {
      reg_39 = lua$get(reg_29, cns3751);
      goto_78 = !tobool(lua$get(reg_29, cns3752));
      if (!goto_78) {
        reg_44 = lua$get(reg_4, cns3753);
        reg_47 = lua$get(reg_44, lua$get(reg_4, cns3754));
        reg_49 = lua$get(reg_4, cns3755);
        reg_54 = lua$get(lua$get(reg_49, lua$get(reg_29, cns3756)), cns3757);
        reg_56 = lua$get(reg_4, cns3758);
        reg_57 = newStack();
        push(reg_57, reg_4);
        reg_58 = newTable();
        lua$set(reg_58, cns3759, reg_54);
        lua$set(reg_58, cns3760, reg_47);
        lua$set(reg_58, cns3761, reg_39);
        push(reg_57, reg_58);
        reg_62 = call(reg_56, reg_57);
      }
      if ((goto_78 || false)) {
        goto_78 = false;
        reg_66 = eq(lua$get(reg_39, cns3762), cns3763);
        if (tobool(reg_66)) {
          reg_66 = not(lua$get(reg_39, cns3764));
        }
        goto_111 = !tobool(reg_66);
        if (!goto_111) {
          reg_73 = lua$get(reg_4, cns3765);
          reg_74 = newStack();
          push(reg_74, reg_4);
          reg_75 = newTable();
          lua$set(reg_75, cns3766, cns3767);
          lua$set(reg_75, cns3768, reg_39);
          push(reg_74, reg_75);
          reg_79 = call(reg_73, reg_74);
          reg_80 = cns3769;
          lua$set(reg_29, reg_80, first(call(lua$get(reg_1.a, cns3770), newStack())));
        }
        if ((goto_111 || false)) {
          goto_111 = false;
          goto_120 = !tobool(lua$get(reg_39, cns3771));
          if (!goto_120) {
            reg_90 = cns3772;
            lua$set(reg_29, reg_90, lua$get(reg_39, cns3773));
          }
          if ((goto_120 || false)) {
            goto_120 = false;
            reg_95 = lua$get(reg_1.a, cns3774);
            reg_96 = newStack();
            push(reg_96, cns3775);
            reg_98 = call(reg_95, reg_96);
          }
        }
      }
    }
    if ((goto_128 || false)) {
      goto_128 = false;
      goto_186 = !tobool(eq(reg_34, cns3776));
      if (!goto_186) {
        reg_103 = lua$get(reg_29, cns3777);
        goto_179 = !tobool(lua$get(reg_103, cns3778));
        if (!goto_179) {
          reg_108 = lua$get(reg_4, cns3779);
          reg_111 = lua$get(reg_108, lua$get(reg_103, cns3780));
          reg_113 = lua$get(reg_4, cns3781);
          reg_114 = newStack();
          push(reg_114, reg_4);
          push(reg_114, lua$get(reg_103, cns3782));
          reg_120 = lua$get(first(call(reg_113, reg_114)), cns3783);
          reg_125 = lua$get(lua$get(reg_120, lua$get(reg_103, cns3784)), cns3785);
          reg_127 = lua$get(reg_4, cns3786);
          reg_128 = newStack();
          push(reg_128, reg_4);
          reg_129 = newTable();
          lua$set(reg_129, cns3787, reg_125);
          lua$set(reg_129, cns3788, reg_111);
          push(reg_128, reg_129);
          reg_132 = call(reg_127, reg_128);
          reg_133 = cns3789;
          lua$set(reg_29, reg_133, first(call(lua$get(reg_1.a, cns3790), newStack())));
        }
        if ((goto_179 || false)) {
          goto_179 = false;
          reg_140 = cns3791;
          lua$set(reg_29, reg_140, lua$get(lua$get(reg_29, cns3792), cns3793));
        }
      }
      if ((goto_186 || false)) {
        goto_186 = false;
        goto_240 = !tobool(eq(reg_34, cns3794));
        if (!goto_240) {
          reg_149 = lua$get(reg_29, cns3795);
          reg_151 = lua$get(reg_29, cns3796);
          goto_233 = !tobool(lua$get(reg_149, cns3797));
          if (!goto_233) {
            reg_156 = lua$get(reg_4, cns3798);
            reg_159 = lua$get(reg_156, lua$get(reg_149, cns3799));
            reg_161 = lua$get(reg_4, cns3800);
            reg_162 = newStack();
            push(reg_162, reg_4);
            push(reg_162, lua$get(reg_149, cns3801));
            reg_168 = lua$get(first(call(reg_161, reg_162)), cns3802);
            reg_173 = lua$get(lua$get(reg_168, lua$get(reg_149, cns3803)), cns3804);
            reg_175 = lua$get(reg_4, cns3805);
            reg_176 = newStack();
            push(reg_176, reg_4);
            reg_177 = newTable();
            lua$set(reg_177, cns3806, reg_173);
            lua$set(reg_177, cns3807, reg_159);
            lua$set(reg_177, cns3808, reg_151);
            push(reg_176, reg_177);
            reg_181 = call(reg_175, reg_176);
          }
          if ((goto_233 || false)) {
            goto_233 = false;
            reg_183 = lua$get(reg_4, cns3809);
            reg_184 = newStack();
            push(reg_184, reg_4);
            push(reg_184, reg_29);
            reg_185 = call(reg_183, reg_184);
          }
        }
        if ((goto_240 || false)) {
          goto_240 = false;
          goto_253 = !tobool(eq(reg_34, cns3810));
          if (!goto_253) {
            reg_190 = lua$get(reg_4, cns3811);
            reg_192 = lua$get(reg_29, cns3812);
            lua$set(reg_190, reg_192, lua$length(lua$get(reg_4, cns3813)));
          }
          if ((goto_253 || false)) {
            goto_253 = false;
            reg_197 = eq(reg_34, cns3814);
            if (!tobool(reg_197)) {
              reg_197 = eq(reg_34, cns3815);
            }
            goto_306 = !tobool(reg_197);
            if (!goto_306) {
              if (tobool(not(lua$get(lua$get(reg_29, cns3816), cns3817)))) {
                reg_208 = cns3818;
                reg_210 = lua$get(reg_4, cns3819);
                reg_211 = newStack();
                push(reg_211, reg_4);
                reg_212 = newTable();
                reg_213 = cns3820;
                lua$set(reg_212, reg_213, lua$get(reg_1.a, cns3821));
                reg_217 = cns3822;
                lua$set(reg_212, reg_217, lua$get(reg_29, cns3823));
                push(reg_211, reg_212);
                lua$set(reg_29, reg_208, first(call(reg_210, reg_211)));
                reg_223 = lua$get(reg_29, cns3824);
                reg_224 = cns3825;
                lua$set(reg_223, reg_224, first(call(lua$get(reg_1.a, cns3826), newStack())));
              }
              reg_232 = lua$get(reg_4, cns3827);
              reg_233 = newStack();
              push(reg_233, reg_4);
              push(reg_233, reg_29);
              reg_234 = call(reg_232, reg_233);
            }
            if ((goto_306 || false)) {
              goto_306 = false;
              reg_237 = lua$get(reg_1.a, cns3828);
              reg_238 = newStack();
              push(reg_238, reg_34);
              goto_427 = !tobool(eq(first(call(reg_237, reg_238)), cns3829));
              if (!goto_427) {
                reg_246 = sub(lua$length(reg_29), cns3830);
                if (tobool(ne(reg_246, lua$length(lua$get(reg_34, cns3831))))) {
                  reg_254 = lua$get(reg_1.a, cns3832);
                  reg_255 = newStack();
                  reg_258 = lua$get(reg_1.a, cns3833);
                  reg_259 = newStack();
                  push(reg_259, reg_34);
                  reg_261 = first(call(reg_258, reg_259));
                  reg_262 = cns3834;
                  reg_265 = lua$length(lua$get(reg_34, cns3835));
                  reg_266 = cns3836;
                  push(reg_255, concat(reg_261, concat(reg_262, concat(reg_265, concat(reg_266, sub(lua$length(reg_29), cns3837))))));
                  reg_274 = call(reg_254, reg_255);
                }
                goto_368 = !tobool(eq(lua$length(lua$get(reg_34, cns3838)), cns3839));
                if (!goto_368) {
                  reg_281 = cns3840;
                  lua$set(reg_29, reg_281, first(call(lua$get(reg_1.a, cns3841), newStack())));
                }
                if ((goto_368 || false)) {
                  goto_368 = false;
                  if (tobool(gt(lua$length(lua$get(reg_34, cns3842)), cns3843))) {
                    lua$set(reg_29, cns3844, reg_3.b);
                    reg_296 = reg_3.b;
                    reg_3.b = add(reg_296, lua$length(lua$get(reg_34, cns3845)));
                  }
                }
                if (tobool(lua$get(reg_29, cns3846))) {
                  reg_306 = lua$get(reg_1.a, cns3847);
                  reg_307 = newStack();
                  push(reg_307, lua$get(reg_29, cns3848));
                  reg_310 = call(reg_306, reg_307);
                  reg_311 = next(reg_310);
                  reg_312 = next(reg_310);
                  reg_313 = next(reg_310);
                  loop_2: while (true) {
                    reg_314 = newStack();
                    push(reg_314, reg_312);
                    push(reg_314, reg_313);
                    reg_315 = call(reg_311, reg_314);
                    reg_316 = next(reg_315);
                    reg_313 = reg_316;
                    reg_317 = next(reg_315);
                    if (tobool(eq(reg_313, nil()))) break loop_2;
                    reg_321 = cns3849;
                    lua$set(reg_317, reg_321, sub(add(lua$get(reg_29, cns3850), reg_316), cns3851));
                  }
                }
                reg_328 = lua$get(reg_4, cns3852);
                reg_329 = newStack();
                push(reg_329, reg_4);
                push(reg_329, reg_29);
                reg_330 = call(reg_328, reg_329);
              }
              if ((goto_427 || false)) {
                goto_427 = false;
                reg_332 = lua$get(reg_4, cns3853);
                reg_333 = newStack();
                push(reg_333, reg_4);
                push(reg_333, reg_29);
                reg_334 = call(reg_332, reg_333);
              }
            }
          }
        }
      }
    }
  }
  reg_335 = newStack();
  return reg_335;
}
var cns3854 = anyStr("Function")
var cns3855 = anyStr("generate_sourcemap")
var cns3856 = parseNum("1")
var cns3857 = anyStr("code")
var cns3858 = anyStr("ipairs")
var cns3859 = anyStr("code")
var cns3860 = anyStr("line")
var cns3861 = anyStr("table")
var cns3862 = anyStr("insert")
var cns3863 = parseNum("1")
var cns3864 = parseNum("2")
var cns3865 = anyStr("line")
var cns3866 = parseNum("3")
var cns3867 = anyStr("column")
var cns3868 = parseNum("1")
var cns3869 = anyStr("function")
var cns3870 = parseNum("2")
var cns3871 = anyStr("id")
var cns3872 = parseNum("3")
var cns3873 = anyStr("name")
var cns3874 = anyStr("table")
var cns3875 = anyStr("insert")
var cns3876 = parseNum("1")
var cns3877 = anyStr("name")
var cns3878 = parseNum("2")
var cns3879 = anyStr("name")
var cns3880 = anyStr("line")
var cns3881 = anyStr("table")
var cns3882 = anyStr("insert")
var cns3883 = parseNum("1")
var cns3884 = anyStr("line")
var cns3885 = parseNum("2")
var cns3886 = anyStr("line")
var cns3887 = anyStr("table")
var cns3888 = anyStr("insert")
var cns3889 = anyStr("sourcemap")
function Function_generate_sourcemap (reg_0, reg_1) {
  var reg_4, reg_5, reg_10, reg_11, reg_14, reg_15, reg_16, reg_17, reg_18, reg_19, reg_20, reg_21, reg_32, reg_33, reg_34, reg_36, reg_39, reg_43, reg_46, reg_48, reg_49, reg_60, reg_61, reg_62, reg_65, reg_76, reg_77, reg_78, reg_81, reg_89, reg_90, reg_95;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = newTable();
  lua$set(reg_5, cns3856, cns3857);
  reg_10 = lua$get(reg_1.a, cns3858);
  reg_11 = newStack();
  push(reg_11, lua$get(reg_4, cns3859));
  reg_14 = call(reg_10, reg_11);
  reg_15 = next(reg_14);
  reg_16 = next(reg_14);
  reg_17 = next(reg_14);
  loop_1: while (true) {
    reg_18 = newStack();
    push(reg_18, reg_16);
    push(reg_18, reg_17);
    reg_19 = call(reg_15, reg_18);
    reg_20 = next(reg_19);
    reg_17 = reg_20;
    reg_21 = next(reg_19);
    if (tobool(eq(reg_17, nil()))) break loop_1;
    if (tobool(lua$get(reg_21, cns3860))) {
      reg_32 = lua$get(lua$get(reg_1.a, cns3861), cns3862);
      reg_33 = newStack();
      push(reg_33, reg_5);
      reg_34 = newTable();
      lua$set(reg_34, cns3863, reg_20);
      reg_36 = cns3864;
      lua$set(reg_34, reg_36, lua$get(reg_21, cns3865));
      reg_39 = cns3866;
      lua$set(reg_34, reg_39, lua$get(reg_21, cns3867));
      push(reg_33, reg_34);
      reg_42 = call(reg_32, reg_33);
    }
  }
  reg_43 = newTable();
  lua$set(reg_43, cns3868, cns3869);
  reg_46 = cns3870;
  reg_48 = lua$get(reg_4, cns3871);
  reg_49 = newStack();
  push(reg_49, reg_4);
  lua$set(reg_43, reg_46, first(call(reg_48, reg_49)));
  lua$set(reg_43, cns3872, reg_5);
  if (tobool(lua$get(reg_4, cns3873))) {
    reg_60 = lua$get(lua$get(reg_1.a, cns3874), cns3875);
    reg_61 = newStack();
    push(reg_61, reg_43);
    reg_62 = newTable();
    lua$set(reg_62, cns3876, cns3877);
    reg_65 = cns3878;
    lua$set(reg_62, reg_65, lua$get(reg_4, cns3879));
    push(reg_61, reg_62);
    reg_68 = call(reg_60, reg_61);
  }
  if (tobool(lua$get(reg_4, cns3880))) {
    reg_76 = lua$get(lua$get(reg_1.a, cns3881), cns3882);
    reg_77 = newStack();
    push(reg_77, reg_43);
    reg_78 = newTable();
    lua$set(reg_78, cns3883, cns3884);
    reg_81 = cns3885;
    lua$set(reg_78, reg_81, lua$get(reg_4, cns3886));
    push(reg_77, reg_78);
    reg_84 = call(reg_76, reg_77);
  }
  reg_89 = lua$get(lua$get(reg_1.a, cns3887), cns3888);
  reg_90 = newStack();
  push(reg_90, lua$get(reg_1.a, cns3889));
  push(reg_90, reg_43);
  reg_94 = call(reg_89, reg_90);
  reg_95 = newStack();
  return reg_95;
}
var cns3890 = anyStr("create_compiler_state")
var cns3891 = anyStr("code")
var cns3892 = anyStr("lua_main")
var cns3893 = anyStr("lua_main")
var cns3894 = anyStr("lua_main")
var cns3895 = anyStr("ins")
var cns3896 = parseNum("1")
var cns3897 = anyStr("any_t")
var cns3898 = anyStr("id")
var cns3899 = anyStr("lua_main")
var cns3900 = anyStr("outs")
var cns3901 = parseNum("1")
var cns3902 = anyStr("stack_t")
var cns3903 = anyStr("id")
var cns3904 = anyStr("lua_main")
var cns3905 = anyStr("level")
var cns3906 = parseNum("1")
var cns3907 = anyStr("lua_main")
var cns3908 = anyStr("line")
var cns3909 = parseNum("1")
var cns3910 = anyStr("lua_main")
var cns3911 = anyStr("create_upval_info")
var cns3912 = anyStr("lua_main")
var cns3913 = anyStr("scope")
var cns3914 = anyStr("locals")
var cns3915 = anyStr("_ENV")
var cns3916 = anyStr("lua_main")
var cns3917 = anyStr("inst")
var cns3918 = parseNum("1")
var cns3919 = anyStr("local")
var cns3920 = parseNum("2")
var cns3921 = anyStr("reg")
var cns3922 = parseNum("0")
var cns3923 = anyStr("lua_main")
var cns3924 = anyStr("scope")
var cns3925 = anyStr("locals")
var cns3926 = anyStr(".ENV")
var cns3927 = anyStr("lua_main")
var cns3928 = anyStr("inst")
var cns3929 = parseNum("1")
var cns3930 = anyStr("local")
var cns3931 = parseNum("2")
var cns3932 = anyStr("reg")
var cns3933 = parseNum("0")
var cns3934 = anyStr("lua_main")
var cns3935 = anyStr("scope")
var cns3936 = anyStr("locals")
var cns3937 = anyStr("_AU_IMPORT")
var cns3938 = anyStr("au_type")
var cns3939 = anyStr("macro")
var cns3940 = anyStr("macro")
var cns3941 = anyStr("import")
var cns3942 = anyStr("lua_main")
var cns3943 = anyStr("scope")
var cns3944 = anyStr("locals")
var cns3945 = anyStr("_AU_FUNCTION")
var cns3946 = anyStr("au_type")
var cns3947 = anyStr("macro")
var cns3948 = anyStr("macro")
var cns3949 = anyStr("function")
var cns3950 = anyStr("lua_main")
var cns3951 = anyStr("compileBlock")
var cns3952 = parseNum("0")
var cns3953 = anyStr("type")
var cns3954 = anyStr("return")
var cns3955 = anyStr("lua_main")
var cns3956 = anyStr("call")
var cns3957 = anyStr("stack_f")
var cns3958 = anyStr("lua_main")
var cns3959 = anyStr("inst")
var cns3960 = parseNum("1")
var cns3961 = anyStr("end")
var cns3962 = parseNum("2")
var cns3963 = anyStr("lua_main")
var cns3964 = anyStr("build_upvals")
var cns3965 = anyStr("lua_main")
var cns3966 = anyStr("transform")
var cns3967 = anyStr("code")
var cns3968 = anyStr("main")
var cns3969 = anyStr("main")
var cns3970 = anyStr("main")
var cns3971 = anyStr("inst")
var cns3972 = parseNum("1")
var cns3973 = anyStr("global_f")
var cns3974 = anyStr("reg")
var cns3975 = parseNum("0")
var cns3976 = anyStr("main")
var cns3977 = anyStr("inst")
var cns3978 = parseNum("1")
var cns3979 = anyStr("lua_main")
var cns3980 = parseNum("2")
var cns3981 = anyStr("reg")
var cns3982 = parseNum("1")
var cns3983 = anyStr("main")
var cns3984 = anyStr("inst")
var cns3985 = parseNum("1")
var cns3986 = anyStr("end")
var cns3987 = anyStr("table")
var cns3988 = anyStr("insert")
var cns3989 = anyStr("sourcemap")
var cns3990 = parseNum("1")
var cns3991 = anyStr("file")
var cns3992 = parseNum("2")
var cns3993 = anyStr("ipairs")
var cns3994 = anyStr("funcs")
var cns3995 = anyStr("code")
var cns3996 = anyStr("generate_sourcemap")
var cns3997 = anyStr("table")
var cns3998 = anyStr("insert")
var cns3999 = anyStr("metadata")
var cns4000 = anyStr("sourcemap")
function function$89 (reg_0, reg_1) {
  var reg_4, reg_5, reg_13, reg_14, reg_17, reg_22, reg_23, reg_24, reg_25, reg_33, reg_34, reg_35, reg_36, reg_54, reg_56, reg_57, reg_65, reg_66, reg_69, reg_71, reg_72, reg_73, reg_76, reg_77, reg_88, reg_89, reg_92, reg_94, reg_95, reg_96, reg_99, reg_100, reg_111, reg_112, reg_113, reg_124, reg_125, reg_126, reg_133, reg_135, reg_136, reg_140, reg_151, reg_153, reg_154, reg_159, reg_162, reg_164, reg_165, reg_166, reg_173, reg_175, reg_176, reg_180, reg_182, reg_183, reg_187, reg_188, reg_191, reg_196, reg_198, reg_199, reg_200, reg_201, reg_208, reg_211, reg_213, reg_214, reg_215, reg_216, reg_226, reg_228, reg_229, reg_230, reg_239, reg_240, reg_244, reg_251, reg_252, reg_256, reg_257, reg_258, reg_259, reg_260, reg_261, reg_263, reg_271, reg_272, reg_278, reg_279, reg_287;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_10 = call(lua$get(reg_1.a, cns3890), newStack());
  reg_13 = lua$get(reg_1.a, cns3891);
  reg_14 = newStack();
  push(reg_14, cns3892);
  reg_17 = first(call(reg_13, reg_14));
  lua$set(reg_1.a, cns3893, reg_17);
  reg_22 = lua$get(reg_1.a, cns3894);
  reg_23 = cns3895;
  reg_24 = newTable();
  reg_25 = cns3896;
  lua$set(reg_24, reg_25, lua$get(lua$get(reg_1.a, cns3897), cns3898));
  lua$set(reg_22, reg_23, reg_24);
  reg_33 = lua$get(reg_1.a, cns3899);
  reg_34 = cns3900;
  reg_35 = newTable();
  reg_36 = cns3901;
  lua$set(reg_35, reg_36, lua$get(lua$get(reg_1.a, cns3902), cns3903));
  lua$set(reg_33, reg_34, reg_35);
  lua$set(lua$get(reg_1.a, cns3904), cns3905, cns3906);
  lua$set(lua$get(reg_1.a, cns3907), cns3908, cns3909);
  reg_54 = lua$get(reg_1.a, cns3910);
  reg_56 = lua$get(reg_54, cns3911);
  reg_57 = newStack();
  push(reg_57, reg_54);
  reg_58 = call(reg_56, reg_57);
  reg_65 = lua$get(lua$get(lua$get(reg_1.a, cns3912), cns3913), cns3914);
  reg_66 = cns3915;
  reg_69 = lua$get(reg_1.a, cns3916);
  reg_71 = lua$get(reg_69, cns3917);
  reg_72 = newStack();
  push(reg_72, reg_69);
  reg_73 = newTable();
  lua$set(reg_73, cns3918, cns3919);
  reg_76 = cns3920;
  reg_77 = newTable();
  lua$set(reg_77, cns3921, cns3922);
  lua$set(reg_73, reg_76, reg_77);
  push(reg_72, reg_73);
  lua$set(reg_65, reg_66, first(call(reg_71, reg_72)));
  reg_88 = lua$get(lua$get(lua$get(reg_1.a, cns3923), cns3924), cns3925);
  reg_89 = cns3926;
  reg_92 = lua$get(reg_1.a, cns3927);
  reg_94 = lua$get(reg_92, cns3928);
  reg_95 = newStack();
  push(reg_95, reg_92);
  reg_96 = newTable();
  lua$set(reg_96, cns3929, cns3930);
  reg_99 = cns3931;
  reg_100 = newTable();
  lua$set(reg_100, cns3932, cns3933);
  lua$set(reg_96, reg_99, reg_100);
  push(reg_95, reg_96);
  lua$set(reg_88, reg_89, first(call(reg_94, reg_95)));
  reg_111 = lua$get(lua$get(lua$get(reg_1.a, cns3934), cns3935), cns3936);
  reg_112 = cns3937;
  reg_113 = newTable();
  lua$set(reg_113, cns3938, cns3939);
  lua$set(reg_113, cns3940, cns3941);
  lua$set(reg_111, reg_112, reg_113);
  reg_124 = lua$get(lua$get(lua$get(reg_1.a, cns3942), cns3943), cns3944);
  reg_125 = cns3945;
  reg_126 = newTable();
  lua$set(reg_126, cns3946, cns3947);
  lua$set(reg_126, cns3948, cns3949);
  lua$set(reg_124, reg_125, reg_126);
  reg_133 = lua$get(reg_1.a, cns3950);
  reg_135 = lua$get(reg_133, cns3951);
  reg_136 = newStack();
  push(reg_136, reg_133);
  push(reg_136, reg_4);
  reg_137 = call(reg_135, reg_136);
  reg_140 = eq(lua$length(reg_4), cns3952);
  if (!tobool(reg_140)) {
    reg_140 = ne(lua$get(lua$get(reg_4, lua$length(reg_4)), cns3953), cns3954);
  }
  if (tobool(reg_140)) {
    reg_151 = lua$get(reg_1.a, cns3955);
    reg_153 = lua$get(reg_151, cns3956);
    reg_154 = newStack();
    push(reg_154, reg_151);
    push(reg_154, lua$get(reg_1.a, cns3957));
    reg_159 = first(call(reg_153, reg_154));
    reg_162 = lua$get(reg_1.a, cns3958);
    reg_164 = lua$get(reg_162, cns3959);
    reg_165 = newStack();
    push(reg_165, reg_162);
    reg_166 = newTable();
    lua$set(reg_166, cns3960, cns3961);
    lua$set(reg_166, cns3962, reg_159);
    push(reg_165, reg_166);
    reg_170 = call(reg_164, reg_165);
  }
  reg_173 = lua$get(reg_1.a, cns3963);
  reg_175 = lua$get(reg_173, cns3964);
  reg_176 = newStack();
  push(reg_176, reg_173);
  reg_177 = call(reg_175, reg_176);
  reg_180 = lua$get(reg_1.a, cns3965);
  reg_182 = lua$get(reg_180, cns3966);
  reg_183 = newStack();
  push(reg_183, reg_180);
  reg_184 = call(reg_182, reg_183);
  reg_187 = lua$get(reg_1.a, cns3967);
  reg_188 = newStack();
  push(reg_188, cns3968);
  reg_191 = first(call(reg_187, reg_188));
  lua$set(reg_1.a, cns3969, reg_191);
  reg_196 = lua$get(reg_1.a, cns3970);
  reg_198 = lua$get(reg_196, cns3971);
  reg_199 = newStack();
  push(reg_199, reg_196);
  reg_200 = newTable();
  reg_201 = cns3972;
  lua$set(reg_200, reg_201, lua$get(reg_1.a, cns3973));
  lua$set(reg_200, cns3974, cns3975);
  push(reg_199, reg_200);
  reg_208 = first(call(reg_198, reg_199));
  reg_211 = lua$get(reg_1.a, cns3976);
  reg_213 = lua$get(reg_211, cns3977);
  reg_214 = newStack();
  push(reg_214, reg_211);
  reg_215 = newTable();
  reg_216 = cns3978;
  lua$set(reg_215, reg_216, lua$get(reg_1.a, cns3979));
  lua$set(reg_215, cns3980, reg_208);
  lua$set(reg_215, cns3981, cns3982);
  push(reg_214, reg_215);
  reg_223 = call(reg_213, reg_214);
  reg_226 = lua$get(reg_1.a, cns3983);
  reg_228 = lua$get(reg_226, cns3984);
  reg_229 = newStack();
  push(reg_229, reg_226);
  reg_230 = newTable();
  lua$set(reg_230, cns3985, cns3986);
  push(reg_229, reg_230);
  reg_233 = call(reg_228, reg_229);
  if (tobool(reg_5)) {
    reg_239 = lua$get(lua$get(reg_1.a, cns3987), cns3988);
    reg_240 = newStack();
    push(reg_240, lua$get(reg_1.a, cns3989));
    reg_244 = newTable();
    lua$set(reg_244, cns3990, cns3991);
    lua$set(reg_244, cns3992, reg_5);
    push(reg_240, reg_244);
    reg_248 = call(reg_239, reg_240);
  }
  reg_251 = lua$get(reg_1.a, cns3993);
  reg_252 = newStack();
  push(reg_252, lua$get(reg_1.a, cns3994));
  reg_256 = call(reg_251, reg_252);
  reg_257 = next(reg_256);
  reg_258 = next(reg_256);
  reg_259 = next(reg_256);
  loop_1: while (true) {
    reg_260 = newStack();
    push(reg_260, reg_258);
    push(reg_260, reg_259);
    reg_261 = call(reg_257, reg_260);
    reg_259 = next(reg_261);
    reg_263 = next(reg_261);
    if (tobool(eq(reg_259, nil()))) break loop_1;
    if (tobool(lua$get(reg_263, cns3995))) {
      reg_271 = lua$get(reg_263, cns3996);
      reg_272 = newStack();
      push(reg_272, reg_263);
      reg_273 = call(reg_271, reg_272);
    }
  }
  reg_278 = lua$get(lua$get(reg_1.a, cns3997), cns3998);
  reg_279 = newStack();
  push(reg_279, lua$get(reg_1.a, cns3999));
  push(reg_279, lua$get(reg_1.a, cns4000));
  reg_286 = call(reg_278, reg_279);
  reg_287 = newStack();
  return reg_287;
}
function aulua_codegen$lua_main (reg_0) {
  var reg_2, reg_8, reg_9, reg_14, reg_15, reg_20, reg_21, reg_26, reg_27, reg_32, reg_33, reg_38, reg_39, reg_44, reg_45, reg_50, reg_51, reg_56, reg_57, reg_62, reg_63, reg_68, reg_69, reg_74, reg_75, reg_79, reg_84, reg_85, reg_90, reg_91, reg_96, reg_97, reg_102, reg_103, reg_108, reg_109, reg_114, reg_115, reg_118;
  reg_2 = {a: nil()};
  reg_2.a = reg_0;
  reg_3 = aulua_helpers$lua_main(reg_0);
  reg_4 = aulua_basics$lua_main(reg_0);
  reg_5 = aulua_auro_syntax$lua_main(reg_0);
  reg_8 = lua$get(reg_2.a, cns2577);
  reg_9 = cns2578;
  lua$set(reg_8, reg_9, anyFn((function (a) { return Function_create_upval_info(a,this) }).bind(reg_2)));
  reg_14 = lua$get(reg_2.a, cns2628);
  reg_15 = cns2629;
  lua$set(reg_14, reg_15, anyFn((function (a) { return Function_build_upvals(a,this) }).bind(reg_2)));
  reg_20 = lua$get(reg_2.a, cns2683);
  reg_21 = cns2684;
  lua$set(reg_20, reg_21, anyFn((function (a) { return Function_get_vararg(a,this) }).bind(reg_2)));
  reg_26 = lua$get(reg_2.a, cns2689);
  reg_27 = cns2690;
  lua$set(reg_26, reg_27, anyFn((function (a) { return Function_get_local(a,this) }).bind(reg_2)));
  reg_32 = lua$get(reg_2.a, cns2735);
  reg_33 = cns2736;
  lua$set(reg_32, reg_33, anyFn((function (a) { return Function_get_level_ancestor(a,this) }).bind(reg_2)));
  reg_38 = lua$get(reg_2.a, cns2741);
  reg_39 = cns2742;
  lua$set(reg_38, reg_39, anyFn((function (a) { return Function_createFunction(a,this) }).bind(reg_2)));
  reg_44 = lua$get(reg_2.a, cns2826);
  reg_45 = cns2827;
  lua$set(reg_44, reg_45, anyFn((function (a) { return Function_compile_require(a,this) }).bind(reg_2)));
  reg_50 = lua$get(reg_2.a, cns2866);
  reg_51 = cns2867;
  lua$set(reg_50, reg_51, anyFn((function (a) { return Function_compile_call(a,this) }).bind(reg_2)));
  reg_56 = lua$get(reg_2.a, cns2934);
  reg_57 = cns2935;
  lua$set(reg_56, reg_57, anyFn((function (a) { return Function_compile_bool(a,this) }).bind(reg_2)));
  reg_62 = lua$get(reg_2.a, cns2954);
  reg_63 = cns2955;
  lua$set(reg_62, reg_63, anyFn((function (a) { return Function_compileExpr(a,this) }).bind(reg_2)));
  reg_68 = lua$get(reg_2.a, cns3163);
  reg_69 = cns3164;
  lua$set(reg_68, reg_69, anyFn((function (a) { return Function_assign(a,this) }).bind(reg_2)));
  reg_74 = lua$get(reg_2.a, cns3269);
  reg_75 = cns3270;
  lua$set(reg_74, reg_75, anyFn((function (a) { return Function_compileLhs(a,this) }).bind(reg_2)));
  reg_79 = anyFn((function (a) { return aulua_codegen$function(a,this) }).bind(reg_2));
  lua$set(reg_2.a, cns3334, reg_79);
  reg_84 = lua$get(reg_2.a, cns3335);
  reg_85 = cns3336;
  lua$set(reg_84, reg_85, anyFn((function (a) { return Function_compile_numfor(a,this) }).bind(reg_2)));
  reg_90 = lua$get(reg_2.a, cns3451);
  reg_91 = cns3452;
  lua$set(reg_90, reg_91, anyFn((function (a) { return Function_compile_genfor(a,this) }).bind(reg_2)));
  reg_96 = lua$get(reg_2.a, cns3547);
  reg_97 = cns3548;
  lua$set(reg_96, reg_97, anyFn((function (a) { return Function_compileStmt(a,this) }).bind(reg_2)));
  reg_102 = lua$get(reg_2.a, cns3736);
  reg_103 = cns3737;
  lua$set(reg_102, reg_103, anyFn((function (a) { return Function_compileBlock(a,this) }).bind(reg_2)));
  reg_108 = lua$get(reg_2.a, cns3740);
  reg_109 = cns3741;
  lua$set(reg_108, reg_109, anyFn((function (a) { return Function_transform(a,this) }).bind(reg_2)));
  reg_114 = lua$get(reg_2.a, cns3854);
  reg_115 = cns3855;
  lua$set(reg_114, reg_115, anyFn((function (a) { return Function_generate_sourcemap(a,this) }).bind(reg_2)));
  reg_118 = newStack();
  push(reg_118, anyFn((function (a) { return function$89(a,this) }).bind(reg_2)));
  return reg_118;
}
var cns4001 = anyStr("ipairs")
var cns4002 = parseNum("1")
function function$90 (reg_0, reg_1) {
  var reg_2, reg_7, reg_8, reg_9, reg_12, reg_13, reg_14, reg_15, reg_16, reg_17, reg_19, reg_23, reg_24, reg_26;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_7 = lua$get(reg_2.a, cns4001);
  reg_8 = newStack();
  reg_9 = newTable();
  table_append(reg_9, cns4002, copy(reg_0));
  push(reg_8, reg_9);
  reg_12 = call(reg_7, reg_8);
  reg_13 = next(reg_12);
  reg_14 = next(reg_12);
  reg_15 = next(reg_12);
  loop_1: while (true) {
    reg_16 = newStack();
    push(reg_16, reg_14);
    push(reg_16, reg_15);
    reg_17 = call(reg_13, reg_16);
    reg_15 = next(reg_17);
    reg_19 = next(reg_17);
    if (tobool(eq(reg_15, nil()))) break loop_1;
    reg_23 = reg_1.b;
    reg_24 = newStack();
    push(reg_24, reg_19);
    reg_25 = call(reg_23, reg_24);
  }
  reg_26 = newStack();
  return reg_26;
}
var cns4003 = parseNum("0")
var cns4004 = parseNum("128")
function idiv (reg_0, reg_1) {
  var reg_2, reg_5, reg_6, reg_7, reg_8, reg_9, reg_17, reg_20, reg_21, reg_22, reg_24, reg_25, reg_26, reg_29, reg_31;
  reg_2 = (reg_0 instanceof Integer);
  if (reg_2) {
    reg_2 = (reg_1 instanceof Integer);
  }
  if (reg_2) {
    reg_5 = reg_0.val;
    reg_6 = reg_1.val;
    reg_7 = ((reg_5 / reg_6) | 0);
    reg_9 = (reg_7 > 0);
    if (!reg_9) {
      reg_9 = ((reg_5 * reg_6) > 0);
    }
    reg_8 = reg_9;
    if (!reg_8) {
      reg_8 = ((reg_7 * reg_6) == reg_5);
    }
    if (reg_8) {
      reg_17 = new Integer(reg_7);
      return reg_17;
    }
    reg_20 = new Integer((reg_7 - 1));
    return reg_20;
  }
  var _r = getFloats(reg_0, reg_1); reg_24 = _r[0]; reg_25 = _r[1]; reg_26 = _r[2];
  reg_21 = reg_24;
  reg_22 = reg_25;
  if (reg_26) {
    reg_29 = Math.floor((reg_21 / reg_22));
    return reg_29;
  }
  reg_31 = meta_arith(reg_0, reg_1, "__idiv");
  return reg_31;
}
var cns4005 = parseNum("128")
var cns4006 = parseNum("128")
function function$92 (reg_0, reg_1) {
  var reg_2, reg_6, reg_11, reg_12, reg_13, reg_15, reg_16, reg_23;
  reg_2 = reg_1.a;
  reg_3 = reg_2.a;
  reg_4 = nil();
  reg_5 = {a: reg_1};
  reg_6 = next(reg_0);
  if (tobool(gt(reg_6, cns4003))) {
    reg_11 = idiv(reg_6, cns4004);
    reg_12 = reg_1.b;
    reg_13 = newStack();
    push(reg_13, reg_11);
    reg_14 = call(reg_12, reg_13);
    reg_15 = reg_2.c;
    reg_16 = newStack();
    push(reg_16, add(sub(reg_6, mul(reg_11, cns4005)), cns4006));
    reg_22 = call(reg_15, reg_16);
  }
  reg_23 = newStack();
  return reg_23;
}
var cns4007 = parseNum("128")
var cns4008 = parseNum("128")
function function$91 (reg_0, reg_1) {
  var reg_4, reg_5, reg_9, reg_10, reg_11, reg_13, reg_14, reg_19;
  reg_2 = reg_1.a;
  reg_4 = {a: reg_1, b: nil()};
  reg_5 = next(reg_0);
  reg_4.b = anyFn((function (a) { return function$92(a,this) }).bind(reg_4));
  reg_9 = idiv(reg_5, cns4007);
  reg_10 = reg_4.b;
  reg_11 = newStack();
  push(reg_11, reg_9);
  reg_12 = call(reg_10, reg_11);
  reg_13 = reg_1.c;
  reg_14 = newStack();
  push(reg_14, sub(reg_5, mul(reg_9, cns4008)));
  reg_18 = call(reg_13, reg_14);
  reg_19 = newStack();
  return reg_19;
}
var cns4009 = anyStr("byte")
var cns4010 = parseNum("1")
var cns4011 = parseNum("1")
function function$93 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_9, reg_10, reg_16;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = reg_1.c;
  reg_7 = newStack();
  reg_9 = lua$get(reg_5, cns4009);
  reg_10 = newStack();
  push(reg_10, reg_5);
  push(reg_10, cns4010);
  push(reg_10, unm(cns4011));
  append(reg_7, call(reg_9, reg_10));
  reg_15 = call(reg_6, reg_7);
  reg_16 = newStack();
  return reg_16;
}
function function$94 (reg_0, reg_1) {
  var reg_5, reg_6, reg_7, reg_10, reg_11, reg_13;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_6 = reg_1.d;
  reg_7 = newStack();
  push(reg_7, lua$length(reg_5));
  reg_9 = call(reg_6, reg_7);
  reg_10 = reg_1.e;
  reg_11 = newStack();
  push(reg_11, reg_5);
  reg_12 = call(reg_10, reg_11);
  reg_13 = newStack();
  return reg_13;
}
var cns4012 = anyStr("Auro 0.6\x00")
var cns4013 = anyStr("modules")
var cns4014 = parseNum("1")
var cns4015 = parseNum("2")
var cns4016 = parseNum("2")
var cns4017 = parseNum("2")
var cns4018 = anyStr("lua_main")
var cns4019 = anyStr("id")
var cns4020 = anyStr("lua_main")
var cns4021 = parseNum("2")
var cns4022 = anyStr("main")
var cns4023 = anyStr("id")
var cns4024 = anyStr("main")
var cns4025 = anyStr("ipairs")
var cns4026 = anyStr("modules")
var cns4027 = anyStr("items")
var cns4028 = parseNum("2")
var cns4029 = anyStr("items")
var cns4030 = anyStr("ipairs")
var cns4031 = anyStr("items")
var cns4032 = anyStr("fn")
var cns4033 = parseNum("2")
var cns4034 = anyStr("fn")
var cns4035 = anyStr("id")
var cns4036 = anyStr("tp")
var cns4037 = parseNum("1")
var cns4038 = anyStr("tp")
var cns4039 = anyStr("id")
var cns4040 = anyStr("err")
var cns4041 = anyStr("Unknown item kind for ")
var cns4042 = anyStr("tostr")
var cns4043 = anyStr("name")
var cns4044 = anyStr("base")
var cns4045 = anyStr("argument")
var cns4046 = parseNum("4")
var cns4047 = anyStr("base")
var cns4048 = anyStr("id")
var cns4049 = anyStr("argument")
var cns4050 = anyStr("id")
var cns4051 = anyStr("from")
var cns4052 = parseNum("3")
var cns4053 = anyStr("from")
var cns4054 = anyStr("id")
var cns4055 = anyStr("name")
var cns4056 = parseNum("1")
var cns4057 = anyStr("name")
var cns4058 = anyStr("types")
var cns4059 = anyStr("ipairs")
var cns4060 = anyStr("types")
var cns4061 = anyStr("module")
var cns4062 = parseNum("1")
var cns4063 = anyStr("name")
var cns4064 = anyStr("funcs")
var cns4065 = anyStr("ipairs")
var cns4066 = anyStr("funcs")
var cns4067 = anyStr("code")
var cns4068 = parseNum("1")
var cns4069 = anyStr("module")
var cns4070 = anyStr("module")
var cns4071 = parseNum("2")
var cns4072 = anyStr("error")
var cns4073 = anyStr("???")
var cns4074 = anyStr("ins")
var cns4075 = anyStr("ipairs")
var cns4076 = anyStr("ins")
var cns4077 = anyStr("outs")
var cns4078 = anyStr("ipairs")
var cns4079 = anyStr("outs")
var cns4080 = anyStr("module")
var cns4081 = anyStr("name")
var cns4082 = anyStr("constants")
var cns4083 = anyStr("ipairs")
var cns4084 = anyStr("constants")
var cns4085 = anyStr("type")
var cns4086 = anyStr("int")
var cns4087 = parseNum("1")
var cns4088 = anyStr("value")
var cns4089 = anyStr("type")
var cns4090 = anyStr("bin")
var cns4091 = parseNum("2")
var cns4092 = anyStr("value")
var cns4093 = anyStr("type")
var cns4094 = anyStr("call")
var cns4095 = anyStr("args")
var cns4096 = anyStr("f")
var cns4097 = anyStr("ins")
var cns4098 = anyStr("error")
var cns4099 = anyStr("f")
var cns4100 = anyStr("name")
var cns4101 = anyStr(" expects ")
var cns4102 = anyStr("f")
var cns4103 = anyStr("ins")
var cns4104 = anyStr(" arguments, but got ")
var cns4105 = anyStr("args")
var cns4106 = anyStr("f")
var cns4107 = anyStr("id")
var cns4108 = parseNum("16")
var cns4109 = anyStr("ipairs")
var cns4110 = anyStr("args")
var cns4111 = anyStr("id")
var cns4112 = anyStr("error")
var cns4113 = anyStr("constant ")
var cns4114 = anyStr("type")
var cns4115 = anyStr(" not supported")
var cns4116 = parseNum("0")
var cns4117 = anyStr("regs")
var cns4118 = anyStr("labels")
var cns4119 = anyStr("error")
var cns4120 = anyStr("no visible label '")
var cns4121 = anyStr("' for <goto> at line ")
function function$96 (reg_0, reg_1) {
  var reg_3, reg_6, reg_7, reg_11, reg_16, reg_17, reg_18, reg_24;
  reg_3 = reg_1.a.a;
  reg_4 = nil();
  reg_5 = {a: reg_1};
  reg_6 = next(reg_0);
  reg_7 = next(reg_0);
  reg_11 = lua$get(lua$get(reg_1.b, cns4118), reg_6);
  if (tobool(not(reg_11))) {
    reg_16 = lua$get(reg_3.a, cns4119);
    reg_17 = newStack();
    reg_18 = cns4120;
    push(reg_17, concat(reg_18, concat(reg_6, concat(cns4121, reg_7))));
    reg_23 = call(reg_16, reg_17);
  }
  reg_24 = newStack();
  push(reg_24, reg_11);
  return reg_24;
}
var cns4122 = anyStr("code")
var cns4123 = anyStr("ipairs")
var cns4124 = anyStr("code")
var cns4125 = parseNum("1")
var cns4126 = anyStr("end")
var cns4127 = parseNum("1")
var cns4128 = anyStr("outs")
var cns4129 = anyStr("error")
var cns4130 = anyStr("name")
var cns4131 = anyStr(" outputs ")
var cns4132 = anyStr("outs")
var cns4133 = anyStr(" results, but end instrucion has ")
var cns4134 = parseNum("1")
var cns4135 = parseNum("0")
var cns4136 = parseNum("2")
var cns4137 = parseNum("1")
var cns4138 = parseNum("0")
var cns4139 = anyStr("reg")
var cns4140 = anyStr("dup")
var cns4141 = parseNum("3")
var cns4142 = parseNum("2")
var cns4143 = anyStr("reg")
var cns4144 = anyStr("reg")
var cns4145 = parseNum("1")
var cns4146 = anyStr("set")
var cns4147 = parseNum("4")
var cns4148 = parseNum("2")
var cns4149 = anyStr("reg")
var cns4150 = parseNum("3")
var cns4151 = anyStr("reg")
var cns4152 = anyStr("jmp")
var cns4153 = parseNum("5")
var cns4154 = parseNum("2")
var cns4155 = anyStr("line")
var cns4156 = anyStr("jif")
var cns4157 = parseNum("6")
var cns4158 = parseNum("2")
var cns4159 = parseNum("3")
var cns4160 = anyStr("reg")
var cns4161 = anyStr("nif")
var cns4162 = parseNum("7")
var cns4163 = parseNum("2")
var cns4164 = parseNum("3")
var cns4165 = anyStr("reg")
var cns4166 = anyStr("type")
var cns4167 = anyStr("table")
var cns4168 = anyStr("id")
var cns4169 = parseNum("16")
var cns4170 = parseNum("2")
var cns4171 = parseNum("1")
var cns4172 = parseNum("0")
var cns4173 = anyStr("reg")
var cns4174 = anyStr("error")
var cns4175 = anyStr("Unsupported instruction: ")
var cns4176 = anyStr("tostring")
function function$95 (reg_0, reg_1) {
  var reg_2, reg_4, reg_11, reg_13, reg_14, reg_15, reg_23, reg_24, reg_28, reg_29, reg_30, reg_31, reg_32, reg_33, reg_35, reg_40, reg_46, reg_55, reg_56, reg_59, reg_60, reg_64, reg_65, reg_74, reg_75, reg_78, reg_79, reg_80, reg_82, reg_88, reg_89, reg_98, reg_99, reg_102, reg_103, reg_115, reg_116, reg_119, reg_120, reg_126, reg_127, reg_136, reg_137, reg_140, reg_141, reg_142, reg_152, reg_153, reg_156, reg_157, reg_158, reg_163, reg_164, reg_173, reg_174, reg_177, reg_178, reg_179, reg_184, reg_185, reg_193, reg_194, reg_200, reg_201, reg_203, reg_204, reg_210, reg_211, reg_212, reg_214, reg_220, reg_221, reg_229, reg_230, reg_231, reg_234, reg_235, reg_240;
  var goto_99=false, goto_113=false, goto_136=false, goto_162=false, goto_184=false, goto_211=false, goto_238=false, goto_272=false, goto_286=false;
  reg_2 = reg_1.a;
  reg_4 = {a: reg_1, b: nil()};
  reg_4.b = next(reg_0);
  reg_6 = newTable();
  reg_7 = cns4116;
  reg_11 = lua$length(lua$get(reg_4.b, cns4117));
  reg_13 = anyFn((function (a) { return function$96(a,this) }).bind(reg_4));
  reg_14 = reg_1.d;
  reg_15 = newStack();
  push(reg_15, lua$length(lua$get(reg_4.b, cns4122)));
  reg_20 = call(reg_14, reg_15);
  reg_23 = lua$get(reg_2.a, cns4123);
  reg_24 = newStack();
  push(reg_24, lua$get(reg_4.b, cns4124));
  reg_28 = call(reg_23, reg_24);
  reg_29 = next(reg_28);
  reg_30 = next(reg_28);
  reg_31 = next(reg_28);
  loop_1: while (true) {
    reg_32 = newStack();
    push(reg_32, reg_30);
    push(reg_32, reg_31);
    reg_33 = call(reg_29, reg_32);
    reg_31 = next(reg_33);
    reg_35 = next(reg_33);
    if (tobool(eq(reg_31, nil()))) break loop_1;
    reg_40 = lua$get(reg_35, cns4125);
    goto_113 = !tobool(eq(reg_40, cns4126));
    if (!goto_113) {
      reg_46 = sub(lua$length(reg_35), cns4127);
      if (tobool(ne(reg_46, lua$length(lua$get(reg_4.b, cns4128))))) {
        reg_55 = lua$get(reg_2.a, cns4129);
        reg_56 = newStack();
        reg_59 = lua$get(reg_4.b, cns4130);
        reg_60 = cns4131;
        reg_64 = lua$length(lua$get(reg_4.b, cns4132));
        reg_65 = cns4133;
        push(reg_56, concat(reg_59, concat(reg_60, concat(reg_64, concat(reg_65, sub(lua$length(reg_35), cns4134))))));
        reg_73 = call(reg_55, reg_56);
      }
      reg_74 = reg_1.c;
      reg_75 = newStack();
      push(reg_75, cns4135);
      reg_77 = call(reg_74, reg_75);
      reg_78 = cns4136;
      reg_79 = lua$length(reg_35);
      reg_80 = cns4137;
      reg_82 = lt(reg_80, cns4138);
      loop_3: while (true) {
        goto_99 = tobool(reg_82);
        if (!goto_99) {
          if (tobool(gt(reg_78, reg_79))) break loop_3;
        }
        if ((goto_99 || false)) {
          goto_99 = false;
          if (tobool(lt(reg_78, reg_79))) break loop_3;
        }
        reg_88 = reg_1.d;
        reg_89 = newStack();
        push(reg_89, lua$get(lua$get(reg_35, reg_78), cns4139));
        reg_93 = call(reg_88, reg_89);
        reg_78 = add(reg_78, reg_80);
      }
    }
    if ((goto_113 || false)) {
      goto_113 = false;
      goto_136 = !tobool(eq(reg_40, cns4140));
      if (!goto_136) {
        reg_98 = reg_1.c;
        reg_99 = newStack();
        push(reg_99, cns4141);
        reg_101 = call(reg_98, reg_99);
        reg_102 = reg_1.d;
        reg_103 = newStack();
        push(reg_103, lua$get(lua$get(reg_35, cns4142), cns4143));
        reg_108 = call(reg_102, reg_103);
        lua$set(reg_35, cns4144, reg_11);
        reg_11 = add(reg_11, cns4145);
      }
      if ((goto_136 || false)) {
        goto_136 = false;
        goto_162 = !tobool(eq(reg_40, cns4146));
        if (!goto_162) {
          reg_115 = reg_1.c;
          reg_116 = newStack();
          push(reg_116, cns4147);
          reg_118 = call(reg_115, reg_116);
          reg_119 = reg_1.d;
          reg_120 = newStack();
          push(reg_120, lua$get(lua$get(reg_35, cns4148), cns4149));
          reg_125 = call(reg_119, reg_120);
          reg_126 = reg_1.d;
          reg_127 = newStack();
          push(reg_127, lua$get(lua$get(reg_35, cns4150), cns4151));
          reg_132 = call(reg_126, reg_127);
        }
        if ((goto_162 || false)) {
          goto_162 = false;
          goto_184 = !tobool(eq(reg_40, cns4152));
          if (!goto_184) {
            reg_136 = reg_1.c;
            reg_137 = newStack();
            push(reg_137, cns4153);
            reg_139 = call(reg_136, reg_137);
            reg_140 = reg_1.d;
            reg_141 = newStack();
            reg_142 = newStack();
            push(reg_142, lua$get(reg_35, cns4154));
            push(reg_142, lua$get(reg_35, cns4155));
            append(reg_141, call(reg_13, reg_142));
            reg_148 = call(reg_140, reg_141);
          }
          if ((goto_184 || false)) {
            goto_184 = false;
            goto_211 = !tobool(eq(reg_40, cns4156));
            if (!goto_211) {
              reg_152 = reg_1.c;
              reg_153 = newStack();
              push(reg_153, cns4157);
              reg_155 = call(reg_152, reg_153);
              reg_156 = reg_1.d;
              reg_157 = newStack();
              reg_158 = newStack();
              push(reg_158, lua$get(reg_35, cns4158));
              append(reg_157, call(reg_13, reg_158));
              reg_162 = call(reg_156, reg_157);
              reg_163 = reg_1.d;
              reg_164 = newStack();
              push(reg_164, lua$get(lua$get(reg_35, cns4159), cns4160));
              reg_169 = call(reg_163, reg_164);
            }
            if ((goto_211 || false)) {
              goto_211 = false;
              goto_238 = !tobool(eq(reg_40, cns4161));
              if (!goto_238) {
                reg_173 = reg_1.c;
                reg_174 = newStack();
                push(reg_174, cns4162);
                reg_176 = call(reg_173, reg_174);
                reg_177 = reg_1.d;
                reg_178 = newStack();
                reg_179 = newStack();
                push(reg_179, lua$get(reg_35, cns4163));
                append(reg_178, call(reg_13, reg_179));
                reg_183 = call(reg_177, reg_178);
                reg_184 = reg_1.d;
                reg_185 = newStack();
                push(reg_185, lua$get(lua$get(reg_35, cns4164), cns4165));
                reg_190 = call(reg_184, reg_185);
              }
              if ((goto_238 || false)) {
                goto_238 = false;
                reg_193 = lua$get(reg_2.a, cns4166);
                reg_194 = newStack();
                push(reg_194, reg_40);
                goto_286 = !tobool(eq(first(call(reg_193, reg_194)), cns4167));
                if (!goto_286) {
                  reg_200 = reg_1.d;
                  reg_201 = newStack();
                  reg_203 = lua$get(reg_40, cns4168);
                  reg_204 = newStack();
                  push(reg_204, reg_40);
                  push(reg_201, add(first(call(reg_203, reg_204)), cns4169));
                  reg_209 = call(reg_200, reg_201);
                  reg_210 = cns4170;
                  reg_211 = lua$length(reg_35);
                  reg_212 = cns4171;
                  reg_214 = lt(reg_212, cns4172);
                  loop_2: while (true) {
                    goto_272 = tobool(reg_214);
                    if (!goto_272) {
                      if (tobool(gt(reg_210, reg_211))) break loop_2;
                    }
                    if ((goto_272 || false)) {
                      goto_272 = false;
                      if (tobool(lt(reg_210, reg_211))) break loop_2;
                    }
                    reg_220 = reg_1.d;
                    reg_221 = newStack();
                    push(reg_221, lua$get(lua$get(reg_35, reg_210), cns4173));
                    reg_225 = call(reg_220, reg_221);
                    reg_210 = add(reg_210, reg_212);
                  }
                }
                if ((goto_286 || false)) {
                  goto_286 = false;
                  reg_229 = lua$get(reg_2.a, cns4174);
                  reg_230 = newStack();
                  reg_231 = cns4175;
                  reg_234 = lua$get(reg_2.a, cns4176);
                  reg_235 = newStack();
                  push(reg_235, reg_40);
                  push(reg_230, concat(reg_231, first(call(reg_234, reg_235))));
                  reg_239 = call(reg_229, reg_230);
                }
              }
            }
          }
        }
      }
    }
  }
  reg_240 = newStack();
  return reg_240;
}
var cns4177 = anyStr("ipairs")
var cns4178 = anyStr("funcs")
var cns4179 = anyStr("code")
var cns4180 = anyStr("type")
var cns4181 = anyStr("number")
var cns4182 = parseNum("2")
var cns4183 = parseNum("1")
var cns4184 = anyStr("type")
var cns4185 = anyStr("string")
var cns4186 = parseNum("4")
var cns4187 = parseNum("2")
var cns4188 = anyStr("type")
var cns4189 = anyStr("table")
var cns4190 = parseNum("4")
var cns4191 = parseNum("1")
var cns4192 = parseNum("1")
var cns4193 = parseNum("0")
var cns4194 = anyStr("write_node")
var cns4195 = anyStr("error")
var cns4196 = anyStr("wtf: ")
var cns4197 = anyStr("tostring")
function function$97 (reg_0, reg_1) {
  var reg_2, reg_5, reg_8, reg_9, reg_15, reg_16, reg_24, reg_25, reg_31, reg_32, reg_39, reg_40, reg_44, reg_45, reg_51, reg_52, reg_57, reg_58, reg_59, reg_61, reg_69, reg_70, reg_76, reg_77, reg_78, reg_81, reg_82, reg_87;
  var goto_24=false, goto_49=false, goto_78=false, goto_92=false;
  reg_2 = reg_1.a;
  reg_3 = nil();
  reg_4 = {a: reg_1};
  reg_5 = next(reg_0);
  reg_8 = lua$get(reg_2.a, cns4180);
  reg_9 = newStack();
  push(reg_9, reg_5);
  goto_24 = !tobool(eq(first(call(reg_8, reg_9)), cns4181));
  if (!goto_24) {
    reg_15 = reg_1.d;
    reg_16 = newStack();
    push(reg_16, add(mul(reg_5, cns4182), cns4183));
    reg_21 = call(reg_15, reg_16);
  }
  if ((goto_24 || false)) {
    goto_24 = false;
    reg_24 = lua$get(reg_2.a, cns4184);
    reg_25 = newStack();
    push(reg_25, reg_5);
    goto_49 = !tobool(eq(first(call(reg_24, reg_25)), cns4185));
    if (!goto_49) {
      reg_31 = reg_1.d;
      reg_32 = newStack();
      push(reg_32, add(mul(lua$length(reg_5), cns4186), cns4187));
      reg_38 = call(reg_31, reg_32);
      reg_39 = reg_1.e;
      reg_40 = newStack();
      push(reg_40, reg_5);
      reg_41 = call(reg_39, reg_40);
    }
    if ((goto_49 || false)) {
      goto_49 = false;
      reg_44 = lua$get(reg_2.a, cns4188);
      reg_45 = newStack();
      push(reg_45, reg_5);
      goto_92 = !tobool(eq(first(call(reg_44, reg_45)), cns4189));
      if (!goto_92) {
        reg_51 = reg_1.d;
        reg_52 = newStack();
        push(reg_52, mul(lua$length(reg_5), cns4190));
        reg_56 = call(reg_51, reg_52);
        reg_57 = cns4191;
        reg_58 = lua$length(reg_5);
        reg_59 = cns4192;
        reg_61 = lt(reg_59, cns4193);
        loop_1: while (true) {
          goto_78 = tobool(reg_61);
          if (!goto_78) {
            if (tobool(gt(reg_57, reg_58))) break loop_1;
          }
          if ((goto_78 || false)) {
            goto_78 = false;
            if (tobool(lt(reg_57, reg_58))) break loop_1;
          }
          reg_69 = lua$get(reg_2.a, cns4194);
          reg_70 = newStack();
          push(reg_70, lua$get(reg_5, reg_57));
          reg_72 = call(reg_69, reg_70);
          reg_57 = add(reg_57, reg_59);
        }
      }
      if ((goto_92 || false)) {
        goto_92 = false;
        reg_76 = lua$get(reg_2.a, cns4195);
        reg_77 = newStack();
        reg_78 = cns4196;
        reg_81 = lua$get(reg_2.a, cns4197);
        reg_82 = newStack();
        push(reg_82, reg_5);
        push(reg_77, concat(reg_78, first(call(reg_81, reg_82))));
        reg_86 = call(reg_76, reg_77);
      }
    }
  }
  reg_87 = newStack();
  return reg_87;
}
var cns4198 = anyStr("write_node")
var cns4199 = anyStr("write_node")
var cns4200 = anyStr("metadata")
function aulua_write$function (reg_0, reg_1) {
  var reg_2, reg_3, reg_12, reg_13, reg_14, reg_17, reg_18, reg_26, reg_27, reg_31, reg_32, reg_35, reg_36, reg_39, reg_41, reg_42, reg_45, reg_48, reg_49, reg_52, reg_53, reg_56, reg_58, reg_59, reg_62, reg_67, reg_68, reg_72, reg_73, reg_74, reg_75, reg_76, reg_77, reg_79, reg_86, reg_87, reg_90, reg_91, reg_98, reg_99, reg_102, reg_103, reg_104, reg_105, reg_106, reg_107, reg_109, reg_116, reg_117, reg_120, reg_121, reg_123, reg_125, reg_126, reg_132, reg_133, reg_136, reg_137, reg_145, reg_146, reg_147, reg_150, reg_151, reg_156, reg_161, reg_166, reg_167, reg_170, reg_171, reg_177, reg_178, reg_187, reg_188, reg_191, reg_192, reg_198, reg_202, reg_203, reg_206, reg_210, reg_211, reg_219, reg_220, reg_224, reg_225, reg_226, reg_227, reg_228, reg_229, reg_231, reg_235, reg_236, reg_242, reg_246, reg_247, reg_255, reg_256, reg_260, reg_261, reg_262, reg_263, reg_264, reg_265, reg_267, reg_274, reg_275, reg_281, reg_282, reg_290, reg_291, reg_294, reg_295, reg_302, reg_303, reg_306, reg_307, reg_308, reg_309, reg_310, reg_311, reg_313, reg_317, reg_318, reg_320, reg_321, reg_328, reg_329, reg_332, reg_333, reg_334, reg_335, reg_336, reg_337, reg_339, reg_343, reg_344, reg_349, reg_353, reg_354, reg_362, reg_363, reg_367, reg_368, reg_369, reg_370, reg_371, reg_372, reg_374, reg_383, reg_384, reg_387, reg_388, reg_397, reg_398, reg_401, reg_412, reg_422, reg_423, reg_427, reg_428, reg_433, reg_434, reg_443, reg_444, reg_446, reg_448, reg_449, reg_457, reg_458, reg_461, reg_462, reg_463, reg_464, reg_465, reg_466, reg_468, reg_472, reg_473, reg_475, reg_476, reg_481, reg_482, reg_483, reg_491, reg_494, reg_495, reg_499, reg_500, reg_501, reg_502, reg_503, reg_504, reg_506, reg_513, reg_516, reg_521, reg_522, reg_527;
  var goto_161=false, goto_179=false, goto_201=false, goto_232=false, goto_255=false, goto_352=false, goto_365=false, goto_500=false, goto_517=false, goto_606=false;
  reg_2 = nil();
  reg_3 = {a: reg_1, b: reg_2, c: reg_2, d: reg_2, e: reg_2};
  reg_3.b = next(reg_0);
  reg_3.c = anyFn((function (a) { return function$90(a,this) }).bind(reg_3));
  reg_3.d = anyFn((function (a) { return function$91(a,this) }).bind(reg_3));
  reg_3.e = anyFn((function (a) { return function$93(a,this) }).bind(reg_3));
  reg_12 = anyFn((function (a) { return function$94(a,this) }).bind(reg_3));
  reg_13 = reg_3.e;
  reg_14 = newStack();
  push(reg_14, cns4012);
  reg_16 = call(reg_13, reg_14);
  reg_17 = reg_3.d;
  reg_18 = newStack();
  push(reg_18, add(lua$length(lua$get(reg_1.a, cns4013)), cns4014));
  reg_25 = call(reg_17, reg_18);
  reg_26 = reg_3.c;
  reg_27 = newStack();
  push(reg_27, cns4015);
  push(reg_27, cns4016);
  reg_30 = call(reg_26, reg_27);
  reg_31 = reg_3.c;
  reg_32 = newStack();
  push(reg_32, cns4017);
  reg_34 = call(reg_31, reg_32);
  reg_35 = reg_3.d;
  reg_36 = newStack();
  reg_39 = lua$get(reg_1.a, cns4018);
  reg_41 = lua$get(reg_39, cns4019);
  reg_42 = newStack();
  push(reg_42, reg_39);
  append(reg_36, call(reg_41, reg_42));
  reg_44 = call(reg_35, reg_36);
  reg_45 = newStack();
  push(reg_45, cns4020);
  reg_47 = call(reg_12, reg_45);
  reg_48 = reg_3.c;
  reg_49 = newStack();
  push(reg_49, cns4021);
  reg_51 = call(reg_48, reg_49);
  reg_52 = reg_3.d;
  reg_53 = newStack();
  reg_56 = lua$get(reg_1.a, cns4022);
  reg_58 = lua$get(reg_56, cns4023);
  reg_59 = newStack();
  push(reg_59, reg_56);
  append(reg_53, call(reg_58, reg_59));
  reg_61 = call(reg_52, reg_53);
  reg_62 = newStack();
  push(reg_62, cns4024);
  reg_64 = call(reg_12, reg_62);
  reg_67 = lua$get(reg_1.a, cns4025);
  reg_68 = newStack();
  push(reg_68, lua$get(reg_1.a, cns4026));
  reg_72 = call(reg_67, reg_68);
  reg_73 = next(reg_72);
  reg_74 = next(reg_72);
  reg_75 = next(reg_72);
  loop_8: while (true) {
    reg_76 = newStack();
    push(reg_76, reg_74);
    push(reg_76, reg_75);
    reg_77 = call(reg_73, reg_76);
    reg_75 = next(reg_77);
    reg_79 = next(reg_77);
    if (tobool(eq(reg_75, nil()))) break loop_8;
    goto_201 = !tobool(lua$get(reg_79, cns4027));
    if (!goto_201) {
      reg_86 = reg_3.c;
      reg_87 = newStack();
      push(reg_87, cns4028);
      reg_89 = call(reg_86, reg_87);
      reg_90 = reg_3.d;
      reg_91 = newStack();
      push(reg_91, lua$length(lua$get(reg_79, cns4029)));
      reg_95 = call(reg_90, reg_91);
      reg_98 = lua$get(reg_1.a, cns4030);
      reg_99 = newStack();
      push(reg_99, lua$get(reg_79, cns4031));
      reg_102 = call(reg_98, reg_99);
      reg_103 = next(reg_102);
      reg_104 = next(reg_102);
      reg_105 = next(reg_102);
      loop_9: while (true) {
        reg_106 = newStack();
        push(reg_106, reg_104);
        push(reg_106, reg_105);
        reg_107 = call(reg_103, reg_106);
        reg_105 = next(reg_107);
        reg_109 = next(reg_107);
        if (tobool(eq(reg_105, nil()))) break loop_9;
        goto_161 = !tobool(lua$get(reg_109, cns4032));
        if (!goto_161) {
          reg_116 = reg_3.c;
          reg_117 = newStack();
          push(reg_117, cns4033);
          reg_119 = call(reg_116, reg_117);
          reg_120 = reg_3.d;
          reg_121 = newStack();
          reg_123 = lua$get(reg_109, cns4034);
          reg_125 = lua$get(reg_123, cns4035);
          reg_126 = newStack();
          push(reg_126, reg_123);
          append(reg_121, call(reg_125, reg_126));
          reg_128 = call(reg_120, reg_121);
        }
        if ((goto_161 || false)) {
          goto_161 = false;
          goto_179 = !tobool(lua$get(reg_109, cns4036));
          if (!goto_179) {
            reg_132 = reg_3.c;
            reg_133 = newStack();
            push(reg_133, cns4037);
            reg_135 = call(reg_132, reg_133);
            reg_136 = reg_3.d;
            reg_137 = newStack();
            push(reg_137, lua$get(lua$get(reg_109, cns4038), cns4039));
            reg_142 = call(reg_136, reg_137);
          }
          if ((goto_179 || false)) {
            goto_179 = false;
            reg_145 = lua$get(reg_1.a, cns4040);
            reg_146 = newStack();
            reg_147 = cns4041;
            reg_150 = lua$get(reg_1.a, cns4042);
            reg_151 = newStack();
            push(reg_151, reg_109);
            push(reg_146, concat(reg_147, first(call(reg_150, reg_151))));
            reg_155 = call(reg_145, reg_146);
          }
        }
        reg_156 = newStack();
        push(reg_156, lua$get(reg_109, cns4043));
        reg_159 = call(reg_12, reg_156);
      }
    }
    if ((goto_201 || false)) {
      goto_201 = false;
      reg_161 = lua$get(reg_79, cns4044);
      if (tobool(reg_161)) {
        reg_161 = lua$get(reg_79, cns4045);
      }
      goto_232 = !tobool(reg_161);
      if (!goto_232) {
        reg_166 = reg_3.c;
        reg_167 = newStack();
        push(reg_167, cns4046);
        reg_169 = call(reg_166, reg_167);
        reg_170 = reg_3.d;
        reg_171 = newStack();
        push(reg_171, lua$get(lua$get(reg_79, cns4047), cns4048));
        reg_176 = call(reg_170, reg_171);
        reg_177 = reg_3.d;
        reg_178 = newStack();
        push(reg_178, lua$get(lua$get(reg_79, cns4049), cns4050));
        reg_183 = call(reg_177, reg_178);
      }
      if ((goto_232 || false)) {
        goto_232 = false;
        goto_255 = !tobool(lua$get(reg_79, cns4051));
        if (!goto_255) {
          reg_187 = reg_3.c;
          reg_188 = newStack();
          push(reg_188, cns4052);
          reg_190 = call(reg_187, reg_188);
          reg_191 = reg_3.d;
          reg_192 = newStack();
          push(reg_192, lua$get(lua$get(reg_79, cns4053), cns4054));
          reg_197 = call(reg_191, reg_192);
          reg_198 = newStack();
          push(reg_198, lua$get(reg_79, cns4055));
          reg_201 = call(reg_12, reg_198);
        }
        if ((goto_255 || false)) {
          goto_255 = false;
          reg_202 = reg_3.c;
          reg_203 = newStack();
          push(reg_203, cns4056);
          reg_205 = call(reg_202, reg_203);
          reg_206 = newStack();
          push(reg_206, lua$get(reg_79, cns4057));
          reg_209 = call(reg_12, reg_206);
        }
      }
    }
  }
  reg_210 = reg_3.d;
  reg_211 = newStack();
  push(reg_211, lua$length(lua$get(reg_1.a, cns4058)));
  reg_216 = call(reg_210, reg_211);
  reg_219 = lua$get(reg_1.a, cns4059);
  reg_220 = newStack();
  push(reg_220, lua$get(reg_1.a, cns4060));
  reg_224 = call(reg_219, reg_220);
  reg_225 = next(reg_224);
  reg_226 = next(reg_224);
  reg_227 = next(reg_224);
  loop_7: while (true) {
    reg_228 = newStack();
    push(reg_228, reg_226);
    push(reg_228, reg_227);
    reg_229 = call(reg_225, reg_228);
    reg_227 = next(reg_229);
    reg_231 = next(reg_229);
    if (tobool(eq(reg_227, nil()))) break loop_7;
    reg_235 = reg_3.d;
    reg_236 = newStack();
    push(reg_236, add(lua$get(reg_231, cns4061), cns4062));
    reg_241 = call(reg_235, reg_236);
    reg_242 = newStack();
    push(reg_242, lua$get(reg_231, cns4063));
    reg_245 = call(reg_12, reg_242);
  }
  reg_246 = reg_3.d;
  reg_247 = newStack();
  push(reg_247, lua$length(lua$get(reg_1.a, cns4064)));
  reg_252 = call(reg_246, reg_247);
  reg_255 = lua$get(reg_1.a, cns4065);
  reg_256 = newStack();
  push(reg_256, lua$get(reg_1.a, cns4066));
  reg_260 = call(reg_255, reg_256);
  reg_261 = next(reg_260);
  reg_262 = next(reg_260);
  reg_263 = next(reg_260);
  loop_4: while (true) {
    reg_264 = newStack();
    push(reg_264, reg_262);
    push(reg_264, reg_263);
    reg_265 = call(reg_261, reg_264);
    reg_263 = next(reg_265);
    reg_267 = next(reg_265);
    if (tobool(eq(reg_263, nil()))) break loop_4;
    goto_352 = !tobool(lua$get(reg_267, cns4067));
    if (!goto_352) {
      reg_274 = reg_3.c;
      reg_275 = newStack();
      push(reg_275, cns4068);
      reg_277 = call(reg_274, reg_275);
    }
    if ((goto_352 || false)) {
      goto_352 = false;
      goto_365 = !tobool(lua$get(reg_267, cns4069));
      if (!goto_365) {
        reg_281 = reg_3.d;
        reg_282 = newStack();
        push(reg_282, add(lua$get(reg_267, cns4070), cns4071));
        reg_287 = call(reg_281, reg_282);
      }
      if ((goto_365 || false)) {
        goto_365 = false;
        reg_290 = lua$get(reg_1.a, cns4072);
        reg_291 = newStack();
        push(reg_291, cns4073);
        reg_293 = call(reg_290, reg_291);
      }
    }
    reg_294 = reg_3.d;
    reg_295 = newStack();
    push(reg_295, lua$length(lua$get(reg_267, cns4074)));
    reg_299 = call(reg_294, reg_295);
    reg_302 = lua$get(reg_1.a, cns4075);
    reg_303 = newStack();
    push(reg_303, lua$get(reg_267, cns4076));
    reg_306 = call(reg_302, reg_303);
    reg_307 = next(reg_306);
    reg_308 = next(reg_306);
    reg_309 = next(reg_306);
    loop_6: while (true) {
      reg_310 = newStack();
      push(reg_310, reg_308);
      push(reg_310, reg_309);
      reg_311 = call(reg_307, reg_310);
      reg_309 = next(reg_311);
      reg_313 = next(reg_311);
      if (tobool(eq(reg_309, nil()))) break loop_6;
      reg_317 = reg_3.d;
      reg_318 = newStack();
      push(reg_318, reg_313);
      reg_319 = call(reg_317, reg_318);
    }
    reg_320 = reg_3.d;
    reg_321 = newStack();
    push(reg_321, lua$length(lua$get(reg_267, cns4077)));
    reg_325 = call(reg_320, reg_321);
    reg_328 = lua$get(reg_1.a, cns4078);
    reg_329 = newStack();
    push(reg_329, lua$get(reg_267, cns4079));
    reg_332 = call(reg_328, reg_329);
    reg_333 = next(reg_332);
    reg_334 = next(reg_332);
    reg_335 = next(reg_332);
    loop_5: while (true) {
      reg_336 = newStack();
      push(reg_336, reg_334);
      push(reg_336, reg_335);
      reg_337 = call(reg_333, reg_336);
      reg_335 = next(reg_337);
      reg_339 = next(reg_337);
      if (tobool(eq(reg_335, nil()))) break loop_5;
      reg_343 = reg_3.d;
      reg_344 = newStack();
      push(reg_344, reg_339);
      reg_345 = call(reg_343, reg_344);
    }
    if (tobool(lua$get(reg_267, cns4080))) {
      reg_349 = newStack();
      push(reg_349, lua$get(reg_267, cns4081));
      reg_352 = call(reg_12, reg_349);
    }
  }
  reg_353 = reg_3.d;
  reg_354 = newStack();
  push(reg_354, lua$length(lua$get(reg_1.a, cns4082)));
  reg_359 = call(reg_353, reg_354);
  reg_362 = lua$get(reg_1.a, cns4083);
  reg_363 = newStack();
  push(reg_363, lua$get(reg_1.a, cns4084));
  reg_367 = call(reg_362, reg_363);
  reg_368 = next(reg_367);
  reg_369 = next(reg_367);
  reg_370 = next(reg_367);
  loop_2: while (true) {
    reg_371 = newStack();
    push(reg_371, reg_369);
    push(reg_371, reg_370);
    reg_372 = call(reg_368, reg_371);
    reg_370 = next(reg_372);
    reg_374 = next(reg_372);
    if (tobool(eq(reg_370, nil()))) break loop_2;
    goto_500 = !tobool(eq(lua$get(reg_374, cns4085), cns4086));
    if (!goto_500) {
      reg_383 = reg_3.c;
      reg_384 = newStack();
      push(reg_384, cns4087);
      reg_386 = call(reg_383, reg_384);
      reg_387 = reg_3.d;
      reg_388 = newStack();
      push(reg_388, lua$get(reg_374, cns4088));
      reg_391 = call(reg_387, reg_388);
    }
    if ((goto_500 || false)) {
      goto_500 = false;
      goto_517 = !tobool(eq(lua$get(reg_374, cns4089), cns4090));
      if (!goto_517) {
        reg_397 = reg_3.c;
        reg_398 = newStack();
        push(reg_398, cns4091);
        reg_400 = call(reg_397, reg_398);
        reg_401 = newStack();
        push(reg_401, lua$get(reg_374, cns4092));
        reg_404 = call(reg_12, reg_401);
      }
      if ((goto_517 || false)) {
        goto_517 = false;
        goto_606 = !tobool(eq(lua$get(reg_374, cns4093), cns4094));
        if (!goto_606) {
          reg_412 = lua$length(lua$get(reg_374, cns4095));
          if (tobool(ne(reg_412, lua$length(lua$get(lua$get(reg_374, cns4096), cns4097))))) {
            reg_422 = lua$get(reg_1.a, cns4098);
            reg_423 = newStack();
            reg_427 = lua$get(lua$get(reg_374, cns4099), cns4100);
            reg_428 = cns4101;
            reg_433 = lua$length(lua$get(lua$get(reg_374, cns4102), cns4103));
            reg_434 = cns4104;
            push(reg_423, concat(reg_427, concat(reg_428, concat(reg_433, concat(reg_434, lua$length(lua$get(reg_374, cns4105)))))));
            reg_442 = call(reg_422, reg_423);
          }
          reg_443 = reg_3.d;
          reg_444 = newStack();
          reg_446 = lua$get(reg_374, cns4106);
          reg_448 = lua$get(reg_446, cns4107);
          reg_449 = newStack();
          push(reg_449, reg_446);
          push(reg_444, add(first(call(reg_448, reg_449)), cns4108));
          reg_454 = call(reg_443, reg_444);
          reg_457 = lua$get(reg_1.a, cns4109);
          reg_458 = newStack();
          push(reg_458, lua$get(reg_374, cns4110));
          reg_461 = call(reg_457, reg_458);
          reg_462 = next(reg_461);
          reg_463 = next(reg_461);
          reg_464 = next(reg_461);
          loop_3: while (true) {
            reg_465 = newStack();
            push(reg_465, reg_463);
            push(reg_465, reg_464);
            reg_466 = call(reg_462, reg_465);
            reg_464 = next(reg_466);
            reg_468 = next(reg_466);
            if (tobool(eq(reg_464, nil()))) break loop_3;
            reg_472 = reg_3.d;
            reg_473 = newStack();
            reg_475 = lua$get(reg_468, cns4111);
            reg_476 = newStack();
            push(reg_476, reg_468);
            append(reg_473, call(reg_475, reg_476));
            reg_478 = call(reg_472, reg_473);
          }
        }
        if ((goto_606 || false)) {
          goto_606 = false;
          reg_481 = lua$get(reg_1.a, cns4112);
          reg_482 = newStack();
          reg_483 = cns4113;
          push(reg_482, concat(reg_483, concat(lua$get(reg_374, cns4114), cns4115)));
          reg_489 = call(reg_481, reg_482);
        }
      }
    }
  }
  reg_491 = anyFn((function (a) { return function$95(a,this) }).bind(reg_3));
  reg_494 = lua$get(reg_1.a, cns4177);
  reg_495 = newStack();
  push(reg_495, lua$get(reg_1.a, cns4178));
  reg_499 = call(reg_494, reg_495);
  reg_500 = next(reg_499);
  reg_501 = next(reg_499);
  reg_502 = next(reg_499);
  loop_1: while (true) {
    reg_503 = newStack();
    push(reg_503, reg_501);
    push(reg_503, reg_502);
    reg_504 = call(reg_500, reg_503);
    reg_502 = next(reg_504);
    reg_506 = next(reg_504);
    if (tobool(eq(reg_502, nil()))) break loop_1;
    if (tobool(lua$get(reg_506, cns4179))) {
      reg_513 = newStack();
      push(reg_513, reg_506);
      reg_514 = call(reg_491, reg_513);
    }
  }
  reg_516 = anyFn((function (a) { return function$97(a,this) }).bind(reg_3));
  lua$set(reg_1.a, cns4198, reg_516);
  reg_521 = lua$get(reg_1.a, cns4199);
  reg_522 = newStack();
  push(reg_522, lua$get(reg_1.a, cns4200));
  reg_526 = call(reg_521, reg_522);
  reg_527 = newStack();
  return reg_527;
}
function aulua_write$lua_main (reg_0) {
  var reg_2, reg_3;
  reg_2 = {a: nil()};
  reg_2.a = reg_0;
  reg_3 = newStack();
  push(reg_3, anyFn((function (a) { return aulua_write$function(a,this) }).bind(reg_2)));
  return reg_3;
}
var cns4201 = anyStr("open")
var cns4202 = anyStr("parse")
function aulua_compile$function (reg_0, reg_1) {
  var reg_4, reg_5, reg_6, reg_9, reg_10, reg_16, reg_17, reg_18, reg_20, reg_21, reg_22, reg_24, reg_25, reg_27;
  reg_2 = nil();
  reg_3 = {a: reg_1};
  reg_4 = next(reg_0);
  reg_5 = next(reg_0);
  reg_6 = next(reg_0);
  reg_9 = lua$get(reg_1.a, cns4201);
  reg_10 = newStack();
  push(reg_10, reg_4);
  reg_11 = call(reg_9, reg_10);
  reg_16 = call(lua$get(reg_1.a, cns4202), newStack());
  reg_17 = next(reg_16);
  reg_18 = next(reg_16);
  if (tobool(reg_18)) {
    reg_20 = newStack();
    push(reg_20, reg_18);
    return reg_20;
  }
  reg_21 = reg_1.b;
  reg_22 = newStack();
  push(reg_22, reg_17);
  push(reg_22, reg_6);
  reg_23 = call(reg_21, reg_22);
  reg_24 = reg_1.c;
  reg_25 = newStack();
  push(reg_25, reg_5);
  reg_26 = call(reg_24, reg_25);
  reg_27 = newStack();
  return reg_27;
}
function aulua_compile$lua_main (reg_0) {
  var reg_1, reg_2, reg_9;
  reg_1 = nil();
  reg_2 = {a: reg_1, b: reg_1, c: reg_1};
  reg_2.a = first(lua_parser_parser$lua_main(reg_0));
  reg_2.b = first(aulua_codegen$lua_main(reg_0));
  reg_2.c = first(aulua_write$lua_main(reg_0));
  reg_9 = newStack();
  push(reg_9, anyFn((function (a) { return aulua_compile$function(a,this) }).bind(reg_2)));
  return reg_9;
}
function fn1 (reg_0, reg_1) {
  var reg_6, reg_8;
  reg_6 = next(aulua_compile$lua_main(get_global()));
  reg_8 = newStack();
  push(reg_8, anyStr(reg_0));
  push(reg_8, reg_1);
  push(reg_8, anyStr("<lua-string>"));
  reg_12 = call(reg_6, reg_8);
  return;
}
window["LuaCompiler"] = {
  "compile": fn1,
  "Stack_new": newStack,
  "Stack_next": next,
  "Stack_push": push,
  "Function": Function$29,
};
})(window)
